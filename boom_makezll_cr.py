# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_zllvr_proxy_comparison_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.cuts.kinematics import CutInvariantMass_zll_vr

# retrieve physics processes
physicsProcesses = get_processes()
physicsProcesses = filter(lambda p: 'Zll' in p.name, physicsProcesses)

# choose which channel to compare with corresponding Zll VR proxy (default: hadhad)
do_ll = False
do_lh = True

# choose which region to compare with corresponding Zll VR proxy (default : VBF)
do_boost = True

years = (
#    '15', 
#    '16', 
#    '17', 
   '18'
   )
#years = ('16',)

if do_ll:
   chs = ('emu', 'mue')
   zll_region = ('Ztt_cr_corr_ll',)
elif do_lh :
   chs = ('e1p', 'e3p', 'mu1p', 'mu3p')
   zll_region = ('Ztt_cr_corr_lh',)
else :
   chs = ('1p1p','1p3p','3p1p','3p3p') 
   zll_region = ('Ztt_cr_corr_hh',)

if do_boost: 
     sels_zllvr = get_selections(
        years=years, 
        regions=zll_region, 
        categories=('boost'))
else : 
     if (do_lh or do_ll) :  
         sels_zllvr = get_selections(
            years=years, 
            regions=zll_region, 
            categories=('vbf_tight','vbf_loose'))
     else : 
         sels_zllvr = get_selections(
            years=years, 
            regions=zll_region, 
            categories=('vbf_tight','vbf_loose','vbf_lowdr'))

sels = sels_zllvr

#CutInvariantMass_zll_vr.cut.cut = 'ditau_p4.M() > 80 && tau_0_p4.Pt() > 70 && tau_1_p4.Pt() > 25'
### define your list of variables
variables = [
   VARIABLES['colinear_mass_corr'],
#     VARIABLES['pt_total'],
#     VARIABLES['mjj'],
#     VARIABLES['jets_deta'],
#     VARIABLES['jets_eta_prod'],
#     VARIABLES['var_ptjj'],
#     VARIABLES['vbf_tagger'],
#      VARIABLES['met'],
#    VARIABLES['met_centrality'],
#     VARIABLES['ditau_dr'],
#     VARIABLES['higgs_pt'],
#     VARIABLES['higgs_pt_res']
#      VARIABLES['mjj'],
#      VARIABLES['jets_deta'],
#      VARIABLES['tau_0_pt'],
#      VARIABLES['tau_1_pt'],
#   VARIABLES['ditau_pt_truth'],
#   VARIABLES['pt_total'],
#   VARIABLES['mjj'],
#   VARIABLES['jets_deta'],
#   VARIABLES['jets_eta_prod'],
#   VARIABLES['var_ptjj'],
#   VARIABLES['vbf_tagger'],
  # VARIABLES['tau_0_pt'],
##   VARIABLES['tau_1_pt'],
#   VARIABLES['tau_0_pt_corr'],
#   VARIABLES['hh_vh_tagger_corr'],
#   VARIABLES['lh_vh_tagger_corr'],
#   VARIABLES['ll_vh_tagger_corr'],
   # VARIABLES['jet_1_tau_0_dr_corr'],
   # VARIABLES['jet_1_tau_1_dr_corr'],
   # VARIABLES['jet_0_tau_0_dr_corr'],
   # VARIABLES['jet_0_tau_1_dr_corr'],
#    variables['tau_1_pt_corr'],
#   VARIABLES['met'],
#   VARIABLES['ditau_dr'],
#   VARIABLES['met_centrality'],
#   VARIABLES['taus_pt_ratio'],
#    VARIABLES['higgs_pt'],
#   VARIABLES['ditau_pt_truth'],
#   VARIABLES['met'],
##   VARIABLES['ditau_pt_corr'],
#   VARIABLES['met_corr'],
#   VARIABLES['x0'],
#   VARIABLES['x0_corr'],
#   VARIABLES['x1'],
#   VARIABLES['x1_corr'],

   ]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables, verbose=True)
processor.book()
processor.run(n_cores=1, nentries=50000)

title = 'LepLep' if do_ll else 'LepHad' if do_lh else 'HadHad'
title += '_Boost SRs' if do_boost else '_VBF SRs'
#title += '_2Dcut'

### plot making

# Create new file
output_file = ROOT.TFile('zll_histograms.root', 'recreate')

# Store the histogram we want
for var in variables:
   h_zllvr = processor.get_hist_physics_process('Zll', sels_zllvr,  var)
   h_zllvr.SetName(var.name)

   output_file.cd()
   h_zllvr.Write()

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
