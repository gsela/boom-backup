import os
import itertools
import json

with open('data/condor_queue.json') as f:
    _QUEUE_DICT = json.load(f)

with open('data/condor_memory.json') as f:
    _MEMORY_DICT = json.load(f)

_QUEUES = [
    'espresso',
    'microcentury',
    'longlunch',
    'workday',
    'tomorrow',
    'testmatch',
    'nextweek',
]

def _determine_memory_need(memory):
    _vals = [500, 1500, 2000, 4000, 8000, 12000, 16000, 20000, 24000, 32000]
    _closest = min(_vals, key=lambda x: abs(x - memory))
    _idx = _vals.index(_closest)
    if _idx == len(_vals) - 1:
        return _vals[_idx]
    else:
        return _vals[_idx + 1]

def _rerun_cmd(job, naf=False, bnl=False):
    args = [
        'python',
        'submit_workspace.py',
        '--year {}'.format(job.year),
    ]
    if naf:
        args += ["--naf"]

    if bnl:
        args += ["--bnl"]

    if job.channel != 'all':
        args += ['--channel {}'.format(job.channel)]
    args +=[
        '--process-group-name {}'.format(job.process),
        '--var-group-name {}'.format(job.variation),
    ]

    if job.variation != 'nominal':
        args += ['--skip-nominal']
    
    if job.zcr:
        args += ['--zcr']
        if 'ee' in job.region:
            args += ['--zcr-flavour ee']
        elif 'mumu' in job.region:
            args += ['--zcr-flavour mumu']
        else:
            raise ValueError('zcr flavour needs to be specified')
            
        if not 'corr' in job.region:
            args += ['--zcr-deactivate-corrections']
        
        if 'embed_' in job.variation:
            args += ['--zcr-embeding-setup {}'.format(job.variation)]

        if job.zllqcd_dsid != None:
            args += ['--zcr-zllqcd-dsid {}'.format(job.zllqcd_dsid)]

    _submission_key = '_'.join([
        job.year,
        job.channel,
        job.region,
        job.variation,
        job.process])

    if job.zllqcd_dsid != None:
        _submission_key += '_' + job.zllqcd_dsid

    if not bnl:
        if not _submission_key in _QUEUE_DICT.keys():
            # print 'BOOM: {} is missing from the queue dict'.format(_queue_key)
            if 'msg' in job.output.keys() and 'aborted' in job.output['msg']['log']:
                # default is workday, if it faills move to tomorrow
                args += ['--forced-queue tomorrow']
        else:
            _old_queue = _QUEUE_DICT[_submission_key]
            if 'msg' in job.output.keys() and 'aborted' in job.output['msg']['log']:
                _idx = _QUEUES.index(_old_queue)
                if _idx + job.job_number > len(_QUEUES) - 2:
                    _queue = _QUEUES[-1]
                else:
                    _queue = _QUEUES[_idx + job.job_number + 1]
                
                args += ['--forced-queue {}'.format(_queue)]
    else:
        if job.memory != None:
            args += ['--memory {}'.format(_determine_memory_need(job.memory))]

    args += ['--submit']
    return ' '.join(args)
    

def _wsi_output_name(outdir, years=None, channels=None, regions=None):

    if years != None:
        if not isinstance(years, (list, tuple)):
            _years = [years]
        else:
            _years = years
        _years_str = ''.join(_years).replace('_', '').replace(' ', '')
    else:
        _years_str = '15161718'

    if channels != None:
        if not isinstance(channels, (list, tuple)):
            _channels = [channels]
        else:
            _channels = channels
        _channels_str = ''.join(_channels)
    else:
        _channels_str = 'All'

    if regions != None:
        if not isinstance(regions, (list, tuple)):
            _regions = [regions]
        else:
            _regions = regions
        _regions_str = ''.join(regions)
    else:
        _regions_str = 'Allregions'

    from datetime import date
    _date = '{}.{}.{}'.format(
        date.today().year,        
        date.today().month,
        date.today().day)

    _file_name = 'wsi_{}_{}decays_{}_{}.root'.format(_years_str, _channels_str, _regions_str, _date)
    _name = os.path.join(outdir, _file_name)
    return _name

def _build_hadd_list(jobs):
    _hadd_list = []
    _variations = []
    for _job in jobs:
        if not _job.corrupted:
            print _job.output['dir']
            if _job.variation not in _variations:
                if _job.variation != None:
                    _variations.append(_job.variation)
            _path = os.path.join(
                _job.output['path'], 
                _job.output['dir'], 
                _job.output['rfile'])
            _hadd_list.append(_path)
        else:
            print 'BOOM: corupted job {} -- ignore!'.format(_job)
    if len(_variations) == 0:
        _variations = None
    return _hadd_list, _variations

def _hadd(out_name, input_list, variations=None, dry_run=False):
    import subprocess
    _cmds = []
    if variations != None:
        _output_fragments = []
        for _v in variations:
            _input_list = filter(lambda n: _v in n, input_list)
            _out_fragment = out_name.replace('.root',  '_' + _v + '.root')
            _output_fragments += [_out_fragment]
            _cmds += ['hadd {} {}'.format(_out_fragment, ' '.join(_input_list))]
        print _output_fragments
        if len(_output_fragments) > 1:
            _cmds += ['hadd {} {}'.format(out_name, ' '.join(_output_fragments))]
    else:
        _input_str = ' '.join(input_list)
        _cmds += ['hadd {} {}'.format(out_name, _input_str)]

    for i_cmd, _cmd in enumerate(_cmds):
        print '{} / {}: {}'.format(i_cmd + 1, len(_cmds), _cmd)
        if not dry_run:
            subprocess.call(_cmd, shell=True)



if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('condor_input_dir')
    parser.add_argument('--channel', default=None)
    parser.add_argument('--year', default=None)
    parser.add_argument('--process-group-name', default=None, nargs='*')
    parser.add_argument('--var-group-name', default=None, nargs='*')
    parser.add_argument('--zcr', default=False, action='store_true')
    parser.add_argument('--zcr-flavour', default='mumu', choices=['ee', 'mumu'])
    parser.add_argument('--zcr-deactivate-corrections', default=False, action='store_true', help = "(default: %(default)s)")
    parser.add_argument('--stxs', default='stxs12_fine', choices=['stxs1', 'stxs12', 'stxs12_fine'])
    parser.add_argument('--hadd', default=False, action='store_true')
    parser.add_argument('--dry', default=False, action='store_true')
    parser.add_argument('--outdir', default='./')
    parser.add_argument('--construct-dict', default=False, action='store_true')
    parser.add_argument('--verbose', default=False, action='store_true')
    alternate_sub = parser.add_mutually_exclusive_group()
    alternate_sub.add_argument('--naf', default=False, action='store_true')
    alternate_sub.add_argument('--bnl', default=False, action='store_true')
    args = parser.parse_args()

    from boom.extern.tabulate import tabulate
    from boom.batch.utils import filter_jobs, expected_jobs, build_jobs

    if args.zcr:
        if args.zcr_deactivate_corrections:
            _region = 'zcr_' + args.zcr_flavour
        else:
            _region = 'zcr_{}_corr'.format(args.zcr_flavour)
    else:
        _region = 'sr'
        
    _dir = args.condor_input_dir
    _outdir = args.outdir

    _all_jobs = build_jobs(_dir)
    _all_jobs = filter_jobs(
        _all_jobs, 
        channel=args.channel, 
        process=args.process_group_name, 
        variation=args.var_group_name,
        year=args.year, 
        region=_region)
    if len(_all_jobs) == 0:
        raise RuntimeError('No jobs are being considered. Are you using the right arguments?')

    missings = []
    _expected = expected_jobs(
        channel=args.channel, 
        year=args.year, 
        process_group=args.process_group_name,
        variation_group=args.var_group_name,
        z_cr=args.zcr, 
        z_cr_flavour=args.zcr_flavour,
        stxs=args.stxs)

    
    for  _job in _expected:
        _filtered_jobs = filter_jobs(
            _all_jobs, 
            year=_job.year, 
            channel=_job.channel, 
            process=_job.process, 
            variation=_job.variation, 
            region=_job.region,
            zllqcd_dsid=_job.zllqcd_dsid,
            verbose=args.verbose)
        if len(_filtered_jobs) > 1:
            raise ValueError('ambiguous jobs')
        
        if len(_filtered_jobs) == 0 or _filtered_jobs[0].output['rfile'] == None:
            if len(_filtered_jobs) == 0:
                _attempt = 0
                _status = 'not started'
            else:
                _attempt = _filtered_jobs[0].job_number
                _status = _filtered_jobs[0].status
            missings += [[
                _job.channel,
                _job.year,
                _job.region,
                _job.process,
                _job.variation,
                _job.zllqcd_dsid,
                _status,
                _attempt,
            ]]
            
    print
    print 'BOOM: missing jobs:'
    print
    print tabulate(missings, headers=['chan', 'year', 'reg', 'proc', 'var', 'dsid', 'state', 'running attempt'])
    print 

    _finished_jobs = filter_jobs(_all_jobs, finished=True)
    _corrupted_jobs = filter_jobs(_all_jobs, corrupted=True, finished=True)
    _evicted_jobs = filter_jobs(_all_jobs, evicted=True, finished=True)
    _held_jobs = filter_jobs(_all_jobs, held=True, finished=True)
    _bad_jobs = _corrupted_jobs + _evicted_jobs + _held_jobs
    print
    print 'BOOM: running status:'
    print
    print tabulate([[
        len(_all_jobs), 
        len(_finished_jobs), 
        len(_finished_jobs) - len(_bad_jobs),
        float(len(_finished_jobs) - len(_bad_jobs)) / float(len(_all_jobs)) * 100.]],
                    headers=['Total', 'Finished', 'Uncorrupted', 'Fraction done (%)'])


    lines = []
    for _job in _bad_jobs:
        lines += [[
            _job.channel,
            _job.year,
            _job.region,
            _job.process,
            _job.variation,
            _job.corrupted,
            _job.zllqcd_dsid,
            _job.status,
            _job.done
            ]]

    print
    print 'BOOM: bad jobs'
    print
    print tabulate(lines, headers=['chan', 'year', 'reg', 'proc', 'var', 'corrupted', 'dsid', 'status', 'finished'])
    print  
    
    if len(_corrupted_jobs) != 0:
        print 'BOOM: Corruption Reports:'
        print 'BOOM: ------------------'
        for _job in _corrupted_jobs:
            print 'BOOM:', _job
            print 'BOOM: \t dir - ', _job.output['dir']
            print 'BOOM: \t err - ', _job.output['msg']['err']
            print 'BOOM: \t out - ', _job.output['msg']['out']
            print 'BOOM: \t log - ', _job.output['msg']['log']
            print

        print 'BOOM: Rerun commands:'
        print 'BOOM: --------------'
        for _job in _corrupted_jobs:
            print _rerun_cmd(_job, naf=args.naf, bnl=args.bnl)
        print 'WARNING WARNING: If you are running on something else than the default variable, do not forget to add --variable YOUR_VAR (the rerun command does not have it just yet)'

    if len(_evicted_jobs) != 0:
        print 'BOOM: Eviction Reports:'
        print 'BOOM: ----------------'
        from boom.batch.tools import _memory
        for _job in _evicted_jobs:
            print 'BOOM:', _job, _job.memory
        for _job in _evicted_jobs:
            print _rerun_cmd(_job, naf=args.naf, bnl=args.bnl)

    if args.hadd:
        _name = _wsi_output_name(_outdir, years=args.year, channels=args.channel, regions=_region)
        _hadd_list, _variations = _build_hadd_list(_all_jobs)
        _hadd(_name, _hadd_list, variations=_variations, dry_run=args.dry)



    if args.construct_dict:
        _submission_dict = {}
        for _job in _all_jobs:
            if not _job.corrupted:
                _key = '_'.join([
                    _job.year,
                    _job.channel,
                    _job.region,
                    _job.variation,
                    _job.process])
                if _job.zllqcd_dsid != None:
                    _key += '_' + _job.zllqcd_dsid
                if args.bnl:
                    if _job.memory != None:
                        _submission_dict[_key] = _determine_memory_need(_job.memory)
                else:
                    _submission_dict[_key] = _job.best_queue

        if args.bnl:
            _json_outfile = args.condor_input_dir.replace('/', '') + '_memory.json'
        else:
            _json_outfile = args.condor_input_dir.replace('/', '') + '_queue.json'
        import json
        with open(_json_outfile, 'w') as fout:
            json.dump(_submission_dict, fout, indent=2)
