import itertools
import os

def _wsi_output_name(outdir, variable, years=None, channels=None):

    if years != None:
        if not isinstance(years, (list, tuple)):
            _years = [years]
        else:
            _years = years
        _years_str = ''.join(_years).replace('_', '').replace(' ', '')
    else:
        _years_str = '15161718'

    if channels != None:
        if not isinstance(channels, (list, tuple)):
            _channels = [channels]
        else:
            _channels = channels
        _channels_str = ''.join(_channels)
    else:
        _channels_str = 'All'

    from datetime import date
    _date = '{}.{}.{}'.format(
        date.today().year,        
        date.today().month,
        date.today().day)

    _file_name = 'wsi_embedding_{}_{}_{}decays_{}.root'.format(
        variable, _years_str, _channels_str, _date)
    _name = os.path.join(outdir, _file_name)
    return _name

def _build_hadd_list(jobs, oversampling=False):
    _hadd_list = []
    for _job in jobs:
        if _job.attempts != 1 and oversampling and '_corr' in _job.region:
            for _sub_job in _job._infos:
                if not _sub_job['corrupted']:
                    print _sub_job['dir']
                    _path = os.path.join(
                        _sub_job['path'], 
                        _sub_job['dir'], 
                        _sub_job['rfile'])
                    _hadd_list.append(_path)
                else:
                    print 'BOOM: corupted job {} -- ignore!'.format(_sub_job)
        else:
            if not _job.corrupted:
                print _job.output['dir']
                _path = os.path.join(
                    _job.output['path'], 
                    _job.output['dir'], 
                    _job.output['rfile'])
                _hadd_list.append(_path)
            else:
                print 'BOOM: corupted job {} -- ignore!'.format(_job)
    return _hadd_list

def _hadd(out_name, input_list, dry_run=False):
    import subprocess
    _input_str = ' '.join(input_list)
    _cmd = 'hadd {} {}'.format(out_name, _input_str)
    if not dry_run:
        subprocess.call(_cmd, shell=True)
    else:
        print out_name
        for _i_f, _file in enumerate(input_list):
            print '{} / {} - {}'.format(_i_f + 1, len(input_list), _file.split('/')[-1])


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('variable')
    parser.add_argument('--condor-input-dir', default=None)
    parser.add_argument('--year', default=None)
    parser.add_argument('--channel', default=None)
    parser.add_argument('--hadd', default=False, action='store_true')
    parser.add_argument('--dry', default=False, action='store_true')
    parser.add_argument('--outdir', default='./')
    parser.add_argument('--oversampling', default=False, action='store_true')
    args = parser.parse_args()


    from boom.batch.utils import build_jobs, filter_jobs
    from boom.extern.tabulate import tabulate

    _input_dir = 'condor_' + args.variable
    if args.condor_input_dir != None:
        _input_dir = args.condor_input_dir

    _all_jobs = build_jobs(_input_dir)
    _all_jobs = filter_jobs(_all_jobs, channel=args.channel, year=args.year)

    for _job in _all_jobs:
        if '_corr' in _job.region:
            _job._oversampling = args.oversampling

    _corrupted_jobs = filter_jobs(_all_jobs, corrupted=True)
    lines = []
    for _job in _corrupted_jobs:
        lines += [[
            _job.channel,
            _job.year,
            _job.region,
            _job.process,
            _job.variation,
            _job.corrupted,
            _job.done
            ]]

    print
    print 'BOOM: corrupted jobs'
    print
    print tabulate(lines, headers=['chan', 'year', 'reg', 'proc', 'var', 'corrupted', 'finished'])
    print  

    for job in _all_jobs:
        if not job.corrupted:
            continue
        if job.attempts > 1:
            for _sub_job in job._infos:
                # print _sub_job.keys(), _sub_job['corrupted']
                if _sub_job['corrupted']:
                    print 'BOOM:', job
                    print 'BOOM: \t dir - ', _sub_job['dir']
                    print 'BOOM: \t err - ', _sub_job['msg']['err']
                    print 'BOOM: \t out - ', _sub_job['msg']['out']
                    print 'BOOM: \t log - ', _sub_job['msg']['log']
                    print
        else:
            print 'BOOM:', job
            print 'BOOM: \t dir - ', job.output['dir']
            print 'BOOM: \t err - ', job.output['msg']['err']
            print 'BOOM: \t out - ', job.output['msg']['out']
            print 'BOOM: \t log - ', job.output['msg']['log']
            print


    if args.hadd:
        _name = _wsi_output_name(args.outdir, args.variable, years=args.year, channels=args.channel)
        _hadd_list = _build_hadd_list(_all_jobs, oversampling=args.oversampling)
        _hadd(_name, _hadd_list, dry_run=args.dry)
