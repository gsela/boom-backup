# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes


#import logging
#logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes(stxs='stxs0', squash=True)

_channels = (
    '1p1p',
    '1p3p',
    '3p1p',
    '3p3p',
    )

_years = (
    '15',
    '16',
    '17',
    '18',
)

_categories = (
    'boost',
    'vh',
    'vbf',
    'tth',
)

_regions = (
    'SR',
    )

### define your selection objects 
sels = get_selections(
    channels=_channels, 
    years=_years,
    categories=_categories,
    regions=_regions)


# define your list of variables
_variables = [
    VARIABLES['tau_0_pt'],
    VARIABLES['tau_0_eta'],
    VARIABLES['tau_0_phi'],
    VARIABLES['tau_1_pt'],
    VARIABLES['tau_1_eta'],
    VARIABLES['tau_1_phi'],
    VARIABLES['jet_0_pt'],
    VARIABLES['jet_0_eta'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, _variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

# plot making
for reg in _regions:
    for var in _variables:
        for cat in _categories:
            # no channel split is possible while we have nOS
            make_data_mc_plot(processor, sels, var, categories=cat, regions=reg)


print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
