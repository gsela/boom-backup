# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_channel_comparison_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# retrieve physics processes
physicsProcesses = get_processes()
physicsProcesses = filter(lambda p: 'Ztt' in p.name or p.isSignal, physicsProcesses)

normalize = True

_categories = (
    'preselection',
    'boost',
    'vbf',
    'vh',
    )

_years = (
    '15',
    '16',
    '17',
    '18',
     )

### define your selection objects 
sels = get_selections(
    channels=(
        'emu', 'mue', 
        'e1p', 'e3p', 'mu1p', 'mu3p', 
        '1p1p', '1p3p', '3p1p', '3p3p'),
    regions='SR',
    years=_years,
    categories=_categories)

### define your list of variables
variables = [
    VARIABLES['ditau_dr'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

### plot making
for cat in _categories:
    for var in variables:
        make_channel_comparison_plot(processor, sels, var, process_name='Signal', title=cat, norm_unit=normalize )
        make_channel_comparison_plot(processor, sels, var, process_name='Ztt', title=cat, norm_unit=normalize)

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
