import os
import ROOT
from multiprocessing import cpu_count
#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# Switch on debug output
# import logging
# logging.root.setLevel( logging.DEBUG )


compute_significance = True
split_signal = True
force_unblind = True

# retrieve physics processes
physicsProcesses = get_processes()

_categories = (
    'preselection',
    # 'boost',
    # 'vbf',
    # 'vh',
    # 'tth_1',
)

_years = (
    '15',
    '16',
    '17',
    '18',
)

_channels = (
    'emu', 'mue', 
    'e1p', 'e3p', 'mu1p', 'mu3p',
    '1p1p', '1p3p', '3p1p', '3p3p'
)

### define your selection objects 
sels = get_selections(
    channels=_channels,
    categories=_categories,
    years=_years,
    regions='SR')

# define your list of variables
variables = [
    VARIABLES['higgs_pt_maxw_low'],
    VARIABLES['higgs_pt_low'],
    VARIABLES['higgs_pt_maxw_minus_higgs_pt'],
    # VARIABLES['vbf_tagger_pr'],
    # VARIABLES['mmc_mlm_m'],
    # VARIABLES['tau_0_pt'],
    # VARIABLES['tau_1_pt'],
    # VARIABLES['higgs_pt'],
    # VARIABLES['ditau_dr'],
    # VARIABLES['ditau_deta'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

# plot making
for var in variables:
    #     # all channels
    make_data_mc_plot(processor, sels, var)
    # leplep
    make_data_mc_plot(processor, sels, var, channels=('emu', 'mue'))
    # lephad
    make_data_mc_plot(processor, sels, var, channels=('e1p', 'e3p', 'mu1p', 'mu3p'))
    # hadhad
    make_data_mc_plot(processor, sels, var, channels=('1p1p', '1p3p', '3p1p', '3p3p'))

    for _cat in _categories:
        make_data_mc_plot(
            processor, sels, var, 
            categories=_cat, 
            compute_significance=compute_significance, 
            force_unblind=force_unblind,
            split_signal=split_signal)
        make_data_mc_plot(
            processor, sels, var, 
            channels=('emu', 'mue'), 
            categories=_cat, 
            compute_significance=compute_significance, 
            force_unblind=force_unblind,
            split_signal=split_signal)
        make_data_mc_plot(
            processor, sels, var, 
            channels=('e1p', 'e3p', 'mu1p', 'mu3p'), 
            categories=_cat, 
            compute_significance=compute_significance, 
            force_unblind=force_unblind,
            split_signal=split_signal)
        make_data_mc_plot(
            processor, sels, var, 
            channels=('1p1p', '1p3p', '3p1p', '3p3p'), 
            categories=_cat, 
            compute_significance=compute_significance, 
            force_unblind=force_unblind,
            split_signal=split_signal)




print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
