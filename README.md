Welcome to the BOOM analysis package.
Download it, run it and boom you get plots.

# [Documentation](http://cern.ch/boom-doc)

The documentation of the project lives here: http://cern.ch/boom-doc

## Installation on lxplus (to be adapted for other computer systems)
```bash
setupATLAS
lsetup "root 6.18.04-x86_64-centos7-gcc8-opt"
git clone ssh://git@gitlab.cern.ch:7999/ATauLeptonAnalysiS/HAPPy.git
git clone ssh://git@gitlab.cern.ch:7999/ATauLeptonAnalysiS/boom.git
source HAPPy/setup.sh
cd boom/
```

## Authors
* Antonio De Maria
* Quentin Buat
* Pier-Olivier Deviveiros
