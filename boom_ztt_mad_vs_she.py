# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# retrieve physics processes
physicsProcesses = get_processes(stxs='stxs0', squash=True, add_ztt_madgraph=True)
ztt_processes = filter(lambda p: 'Ztt' in p.name, physicsProcesses)

_categories = (
     'boost_0',
     'boost_1',
     'boost_2',
     'boost_3',
     'vbf_0',
     'vbf_1',
     'vh_0',
     'vh_1',
    )

### define your selection objects 
sels = get_selections(
    channels=(
        'emu', 'mue',
        'e1p', 'e3p', 'mu1p', 'mu3p',
        '1p1p', '1p3p', '3p1p', '3p3p'
        ),
    years=('15', '16', '17', '18'), 
    categories=_categories,
    regions='SR')

# define your list of variables
variables = [
    VARIABLES['mmc_mlm_m'],
]

# #### processor declaration, booking and running
processor = boom_processor(ztt_processes, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

# plot making
import ROOT
stop_watch = ROOT.TStopwatch()

from boom.plotting import make_mad_vs_she_plot
for _cat in _categories:
    for var in variables:
        make_mad_vs_she_plot(processor, sels, var, categories=_cat)


stop_watch.Stop()
stop_watch.Print()

print 'closing stores...'
close_stores(ztt_processes)
print 'done'



