# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

physicsProcesses = get_processes(stxs='stxs0',squash=True,split_ttbar=True)

_channels = (
         '1p1p',
         '1p3p',
         '3p1p',
         '3p3p',
         )

_years = (
         '15',
         '16',
         '17',
         '18',
     )

_categories = (
     'tth',
     'tth_0',
     'tth_1',
     )

_regions = (
         'SR',
         )

sels = get_selections(
     channels=_channels,
     years=_years,
     categories=_categories,
     regions=_regions)

_variables = [
#     VARIABLES['ditau_dr'],
     VARIABLES['mmc_mlm_m']
]


# #### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, _variables,verbose=True)
processor.book()
processor.run(n_cores=cpu_count() - 1)

# plot making
import ROOT
stop_watch = ROOT.TStopwatch()
for var in _variables:
     for reg in _regions:
          for cat in _categories:
               make_data_mc_plot(processor, sels, var, categories=cat, regions=reg)

stop_watch.Stop()
stop_watch.Print()

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'



