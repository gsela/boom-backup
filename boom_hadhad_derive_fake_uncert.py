# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections, filter_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.extern.tabulate import tabulate
from boom.utils import hist_sum, yield_tuple, hist_max, rqcd_calc
from boom.plotting import derive_syst_unc

# import logging
# logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes(no_fake=True)

regions = (
    'nos', 
    'anti_tau', 
    )

categories = ('preselection','boost','vbf','vh')

### define your selection objects 
sels = get_selections(
    channels=('1p1p', '1p3p', '3p1p', '3p3p'),
    years=('15', '16', '17', '18'),
    categories=categories,
    regions=regions)


# define your list of variables
variable = VARIABLES['mmc_mlm_m']

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variable)
processor.book()
processor.run(n_cores=cpu_count() - 1)

rqcd_transfers = [
     ('anti_tau', 'nos'),
]

for category in categories:

    lines = []
    data_minus_mc = {}

    for region in regions:
        print region
        sels_r = filter_selections(sels, regions=region, categories=category)
        hdata = processor.get_hist_physics_process('Data', sels_r, variable)
        data = hdata.Integral()

        h_mcs = []
        mc = 0
        for p in processor.mc_backgrounds:
            h = processor.get_hist_physics_process(p.name, sels_r, variable)
            mc += h.Integral()
            h_mcs.append(h)

        h_mc = hist_sum(h_mcs)
        h_data_minus_mc = hdata.Clone()
        h_data_minus_mc.Add(h_mc, -1)
        data_minus_mc[region] = h_data_minus_mc

    for (sr, cr) in rqcd_transfers:
        derive_syst_unc( data_minus_mc[sr], data_minus_mc[cr] , variable, category)

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
