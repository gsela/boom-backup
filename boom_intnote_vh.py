import os
import ROOT
from multiprocessing import cpu_count
#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# Switch on debug output
# import logging
# logging.root.setLevel( logging.DEBUG )

do_top_cr = True

if do_top_cr:
    print 50 * '-'
    print ''
    print 'WILL MAKE PLOTS FOR TOP CR'
    print 'Change value of do_top_cr in the script to run over the SRs'
    print ''
    print 50 * '-'

compute_significance = True
split_signal = True
force_unblind = False
if do_top_cr:
    force_unblind = True

# retrieve physics processes
physicsProcesses = get_processes()

_categories = (
    'vh',
)

_years = (
    '15',
    '16',
    '17',
    '18',
)

if do_top_cr:
    _channels = (
        'emu', 'mue', 
        'e1p', 'e3p', 'mu1p', 'mu3p',
    )
    _region = 'top'
else:
    _channels =  (
        'emu', 'mue', 
        'e1p', 'e3p', 'mu1p', 'mu3p',
        '1p1p', '1p3p', '3p1p', '3p3p'
    )
    _region = 'SR'

### define your selection objects 
sels = get_selections(
    channels=_channels,
    categories=_categories,
    years=_years,
    regions=_region)

# define your list of variables
variables = [
    VARIABLES['vh_tagger_pr'],
    VARIABLES['mjj_vh'],
    VARIABLES['dijetditau_dr'],
    VARIABLES['higgs_pt'],
    VARIABLES['dijet_pt'],
    VARIABLES['higgs_pt_over_dijet_pt'],
    VARIABLES['met'],
    VARIABLES['jets_dr'],
    VARIABLES['jets_deta_wide'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

# plot making
for var in variables:
    if not do_top_cr:
        # all channels
        make_data_mc_plot(
            processor, sels, var, 
            categories='vh', 
            compute_significance=compute_significance, 
            force_unblind=force_unblind,
            split_signal=split_signal)

    # leplep
    make_data_mc_plot(
        processor, sels, var, 
        channels=('emu', 'mue'), 
        categories='vh', 
        compute_significance=compute_significance, 
        force_unblind=force_unblind,
        split_signal=split_signal)

    # lephad 
    make_data_mc_plot(
        processor, sels, var, 
        channels=('e1p', 'e3p', 'mu1p', 'mu3p'), 
        categories='vh', 
        compute_significance=compute_significance, 
        force_unblind=force_unblind,
        split_signal=split_signal)

    if not do_top_cr:
        # hadhad
        make_data_mc_plot(
            processor, sels, var, 
            channels=('1p1p', '1p3p', '3p1p', '3p3p'), 
            categories='vh', 
            compute_significance=compute_significance, 
            force_unblind=force_unblind,
            split_signal=split_signal)




print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
