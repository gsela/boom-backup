import uuid
import os

def yield_hist(chan, names, syst='nominal'):
    h = ROOT.TH1F(
        uuid.uuid4().hex + chan.name, "", 
        len(names), 0, len(names))

    for _sampl in chan.samples:
        if not _sampl.name in names:
            continue
        _syst = syst
        if not syst in _sampl.hist_names:
            _syst = 'nominal'
        _hist = _sampl.hist(_syst)
        _ibin = names.index(_sampl.name)
        h.SetBinContent(_ibin + 1, _hist.Integral())
#         h.SetBinContent(i + 1, _hist.Integral(0, _hist.GetNbinsX() + 1))
        h.GetXaxis().SetBinLabel(_ibin + 1, _sampl.name)
    h.SetFillColor(ROOT.kBlue)
    return h


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('file_0')
    parser.add_argument('file_1')
    parser.add_argument('--no-signal', default=False, action='store_true')
    parser.add_argument('--name', default='nominal')
    args = parser.parse_args()


    import ROOT
    from boom.wsi_tools import workspace_input
    rfile_0 = ROOT.TFile(args.file_0)
    rfile_1 = ROOT.TFile(args.file_1)

    _wsi_0 = workspace_input(rfile_0)
    _wsi_1 = workspace_input(rfile_1)

    channel_names_0 = _wsi_0.channel_names
    channel_names_1 = _wsi_1.channel_names

    # build list of common channels
    channel_names = []
    for _name in channel_names_0 + channel_names_1:
        if not _name in channel_names_0:
            print '{} not in {}'.format(_name, args.file_0)
            continue
        if not _name in channel_names_1:
            print '{} not in {}'.format(_name, args.file_1)
            continue
        if not _name in channel_names:
            channel_names.append(_name)


    for _name in channel_names:
        print 'Scanning channel {}'.format(_name)
        _chan_0 = _wsi_0.channel(_name)
        _chan_1 = _wsi_1.channel(_name)

        _samples_0 = _chan_0.sample_names
        _samples_1 = _chan_1.sample_names
        _sample_names = []
        for _name in _samples_0 + _samples_1:
            if not _name in _samples_0:
                print '{} not in {}'.format(_name, _chan_0.name)
                continue
            if not _name in _samples_1:
                print '{} not in {}'.format(_name, _chan_1.name)
                continue
            if not _name in _sample_names:
                _sample_names.append(_name)
            
        for _name in _sample_names:
            _samp_0 = _chan_0.sample(_name)
            _samp_1 = _chan_1.sample(_name)
            
            if len(_samp_0.hist_names) != len(_samp_1.hist_names):
                print '\t Different number of histograms for sample {}'.format(_name)

            else:
                for _hist0, _hist1 in zip(sorted(_samp_0.hist_names), sorted(_samp_1.hist_names)):
                    if _hist0 != _hist1:
                        print '\t Difference between file0 and file 1: {}, {}'.format(_hist0, _hist1)
        


#         print _sample_names
#         print _chan_0.samples
#         print _chan_1.samples
        c = ROOT.TCanvas()
        c.SetTopMargin(0.3)
        c.SetBottomMargin(0.3)
        c.Divide(1, 2, 0)
        c.cd(1)

        h_0 = yield_hist(_chan_0, _sample_names, syst=args.name)
        h_0.SetBarWidth(0.4)
        h_0.SetBarOffset(0.1)
        h_0.GetYaxis().SetTitle('Yields')
        h_1 = yield_hist(_chan_1, _sample_names, syst=args.name)
        h_1.SetBarWidth(0.4)
        h_1.SetBarOffset(0.5)
        h_1.SetFillColor(ROOT.kRed)

        maxi = max(h_0.GetBinContent(h_0.GetMaximumBin()), h_1.GetBinContent(h_1.GetMaximumBin()))
        h_0.GetYaxis().SetRangeUser(0, 1.05 * maxi)
        h_0.Draw('B')
        h_1.Draw('SAMEB')

        label = ROOT.TText(0.7, 0.85, _chan_0.name)
        label.SetNDC(True)
        label.SetTextSize(0.05)
        label.Draw('same')

        label_syst = ROOT.TText(0.7, 0.8, args.name)
        label_syst.SetNDC(True)
        label_syst.SetTextSize(0.05)
        label_syst.Draw('same')

        c.cd(2)
        h_r = h_0.Clone()
        h_r.SetFillColor(0)
        h_r.GetYaxis().SetRangeUser(-0.1, 2)
        h_r.Divide(h_1)
        h_r.GetYaxis().SetTitle('Ratio')
        h_r.Draw('HIST')

        label_0 = ROOT.TText(
            c.GetLeftMargin() + 0.015, 0.93, 
            'file 0 = {}'.format(os.path.basename(args.file_0)))
        label_0.SetNDC(True)
        label_0.SetTextColor(ROOT.kBlue)
        label_0.SetTextSize(0.05)
        label_0.Draw('same')

        label_1 = ROOT.TText(
            c.GetLeftMargin() + 0.015, 0.880, 
            'file 1 = {}'.format(os.path.basename(args.file_1)))
        label_1.SetNDC(True)
        label_1.SetTextColor(ROOT.kRed)
        label_1.SetTextSize(0.05)
        label_1.Draw('same')

        c.Update()

        c.RedrawAxis()
        c.SaveAs('plots/wsi_compare_{}_{}.pdf'.format(_chan_0.name, args.name))
    ROOT.gROOT.GetListOfFiles().Remove(rfile_0)
    ROOT.gROOT.GetListOfFiles().Remove(rfile_1)
