# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections, filter_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.extern.tabulate import tabulate
from boom.utils import hist_sum, yield_tuple, hist_max, rqcd_calc
from boom.plotting import derive_syst_unc

# import logging
# logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes(stxs='stxs0', squash=True)

categories = ('preselection','boost','vbf','vh')

do_1prong = False

if do_1prong :
    _channels = ('e1p','mu1p')
else :
    _channels = ('e3p','mu3p')

### define your selection objects 
sels = get_selections(
    channels=_channels,
    years=('15', '16', '17', '18'),
    categories=categories,
    regions='same_sign_SR')


# define your list of variables
variable = VARIABLES['mmc_mlm_m_closure']

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variable)
processor.book()
processor.run(n_cores=cpu_count() - 1)

for category in categories:

    sels_r = filter_selections(sels, categories=category)
    h_data = processor.get_hist_physics_process('Data', sels_r, variable)

    bkgs = []
    for name in processor.merged_physics_process_names[::-1]:
        p = processor.get_physics_process(name)[0]
        if (p.isSignal and 'HWW' not in p.name):
            continue
        if p.name == 'Data':
            continue
        h = processor.get_hist_physics_process(name, sels_r, variable)
        bkgs.append(h)
    hBkg = hist_sum(bkgs)
    h_data.Divide(hBkg)

    prong = '1' if do_1prong else '3'
    cat = 'Presel' if 'preselection' in category else 'VBF' if 'vbf' in category else 'VH' if 'vh' in category else 'Boosted'
    filename = 'lh_fake_closure_uncert_' + cat + '_' + prong + 'prong'
    outfile = ROOT.TFile(filename+'.root','recreate')
    h_data.SetName(filename)
    h_data.Write()
    outfile.Close()
         
print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
