import os
import ROOT
from multiprocessing import cpu_count
import math
#local imports
from boom.plotting import make_data_mc_plot, make_vbf_roc_plot
from boom.utils import hist_sum, significance_counting
from boom.core import boom_processor, close_stores
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.selection_utils import get_selections, print_selection, filter_selections
from boom.cuts.vbf import Cut_VBF_0_hh, Cut_VBF_1_hh 
from boom.cuts.vbf import Cut_VBF_0_lh, Cut_VBF_1_lh 
from boom.cuts.vbf import Cut_VBF_0_ll, Cut_VBF_1_ll 
from boom.mva import vbf_tagger_expr
from boom.extern.tabulate import tabulate

# retrieve physics processes
physicsProcesses = get_processes()


### define your selection objects 
cut_value = 0.1

_channels = (
        'emu', 'mue', 
        'e1p', 'e3p', 'mu1p', 'mu3p',
        '1p1p', '1p3p', '3p1p', '3p3p')

sels_0 = get_selections(
    channels=_channels,
    categories='vbf_0',
    regions='SR')

sels_1 = get_selections(
    channels=_channels,
    categories='vbf_1',
    regions='SR')

var = VARIABLES['mmc_mlm_m']

Cut_VBF_0_hh.cut.cut = '{} < {}'.format(vbf_tagger_expr, cut_value)
Cut_VBF_1_hh.cut.cut = '{} > {}'.format(vbf_tagger_expr, cut_value)

Cut_VBF_0_lh.cut.cut = '{} < {}'.format(vbf_tagger_expr, cut_value)
Cut_VBF_1_lh.cut.cut = '{} > {}'.format(vbf_tagger_expr, cut_value)

Cut_VBF_0_ll.cut.cut = '{} < {}'.format(vbf_tagger_expr, cut_value)
Cut_VBF_1_ll.cut.cut = '{} > {}'.format(vbf_tagger_expr, cut_value)

processor = boom_processor(physicsProcesses, sels_0 + sels_1, var)
processor.book()
processor.run(n_cores=cpu_count() - 1)

channel_groups = {
    'leplep': ('emu', 'mue'),
    'lephad': ('e1p', 'e3p', 'mu1p', 'mu3p'),
    'hadhad': ('1p1p', '1p3p', '3p1p', '3p3p'),
}

lines = []
for gr in sorted(channel_groups.keys()):
    print gr
    _channels = channel_groups[gr]
    _sels_0 = filter_selections(sels_0, channels=_channels)
    _sels_1 = filter_selections(sels_1, channels=_channels)

    signal_0 =  processor.get_hist_physics_process('VBFH', _sels_0, var)
    signal_1 =  processor.get_hist_physics_process('VBFH', _sels_1, var)

    bkgs_0 = []
    bkgs_1 = []
    for _name in processor._physics_process_names:
        _process = processor.get_physics_process(_name)
        if _process[0].isSignal and _name == 'VBFH':
            continue
        if _process[0].isData and _name != 'Fake':
            continue
        bkgs_0.append(processor.get_hist_physics_process(_name, _sels_0, var))
        bkgs_1.append(processor.get_hist_physics_process(_name, _sels_1, var))
    
    bkg_0 = hist_sum(bkgs_0)
    bkg_1 = hist_sum(bkgs_1)

    signi_0 = significance_counting(signal_0, bkg_0)
    signi_1 = significance_counting(signal_1, bkg_1)
    signi_comb = math.sqrt(signi_0 * signi_0 + signi_1 * signi_1)
    line = [gr, cut_value, signi_0, signi_1, signi_comb]
    lines.append(line)

print tabulate(lines, headers=['channel', 'cut value', 'low cat', 'high cat', 'comb'])

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
