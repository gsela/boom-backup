# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_zllvr_proxy_comparison_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.cuts.kinematics import CutInvariantMass_zll_vr

# retrieve physics processes
physicsProcesses = get_processes()
physicsProcesses = filter(lambda p: 'Ztt' in p.name or 'Zll' in p.name, physicsProcesses)

# choose which channel to compare with corresponding Zll VR proxy (default: hadhad)
do_ll = False
do_lh = False

# choose which region to compare with corresponding Zll VR proxy (default : VBF)
do_boost = True

#years = ('15', '16', '17', '18')
years = ('17',)

if do_ll:
   chs = ('emu', 'mue')
   zll_region = ('Ztt_cr_ll')
elif do_lh :
   chs = ('e1p', 'e3p', 'mu1p', 'mu3p')
   zll_region = ('Ztt_cr_lh')
else :
   chs = ('1p1p','1p3p','3p1p','3p3p') 
   zll_region = ('Ztt_cr_hh')

if do_boost: 
     sels_sr =  get_selections(
        channels=chs, 
        years=years, 
        regions='SR', 
        categories=('boost'))
     sels_zllvr = get_selections(
        years=years, 
        regions=zll_region, 
        categories=('boost'))
else : 
     if (do_lh or do_ll) :  
         sels_sr = get_selections(
            channels=chs, 
            years=years, 
            regions='SR', 
            categories=('vbf_tight','vbf_loose'))
         sels_zllvr = get_selections(
            years=years, 
            regions=zll_region, 
            categories=('vbf_tight','vbf_loose'))
     else : 
         sels_sr = get_selections(
            channels=chs, 
            years=years, 
            regions='SR', 
            categories=('vbf_tight','vbf_loose','vbf_lowdr'))
         sels_zllvr = get_selections(
            years=years, 
            regions=zll_region, 
            categories=('vbf_tight','vbf_loose','vbf_lowdr'))

sels = sels_sr + sels_zllvr

#CutInvariantMass_zll_vr.cut.cut = 'ditau_p4.M() > 80 && tau_0_p4.Pt() > 70 && tau_1_p4.Pt() > 25'

### define your list of variables
variables = [
#     VARIABLES['pt_total'],
#     VARIABLES['mjj'],
#     VARIABLES['jets_deta'],
#     VARIABLES['jets_eta_prod'],
#     VARIABLES['var_ptjj'],
#     VARIABLES['vbf_tagger'],
#      VARIABLES['met'],
#    VARIABLES['met_centrality'],
#     VARIABLES['ditau_dr'],
#     VARIABLES['higgs_pt'],
#     VARIABLES['higgs_pt_res']
#      VARIABLES['mjj'],
#      VARIABLES['jets_deta'],
#      VARIABLES['tau_0_pt'],
#      VARIABLES['tau_1_pt'],
#   VARIABLES['ditau_pt_truth'],
#   VARIABLES['pt_total'],
#   VARIABLES['mjj'],
#   VARIABLES['jets_deta'],
#   VARIABLES['jets_eta_prod'],
#   VARIABLES['var_ptjj'],
#   VARIABLES['vbf_tagger'],
   VARIABLES['tau_0_pt'],
   VARIABLES['tau_1_pt'],
   VARIABLES['tau_0_pt_corr'],
   VARIABLES['tau_1_pt_corr'],
#   VARIABLES['met'],
#   VARIABLES['ditau_dr'],
#   VARIABLES['ditau_dr_corr'],
   #   VARIABLES['met_centrality'],
   VARIABLES['taus_pt_ratio'],
   VARIABLES['taus_pt_ratio_corr'],
   VARIABLES['higgs_pt'],
   VARIABLES['ditau_pt_truth'],
   VARIABLES['met'],
   VARIABLES['ditau_pt_corr'],
   VARIABLES['met_corr'],
   VARIABLES['x0'],
   VARIABLES['x0_corr'],
   VARIABLES['x1'],
   VARIABLES['x1_corr'],
   VARIABLES['ditau_deta'],
   VARIABLES['ditau_deta_corr'],
   VARIABLES['tau_0_eta'],
   VARIABLES['eta_corr_0'],
   VARIABLES['tau_1_eta'],
   VARIABLES['eta_corr_1'],
   VARIABLES['colinear_mass'],
   VARIABLES['visible_mass'],
   VARIABLES['visible_mass_corr'],
]

#### processor declaration, booking and runninge
processor = boom_processor(physicsProcesses, sels, variables, verbose=True)
processor.book()
processor.run(n_cores=cpu_count() - 1)

title = 'LepLep' if do_ll else 'LepHad' if do_lh else 'HadHad'
title += '_Boost SRs' if do_boost else '_VBF SRs'
#title += '_2Dcut'

### plot making
for var in variables:
    make_zllvr_proxy_comparison_plot(processor, sels_sr, sels_zllvr, var, title=title)


print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
