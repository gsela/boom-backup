import ROOT
from boom.database import get_processes
from boom.extern.tabulate import tabulate

processes = get_processes()

_lines = []
for p in processes:
    if p.isData:
        continue
    _d = p.datasets[0]
    _dsids = []
    for _f in _d.fileNames:
        _dsid = int(_f.split('/')[-2].replace('*', ''))
        _dsids.append(_dsid)
    for _dsid in _dsids:
        _lines += [[
            p.name, 
            _dsid,
            'mc16a',
            ROOT.crossSectionDict.m_Map_hh_a[_dsid],
            ROOT.crossSectionDict.m_Map_lh_a[_dsid],
            ROOT.crossSectionDict.m_Map_ll_a[_dsid],
            ROOT.crossSectionDict.m_Map_vr_a[_dsid],
            ]]
        
        _lines += [[
            p.name, 
            _dsid,
            'mc16d',
            ROOT.crossSectionDict.m_Map_hh_d[_dsid],
            ROOT.crossSectionDict.m_Map_lh_d[_dsid],
            ROOT.crossSectionDict.m_Map_ll_d[_dsid],
            ROOT.crossSectionDict.m_Map_vr_d[_dsid],
            ]]

        _lines += [[
            p.name, 
            _dsid,
            'mc16e',
            ROOT.crossSectionDict.m_Map_hh_e[_dsid],
            ROOT.crossSectionDict.m_Map_lh_e[_dsid],
            ROOT.crossSectionDict.m_Map_ll_e[_dsid],
            ROOT.crossSectionDict.m_Map_vr_e[_dsid],
            ]]
        
    


print tabulate(_lines, headers=['Process', 'DSID', 'Campaign', 'hadhad', 'lephad', 'leplep', 'zll_vr'], floatfmt=".15E", tablefmt='pipe')
