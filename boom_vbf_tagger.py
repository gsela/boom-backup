import os
import ROOT
from multiprocessing import cpu_count
#local imports
from boom.plotting import make_data_mc_plot, make_vbf_roc_plot


do_roc = True
years = ('15', '16', '17', '18')

# retrieve physics processes
from boom.database import get_processes
physicsProcesses = get_processes()


### define your selection objects 
from boom.selection_utils import get_selections
sels = get_selections(
    channels=(
        'emu', 'mue', 
        'e1p', 'e3p', 'mu1p', 'mu3p',
        '1p1p', '1p3p', '3p1p', '3p3p'),
    categories=('vbf_loose', 'vbf_tight', 'vbf_lowdr'),
    years=years,
    regions='SR')

from boom.variables import VARIABLES
if do_roc:
    var = VARIABLES["vbf_tagger_fine_binning"]
else:
    var = VARIABLES["vbf_tagger"]


#### processor declaration, booking and running
from boom.core import boom_processor, close_stores
processor = boom_processor(physicsProcesses, sels, var)
processor.book()
processor.run(n_cores=cpu_count() - 1)

if do_roc:
    make_vbf_roc_plot(processor, sels, var)
    make_vbf_roc_plot(processor, sels, var, channels=('emu', 'mue'))
    make_vbf_roc_plot(processor, sels, var, channels=('e1p', 'e3p', 'mu1p', 'mu3p'))
    make_vbf_roc_plot(processor, sels, var, channels=('1p1p', '1p3p', '3p1p', '3p3p'))
else:
    # all channels
    make_data_mc_plot(processor, sels, var, split_signal=True)
    # leplep
    make_data_mc_plot(processor, sels, var, channels=('emu', 'mue'), split_signal=True)
    # lephad
    make_data_mc_plot(processor, sels, var, channels=('e1p', 'e3p', 'mu1p', 'mu3p'), split_signal=True)
    # hadhad
    make_data_mc_plot(processor, sels, var, channels=('1p1p', '1p3p', '3p1p', '3p3p'), split_signal=True)


print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
