# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import compare_selections_plot
from boom.selection_utils import get_selections, filter_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.extern.tabulate import tabulate

# retrieve physics processes
physicsProcesses = get_processes(stxs='stxs1')
physicsProcesses = filter(lambda p: 'VBFH' in p.name, physicsProcesses)

### define your selection objects 
sels = get_selections(
    channels=(
        'emu', 'mue', 
        'e1p', 'e3p', 'mu1p', 'mu3p', 
        '1p1p', '1p3p', '3p1p', '3p3p'),
    regions='SR',
    categories=('vbf_0', 'vbf_1'))

sels_vbf_1 = filter_selections(sels, categories='vbf_1')
sels_vbf_0 = filter_selections(sels, categories='vbf_0')

### define your list of variables
variables = [
#     VARIABLES['met'],
#     VARIABLES['met_centrality'],
    VARIABLES['mmc_mlm_m'],
    VARIABLES['norm'],
#     VARIABLES['ditau_dr'],
#     VARIABLES['higgs_pt'],
#     VARIABLES['higgs_pt_res']
#     VARIABLES['mjj'],
#     VARIABLES['jets_deta'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

lines = []
for _process in physicsProcesses:
    h_tot = processor.get_hist_physics_process(
        _process.name, sels, VARIABLES['norm'])
    h_vbf_1 = processor.get_hist_physics_process(
        _process.name, sels_vbf_1, VARIABLES['norm'])

    eff = ROOT.TEfficiency(h_vbf_1, h_tot)

    line = [
        _process.name,
        '{0:1.2f} +/- {1:1.2f}'.format(h_tot.GetBinContent(1), h_tot.GetBinError(1)),
        '{0:1.2f} +/- {1:1.2f}'.format(h_vbf_1.GetBinContent(1), h_vbf_1.GetBinError(1)),
        '{0:1.2f} +/- {1:1.2f}'.format(
            eff.GetEfficiency(1), 
            max(eff.GetEfficiencyErrorLow(1), 
                eff.GetEfficiencyErrorUp(1)))
        ]
    lines.append(line)
    compare_selections_plot(
        processor, 
        _process.name, 
        sels_vbf_0, 
        sels_vbf_1, 
        VARIABLES['mmc_mlm_m'],
        sel_names=('vbf_0', 'vbf_1'))
print
print tabulate(lines, headers=['process', 'vbf inclusive', 'vbf_1', 'efficiency'])
print




print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
