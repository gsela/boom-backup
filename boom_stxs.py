# python imports
import ROOT
import os
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.plotting import make_stxs_bin_comparison_plot


# Create the histogram stores
physicsProcesses = get_processes(signal_only=True, stxs_stage1=True)

physicsProcesses = filter(
    lambda p: 'ggH_1J' in p.name or 'ggH_ge2J' in p.name, 
    physicsProcesses)

_categories = _categories = (
    'boost_0_1J', 'boost_1_1J', 'boost_2_1J', 'boost_3_1J',
    'boost_0_ge2J', 'boost_1_ge2J', 'boost_2_ge2J', 'boost_3_ge2J')

sels = get_selections(
    channels=(
        'emu', 'mue',
        'e1p', 'mu1p', 'e3p', 'mu3p',
        '1p1p', '1p3p', '3p1p', '3p3p'), 
    categories=_categories,
    regions='SR')


variable = VARIABLES['mmc_mlm_m']

processor = boom_processor(physicsProcesses, sels, variable)
processor.book()
processor.run(n_cores=cpu_count() - 1)

for _cat in _categories:
    make_stxs_bin_comparison_plot(processor, sels, variable, categories=_cat)


print 'closing stores...'
close_stores(physicsProcesses)
print 'done'

