
if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--channels', default=None, nargs='*')
    parser.add_argument('--categories', default=None, nargs='*')
    parser.add_argument('--years', default=None, nargs='*')
    parser.add_argument('--regions', default=None, nargs='*')
    parser.add_argument('--triggers', default=None, nargs='*')
    args = parser.parse_args()
    

    from boom.selection_utils import get_selections
    from boom.selection_utils import print_selection


    sels = get_selections(
        channels=args.channels,
        categories=args.categories,
        years=args.years,
        regions=args.regions,
        triggers=args.triggers)

    for sel in sels:
        print_selection(sel)
