
WhoSignal=1 #mark where the signal is. 1 for mutau, 2 for eltau
UseRatio=1 #1 means use eff_ratio, 0 means correcting each channel independently
SaveHist=1

ExtraCuts=0

#for creating trees:
CreateSmallTree=0 #Use the eventLoop code that creates a skimmed small tree. Only works with directly stating variables to save, due to AddFriend method.
SmallTreeFolder='Data' #Where to save small trees. make sure the folder exists.
UseFilteredTree=0 # USe the trees created in cmake


#outdated and not needed:
isMC=0
truthMatch=0
anti_tau_ff=0
isAC=0 #for acceptance challenge