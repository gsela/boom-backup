def generate_weight(physics_processes, campaign='mc16a', ntuple_extension='_hh'):
    """
    Helper function to generate json file to store cross-section and sum-of-weights
    for each DSID
    Arguments
    ---------
       physics_processes: list(happy.CombinedDataset)
       campaign: str
       ntuple_extension: str
    Returns
    -------
       dsid_weight; list([int, float, float])
    """
    from boom.database import sum_of_weight_bin
    dsid_weight = []
    for process in physics_processes:
        if process.isData:
            continue
        print 'BOOM:  \t generation for process {}'.format(process.name)
        sum_of_weight_bin(process)
        for dataset in process.datasets:
            if not campaign in dataset.name:
                continue
            if not ntuple_extension in dataset.name:
                continue

            dsid_weight.append([
                dataset.dsid, 
                dataset.effectiveCrossSection, 
                float(dataset.sumOfWeights)])
    
    return dsid_weight


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--ntuples-block', default='nom')
    args = parser.parse_args()

    if 'nom' in args.ntuples_block:
        add_ztt_madgraph=True 
        add_ztt_powheg=True
    else:
        add_ztt_madgraph=False
        add_ztt_powheg=False

    # generate all the processes with squashing disabled
    from boom.database import get_processes
    physicsProcesses = get_processes(
        add_ztt_madgraph=add_ztt_madgraph,
        add_ztt_powheg=add_ztt_powheg,
        squash=False, 
        ntuples_block=args.ntuples_block, 
        xsec_weights_loading=False)

    # mc campaigns
    campaigns = [
        'mc16a', 
        'mc16d', 
        'mc16e'
        ]

    # ntuple flavors
    streams = [
        'hh', 
        'lh', 
        'll', 
        'zll_vr'
        ]

    # build the dictionary to be dumped to the json file
    # for each campaign, stream store a list of [DSID, xsec, sum-of-weights]
    # files should be generated for nom and each sys_XX block as sum-of-weights can
    # vary (mainly because of failing jobs on the grid)
    xsec_weights_dict = {}
    for _camp in campaigns:
        xsec_weights_dict[_camp] = {}
        for _stream in streams:
            print 'BOOM:  generating list([dsid, xsec, s-o-w]) for {}, {}'.format(
                _camp, _stream)
            dsid_xsec_sumofweights = generate_weight(
                physicsProcesses, 
                campaign=_camp, 
                ntuple_extension='_' + _stream) 
            xsec_weights_dict[_camp][_stream] = dsid_xsec_sumofweights

    import json
    _filename = 'xsec_sumofweights_{}.json'.format(args.ntuples_block) 
    with open(_filename, 'w') as fout:
        json.dump(xsec_weights_dict, fout, indent=2)
        print 'file written'
