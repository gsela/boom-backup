# python imports
import uuid
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database  import get_processes
from boom.utils import hist_sum, lumi_calc

def _data_over_bkg(processor, _sels, variable, verbose=False):
     h_data = processor.get_hist_physics_process('Data', _sels, variable)
     bkgs = []
     for name in processor.merged_physics_process_names[::-1]:
          p = processor.get_physics_process(name)[0]
          if p.isSignal:
               continue
          if p.name == 'Data':
               continue
          h = processor.get_hist_physics_process(name, _sels, variable)
          bkgs.append(h)
     h_bkg = hist_sum(bkgs)
     if verbose:
          print 'Data = {}'.format(h_data.Integral())
          print 'Bkgd = {}'.format(h_bkg.Integral())

     h_ratio = h_data.Clone()
     h_ratio.Divide(h_bkg)
     return h_ratio

def plot_ztt_nf(processor, channels, years, categories, regions, y_range=(0.8, 1.2), verbose=False, suffix='new'):
     _channels = channels
     _years = years
     _categories = categories
     _regions = regions
     _low, _high = y_range[0], y_range[1]
     _suffix = suffix
     # fill graphs
     graphs = {}
     vals = []
     for i_reg, _reg in enumerate(_regions):
          graphs[_reg] = ROOT.TGraphErrors(len(_categories))
          for i_cat, _cat in enumerate(_categories):
               _sels = get_selections(
                    channels=_channels,
                    categories=_cat,
                    regions=_reg,
                    years=_years)
               if len(_sels) == 0:
#                     graphs[_reg].SetPoint(i_cat, i_cat, 0)
                    graphs[_reg].SetPoint(i_cat, i_cat, 0)
                    graphs[_reg].SetPointError(i_cat, 0, 0)
               else:
                    h_ratio = _data_over_bkg(processor, _sels, variable, verbose=verbose)
                    vals.append(h_ratio.GetBinContent(1))
#                     graphs[_reg].SetPoint(i_cat, i_cat + 0.3 + 0.2 * i_reg, h_ratio.GetBinContent(1))
#                     graphs[_reg].SetPointError(i_cat, 0, h_ratio.GetBinError(1))
#                     graphs[_reg].SetPoint(i_cat, 1, i_cat)
                    graphs[_reg].SetPoint(i_cat, h_ratio.GetBinContent(1), i_cat + 0.3 + 0.2 * i_reg)
                    graphs[_reg].SetPointError(i_cat, h_ratio.GetBinError(1), 0)
               


     # plot
     canvas = ROOT.TCanvas(uuid.uuid4().hex, '', 600, 800)
     canvas.SetLeftMargin(0.3)
     h_template = ROOT.TH1F('h_temp', '', len(_categories), 0, len(_categories))
     for i_cat, _cat in enumerate(_categories):
          h_template.GetXaxis().SetBinLabel(i_cat + 1, _cat)
#           h_template.GetXaxis().SetTitle('Category')
          h_template.GetYaxis().SetTitle('Data / MC')
          h_template.GetYaxis().SetRangeUser(_low, _high)
     h_template.Draw('hbar')
     _colors = (ROOT.kBlack, ROOT.kBlue, ROOT.kRed)
     leg = ROOT.TLegend(0.32, 0.18, 0.54, 0.4)
     for _reg, _col in zip(_regions, _colors):
          graphs[_reg].SetLineColor(_col)
          graphs[_reg].SetMarkerColor(_col)
          graphs[_reg].Draw('samePE')
          leg.AddEntry(graphs[_reg], _reg.split('_')[-1], 'lp')
     leg.SetFillColor(0)
     leg.SetFillStyle(0)
     leg.SetBorderSize(0)
     leg.Draw()
     label = ROOT.TLatex(
          canvas.GetLeftMargin() + 0.02, 1 - canvas.GetTopMargin() + 0.02, 
          'Z#rightarrowll CR, L = {0:1.0f} fb^{{-1}}'.format(lumi_calc(sels) / 1000.))
     label.SetTextSize(0.8 * label.GetTextSize())
     label.SetNDC()
     label.Draw()
     l_1 = ROOT.TLine(1, 0, 1, len(_categories))
     l_1.SetLineStyle(ROOT.kDashed)
     l_1.Draw()
     canvas.RedrawAxis()
     canvas.SaveAs('plots/ztt_prefit_nf_{}.pdf'.format(_suffix))


### config
_channels = ('ee', 'mumu')
_regions = ('Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh')
_categories = (
     'preselection',
     'boost_0_1J', 'boost_1_1J', 'boost_2_1J', 'boost_3_1J',
     'boost_0_ge2J', 'boost_1_ge2J', 'boost_2_ge2J', 'boost_3_ge2J',
     'vh_0','vh_1',
     'vbf_0', 'vbf_1',)
_years = (
     '15',
     '16',
     '17',
     '18',
)

##
sels = get_selections(
     channels=_channels,
     categories=_categories,
     years=_years,
     regions=_regions)

# retrieve physics processes
physics_processes = get_processes(no_fake=True)

# define your list of variables
variable = VARIABLES['norm']

#### processor declaration, booking and running
processor = boom_processor(physics_processes, sels, variable)
processor.book()
processor.run(n_cores=cpu_count() - 1)


_categories_new = (
     'preselection',
     'boost_0_1J', 'boost_1_1J', 'boost_2_1J', 'boost_3_1J',
     'boost_0_ge2J', 'boost_1_ge2J', 'boost_2_ge2J', 'boost_3_ge2J',
     'vh_0', 'vh_1',
     'vbf_0', 'vbf_1',)

plot_ztt_nf(
     processor, 
     _channels, 
     _years, 
     _categories_new, 
     _regions, 
     y_range=(0.7, 1.3), 
     verbose=False, 
     suffix='new')


          
print 'closing stores...'
close_stores(physics_processes)
print 'done'



