# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_region_comparison_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# retrieve physics processes
physicsProcesses = get_processes(no_fake=True)
physicsProcesses = filter(lambda p: not p.isSignal, physicsProcesses) 

_categories = (
   'preselection',
   'boost',
   'vbf',
   'vh',
   )

### define your selection objects 
sels = get_selections(
    channels=('e1p', 'e3p', 'mu1p', 'mu3p'), 
    years=('15','16','17','18'),
    regions=('anti_tau','qcd_lh_anti_tau','W_lh_anti_tau'),
    categories=_categories)

### define your list of variables
variables = [
    VARIABLES['tau_0_rnn_score_coarse'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

### plot making
for cat in _categories:
    for var in variables:
        make_region_comparison_plot(processor, sels, var, process_name='Data', category=cat)

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
