import os
import argparse
import uuid

import ROOT
ROOT.gROOT.SetStyle('ATLAS')
ROOT.gROOT.SetBatch(True)


_folders = [
    'hadronic',
    'muonic',
    'electronic'
]

_TITLE = {
    'hadronic': '#tau #rightarrow hadrons + #nu_{#tau}',
    'muonic': '#tau #rightarrow #mu + #nu_{#mu} + #nu_{#tau}',
    'electronic' : '#tau #rightarrow e + #nu_{e} + #nu_{#tau}',
}

def _merge_hist(rfile, tau_type, var='NOMINAL'):
    _n_hists = len(rfile.Get(os.path.join(tau_type, var)).GetListOfKeys())
    _merged_hist = rfile.Get(os.path.join(tau_type, var, 'bin0')).Clone()
    _merged_hist.Scale(_merged_hist.GetEntries())
    # normalise with number of entries (to respect importance of the spectrum)
    _total_entries = _merged_hist.GetEntries()
    for i in range(1, _n_hists):
        _h = rfile.Get(os.path.join(tau_type, var, 'bin{}'.format(i)))
        _h.Scale(_h.GetEntries())
        _merged_hist.Add(_h)
        _total_entries += _h.GetEntries()
    _merged_hist.Scale(1. / _total_entries)
#     print _merged_hist.Integral(0, _merged_hist.GetNbinsX() + 1)
    _merged_hist.SetName('merged_{}'.format(tau_type))
    _merged_hist.SetTitle(_TITLE[tau_type])
    return _merged_hist

def plot_summary(rfile, bin_number=None, logy=False):
    if bin_number == None:
        _hadronic   = _merge_hist(rfile, 'hadronic')
        _muonic     = _merge_hist(rfile, 'muonic')
        _electronic = _merge_hist(rfile, 'electronic')
    else:
        _hadronic   = rfile.Get('hadronic/NOMINAL/bin{}'.format(bin_number))
        _muonic     = rfile.Get('muonic/NOMINAL/bin{}'.format(bin_number))
        _electronic = rfile.Get('electronic/NOMINAL/bin{}'.format(bin_number))
        _info = _hadronic.GetTitle()
        _hadronic.SetTitle(_TITLE['hadronic'])
        _muonic.SetTitle(_TITLE['muonic'])
        _electronic.SetTitle(_TITLE['electronic'])

    max_val = max(
        _hadronic.GetBinContent(_hadronic.GetMaximumBin()),
        _muonic.GetBinContent(_muonic.GetMaximumBin()),
        _electronic.GetBinContent(_electronic.GetMaximumBin()))

    _hadronic.SetLineColor(ROOT.kRed)
    _muonic.SetLineColor(ROOT.kBlue)
    _electronic.SetLineColor(ROOT.kOrange)
    
    if logy:
        _template = ROOT.TH1F(uuid.uuid4().hex, '', 10, -0.2, 2)
    else:
        _template = ROOT.TH1F(uuid.uuid4().hex, '', 10, 0.03, 2)
        
    _template.GetXaxis().SetTitle(_hadronic.GetXaxis().GetTitle())
    _template.GetYaxis().SetTitle(_hadronic.GetYaxis().GetTitle())

    if logy:
        _template.GetYaxis().SetRangeUser(1e-9,  1.1)
    else:
        _template.GetYaxis().SetRangeUser(0, 0.05)

    c = ROOT.TCanvas()
    c.SetTopMargin(0.08)
    c.SetLogy(logy)
    _template.Draw('HIST')
    _hadronic.Draw('sameHISTE')
    _muonic.Draw('sameHISTE')
    _electronic.Draw('sameHISTE')
    leg = ROOT.TLegend(0.6, 0.75, 0.9, 0.9)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.AddEntry(_hadronic, _hadronic.GetTitle(), 'l')
    leg.AddEntry(_muonic, _muonic.GetTitle(), 'l')
    leg.AddEntry(_electronic, _electronic.GetTitle(), 'l')
    leg.Draw()
    
    if bin_number == None:
        _label = ROOT.TLatex(
            1 - c.GetRightMargin(),
            1 - c.GetTopMargin() / 2.,
            'Integrated over all p_{T} and |#eta| bins')
    else:
        c.cd()
#         print _info
        _info = _info.split('_')
        _label = ROOT.TLatex(
            1 - c.GetRightMargin(),
            1 - c.GetTopMargin() / 2.,
            '{} < p_{{T}} < {}, {} < |#eta| < {}'.format(_info[1], _info[2], _info[4], _info[5])
            )
    _label.SetTextAlign(32)
    _label.SetNDC()
    _label.Draw()

    c.RedrawAxis()
    c.Update()
    if logy:
        if bin_number == None:
            c.SaveAs('plots/embedding_correction_summary_log.pdf')
        else:
            c.SaveAs('plots/embedding_correction_bin{}_log.pdf'.format(bin_number))

    else:
        if bin_number == None:
            c.SaveAs('plots/embedding_correction_summary_zerosuppressed.pdf'.format(bin_number))
        else:
            c.SaveAs('plots/embedding_correction_bin{}_zerosuppressed.pdf'.format(bin_number))


def plot_systematic(rfile, tau_type='hadronic', systematic='TAUS_TRUEHADTAU_SME_TES_INSITUFIT', bin_number=None, logy=False):
    if bin_number == None:
        _ratio    = _merge_hist(rfile, tau_type, var='NOMINAL')
        _ratio_up = _merge_hist(rfile, tau_type, var=systematic + '__1up')
        _ratio_do = _merge_hist(rfile, tau_type, var=systematic + '__1down')
    else:
        _ratio    = rfile.Get('{}/{}/bin{}'.format(tau_type, 'NOMINAL', bin_number))
        _ratio_up = rfile.Get('{}/{}/bin{}'.format(tau_type, systematic + '__1up', bin_number))
        _ratio_do = rfile.Get('{}/{}/bin{}'.format(tau_type, systematic + '__1down', bin_number))

        _info = _ratio.GetTitle()
        _ratio.SetTitle('NOMINAL')
        _ratio_up.SetTitle('1up')
        _ratio_do.SetTitle('1down')

    max_val = max(
        _ratio.GetBinContent(_ratio.GetMaximumBin()),
        _ratio_up.GetBinContent(_ratio_up.GetMaximumBin()),
        _ratio_do.GetBinContent(_ratio_do.GetMaximumBin()))

    _ratio.SetLineColor(ROOT.kBlack)
    _ratio_up.SetLineColor(ROOT.kRed)
    _ratio_do.SetLineColor(ROOT.kBlue)
    
    _double_ratio_up = _ratio_up.Clone(uuid.uuid4().hex)
    _double_ratio_up.Divide(_ratio)

    _double_ratio_do = _ratio_do.Clone(uuid.uuid4().hex)
    _double_ratio_do.Divide(_ratio)

    if logy:
        _template_top = ROOT.TH1F(uuid.uuid4().hex, '', 10, -0.2, 2)
        _template_bot = ROOT.TH1F(uuid.uuid4().hex, '', 10, -0.2, 2)
    else:
        _template_top = ROOT.TH1F(uuid.uuid4().hex, '', 10, 0.03, 2)
        _template_bot = ROOT.TH1F(uuid.uuid4().hex, '', 10, 0.03, 2)
        
    _template_top.GetXaxis().SetTitle(_ratio.GetXaxis().GetTitle())
    _template_bot.GetXaxis().SetTitle(_ratio.GetXaxis().GetTitle())

    _template_top.GetYaxis().SetTitle()
    _template_bot.GetYaxis().SetTitle()
    _template_top.GetYaxis().SetTitle('Arbitrary Units')
    _template_bot.GetYaxis().SetTitle('Variation / Nominal')


    if logy:
        _template_top.GetYaxis().SetRangeUser(2e-9,  2)
        _template_bot.GetYaxis().SetRangeUser(0.57, 1.43)
    else:
        _template_top.GetYaxis().SetRangeUser(0, 0.05)
        _template_bot.GetYaxis().SetRangeUser(0.8, 1.2)

    c = ROOT.TCanvas(uuid.uuid4().hex, '', 800, 600)
    c.SetLeftMargin(0.1)
    c.SetRightMargin(0.02)
    c.SetTopMargin(0.08)
    c.SetBottomMargin(0.)
    c.Divide(1, 2)
    p_top = c.GetPad(1)
    p_top.SetPad(
#         c.GetLeftMargin(), #x_low
        0,
        0.4, #y_low
        1 - c.GetRightMargin(), #x_high
        1 - c.GetTopMargin())
    p_top.SetTopMargin(0)
    p_top.SetBottomMargin(0)

    p_bot = c.GetPad(2)
    p_bot.SetPad(
        0,
        #c.GetLeftMargin(), #x_low
        c.GetBottomMargin() + 0.05, #y_low
        1 - c.GetRightMargin(), #x_high
        0.4)
    p_bot.SetTopMargin(0)
    p_bot.SetBottomMargin(0.9)

    _template_top.GetYaxis().SetTitleOffset(0.8)
    _template_bot.GetYaxis().SetNdivisions(5)
    _template_bot.GetYaxis().SetLabelSize(2 * _template_bot.GetYaxis().GetLabelSize())
    _template_bot.GetYaxis().SetTitleSize(1.8 * _template_bot.GetYaxis().GetTitleSize())
    _template_bot.GetYaxis().SetTitleOffset(0.45)

    _template_bot.GetXaxis().SetLabelSize(2 * _template_bot.GetXaxis().GetLabelSize())
    _template_bot.GetXaxis().SetTitleSize(1.8 * _template_bot.GetXaxis().GetTitleSize())
    _template_bot.GetXaxis().SetTitleOffset(1.05)
    _template_bot.GetXaxis().SetTickSize(0.06)
    p_top.cd()
    p_top.SetLogy(logy)
    _template_top.Draw('HIST')
    _ratio.Draw('sameHISTE')
    _ratio_up.Draw('sameHISTE')
    _ratio_do.Draw('sameHISTE')
    leg = ROOT.TLegend(0.7, 0.75, 1., 0.95)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetHeader(systematic)
    leg.AddEntry(_ratio, _ratio.GetTitle(), 'l')
    leg.AddEntry(_ratio_up, _ratio_up.GetTitle(), 'l')
    leg.AddEntry(_ratio_do, _ratio_do.GetTitle(), 'l')
    leg.Draw()

    p_bot.cd()
    _template_bot.Draw('HIST')
    _double_ratio_up.Draw('SAMEHISTE')
    _double_ratio_do.Draw('SAMEHISTE')
    p_bot.RedrawAxis()
    p_top.RedrawAxis()

    c.cd()
    if bin_number == None:
        _label = ROOT.TLatex(
            1 - p_top.GetRightMargin() + c.GetRightMargin(),
            1 - c.GetTopMargin() / 2.,
            '{}, Integrated over all p_{T} and |#eta| bins'.format(_TITLE[tau_type]))
    else:
        c.cd()
        _info = _info.split('_')
        _label = ROOT.TLatex(
            1 - c.GetRightMargin(),
            1 - c.GetTopMargin() / 2.,
            '{}, {} < p_{{T}} < {}, {} < |#eta| < {}'.format(_TITLE[tau_type], _info[1], _info[2], _info[4], _info[5])
            )
    _label.SetTextAlign(32)
    _label.SetTextSize(_label.GetTextSize() * 0.7)
    _label.SetNDC()
    _label.Draw()
    c.Update()

    p_top.SetLeftMargin(0.1)
    p_bot.SetLeftMargin(0.1)
    p_bot.SetBottomMargin(0.2)
    p_top.SetRightMargin(0.02)
    p_bot.SetRightMargin(0.02)

    if logy:
        if bin_number == None:
            c.SaveAs('plots/embedding_correction_{}_{}_summary_log.pdf'.format(tau_type, systematic))
        else:
            c.SaveAs('plots/embedding_correction_{}_{}_bin{}_log.pdf'.format(tau_type, systematic, bin_number))

    else:
        if bin_number == None:
            c.SaveAs('plots/embedding_correction_{}_{}_summary_zerosuppressed.pdf'.format(tau_type, systematic, bin_number))
        else:
            c.SaveAs('plots/embedding_correction_{}_{}_bin{}_zerosuppressed.pdf'.format(tau_type, systematic, bin_number))

def chi2_mc_campaign(rfile_a, rfile_d, rfile_e, tau_type='hadronic', var='NOMINAL'):

    dir_a = rfile_a.Get(os.path.join(tau_type, var))
    hists_a = {}
    for k in dir_a.GetListOfKeys():
        h = k.ReadObj()
        hists_a[h.GetName()] = h

    dir_d = rfile_d.Get(os.path.join(tau_type, var))
    hists_d = {}
    for k in dir_d.GetListOfKeys():
        h = k.ReadObj()
        hists_d[h.GetName()] = h

    dir_e = rfile_e.Get(os.path.join(tau_type, var))
    hists_e = {}
    for k in dir_e.GetListOfKeys():
        h = k.ReadObj()
        hists_e[h.GetName()] = h

    pt_bins = []
    eta_bins = []
    chisquares_a_d = []
    for k in sorted(hists_a.keys()):
        h_a = hists_a[k]
        h_d = hists_d[k]
        h_e = hists_e[k]
        _info_title = h_a.GetTitle().split('_')
        pt_bin  = (float(_info_title[1]), float(_info_title[2]))
        eta_bin = (float(_info_title[4]), float(_info_title[5]))
        if pt_bin not in pt_bins:
            pt_bins += [pt_bin]
        if eta_bin not in eta_bins:
            eta_bins += [eta_bin]
            
        chi2_a_d = h_a.Chi2Test(h_d, 'WWCHI2/NDF')
        chi2_a_e = h_a.Chi2Test(h_e, 'WWCHI2/NDF')
        chisquares_a_d += [{'pt': pt_bin, 'eta': eta_bin, 'chi2_a_d': chi2_a_d, 'chi2_a_e': chi2_a_e}]
        print pt_bin, eta_bin, chi2_a_d, chi2_a_e

    pt_bins = sorted(pt_bins)
    eta_bins = sorted(eta_bins)
    map_d = ROOT.TH2F('chi2_d', '', len(pt_bins), 0, len(pt_bins), len(eta_bins), 0, len(eta_bins))
    map_e = ROOT.TH2F('chi2_e', '', len(pt_bins), 0, len(pt_bins), len(eta_bins), 0, len(eta_bins))
    map_d.GetXaxis().SetTitle('Truth Tau Lepton p_{T} [GeV]')
    map_d.GetYaxis().SetTitle('|#eta|')
    map_e.GetXaxis().SetTitle('Truth Tau Lepton p_{T} [GeV]')
    map_e.GetYaxis().SetTitle('|#eta|')

    for i_b, pt_bin in enumerate(pt_bins):
        map_d.GetXaxis().SetBinLabel(i_b + 1, '({}, {})'.format(pt_bin[0], pt_bin[1] if pt_bin[1] < 200 else '#infty'))
        map_e.GetXaxis().SetBinLabel(i_b + 1, '({}, {})'.format(pt_bin[0], pt_bin[1] if pt_bin[1] < 200 else '#infty'))
    for i_b, eta_bin in enumerate(eta_bins):
        map_d.GetYaxis().SetBinLabel(i_b + 1, '({}, {})'.format(eta_bin[0], eta_bin[1]))
        map_e.GetYaxis().SetBinLabel(i_b + 1, '({}, {})'.format(eta_bin[0], eta_bin[1]))

    for i_pt, pt_bin in enumerate(pt_bins):
        for i_eta, eta_bin in enumerate(eta_bins):
            chi2 = filter(lambda c: c['pt'] == pt_bin and c['eta'] == eta_bin, chisquares_a_d)
            map_d.SetBinContent(i_pt + 1, i_eta + 1, float(chi2[0]['chi2_a_d']))
            map_e.SetBinContent(i_pt + 1, i_eta + 1, float(chi2[0]['chi2_a_e']))

    c = ROOT.TCanvas(uuid.uuid4().hex, '', 800, 600)
    c.SetTopMargin(0.1)
    c.SetRightMargin(0.1)
    c.SetLeftMargin(0.2)
    map_d.Draw('COLZTEXT')
    _label = ROOT.TLatex(
        0.5, 0.97,
        '{}: #chi^{{2}}/NDF of mc16a and mc16d campaigns'.format(_TITLE[tau_type]))
    _label.SetTextAlign(22)
    _label.SetNDC()
    _label.SetTextSize(0.8 * _label.GetTextSize())
    _label.Draw()
    c.SaveAs('plots/embedding_ratio_chi2_{}_mc16d_mc16a.pdf'.format(tau_type))

    c1 = ROOT.TCanvas(uuid.uuid4().hex, '', 800, 600)
    c1.SetTopMargin(0.1)
    c1.SetRightMargin(0.1)
    c1.SetLeftMargin(0.2)
    map_e.Draw('COLZTEXT')
    _label = ROOT.TLatex(
        0.5, 0.97,
        '{}: #chi^{{2}}/NDF of mc16a and mc16e campaigns'.format(_TITLE[tau_type]))
    _label.SetTextAlign(22)
    _label.SetNDC()
    _label.SetTextSize(0.8 * _label.GetTextSize())
    _label.Draw()
    c1.SaveAs('plots/embedding_ratio_chi2_{}_mc16e_mc16a.pdf'.format(tau_type))

def plot_mc_campaign(rfile_a, rfile_d, rfile_e, tau_type='hadronic', bin_number=None, logy=False):
    if bin_number == None:
        _ratio_a = _merge_hist(rfile_a, tau_type)
        _ratio_d = _merge_hist(rfile_d, tau_type)
        _ratio_e = _merge_hist(rfile_e, tau_type)
    else:
        _ratio_a = rfile_a.Get('{}/{}/bin{}'.format(tau_type, 'NOMINAL', bin_number))
        _ratio_d = rfile_d.Get('{}/{}/bin{}'.format(tau_type, 'NOMINAL', bin_number))
        _ratio_e = rfile_e.Get('{}/{}/bin{}'.format(tau_type, 'NOMINAL', bin_number))

        _info = _ratio_a.GetTitle()
    _ratio_a.SetTitle('mc16a')
    _ratio_d.SetTitle('mc16d')
    _ratio_e.SetTitle('mc16e')

    max_val = max(
        _ratio_a.GetBinContent(_ratio_a.GetMaximumBin()),
        _ratio_d.GetBinContent(_ratio_d.GetMaximumBin()),
        _ratio_e.GetBinContent(_ratio_e.GetMaximumBin()))

    _ratio_a.SetLineColor(ROOT.kBlack)
    _ratio_d.SetLineColor(ROOT.kRed)
    _ratio_e.SetLineColor(ROOT.kBlue)
    
    _double_ratio_d = _ratio_d.Clone(uuid.uuid4().hex)
    _double_ratio_d.Divide(_ratio_a)

    _double_ratio_e = _ratio_e.Clone(uuid.uuid4().hex)
    _double_ratio_e.Divide(_ratio_a)

    if logy:
        _template_top = ROOT.TH1F(uuid.uuid4().hex, '', 10, -0.2, 2)
        _template_bot = ROOT.TH1F(uuid.uuid4().hex, '', 10, -0.2, 2)
    else:
        _template_top = ROOT.TH1F(uuid.uuid4().hex, '', 10, 0.03, 2)
        _template_bot = ROOT.TH1F(uuid.uuid4().hex, '', 10, 0.03, 2)
        
    _template_top.GetXaxis().SetTitle(_ratio_a.GetXaxis().GetTitle())
    _template_bot.GetXaxis().SetTitle(_ratio_a.GetXaxis().GetTitle())

    _template_top.GetYaxis().SetTitle()
    _template_bot.GetYaxis().SetTitle()
    _template_top.GetYaxis().SetTitle('Arbitrary Units')
    _template_bot.GetYaxis().SetTitle('mc16d(e) / mc16a')


    if logy:
        _template_top.GetYaxis().SetRangeUser(2e-9,  2)
        _template_bot.GetYaxis().SetRangeUser(0.57, 1.43)
    else:
        _template_top.GetYaxis().SetRangeUser(0, 0.05)
        _template_bot.GetYaxis().SetRangeUser(0.8, 1.2)

    c = ROOT.TCanvas(uuid.uuid4().hex, '', 800, 600)
    c.SetLeftMargin(0.1)
    c.SetRightMargin(0.02)
    c.SetTopMargin(0.08)
    c.SetBottomMargin(0.)
    c.Divide(1, 2)
    p_top = c.GetPad(1)
    p_top.SetPad(
#         c.GetLeftMargin(), #x_low
        0,
        0.4, #y_low
        1 - c.GetRightMargin(), #x_high
        1 - c.GetTopMargin())
    p_top.SetTopMargin(0)
    p_top.SetBottomMargin(0)

    p_bot = c.GetPad(2)
    p_bot.SetPad(
        0,
        #c.GetLeftMargin(), #x_low
        c.GetBottomMargin() + 0.05, #y_low
        1 - c.GetRightMargin(), #x_high
        0.4)
    p_bot.SetTopMargin(0)
    p_bot.SetBottomMargin(0.9)
    p_bot.SetGridy()

    _template_top.GetYaxis().SetTitleOffset(0.8)
    _template_bot.GetYaxis().SetNdivisions(5)
    _template_bot.GetYaxis().SetLabelSize(2 * _template_bot.GetYaxis().GetLabelSize())
    _template_bot.GetYaxis().SetTitleSize(1.8 * _template_bot.GetYaxis().GetTitleSize())
    _template_bot.GetYaxis().SetTitleOffset(0.45)

    _template_bot.GetXaxis().SetLabelSize(2 * _template_bot.GetXaxis().GetLabelSize())
    _template_bot.GetXaxis().SetTitleSize(1.8 * _template_bot.GetXaxis().GetTitleSize())
    _template_bot.GetXaxis().SetTitleOffset(1.05)
    _template_bot.GetXaxis().SetTickSize(0.06)
    p_top.cd()
    p_top.SetLogy(logy)
    _template_top.Draw('HIST')
    _ratio_a.Draw('sameHISTE')
    _ratio_d.Draw('sameHISTE')
    _ratio_e.Draw('sameHISTE')
    leg = ROOT.TLegend(0.7, 0.75, 1., 0.95)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetHeader('MC campaign')
    leg.AddEntry(_ratio_a, _ratio_a.GetTitle(), 'l')
    leg.AddEntry(_ratio_d, _ratio_d.GetTitle(), 'l')
    leg.AddEntry(_ratio_e, _ratio_e.GetTitle(), 'l')
    leg.Draw()

    p_bot.cd()
    _template_bot.Draw('HIST')
    _double_ratio_d.Draw('SAMEHISTE')
    _double_ratio_e.Draw('SAMEHISTE')
    p_bot.RedrawAxis()
    p_top.RedrawAxis()

    c.cd()
    if bin_number == None:
        _label = ROOT.TLatex(
            1 - p_top.GetRightMargin() + c.GetRightMargin(),
            1 - c.GetTopMargin() / 2.,
            '{}, Integrated over all p_{{T}} and |#eta| bins'.format(_TITLE[tau_type]))
    else:
        c.cd()
        _info = _info.split('_')
        _label = ROOT.TLatex(
            1 - c.GetRightMargin(),
            1 - c.GetTopMargin() / 2.,
            '{}, {} < p_{{T}} < {}, {} < |#eta| < {}'.format(_TITLE[tau_type], _info[1], _info[2], _info[4], _info[5])
            )
    _label.SetTextAlign(32)
    _label.SetTextSize(_label.GetTextSize() * 0.7)
    _label.SetNDC()
    _label.Draw()
    c.Update()

    p_top.SetLeftMargin(0.1)
    p_bot.SetLeftMargin(0.1)
    p_bot.SetBottomMargin(0.2)
    p_top.SetRightMargin(0.02)
    p_bot.SetRightMargin(0.02)

    if logy:
        if bin_number == None:
            c.SaveAs('plots/embedding_correction_{}_mccampaign_summary_log.pdf'.format(tau_type))
        else:
            c.SaveAs('plots/embedding_correction_{}_mccampaign_bin{}_log.pdf'.format(tau_type, bin_number))

    else:
        if bin_number == None:
            c.SaveAs('plots/embedding_correction_{}_{}_summary_zerosuppressed.pdf'.format(tau_type, 'mccampaign', bin_number))
        else:
            c.SaveAs('plots/embedding_correction_{}_{}_bin{}_zerosuppressed.pdf'.format(tau_type, 'mccampaign', bin_number))

def plot_matrix(rfile, tau_type, color=ROOT.kRed):
    rfile.cd()
    _dir = rfile.Get(os.path.join(tau_type, 'NOMINAL'))
    _histograms = []
    for key in _dir.GetListOfKeys():
        _h = key.ReadObj()
        if isinstance(_h, ROOT.TH1):
            _histograms.append(_h)

    # rebuild pt and eta bins
    _pt_bins = []
    _eta_bins = []
    _hist_array = {}
    for _h in _histograms:
        _info_title = _h.GetTitle().split('_')
        pt_bin  = (float(_info_title[1]), float(_info_title[2]))
        eta_bin = (float(_info_title[4]), float(_info_title[5]))
        if eta_bin not in _eta_bins:
            _eta_bins.append(eta_bin)
            if not eta_bin in _hist_array.keys():
                _hist_array[eta_bin] = {}
        if pt_bin not in _pt_bins:
            _pt_bins.append(pt_bin)
        if not pt_bin in _hist_array[eta_bin].keys():
            _hist_array[eta_bin][pt_bin] = _h


#     print _pt_bins
#     print _eta_bins
    from happy.doublePlot import PlotMatrix
    from happy.variable import Variable, var_Entries, Binning

    _vars_x = []
    for _bin in _pt_bins:
        _vars_x += [Variable(
            '{} < p_{{T}}(#tau) < {}'.format(_bin[0], _bin[1] if _bin[1] < 1e3 else '#infty'), 
            binning=Binning(low=0, high=2))]

    _vars_y = []
    for _bin in _eta_bins:
        _vars_y += [Variable(
            '{} < |#eta| < {}'.format(_bin[0], _bin[1]), 
            binning=Binning(low=1e-9, high=1.1))]
                     
    ROOT.gStyle.SetPadTopMargin(0.2)
    ROOT.gStyle.SetPadLeftMargin(0.2)
    ROOT.gStyle.SetTitleYOffset(2)
    plot = PlotMatrix(
        'Plot',
        variablesX=_vars_x,
        variablesY=_vars_y,
        padSizesY=[1 for i in xrange(len(_eta_bins))],
        padSizesX=[1 for i in xrange(len(_pt_bins))])

    plot.sizeY = 900
    plot.sizeX = 1600
    plot.referenceSizeX = 400
    plot.referenceSizeY = 300
#     plot.referenceSizeX = 150
#     plot.referenceSizeY = 200
#     plot.padMarginY = 0
#     plot.padMarginX = 0

    for i in xrange(len(_pt_bins)):
        for ii in xrange(len(_eta_bins)):
#             print _pt_bins[ii]
#             print _eta_bins[i]
            _h = _hist_array[_eta_bins[ii]][_pt_bins[i]]
            _h.SetFillColor(color)
            _h.SetLineColor(color)
            _h.SetMarkerSize(0)
            plot.plots[i][ii].showBinWidthY = False
            plot.plots[i][ii].logY = True
            plot.plots[i][ii].drawLegend = False
            _template = ROOT.TH1F(uuid.uuid4().hex, '', 10, -0.2, 2)
            plot.plots[i][ii].addHistogram(_template, 'HIST')
            plot.plots[i][ii].addHistogram(_h, 'HIST')
    plot.draw()
    plot.canvas.cd()
    _label = ROOT.TLatex(
        0.5, 0.97,
        'p_{T}(visible reconstructed decay) / p_{T}(truth tau lepton) for  ' + _TITLE[tau_type])
    _label.SetTextAlign(22)
    _label.Draw()
    plot.saveAs('plots/embedding_full_corrections_{}.pdf'.format(tau_type))


if __name__ == '__main__':

    rfile   = ROOT.TFile('data/embedding/ratios_normalised.root')
    rfile_a = ROOT.TFile('data/embedding/ratios_mc16a_normalised.root')
    rfile_d = ROOT.TFile('data/embedding/ratios_mc16d_normalised.root')
    rfile_e = ROOT.TFile('data/embedding/ratios_mc16e_normalised.root')

    plot_summary(rfile)
    plot_summary(rfile, logy=True)
    plot_summary(rfile, bin_number=6, logy=True)

    plot_matrix(rfile, 'hadronic', color=ROOT.kRed)
    plot_matrix(rfile, 'muonic', color=ROOT.kBlue)
    plot_matrix(rfile, 'electronic', color=ROOT.kOrange)

    _tau_sys = [
        'TAUS_TRUEHADTAU_SME_TES_MODEL_CLOSURE',
        'TAUS_TRUEHADTAU_SME_TES_PHYSICSLIST',
        'TAUS_TRUEHADTAU_SME_TES_INSITUFIT',
        'TAUS_TRUEHADTAU_SME_TES_INSITUEXP'
        ]
    for sys in _tau_sys:
        plot_systematic(
            rfile, tau_type='hadronic', systematic=sys, bin_number=6, logy=True)

    _ele_sys = [
        'EG_RESOLUTION_ALL',
        'EG_SCALE_ALL',
        ]
    for sys in _ele_sys:
        plot_systematic(
            rfile, tau_type='electronic', systematic=sys, bin_number=6, logy=True)



    _muon_sys = [
        'MUON_SAGITTA_RHO',
        'MUON_SAGITTA_RESBIAS',
        'MUON_MS',
        'MUON_ID',
        'MUON_SCALE',
        ]
    for sys in _muon_sys:
        plot_systematic(
            rfile, tau_type='muonic', systematic=sys, bin_number=6, logy=True)


    plot_mc_campaign(rfile_a, rfile_d, rfile_e, tau_type='hadronic', bin_number=None, logy=True)
    plot_mc_campaign(rfile_a, rfile_d, rfile_e, tau_type='electronic', bin_number=None, logy=True)
    plot_mc_campaign(rfile_a, rfile_d, rfile_e, tau_type='muonic', bin_number=None, logy=True)
    plot_mc_campaign(rfile_a, rfile_d, rfile_e, tau_type='hadronic', bin_number=6, logy=True)
    plot_mc_campaign(rfile_a, rfile_d, rfile_e, tau_type='electronic', bin_number=6, logy=True)
    plot_mc_campaign(rfile_a, rfile_d, rfile_e, tau_type='muonic', bin_number=6, logy=True)

    chi2_mc_campaign(rfile_a, rfile_d, rfile_e, tau_type='hadronic')
    chi2_mc_campaign(rfile_a, rfile_d, rfile_e, tau_type='electronic')
    chi2_mc_campaign(rfile_a, rfile_d, rfile_e, tau_type='muonic')
