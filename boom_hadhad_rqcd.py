# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections, filter_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.extern.tabulate import tabulate
from boom.utils import hist_sum, yield_tuple, hist_max, rqcd_calc
from boom.plotting import make_rqcd_plot

# import logging
# logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes(no_fake=True)

regions = (
    'SR', 
    'anti_tau', 
#   'same_sign', 
    )

### define your selection objects 
sels = get_selections(
    channels=('1p1p', '1p3p', '3p1p', '3p3p'),
    years=('15', '16', '17', '18'),
    categories=('qcd_fit'),
    regions=regions)


# define your list of variables
variable = VARIABLES['norm']

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variable)
processor.book()
processor.run(n_cores=cpu_count() - 1)

lines = []
data_minus_mc = {}
for region in regions:
    print region
    sels_r = filter_selections(sels, regions=region, categories=('qcd_fit'))
    hdata = processor.get_hist_physics_process('Data', sels_r, variable)
    data = hdata.Integral()

    h_mcs = []
    mc = 0
    for p in processor.mc_backgrounds:
        h = processor.get_hist_physics_process(p.name, sels_r, variable)
        mc += h.Integral()
        h_mcs.append(h)

    h_mc = hist_sum(h_mcs)
    h_data_minus_mc = hdata.Clone()
    h_data_minus_mc.Add(h_mc, -1)
    print data, mc, data - mc
    print hdata.Integral(), h_mc.Integral(), h_data_minus_mc.Integral()
    lines.append([region, data, mc, data - mc])
    data_minus_mc[region] = h_data_minus_mc

print tabulate(lines, headers=['region', 'data', 'mc', 'data - mc'])

rqcd_transfers = [
    ('SR', 'anti_tau'), 
#   ('SR', 'same_sign'), 
]

line = []

rqcd_hists = []

for (sr, cr) in rqcd_transfers:
    rqcd_val, rqcd_err = rqcd_calc(data_minus_mc[sr], data_minus_mc[cr])
    line.append('{} +/- {}'.format(rqcd_val, rqcd_err))
    rqcd = data_minus_mc[sr].Clone()
    rqcd.Divide(data_minus_mc[cr])
    rqcd.SetTitle('{}/{}'.format(sr, cr))
    rqcd_hists.append(rqcd)

print tabulate([line], headers=['{}/{}'.format(sr, cr) for (sr, cr) in rqcd_transfers])
make_rqcd_plot(sels, rqcd_hists, variable)

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
