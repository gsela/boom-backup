import os
import subprocess
import itertools
import json

_QUEUE_DICT = {}
with open('data/condor_queue.json') as f:
    _QUEUE_DICT = json.load(f)

_MEMORY_DICT = {}
with open('data/condor_memory.json') as f:
    _MEMORY_DICT = json.load(f)

def _build_command(
        job,
        stxs,
        variable='mmc_mlm_m_fine_binning',
        executable='python condor_boom', 
        steering_script='boom_workspace_prod.py',
        json_file=None,
        z_cr_deactivate_corrections=False,
        z_cr_embedding_setup='NOMINAL',
        forced_queue=None,
        forced_single_cpu=False,
        memory=None,
        local=False,
        naf=False,
        bnl=False):
    """
    """



    _cmd_args = [
        executable,
        steering_script,
        '--year ' + job.year,
        '--channel ' + job.channel,
        '--var-group-name ' + job.variation,
        '--process-group-name ' + job.process,
        ]

    if stxs != 'stxs0':
        _cmd_args += ['--stxs ' + stxs]


    if json_file != None:
        _cmd_args += ['--json ' + json_file]

    if job.zcr:
        _cmd_args += ['--variable norm']
    else:
        _cmd_args += ['--variable ' + variable]

    _kinematics = get_variations('kinematics', z_cr=job.zcr)
    if job.variation in _kinematics.keys():
        if job.zcr:
            _cmd_args += ['--ntuples-block ' + job.variation.split('__')[0]]
        else:
            _cmd_args += ['--ntuples-block ' + job.variation]
        

    if job.zcr:
        _cmd_args += ['--zcr']
        _cmd_args += ['--zcr-flavour {}'.format(job.zcr_flavour)]
        if z_cr_deactivate_corrections:
            _cmd_args += ['--zcr-deactivate-corrections']
        else:
            _cmd_args += ['--embed-setup {}'.format(
                    z_cr_embedding_setup)]

        if job.zllqcd_dsid != None:
            _cmd_args += ['--zcr-zllqcd-dsid {}'.format(
                job.zllqcd_dsid)]
    
    # queue arg is only needed for NAF and LXBATCH
    if args.local == False and args.bnl == False:
        if forced_queue != None:
            _cmd_args += ['--queue {}'.format(forced_queue)]
        else:
            _queue_key = '_'.join([
                job.year,
                job.channel,
                'zcr' if job.zcr else 'sr',
                job.variation,
                job.process])
            if job.zllqcd_dsid != None:
                _queue_key += '_' + str(job.zllqcd_dsid)

            if not _queue_key in _QUEUE_DICT.keys():
                print 'BOOM: {} is missing from the queue dict'.format(_queue_key)
                if job.process == 'ZttQCD' and job.variation != 'nominal':
                    _cmd_args += ['--queue tomorrow']
                else:
                    _cmd_args += ['--queue workday']
            else:
                _cmd_args += ['--queue {}'.format(_QUEUE_DICT[_queue_key])]


    out_dir = 'condor_{}'.format(variable)
    if job.zcr:
        out_dir += '_zcr_{}'.format(job.zcr_flavour)
    else:
        out_dir += '_{}'.format(job.channel)
    if not args.local:
        _cmd_args += ['--outdir {}'.format(out_dir)]

    if args.bnl and memory != None:
        _memory_key = '_'.join([
            job.year,
            job.channel,
            'zcr' if job.zcr else 'sr',
            job.variation,
            job.process])
        if job.zllqcd_dsid != None:
            _memory_key += '_' + str(job.zllqcd_dsid)

        if _memory_key in _MEMORY_DICT.keys():
            if _MEMORY_DICT[_memory_key] > memory:
                _cmd_args += ['--memory {}'.format(_MEMORY_DICT[_memory_key])]
            else:
                _cmd_args += ['--memory {}'.format(memory)]
        else:
            _cmd_args += ['--memory {}'.format(memory)]

    _command = ' '.join(_cmd_args)
    return _command


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser(usage='%(prog)s [options]')
    parser.add_argument('--year', default=None, nargs='*', choices=['15', '16', '17', '18'])
    parser.add_argument('--channel', default=None, nargs='*', choices=['ll', 'lh', 'hh'])
    parser.add_argument('--process-group-name', default=None, nargs='*')
    parser.add_argument('--json', default='data/wsi/wsi_with_embedding.json', help = "(default: %(default)s)")
    parser.add_argument('--var-group-name', default=None, nargs='*')
    parser.add_argument('--variable', default='mmc_mlm_m_fine_binning', help = "(default: %(default)s)")
    parser.add_argument('--steering-script', default='boom_workspace_prod.py')
    parser.add_argument('--zcr', default=False, action='store_true')
    parser.add_argument('--zcr-deactivate-corrections', default=False, action='store_true', help = "(default: %(default)s)")
    parser.add_argument('--zcr-flavour', default=['ee', 'mumu'], nargs='*', choices=['ee', 'mumu'])
    parser.add_argument('--zcr-zllqcd-dsid', default=None)
    parser.add_argument('--stxs', default='stxs12_fine', choices=['stxs1', 'stxs12', 'stxs12_fine'])
    # skip args
    parser.add_argument('--skip-nominal', default=False, action='store_true')
    parser.add_argument('--skip-variations', default=False, action='store_true')
    parser.add_argument('--skip-weights', default=False, action='store_true')
    parser.add_argument('--skip-kinematics', default=False, action='store_true')
    parser.add_argument('--skip-embedding-variations', default=False, action='store_true')
    # submit args
    alternate_sub = parser.add_mutually_exclusive_group()
    alternate_sub.add_argument('--local', default=False, action='store_true')
    alternate_sub.add_argument('--naf', default=False, action='store_true')
    alternate_sub.add_argument('--bnl', default=False, action='store_true')

    parser.add_argument('--submit', default=False, action='store_true')
    parser.add_argument('--forced-queue', default=None)
    parser.add_argument('--forced-single-cpu', default=False, action='store_true')
    parser.add_argument('--memory', default=1500, type=int)
    args = parser.parse_args()

    from boom.batch.definitions import get_variations
    from boom.batch.utils import expected_jobs

    if args.channel == None:
        _channels = [
            'll', 
            'lh', 
            'hh', 
        ]
    else:
        _channels = args.channel

    if args.zcr:
        _channels = ['all']

    if args.year == None:
        _years = [
            '15 16', 
            '17', 
            '18',
        ]
    else:
        if '15' in args.year and '16' in args.year:
            _years = args.year
            _years.remove('15')
            _years.remove('16')
            _years.append('15 16')
        else:
            _years = args.year

    _kinematics = get_variations('kinematics', z_cr=args.zcr)
    _weights = get_variations('weights')

    # do embedding variations
    if args.zcr and not args.zcr_deactivate_corrections:
        _embedding = get_variations('embedding')

    _specific_systematic_block = args.var_group_name
    _specific_process_group = args.process_group_name

    _zcr_flavours = args.zcr_flavour
    if not isinstance(_zcr_flavours, (list, tuple)):
        _zcr_flavours = [_zcr_flavour]
    if not args.zcr:
        _zcr_flavours = [None]

    if args.local:
        _executable = 'python'
    elif args.naf:
        _executable = 'python condor_boom_naf'
    elif args.bnl:
        _executable = 'python condor_boom_bnl'
    else:
        _executable = 'python condor_boom'
    _steering_script = args.steering_script

    commands = []

    _expected_jobs = expected_jobs(
        channel=_channels, 
        year=_years, 
        process_group=_specific_process_group,
        variation_group=_specific_systematic_block,
        z_cr=args.zcr, 
        z_cr_flavour=_zcr_flavours,
        z_cr_deactivate_corrections=args.zcr_deactivate_corrections,
        stxs=args.stxs)

    
    _commands = []
    for _job in _expected_jobs:
        if args.skip_nominal:
            if _job.variation == 'nominal':
                continue

        if args.skip_variations:
            if _job.variation != 'nominal':
                continue

        if args.skip_weights:
            if _job.variation in _weights.keys():
                continue

        if args.skip_kinematics:
            if _job.variation in _kinematics.keys():
                continue
            
        if args.skip_embedding_variations:
            if _job.variation in _embedding.keys():
                continue
        
        if args.stxs in _job.process:
            _stxs = args.stxs
        else:
            _stxs = 'stxs0'

        if args.zcr and args.zcr_zllqcd_dsid != None:
            if _job.zllqcd_dsid != args.zcr_zllqcd_dsid:
                continue

        z_cr_embedding_setup= 'NOMINAL'
        if _job.variation.startswith('embed'):
            z_cr_embedding_setup = _job.variation

        _cmd = _build_command(
            _job,
            _stxs,
            args.variable,
            executable=_executable, 
            steering_script=_steering_script,
            json_file=args.json,
            z_cr_deactivate_corrections=args.zcr_deactivate_corrections,
            z_cr_embedding_setup=z_cr_embedding_setup,
            forced_queue=args.forced_queue,
            forced_single_cpu=args.forced_single_cpu,
            memory=args.memory,
            local=args.local,
            naf=args.naf,
            bnl=args.bnl)
        _commands.append(_cmd)

    for i_cmd, _cmd in enumerate(_commands):
        print '{}/{}'.format(i_cmd + 1, len(_commands)), _cmd
        if args.submit:
            subprocess.call(_cmd, shell=True)
