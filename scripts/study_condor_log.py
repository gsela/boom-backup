import os
import datetime
import ROOT
import uuid



def _plot_queue(jobs):

    _queues = [
        'espresso',
        'microcentury',
        'longlunch',
        'workday',  
        'tomorrow',    
        'testmatch',   
        'nextweek',
    ]  

    def _hist(color=None, title=''):
        __h = ROOT.TH1F(uuid.uuid4().hex, title, len(_queues), 0, len(_queues))
        __h.GetYaxis().SetTitle('Number Of Jobs')
        __h.GetXaxis().SetTitle('Condor Queue')
        __h.GetXaxis().SetLabelSize(0.8 * __h.GetXaxis().GetLabelSize())
        for i_q, _queue in enumerate(_queues):
            __h.GetXaxis().SetBinLabel(i_q + 1, _queue)
        if color != None:
            __h.SetLineColor(color)
            __h.SetMarkerColor(color)
            __h.SetFillColor(color)
        return __h

    _h_tot = _hist()
    _h_hh = _hist(title='hh', color=ROOT.kRed)
    _h_lh = _hist(title='lh', color=ROOT.kBlue)
    _h_ll = _hist(title='ll', color=ROOT.kGreen)
    _h_1516 = _hist(title='15 16', color=ROOT.kRed)
    _h_17   = _hist(title='17', color=ROOT.kBlue)
    _h_18   = _hist(title='18', color=ROOT.kGreen)

    
    for _job in jobs:
        if _job.output['totalruntime'] == None:
            continue
        _h_tot.Fill(_job.best_queue, 1)
        _h_hh.Fill(_job.best_queue, _job.channel == 'hh')
        _h_lh.Fill(_job.best_queue, _job.channel == 'lh')
        _h_ll.Fill(_job.best_queue, _job.channel == 'll')
        _h_1516.Fill(_job.best_queue, _job.year == '15 16')
        _h_17.Fill(_job.best_queue, _job.year == '17')
        _h_18.Fill(_job.best_queue, _job.year == '18')

    c_0 = ROOT.TCanvas()
    c_0.SetTopMargin(0.08)
    stack_channel = ROOT.THStack()
    stack_channel.Add(_h_hh)
    stack_channel.Add(_h_lh)
    stack_channel.Add(_h_ll)
    _h_tot.Draw('HIST')
    stack_channel.Draw('sameHIST')
    _h_tot.Draw('sameHIST')
    _legend_0 = ROOT.TLegend(        
        c_0.GetLeftMargin() + 0.2,
        1 - c_0.GetTopMargin(),
        1 - c_0.GetRightMargin(),
        1)
    for __h in stack_channel.GetHists():
        _legend_0.AddEntry(__h, __h.GetTitle(), 'f')
    _legend_0.SetNColumns(len(stack_channel.GetHists()))
    _legend_0.Draw('same')
    c_0.SetLogy()
    c_0.RedrawAxis()
    c_0.SaveAs('/eos/user/q/qbuat/condor_queues_channels.pdf')

    c_1 = ROOT.TCanvas()
    c_1.SetTopMargin(0.08)
    stack_year = ROOT.THStack()
    stack_year.Add(_h_1516)
    stack_year.Add(_h_17)
    stack_year.Add(_h_18)
    _h_tot.Draw('HIST')
    stack_year.Draw('sameHIST')
    _h_tot.Draw('sameHIST')
    _legend_1 = ROOT.TLegend(        
        c_1.GetLeftMargin() + 0.2,
        1 - c_1.GetTopMargin(),
        1 - c_1.GetRightMargin(),
        1)
    for __h in stack_year.GetHists():
        _legend_1.AddEntry(__h, __h.GetTitle(), 'f')
    _legend_1.SetNColumns(len(stack_year.GetHists()))
    _legend_1.Draw('same')
    c_1.SetLogy()
    c_1.RedrawAxis()
    c_1.SaveAs('/eos/user/q/qbuat/condor_queues_years.pdf')
    

    


def _plot(jobs, years=None, channels=None, syst=None, process=None, name='condor_time'):

    _max  = max([j.minutes for j in jobs]) * 1.1
    _min = 0
    _nbins = 100
    _hists = []
    # year block
    if years != None:
        for _year in years:
            _jobs = filter(lambda j: j.year == _year, jobs)
            h = ROOT.TH1F(uuid.uuid4().hex, _year, _nbins, _min, _max)
            for _job in _jobs:
                h.Fill(_job.minutes)
            _hists.append(h)

    # channel block
    if channels != None:
        for _channel in channels:
            _jobs = filter(lambda j: j.channel == _channel, jobs)
            h = ROOT.TH1F(uuid.uuid4().hex, _channel, _nbins, _min, _max)
            h.Sumw2()
            for _job in _jobs:
                h.Fill(_job.minutes)
            _hists.append(h)

    # process block
    if process != None:
        for _proc in process:
            _jobs = filter(lambda j: j.process == _proc, jobs)
            h = ROOT.TH1F(uuid.uuid4().hex, _proc, _nbins, _min, _max)
            h.Sumw2()
            for _job in _jobs:
                h.Fill(_job.minutes)
            _hists.append(h)

    # process block
    if syst != None:
        for _s in syst:
            _jobs = filter(lambda j: j.variation == _s, jobs)
            h = ROOT.TH1F(uuid.uuid4().hex, _s, _nbins, _min, _max)
            h.Sumw2()
            for _job in _jobs:
                h.Fill(_job.minutes)
            _hists.append(h)

    for i_h, _h in enumerate(_hists):
        _h.SetLineColor(i_h + 1)
        _h.SetMarkerColor(i_h + 1)
        _h.SetMarkerSize(0.00001)

    _y_max = max([_h.GetBinContent(_h.GetMaximumBin()) for _h in _hists])

    c = ROOT.TCanvas()
    c.SetTopMargin(0.08)
    c.SetRightMargin(0.1)
    h_template = ROOT.TH1F('h_tem', '', _nbins, _min, _max)
    h_template.GetYaxis().SetRangeUser(0.1, 1.05 * _y_max)
    h_template.GetXaxis().SetTitle('Condor Run Time [min]')
    h_template.GetYaxis().SetTitle('Number of Jobs')
    h_template.Draw()
    _h_errors = []
    for _h in _hists:
        _h_error = _h.Clone()
        _h_error.SetFillStyle(3354)
        _h_error.SetFillColor(_h.GetLineColor())
        _h_errors += [_h_error]
        _h_error.Draw('sameE2')
        _h.Draw('sameHIST')

    leg = ROOT.TLegend(
        c.GetLeftMargin(),
        1 - c.GetTopMargin(),
        1 - c.GetRightMargin(),
        1)
    for _h in _hists:
        leg.AddEntry(_h, _h.GetTitle(), 'f')
    leg.SetNColumns(len(_hists))

    leg.Draw('same')
    c.SetLogy()
    c.RedrawAxis()
    c.SaveAs('/eos/user/q/qbuat/{}.pdf'.format(name))

if __name__ == '__main__':
    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.SetStyle('ATLAS')

    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('condor_input_dir')
    args = parser.parse_args()

    _dir = args.condor_input_dir

    from boom.batch import build_jobs, filter_jobs
    _jobs = build_jobs(_dir)

    _jobs = filter_jobs(
        _jobs, 
        # channel='hh',
        # year='17',
        # process='ZttQCD', 
        # variation=('nominal', 'TAU_SF', 'sys_j1', 'sys_j2', 'FT_SF',),
        corrupted=False)

    _jobs = filter(lambda j: j.output['totalruntime'] != None, _jobs)

    _plot_queue(_jobs)

    _years = []
    for _job in _jobs:
        if not _job.year in _years:
            _years += [_job.year]
    if len(_years) != 0:
        _plot(_jobs, years=_years, name='condor_year_time')

    _channels = []
    for _job in _jobs:
        if not _job.channel in _channels:
            _channels += [_job.channel]
    if len(_channels) != 0:
        _plot(_jobs, channels=_channels, name='condor_channel_time')

    _processes = []
    for _job in _jobs:
        if not _job.process in _processes:
            _processes += [_job.process]
    if len(_processes) != 0:
        _plot(_jobs, process=_processes, name='condor_process_time')

    _systs = []
    for _job in _jobs:
        if not _job.variation in _systs:
            _systs += [_job.variation]
    if len(_systs) != 0:
        _plot(_jobs, syst=_systs, name='condor_syst_time')


    for _job in _jobs:
        if _job.best_queue in ('tomorrow',):
            print _job
