# python imports
import os
import ROOT
from multiprocessing import cpu_count

# happy import
from happy.style import Style

#local imports
from boom.core import boom_processor, close_stores
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.extern.tabulate import tabulate
from boom.plotting import make_zllvr_proxy_comparison_plot
# Switch on debug output
# import logging
# logging.root.setLevel( logging.DEBUG )


def _plot_compare():
    # retrieve physics processes
    _processes = get_processes(stxs_stage1=False, squash=True, split_ztt=True, split_zll=True)
    _processes = filter(lambda p: 'Ztt' in p.name or 'Zll' in p.name, _processes)

    _categories = (
        'boost',
        )
    
    # ### define your selection objects 
    sels_sr = get_selections(
        channels=('1p1p', '1p3p', '3p1p', '3p3p'),
        categories=_categories,
        regions='SR')

    sels_cr = get_selections(
        channels=('ee', 'mumu'),
        categories=_categories,
        regions='SR')

    sels = sels_sr + sels_cr

    # define your list of variables
    variables = [
        VARIABLES['ditau_pt_truth_fine'],
        ]

    # #### processor declaration, booking and running
    processor = boom_processor(_processes, sels, variables)
    processor.book()
    processor.run(n_cores=cpu_count() - 1)

    for _var in variables:
        make_zllvr_proxy_comparison_plot(processor, sels_sr, sels_cr, _var, 'ZllQCD_truth_0', 'ZttQCD_truth_0', 'HadHad Boost')
        make_zllvr_proxy_comparison_plot(processor, sels_sr, sels_cr, _var, 'ZllQCD_truth_1', 'ZttQCD_truth_1', 'HadHad Boost')
        make_zllvr_proxy_comparison_plot(processor, sels_sr, sels_cr, _var, 'ZllQCD_truth_2', 'ZttQCD_truth_2', 'HadHad Boost')
        make_zllvr_proxy_comparison_plot(processor, sels_sr, sels_cr, _var, 'ZllQCD_truth_3', 'ZttQCD_truth_3', 'HadHad Boost')

    print 'closing stores...'
    close_stores(_processes)
    print 'done'



def _plot_sr():
    # retrieve physics processes
    _processes = get_processes(stxs_stage1=False, squash=True, split_zll=True, split_ztt=True)
    _categories = (
        'boost_2_1J',
        'boost_3_1J',
        )
    
    # ### define your selection objects 
    sels = get_selections(
        channels=('1p1p', '1p3p', '3p1p', '3p3p'),
        categories=_categories,
        regions='SR')



    # define your list of variables
    variables = [
        #     VARIABLES['norm'],
        #     VARIABLES['ditau_p4.Pt()'],
        VARIABLES['higgs_pt'],
        ]

    # #### processor declaration, booking and running
    processor = boom_processor(_processes, sels, variables)
    processor.book()
    processor.run(n_cores=cpu_count() - 1)

    for _process in _processes:
        if 'truth_0' in _process.name:
            _process.style = Style(color=861)
        if 'truth_1' in _process.name:
            _process.style = Style(color=862)
        if 'truth_2' in _process.name:
            _process.style = Style(color=863)
        if 'truth_3' in _process.name:
            _process.style = Style(color=864)
        

    from boom.plotting.data_mc import make_data_mc_plot


    _PROCESS_MERGING_SCHEME = {
        'Signal': [('ggH', 'VBFH', 'WH', 'ZH', 'ttH'), 'Signal'],
        'Ztt_truth_0': [('ZttQCD_truth_0', 'ZttEWK_truth_0'), 'Z#tau#tau, 100 < p_{T}^{truth}(Z) < 120'],
        'Ztt_truth_1': [('ZttQCD_truth_1', 'ZttEWK_truth_1'), 'Z#tau#tau, 120 < p_{T}^{truth}(Z) < 200'],
        'Ztt_truth_2': [('ZttQCD_truth_2', 'ZttEWK_truth_2'), 'Z#tau#tau, 200 < p_{T}^{truth}(Z) < 300'],
        'Ztt_truth_3': [('ZttQCD_truth_3', 'ZttEWK_truth_3'), 'Z#tau#tau, 300 < p_{T}^{truth}(Z) < inf'],
        'Zll': [
            ('ZllQCD_truth_0', 
             'ZllQCD_truth_1', 
             'ZllQCD_truth_2', 
             'ZllQCD_truth_3', 
             'ZllEWK_truth_0', 
             'ZllEWK_truth_1', 
             'ZllEWK_truth_2', 
             'ZllEWK_truth_3'), 
            'Z#rightarrowll'], 
        'Others': [('VBFHWW', 'ggHWW', 'Top', 'W', 'VV'),  'Others'],
        }
    processor._process_merging_scheme = _PROCESS_MERGING_SCHEME
    for _var in variables:
        make_data_mc_plot(processor, sels, _var, legend_entries_per_columns=8)


    print 'closing stores...'
    close_stores(_processes)
    print 'done'

def _plot_cr():
    # retrieve physics processes
    _processes = get_processes(stxs_stage1=False, squash=True, split_zll=True, split_ztt=False, no_fake=True)
    _categories = (
        'boost_1_1J',
        'boost_2_1J',
        )
    
    # ### define your selection objects 
    sels = get_selections(
        channels=('ee', 'mumu'),
        categories=_categories,
        regions='Ztt_cr_hh')



    # define your list of variables
    variables = [
        VARIABLES['norm'],
        VARIABLES['ditau_pt_low'],
        ]

    # #### processor declaration, booking and running
    processor = boom_processor(_processes, sels, variables)
    processor.book()
    processor.run(n_cores=cpu_count() - 1)

    for _process in _processes:
        if 'truth_0' in _process.name:
            _process.style = Style(color=861)
        if 'truth_1' in _process.name:
            _process.style = Style(color=862)
        if 'truth_2' in _process.name:
            _process.style = Style(color=863)
        if 'truth_3' in _process.name:
            _process.style = Style(color=864)
        

    from boom.plotting.data_mc import make_data_mc_plot


    _PROCESS_MERGING_SCHEME = {
        'Signal': [('ggH', 'VBFH', 'WH', 'ZH', 'ttH'), 'Signal'],
        'Zll_truth_0': [('ZllQCD_truth_0', 'ZllEWK_truth_0'), 'Zll, 100 < p_{T}^{truth}(Z) < 120'],
        'Zll_truth_1': [('ZllQCD_truth_1', 'ZllEWK_truth_1'), 'Zll, 120 < p_{T}^{truth}(Z) < 200'],
        'Zll_truth_2': [('ZllQCD_truth_2', 'ZllEWK_truth_2'), 'Zll, 200 < p_{T}^{truth}(Z) < 300'],
        'Zll_truth_3': [('ZllQCD_truth_3', 'ZllEWK_truth_3'), 'Zll, 300 < p_{T}^{truth}(Z) < inf'],
        'Others': [('VBFHWW', 'ggHWW', 'Top', 'W', 'VV', 'ZttQCD', 'ZttEWK'),  'Others'],
        }
    processor._process_merging_scheme = _PROCESS_MERGING_SCHEME
    for _var in variables:
        for _cat in _categories:
            make_data_mc_plot(processor, sels, _var, categories=_cat, legend_entries_per_columns=6)


    print 'closing stores...'
    close_stores(_processes)
    print 'done'


if __name__ == '__main__':
#     _plot_sr()

#     _plot_cr()

    _plot_compare()
