# python imports
import os
import sys, argparse
import ROOT
from multiprocessing import cpu_count


#command-line inputs for batch submission                                                                                                                                                                                                    
parser = argparse.ArgumentParser(description='Zll comparison script -- batch version')
parser.add_argument('rfile')
#parser.add_argument('--channel', default=None)
args = parser.parse_args()

#local imports
from boom.plotting import make_nf_plot
from boom.wsi_tools import workspace_input



_wsi = workspace_input(ROOT.TFile(args.rfile))


make_nf_plot(_wsi, channel=None, title='nf embedding')
make_nf_plot(_wsi, channel='ee', title='nf embedding')
make_nf_plot(_wsi, channel='mumu', title='nf embedding')


    # ------------
ROOT.gStyle.SetPaintTextFormat("1.2f");
ROOT.gStyle.SetPalette(ROOT.kCherry)
ROOT.TColor.InvertPalette()
from boom.plotting.wsi import plot_z_migration
plot_z_migration(_wsi)
plot_z_migration(_wsi, corrected_zcr=True)
plot_z_migration(_wsi, finalstate='lh')
plot_z_migration(_wsi, finalstate='lh', corrected_zcr=True)
plot_z_migration(_wsi, finalstate='ll')
plot_z_migration(_wsi, finalstate='ll', corrected_zcr=True)
