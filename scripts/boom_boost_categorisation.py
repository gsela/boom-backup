import os
import ROOT
from multiprocessing import cpu_count
# happy imports
from happy.variable import Variable, Binning
#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot, make_process_plot
from boom import scheduler
from boom.selection import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes


do_ztt_plot = True
do_zll_vr = True

# retrieve physics processes
physicsProcesses = get_processes()
if do_ztt_plot:
    if do_zll_vr:
        physicsProcesses = filter(lambda p: 'Zll' in p.name, physicsProcesses)
    else:
        physicsProcesses = filter(lambda p: 'Ztt' in p.name, physicsProcesses)

if do_zll_vr:
    physicsProcesses = filter(lambda p: p.name != 'Fake', physicsProcesses)


_categories = ('boost_0', 'boost_1', 'boost_2', 'boost_3')
if do_zll_vr:
    sels = get_selections(
        channels==('ee', 'mumu'), 
        categories=_categories, 
        regions='Ztt_cr_lh')
else:
    sels = get_selections(
        channels=('emu', 'mue', 'e1p', 'e3p', 'mu1p', 'mu3p', '1p1p', '1p3p', '3p1p', '3p3p'),
        categories=_categories,
        regions='SR')



# define your list of variables
variables = [
    VARIABLES['ditau_pt_coarse']
#     VARIABLES['norm'],
#     VARIABLES['tau_0_pt'],
#     VARIABLES['mmc_mlm_m']
#     VARIABLES['tau_1_pt'],
#     VARIABLES['tau_0_bdt_score'],
#     VARIABLES['higgs_pt_coarse'],
#     VARIABLES['mjj'],
#     VARIABLES['jets_deta'],
#     VARIABLES['jet_1_pt'],
#     VARIABLES['jet_0_pt'],
#     VARIABLES['n_jets_30'],
#     VARIABLES['ditau_dr'],
#     VARIABLES['ditau_deta'],
#     VARIABLES['transverse_mass'],
#     VARIABLES['met'],
]

if do_ztt_plot:
    variables = [
        VARIABLES['ditau_pt_truth']
        ]



#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(scheduler, cpu_count() - 1)

# plot making
import ROOT
stop_watch = ROOT.TStopwatch()
if do_ztt_plot:
    if do_zll_vr:
        process_name = 'Zll'
    else:
        process_name = 'Ztt'
    for var in variables:
        for _cat in _categories:
            make_process_plot(processor, process_name, sels, var, categories=_cat)
            if do_zll_vr:
                continue
            make_process_plot(processor, process_name, sels, var, categories=_cat, channels=('emu', 'mue'))                  
            make_process_plot(processor, process_name, sels, var, categories=_cat, channels=('e1p', 'e3p', 'mu1p', 'mu3p'))  
            make_process_plot(processor, process_name, sels, var, categories=_cat, channels=('1p1p', '1p3p', '3p1p', '3p3p'))


else:
    for var in variables:
        # all channels
        make_data_mc_plot(processor, sels, var)
        if do_zll_vr:
            for _cat in _categories:
                make_data_mc_plot(processor, sels, var, categories=_cat)
            continue
        # leplep
        make_data_mc_plot(processor, sels, var, channels=('emu', 'mue'))
        # lephad
        make_data_mc_plot(processor, sels, var, channels=('e1p', 'e3p', 'mu1p', 'mu3p'))
        # hadhad
        make_data_mc_plot(processor, sels, var, channels=('1p1p', '1p3p', '3p1p', '3p3p'))
        for _cat in _categories:
            make_data_mc_plot(processor, sels, var, categories=_cat)
            make_data_mc_plot(processor, sels, var, categories=_cat, channels=('emu', 'mue'))
            make_data_mc_plot(processor, sels, var, categories=_cat, channels=('e1p', 'e3p', 'mu1p', 'mu3p'))
            make_data_mc_plot(processor, sels, var, categories=_cat, channels=('1p1p', '1p3p', '3p1p', '3p3p'))

stop_watch.Stop()
stop_watch.Print()

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'


