# python imports
import os
import ROOT

# happy imports
from happy.xmlParser import parse, physicsProcesses


from boom.cuts.base import DecoratedCut
from boom.cuts import CUTBOOK

mmc_window = DecoratedCut(
    'mmc_window', 'mmc_window',
    'ditau_mmc_mlm_m > 100 && ditau_mmc_mlm_m < 150')
CUTBOOK.append(mmc_window)

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.variables import VARIABLES

# mandatory HAPPy lines - Start by importing samples, then create the hisogram stores
parse('./data/allchannels-dataset-R21-MC16a.xml')


physicsProcesses = filter(lambda p : p.name == 'Ztt', physicsProcesses)

### define your selection objects 
from boom.selection_utils import HADHAD_SELECTIONS, LEPHAD_SELECTIONS, LEPLEP_SELECTIONS
sels = HADHAD_SELECTIONS + LEPHAD_SELECTIONS + LEPLEP_SELECTIONS


# define your list of variables
variables = [
    VARIABLES['norm'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables, verbose=True)
processor.book()
processor.run()

def line_info(name, h):
    line_fmt = '{cat}: {rel:1.2f}% \t {nom:1.2f} \t {err:1.2f} \t {entries}'
    return line_fmt.format(
        cat=name,
        rel=h.GetBinError(1) / float(h.GetBinContent(1)) * 100,
        nom=h.GetBinContent(1),
        err=h.GetBinError(1),
        entries=int(h.GetEntries()))

sels = filter(lambda s: s.category == 'boost_loose', HADHAD_SELECTIONS)
h_hh_boost_loose = processor.get_hist_physics_process('Ztt', sels, variables[0])

sels = filter(lambda s: s.category == 'boost_tight', HADHAD_SELECTIONS)
h_hh_boost_tight = processor.get_hist_physics_process('Ztt', sels, variables[0])

sels = filter(lambda s: s.category == 'vbf_tight', HADHAD_SELECTIONS)
h_hh_vbf_tight = processor.get_hist_physics_process('Ztt', sels, variables[0])

sels = filter(lambda s: s.category == 'vbf_loose', HADHAD_SELECTIONS)
h_hh_vbf_loose = processor.get_hist_physics_process('Ztt', sels, variables[0])

sels = filter(lambda s: s.category == 'vbf_lowdr', HADHAD_SELECTIONS)
h_hh_vbf_lowdr = processor.get_hist_physics_process('Ztt', sels, variables[0])

sels = filter(lambda s: s.category == 'boost_loose', LEPHAD_SELECTIONS)
h_lh_boost_loose = processor.get_hist_physics_process('Ztt', sels, variables[0])

sels = filter(lambda s: s.category == 'boost_tight', LEPHAD_SELECTIONS)
h_lh_boost_tight = processor.get_hist_physics_process('Ztt', sels, variables[0])

sels = filter(lambda s: 'vbf_tight' in s.category, LEPHAD_SELECTIONS)
h_lh_vbf_tight = processor.get_hist_physics_process('Ztt', sels, variables[0])

sels = filter(lambda s: 'vbf_loose' in s.category, LEPHAD_SELECTIONS)
h_lh_vbf_loose = processor.get_hist_physics_process('Ztt', sels, variables[0])

sels = filter(lambda s: s.category == 'boost_loose', LEPLEP_SELECTIONS)
h_ll_boost_loose = processor.get_hist_physics_process('Ztt', sels, variables[0])

sels = filter(lambda s: s.category == 'boost_tight', LEPLEP_SELECTIONS)
h_ll_boost_tight = processor.get_hist_physics_process('Ztt', sels, variables[0])

sels = filter(lambda s: 'vbf_tight' in s.category, LEPLEP_SELECTIONS)
h_ll_vbf_tight = processor.get_hist_physics_process('Ztt', sels, variables[0])

sels = filter(lambda s: 'vbf_loose' in s.category, LEPLEP_SELECTIONS)
h_ll_vbf_loose = processor.get_hist_physics_process('Ztt', sels, variables[0])

print
print line_info('hadhad: boost loose', h_hh_boost_loose)
print line_info('hadhad: boost tight', h_hh_boost_tight)
print line_info('hadhad:   vbf loose', h_hh_vbf_loose)
print line_info('hadhad:   vbf lowdr', h_hh_vbf_lowdr)
print line_info('hadhad:   vbf tight', h_hh_vbf_tight)
print
print line_info('lephad: boost loose', h_lh_boost_loose)
print line_info('lephad: boost tight', h_lh_boost_tight)
print line_info('lephad:   vbf loose', h_lh_vbf_loose)
print line_info('lephad:   vbf tight', h_lh_vbf_tight)
print
print line_info('leplep: boost loose', h_ll_boost_loose)
print line_info('leplep: boost tight', h_ll_boost_tight)
print line_info('leplep:   vbf loose', h_ll_vbf_loose)
print line_info('leplep:   vbf tight', h_ll_vbf_tight)
print

print 'closing stores...'
close_store(physicsProcesses)
print 'done'
