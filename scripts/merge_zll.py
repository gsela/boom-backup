import ROOT

variable = 'ditau_pt_corr'
jobinc = '0'

myFilese = []
myPlotse = []
myFilesmu = []
myPlotsmu = []
variablett = variable.replace('_corr', '')
if variable == 'met_corr':
    variablett = 'met_reco_et'


# Electron Loops
for i in range(10):
    j = str(i)
    myFilese.append(ROOT.TFile("condor/zllvr_proxy_e"+j+"_job"+jobinc+"/zll_histograms.root"))
    myPlotse.append(myFilese[i].Get(variable))

mergedPlote = myPlotse[0].Clone()

for i in range(9):
    mergedPlote.Add(myPlotse[i+1])

# Muon Loops
for i in range(10):
    j = str(i)
    myFilesmu.append(ROOT.TFile("condor/zllvr_proxy_mu"+j+"_job"+jobinc+"/zll_histograms.root"))
    myPlotsmu.append(myFilesmu[i].Get(variable))

mergedPlotmu = myPlotsmu[0].Clone()

for i in range(9):
    mergedPlotmu.Add(myPlotsmu[i+1])

# Ztt SR
zttFile = ROOT.TFile("condor/zttsr_proxy_0_job"+jobinc+"/ztt_histograms.root")
zttPlot = zttFile.Get(variablett)

# Renormalization
intZtt = zttPlot.Integral()
intZlle = mergedPlote.Integral()
intZllmu = mergedPlotmu.Integral()

mergedPlote.Scale(intZtt/intZlle)
mergedPlotmu.Scale(intZtt/intZllmu)

# Format
mergedPlote.SetLineColor(ROOT.kRed)
mergedPlote.SetMarkerColor(ROOT.kRed)
mergedPlotmu.SetLineColor(ROOT.kMagenta)
mergedPlotmu.SetMarkerColor(ROOT.kMagenta)

zttPlot.Draw()
mergedPlote.Draw("same")
mergedPlotmu.Draw("same")


input("Press Enter to Exit...")





