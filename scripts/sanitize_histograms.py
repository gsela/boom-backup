import ROOT

myFile = ROOT.TFile("../data/norm_pt_noRecoCut_0bin.root")

newOutput = ROOT.TFile("../data/norm_pt_noRecoCut_0bin_noneg.root", "RECREATE")

for key in myFile.GetListOfKeys():
    # Fetch histogram
    currentHisto = myFile.Get(key.GetName())
    for i in range(currentHisto.GetNbinsX()+2):
        myBin = currentHisto.GetBinContent(i)
        if myBin < 0:
            currentHisto.SetBinContent(i, 0)
    newOutput.cd()
    currentHisto.Write()
