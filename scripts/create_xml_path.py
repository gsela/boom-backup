# This is a script which will open an xml file,
# and replace the path names with XML entities

infile  = open("../data/lh-dataset-R21-MC16a.xml", "r")
outfile = open("../data/allchannels-dataset-R21-MC16a.xml", "w")

# first, replace all instances of paths with the variable
myLines = infile.readlines()

theString = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4/SM_Htautau_R21/V01"

myLength = len(theString)

for line in myLines:
    # find index of lh path
    myIndex = line.find(theString)
    if myIndex != -1 and line.find("ENTITY") == -1:
        outfile.write(line[0:myIndex]+"&path;"+line[myIndex+myLength:])
    else: 
        outfile.write(line)
