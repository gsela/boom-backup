# Z->ll jobs (ee channel)
python condor_boom boom_makezll_ecr.py --jobname zllvr_proxy_e0 --seed 2733
python condor_boom boom_makezll_ecr.py --jobname zllvr_proxy_e1 --seed 734
python condor_boom boom_makezll_ecr.py --jobname zllvr_proxy_e2 --seed 593
python condor_boom boom_makezll_ecr.py --jobname zllvr_proxy_e3 --seed 7008
python condor_boom boom_makezll_ecr.py --jobname zllvr_proxy_e4 --seed 149
python condor_boom boom_makezll_ecr.py --jobname zllvr_proxy_e5 --seed 3871
python condor_boom boom_makezll_ecr.py --jobname zllvr_proxy_e6 --seed 7685
python condor_boom boom_makezll_ecr.py --jobname zllvr_proxy_e7 --seed 932
python condor_boom boom_makezll_ecr.py --jobname zllvr_proxy_e8 --seed 5177
python condor_boom boom_makezll_ecr.py --jobname zllvr_proxy_e9 --seed 1708

# Z->ll jobs (mumu channel)
python condor_boom boom_makezll_mucr.py --jobname zllvr_proxy_mu0 --seed 2733
python condor_boom boom_makezll_mucr.py --jobname zllvr_proxy_mu1 --seed 734
python condor_boom boom_makezll_mucr.py --jobname zllvr_proxy_mu2 --seed 593
python condor_boom boom_makezll_mucr.py --jobname zllvr_proxy_mu3 --seed 7008
python condor_boom boom_makezll_mucr.py --jobname zllvr_proxy_mu4 --seed 149
python condor_boom boom_makezll_mucr.py --jobname zllvr_proxy_mu5 --seed 3871
python condor_boom boom_makezll_mucr.py --jobname zllvr_proxy_mu6 --seed 7685
python condor_boom boom_makezll_mucr.py --jobname zllvr_proxy_mu7 --seed 932
python condor_boom boom_makezll_mucr.py --jobname zllvr_proxy_mu8 --seed 5177
python condor_boom boom_makezll_mucr.py --jobname zllvr_proxy_mu9 --seed 1708

# Z->tautau job
python condor_boom boom_makeztt_sr.py --jobname zttsr_proxy_0

# Totally random numbers generated online xD
#2733 734 593 7008 149 3871 7685 932 5177 1708