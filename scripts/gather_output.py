# Imports
import sys
import string
import os


# First step: parse in arguments & configuration
if len(sys.argv) < 3:
    print "ERROR: No job epoch & output name provided"
    exit()

# Read argument
myEpoch = sys.argv[1]
myJobName = 'job'+myEpoch
print "Will use Epoch: "+myJobName

myOutput = sys.argv[2]
print "Will use Output File: "+myOutput

# List of full paths for files to merge
filesToMerge = []

# Navigate to output files
myPath = os.getcwd()  
outputPath = os.path.join(myPath, 'condor')
allFiles = os.listdir(outputPath)
for entry in allFiles:
    myFile = os.path.join(outputPath, entry)
    if os.path.isdir(myFile):
        if entry[-1*len(myJobName):] == myJobName:
            print "Found job folder: "+entry
            outputFiles = os.listdir(myFile)
            for subfile in outputFiles:
                if subfile[-5:] == ".root":
                    filesToMerge += [os.path.join(myFile, subfile)]
# Execute merge call
commandString = string.join(filesToMerge, " ")
commandString = "hadd "+myOutput+" "+commandString
print "Merging all files"
os.system(commandString)
