import os
from multiprocessing import cpu_count

def _systematic_list(args):
    from boom.batch.definitions import get_variations 
    from boom.batch import SIGNAL_PROCESS_GROUPS
    from boom.systematics.utils import _is_valid_list

    _kinematics = get_variations('kinematics', z_cr=args.zcr)
    _weights = get_variations('weights')
    _embedding = get_variations('embedding')
    if args.var_group_name in _kinematics.keys():
        _full_list = _kinematics[args.var_group_name]
    elif args.var_group_name in _weights.keys():
        _full_list = _weights[args.var_group_name]
    else:
        if args.zcr and args.var_group_name in _embedding.keys():
            _full_list = _embedding[args.var_group_name]
        else:
            raise ValueError(args.var_group_name)

    _final_list = _is_valid_list(
        _full_list, 
        args.channel, 
        args.year,
        args.process_group_name,
        is_signal=args.process_group_name in SIGNAL_PROCESS_GROUPS,
        z_cr=args.zcr,
        z_cr_flavour=args.zcr_flavour)
    return _final_list


def _wsi_file_name(args):
    from datetime import date

    _year = args.year
    if isinstance(_year, (list, tuple)):
        _year = ''.join(_year)

    _channel = args.channel
    if isinstance(_channel, (list, tuple)):
        _channel = ''.join(_channel)

    _region = 'sigreg'
    if args.zcr:
        _region = 'zllreg_' + ''.join(args.zcr_flavour)
        if not args.zcr_deactivate_corrections:
            _region += '_corr'
    

    _stxs = args.stxs
    _var = args.var_group_name
    
    if args.process_group_name == None:
        _processes = 'all'
    else:
        _processes = args.process_group_name


    _date = '{}.{}.{}'.format(
        date.today().year,        
        date.today().month,
        date.today().day)

    workspace_file = 'wsi_{year}_{stxs}_{chan}decays_{reg}_{stxs}_{var}_{processes}_{date}.root'.format(
        year=_year,
        chan=_channel,
        reg=_region,
        stxs=_stxs,
        var=_var,
        processes=_processes,
        date=_date)
    return workspace_file

def _hist_name(var_name, var_type):
    if var_name == 'nominal':
        return 'nominal'
    else:
        if var_type == 'up':
            return var_name + '_1up'
        elif var_type == 'down':
            return var_name + '_1down'
        else:
            raise ValueError
        
def _get_channels(channel_dict, args):
    from boom.selection_utils import get_selections
    _channel_sels = {}
    for key, _chan in channel_dict.items():
        if args.channel[0] != 'all':
            _channel_names = args.channel
            if not isinstance(_channel_names, (list, tuple)):
                _channel_names = [_channel_names]
            _include_channel = False
            for _name in _channel_names:
                if _name in key:
                    _include_channel = True
            if not _include_channel:
                continue

        if args.zcr:
            if '_ztt' not in key:
                continue
            _zcr_flavours = args.zcr_flavour
            if not isinstance(_zcr_flavours, (list, tuple)):
                _zcr_flavours = [_zcr_flavours]
            _boom_channels = _chan['channels']
            if not isinstance(_boom_channels, (list, tuple)):
                _boom_channels = [_boom_channels]
            _include_channel = False
            for _zcr_flavour in _zcr_flavours:
                if _zcr_flavour in _boom_channels:
                    _include_channel = True
            if not _include_channel:
                continue
        else:
            if '_ztt' in key:
                continue

        

        _channel_sels[key] = get_selections(
            channels=_chan['channels'],
            categories=_chan['categories'],
            years=args.year,
            regions=_chan['regions'])
    return _channel_sels
    
def _get_processes(args):
    from boom.database import get_processes
        
    if args.process_group_name != None:
        from boom.batch import PROCESS_GROUPS
        _specified_processes = PROCESS_GROUPS[args.process_group_name]
 
    _split_z = False
    if 'Ztt' in args.process_group_name or 'Zll' in args.process_group_name:
        if len(_specified_processes) != 1:
            print 'BOOM: activate z splitting'
            _split_z = True
 
    _squash_db = True
    # when running ZttQCD embedding, unsquash the DB
    # if args.process_group_name != None:
    #     if args.zcr and not args.zcr_deactivate_corrections:
    #         if args.process_group_name == 'ZllQCD':
    #             _squash_db = False

    _processes = get_processes(
        stxs=args.stxs,
        split_ztt=_split_z,
        split_zll=_split_z,
        no_fake=args.zcr,
        squash=_squash_db, 
        ntuples_block=args.ntuples_block)

    if args.zcr and not args.zcr_deactivate_corrections:
        _processes = filter(lambda p: 'Zll' in p.name or p.name == 'Data', _processes)

    # nasty hack to reduce the list of samples to a single dsid
    if args.zcr and args.zcr_zllqcd_dsid != None:
        print 'BOOM: reducing ZllQCD to a single DSID per job'
        for _process in _processes:
            if  not 'ZllQCD' in _process.name:
                continue
            for _dataset in _process.datasets:
                _fileNames = _dataset.__dict__['fileNames']
                _new_fileNames = []
                _str_to_find = '*' + str(args.zcr_zllqcd_dsid) + '*'
                for _file_name in _fileNames:
                    if _str_to_find in _file_name:
                        _new_fileNames.append(_file_name)
                _dataset.__dict__['fileNames'] = _new_fileNames

    if args.process_group_name == None:
        return _processes
    else:
        _processes = filter(lambda p: p.name in _specified_processes, _processes)
        return _processes

if __name__ == '__main__':

    from argparse import ArgumentParser
    parser = ArgumentParser(usage='%(prog)s [options]')
    parser.add_argument(
        '--year', default=['15', '16', '17', '18'], nargs='*', 
        choices=['15', '16', '17', '18'], help = "year (default: %(default)s)")
    parser.add_argument('--channel', default=['all'], nargs='*', choices=['all', 'll', 'lh', 'hh'], help = "(default: %(default)s)")
    parser.add_argument('--zcr', default=False, action='store_true', help = "(default: %(default)s)")
    parser.add_argument('--zcr-deactivate-corrections', default=False, action='store_true', help = "(default: %(default)s)")
    parser.add_argument('--zcr-flavour', default=['ee', 'mumu'], nargs='*', choices=['ee', 'mumu'], help="(default: %(default)s)")
    parser.add_argument('--zcr-zllqcd-dsid', default=None, help="(default: %(default)s)")
    parser.add_argument('--embed-setup', default='NOMINAL', help="(default: %(default)s)")
    parser.add_argument('--stxs', default='stxs0', choices=['stxs0', 'stxs1', 'stxs12', 'stxs12_fine'], help = "(default: %(default)s)")
    parser.add_argument('--process-group-name', default=None, help = "(default: %(default)s)")
    parser.add_argument('--var-group-name', default='nominal', help="(default: %(default)s)")
    parser.add_argument('--json', default='data/wsi/wsi_with_embedding.json', help = "(default: %(default)s)")
    parser.add_argument('--variable', default='mmc_mlm_m_fine_binning', help = "(default: %(default)s)")
    parser.add_argument(
        '--ntuples-block', 
        default='nom', 
        choices=['nom', 'sys_j1', 'sys_j2', 'sys_j4', 'sys_j5', 'sys_j6', 'sys_l'], 
        help="(default: %(default)s)")
    parser.add_argument('--queue', default='workday')
    args = parser.parse_args()

    import json
    with open(args.json) as f:
        channel_dict = json.load(f)

        
    _channels_dict = _get_channels(channel_dict, args)
    _processes = _get_processes(args)

    _selections = []
    for key, _sels in _channels_dict.items():
        _selections += _sels

    from boom.variables import VARIABLES
    _variable = VARIABLES[args.variable]

    if args.var_group_name == 'nominal':
        _syst = None
    else:
        _syst = _systematic_list(args)

    if args.embed_setup != 'NOMINAL':
        _syst = None

    # setup the embedding inputs according to the specified argument
    from boom.embedding import setup_embedding
    setup_embedding(var_type=args.embed_setup.replace('embed_', ''))

    from boom.core import boom_processor
    _mc_processes = filter(lambda p: p.isData == False and (p.isSignal == False or 'HWW' in p.name), _processes)
    processor = boom_processor(
        _processes, 
        _selections,
        _variable,
        systematic_names=_syst,
        force_fake_mc_subtraction=not args.zcr and len(_mc_processes) != 0)
    processor.book()
    processor.run(n_cores=cpu_count() - 1)

    _wsi_input = _wsi_file_name(args)
    
    from boom.workspace import channel, make_skeleton, fill_wsi
    # assume for now that all channels have the same list of samples
    sample_names = [p.name for p in _processes]

    if not args.zcr:
        if len(processor.mc_backgrounds) != 0:
            if not 'Data' in sample_names:
                if not 'Fake' in sample_names:
                    sample_names += ['Fake_subtraction']

    write_lumi = True
    if args.var_group_name != 'nominal':
        write_lumi = False
    if not 'Data' in sample_names:
        write_lumi = False

    if args.embed_setup != 'NOMINAL':
        write_lumi = False

    _channel_list = []
    for k in sorted(_channels_dict.keys()):
        chan = channel(k)
        chan.samples = sample_names
        chan.selections = _channels_dict[k]
        _channel_list.append(chan)

    make_skeleton(os.getcwd(), _wsi_input, _channel_list)
    if _syst == None:
        hist_name = 'nominal'
        if args.embed_setup != 'NOMINAL':
            hist_name = args.embed_setup
        fill_wsi(
            os.getcwd(), 
            _wsi_input, 
            _channel_list, 
            _variable, 
            processor, 
            hist_name=hist_name, 
            write_lumi=write_lumi,
            syst_name=_syst,
            syst_type='up',
            multi_processing=True)
    else:
        if not isinstance(_syst, (list, tuple)):
            _syst = [_syst]
        for _s in _syst:
            for _var_type in ('up', 'down'):
                fill_wsi(
                    os.getcwd(), 
                    _wsi_input, 
                    _channel_list, 
                    _variable, 
                    processor, 
                    hist_name=_hist_name(_s, _var_type), 
                    write_lumi=write_lumi,
                    syst_name=_s,
                    syst_type=_var_type,
                    multi_processing=True)
                    
    print 'closing stores...'
    from boom.core import close_stores
    close_stores(_processes)
    print 'done'

