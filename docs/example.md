.. _steering_example:

steering script example
-----------------------

.. literalinclude:: example.py
    :linenos:
    :language: python
