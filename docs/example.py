# From the core of boom, you need the processor and the store handling functions
# for the implementation, see boom/core.py
from boom.core import boom_processor, close_stores

# From the selection_utils module, you import the get_selections function
# for the implementation, see boom/selection_utils.py
from boom.selection_utils import get_selections

# From the variables module, you import the dictionnary of variables (VARIABLES)
# note that you can also define your own HAPPy variable following the examples
# in the boom/variables.py file 
from boom.variables import VARIABLES

# From the database you import the list of physics processes
# for the implementation, see boom/database.oy
from boom.database import get_processes
physicsProcesses = get_processes()

# define the list of selections you want to look at
sels = get_selections(
    channels=('e1p', 'mu1p'), 
    categories=('vbf_loose', 'vbf_tight'),
    years=('15', '16'), 
    regions=('SR'),
    triggers=None)
# this example will only select lephad events with 1p taus 
# in the VBF signal regions for 2015 and 2016.
# each keyword argument accept a string or a tuple/ list of strings
# For the list of possible arguments, please have a look at boom/cuts/__init__.py

# this is the list of variables you want to look at 
variables = [
    VARIABLES['tau_0_pt'],
]

# define the processor with a list of physics processes, 
# a list of selections and a list of variables
processor = boom_processor(physicsProcesses, sels, variables)
# book all the histogram requests
processor.book()
# run! (will fill all the histograms)
# the second argument (here set to 3) is the number of cores you want to use
# user experience tends to show that asking for Ncores - 1 is the most adequate 
from multiprocessing import cpu_count
processor.run(n_cores=cpu_count() - 1)

# now you need to retrieve the histograms and plot them as you wish
# some examples can be found in boom/plotting.py

# with this line, we will retrieve the Ztt leading tau pt distribution
# for the full list of selections we ran over
h = processor.get_hist_physics_process('Ztt', sels, VARIABLES['tau_0_pt'])

# make a plot of the histogram we retrieved
from boom.plotting import basic_plot
basic_plot(sels, h, VARIABLES['tau_0_pt'])

## line to clean after yourself and properly close all the stores you opened
close_stores(physicsProcesses)
