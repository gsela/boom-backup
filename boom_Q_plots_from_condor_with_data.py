# python imports
import os
import sys, argparse
import ROOT
from multiprocessing import cpu_count


#command-line inputs for batch submission                                                                                                                                                                                                    
parser = argparse.ArgumentParser(description='Zll comparison script -- batch version')
parser.add_argument('rfile')
parser.add_argument('-v', '--variable', type=str, required=True, help="Variable to plot")
parser.add_argument('-c', '--channel', type=str, required=True, help="Channel to plot")
parser.add_argument('-b', '--boost', default=False, action='store_true', help="Do boost")
parser.add_argument('--z-flavour', default='ll', choices=['ll', 'ee', 'mumu'], help='Pick a Z->ll flavour')
parser.add_argument('--z-ewk-k-factor', default=1., type=float)
parser.add_argument('--oversampling', default=1, type=int)
args = parser.parse_args()

#local imports
from boom.plotting import make_Quentin_plot_from_condor_with_data
from boom.variables import VARIABLES
from boom.wsi_tools import workspace_input


_chan = '{}'.format(args.channel)

if args.boost is True:
    _cat = 'boost'
else:
    _cat = 'vbf'


wsi = workspace_input(ROOT.TFile(args.rfile))
_channel = 'chan_{}__cat_{}'.format(_chan, _cat)

# Ztt sherpa 
ztt_qcd_sr = wsi.channel(_channel + '__sr').sample('ZttQCD_truth_0')
ztt_qcd_sr+= wsi.channel(_channel + '__sr').sample('ZttQCD_truth_1')
ztt_qcd_sr+= wsi.channel(_channel + '__sr').sample('ZttQCD_truth_2')
ztt_qcd_sr+= wsi.channel(_channel + '__sr').sample('ZttQCD_truth_3')

ztt_ewk_sr = wsi.channel(_channel + '__sr').sample('ZttEWK_truth_0')
ztt_ewk_sr+= wsi.channel(_channel + '__sr').sample('ZttEWK_truth_1')
ztt_ewk_sr+= wsi.channel(_channel + '__sr').sample('ZttEWK_truth_2')
ztt_ewk_sr+= wsi.channel(_channel + '__sr').sample('ZttEWK_truth_3')

# Uncorrected Zll
if args.z_flavour in ('ll', 'ee',):
    ztt_qcd_cr = wsi.channel(_channel + '__cr_ztt_ee').sample('ZllQCD_truth_0')
    ztt_qcd_cr+= wsi.channel(_channel + '__cr_ztt_ee').sample('ZllQCD_truth_1')
    ztt_qcd_cr+= wsi.channel(_channel + '__cr_ztt_ee').sample('ZllQCD_truth_2')
    ztt_qcd_cr+= wsi.channel(_channel + '__cr_ztt_ee').sample('ZllQCD_truth_3')
    if args.z_flavour == 'll':
        ztt_qcd_cr+= wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllQCD_truth_0')
        ztt_qcd_cr+= wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllQCD_truth_1')
        ztt_qcd_cr+= wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllQCD_truth_2')
        ztt_qcd_cr+= wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllQCD_truth_3')
else:
    ztt_qcd_cr = wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllQCD_truth_0')
    ztt_qcd_cr+= wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllQCD_truth_1')
    ztt_qcd_cr+= wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllQCD_truth_2')
    ztt_qcd_cr+= wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllQCD_truth_3')

if args.z_flavour in ('ll', 'ee',):
    ztt_ewk_cr = wsi.channel(_channel + '__cr_ztt_ee').sample('ZllEWK_truth_0')
    ztt_ewk_cr+= wsi.channel(_channel + '__cr_ztt_ee').sample('ZllEWK_truth_1')
    ztt_ewk_cr+= wsi.channel(_channel + '__cr_ztt_ee').sample('ZllEWK_truth_2')
    ztt_ewk_cr+= wsi.channel(_channel + '__cr_ztt_ee').sample('ZllEWK_truth_3')
    if args.z_flavour == 'll':
        ztt_ewk_cr+= wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllEWK_truth_0')
        ztt_ewk_cr+= wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllEWK_truth_1')
        ztt_ewk_cr+= wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllEWK_truth_2')
        ztt_ewk_cr+= wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllEWK_truth_3')
else:
    ztt_ewk_cr = wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllEWK_truth_0')
    ztt_ewk_cr+= wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllEWK_truth_1')
    ztt_ewk_cr+= wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllEWK_truth_2')
    ztt_ewk_cr+= wsi.channel(_channel + '__cr_ztt_mumu').sample('ZllEWK_truth_3')

# Corrected Zll
if args.z_flavour in ('ll', 'ee',):
    ztt_qcd_cr_corr = wsi.channel(_channel + '__cr_ztt_ee_corr').sample('ZllQCD_truth_0')
    ztt_qcd_cr_corr+= wsi.channel(_channel + '__cr_ztt_ee_corr').sample('ZllQCD_truth_1')
    ztt_qcd_cr_corr+= wsi.channel(_channel + '__cr_ztt_ee_corr').sample('ZllQCD_truth_2')
    ztt_qcd_cr_corr+= wsi.channel(_channel + '__cr_ztt_ee_corr').sample('ZllQCD_truth_3')
    if args.z_flavour == 'll':
        ztt_qcd_cr_corr+= wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllQCD_truth_0')
        ztt_qcd_cr_corr+= wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllQCD_truth_1')
        ztt_qcd_cr_corr+= wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllQCD_truth_2')
        ztt_qcd_cr_corr+= wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllQCD_truth_3')
else:
    ztt_qcd_cr_corr = wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllQCD_truth_0')
    ztt_qcd_cr_corr+= wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllQCD_truth_1')
    ztt_qcd_cr_corr+= wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllQCD_truth_2')
    ztt_qcd_cr_corr+= wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllQCD_truth_3')

if args.z_flavour in ('ll', 'ee',):
    ztt_ewk_cr_corr = wsi.channel(_channel + '__cr_ztt_ee_corr').sample('ZllEWK_truth_0')
    ztt_ewk_cr_corr+= wsi.channel(_channel + '__cr_ztt_ee_corr').sample('ZllEWK_truth_1')
    ztt_ewk_cr_corr+= wsi.channel(_channel + '__cr_ztt_ee_corr').sample('ZllEWK_truth_2')
    ztt_ewk_cr_corr+= wsi.channel(_channel + '__cr_ztt_ee_corr').sample('ZllEWK_truth_3')
    if args.z_flavour == 'll':
        ztt_ewk_cr_corr+= wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllEWK_truth_0')
        ztt_ewk_cr_corr+= wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllEWK_truth_1')
        ztt_ewk_cr_corr+= wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllEWK_truth_2')
        ztt_ewk_cr_corr+= wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllEWK_truth_3')
else:
    ztt_ewk_cr_corr = wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllEWK_truth_0')
    ztt_ewk_cr_corr+= wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllEWK_truth_1')
    ztt_ewk_cr_corr+= wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllEWK_truth_2')
    ztt_ewk_cr_corr+= wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('ZllEWK_truth_3')

# apply ewk SF
ztt_ewk_sr.nominal.Scale(args.z_ewk_k_factor)
ztt_ewk_cr.nominal.Scale(args.z_ewk_k_factor)
ztt_ewk_cr_corr.nominal.Scale(args.z_ewk_k_factor)

if args.z_flavour in ('ll', 'ee',):
    data_cr = wsi.channel(_channel + '__cr_ztt_ee').sample('Data')
    if args.z_flavour == 'll':
        data_cr+= wsi.channel(_channel + '__cr_ztt_mumu').sample('Data')
else:
    data_cr = wsi.channel(_channel + '__cr_ztt_mumu').sample('Data')

if args.z_flavour in ('ll', 'ee',):
    data_cr_corr = wsi.channel(_channel + '__cr_ztt_ee_corr').sample('Data')
    if args.z_flavour == 'll':
        data_cr_corr+= wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('Data')
else:
    data_cr_corr = wsi.channel(_channel + '__cr_ztt_mumu_corr').sample('Data')

_lumi = wsi.channel(_channel + '__cr_ztt_ee_corr').lumi.GetBinContent(1) / float(args.oversampling)

ztt_sr = ztt_qcd_sr.nominal.Clone()
ztt_sr.Add(ztt_ewk_sr.nominal)
ztt_sr.SetLineColor(ztt_ewk_sr.nominal.GetLineColor())

ztt_cr = ztt_qcd_cr.nominal.Clone()
ztt_cr.Add(ztt_ewk_cr.nominal)
ztt_cr.SetLineColor(ztt_ewk_cr.nominal.GetLineColor())

ztt_cr_corr = ztt_qcd_cr_corr.nominal.Clone()
ztt_cr_corr.Add(ztt_ewk_cr_corr.nominal)
ztt_cr_corr.SetLineColor(ztt_ewk_cr_corr.nominal.GetLineColor())



make_Quentin_plot_from_condor_with_data(
    ztt_sr,
    ztt_cr,
    ztt_cr_corr,
    data_cr.nominal,
    data_cr_corr.nominal,
    VARIABLES['{}'.format(args.variable)],
    lumi=_lumi,
    oversampling=args.oversampling,
    title='channel {}, cat {}, EWK Z kfactor {}, Z{}'.format(_chan, _cat, args.z_ewk_k_factor, args.z_flavour),
    ratio=True)
