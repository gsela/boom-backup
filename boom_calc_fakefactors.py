# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.selection_utils import get_selections, filter_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.utils import hist_sum

#import logging
#logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes(no_fake=True)

do_closure = False
do_hadhad = False

if do_hadhad :
    _regions = (
        'W_lh_hhff',
        'W_lh_anti_tau_hhff',
        )
    variables = [
        VARIABLES['tau_0_pt_hhff_param_lh'],
        VARIABLES['tau_0_eta_hhff_param'],
    ]
    _categories = (
        'preselection',
    )

else:
    if do_closure :
        _regions = (
            'W_lh_same_sign',
            'W_lh_same_sign_anti_tau',
            'qcd_lh_same_sign',
            'qcd_lh_same_sign_anti_tau',
            )
    else:
        _regions = (
            'W_lh',
            'W_lh_anti_tau',
            'qcd_lh',
            'qcd_lh_anti_tau',
            )
    variables = [
        VARIABLES['tau_0_pt_ff_preselection'],
        VARIABLES['tau_0_pt_ff_boost'],
        VARIABLES['tau_0_pt_ff_vh'],
        VARIABLES['tau_0_pt_ff_vbf'],
    ]
    _categories = (
        'preselection',
        'boost',
        'vbf',
        'vh'
        )

### define your selection objects 
sels = get_selections(
    channels=('e1p', 'e3p', 'mu1p', 'mu3p'),
    categories=_categories,
    regions=_regions)

# define your list of variables
#### processor declaration, booking and running
if do_hadhad:
    processor = boom_processor(physicsProcesses, sels, (variables[0]), (variables[1]))
    processor.book_2D()
else:
    processor = boom_processor(physicsProcesses, sels, variables)
    processor.book()

processor.run(n_cores=cpu_count() - 1)

_prongness = (
    '1p',
    '3p',
    )

# plot making
data_minus_mc = {}
for prong in _prongness:
    for region in _regions:
        for category in _categories:
 
            if 'presel' in  category : var = VARIABLES['tau_0_pt_ff_preselection']
            elif 'boost' in category : var = VARIABLES['tau_0_pt_ff_boost']
            elif 'vh' in category : var = VARIABLES['tau_0_pt_ff_vh'] 
            else : var = VARIABLES['tau_0_pt_ff_vbf']   

            if '1p' in prong:
                channel = ('e1p','mu1p')
            else:
                channel = ('e3p','mu3p')
      
            sels_r = filter_selections(sels, channels=channel, regions=region, categories=category)
            hdata = processor.get_hist_physics_process('Data', sels_r, var) if not do_hadhad else processor.get_hist_physics_process_2D('Data', sels_r, variables[0], variables[1])
            data = hdata.Integral()

            h_mcs = []
            mc = 0
            for p in processor.mc_backgrounds:
                 h = processor.get_hist_physics_process(p.name, sels_r, var) if not do_hadhad else processor.get_hist_physics_process_2D(p.name, sels_r, variables[0], variables[1])
                 mc += h.Integral()
                 h_mcs.append(h)

            h_mc = hist_sum(h_mcs)
            h_data_minus_mc = hdata.Clone()
            h_data_minus_mc.Add(h_mc, -1)
            data_minus_mc[region+'_'+category+'_'+prong] = h_data_minus_mc

rname = 'same_sign_' if do_closure else ''

if do_hadhad:
    ff_transfers = [
        ('W_lh_hhff_'+rname+'preselection_1p','W_lh_'+rname+'anti_tau_hhff_preselection_1p'),
        ('W_lh_hhff_'+rname+'preselection_3p','W_lh_'+rname+'anti_tau_hhff_preselection_3p'),
    ]
else:
    ff_transfers = [
        ('W_lh_'+rname+'preselection_1p','W_lh_'+rname+'anti_tau_preselection_1p'),
        ('W_lh_'+rname+'boost_1p','W_lh_'+rname+'anti_tau_boost_1p'),
        ('W_lh_'+rname+'vh_1p','W_lh_'+rname+'anti_tau_vh_1p'),
        ('W_lh_'+rname+'vbf_1p','W_lh_'+rname+'anti_tau_vbf_1p'),
        ('W_lh_'+rname+'preselection_3p','W_lh_'+rname+'anti_tau_preselection_3p'),
        ('W_lh_'+rname+'boost_3p','W_lh_'+rname+'anti_tau_boost_3p'),
        ('W_lh_'+rname+'vh_3p','W_lh_'+rname+'anti_tau_vh_3p'),
        ('W_lh_'+rname+'vbf_3p','W_lh_'+rname+'anti_tau_vbf_3p'),
        ('qcd_lh_'+rname+'preselection_1p','qcd_lh_'+rname+'anti_tau_preselection_1p'),
        ('qcd_lh_'+rname+'boost_1p','qcd_lh_'+rname+'anti_tau_boost_1p'),
        ('qcd_lh_'+rname+'vh_1p','qcd_lh_'+rname+'anti_tau_vh_1p'),
        ('qcd_lh_'+rname+'vbf_1p','qcd_lh_'+rname+'anti_tau_vbf_1p'),
        ('qcd_lh_'+rname+'preselection_3p','qcd_lh_'+rname+'anti_tau_preselection_3p'),
        ('qcd_lh_'+rname+'boost_3p','qcd_lh_'+rname+'anti_tau_boost_3p'),
        ('qcd_lh_'+rname+'vh_3p','qcd_lh_'+rname+'anti_tau_vh_3p'),
        ('qcd_lh_'+rname+'vbf_3p','qcd_lh_'+rname+'anti_tau_vbf_3p'),
    ]

output_filename = 'FFs_lephad_closure.root' if do_closure else ('FFs_hadhad.root' if do_hadhad else 'FFs_lephad.root')

outfile = ROOT.TFile(output_filename,"recreate")
for (num, den) in ff_transfers:

    title = 'FF' 
    title += '_QCDCR' if 'qcd' in num else '_WCR'
    title += '_Presel' if 'presel' in num else '_Boosted' if 'boost' in num else '_VBF' if 'vbf' in num else '_VH'
    title += '_All_Comb_SLTandTLT' if do_hadhad else '_All_Comb_SLT'
    title += '_1prong' if '1p' in num else '_3prong'

    fake_factor = data_minus_mc[num].Clone()
    fake_factor.Divide(data_minus_mc[den])
    fake_factor.SetTitle(title)
    fake_factor.SetName(title)
    fake_factor_stat_up   = fake_factor.Clone(title+'_stat_up')
    fake_factor_stat_down = fake_factor.Clone(title+'_stat_down')

    if do_hadhad:
        for i in range(fake_factor.GetNbinsX() + 1):
            for j in range(fake_factor.GetNbinsY() + 1):
                fake_factor_stat_up.SetBinContent(i, j, fake_factor.GetBinContent(i, j) + fake_factor.GetBinError(i, j))
                fake_factor_stat_down.SetBinContent(i, j, fake_factor.GetBinContent(i, j) - fake_factor.GetBinError(i, j))
    else:
        for i in range(fake_factor.GetNbinsX() + 1):
            fake_factor_stat_up.SetBinContent(i, fake_factor.GetBinContent(i) + fake_factor.GetBinError(i))
            fake_factor_stat_down.SetBinContent(i, fake_factor.GetBinContent(i) - fake_factor.GetBinError(i))

    outfile.cd()
    final_fake_factor      = ROOT.TH2D() if do_hadhad else ROOT.TH1D()
    final_fake_factor_up   = ROOT.TH2D() if do_hadhad else ROOT.TH1D()
    final_fake_factor_down = ROOT.TH2D() if do_hadhad else ROOT.TH1D()

    fake_factor.Copy(final_fake_factor)
    fake_factor_stat_up.Copy(final_fake_factor_up)
    fake_factor_stat_down.Copy(final_fake_factor_down)

    final_fake_factor.Write()
    final_fake_factor_up.Write() 
    final_fake_factor_down.Write()

outfile.Close()

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
