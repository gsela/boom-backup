# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.selection_utils import get_selections, filter_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.utils import hist_sum

#import logging
#logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes(no_fake=True)

do_closure = False

_regions = (
    'iso_fact_lh_os_anti_tau_num',
    'iso_fact_lh_os_anti_tau_den',
    'iso_fact_lh_same_sign_anti_tau_num',
    'iso_fact_lh_same_sign_anti_tau_den',
    ) 

### define your selection objects 
sels = get_selections(
    channels=('e1p','e3p','mu1p','mu3p'),
    categories=('preselection'),     
    regions=_regions)

# define your list of variables
variables_x = [
    VARIABLES['tau_1_pt_isofac'],
]

variables_y = [
    VARIABLES['tau_1_eta_isofac'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables_x, variables_y)
processor.book_2D()
processor.run(n_cores=cpu_count() - 1)

_prongness = (
    '1p',
    '3p',
    )

# plot making
data_minus_mc = {}

for prong in _prongness:
    for region in _regions:
        if '1p' in prong:
            channel = ('e1p','mu1p')
        else:
            channel = ('e3p','mu3p')

        sels_r = filter_selections(sels, channels=channel, regions=region, categories=('preselection'))
        hdata = processor.get_hist_physics_process_2D('Data', sels_r, VARIABLES['tau_1_pt_isofac'], VARIABLES['tau_1_eta_isofac'])
    
        data = hdata.Integral()

        h_mcs = []
        mc = 0
        for p in processor.mc_backgrounds:
            h = processor.get_hist_physics_process_2D(p.name, sels_r, VARIABLES['tau_1_pt_isofac'], VARIABLES['tau_1_eta_isofac'])
            mc += h.Integral()
            h_mcs.append(h)

        h_mc = hist_sum(h_mcs)
        h_data_minus_mc = hdata.Clone()
        h_data_minus_mc.Add(h_mc, -1)
        data_minus_mc[region+'_'+prong] = h_data_minus_mc

outfile_name = 'data/IsoFactor_closure_lephad.root' if do_closure else 'data/IsoFactor_lephad.root'
outfile = ROOT.TFile(outfile_name,"recreate")

if do_closure:
    iso_factor_transfers = [
        ('iso_fact_lh_os_anti_tau_num_1p','iso_fact_lh_os_anti_tau_den_1p','iso_fact_lh_same_sign_anti_tau_num_1p','iso_fact_lh_same_sign_anti_tau_den_1p'),
        ('iso_fact_lh_os_anti_tau_num_3p','iso_fact_lh_os_anti_tau_den_3p','iso_fact_lh_same_sign_anti_tau_num_3p','iso_fact_lh_same_sign_anti_tau_den_3p')
    ]
else:
    iso_factor_transfers = [
        ('iso_fact_lh_same_sign_anti_tau_num_1p','iso_fact_lh_same_sign_anti_tau_den_1p','iso_fact_lh_os_anti_tau_num_1p','iso_fact_lh_os_anti_tau_den_1p'),
        ('iso_fact_lh_same_sign_anti_tau_num_3p','iso_fact_lh_same_sign_anti_tau_den_3p','iso_fact_lh_os_anti_tau_num_3p','iso_fact_lh_os_anti_tau_den_3p')
    ]


for (num1, den1, num2, den2) in iso_factor_transfers:
    title = 'Iso_Factor_Presel_All_Comb_SLT_1prong' if '1p' in num1 else 'Iso_Factor_Presel_All_Comb_SLT_3prong'
    iso_factor = data_minus_mc[num1].Clone()
    iso_factor.Divide(data_minus_mc[den1])
    iso_factor.SetName(title)
    iso_factor.SetTitle(title)

    # calc stat uncertainty
    iso_factor_stat_up   = iso_factor.Clone(title+'_stat_up')
    iso_factor_stat_down = iso_factor.Clone(title+'_stat_down')
    for i in range(iso_factor.GetNbinsX()+1):
        for j in range(iso_factor.GetNbinsY()+1):
            iso_factor_stat_up.SetBinContent(i,j, iso_factor.GetBinContent(i,j) + iso_factor.GetBinError(i,j))
            iso_factor_stat_down.SetBinContent(i,j, iso_factor.GetBinContent(i,j) - iso_factor.GetBinError(i,j))

    # calc syst uncertainty
    iso_factor_syst_up   = iso_factor.Clone(title+'_syst_up')
    iso_factor_syst_down = iso_factor.Clone(title+'_syst_down')
    aux_hist = data_minus_mc[num2].Clone()
    aux_hist.Divide(data_minus_mc[den2])

    for j in range(iso_factor.GetNbinsX()+1):
        for k in range(iso_factor.GetNbinsY()+1):
            iso_factor_syst_up.SetBinContent(j,k, iso_factor.GetBinContent(j,k) + abs( iso_factor.GetBinContent(j,k) - aux_hist.GetBinContent(j,k)))
            iso_factor_syst_down.SetBinContent(j,k, iso_factor.GetBinContent(j,k) - abs( iso_factor.GetBinContent(j,k) - aux_hist.GetBinContent(j,k)))

    outfile.cd()
    iso_factor.Write()   
    iso_factor_stat_up.Write()
    iso_factor_stat_down.Write()
    iso_factor_syst_up.Write()
    iso_factor_syst_down.Write()

outfile.Close()
print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
