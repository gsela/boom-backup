# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.extern.tabulate import tabulate

# Switch on debug output
# import logging
# logging.root.setLevel( logging.DEBUG )

def _min_max_err(yields):
    r_low = yields[0][0] / yields[1][0]
    r_high = yields[2][0] / yields[1][0]
    return min(r_low, r_high), max(r_low, r_high)

def _plot(categories, yields_zll, yields_ztt, uncert_source='PDF', channel_name='hadhad'):
    import uuid
    _template = ROOT.TH1F(uuid.uuid4().hex, "", len(categories), 0, len(categories))
    for _ibin in xrange(_template.GetNbinsX()):
        _template.GetXaxis().SetBinLabel(_ibin + 1, categories[_ibin])
        _template.SetBinContent(_ibin + 1, 1)
    _template.GetXaxis().SetTitle('Category')
    _template.GetYaxis().SetTitle('Relative Uncertainty')
    _template.GetYaxis().SetRangeUser(0.7, 1.3)
    _template.SetLabelSize(0.8 * _template.GetLabelSize())
    _template.SetTitleOffset(0.8 * _template.GetTitleOffset('Y'), 'Y')

    _gr_zll = ROOT.TGraphAsymmErrors()
    _gr_ztt = ROOT.TGraphAsymmErrors()
    _gr_ratio = ROOT.TGraphAsymmErrors()

    _gr_zll_nom_stat_err  = ROOT.TGraphErrors()
    _gr_zll_high_stat_err = ROOT.TGraphErrors()
    _gr_zll_low_stat_err  = ROOT.TGraphErrors()
    _gr_ztt_nom_stat_err  = ROOT.TGraphErrors()
    _gr_ztt_high_stat_err = ROOT.TGraphErrors()
    _gr_ztt_low_stat_err  = ROOT.TGraphErrors()

    for i_l, (_y_zll, _y_ztt) in enumerate(zip(yields_zll, yields_ztt)):
        _gr_ztt.SetPoint(i_l, i_l + 0.3, 1)
        _gr_zll.SetPoint(i_l, i_l + 0.5, 1)
        _gr_ratio.SetPoint(i_l, i_l + 0.7, 1)

        _low, _high = _min_max_err(_y_zll)
        _gr_zll.SetPointError(i_l, 0.1, 0.1, 1 - _low, _high - 1)

        _low, _high = _min_max_err(_y_ztt)
        _gr_ztt.SetPointError(i_l, 0.1, 0.1, 1 - _low, _high - 1)

        _y_ratios = [
            (_y_ztt[0][0] / _y_zll [0][0], 0),
            (_y_ztt[1][0] / _y_zll [1][0], 0),
            (_y_ztt[2][0] / _y_zll [2][0], 0),
            ]
        _low, _high = _min_max_err(_y_ratios)
        _gr_ratio.SetPointError(i_l, 0.1, 0.1, 1 - _low, _high - 1)
        
        _gr_ztt_low_stat_err  .SetPoint(i_l, i_l + 0.3, 1)
        _gr_ztt_nom_stat_err  .SetPoint(i_l, i_l + 0.4, 1)
        _gr_ztt_high_stat_err .SetPoint(i_l, i_l + 0.5, 1)
        _gr_zll_low_stat_err  .SetPoint(i_l, i_l + 0.6, 1)
        _gr_zll_nom_stat_err  .SetPoint(i_l, i_l + 0.7, 1)
        _gr_zll_high_stat_err .SetPoint(i_l, i_l + 0.8, 1)

        _gr_zll_low_stat_err  .SetPointError(i_l, 0.05,  _y_zll[0][1] / _y_zll[0][0])
        _gr_zll_nom_stat_err  .SetPointError(i_l, 0.05,  _y_zll[1][1] / _y_zll[1][0])
        _gr_zll_high_stat_err .SetPointError(i_l, 0.05,  _y_zll[2][1] / _y_zll[2][0])
        _gr_ztt_low_stat_err  .SetPointError(i_l, 0.05,  _y_ztt[0][1] / _y_ztt[0][0])
        _gr_ztt_nom_stat_err  .SetPointError(i_l, 0.05,  _y_ztt[1][1] / _y_ztt[1][0])
        _gr_ztt_high_stat_err .SetPointError(i_l, 0.05,  _y_ztt[2][1] / _y_ztt[2][0])

    _gr_zll.SetFillColor(ROOT.kRed)
    _gr_ztt.SetFillColor(ROOT.kGreen)
    _gr_ratio.SetFillColor(ROOT.kBlack)
    _gr_zll_nom_stat_err  .SetFillColor(ROOT.kRed)
    _gr_zll_high_stat_err .SetFillColor(ROOT.kRed)
    _gr_zll_low_stat_err  .SetFillColor(ROOT.kRed)
    _gr_ztt_nom_stat_err  .SetFillColor(ROOT.kGreen)
    _gr_ztt_high_stat_err .SetFillColor(ROOT.kGreen)
    _gr_ztt_low_stat_err  .SetFillColor(ROOT.kGreen)

    _gr_zll_high_stat_err .SetFillStyle(3001)
    _gr_zll_low_stat_err  .SetFillStyle(3001)
    _gr_ztt_high_stat_err .SetFillStyle(3001)
    _gr_ztt_low_stat_err  .SetFillStyle(3001)





    c = ROOT.TCanvas(uuid.uuid4().hex, "", 900, 800)
    c.SetTopMargin(0.12)
    c.SetLeftMargin(0.7 * c.GetLeftMargin())
    c.Divide(1, 2, 0, 0)
    #
    _p1 = c.cd(1)
    _template.SetLineWidth(0)
    _template.Draw('HIST')
    _gr_zll.Draw('sameE2')
    _gr_ztt.Draw('sameE2')
    _gr_ratio.Draw('sameE2')
    _template.Draw('sameHIST')
    _p1.SetGridx()
    #
    _p2 = c.cd(2)
    _template_2 = _template.Clone()
    _template_2.GetYaxis().SetRangeUser(0.8, 1.2)
    _template_2.GetYaxis().SetTitle('Statistical Uncertainty')
    _template_2.Draw('HIST')
    _gr_zll_nom_stat_err  .Draw('sameE2')
    _gr_zll_high_stat_err .Draw('sameE2')    
    _gr_zll_low_stat_err  .Draw('sameE2')
    _gr_ztt_nom_stat_err  .Draw('sameE2')
    _gr_ztt_high_stat_err .Draw('sameE2')
    _gr_ztt_low_stat_err  .Draw('sameE2')
    _p2.SetGridx()

    c.cd(0)
    c.RedrawAxis()
    
    _label = ROOT.TText(
        c.GetLeftMargin(),
        1 - c.GetTopMargin() / 2,
        'Source: {} - Channel: {}'.format(uncert_source, channel_name))
    _label.SetNDC(True)
    _label.SetTextSize(0.6 * _label.GetTextSize())
    _label.Draw()

    c.cd(1)
    _legend = ROOT.TLegend(0.15, 0.85, 0.6, 1.)
    _legend.AddEntry(_gr_zll, 'Z#rightarrowee yields', 'f')
    _legend.AddEntry(_gr_ztt, 'Z#rightarrow#tau#tau yields ', 'f')
    _legend.AddEntry(_gr_ratio, 'Z#rightarrow#tau#tau / Z#rightarrowee ratio', 'f')
    _legend.SetNColumns(3)
    _legend.SetFillStyle(0)
    _legend.SetFillColor(0)
    _legend.SetBorderSize(0)
    _legend.Draw()

    c.cd(2)
    _legend_2 = ROOT.TLegend(0.15, 0.85, 0.9, 1.)
    _legend_2.AddEntry(_gr_zll_low_stat_err, 'Z#rightarrowee low', 'f')  
    _legend_2.AddEntry(_gr_zll_nom_stat_err, 'Z#rightarrowee nom', 'f')    
    _legend_2.AddEntry(_gr_zll_high_stat_err , 'Z#rightarrowee high', 'f')  
    _legend_2.AddEntry(_gr_ztt_low_stat_err  , 'Z#rightarrow#tau#tau low', 'f') 
    _legend_2.AddEntry(_gr_ztt_nom_stat_err , 'Z#rightarrow#tau#tau nom ', 'f') 
    _legend_2.AddEntry(_gr_ztt_high_stat_err , 'Z#rightarrow#tau#tau high', 'f') 
    _legend_2.SetNColumns(6)
    _legend_2.SetFillStyle(0)
    _legend_2.SetFillColor(0)
    _legend_2.SetBorderSize(0)
    _legend_2.Draw()

    c.SaveAs('zllztt_variation_hh_{}.pdf'.format(syst_name))



# retrieve physics processes
physicsProcesses = get_processes(stxs_stage1=False, squash=True)
ztt_processes = filter(lambda p: p.name in ('ZttQCD', 'ZllQCD'), physicsProcesses)

# syst_name = 'theory_ztt_lhe3weight_mur2_muf2_pdf261000'
# syst_name = 'theory_ztt_lhe3weight_mur2_muf1_pdf261000'
# syst_name = 'theory_ztt_lhe3weight_mur1_muf2_pdf261000'
# syst_name = 'theory_ztt_lhe3weight_mur1_muf05_pdf261000'
# syst_name = 'theory_ztt_lhe3weight_mur05_muf1_pdf261000'
# syst_name = 'theory_ztt_alphaS'
# syst_name = 'theory_ztt_PDF_mmht'
# syst_name = 'theory_ztt_PDF_ct14'
syst_name = 'theory_ztt_PDF'
# syst_name = 'theory_ztt_lhe3weight_mur2_muf2_pdf261000'
_categories = (
#     'vbf_0',
#     'vbf_1',
#     'boost_0_1J',
#     'boost_1_1J',
    'boost_2_1J',
    'boost_3_1J',
#     'boost_0_ge2J',
    'boost_1_ge2J',
    'boost_2_ge2J',
    'boost_3_ge2J',
    )

### define your selection objects 
sels = get_selections(
    channels=('1p1p', '1p3p', '3p1p', '3p3p'),
 #    years=('18'),
    categories=_categories,
    regions='SR')

sels += get_selections(
    channels=('ee'),#, 'mumu'),
#     years=('17', '18'),
    categories=_categories,
    regions='Ztt_cr_hh')


# define your list of variables
variables = [
    VARIABLES['norm'],
    ]

# #### processor declaration, booking and running
processor = boom_processor(ztt_processes, sels, variables, systematic_names=syst_name)
processor.book()
processor.run(n_cores=cpu_count() - 1)

from boom.selection_utils import filter_selections

yields_zll = []
yields_ztt = []


for _cat in _categories:
    _sels_hh = filter_selections(sels, categories=_cat, channels=('1p1p', '1p3p', '3p1p', '3p3p'))
    _sels_zcr = filter_selections(sels, categories=_cat, channels=('ee', 'mumu'))


    variable = variables[0]
    h_ztt_hh_nom = processor.get_hist_physics_process('ZttQCD', _sels_hh, variable)
    h_ztt_hh_high = processor.get_hist_physics_process('ZttQCD', _sels_hh, variable, syst_name=syst_name, syst_type='up')
    h_ztt_hh_low = processor.get_hist_physics_process('ZttQCD', _sels_hh, variable, syst_name=syst_name, syst_type='down')

    ztt_hh_nom = h_ztt_hh_nom .GetBinContent(1)
    ztt_hh_high = h_ztt_hh_high.GetBinContent(1)
    ztt_hh_low = h_ztt_hh_low .GetBinContent(1)

    ztt_hh_err_nom = h_ztt_hh_nom .GetBinError(1)
    ztt_hh_err_high = h_ztt_hh_high.GetBinError(1)
    ztt_hh_err_low = h_ztt_hh_low .GetBinError(1)

    h_ztt_zcr_nom = processor.get_hist_physics_process('ZllQCD', _sels_zcr, variable)
    h_ztt_zcr_high = processor.get_hist_physics_process('ZllQCD', _sels_zcr, variable, syst_name=syst_name, syst_type='up')
    h_ztt_zcr_low = processor.get_hist_physics_process('ZllQCD', _sels_zcr, variable, syst_name=syst_name, syst_type='down')

    ztt_zcr_nom = h_ztt_zcr_nom .GetBinContent(1)
    ztt_zcr_high= h_ztt_zcr_high.GetBinContent(1)
    ztt_zcr_low = h_ztt_zcr_low .GetBinContent(1)

    ztt_zcr_err_nom = h_ztt_zcr_nom .GetBinError(1)
    ztt_zcr_err_high= h_ztt_zcr_high.GetBinError(1)
    ztt_zcr_err_low = h_ztt_zcr_low .GetBinError(1)

#     # UGLY FIX
#     ztt_zcr_delta = ztt_zcr_high - ztt_zcr_low
#     ztt_zcr_high = ztt_zcr_nom + ztt_zcr_delta / 2.
#     ztt_zcr_low = ztt_zcr_nom - ztt_zcr_delta / 2. 

    yields_zll += [[
            (ztt_zcr_low, ztt_zcr_err_low), 
            (ztt_zcr_nom, ztt_zcr_err_nom), 
            (ztt_zcr_high, ztt_zcr_err_high)
            ]]
    yields_ztt += [[
            (ztt_hh_low, ztt_hh_err_low),
            (ztt_hh_nom, ztt_hh_err_nom),
            (ztt_hh_high,ztt_hh_err_low),
            ]]


    _ratio_nom = ztt_hh_nom / ztt_zcr_nom
    _ratio_low = ztt_hh_low / ztt_zcr_low
    _ratio_high = ztt_hh_high / ztt_zcr_high

    _delta_ztt = abs(ztt_hh_high - ztt_hh_low) / (2 * ztt_hh_nom)
    _delta_zll = abs(ztt_zcr_high - ztt_zcr_low) / (2 * ztt_zcr_nom)
    _delta_ratio = abs(_ratio_high - _ratio_low) / (2 * _ratio_nom)
 

    print _cat
    lines = [
        ['Ztt', ztt_hh_low, ztt_hh_nom, ztt_hh_high, _delta_ztt],
        ['Zll', ztt_zcr_low, ztt_zcr_nom, ztt_zcr_high, _delta_zll],
        ['Ztt/Zll', _ratio_low, _ratio_nom, _ratio_high, _delta_ratio],
        ['Ztt stat error', ztt_hh_err_low, ztt_hh_err_nom, ztt_hh_err_high, '-'],
        ['Zll stat error', ztt_zcr_err_low, ztt_zcr_err_nom, ztt_zcr_err_high, '-']
    ]
    print tabulate(lines, headers=['sample', 'low', 'nominal', 'high', '|high - low| / (2*nominal)'], floatfmt='.3f')
    print

_plot(_categories, yields_zll, yields_ztt, uncert_source=syst_name)

print 'closing stores...'
close_stores(ztt_processes)
print 'done'
