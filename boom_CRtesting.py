# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_zllvr_proxy_comparison_plot, make_zllvr_proxy_comparison_plot_2D
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# retrieve physics processes
physicsProcesses = get_processes()
physicsProcesses = filter(lambda p: 'Ztt' in p.name or 'Zll' in p.name, physicsProcesses)

# choose which channel to compare with corresponding Zll VR proxy (default: hadhad)
do_ll = False
do_lh = True

# choose which region to compare with corresponding Zll VR proxy (default : VBF)
do_boost = True

years = ('17')#('15', '16', '17')

if do_ll:
   chs = ('emu', 'mue')
   zll_region = ('Ztt_cr_corr_ll')
elif do_lh :
   chs = ('e1p', 'e3p', 'mu1p', 'mu3p')
   zll_region = ('Ztt_cr_corr_lh')
else :
   chs = ('1p1p','1p3p','3p1p','3p3p') 
   zll_region = ('Ztt_cr_corr_hh')

if do_boost: 
     sels_sr =  get_selections(
        channels=chs, 
        years=years, 
        regions='SR', 
        categories=('boost'))
     sels_zllvr = get_selections(
        years=years, 
        regions=zll_region, 
        categories=('boost'))
else : 
     if (do_lh or do_ll) :  
         sels_sr = get_selections(
            channels=chs, 
            years=years, 
            regions='SR', 
            categories=('vbf_tight','vbf_loose'))
         sels_zllvr = get_selections(
            years=years, 
            regions=zll_region, 
            categories=('vbf_tight','vbf_loose'))
     else : 
         sels_sr = get_selections(
            channels=chs, 
            years=years, 
            regions='SR', 
            categories=('vbf_tight','vbf_loose','vbf_lowdr'))
         sels_zllvr = get_selections(
            years=years, 
            regions=zll_region, 
            categories=('vbf_tight','vbf_loose','vbf_lowdr'))

sels = sels_sr + sels_zllvr

### define your list of variables
variables_x = [
#   VARIABLES['pt_total'],
#   VARIABLES['mjj'],
#   VARIABLES['jets_deta'],
#   VARIABLES['jets_eta_prod'],
#   VARIABLES['var_ptjj'],
#   VARIABLES['vbf_tagger'],
   VARIABLES['tau_0_pt'],
   VARIABLES['tau_1_pt'],
   VARIABLES['tau_0_eta'],
   VARIABLES['tau_1_eta'],
#   VARIABLES['met'],
#   VARIABLES['ditau_dr'],
#   VARIABLES['met_centrality'],
#   VARIABLES['taus_pt_ratio'],
#   VARIABLES['higgs_pt'],
#      VARIABLES['met'],
#    VARIABLES['met_centrality'],
#     VARIABLES['ditau_dr'],
#     VARIABLES['higgs_pt'],
#     VARIABLES['higgs_pt_res']
#      VARIABLES['mjj'],
#      VARIABLES['jets_deta'],
   VARIABLES['tau_0_pt'],
   VARIABLES['tau_1_pt'],
#    VARIABLES['jet_0_pt'],
#    VARIABLES['jet_1_pt'],
]

variables_y = [
   VARIABLES['tau_0_pt'],
   VARIABLES['tau_1_pt'],
]


#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables_x, variables_y)
processor.book_2D()
#processor = boom_processor(physicsProcesses, sels, variables_x)
#processor.book()

processor.run(n_cores=cpu_count() - 1)

title = 'LepLep' if do_ll else 'LepHad' if do_lh else 'HadHad'
title += '_Boost SRs' if do_boost else '_VBF SRs'

### plot making
for varx in variables_x:
#   make_zllvr_proxy_comparison_plot(processor, sels_sr, sels_zllvr, varx, title=title)
   for vary in variables_y:
      make_zllvr_proxy_comparison_plot_2D(processor, sels_sr, sels_zllvr, varx, vary, title=title)

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
