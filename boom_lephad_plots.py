# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes


#import logging
#logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes(stxs='stxs0', squash=True)

_channels = (
    'e1p',
    'e3p',
    'mu1p',
    'mu3p',
    )

_years = (
    '15',
    '16',
    '17',
    '18',
)

_categories = (
    'preselection',
    'boost',
    'vh',
    'vbf',
)

_regions = (
    'SR',
    #'same_sign_SR', # use it for do FF closure plots
    'top', # use it for Top control region
    'W_lh', # use it for W control region 
    'qcd_lh', # use it for QCD control region
    )

### define your selection objects 
sels = get_selections(
    channels=_channels, 
    years=_years,
    categories=_categories,
    regions=_regions)


# define your list of variables
_variables = [
#     VARIABLES['mmc_mlm_m'],
#     VARIABLES['tau_0_pt'],
#     VARIABLES['tau_1_pt'],
#     VARIABLES['vbf_tagger'],
     VARIABLES['higgs_pt'],
     VARIABLES['n_jets'],
     VARIABLES['ditau_dr'],
     VARIABLES['ditau_deta'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, _variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

# plot making
for reg in _regions:
    for var in _variables:
        for cat in _categories:
            make_data_mc_plot(processor, sels, var, categories=cat, regions=reg)

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
