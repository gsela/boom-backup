# python imports
import os
import sys, argparse
import ROOT
from multiprocessing import cpu_count


#command-line inputs for batch submission                                                                                                                                                                                                     
parser = argparse.ArgumentParser(description='Zll comparison script -- batch version')
parser.add_argument('rfile')
parser.add_argument('-v', '--variable', type=str, required=True, help="Variable to plot")
parser.add_argument('-c', '--channel', type=str, required=True, help="Channel to plot")
parser.add_argument('-b', '--boost', default=False, action='store_true', help="Do boost")
# need to do this so --help works                                                                                                                                                                                                             
args = parser.parse_args()

#local imports
from boom.plotting import make_Quentin_plot_from_condor
from boom.variables import VARIABLES
from boom.wsi_tools import workspace_input


_chan = '{}'.format(args.channel)

if args.boost is True:
    _cat = 'boost'
else:
    _cat = 'vbf'


wsi = workspace_input(ROOT.TFile(args.rfile))
_channel = 'chan_{}__cat_{}'.format(_chan, _cat)

ztt_sr = wsi.channel(_channel + '__sr').sample('ZttQCD_truth_0')
ztt_sr+= wsi.channel(_channel + '__sr').sample('ZttQCD_truth_1')
ztt_sr+= wsi.channel(_channel + '__sr').sample('ZttQCD_truth_2')
ztt_sr+= wsi.channel(_channel + '__sr').sample('ZttQCD_truth_3')

ztt_cr = wsi.channel(_channel + '__cr_ztt_ee').sample('ZllQCD_truth_0')
ztt_cr+= wsi.channel(_channel + '__cr_ztt_ee').sample('ZllQCD_truth_1')
ztt_cr+= wsi.channel(_channel + '__cr_ztt_ee').sample('ZllQCD_truth_2')
ztt_cr+= wsi.channel(_channel + '__cr_ztt_ee').sample('ZllQCD_truth_3')

ztt_cr_corr = wsi.channel(_channel + '__cr_ztt_ee_corr').sample('ZllQCD_truth_0')
ztt_cr_corr+= wsi.channel(_channel + '__cr_ztt_ee_corr').sample('ZllQCD_truth_1')
ztt_cr_corr+= wsi.channel(_channel + '__cr_ztt_ee_corr').sample('ZllQCD_truth_2')
ztt_cr_corr+= wsi.channel(_channel + '__cr_ztt_ee_corr').sample('ZllQCD_truth_3')


make_Quentin_plot_from_condor(
    ztt_sr.nominal,
    ztt_cr.nominal,
    ztt_cr_corr.nominal,
    VARIABLES['{}'.format(args.variable)],
    title='channel {}, category {}'.format(_chan, _cat),
    ratio=True)
