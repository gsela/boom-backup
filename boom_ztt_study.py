# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# Switch on debug output
# import logging
# logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes(stxs='stxs0', squash=True)
ztt_processes = filter(lambda p: 'Ztt' in p.name, physicsProcesses)

#syst_name = 'ztheory_ckkw'
syst_name = 'theory_ztt_lhe3weight_mur05_muf05_pdf261000'

_categories = (
     'preselection',
     'boost',
     'vbf',
     'vh',
    )

### define your selection objects 
sels = get_selections(
    channels=(
        'emu', 'mue',
        'e1p', 'e3p', 'mu1p', 'mu3p',
        '1p1p', '1p3p', '3p1p', '3p3p'),
    years=('15', '16', '17', '18'), 
    categories=_categories,
    regions='SR')


# define your list of variables
variables = [
    VARIABLES['mmc_mlm_m'],
]

# #### processor declaration, booking and running
processor = boom_processor(ztt_processes, sels, variables, systematic_names=syst_name)
processor.book()
processor.run(n_cores=cpu_count() - 1)

# plot making
import ROOT
stop_watch = ROOT.TStopwatch()

from boom.plotting import make_sys_nom_plot
for _cat in _categories:
    for var in variables:
        make_sys_nom_plot(processor, sels, var, syst_name, categories=_cat)


stop_watch.Stop()
stop_watch.Print()

print 'closing stores...'
close_stores(ztt_processes)
print 'done'
