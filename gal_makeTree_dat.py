import gconfig
gconfig.UseRatio=0
gconfig.WhoSignal=1
gconfig.SaveHist=0

gconfig.ExtraCuts=0
gconfig.CreateSmallTree=1
gconfig.SmallTreeFolder='Data2'
gconfig.UseFilteredTree=1
# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import g_plot_compare_EB ,g_plot_mattias,g_plot_mattias_mc
from boom.selection_utils import get_selections ,print_selection
from boom.variables import VARIABLES
from boom.database import get_processes


#import logging
#logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
#physicsProcesses = get_processes(data_only=True, no_fake=True, stxs='stxs0', squash=True) #what is that?
physicsProcesses = get_processes(data_only=True) 


#define name and saving folder:
# save_to_dir="~/boom/plots/All_sym_results"
# start_title='SymCorr_Data'
# file_to_save_hists='All_sym_results/'+start_title+'.root'

_channels = (
    'e1p',
    'e3p',
    'mu1p',
    'mu3p',
    )

_years = (
      '15',
      '16',
      '17',
      '18',
)

_categories = (
    #'boost',
    #'vh',
    #'vbf_0',
    # 'vbf_1',
     'preselection',
)

_regions = (
     #'all_fakes',
    'SR',
    #'same_sign_SR', # use it for do FF closure plots
    #'top', # use it for Top control region
    # 'W_lh', # use it for W control region 
    # 'qcd_lh', # use it for QCD control region
    )

### define your selection objects 
sels = get_selections(
    channels=_channels, 
    years=_years,
    categories=_categories,
    regions=_regions
    )

#print_selection(sels)
# define your list of variables
_variables = [
    VARIABLES['tau_had_eta'],
] 

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, _variables, do_fake_mc_subtraction=False)
processor.book()
processor.run(n_cores=cpu_count() - 1)


# plot making. Do we want it differently for eath reg\category?
# for reg in _regions:
#     for var in _variables:
#         for cat in _categories:
#             t=start_title+'_'+str(cat)+'_'+str(reg)+'_'+str(var.name)
#             g_plot_mattias(processor, sels, var, cat, reg, title=t,plotdir=save_to_dir, save_to_file=file_to_save_hists)
    


print 'closing stores...'
close_stores(physicsProcesses)
print 'done'