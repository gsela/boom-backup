#MC or data? set to 1 or 0 accordingly
import gconfig
gconfig.isMC=1
gconfig.truthMatch=0
gconfig.UseRatio=1
gconfig.WhoSignal=2
gconfig.SaveHist=0
gconfig.isAC=1

# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import g_plot_compare_EB ,g_plot_mattias,g_plot_mattias_mc
from boom.selection_utils import get_selections ,print_selection
from boom.variables import VARIABLES
from boom.database import get_processes


# import logging
# logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
#physicsProcesses = get_processes(data_only=True, no_fake=True, stxs='stxs0', squash=True) #what is that?
#mc_processes=['ZttQCD', 'ZttEWK','Diboson','Top','W_Jets','ZllQCD','ZllEWK','ggHtt', 'ggHWW', 'VBFHtt', 'VBFHWW','WHtt', 'ZHtt', 'ttHtt']
mc_processes=['ZttQCD', 'ZttEWK','Diboson','Top','W_Jets','ZllQCD','ZllEWK','ggHtt', 'ggHWW', 'VBFHtt', 'VBFHWW','WHtt', 'ZHtt', 'ttHtt','LFVggHETAU', 'LFVVBFHETAU','LFVggHMUTAU', 'LFVVBFHMUTAU']
#mc_processes=['ggHtt', 'ggHWW', 'VBFHtt', 'VBFHWW','WHtt', 'ZHtt', 'ttHtt']
#mc_processes=['LFVggHETAU', 'LFVVBFHETAU', 'LFVWHETAU', 'LFVZHETAU', 'LFVggHMUTAU', 'LFVVBFHMUTAU', 'LFVWHMUTAU', 'LFVZHMUTAU']
physicsProcesses = get_processes(mc_only=True, mc_only_wanted_processes=True, wanted_processes=mc_processes) 


#define name and saving folder:
save_to_dir="~/boom/plots/AC"
start_title='AC_mc16e_etauSig_nonVBF_Effcorr'
#start_title='AC_newPMG_BDTSF_BaseEtaCut_mc16a_nonVBF_W_Jets'
file_to_save_hists=start_title+'.root'

_channels = (
    'e1p',
    'e3p',
    'mu1p',
    'mu3p',
    )

_years = (
    #  '15',
    #  '16',
    #   '17',
      '18',
)

_categories = (
    #'boost',
    #'vh',
     'vbf_0',
    #'preselection',
)

_regions = (
     'SR',
    # #'same_sign_SR', # use it for do FF closure plots
    #'top', # use it for Top control region
    # 'W_lh', # use it for W control region 
    # 'qcd_lh', # use it for QCD control region
    )

### define your selection objects 
sels = get_selections(
    channels=_channels, 
    years=_years,
    categories=_categories,
    regions=_regions
    )

#print_selection(sels)
# define your list of variables
_variables = [
    # VARIABLES['tau_had_pt'],
     VARIABLES['tau_had_eta'],
    # VARIABLES['tau_had_phi'],
    # VARIABLES['lepton_pt'],
    # VARIABLES['lepton_eta'],
    # VARIABLES['lepton_phi'],

    # VARIABLES['ditau_deta'],
    # VARIABLES['ditau_dphi'],
    # VARIABLES['n_jets'],
    # VARIABLES['visible_mass_extended'],
    # VARIABLES['mll'],

    # VARIABLES['colinear_mass_lfv'],
    # VARIABLES['met_fine'],
    # VARIABLES['transverse_mass_l1'],
    # VARIABLES['transverse_mass_l0'],
    # VARIABLES['dphi_MET_0'],
    # VARIABLES['dphi_MET_1'],
    # VARIABLES['met_phi'],
    # VARIABLES['met_soft_term'],
    # VARIABLES['met_delta'],

    # VARIABLES['met_manual_pt'],
    # VARIABLES['met_manual_phi'],
    # VARIABLES['met_manual_pt_plus_soft'],
    # VARIABLES['dphi_tau_manual_met'],
    # VARIABLES['dphi_lep_manual_met'],
    # VARIABLES['met_manual_trans_lep'],
    # VARIABLES['met_manual_trans_tau'],
    # VARIABLES['met_manual_coll'],
     VARIABLES['tau_bdt']
] 

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, _variables, do_fake_mc_subtraction=False)
processor.book()
processor.run(n_cores=cpu_count() - 1)


# plot making. Do we want it differently for eath reg\category?
for reg in _regions:
    for var in _variables:
        for cat in _categories:
            t=start_title+'_'+str(var.name)
            g_plot_mattias_mc(processor, sels, var, cat, reg, title=t,samps=mc_processes,plotdir=save_to_dir, save_to_file=file_to_save_hists, Sig=2)

#for mutauSig plots:
for reg in _regions:
    for var in _variables:
        for cat in _categories:
            t=start_title+'_'+str(var.name)+'_Sig_etau'
            g_plot_mattias_mc(processor, sels, var, cat, reg, title=t,samps=mc_processes,plotdir=save_to_dir, save_to_file=file_to_save_hists, Sig=1)    


print 'closing stores...'
close_stores(physicsProcesses)
print 'done'

