# python imports
import os
import ROOT

# happy imports
from happy.xmlParser import parse, physicsProcesses

#local imports
from boom.core import boom_processor
from boom.plotting import make_data_mc_plot
from boom import scheduler
from boom.variables import VARIABLES
from boom.database import get_processes

# mandatory HAPPy lines - Start by importing samples, then create the hisogram stores
parse('./data/allchannels-dataset-R21-MC16a.xml')

physicsProcesses2 = get_processes(stash=False)
print physicsProcesses
print physicsProcesses2

for i in range(len(physicsProcesses)):
    print "Running over physicsProcess: "+physicsProcesses[i].name

    # Construct dictionary
    referenceDict = {}
    for entry in physicsProcesses[i].datasets:
        referenceDict[entry.name] = entry

    # Perform comparison
    referenceDict2 = {}
    for entry in physicsProcesses2[i].datasets:
        referenceDict2[entry.name] = entry
        # Check for missing entry
        if entry.name not in referenceDict:
            print "Dataset Missing from XML: "+entry.name
        elif abs((entry.effectiveCrossSection / referenceDict[entry.name].effectiveCrossSection) - 1) > 0.15:
            print "Cross-section mismatch: "+entry.name+" "+str(entry.effectiveCrossSection)+" != "+str(referenceDict[entry.name].effectiveCrossSection)
        # k-Factor mismatch checking is not very important
        #elif abs((entry.kFactor / referenceDict[entry.name].kFactor) - 1) > 0.05:
        #    print "k-Factor mismatch: "+entry.name+" "+str(entry.kFactor)+" != "+str(referenceDict[entry.name].kFactor)


    # re-loop to check missing DSIDs
    for entry in physicsProcesses[i].datasets:
        if entry.name not in referenceDict2:
            print "Dataset Missing from python: "+entry.name

    #print physicsProcesses[i]
    #print physicsProcesses2[i]



#for process in physicsProcesses:
#    print process

