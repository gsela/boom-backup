# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.selection_utils import get_selections, filter_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.utils import hist_sum

#import logging
#logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes(no_fake=True, no_signal=True)

do_1prong = True
do_closure = True

if do_1prong:
    _channels = ('e1p','mu1p')
    syst_name = ['isofac_stat_1prong','isofac_syst_1prong']
    prong = '1prong'
else:
    _channels = ('e3p','mu3p')
    syst_name = ['isofac_stat_3prong','isofac_syst_3prong']
    prong = '3prong'


if do_closure:
     _regions = (
        'qcd_lh_same_sign_anti_tau_truth_lep',
        'same_sign_anti_tau_lh'
        )
else :
    _regions = (
        'qcd_lh_anti_tau_truth_lep',
        'anti_tau'
        ) 

_categories = (
    'preselection',
    'boost',
    'vbf',
    'vh'
    )

### define your selection objects 
sels = get_selections(
    channels=_channels,
    categories=_categories,     
    regions=_regions)

# define your list of variables
var_x = VARIABLES['tau_0_pt_rqcd']
var_y = VARIABLES['dphi_met_tau_rqcd']

variables_x = [ var_x ]
variables_y = [ var_y ]    

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables_x, variables_y, systematic_names=syst_name)
processor.book_2D()
processor.run(n_cores=cpu_count() - 1)

# plot making
data_minus_mc = {}

for region in _regions:
    for category in _categories: 
        if do_1prong:
            channel = ('e1p','mu1p')
        else:
            channel = ('e3p','mu3p')

        sels_r = filter_selections(sels, channels=channel, regions=region, categories=category)
        hdata           = processor.get_hist_physics_process_2D('Data', sels_r, var_x, var_y)
        hdata_stat_up   = processor.get_hist_physics_process_2D('Data', sels_r, var_x, var_y, syst_name='isofac_stat_'+prong, syst_type='up')
        hdata_stat_down = processor.get_hist_physics_process_2D('Data', sels_r, var_x, var_y, syst_name='isofac_stat_'+prong, syst_type='down')
        hdata_syst_up   = processor.get_hist_physics_process_2D('Data', sels_r, var_x, var_y, syst_name='isofac_syst_'+prong, syst_type='up')
        hdata_syst_down = processor.get_hist_physics_process_2D('Data', sels_r, var_x, var_y, syst_name='isofac_syst_'+prong, syst_type='down')

        h_mcs           = []
        h_mcs_stat_up   = []
        h_mcs_stat_down = []
        h_mcs_syst_up   = []
        h_mcs_syst_down = []

        for p in processor.mc_backgrounds:
            h           = processor.get_hist_physics_process_2D(p.name, sels_r, var_x, var_y)
            h_stat_up   = processor.get_hist_physics_process_2D(p.name, sels_r, var_x, var_y, syst_name='isofac_stat_'+prong, syst_type='up')
            h_stat_down = processor.get_hist_physics_process_2D(p.name, sels_r, var_x, var_y, syst_name='isofac_stat_'+prong, syst_type='down')
            h_syst_up   = processor.get_hist_physics_process_2D(p.name, sels_r, var_x, var_y, syst_name='isofac_syst_'+prong, syst_type='up')
            h_syst_down = processor.get_hist_physics_process_2D(p.name, sels_r, var_x, var_y, syst_name='isofac_syst_'+prong, syst_type='down')

            h_mcs.append(h)
            h_mcs_stat_up.append(h_stat_up)
            h_mcs_stat_down.append(h_stat_down)
            h_mcs_syst_up.append(h_syst_up)
            h_mcs_syst_down.append(h_syst_down)

        h_mc = hist_sum(h_mcs)
        h_data_minus_mc = hdata.Clone()
        h_data_minus_mc.Add(h_mc, -1)

        h_mc_stat_up = hist_sum(h_mcs_stat_up)
        h_data_minus_mc_stat_up = hdata_stat_up.Clone()
        h_data_minus_mc_stat_up.Add(h_mc_stat_up, -1)

        h_mc_stat_down = hist_sum(h_mcs_stat_down)
        h_data_minus_mc_stat_down = hdata_stat_down.Clone()
        h_data_minus_mc_stat_down.Add(h_mc_stat_down, -1)

        h_mc_syst_up = hist_sum(h_mcs_syst_up)
        h_data_minus_mc_syst_up = hdata_syst_up.Clone()
        h_data_minus_mc_syst_up.Add(h_mc_syst_up, -1)

        h_mc_syst_down = hist_sum(h_mcs_syst_down)
        h_data_minus_mc_syst_down = hdata_syst_down.Clone()
        h_data_minus_mc_syst_down.Add(h_mc_syst_down, -1)


        data_minus_mc[region+'_'+category+'_'+prong]              = h_data_minus_mc
        data_minus_mc[region+'_'+category+'_'+prong+'_stat_up']   = h_data_minus_mc_stat_up
        data_minus_mc[region+'_'+category+'_'+prong+'_stat_down'] = h_data_minus_mc_stat_down
        data_minus_mc[region+'_'+category+'_'+prong+'_syst_up']   = h_data_minus_mc_syst_up
        data_minus_mc[region+'_'+category+'_'+prong+'_syst_down'] = h_data_minus_mc_syst_down


if do_closure:
    suff = 'same_sign_'
    ch = 'lh_'
else:
    suff = ''
    ch = ''

ff_transfers = [
    ('qcd_lh_'+suff+'anti_tau_truth_lep_preselection_'+prong, suff+'anti_tau_'+ch+'preselection_'+prong),
    ('qcd_lh_'+suff+'anti_tau_truth_lep_boost_'+prong, suff+'anti_tau_'+ch+'boost_'+prong),
    ('qcd_lh_'+suff+'anti_tau_truth_lep_vh_'+prong, suff+'anti_tau_'+ch+'vh_'+prong),
    ('qcd_lh_'+suff+'anti_tau_truth_lep_vbf_'+prong, suff+'anti_tau_'+ch+'vbf_'+prong),

    #stat up histograms
    ('qcd_lh_'+suff+'anti_tau_truth_lep_preselection_'+prong+'_stat_up', suff+'anti_tau_'+ch+'preselection_'+prong+'_stat_up'),
    ('qcd_lh_'+suff+'anti_tau_truth_lep_boost_'+prong+'_stat_up', suff+'anti_tau_'+ch+'boost_'+prong+'_stat_up'),
    ('qcd_lh_'+suff+'anti_tau_truth_lep_vh_'+prong+'_stat_up', suff+'anti_tau_'+ch+'vh_'+prong+'_stat_up'),
    ('qcd_lh_'+suff+'anti_tau_truth_lep_vbf_'+prong+'_stat_up', suff+'anti_tau_'+ch+'vbf_'+prong+'_stat_up'),

    #stat down histograms
    ('qcd_lh_'+suff+'anti_tau_truth_lep_preselection_'+prong+'_stat_down', suff+'anti_tau_'+ch+'preselection_'+prong+'_stat_down'),
    ('qcd_lh_'+suff+'anti_tau_truth_lep_boost_'+prong+'_stat_down', suff+'anti_tau_'+ch+'boost_'+prong+'_stat_down'),
    ('qcd_lh_'+suff+'anti_tau_truth_lep_vh_'+prong+'_stat_down', suff+'anti_tau_'+ch+'vh_'+prong+'_stat_down'),
    ('qcd_lh_'+suff+'anti_tau_truth_lep_vbf_'+prong+'_stat_down', suff+'anti_tau_'+ch+'vbf_'+prong+'_stat_down'),

    #syst up histograms
    ('qcd_lh_'+suff+'anti_tau_truth_lep_preselection_'+prong+'_syst_up', suff+'anti_tau_'+ch+'preselection_'+prong+'_syst_up'),
    ('qcd_lh_'+suff+'anti_tau_truth_lep_boost_'+prong+'_syst_up', suff+'anti_tau_'+ch+'boost_'+prong+'_syst_up'),
    ('qcd_lh_'+suff+'anti_tau_truth_lep_vh_'+prong+'_syst_up', suff+'anti_tau_'+ch+'vh_'+prong+'_syst_up'),
    ('qcd_lh_'+suff+'anti_tau_truth_lep_vbf_'+prong+'_syst_up', suff+'anti_tau_'+ch+'vbf_'+prong+'_syst_up'),

    #syst down histograms
    ('qcd_lh_'+suff+'anti_tau_truth_lep_preselection_'+prong+'_syst_down', suff+'anti_tau_'+ch+'preselection_'+prong+'_syst_down'),
    ('qcd_lh_'+suff+'anti_tau_truth_lep_boost_'+prong+'_syst_down', suff+'anti_tau_'+ch+'boost_'+prong+'_syst_down'),
    ('qcd_lh_'+suff+'anti_tau_truth_lep_vh_'+prong+'_syst_down', suff+'anti_tau_'+ch+'vh_'+prong+'_syst_down'),
    ('qcd_lh_'+suff+'anti_tau_truth_lep_vbf_'+prong+'_syst_down', suff+'anti_tau_'+ch+'vbf_'+prong+'_syst_down'),

]

if do_closure:
    output_filename = 'RQCDs_lephad_'+prong+'_closure.root'
else:
    output_filename = 'RQCDs_lephad_'+prong+'.root'

outfile = ROOT.TFile(output_filename,"recreate")
for (num, den) in ff_transfers:

    title = 'RQCD' 
    title += '_Presel' if 'presel' in num else '_Boosted' if 'boost' in num else '_VBF' if 'vbf' in num else '_VH'
    title += '_All_Comb_SLT'
    title += '_1prong' if do_1prong  else '_3prong'
 
    fake_factor = data_minus_mc[num].Clone()
    fake_factor.Divide(data_minus_mc[den])

    if 'stat_up' in num or 'stat_down' in num:
         if 'stat_up' in num : title += '_isofac_stat_up'
         else : title += '_isofac_stat_down'
         fake_factor.SetTitle(title)
         fake_factor.SetName(title)
         outfile.cd()
         fake_factor.Write()
    elif 'syst_up' in num or 'syst_down' in num:
         if 'syst_up' in num : title += '_isofac_syst_up'
         else : title += '_isofac_syst_down'
         fake_factor.SetTitle(title)
         fake_factor.SetName(title)
         outfile.cd()
         fake_factor.Write()
    else:
         fake_factor.SetTitle(title)
         fake_factor.SetName(title)
         fake_factor_stat_up   = fake_factor.Clone(title+'_stat_up')
         fake_factor_stat_down = fake_factor.Clone(title+'_stat_down')

         for i in range(fake_factor.GetNbinsX()+1):
             for j in range(fake_factor.GetNbinsY()+1):
                 fake_factor_stat_up.SetBinContent(i,j, fake_factor.GetBinContent(i,j) + fake_factor.GetBinError(i,j))
                 fake_factor_stat_down.SetBinContent(i,j, fake_factor.GetBinContent(i,j) - fake_factor.GetBinError(i,j))

         outfile.cd()
         fake_factor.Write()
         fake_factor_stat_up.Write()
         fake_factor_stat_down.Write()

outfile.Close()

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
