# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes


#import logging
#logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes(stxs='stxs0', squash=True)#, no_fake = True)

_channels = (
    '1p1p',
    '1p3p',
    '3p1p',
    '3p3p',
    )

_years = (
    '15',
    '16',
    '17',
    '18',
)

_categories = (
    'preselection',
    #'boost',
    #'vh',
    #'vbf',
    #'boost_1J',
    #'boost_ge2J',
    #'qcd_fit',
    #'boost_0',
    #'boost_1',
    #'boost_2',
    #'boost_3',
    #'vbf_0',
    #'vbf_1',
    #'vh',
)

_regions = (
    'SR',
    #'anti_tau',
    #'same_sign',
    #'high_deta',
    #'high_deta_same_sign',
    #'high_deta_anti_lead_tau',
    #'high_deta_anti_sublead_tau',
    #'high_deta_anti_lead_tau_loose',
    #'high_deta_anti_sublead_tau_loose',
    )

### define your selection objects 
sels = get_selections(
    channels=_channels, 
    years=_years,
    categories=_categories,
    regions=_regions)


# define your list of variables
_variables = [
     #VARIABLES['norm'],
     VARIABLES['tau_0_width'],
     VARIABLES['tau_1_width'],
     #VARIABLES['mmc_mlm_m'],
     #VARIABLES['ditau_dphi'],
     #VARIABLES['ditau_deta'],
     #VARIABLES['ditau_dr'],
     #VARIABLES['tau_0_pt'],
     #VARIABLES['tau_1_pt'],
     #VARIABLES['tau_0_eta'],
     #VARIABLES['tau_1_eta'],
     #VARIABLES['met'],
     #VARIABLES['higgs_pt'],
     #VARIABLES['n_jets_30'],
     #VARIABLES['vbf_tagger_medium_fine_binning'],
     #VARIABLES['vh_tagger'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, _variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

# plot making
for reg in _regions:
    for var in _variables:
        for cat in _categories:
            force_unblind = bool(not reg == "SR")
            make_data_mc_plot(processor, sels, var, categories=cat, regions=reg, force_unblind=force_unblind) # total plot
            for ch in _channels:
                make_data_mc_plot(processor, sels, var, categories=cat, regions=reg, channels=ch, force_unblind=force_unblind) # split in krongs

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
