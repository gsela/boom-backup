# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores, ZLL_VR_PROCESS_MERGING_SCHEME
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database  import get_processes


# retrieve physics processes
physicsProcesses = get_processes(no_fake=True)

### define your selection objects 
_categories = (
      'preselection',
#     'vbf_0',
#     'vbf_1',
#      'vh_1'
#      'boost',
)
_years = (
     '15',
     '16',
     '17',
     '18',
)
_channels = (
     'ee', 
     'mumu'
)
_regions = (
     'Ztt_cr_ll', 
     'Ztt_cr_lh', 
     'Ztt_cr_hh', 
)
sels = get_selections(
     channels=_channels,
     years=_years,
     categories=_categories,
     regions=_regions)

# from boom.cuts.kinematics import Cut_njet
# Cut_njet.cut.cut = 'n_jets >0 && jet_0_p4.Eta() * jet_1_p4.Eta() < 0'
# title = 'ee, mumu, preselection, Ztt_cr_lh, eta prod neg'
title = None

# define your list of variables
variables = [
#     VARIABLES['norm'],
#     VARIABLES['pt_total'],
#      VARIABLES['mjj'],
#     VARIABLES['jets_deta'],
#     VARIABLES['jets_eta_prod'],
#     VARIABLES['var_ptjj'],
#     VARIABLES['vbf_tagger_medium_fine_binning'],
#      VARIABLES['tau_0_pt'],
#     VARIABLES['tau_0_pt_corr'],
#      VARIABLES['tau_1_pt'],
#      VARIABLES['tau_1_pt_corr'],
     VARIABLES['ditau_dr'],
##     VARIABLES['ditau_jet_0_deta'],
#     VARIABLES['ditau_jet_1_deta'],
 #    VARIABLES['ditau_jet_2_deta'],
##     VARIABLES['ditau_rapidity'],
#     VARIABLES['met_high'],
#     VARIABLES['ditau_pt_low'],
#     VARIABLES['higgs_pt_low'],
#     VARIABLES['ditau_mass'],
#     VARIABLES['jet_0_pt_low'],
#     VARIABLES['jet_1_pt'],
#     VARIABLES['jet_2_pt'],
#     VARIABLES['mjj_low'],
#     VARIABLES['dijet_ditau_pt_balance'],
#     VARIABLES['pt_total'],
#     VARIABLES['n_jets'],
#     VARIABLES['ditau_dr'],
#     VARIABLES['ditau_deta'],
#     VARIABLES['jets_deta'],
#     VARIABLES['jets_deta_wide'],
#     VARIABLES['jets_eta_prod_wide'],
#     VARIABLES['ditau_pt'],
#     VARIABLES['ditau_pt_coarse'],
#     VARIABLES['mjj'],
#     VARIABLES['n_jets_30'],
 #   VARIABLES['mjj'],
#      VARIABLES['vbf_hh_triangle']
#
]
#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor._process_merging_scheme = ZLL_VR_PROCESS_MERGING_SCHEME
processor.book()
processor.run(n_cores=cpu_count() - 1)

# plot making
for var in variables:
     for _region in _regions:
          make_data_mc_plot(
               processor, sels, var, 
               regions=_region,
               force_unblind=True, 
               title=title,
               ratio_range=0.5)

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
