
def fill_channel(chan, rfile, no_signal=False, hist_name='nominal'):
    
    _channels = [k.GetName() for k in rfile.GetListOfKeys()]
    if chan.name not in _channels:
        raise ValueError('channel {} does not exist'.format(channel.name))

    r_dir = rfile.Get(chan.name)
    _samples = []
    _histograms = []
    for _key in r_dir.GetListOfKeys():
        _obj = _key.ReadObj()
        if isinstance(_obj, ROOT.TDirectoryFile):
            if no_signal and 'H' in _key.GetName():
                continue
            if hist_name != 'nominal' and _key.GetName() == 'Data':
                continue
            _samples.append(_key.GetName())
            _hist = _obj.Get(hist_name)
            _histograms.append(_hist.Clone())

    chan.samples = _samples
    chan.histograms = _histograms




if __name__ == '__main__':
    from argparse import ArgumentParser
    from boom.workspace import channel
    parser = ArgumentParser()
    parser.add_argument('file')
    parser.add_argument('--hist-name', default='nominal')
    parser.add_argument('--no-signal', default=False, action='store_true')
    args = parser.parse_args()

    import ROOT
    from boom.wsi_tools import workspace_input
    file_name = args.file
    rfile = ROOT.TFile(file_name)
    _wsi = workspace_input(rfile)
    

    headers = ['channel', 'category', 'region']
    headers = headers + _wsi.channels[0].sample_names

    lines = []
    for _chan in _wsi.channels:
        chan_name, cat, region  = _chan.name.split('__')
        line = [chan_name, cat, region]
        for _sample in _chan.samples:
            _h = _sample.hist(args.hist_name)
            line.append(_h.Integral(0, _h.GetNbinsX() + 1))
        lines.append(line)

    lines_0 = []
    lines_1 = []
    lines_2 = []
    lines_3 = []
    lines_4 = []
    lines_5 = []
    lines_6 = []
    for _line in lines:
        headers_0 = headers[0:10]
        headers_1 = headers[10:20]
        headers_2 = headers[20:30]
        headers_3 = headers[30:40]
        headers_4 = headers[40:50]
        headers_5 = headers[50:60]
        headers_6 = headers[60:]
        lines_0.append(_line[0:10])
        lines_1.append(_line[10:20])
        lines_2.append(_line[20:30])
        lines_3.append(_line[30:40])
        lines_4.append(_line[40:50])
        lines_5.append(_line[50:60])
        lines_6.append(_line[60:])

    from boom.extern.tabulate import tabulate
#     for fmt in ('simple'):#, 'latex'):
    fmt = 'simple'
    print tabulate(lines_0, headers=headers_0, tablefmt=fmt)
    print
    print tabulate(lines_1, headers=headers_1, tablefmt=fmt)
    print
    print tabulate(lines_2, headers=headers_2, tablefmt=fmt)
    print
    print tabulate(lines_3, headers=headers_3, tablefmt=fmt)
    print
    print tabulate(lines_4, headers=headers_4, tablefmt=fmt)
    print
    print tabulate(lines_5, headers=headers_5, tablefmt=fmt)
    print
    print tabulate(lines_6, headers=headers_6, tablefmt=fmt)

