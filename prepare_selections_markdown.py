
if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--json', default='data/wsi/wsi.json', help = "(default: %(default)s)")
    parser.add_argument('--dry', default=False, action='store_true')
    args = parser.parse_args()
    

    from boom.selection_utils import get_selections
    from boom.selection_utils import print_selection
    from boom.cuts import YEARS

    import json
    with open(args.json) as f:
        channel_dict = json.load(f)

    
    for key, _chan in channel_dict.items():
        for _year in YEARS:
            sels = get_selections(
                channels=_chan['channels'],
                categories=_chan['categories'],
                years=_year,
                regions=_chan['regions'])
            for sel in sels:
                print sel.name
                print_selection(sel, save_to_file=not args.dry)

