
from boom.core import boom_processor, close_stores


from boom.selection_utils import get_selections


from boom.variables import VARIABLES


from boom.database import get_processes
physicsProcesses = get_processes()


sels = get_selections(
    channels=('e1p', 'mu1p'), 
    categories=('vbf_loose', 'vbf_tight'),
    years=('15', '16'), 
    regions=('SR'),
    triggers=None)






variables = [
    VARIABLES['tau_0_pt'],
]


processor = boom_processor(physicsProcesses, sels, variables)

processor.book()


from multiprocessing import cpu_count
processor.run(n_cores=cpu_count() - 1)


h = processor.get_hist_physics_process('Ztt', sels, VARIABLES['tau_0_pt'])


from boom.plotting import basic_plot
basic_plot(sels, h, VARIABLES['tau_0_pt'])


close_stores(physicsProcesses)
