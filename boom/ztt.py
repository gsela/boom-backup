"""
Implementation of the Z+jets ckkw and qsf weights
"""
import ROOT
import os
from happy.systematics import Systematics, SystematicsSet


_THEOSYST_FILE = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/alltheovar_r21.root")

_root_file = ROOT.TFile(_THEOSYST_FILE, "read") 

ROOT.ztheo_histDict.h_ckkw[0] = _root_file.Get("ckk")
ROOT.ztheo_histDict.h_qsf[0]  = _root_file.Get("qsf")

def ztheosyst_expr(syst, channel):
    expr = 'ZTheoSystHelper::theo_weight(boson_0_truth_p4.Pt(),n_truth_jets_pt20_eta45,{syst},{channel},0)'.format(
         syst    = str(getattr(ROOT.ztheo_histDict, syst)),
         channel = str(getattr(ROOT.ztheo_histDict, channel)), 
         )
    return expr

def get_ztt_weights(channel):
    """Retrieve the ckkq and qsf weights

    Parameters
    __________
    channel : str
       see CHANNELS in cuts/__init__.py

    Returns
    _______
    sys_set : HAPPy SystematicsSet
       set of weights for qcd and ckkw Z+jets theory uncertainties

    Raises
    ______
    NotImplementedError
       If there is no weight for a given combination of input parameters
    """

    if channel in ('1p1p', '1p3p', '3p1p', '3p3p'):
        ch = 'hadhad'
    elif channel in ('e1p', 'mu1p', 'e3p', 'mu3p'):
        ch = 'lephad'
    elif channel in ('emu', 'mue', 'ee', 'mumu'):
        ch = 'leplep'
    else:
        raise NotImplementedError

    ztheo_ckkw_expr = ztheosyst_expr('ckkw',ch)
    ztheo_qsf_expr  = ztheosyst_expr('qsf',ch)

    sys_ckkw = Systematics.weightSystematics('theory_z_ckk', 'theory_z_ckk',
                                                    '('+ztheo_ckkw_expr+')',  
                                                    '(2-'+ztheo_ckkw_expr+')',
                                                    '(1)')

    sys_qsf  = Systematics.weightSystematics('theory_z_qsf', 'theory_z_qsf',
                                                    '('+ztheo_qsf_expr+')',
                                                    '(2-'+ztheo_qsf_expr+')',
                                                    '(1)') 
  
    sys_set  = SystematicsSet(set([ sys_ckkw,
                                    sys_qsf]))

    return sys_set
        
