"""
Module encoding the xsec X kfactor X filter_efficiency / sum_of_weights maps
for all the MC datasets used in the analysis
"""

import os
import ROOT
import json



__all__ = [
    "build",
]

def _fill_map(map, weight_list):
    if len(weight_list) == 0:
        print 'BOOM: warning weight list is empty!'
    else:
        for dsid, xsec, sum_of_weights in weight_list:
            if sum_of_weights == 0:
                #print 'BOOM: warning dsid {}, sum_of_weight = 0'.format(dsid)
                map[dsid] = 0
            else:
                map[dsid] = xsec / sum_of_weights


def _fill_map_zpdf(map, weight_list, camp):

    for (dsid, pdflist) in weight_list[camp].items():
      for variation in range(0,100): 
        #map[dsid][variation]=2 #pdflist[variation]
        map[str(dsid)+"_"+str(variation)] = pdflist[variation]
      map[str(dsid)+"_"+'theory_z_lhe3weight_mur05_muf05_pdf261000']=pdflist[100]
      map[str(dsid)+"_"+'theory_z_lhe3weight_mur05_muf1_pdf261000']=pdflist[101]
      map[str(dsid)+"_"+'theory_z_lhe3weight_mur1_muf05_pdf261000']=pdflist[102]
      map[str(dsid)+"_"+'theory_z_lhe3weight_mur1_muf2_pdf261000']=pdflist[103]
      map[str(dsid)+"_"+'theory_z_lhe3weight_mur2_muf1_pdf261000']=pdflist[104]
      map[str(dsid)+"_"+'theory_z_lhe3weight_mur2_muf2_pdf261000']=pdflist[105]


def _fill_json(ntuples_block='nom'):

    if ntuples_block == 'sys_M':
        _ntuples_block = 'sys_MET'
    else:
        _ntuples_block = ntuples_block

    _file_name = 'xsec_sumofweights_{}.json'.format(_ntuples_block)
    _xsec_sumofweight_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__ )), 
        '..', 'data', 'xsec_sow', _file_name)

    _xsec_weight_dict = {}
    with open(_xsec_sumofweight_file) as f:
        print 'BOOM: reading {}'.format(_xsec_sumofweight_file)
        _xsec_weight_dict = json.load(f)
    return _xsec_weight_dict


def _fill_json_Zpdf():
  
    _xsec_sumofweight_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__ )),
        '..', 'data', 'xsec_sumofweights_theoryZ.json')

    f=open(_xsec_sumofweight_file)
    weights = json.load(f) 
    return weights

def build(ntuples_block='nom'):
    print 'BOOM: filling the xsec X kfactor X filter_efficiency / sum_of_weights maps'
    _xsec_weight_dict = _fill_json(ntuples_block=ntuples_block)
    
    print 'BOOM: filling mc16a hh'
    _fill_map(ROOT.crossSectionDict.m_Map_hh_a, _xsec_weight_dict['mc16a']['hh'])
    print 'BOOM: filling mc16d hh'
    _fill_map(ROOT.crossSectionDict.m_Map_hh_d, _xsec_weight_dict['mc16d']['hh'])
    print 'BOOM: filling mc16e hh'
    _fill_map(ROOT.crossSectionDict.m_Map_hh_e, _xsec_weight_dict['mc16e']['hh'])
    print 'BOOM: filling mc16a lh'
    _fill_map(ROOT.crossSectionDict.m_Map_lh_a, _xsec_weight_dict['mc16a']['lh'])
    print 'BOOM: filling mc16d lh'
    _fill_map(ROOT.crossSectionDict.m_Map_lh_d, _xsec_weight_dict['mc16d']['lh'])
    print 'BOOM: filling mc16e lh'
    _fill_map(ROOT.crossSectionDict.m_Map_lh_e, _xsec_weight_dict['mc16e']['lh'])
    print 'BOOM: filling mc16a ll'
    _fill_map(ROOT.crossSectionDict.m_Map_ll_a, _xsec_weight_dict['mc16a']['ll'])
    print 'BOOM: filling mc16d ll'
    _fill_map(ROOT.crossSectionDict.m_Map_ll_d, _xsec_weight_dict['mc16d']['ll'])
    print 'BOOM: filling mc16a ll'
    _fill_map(ROOT.crossSectionDict.m_Map_ll_e, _xsec_weight_dict['mc16e']['ll'])
    print 'BOOM: filling mc16a zll_vr'
    _fill_map(ROOT.crossSectionDict.m_Map_vr_a, _xsec_weight_dict['mc16a']['zll_vr'])
    print 'BOOM: filling mc16d zll_vr'
    _fill_map(ROOT.crossSectionDict.m_Map_vr_d, _xsec_weight_dict['mc16d']['zll_vr'])
    print 'BOOM: filling mc16e zll_vr'
    _fill_map(ROOT.crossSectionDict.m_Map_vr_e, _xsec_weight_dict['mc16e']['zll_vr'])
   
    _xsec_weight_dict_Zpdf = _fill_json_Zpdf()
    print 'BOOM: filling Zpdf '
    _fill_map_zpdf(ROOT.crossSectionDict.m_Map_Zpdf_a, _xsec_weight_dict_Zpdf, "mc16a")
    _fill_map_zpdf(ROOT.crossSectionDict.m_Map_Zpdf_d, _xsec_weight_dict_Zpdf, "mc16d")
    _fill_map_zpdf(ROOT.crossSectionDict.m_Map_Zpdf_e, _xsec_weight_dict_Zpdf, "mc16e")
    return True
