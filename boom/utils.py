
import ROOT
from .extern.tabulate import tabulate
from .selection_utils import filter_selections
from .lumi import LUMI


def rqcd_calc(h1, h2):
    """
    Helper function to compute the ratio (and its error)
    of the integral of two histograms
    """

    h_ratio = h1.Clone()
    h_ratio = h_ratio.Rebin(h_ratio.GetNbinsX())
    h_den = h2.Clone()
    h_den = h_den.Rebin(h2.GetNbinsX())
    h_ratio.Divide(h_den)
    rqcd_val, rqcd_err = yield_tuple(h_ratio)
    return rqcd_val, rqcd_err


def hist_max(hists):
    """
    Helper function to determine
    max of list of histogram
    """
    if not isinstance(hists, (list, tuple)):
        raise TypeError('type(hists) = {} is not a list or a tuple'.format(type(hists)))

    if len(hists) == 0:
        return 0.
    maximum = max([h.GetBinContent(h.GetMaximumBin()) for h in hists])
    return maximum

def hist_sum(hists):
    """
    Helper function to sum 
    a list of histograms
    """
    if not isinstance(hists, (list, tuple)):
        raise TypeError('type(hists) = {} is not a list or a tuple'.format(type(hists)))
    #print "kanae : histsize ", len(hists)
    hist = hists[0].Clone()
    for h in hists[1:]:
        hist.Add(h)
    return hist

def histlist_sum(histlist):
    """
    Helper function to sum 
    a list of histograms
    """
    if not isinstance(histlist, (list, tuple)):
        raise TypeError('type(histlist) = {} is not a list or a tuple'.format(type(histlist)))
    lists = histlist[0]
    for hists_sel in histlist[1:]:
        i = 0
        for hists in hists_sel:
            j = 0
            for h in hists:
                lists[i][j].Add(h)
                j += 1
            i += 1
    return lists

def blinded_hist(h, var_name='mmc', signal=None, signal_frac=0.5):
    """
    Helper function to blind 
    histogram range
    """

    if not isinstance(h, ROOT.TH1):
        raise NotImplementedError

    blind_range = {
        'mmc': (100, 150),
        'visible': (70, 110),
        'collinear': (100, 150),
        }


    if signal is not None:
        if not isinstance(signal, ROOT.TH1):
            raise NotImplementedError

        _bin_min = 0
        for ibin in xrange(signal.GetNbinsX() + 1):
            _frac = float(signal.Integral(0, ibin))
            _frac /= float(signal.Integral(0, signal.GetNbinsX() + 1))
            if _frac > signal_frac:
                break
            else:
                _bin_min = ibin
        bin_min = _bin_min + 1
        bin_max = signal.GetNbinsX() + 1
            
    else:
        x_min, x_max = blind_range[var_name]
        bin_min = h.FindBin(x_min)
        bin_max = h.FindBin(x_max)

    h_blind = h.Clone(h.GetName() + '_blinded')
    h_blind.SetTitle(h.GetTitle())

    for ibin in range(bin_min, bin_max):
        h_blind.SetBinContent(ibin, 0)
        h_blind.SetBinError(ibin, 0)
    
    return h_blind


def significance_counting(signal, bkg, x_range=(100, 150), return_pval=False):
    """
    Helper function to perform stat-only counting-experiment
    """
    if not isinstance(signal, ROOT.TH1):
        raise NotImplementedError

    if not isinstance(bkg, ROOT.TH1):
        raise NotImplementedError
    
    if not isinstance(x_range, (list, tuple)):
        raise TypeError('x_range should be a list or a tuple')
    
    if len(x_range) != 2:
        raise ValueError('x_range is expected to be of size 2')

    x_min, x_max = x_range[0], x_range[1]
    bin_min = signal.FindBin(x_min)
    bin_max = signal.FindBin(x_max)
    
    _n_sig = signal.Integral(bin_min, bin_max)
    bkg_err = ROOT.Double(0.)
    _n_bkg = bkg.IntegralAndError(bin_min, bin_max, bkg_err)
    _rel_bkg_err = bkg_err / _n_bkg
    
    if return_pval:
        return ROOT.RooStats.NumberCountingUtils.BinomialExpP(
            _n_sig, _n_bkg, _rel_bkg_err)
    else:
        return ROOT.RooStats.NumberCountingUtils.BinomialExpZ(
            _n_sig, _n_bkg, _rel_bkg_err)
        



def yield_tuple(hist):
    """
    Helper function to retrieve total yields and error from 
    a ROOT.TH1F (including over/underflow bins)
    """
    from array import array
    error = array('d', [0])
    integral = hist.IntegralAndError(0, hist.GetNbinsX() + 1, error)
    return integral, error[0]

def yield_table(data, signals, bkgs, sig_names=None, bkg_names=None, blinding_threshold=0.1):
    """
    Pretty print-out of the yields
    """

    if not isinstance(data, ROOT.TH1):
        raise TypeError

    if not isinstance(signals, (list, tuple)):
        signals = [signals]

    for _s in signals:
        if not isinstance(_s, ROOT.TH1):
            raise TypeError

    if not isinstance(bkgs, (list, tuple)):
        bkgs = [bkgs]

    for _b in bkgs:
        if not isinstance(_b, ROOT.TH1):
            raise TypeError
        

    table = []

    if bkg_names is None:
        bkg_names = [b.GetName() for b in bkgs]

    if sig_names is None:
        sig_names = [s.GetName() for s in signals]
    
    if len(bkg_names) != len(bkgs):
        raise ValueError


    for name, b in zip(bkg_names, bkgs):
        integral, error = yield_tuple(b)
        line = [name, integral, error]
        table.append(line)

    for name, s in zip(sig_names, signals):
        integral, error = yield_tuple(s)
        tot_sig = integral
        line = [name, integral, error]
        table.append(line)

    if len(signals) > 1:
        sig = hist_sum(signals)
        tot_sig, tot_sig_err = yield_tuple(sig)
        table.append(['Signal', tot_sig, tot_sig_err])

    bkg = hist_sum(bkgs)
    tot_bkg, tot_bkg_err = yield_tuple(bkg)
    tot_data, _ = yield_tuple(data)
    table.append(['Tot. Bkg', tot_bkg, tot_bkg_err])
    
    # Unblinding protection on S/B>blinding_threshold
    if tot_sig/tot_bkg < blinding_threshold:
        table.append(['Data', tot_data,])
    else:
        #table.append(['Data blinded',0,])
        table.append(['Data', tot_data,])

    headers = ['Sample', 'Yields', 'Stat. Uncert']
    nice_table = tabulate(table, headers=headers, tablefmt='orgtbl', floatfmt='.2f')
    return nice_table


def lumi_calc(selections):
    """
    infer the integrated luminosity
    based on the list of selections
    """
    int_lumi = 0
    for year in ('15', '16', '17', '18'):
        _sels = filter_selections(selections, years=year)
        if len(_sels) != 0:
            int_lumi += LUMI[year]

    return int_lumi

def plot_title_extension(    
    selections,
    channels=None,
    categories=None,
    years=None,
    regions=None,
    triggers=None):
    """Function to generate plot title and file extension

    Arguments
    _________
    selections : list(selection)
       selection objects to filter on
    channels: str or list(str) 
       see CHANNELS in cuts/__init__.py
    categories: str or list(str) 
       see CATEGORIES in cuts/__init__.py
    years: str or list(str) 
       see YEARS in cuts/__init__.py
    regions: str or list(str) 
       see REGIONS in cuts/__init__.py
    triggers: str or list(str) 
       see TRIGGERS in cuts/__init__.py

    Returns
    _______
    title, extension: (str, str)
       plot title, file extension
    """

    _channels = []
    if channels is None:
        for sel in selections:
            if not sel.channel in _channels:
                _channels.append(sel.channel)
    else:
        if isinstance(channels, (list, tuple)):
            _channels = channels[:]
        else:
            _channels = [channels]
    _channels = sorted(_channels)

    _categories = []
    if categories is None:
        for sel in selections:
            if not sel.category in _categories:
                _categories.append(sel.category)
    else:
        if isinstance(categories, (list, tuple)):
            _categories = categories[:]
        else:
            _categories = [categories]
    _categories = sorted(_categories)

    _years = []
    if years is None:
        for sel in selections:
            if not sel.year in _years:
                _years.append(sel.year)
    else:
        if isinstance(years, (list, tuple)):
            _years = years[:]
        else:
            _years = [years]
    _years = sorted(_years)

    _regions = []
    if regions is None:
        for sel in selections:
            if not sel.region in _regions:
                _regions.append(sel.region)
    else:
        if isinstance(regions, (list, tuple)):
            _regions = regions[:]
        else:
            _regions = [regions]
    _regions = sorted(_regions)
    _triggers = []
    if triggers is None:
        for sel in selections:
            if not sel.trigger in _triggers:
                _triggers.append(sel.trigger)
    else:
        if isinstance(triggers, (list, tuple)):
            _triggers = triggers[:]
        else:
            _triggers = [triggers]
    _triggers = sorted(_triggers)

    # channels
    if sorted(_channels) == ['1p1p', '1p3p', '3p1p', '3p3p']:
        _chan_str = 'hh'
    elif _channels == ['e1p', 'e3p', 'mu1p', 'mu3p']:
        _chan_str = 'lh'
    elif _channels == ['e1p', 'e3p']:
        _chan_str = 'eh'
    #elif _channels == ['mu1p', 'mu3p']:
    elif _channels == ['mu1p']:
        _chan_str = 'muh'
    elif _channels == ['e1p', 'mu1p']:
        _chan_str = 'l1p'
    elif _channels == ['e3p', 'mu3p']:
        _chan_str = 'l3p'
    elif _channels == sorted(['ee', 'mumu', 'emu', 'mue']):
        _chan_str = 'll'
    elif _channels == ['emu', 'mue']:
        _chan_str = 'll df'
    elif _channels == sorted(['1p1p', '1p3p', '3p1p', '3p3p', 'e1p', 'e3p', 'mu1p', 'mu3p', 'emu', 'mue']):
        _chan_str = 'all channels'
    else:
        _chan_str = ', '.join(_channels)

    # categories
    if _categories == ['boost_loose', 'boost_tight']:
        _cat_str = 'boost'
    #elif _categories == ['vbf_loose', 'vbf_tight']:
    elif _categories == ['vbf_0']:
        _cat_str = 'nonvbf'
    elif _categories == ['vbf_1']:
        _cat_str = 'vbf'
    elif _categories == ['vbf_0', 'vbf_1']:
        _cat_str = 'vbf'
    elif _categories == ['vh_0', 'vh_1']:
        _cat_str = 'vh'
    elif _categories == sorted(['vbf_loose', 'vbf_tight', 'vbf_lowdr']):
        _cat_str = 'vbf'
    elif _categories == ['boost_loose', 'boost_tight', 'vbf_loose', 'vbf_tight']:
        _cat_str = 'vbf and boost'
    elif _categories == sorted(['boost_loose', 'boost_tight', 'vbf_loose', 'vbf_tight', 'vbf_lowdr']):
        _cat_str = 'vbf and boost'
    else:
        _cat_str = ', '.join(_categories)

    # years
    if _years == ['15', '16', '17', '18']:
        _year_str = ''
    else:
        _year_str = ', '.join(_years)

    # regions
    if len(_regions) > 1:
        _region_str = ''
    else:
        _region_str = _regions[0]

    if _region_str == 'SR':
        _region_str = ''
        
    # triggers
    _trigger_str = ''

    _title = [_chan_str, _cat_str, _region_str, _trigger_str]
    _title = filter(lambda s: s != '', _title)
    _title_str = ', '.join(_title)

    _extension = [_chan_str, _cat_str, _year_str, _region_str, _trigger_str]
    _extension = filter(lambda s: s != '', _extension)
    _extension_str = '_'.join(_extension)
    _extension_str = _extension_str.replace(',', '').replace(' ', '_')

    return _title_str, _extension_str
