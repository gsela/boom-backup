import os
import ROOT

# Here we'll access environment variables
# This is a way to steer the random number seed externally
_random_offset = 666

if 'BOOM_RND_SEED' in os.environ:
    _random_offset += int(os.environ['BOOM_RND_SEED'])

tau_0_pt_corr = 'PtCorrHelper::read_pt_corr_0({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})'.format(
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

tau_1_pt_corr = 'PtCorrHelper::read_pt_corr_1({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})'.format(
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

met_corr = 'PtCorrHelper::MET_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9})'.format(
    'met_p4.Et()',
    'met_p4.Phi()',
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

x0_corr = 'PtCorrHelper::x1_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9})'.format(
    'met_p4.Et()',
    'met_p4.Phi()',
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

x1_corr = 'PtCorrHelper::x2_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9})'.format(
    'met_p4.Et()',
    'met_p4.Phi()',
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

dr_corr = 'PtCorrHelper::dr_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})'.format(
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')


deta_corr = 'PtCorrHelper::deta_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})'.format(
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

mT_corr = 'PtCorrHelper::mT_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9})'.format(
    'met_p4.Et()',
    'met_p4.Phi()',
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

ditau_m_corr = 'PtCorrHelper::ditau_m_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})'.format(
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

ditau_eta_corr = 'PtCorrHelper::ditau_eta_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})'.format(
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

ditau_phi_corr = 'PtCorrHelper::ditau_phi_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})'.format(
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

ditau_pt_corr = 'PtCorrHelper::ditau_pt_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})'.format(
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

lepton_pt_cuts_15_lh = 'PtCorrHelper::lepton_pt_cuts_15_lh({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})'.format(
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

lepton_pt_cuts_lh = 'PtCorrHelper::lepton_pt_cuts_lh({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})'.format(
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

lepton_pt_cuts_ll = 'PtCorrHelper::lepton_pt_cuts_ll({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})'.format(
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')


eta_crack_cuts = 'PtCorrHelper::eta_crack_cuts({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})'.format(
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')
    


dijet_ditau_dr_corr = 'PtCorrHelper::dijet_ditau_dr_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11})'.format(
    'dijet_p4.Pt()',
    'dijet_p4.Eta()',
    'dijet_p4.Phi()',
    'dijet_p4.M()',
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

jet_0_tau_0_dr_corr = 'PtCorrHelper::jet_tau_0_dr_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10})'.format(
    'jet_0_p4.Pt()',
    'jet_0_p4.Eta()',
    'jet_0_p4.Phi()',
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

jet_0_tau_1_dr_corr = 'PtCorrHelper::jet_tau_1_dr_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10})'.format(
    'jet_0_p4.Pt()',
    'jet_0_p4.Eta()',
    'jet_0_p4.Phi()',
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

jet_1_tau_0_dr_corr = 'PtCorrHelper::jet_tau_0_dr_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10})'.format(
    'jet_1_p4.Pt()',
    'jet_1_p4.Eta()',
    'jet_1_p4.Phi()',
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

jet_1_tau_1_dr_corr = 'PtCorrHelper::jet_tau_1_dr_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10})'.format(
    'jet_1_p4.Pt()',
    'jet_1_p4.Eta()',
    'jet_1_p4.Phi()',
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

m_coll_corr = 'PtCorrHelper::m_coll_corr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9})'.format(
    'met_p4.Pt()',
    'met_p4.Phi()',
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

vh_tagger_corr_expr = 'VH_Tagger::vh_bdt_score({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14})'.format(
    # These are easy, just the jet kinematics
    'jet_0_p4.Pt()',
    'jet_0_p4.Eta()',
    'jet_0_p4.Phi()',
    'jet_0_p4.M()',
    'jet_1_p4.Pt()',
    'jet_1_p4.Eta()',
    'jet_1_p4.Phi()',
    'jet_1_p4.M()',
    # Here we need to calculate embedded quantities
    ditau_pt_corr,
    ditau_eta_corr,
    ditau_phi_corr,
    ditau_m_corr,
    met_corr, 
    # higgs pTs are unchanged
    'ditau_higgspt',
    # random number untouched
    'Rnd::mva_random_number(event_number)')


#'PtCorrHelper::dijet_ditau_dr_corr(dijet_p4.Pt(), dijet_p4.Eta(), dijet_p4.Phi(), dijet_p4.M(), '+','.join(tau_0_4vec) + ','.join(tau_1_4vec) + 'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number))',

# Definitions for simplifications
tau_0_4vec   = ['tau_0_p4.Pt()', 'tau_0_p4.Eta()', 'tau_0_p4.Phi()', ' ']
tau_1_4vec   = ['tau_1_p4.Pt()', 'tau_1_p4.Eta()', 'tau_1_p4.Phi()', ' ']
tau_0_m_4vec = ['tau_0_matched_p4.Pt()', 'tau_0_matched_p4.Eta()', 'tau_0_matched_p4.Phi()', ' ']
tau_1_m_4vec = ['tau_1_matched_p4.Pt()', 'tau_1_matched_p4.Eta()', 'tau_1_matched_p4.Phi()', ' ']
met_4vec     = ['met_p4.Et()', 'met_p4.Phi()', ' ']
