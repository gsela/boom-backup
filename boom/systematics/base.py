from ..cuts import CHANNELS, TRIGGERS, YEARS, CATEGORIES, REGIONS, SYSTEMATICS
from happy.systematics import SystematicsSet, Systematics

# building the syst book
SYSTBOOK = []

class DecoratedSystematics(object):
    def __init__(
        self,
        name,
        title,
        systSet,
        bkg_only=False,
        signal_only=False,
        force_data_application=False,
        channels=CHANNELS,
        triggers=TRIGGERS,
        years=YEARS,
        categories=CATEGORIES,
        regions=REGIONS):
        self.name = name
        self.title = title
        self.systSet = systSet
        self.force_data_application = force_data_application
        # channels
        if not isinstance(channels, (list, tuple)):
            self.channels = [channels]
        else:
            self.channels = channels

        # triggers
        if not isinstance(triggers, (list, tuple)):
            self.triggers = [triggers]
        else:
            self.triggers = triggers

        # years
        if not isinstance(years, (list, tuple)):
            self.years = [years]
        else:
            self.years = years
            
        # categories
        if not isinstance(categories, (list, tuple)):
            self.categories = [categories]
        else:
            self.categories = categories

        # regions
        if not isinstance(regions, (list, tuple)):
            self.regions = [regions]
        else:
            self.regions = regions

        self.bkg_only = bkg_only
        self.signal_only = signal_only

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.channels

