from ..cuts import REGIONS
from .base import DecoratedSystematics, SYSTBOOK
from .object_weight_dictionary import scale_factors
from happy.systematics import SystematicsSet, Systematics
import gconfig

sys_lead_tau_reco_total  = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RECO_TOTAL',  'Tau Reco Total', 
                        '({})'.format('tau_0_'+scale_factors['Tau']['Reco']['total_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['Reco']['total_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['Reco']['nominal']))

sys_taus_reco_total_hh  = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RECO_TOTAL',  'Tau Reco Total',
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['Reco']['total_up'],'tau_1_'+scale_factors['Tau']['Reco']['total_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['Reco']['total_down'],'tau_1_'+scale_factors['Tau']['Reco']['total_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['Reco']['nominal'],'tau_1_'+scale_factors['Tau']['Reco']['nominal']))

#sys_lead_tau_eleolr_trueelectron_bdt_stat = Systematics.weightSystematics( 'TAUS_TRUEELECTRON_EFF_ELEBDT_STAT',  'Tau EleOLR True Ele Stat',
#                        '({})'.format('tau_0_'+scale_factors['Tau']['BDTEleOLR']['trueelec_stat_up']),
#                        '({})'.format('tau_0_'+scale_factors['Tau']['BDTEleOLR']['trueelec_stat_down']),
#                        '({})'.format('tau_0_'+scale_factors['Tau']['BDTEleOLR']['nominal']))

sys_lead_tau_eleolr_trueelectron_bdt_stat = Systematics.weightSystematics( 'TAUS_TRUEELECTRON_EFF_ELEBDT_STAT',  'Tau EleOLR True Ele Stat',
                        '({})'.format('tau_0_'+scale_factors['Tau']['BDTEleOLR']['trueelec_stat_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['BDTEleOLR']['trueelec_stat_down']),
                        '(EvetoFactorHelper::get_evetofactor({0}, {1}, {2}, {3}, {4}, {5}))'.format(
                            'tau_0_p4.Pt()',
                            'tau_0_p4.Eta()',
                            'n_electrons',
                            'tau_0_n_charged_tracks',
                            'tau_0_matched_pdgId',
                            'tau_0_decay_mode'))
                        
#for moun e-veto bdt sf:
sys_lead_tau_eleolr_trueelectron_bdt_stat2 = Systematics.weightSystematics( 'TAUS_TRUEELECTRON_EFF_ELEBDT_STAT',  'Tau EleOLR True Ele Stat',
                        '({})'.format('tau_0_'+scale_factors['Tau']['BDTEleOLR']['trueelec_stat_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['BDTEleOLR']['trueelec_stat_down']),
                        '(EvetoFactorHelper::get_evetofactor({0}, {1}, {2}, {3}, {4}, {5}))'.format(
                            'tau_0_p4.Pt()',
                            'tau_0_p4.Eta()',
                            'n_muons',
                            'tau_0_n_charged_tracks',
                            'tau_0_matched_pdgId',
                            'tau_0_decay_mode'))

#for FF:
gal_sys_anti_tau_nonVBF_ff = Systematics.weightSystematics( 'gal_anti_tau_nonVBF_ff',  'gal_anti_tau_nonVBF_ff',
                        '({})'.format('tau_0_'+scale_factors['Tau']['BDTEleOLR']['trueelec_stat_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['BDTEleOLR']['trueelec_stat_down']),
                        # '(EvetoFactorHelper::get_anti_tau_nonVBF_ff({0}, {1}, {2}, {3}))'.format(
                        #     'tau_0_p4.Pt()',
                        #     'tau_0_p4.Eta()',
                        #     'tau_1',
                        #     'tau_0_n_charged_tracks'))
                        'ff_nominal')

#for lep FF:
gal_sys_anti_lep_ff = Systematics.weightSystematics( 'gal_anti_lep_ff',  'gal_anti_lep_ff',
                        '({})'.format('tau_0_'+scale_factors['Tau']['BDTEleOLR']['trueelec_stat_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['BDTEleOLR']['trueelec_stat_down']),
                        '(EvetoFactorHelper::get_anti_lep_ff({0}, {1}, {2}))'.format(
                            'tau_1_p4.Pt()',
                            'tau_1_p4.Eta()',
                            'tau_1'))

sys_lead_tau_eleolr_trueelectron_bdt_syst = Systematics.weightSystematics( 'TAUS_TRUEELECTRON_EFF_ELEBDT_SYST',  'Tau EleOLR True Ele Syst',
                        '({})'.format('tau_0_'+scale_factors['Tau']['BDTEleOLR']['trueelec_syst_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['BDTEleOLR']['trueelec_syst_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['BDTEleOLR']['nominal']))

sys_lead_tau_looseeleolr_trueelectron_bdt_stat = Systematics.weightSystematics( 'TAUS_TRUEELECTRON_EFF_LOOSEELEBDT_STAT',  'Tau LooseEleOLR True Ele Stat',
                        '({})'.format('tau_0_'+scale_factors['Tau']['LooseBDTEleOLR']['trueelec_stat_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['LooseBDTEleOLR']['trueelec_stat_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['LooseBDTEleOLR']['nominal']))

sys_lead_tau_looseeleolr_trueelectron_bdt_syst = Systematics.weightSystematics( 'TAUS_TRUEELECTRON_EFF_LOOSEELEBDT_SYST',  'Tau LooseEleOLR True Ele Syst',
                        '({})'.format('tau_0_'+scale_factors['Tau']['LooseBDTEleOLR']['trueelec_syst_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['LooseBDTEleOLR']['trueelec_syst_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['LooseBDTEleOLR']['nominal']))

sys_sublead_tau_looseeleolr_trueelectron_bdt_stat = Systematics.weightSystematics( 'TAUS_TRUEELECTRON_EFF_LOOSEELEBDT_STAT',  'Tau LooseEleOLR True Ele Stat',
                        '({})'.format('tau_1_'+scale_factors['Tau']['LooseBDTEleOLR']['trueelec_stat_up']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['LooseBDTEleOLR']['trueelec_stat_down']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['LooseBDTEleOLR']['nominal']))

sys_sublead_tau_looseeleolr_trueelectron_bdt_syst = Systematics.weightSystematics( 'TAUS_TRUEELECTRON_EFF_LOOSEELEBDT_SYST',  'Tau LooseEleOLR True Ele Syst',
                        '({})'.format('tau_1_'+scale_factors['Tau']['LooseBDTEleOLR']['trueelec_syst_up']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['LooseBDTEleOLR']['trueelec_syst_down']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['LooseBDTEleOLR']['nominal']))

sys_lead_tau_eleolr_truehadtau = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL',  'Tau EleOLR True HadTau ', 
                        '({})'.format('tau_0_'+scale_factors['Tau']['TauOLR']['truehadtau_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['TauOLR']['truehadtau_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['TauOLR']['nominal']))

sys_taus_eleolr_truehadtau  = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL',  'Tau EleOLR True HadTau ',
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['TauOLR']['truehadtau_up'],'tau_1_'+scale_factors['Tau']['TauOLR']['truehadtau_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['TauOLR']['truehadtau_down'],'tau_1_'+scale_factors['Tau']['TauOLR']['truehadtau_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['TauOLR']['nominal'],'tau_1_'+scale_factors['Tau']['TauOLR']['nominal']))

syst_lead_tau_jetid_1p_pt2025_lh = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025',  'Tau ID (medium) 1p_pt2025',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2025_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2025_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_1p_pt2025_hh = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025',  'Tau ID (medium) 1p_pt2025',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2025_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2025_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_sublead_tau_jetid_1p_pt2025_hh = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025',  'Tau ID (medium) 1p_pt2025',
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2025_up']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2025_down']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_taus_jetid_1p_pt2025_hh  = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025',  'Tau ID (medium-medium) 1p_pt2025',
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2025_up'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2025_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2025_down'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2025_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_1p_pt2530_lh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530',  'Tau ID (medium) 1p_pt2530',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2530_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2530_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_1p_pt2530_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530',  'Tau ID (medium) 1p_pt2530',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2530_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2530_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_sublead_tau_jetid_1p_pt2530_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530',  'Tau ID (medium) 1p_pt2530',
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2530_up']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2530_down']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_taus_jetid_1p_pt2530_hh   = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530',  'Tau ID (medium-medium) 1p_pt2530',
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2530_up'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2530_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2530_down'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2530_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_1p_pt3040_lh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040',  'Tau ID (medium) 1p_pt3040',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt3040_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt3040_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_1p_pt3040_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040',  'Tau ID (medium) 1p_pt3040',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt3040_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt3040_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_sublead_tau_jetid_1p_pt3040_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040',  'Tau ID (medium) 1p_pt3040',
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt3040_up']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt3040_down']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_taus_jetid_1p_pt3040_hh   = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040',  'Tau ID (medium-medium) 1p_pt3040',
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt3040_up'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt3040_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt3040_down'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt3040_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_1p_pt40_lh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40',  'Tau ID (medium) 1p_pt40',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt40_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt40_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_1p_pt40_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40',  'Tau ID (medium) 1p_pt40',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt40_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt40_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_sublead_tau_jetid_1p_pt40_hh    = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40',  'Tau ID (medium) 1p_pt40',
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt40_up']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt40_down']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_taus_jetid_1p_pt40_hh   = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40',  'Tau ID (medium-medium) 1p_pt40',
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt40_up'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt40_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt40_down'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt40_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_3p_pt2025_lh = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025',  'Tau ID (medium) 3p_pt2025',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2025_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2025_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_3p_pt2025_hh = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025',  'Tau ID (medium) 3p_pt2025',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2025_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2025_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_sublead_tau_jetid_3p_pt2025_hh = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025',  'Tau ID (medium) 3p_pt2025',
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt2025_up']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt2025_down']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_taus_jetid_3p_pt2025_hh  = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025',  'Tau ID (medium-medium) 3p_pt2025',
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2025_up'],'tau_1_'+scale_factors['Tau']['mediumID']['3p_pt2025_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2025_down'],'tau_1_'+scale_factors['Tau']['mediumID']['3p_pt2025_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_3p_pt2530_lh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530',  'Tau ID (medium) 3p_pt2530',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2530_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2530_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_3p_pt2530_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530',  'Tau ID (medium) 3p_pt2530',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2530_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2530_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_sublead_tau_jetid_3p_pt2530_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530',  'Tau ID (medium) 3p_pt2530',
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt2530_up']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt2530_down']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_taus_jetid_3p_pt2530_hh   = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530',  'Tau ID (medium-medium) 3p_pt2530',
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2530_up'],'tau_1_'+scale_factors['Tau']['mediumID']['3p_pt2530_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2530_down'],'tau_1_'+scale_factors['Tau']['mediumID']['3p_pt2530_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_3p_pt3040_lh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040',  'Tau ID (medium) 3p_pt3040',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt3040_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt3040_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_3p_pt3040_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040',  'Tau ID (medium) 3p_pt3040',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt3040_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt3040_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_sublead_tau_jetid_3p_pt3040_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040',  'Tau ID (medium) 3p_pt3040',
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt3040_up']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt3040_down']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_taus_jetid_3p_pt3040_hh   = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040',  'Tau ID (medium-medium) 3p_pt3040',
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt3040_up'],'tau_1_'+scale_factors['Tau']['mediumID']['3p_pt3040_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt3040_down'],'tau_1_'+scale_factors['Tau']['mediumID']['3p_pt3040_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_3p_pt40_lh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40',  'Tau ID (medium) 3p_pt40',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt40_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt40_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_3p_pt40_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40',  'Tau ID (medium) 3p_pt40',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt40_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt40_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_sublead_tau_jetid_3p_pt40_hh    = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40',  'Tau ID (medium) 3p_pt40',
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt40_up']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt40_down']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_taus_jetid_3p_pt40_hh   = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40',  'Tau ID (medium-medium) 3p_pt40',
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt40_up'],'tau_1_'+scale_factors['Tau']['mediumID']['3p_pt40_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt40_down'],'tau_1_'+scale_factors['Tau']['mediumID']['3p_pt40_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']))


syst_tau_jetid_syst_lh = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_SYST',  'Tau ID (medium) syst',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['syst_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['syst_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_taus_jetid_syst_hh = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_SYST',  'Tau ID (medium-medium) syst',
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['syst_up'],'tau_1_'+scale_factors['Tau']['mediumID']['syst_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['syst_down'],'tau_1_'+scale_factors['Tau']['mediumID']['syst_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_syst_hh = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_SYST',  'Tau ID (medium-medium) syst',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['syst_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['syst_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_sublead_tau_jetid_syst_hh = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_SYST',  'Tau ID (medium-medium) syst',
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['syst_up']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['syst_down']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_tau_jetid_highpt_lh = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT',  'Tau ID (medium) highpt',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['highpt_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['highpt_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_taus_jetid_highpt_hh = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT',  'Tau ID (medium-medium) highpt',
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['highpt_up'],'tau_1_'+scale_factors['Tau']['mediumID']['highpt_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['highpt_down'],'tau_1_'+scale_factors['Tau']['mediumID']['highpt_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']))

syst_lead_tau_jetid_highpt_hh = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT',  'Tau ID (medium-medium) highpt',
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['highpt_up']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['highpt_down']),
                        '({})'.format('tau_0_'+scale_factors['Tau']['mediumID']['nominal']))

syst_sublead_tau_jetid_highpt_hh = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT',  'Tau ID (medium-medium) highpt',
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['highpt_up']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['highpt_down']),
                        '({})'.format('tau_1_'+scale_factors['Tau']['mediumID']['nominal']))


weight_common_TauSys_lh = DecoratedSystematics(
                  'tau_common_weight_systematics_lh',
                  'Tau common weight systematics collection for LH',
                  systSet = SystematicsSet(set([ sys_lead_tau_reco_total, 
                                                 #sys_lead_tau_eleolr_truehadtau,
                                               ])),
                  channels=['e1p','e3p','mu1p','mu3p', 'ehad', 'muhad'])
SYSTBOOK.append(weight_common_TauSys_lh)

weight_common_TauSys_hh = DecoratedSystematics(
                  'tau_common_weight_systematics_hh',
                  'Tau common weight systematics collection for HH',
                  systSet = SystematicsSet(set([ sys_taus_reco_total_hh,
                                                 sys_taus_eleolr_truehadtau,
                                               ])),
                  channels=['1p1p','1p3p','3p1p','3p3p'],)
SYSTBOOK.append(weight_common_TauSys_hh)

weight_id_1p_TauSys_lh = DecoratedSystematics(
                  'tau_id_1p_weight_systematics_lh',
                  'Tau ID 1 prong weight systematics collection for LH',
                  systSet = SystematicsSet(set([ syst_tau_jetid_syst_lh,
                                                 syst_tau_jetid_highpt_lh,
                                                 syst_lead_tau_jetid_1p_pt2025_lh,
                                                 syst_lead_tau_jetid_1p_pt2530_lh,
                                                 syst_lead_tau_jetid_1p_pt3040_lh,
                                                 syst_lead_tau_jetid_1p_pt40_lh])),
                                                
                  channels=['e1p','mu1p', 'ehad', 'muhad'],
                  regions=filter(lambda t: t not in ('anti_tau', 'qcd_lh_anti_tau', 'qcd_lh_same_sign_anti_tau', 'qcd_lh_anti_tau_truth_lep', 'qcd_lh_same_sign_anti_tau_truth_lep', 'W_lh_anti_tau', 'top_anti_tau','iso_fact_lh_same_sign_anti_tau_num','iso_fact_lh_same_sign_anti_tau_den','iso_fact_lh_os_anti_tau_num','iso_fact_lh_os_anti_tau_den'), REGIONS)) 
SYSTBOOK.append(weight_id_1p_TauSys_lh)

weight_id_3p_TauSys_lh = DecoratedSystematics(
                  'tau_id_3p_weight_systematics_lh',
                  'Tau ID 3 prong weight systematics collection for LH',
                  systSet = SystematicsSet(set([ syst_tau_jetid_syst_lh,
                                                 syst_tau_jetid_highpt_lh,
                                                 syst_lead_tau_jetid_3p_pt2025_lh,
                                                 syst_lead_tau_jetid_3p_pt2530_lh,
                                                 syst_lead_tau_jetid_3p_pt3040_lh,
                                                 syst_lead_tau_jetid_3p_pt40_lh])),
                  channels=['e3p','mu3p', 'ehad', 'muhad'],
                  regions=filter(lambda t: t not in ('anti_tau', 'qcd_lh_anti_tau', 'qcd_lh_same_sign_anti_tau', 'qcd_lh_anti_tau_truth_lep', 'qcd_lh_same_sign_anti_tau_truth_lep', 'W_lh_anti_tau', 'top_anti_tau', 'iso_fact_lh_same_sign_anti_tau_num','iso_fact_lh_same_sign_anti_tau_den','iso_fact_lh_os_anti_tau_num','iso_fact_lh_os_anti_tau_den'), REGIONS))
SYSTBOOK.append(weight_id_3p_TauSys_lh)

weight_e1p_eveto_lh = DecoratedSystematics(
                  'tau_eveto_ehad_1p_systematics_lh',
                  'Tau eveto weight systematics collection for LH 1 prong',
                  systSet = SystematicsSet(set([ sys_lead_tau_eleolr_trueelectron_bdt_stat])),
#                                                 sys_lead_tau_eleolr_trueelectron_bdt_syst])),
                  channels=['e1p', 'ehad'])
SYSTBOOK.append(weight_e1p_eveto_lh)



# #gal:moun bdt SF for e-veto:
if gconfig.isAC==0:
  weight_e1p_eveto_lh2 = DecoratedSystematics(
                    'tau_eveto_ehad_1p_systematics_lh2',
                    'Tau eveto weight systematics collection for LH 1 prong2',
                    systSet = SystematicsSet(set([ sys_lead_tau_eleolr_trueelectron_bdt_stat2])),
  #                                                 sys_lead_tau_eleolr_trueelectron_bdt_syst])),
                    channels=['mu1p','muhad'])
  SYSTBOOK.append(weight_e1p_eveto_lh2)

anti_tau_ff = DecoratedSystematics(
                  'anti_tau_ff',
                  'anti_tau_ff',
                  systSet = SystematicsSet(set([ gal_sys_anti_tau_nonVBF_ff])),
                  channels=['e1p', 'e3p','mu1p','mu3p','ehad','muhad'],
                  categories=['vbf_0','vbf_1'],
                  regions=['j_fake_tau','j_fake_tau_same_sign'],
                  force_data_application=True)
SYSTBOOK.append(anti_tau_ff)

anti_lep_ff = DecoratedSystematics(
                  'anti_lep_ff',
                  'anti_lep_ff',
                  systSet = SystematicsSet(set([ gal_sys_anti_lep_ff])),
                  channels=['e1p', 'e3p','mu1p','mu3p','ehad','muhad'],
                  regions=['fake_lep','fake_lep_same_sign'],
                  force_data_application=True)
SYSTBOOK.append(anti_lep_ff)







weight_lead_eveto_hh = DecoratedSystematics(
                  'tau_eveto_1p_lead_systematics_hh',
                  'Leading tau eveto weight systematics collection for HH 1 prong',
                  systSet = SystematicsSet(set([ sys_lead_tau_looseeleolr_trueelectron_bdt_stat,
                                                 sys_lead_tau_looseeleolr_trueelectron_bdt_syst])),
                  channels=['1p1p','1p3p'],
                  categories=['tth','tth_0','tth_1']
    )
SYSTBOOK.append(weight_lead_eveto_hh)

weight_sublead_eveto_hh = DecoratedSystematics(
                  'tau_eveto_1p_sublead_systematics_hh',
                  'Suleading tau eveto weight systematics collection for HH 1 prong',
                  systSet = SystematicsSet(set([ sys_sublead_tau_looseeleolr_trueelectron_bdt_stat,
                                                 sys_sublead_tau_looseeleolr_trueelectron_bdt_syst])),
                  channels=['1p1p','3p1p'],
                  categories=['tth','tth_0','tth_1']
    )
SYSTBOOK.append(weight_sublead_eveto_hh)

weight_id_1p1p_TauSys_hh = DecoratedSystematics(
                  'tau_id_1p1p_weight_systematics_hh',
                  'Tau ID lead 1 prong sublead 1 prong weight systematics collection for HH',
                  systSet = SystematicsSet(set([ syst_taus_jetid_syst_hh,
                                                 syst_taus_jetid_highpt_hh,
                                                 syst_taus_jetid_1p_pt2025_hh,
                                                 syst_taus_jetid_1p_pt2530_hh,
                                                 syst_taus_jetid_1p_pt3040_hh,
                                                 syst_taus_jetid_1p_pt40_hh])),
                  channels=['1p1p'])
SYSTBOOK.append(weight_id_1p1p_TauSys_hh)

weight_id_3p3p_TauSys_hh = DecoratedSystematics(
                  'tau_id_3p3p_weight_systematics_hh',
                  'Tau ID lead 3 prong sublead 3 prong weight systematics collection for HH',
                  systSet = SystematicsSet(set([ syst_taus_jetid_syst_hh,
                                                 syst_taus_jetid_highpt_hh,
                                                 syst_taus_jetid_3p_pt2025_hh,
                                                 syst_taus_jetid_3p_pt2530_hh,
                                                 syst_taus_jetid_3p_pt3040_hh,
                                                 syst_taus_jetid_3p_pt40_hh])),
                  channels=['3p3p'])
SYSTBOOK.append(weight_id_3p3p_TauSys_hh)

weight_id_1p3p_TauSys_hh = DecoratedSystematics(
                  'tau_id_1p3p_weight_systematics_hh',
                  'Tau ID lead 1 prong sublead 3 prong weight systematics collection for HH',
                  systSet = SystematicsSet(set([ syst_lead_tau_jetid_syst_hh,
                                                 syst_sublead_tau_jetid_syst_hh,
                                                 syst_lead_tau_jetid_highpt_hh,
                                                 syst_sublead_tau_jetid_highpt_hh,
                                                 syst_lead_tau_jetid_1p_pt2025_hh,
                                                 syst_lead_tau_jetid_1p_pt2530_hh,
                                                 syst_lead_tau_jetid_1p_pt3040_hh,
                                                 syst_lead_tau_jetid_1p_pt40_hh,
                                                 syst_sublead_tau_jetid_3p_pt2025_hh,
                                                 syst_sublead_tau_jetid_3p_pt2530_hh,
                                                 syst_sublead_tau_jetid_3p_pt3040_hh,
                                                 syst_sublead_tau_jetid_3p_pt40_hh,
                                                ])),
                  channels=['1p3p'])
SYSTBOOK.append(weight_id_1p3p_TauSys_hh)

weight_id_3p1p_TauSys_hh = DecoratedSystematics(
                  'tau_id_3p1p_weight_systematics_hh',
                  'Tau ID lead 3 prong sublead 1 prong weight systematics collection for HH',
                  systSet = SystematicsSet(set([ syst_lead_tau_jetid_syst_hh,
                                                 syst_sublead_tau_jetid_syst_hh,
                                                 syst_lead_tau_jetid_highpt_hh,
                                                 syst_sublead_tau_jetid_highpt_hh,
                                                 syst_sublead_tau_jetid_1p_pt2025_hh,
                                                 syst_sublead_tau_jetid_1p_pt2530_hh,
                                                 syst_sublead_tau_jetid_1p_pt3040_hh,
                                                 syst_sublead_tau_jetid_1p_pt40_hh,
                                                 syst_lead_tau_jetid_3p_pt2025_hh,
                                                 syst_lead_tau_jetid_3p_pt2530_hh,
                                                 syst_lead_tau_jetid_3p_pt3040_hh,
                                                 syst_lead_tau_jetid_3p_pt40_hh,                 
                                               ])),
                  channels=['3p1p'])
SYSTBOOK.append(weight_id_3p1p_TauSys_hh)

