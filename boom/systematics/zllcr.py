from .base import DecoratedSystematics, SYSTBOOK
from happy.systematics import SystematicsSet, Systematics
from ..zll_pt import _random_offset, tau_0_4vec, tau_1_4vec


sys_weight_zll_mumu_cr = Systematics.weightSystematics(
    'zll_cr_effmumu', 
    'Light Lepton Efficiencies',
    '({})'.format('1./(PtCorrHelper::weight_mu_trig(tau_0_p4.Pt(), tau_0_p4.Eta())*PtCorrHelper::weight_mu_reco(tau_0_p4.Pt(), tau_0_p4.Eta())*PtCorrHelper::weight_mu_reco(tau_1_p4.Pt(), tau_1_p4.Eta()))'),
    '({})'.format('1./(PtCorrHelper::weight_mu_trig(tau_0_p4.Pt(), tau_0_p4.Eta())*PtCorrHelper::weight_mu_reco(tau_0_p4.Pt(), tau_0_p4.Eta())*PtCorrHelper::weight_mu_reco(tau_1_p4.Pt(), tau_1_p4.Eta()))'),
    '({})'.format('1./(PtCorrHelper::weight_mu_trig(tau_0_p4.Pt(), tau_0_p4.Eta())*PtCorrHelper::weight_mu_reco(tau_0_p4.Pt(), tau_0_p4.Eta())*PtCorrHelper::weight_mu_reco(tau_1_p4.Pt(), tau_1_p4.Eta()))'))

sys_weight_zll_ee_cr   = Systematics.weightSystematics(
    'zll_cr_effee', 'Light Lepton Efficiencies',
    '({})'.format('1./(PtCorrHelper::weight_ele_trig(tau_0_p4.Pt(), tau_0_p4.Eta())*PtCorrHelper::weight_ele_reco(tau_0_p4.Pt(), tau_0_p4.Eta())*PtCorrHelper::weight_ele_reco(tau_1_p4.Pt(), tau_1_p4.Eta()))'),
    '({})'.format('1./(PtCorrHelper::weight_ele_trig(tau_0_p4.Pt(), tau_0_p4.Eta())*PtCorrHelper::weight_ele_reco(tau_0_p4.Pt(), tau_0_p4.Eta())*PtCorrHelper::weight_ele_reco(tau_1_p4.Pt(), tau_1_p4.Eta()))'),
    '({})'.format('1./(PtCorrHelper::weight_ele_trig(tau_0_p4.Pt(), tau_0_p4.Eta())*PtCorrHelper::weight_ele_reco(tau_0_p4.Pt(), tau_0_p4.Eta())*PtCorrHelper::weight_ele_reco(tau_1_p4.Pt(), tau_1_p4.Eta()))'))

sys_weight_zll_folded_cr = Systematics.weightSystematics(
    'zll_cr_folding', 'Folding Efficiencies',
    '({})'.format('PtCorrHelper::weight_folded_event('+','.join(tau_0_4vec)+','.join(tau_1_4vec)+'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number))'),
    '({})'.format('PtCorrHelper::weight_folded_event('+','.join(tau_0_4vec)+','.join(tau_1_4vec)+'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number))'),
    '({})'.format('PtCorrHelper::weight_folded_event('+','.join(tau_0_4vec)+','.join(tau_1_4vec)+'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number))'))

_qxq_expr = '(PtCorrHelper::weight_qxq({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}))'.format(
    'tau_0_p4.Pt()',
    'tau_0_p4.Eta()',
    'tau_0_p4.Phi()',
    'tau_1_p4.Pt()',
    'tau_1_p4.Eta()',
    'tau_1_p4.Phi()',
    'event_number + ' + str(_random_offset),
    'PtCorrHelper::zll_choose_channel(event_number)')

sys_weight_zll_qxq = Systematics.weightSystematics(
    'zll_qxq_folding',
    'Folding qxq cut efficiency',
    _qxq_expr,
    _qxq_expr,
    _qxq_expr)


weight_zll_mumu_cr = DecoratedSystematics(
                     'unfold_zll_mumu_cr',
                     'Unfolded Light Lepton Efficiencies',
                     systSet = SystematicsSet(set([sys_weight_zll_mumu_cr])),
                     regions = ['Ztt_cr_corr_ll', 'Ztt_cr_corr_lh', 'Ztt_cr_corr_hh'],
                     channels = ['mumu'],
                     force_data_application=True)
SYSTBOOK.append(weight_zll_mumu_cr)

weight_zll_ee_cr = DecoratedSystematics(
                   'unfold_zll_ee_cr',
                   'Unfolded Light Lepton Efficiencies',
                   systSet = SystematicsSet(set([sys_weight_zll_ee_cr])),
                   regions = ['Ztt_cr_corr_ll', 'Ztt_cr_corr_lh', 'Ztt_cr_corr_hh'],
                   channels = ['ee'],
                   force_data_application=True)
SYSTBOOK.append(weight_zll_ee_cr)

weight_zll_fold_cr = DecoratedSystematics(
                     'fold_zll_ee_cr',
                     'Folded Lepton Efficiencies',
                     systSet = SystematicsSet(set([sys_weight_zll_folded_cr])),
                     regions = ['Ztt_cr_corr_ll', 'Ztt_cr_corr_lh', 'Ztt_cr_corr_hh'],
                     force_data_application=True)
SYSTBOOK.append(weight_zll_fold_cr)


weight_zll_qxq_cr = DecoratedSystematics(
                     'fold_zll_qxq_cr',
                     'Folded qxq cut efficiency',
                     systSet = SystematicsSet(set([sys_weight_zll_qxq])),
                     regions = ['Ztt_cr_corr_ll', 'Ztt_cr_corr_lh', 'Ztt_cr_corr_hh'],
                     force_data_application=True)
SYSTBOOK.append(weight_zll_qxq_cr)
