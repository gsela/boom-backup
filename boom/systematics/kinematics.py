from happy.systematics import SystematicsSet, Systematics

__all__ = ['kinematics']
    

KINEMATIC_VARIATIONS = [
    # egamma
    'EG_RESOLUTION_ALL',
    'EG_SCALE_ALL',
    # muons
    'MUON_MS',
    'MUON_ID',
    'MUON_SCALE',
    'MUON_SAGITTA_RESBIAS',
    'MUON_SAGITTA_RHO',
    # taus
    'TAUS_TRUEHADTAU_SME_TES_INSITU',
    'TAUS_TRUEHADTAU_SME_TES_MODEL',
    'TAUS_TRUEHADTAU_SME_TES_DETECTOR',
    # jets
    'JET_EtaIntercalibration_NonClosure_highE',
    'JET_EtaIntercalibration_NonClosure_negEta',
    'JET_EtaIntercalibration_NonClosure_posEta',
    'JET_EtaIntercalibration_Modelling',
    'JET_EtaIntercalibration_TotalStat',
    'JET_JER_DataVsMC',
    'JET_JER_EffectiveNP_1',
    'JET_JER_EffectiveNP_2',
    'JET_JER_EffectiveNP_3',
    'JET_JER_EffectiveNP_4',
    'JET_JER_EffectiveNP_5',
    'JET_JER_EffectiveNP_6',
    'JET_JER_EffectiveNP_7restTerm',
    'JET_BJES_Response', 
    'JET_Flavor_Composition', 
    'JET_Flavor_Response', 
    'JET_Pileup_OffsetMu', 
    'JET_Pileup_OffsetNPV', 
    'JET_Pileup_PtTerm', 
    'JET_Pileup_RhoTopology', 
    'JET_PunchThrough_MC16', 
    'JET_SingleParticle_HighPt', 
    'JET_EffectiveNP_Detector1',
    'JET_EffectiveNP_Detector2',
    'JET_EffectiveNP_Mixed1',
    'JET_EffectiveNP_Mixed2',
    'JET_EffectiveNP_Mixed3',
    'JET_EffectiveNP_Modelling1',
    'JET_EffectiveNP_Modelling2',
    'JET_EffectiveNP_Modelling3',
    'JET_EffectiveNP_Modelling4',
    'JET_EffectiveNP_Statistical1',
    'JET_EffectiveNP_Statistical2',
    'JET_EffectiveNP_Statistical3',
    'JET_EffectiveNP_Statistical4',
    'JET_EffectiveNP_Statistical5',
    'JET_EffectiveNP_Statistical6',
] 

kinematics = SystematicsSet()

for var_name in KINEMATIC_VARIATIONS:
    _upTreeName = '{}_1up'.format(var_name)
    _downTreeName = '{}_1down'.format(var_name)
    if 'JER' in var_name:
        _downTreeName = None
    variation = Systematics.treeSystematics(
        var_name,
        var_name.replace('_', ' '),
        upTreeName = _upTreeName,
        downTreeName = _downTreeName,
        nominalTreeName='NOMINAL')
    kinematics.add(variation)

# adding the MET soft terms

met_scale = Systematics.treeSystematics(
    'MET_SoftTrk_Scale',  
    'MET SoftTrk Scale',
    upTreeName='MET_SoftTrk_ScaleUp',
    downTreeName='MET_SoftTrk_ScaleDown',
    nominalTreeName='NOMINAL')

met_para = Systematics.treeSystematics( 
    'MET_SoftTrk_ResoPara',  
    'MET SoftTrk ResoPara',
    upTreeName='MET_SoftTrk_ResoPara',
    downTreeName=None,
    nominalTreeName='NOMINAL')

met_perp = Systematics.treeSystematics( 
    'MET_SoftTrk_ResoPerp',  
    'MET_Soft Reso Perp',
    upTreeName='MET_SoftTrk_ResoPerp',
    downTreeName=None,
    nominalTreeName='NOMINAL')


for variation in (met_scale, met_para, met_perp):
    kinematics.add(variation)
