from .base import DecoratedSystematics, SYSTBOOK
from .trigger_weight_dictionary import trigger_weights
from happy.systematics import SystematicsSet, Systematics

#############
# hh channel 
#############

sys_tau_trig_statdata_15   = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015', 'Tau Trigger 15 Stat Data (tight-tight)',
                            '({})'.format(trigger_weights['hh']['tt']['15']['statdata_up']), 
                            '({})'.format(trigger_weights['hh']['tt']['15']['statdata_down']),
                            '({})'.format(trigger_weights['hh']['tt']['15']['nominal']))

sys_tau_trig_statdata_16   = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016', 'Tau Trigger 16 Stat Data (tight-tight)',
                            '({})'.format(trigger_weights['hh']['tt']['16']['statdata_up']),      
                            '({})'.format(trigger_weights['hh']['tt']['16']['statdata_down']),
                            '({})'.format(trigger_weights['hh']['tt']['16']['nominal']))

sys_tau_trig_statdata_17   = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017', 'Tau Trigger 17 Stat Data (tight-tight)',
                            '({})'.format(trigger_weights['hh']['tt']['17']['statdata_up']),      
                            '({})'.format(trigger_weights['hh']['tt']['17']['statdata_down']),
                            '({})'.format(trigger_weights['hh']['tt']['17']['nominal']))

sys_tau_trig_statdata_18   = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018', 'Tau Trigger 18 Stat Data (tight-tight)',
                            '({})'.format(trigger_weights['hh']['tt']['18']['statdata_up']),
                            '({})'.format(trigger_weights['hh']['tt']['18']['statdata_down']),
                            '({})'.format(trigger_weights['hh']['tt']['18']['nominal']))

sys_tau_trig_statmc_15     = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015', 'Tau Trigger 15 Stat MC (tight-tight)',
                            '({})'.format(trigger_weights['hh']['tt']['15']['statmc_up']),      
                            '({})'.format(trigger_weights['hh']['tt']['15']['statmc_down']),
                            '({})'.format(trigger_weights['hh']['tt']['15']['nominal']))

sys_tau_trig_statmc_16     = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016', 'Tau Trigger 16 Stat MC (tight-tight)',
                            '({})'.format(trigger_weights['hh']['tt']['16']['statmc_up']),
                            '({})'.format(trigger_weights['hh']['tt']['16']['statmc_down']),
                            '({})'.format(trigger_weights['hh']['tt']['16']['nominal']))

sys_tau_trig_statmc_17     = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017', 'Tau Trigger 17 Stat MC (tight-tight)',
                            '({})'.format(trigger_weights['hh']['tt']['17']['statmc_up']),
                            '({})'.format(trigger_weights['hh']['tt']['17']['statmc_down']),
                            '({})'.format(trigger_weights['hh']['tt']['17']['nominal']))

sys_tau_trig_statmc_18     = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018', 'Tau Trigger 18 Stat MC (tight-tight)',
                            '({})'.format(trigger_weights['hh']['tt']['18']['statmc_up']),
                            '({})'.format(trigger_weights['hh']['tt']['18']['statmc_down']),
                            '({})'.format(trigger_weights['hh']['tt']['18']['nominal']))

sys_tau_trig_syst_15       = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015', 'Tau Trigger 15 Syst (tight-tight)',
                            '({})'.format(trigger_weights['hh']['tt']['15']['syst_up']),  
                            '({})'.format(trigger_weights['hh']['tt']['15']['syst_down']),
                            '({})'.format(trigger_weights['hh']['tt']['15']['nominal']))

sys_tau_trig_syst_16       = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016', 'Tau Trigger 16 Syst (tight-tight)',
                            '({})'.format(trigger_weights['hh']['tt']['16']['syst_up']),
                            '({})'.format(trigger_weights['hh']['tt']['16']['syst_down']),
                            '({})'.format(trigger_weights['hh']['tt']['16']['nominal']))

sys_tau_trig_syst_17       = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017', 'Tau Trigger 17 Syst (tight-tight)',
                            '({})'.format(trigger_weights['hh']['tt']['17']['syst_up']),
                            '({})'.format(trigger_weights['hh']['tt']['17']['syst_down']),
                            '({})'.format(trigger_weights['hh']['tt']['17']['nominal']))

sys_tau_trig_syst_18       = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018', 'Tau Trigger 18 Syst (tight-tight)',
                            '({})'.format(trigger_weights['hh']['tt']['18']['syst_up']),
                            '({})'.format(trigger_weights['hh']['tt']['18']['syst_down']),
                            '({})'.format(trigger_weights['hh']['tt']['18']['nominal']))

##################
# muhad lh channel
##################

sys_mu_slt_trig_stat_15   = Systematics.weightSystematics('MUON_EFF_TrigStatUncertainty',   'Muon Trigger Eff 15 SLT (Stat)',
                        '({})'.format(trigger_weights['lh']['slt']['mh']['15']['stat_up']),   
                        '({})'.format(trigger_weights['lh']['slt']['mh']['15']['stat_down']),
                        '({})'.format(trigger_weights['lh']['slt']['mh']['15']['nominal']))

sys_mu_slt_trig_stat_16   = Systematics.weightSystematics('MUON_EFF_TrigStatUncertainty',   'Muon Trigger Eff 16 SLT (Stat)',
                        '({})'.format(trigger_weights['lh']['slt']['mh']['16']['stat_up']),   
                        '({})'.format(trigger_weights['lh']['slt']['mh']['16']['stat_down']),
                        '({})'.format(trigger_weights['lh']['slt']['mh']['16']['nominal']))

sys_mu_slt_trig_stat_17   = Systematics.weightSystematics('MUON_EFF_TrigStatUncertainty',   'Muon Trigger Eff 17 SLT (Stat)',
                        '({})'.format(trigger_weights['lh']['slt']['mh']['17']['stat_up']),   
                        '({})'.format(trigger_weights['lh']['slt']['mh']['17']['stat_down']),
                        '({})'.format(trigger_weights['lh']['slt']['mh']['17']['nominal']))

sys_mu_slt_trig_stat_18   = Systematics.weightSystematics('MUON_EFF_TrigStatUncertainty',   'Muon Trigger Eff 18 SLT (Stat)',
                        '({})'.format(trigger_weights['lh']['slt']['mh']['18']['stat_up']),
                        '({})'.format(trigger_weights['lh']['slt']['mh']['18']['stat_down']),
                        '({})'.format(trigger_weights['lh']['slt']['mh']['18']['nominal']))

sys_mu_slt_trig_syst_15   = Systematics.weightSystematics('MUON_EFF_TrigSystUncertainty',   'Muon Trigger Eff 15 SLT (Syst)',
                        '({})'.format(trigger_weights['lh']['slt']['mh']['15']['syst_up']),
                        '({})'.format(trigger_weights['lh']['slt']['mh']['15']['syst_down']),
                        '({})'.format(trigger_weights['lh']['slt']['mh']['15']['nominal']))

sys_mu_slt_trig_syst_16   = Systematics.weightSystematics('MUON_EFF_TrigSystUncertainty',   'Muon Trigger Eff 16 SLT (Syst)',
                        '({})'.format(trigger_weights['lh']['slt']['mh']['16']['syst_up']),
                        '({})'.format(trigger_weights['lh']['slt']['mh']['16']['syst_down']),
                        '({})'.format(trigger_weights['lh']['slt']['mh']['16']['nominal']))

sys_mu_slt_trig_syst_17   = Systematics.weightSystematics('MUON_EFF_TrigSystUncertainty',   'Muon Trigger Eff 17 SLT (Syst)',
                        '({})'.format(trigger_weights['lh']['slt']['mh']['17']['syst_up']),
                        '({})'.format(trigger_weights['lh']['slt']['mh']['17']['syst_down']),
                        '({})'.format(trigger_weights['lh']['slt']['mh']['17']['nominal']))

sys_mu_slt_trig_syst_18   = Systematics.weightSystematics('MUON_EFF_TrigSystUncertainty',   'Muon Trigger Eff 18 SLT (Syst)',
                        '({})'.format(trigger_weights['lh']['slt']['mh']['18']['syst_up']),
                        '({})'.format(trigger_weights['lh']['slt']['mh']['18']['syst_down']),
                        '({})'.format(trigger_weights['lh']['slt']['mh']['18']['nominal']))

##################
# ehad lh channel
##################

sys_el_slt_trig_15   = Systematics.weightSystematics('EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',   'Elec Trigger Eff 15 SLT',
                        '({})'.format(trigger_weights['lh']['slt']['eh']['15']['total_up']),
                        '({})'.format(trigger_weights['lh']['slt']['eh']['15']['total_down']),
                        '({})'.format(trigger_weights['lh']['slt']['eh']['15']['nominal']))

sys_el_slt_trig_16   = Systematics.weightSystematics('EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',   'Elec Trigger Eff 16 SLT',
                        '({})'.format(trigger_weights['lh']['slt']['eh']['16']['total_up']),
                        '({})'.format(trigger_weights['lh']['slt']['eh']['16']['total_down']),
                        '({})'.format(trigger_weights['lh']['slt']['eh']['16']['nominal']))

sys_el_slt_trig_17    = Systematics.weightSystematics('EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',   'Elec Trigger Eff 17 SLT',
                        '({})'.format(trigger_weights['lh']['slt']['eh']['17']['total_up']),
                        '({})'.format(trigger_weights['lh']['slt']['eh']['17']['total_down']),
                        '({})'.format(trigger_weights['lh']['slt']['eh']['17']['nominal']))

sys_el_slt_trig_18    = Systematics.weightSystematics('EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',   'Elec Trigger Eff 18 SLT',
                        '({})'.format(trigger_weights['lh']['slt']['eh']['18']['total_up']),
                        '({})'.format(trigger_weights['lh']['slt']['eh']['18']['total_down']),
                        '({})'.format(trigger_weights['lh']['slt']['eh']['18']['nominal']))

############################
# emu-mue trig OR ll channel
############################	

sys_trig_or_elec                = Systematics.weightSystematics('EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR', 'emu-mue -  Elec Trigger Eff',
                                  '({})'.format(trigger_weights['ll']['trig-OR']['elec_total_up']),
                                  '({})'.format(trigger_weights['ll']['trig-OR']['elec_total_down']),
                                  '({})'.format(trigger_weights['ll']['trig-OR']['nominal']))

sys_trig_or_muon_stat           = Systematics.weightSystematics('MUON_EFF_TrigStatUncertainty', 'emu-mue - Muon Trigger Eff Stat',
                                  '({})'.format(trigger_weights['ll']['trig-OR']['muon_stat_up']),
                                  '({})'.format(trigger_weights['ll']['trig-OR']['muon_stat_down']),
                                  '({})'.format(trigger_weights['ll']['trig-OR']['nominal']))

sys_trig_or_muon_syst           = Systematics.weightSystematics('MUON_EFF_TrigSystUncertainty', 'emu-mue - Muon Trigger Eff Syst',
                                  '({})'.format(trigger_weights['ll']['trig-OR']['muon_syst_up']),
                                  '({})'.format(trigger_weights['ll']['trig-OR']['muon_syst_down']),
                                  '({})'.format(trigger_weights['ll']['trig-OR']['nominal']))

####################
# ee slt zll vr 
####################

sys_ee_slt_trig_elec_total_15   = Systematics.weightSystematics('EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',   'ee - Elec Trigger Eff 15 SLT',
                                  '({})'.format(trigger_weights['ll']['slt']['ee']['15']['elec_total_up']),
                                  '({})'.format(trigger_weights['ll']['slt']['ee']['15']['elec_total_down']),
                                  '({})'.format(trigger_weights['ll']['slt']['ee']['15']['nominal']))

sys_ee_slt_trig_elec_total_16   = Systematics.weightSystematics('EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',   'ee - Elec Trigger Eff 16 SLT',
                                  '({})'.format(trigger_weights['ll']['slt']['ee']['16']['elec_total_up']),
                                  '({})'.format(trigger_weights['ll']['slt']['ee']['16']['elec_total_down']),
                                  '({})'.format(trigger_weights['ll']['slt']['ee']['16']['nominal']))

sys_ee_slt_trig_elec_total_17   = Systematics.weightSystematics('EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',   'ee - Elec Trigger Eff 17 SLT',
                                  '({})'.format(trigger_weights['ll']['slt']['ee']['17']['elec_total_up']),
                                  '({})'.format(trigger_weights['ll']['slt']['ee']['17']['elec_total_down']),
                                  '({})'.format(trigger_weights['ll']['slt']['ee']['17']['nominal']))

sys_ee_slt_trig_elec_total_18   = Systematics.weightSystematics('EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',   'ee - Elec Trigger Eff 18 SLT',
                                  '({})'.format(trigger_weights['ll']['slt']['ee']['18']['elec_total_up']),
                                  '({})'.format(trigger_weights['ll']['slt']['ee']['18']['elec_total_down']),
                                  '({})'.format(trigger_weights['ll']['slt']['ee']['18']['nominal']))

####################
# mumu slt zll vr 
####################

sys_mumu_slt_trig_muon_stat_15   = Systematics.weightSystematics('MUON_EFF_TrigStatUncertainty',   'Mumu - Muon Trigger Eff 15 SLT (Stat)',
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['15']['muon_stat_up']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['15']['muon_stat_down']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['15']['nominal']))

sys_mumu_slt_trig_muon_stat_16   = Systematics.weightSystematics('MUON_EFF_TrigStatUncertainty',   'Mumu - Muon Trigger Eff 16 SLT (Stat)',
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['16']['muon_stat_up']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['16']['muon_stat_down']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['16']['nominal']))

sys_mumu_slt_trig_muon_stat_17   = Systematics.weightSystematics('MUON_EFF_TrigStatUncertainty',   'Mumu - Muon Trigger Eff 17 SLT (Stat)',
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['17']['muon_stat_up']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['17']['muon_stat_down']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['17']['nominal']))

sys_mumu_slt_trig_muon_stat_18   = Systematics.weightSystematics('MUON_EFF_TrigStatUncertainty',   'Mumu - Muon Trigger Eff 18 SLT (Stat)',
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['18']['muon_stat_up']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['18']['muon_stat_down']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['18']['nominal']))

sys_mumu_slt_trig_muon_syst_15   = Systematics.weightSystematics('MUON_EFF_TrigSystUncertainty',   'Mumu - Muon Trigger Eff 15 SLT (Syst)',
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['15']['muon_syst_up']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['15']['muon_syst_down']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['15']['nominal']))

sys_mumu_slt_trig_muon_syst_16   = Systematics.weightSystematics('MUON_EFF_TrigSystUncertainty',   'Mumu - Muon Trigger Eff 16 SLT (Syst)',
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['16']['muon_syst_up']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['16']['muon_syst_down']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['16']['nominal']))

sys_mumu_slt_trig_muon_syst_17   = Systematics.weightSystematics('MUON_EFF_TrigSystUncertainty',   'Mumu - Muon Trigger Eff 17 SLT (Syst)',
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['17']['muon_syst_up']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['17']['muon_syst_down']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['17']['nominal']))

sys_mumu_slt_trig_muon_syst_18   = Systematics.weightSystematics('MUON_EFF_TrigSystUncertainty',   'Mumu - Muon Trigger Eff 18 SLT (Syst)',
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['18']['muon_syst_up']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['18']['muon_syst_down']),
                                  '({})'.format(trigger_weights['ll']['slt']['mumu']['18']['nominal']))

weight_tau_trigger_151617 = DecoratedSystematics(
                        'tau_trigger_weight_systematics',
                        'Tau Trigger weight systematic collection',
                        systSet = SystematicsSet(set([ sys_tau_trig_statdata_15,  
                                                       sys_tau_trig_statmc_15,
                                                       sys_tau_trig_syst_15,
                                                       sys_tau_trig_statdata_16,
                                                       sys_tau_trig_statmc_16,
                                                       sys_tau_trig_syst_16,
                                                       sys_tau_trig_statdata_17,
                                                       sys_tau_trig_statmc_17,
                                                       sys_tau_trig_syst_17
                                                     ])),      
                        years = ['15','16','17'], 
                        channels=['1p1p','1p3p','3p1p','3p3p']) 
SYSTBOOK.append(weight_tau_trigger_151617)

weight_tau_trigger_18 = DecoratedSystematics(
                        'tau_trigger_weight_systematics',
                        'Tau Trigger weight systematic collection',
                        systSet = SystematicsSet(set([ 
                                                       sys_tau_trig_statdata_18,
                                                       sys_tau_trig_statmc_18,
                                                       sys_tau_trig_syst_18])),
                        years = ['18'], 
                        channels=['1p1p','1p3p','3p1p','3p3p'])
SYSTBOOK.append(weight_tau_trigger_18)



weight_muhad_trigger_15 = DecoratedSystematics(
                        'muhad_trigger_weight_systematics_15',
                        'Muhad Trigger weight systematic collection 15',
                        systSet = SystematicsSet(set([ sys_mu_slt_trig_stat_15, 
                                                       sys_mu_slt_trig_syst_15])),
                        years = ['15'],
                        channels=['mu1p','mu3p','muhad'])
SYSTBOOK.append(weight_muhad_trigger_15)

weight_muhad_trigger_16 = DecoratedSystematics(
                        'muhad_trigger_weight_systematics_16',
                        'Muhad Trigger weight systematic collection 16',
                        systSet = SystematicsSet(set([ sys_mu_slt_trig_stat_16,
                                                       sys_mu_slt_trig_syst_16])),
                        years = ['16'],
                        channels=['mu1p','mu3p','muhad'])
SYSTBOOK.append(weight_muhad_trigger_16)

weight_muhad_trigger_17 = DecoratedSystematics(
                        'muhad_trigger_weight_systematics_17',
                        'Muhad Trigger weight systematic collection 17',
                        systSet = SystematicsSet(set([ sys_mu_slt_trig_stat_17,
                                                       sys_mu_slt_trig_syst_17])),
                        years = ['17'],
                        channels=['mu1p','mu3p','muhad'])
SYSTBOOK.append(weight_muhad_trigger_17)

weight_muhad_trigger_18 = DecoratedSystematics(
                        'muhad_trigger_weight_systematics_18',
                        'Muhad Trigger weight systematic collection 18',
                        systSet = SystematicsSet(set([ sys_mu_slt_trig_stat_18,
                                                       sys_mu_slt_trig_syst_18])),
                        years = ['18'],
                        channels=['mu1p','mu3p','muhad'])
SYSTBOOK.append(weight_muhad_trigger_18)

weight_ehad_trigger_15 = DecoratedSystematics(
                        'ehad_trigger_weight_systematics_15',
                        'Ehad Trigger weight systematic collection 15',
                        systSet = SystematicsSet(set([ sys_el_slt_trig_15, 
                                                       ])),
                        years = ['15'],
                        channels=['e1p','e3p','ehad'])
SYSTBOOK.append(weight_ehad_trigger_15)

weight_ehad_trigger_16 = DecoratedSystematics(
                        'ehad_trigger_weight_systematics_16',
                        'Ehad Trigger weight systematic collection 16',
                        systSet = SystematicsSet(set([ sys_el_slt_trig_16,
                                                       ])),
                        years = ['16'],
                        channels=['e1p','e3p','ehad'])
SYSTBOOK.append(weight_ehad_trigger_16)

weight_ehad_trigger_17 = DecoratedSystematics(
                        'ehad_trigger_weight_systematics_17',
                        'Ehad Trigger weight systematic collection 17',
                        systSet = SystematicsSet(set([ sys_el_slt_trig_17,
                                                       ])),
                        years = ['17'],
                        channels=['e1p','e3p','ehad'])
SYSTBOOK.append(weight_ehad_trigger_17)

weight_ehad_trigger_18 = DecoratedSystematics(
                        'ehad_trigger_weight_systematics_18',
                        'Ehad Trigger weight systematic collection 18',
                        systSet = SystematicsSet(set([ sys_el_slt_trig_18,
                                                       ])),
                        years = ['18'],
                        channels=['e1p','e3p','ehad'])
SYSTBOOK.append(weight_ehad_trigger_18)

#leplep
weight_trig_or_trigger = DecoratedSystematics(
                        'emu_mue_trigger_or_weight_systematics',
                        'Emu mue trigger or weight systematic collection',
                         systSet = SystematicsSet(set([ sys_trig_or_elec,
                                                        sys_trig_or_muon_stat,
                                                        sys_trig_or_muon_syst,
                                                       ])),
                        years = ['15','16','17','18'],
                        triggers=['trig-OR'],
                        channels=['emu','mue'])
SYSTBOOK.append(weight_trig_or_trigger)



# zll vr
weight_ee_trigger_15 = DecoratedSystematics(
                        'ee_trigger_weight_systematics_15',
                        'ee Trigger weight systematic collection 15',
                        systSet = SystematicsSet(set([ 
                                                       sys_ee_slt_trig_elec_total_15,
                                                       ])),
                        years = ['15'],
                        triggers=['single-e'],
                        channels=['ee'])
SYSTBOOK.append(weight_ee_trigger_15)

weight_ee_trigger_16 = DecoratedSystematics(
                        'ee_trigger_weight_systematics_16',
                        'ee Trigger weight systematic collection 16',
                        systSet = SystematicsSet(set([ 
                                                       sys_ee_slt_trig_elec_total_16,
                                                       ])),
                        years = ['16'],
                        triggers=['single-e'],
                        channels=['ee'])
SYSTBOOK.append(weight_ee_trigger_16)

weight_ee_trigger_17 = DecoratedSystematics(
                        'ee_trigger_weight_systematics_17',
                        'ee Trigger weight systematic collection 17',
                        systSet = SystematicsSet(set([ 
                                                       sys_ee_slt_trig_elec_total_17,
                                                       ])),
                        years = ['17'],
                        triggers=['single-e'],
                        channels=['ee'])
SYSTBOOK.append(weight_ee_trigger_17)

weight_ee_trigger_18 = DecoratedSystematics(
                        'ee_trigger_weight_systematics_18',
                        'ee Trigger weight systematic collection 18',
                        systSet = SystematicsSet(set([
                                                       sys_ee_slt_trig_elec_total_18,
                                                       ])),
                        years = ['18'],
                        triggers=['single-e'],
                        channels=['ee'])
SYSTBOOK.append(weight_ee_trigger_18)

weight_mumu_trigger_15 = DecoratedSystematics(
                        'mumu_trigger_weight_systematics_15',
                        'Mumu Trigger weight systematic collection 15',
                        systSet = SystematicsSet(set([ sys_mumu_slt_trig_muon_stat_15,
                                                       sys_mumu_slt_trig_muon_syst_15,
                                                       ])),
                        years = ['15'],
                        triggers=['single-mu'],
                        channels=['mumu'])
SYSTBOOK.append(weight_mumu_trigger_15)

weight_mumu_trigger_16 = DecoratedSystematics(
                        'mumu_trigger_weight_systematics_16',
                        'Mumu Trigger weight systematic collection 16',
                        systSet = SystematicsSet(set([ sys_mumu_slt_trig_muon_stat_16,
                                                       sys_mumu_slt_trig_muon_syst_16,
                                                       ])),
                        years = ['16'],
                        triggers=['single-mu'],
                        channels=['mumu'])
SYSTBOOK.append(weight_mumu_trigger_16)

weight_mumu_trigger_17 = DecoratedSystematics(
                        'mumu_trigger_weight_systematics_17',
                        'Mumu Trigger weight systematic collection 17',
                        systSet = SystematicsSet(set([ sys_mumu_slt_trig_muon_stat_17,
                                                       sys_mumu_slt_trig_muon_syst_17,
                                                       ])),
                        years = ['17'],
                        triggers=['single-mu'],
                        channels=['mumu'])
SYSTBOOK.append(weight_mumu_trigger_17)

weight_mumu_trigger_18 = DecoratedSystematics(
                        'mumu_trigger_weight_systematics_18',
                        'Mumu Trigger weight systematic collection 18',
                        systSet = SystematicsSet(set([ sys_mumu_slt_trig_muon_stat_18,
                                                       sys_mumu_slt_trig_muon_syst_18,
                                                       ])),
                        years = ['18'],
                        triggers=['single-mu'],
                        channels=['mumu'])
SYSTBOOK.append(weight_mumu_trigger_18)

