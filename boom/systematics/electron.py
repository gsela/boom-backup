"""
"""
from ..cuts import REGIONS
from .base import DecoratedSystematics, SYSTBOOK
from .object_weight_dictionary import scale_factors
from happy.systematics import SystematicsSet, Systematics

##############
# leading elec
##############

sys_lead_el_reco     = Systematics.weightSystematics( 'EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR',  'Elec Reco Eff', 
                        '({})'.format('tau_0_'+scale_factors['Elec']['Reco']['total_up']),
                        '({})'.format('tau_0_'+scale_factors['Elec']['Reco']['total_down']),
                        '({})'.format('tau_0_'+scale_factors['Elec']['Reco']['nominal']))

sys_lead_el_id       = Systematics.weightSystematics( 'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR',  'Elec ID Eff',
                        '({})'.format('tau_0_'+scale_factors['Elec']['mediumID']['total_up']),
                        '({})'.format('tau_0_'+scale_factors['Elec']['mediumID']['total_down']),
                        '({})'.format('tau_0_'+scale_factors['Elec']['mediumID']['nominal']))

sys_lead_el_non_prompt_id = Systematics.weightSystematics( 'ATLAS_EL_EFF_ID_NONPROMPT_D0', 'Elec ID Non Prompt corr.',
                        '(({}*({}-1))+1)'.format('(tau_0 == 2 && abs(tau_0_matched_mother_pdgId) == 15)', scale_factors['Elec']['Lead_nonPrompt']['up']),
                        '(({}*({}-1))+1)'.format('(tau_0 == 2 && abs(tau_0_matched_mother_pdgId) == 15)', scale_factors['Elec']['Lead_nonPrompt']['nominal']),
                        '(({}*({}-1))+1)'.format('(tau_0 == 2 && abs(tau_0_matched_mother_pdgId) == 15)', scale_factors['Elec']['Lead_nonPrompt']['nominal']))

sys_lead_el_iso     = Systematics.weightSystematics( 'EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR',  'Elec Iso FCLoose Eff', 
                        '({})'.format('tau_0_'+scale_factors['Elec']['Iso']['total_up']),
                        '({})'.format('tau_0_'+scale_factors['Elec']['Iso']['total_down']),
                        '({})'.format('tau_0_'+scale_factors['Elec']['Iso']['nominal']))

sys_lead_el_charge_id_stat = Systematics.weightSystematics( 'EL_CHARGEID_STAT' , 'Ele Charge ID Stat',
                        '({})'.format('tau_0_'+scale_factors['Elec']['ChargeID']['stat_up']),
                        '({})'.format('tau_0_'+scale_factors['Elec']['ChargeID']['stat_down']),
                        '({})'.format('tau_0_'+scale_factors['Elec']['ChargeID']['nominal']))

sys_lead_el_charge_id_syst = Systematics.weightSystematics( 'EL_CHARGEID_SYStotal' , 'Ele Charge ID Stat',
                        '({})'.format('tau_0_'+scale_factors['Elec']['ChargeID']['syst_up']),
                        '({})'.format('tau_0_'+scale_factors['Elec']['ChargeID']['syst_down']),
                        '({})'.format('tau_0_'+scale_factors['Elec']['ChargeID']['nominal']))

#################
# subleading elec
#################

sys_sublead_el_reco  = Systematics.weightSystematics( 'EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR',  'Elec Reco Eff',
                        '({})'.format('tau_1_'+scale_factors['Elec']['Reco']['total_up']),
                        '({})'.format('tau_1_'+scale_factors['Elec']['Reco']['total_down']),
                        '({})'.format('tau_1_'+scale_factors['Elec']['Reco']['nominal']))

sys_sublead_el_id    = Systematics.weightSystematics( 'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR',  'Elec ID Eff',
                        '({})'.format('tau_1_'+scale_factors['Elec']['mediumID']['total_up']),
                        '({})'.format('tau_1_'+scale_factors['Elec']['mediumID']['total_down']),
                        '({})'.format('tau_1_'+scale_factors['Elec']['mediumID']['nominal']))

sys_sublead_el_iso   = Systematics.weightSystematics( 'EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR',  'Elec Iso FCLoose Eff',   
                        '({})'.format('tau_1_'+scale_factors['Elec']['Iso']['total_up']),
                        '({})'.format('tau_1_'+scale_factors['Elec']['Iso']['total_down']),
                        '({})'.format('tau_1_'+scale_factors['Elec']['Iso']['nominal']))

sys_sublead_el_non_prompt_id = Systematics.weightSystematics( 'ATLAS_EL_EFF_ID_NONPROMPT_D0', 'Elec ID Non Prompt corr.',
                        '(({}*({}-1))+1)'.format('(tau_1 == 2 && abs(tau_1_matched_mother_pdgId) == 15)', scale_factors['Elec']['Sublead_nonPrompt']['up']),
                        '(({}*({}-1))+1)'.format('(tau_1 == 2 && abs(tau_1_matched_mother_pdgId) == 15)', scale_factors['Elec']['Sublead_nonPrompt']['nominal']),
                        '(({}*({}-1))+1)'.format('(tau_1 == 2 && abs(tau_1_matched_mother_pdgId) == 15)', scale_factors['Elec']['Sublead_nonPrompt']['nominal']))

sys_sublead_el_charge_id_stat = Systematics.weightSystematics( 'EL_CHARGEID_STAT' , 'Ele Charge ID Stat',
                        '({})'.format('tau_1_'+scale_factors['Elec']['ChargeID']['stat_up']),
                        '({})'.format('tau_1_'+scale_factors['Elec']['ChargeID']['stat_down']),
                        '({})'.format('tau_1_'+scale_factors['Elec']['ChargeID']['nominal']))

sys_sublead_el_charge_id_syst = Systematics.weightSystematics( 'EL_CHARGEID_SYStotal' , 'Ele Charge ID Stat',
                        '({})'.format('tau_1_'+scale_factors['Elec']['ChargeID']['syst_up']),
                        '({})'.format('tau_1_'+scale_factors['Elec']['ChargeID']['syst_down']),
                        '({})'.format('tau_1_'+scale_factors['Elec']['ChargeID']['nominal']))

###################
# ee channel
###################

sys_ee_reco     = Systematics.weightSystematics( 'EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR',  'Elec Reco Eff',
                        '({}*{})'.format('tau_0_'+scale_factors['Elec']['Reco']['total_up'],'tau_1_'+scale_factors['Elec']['Reco']['total_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Elec']['Reco']['total_down'],'tau_1_'+scale_factors['Elec']['Reco']['total_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Elec']['Reco']['nominal'],'tau_1_'+scale_factors['Elec']['Reco']['nominal']))

sys_ee_id       = Systematics.weightSystematics( 'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR',  'Elec ID Eff',
                        '({}*{})'.format('tau_0_'+scale_factors['Elec']['mediumID']['total_up'],'tau_1_'+scale_factors['Elec']['mediumID']['total_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Elec']['mediumID']['total_down'],'tau_1_'+scale_factors['Elec']['mediumID']['total_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Elec']['mediumID']['nominal'],'tau_1_'+scale_factors['Elec']['mediumID']['nominal']))

sys_ee_iso     = Systematics.weightSystematics( 'EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR',  'Elec Iso FCLoose Eff',
                        '({}*{})'.format('tau_0_'+scale_factors['Elec']['Iso']['total_up'],'tau_1_'+scale_factors['Elec']['Iso']['total_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Elec']['Iso']['total_down'],'tau_1_'+scale_factors['Elec']['Iso']['total_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Elec']['Iso']['nominal'],'tau_1_'+scale_factors['Elec']['Iso']['nominal'] ))

sys_ee_charge_id_stat = Systematics.weightSystematics( 'EL_CHARGEID_STAT' , 'Ele Charge ID Stat',
                        '({}*{})'.format('tau_0_'+scale_factors['Elec']['ChargeID']['stat_up'],'tau_1_'+scale_factors['Elec']['ChargeID']['stat_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Elec']['ChargeID']['stat_down'],'tau_1_'+scale_factors['Elec']['ChargeID']['stat_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Elec']['ChargeID']['nominal'],'tau_1_'+scale_factors['Elec']['ChargeID']['nominal']))

sys_ee_charge_id_syst = Systematics.weightSystematics( 'EL_CHARGEID_SYStotal' , 'Ele Charge ID Stat',
                        '({}*{})'.format('tau_0_'+scale_factors['Elec']['ChargeID']['syst_up'],'tau_1_'+scale_factors['Elec']['ChargeID']['syst_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Elec']['ChargeID']['syst_down'],'tau_1_'+scale_factors['Elec']['ChargeID']['syst_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Elec']['ChargeID']['nominal'],'tau_1_'+scale_factors['Elec']['ChargeID']['nominal']))


weight_lead_el_sys = DecoratedSystematics(
             'lead_elec_weight_syst',
             'Leading electron weight systematic collection',
             systSet = SystematicsSet(set([ sys_lead_el_reco,
                                            sys_lead_el_id,
                                            sys_lead_el_iso,
                                            sys_lead_el_charge_id_stat])),
#                                            sys_lead_el_charge_id_syst])),
#                                            sys_lead_el_non_prompt_id])),
             channels=['emu','ee'])
SYSTBOOK.append(weight_lead_el_sys)
                                             
weight_sublead_el_id_sys = DecoratedSystematics(
             'sublead_elec_id_weight_syst',
             'Subleading electron ID weight systematic collection',
             systSet = SystematicsSet(set([ sys_sublead_el_reco,
                                            sys_sublead_el_id,
                                            sys_sublead_el_charge_id_stat])),
#                                            sys_sublead_el_charge_id_syst])),
#                                            sys_sublead_el_non_prompt_id])),
             channels=['mue','e1p','e3p','ee', 'ehad'])
SYSTBOOK.append(weight_sublead_el_id_sys)

weight_sublead_el_iso_sys = DecoratedSystematics(
             'sublead_elec_iso_weight_syst',
             'Subleading electron Iso weight systematic collection',
             systSet = SystematicsSet(set([ sys_sublead_el_iso])),
             channels=['mue','e1p','e3p','ee', 'ehad'],
             regions=filter(lambda t: t not in ('qcd_lh', 'qcd_lh_same_sign', 'qcd_lh_anti_tau', 'qcd_lh_same_sign_anti_tau', 'qcd_lh_anti_tau_truth_lep', 'qcd_lh_same_sign_anti_tau_truth_lep','anti_isol', 'same_sign_anti_isol', 'same_sign_top_anti_isol', 'iso_fact_lh_same_sign_anti_tau_den','iso_fact_lh_os_anti_tau_den'), REGIONS))
SYSTBOOK.append(weight_sublead_el_iso_sys)


weight_ee_sys = DecoratedSystematics(
             'ee_weight_syst',
             'electron weight systematic collection for ZCR ee final state',
             systSet = SystematicsSet(set([ sys_ee_reco,
                                            sys_ee_id,
                                            sys_ee_iso,
                                            sys_ee_charge_id_stat,
                                            sys_ee_charge_id_syst])),
             channels=['ee'])
SYSTBOOK.append(weight_ee_sys)

