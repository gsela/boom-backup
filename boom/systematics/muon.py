"""
"""
from ..cuts import REGIONS
from .base import DecoratedSystematics, SYSTBOOK
from .object_weight_dictionary import scale_factors
from happy.systematics import SystematicsSet, Systematics

#############
# leading mu
#############

sys_lead_mu_id_stat         = Systematics.weightSystematics('MUON_EFF_RECO_STAT',   'Muon ID Medium (Stat)',
                        '({})'.format('tau_0_'+scale_factors['Muon']['ID']['stat_up']), 
                        '({})'.format('tau_0_'+scale_factors['Muon']['ID']['stat_down']),
                        '({})'.format('tau_0_'+scale_factors['Muon']['ID']['nominal']))

sys_lead_mu_id_stat_lowpt   = Systematics.weightSystematics('MUON_EFF_RECO_STAT_LOWPT',   'Muon ID Medium (Stat)',
                        '({})'.format('tau_0_'+scale_factors['Muon']['ID']['stat_lowpt_up']),
                        '({})'.format('tau_0_'+scale_factors['Muon']['ID']['stat_lowpt_down']),
                        '({})'.format('tau_0_'+scale_factors['Muon']['ID']['nominal']))

sys_lead_mu_id_syst         = Systematics.weightSystematics( 'MUON_EFF_RECO_SYS',  'Muon ID Medium (Syst)', 
                        '({})'.format('tau_0_'+scale_factors['Muon']['ID']['syst_up']),
                        '({})'.format('tau_0_'+scale_factors['Muon']['ID']['syst_down']),
                        '({})'.format('tau_0_'+scale_factors['Muon']['ID']['nominal']))

sys_lead_mu_id_syst_lowpt   = Systematics.weightSystematics( 'MUON_EFF_RECO_SYS_LOWPT',  'Muon ID Medium (Syst)',
                        '({})'.format('tau_0_'+scale_factors['Muon']['ID']['syst_lowpt_up']),
                        '({})'.format('tau_0_'+scale_factors['Muon']['ID']['syst_lowpt_down']),
                        '({})'.format('tau_0_'+scale_factors['Muon']['ID']['nominal']))

sys_lead_mu_iso_stat       = Systematics.weightSystematics( 'MUON_EFF_ISO_STAT',  'Muon Iso IsoFCTightTrackOnly (Stat)', 
                        '({})'.format('tau_0_'+scale_factors['Muon']['Iso']['stat_up']),
                        '({})'.format('tau_0_'+scale_factors['Muon']['Iso']['stat_down']),
                        '({})'.format('tau_0_'+scale_factors['Muon']['Iso']['nominal']))

sys_lead_mu_iso_syst       = Systematics.weightSystematics( 'MUON_EFF_ISO_SYS',  'Muon Iso IsoFCTightTrackOnly (Syst)', 
                        '({})'.format('tau_0_'+scale_factors['Muon']['Iso']['syst_up']),
                        '({})'.format('tau_0_'+scale_factors['Muon']['Iso']['syst_down']),
                        '({})'.format('tau_0_'+scale_factors['Muon']['Iso']['nominal']))	

###############
# subleading mu
###############

sys_sublead_mu_id_stat       = Systematics.weightSystematics('MUON_EFF_RECO_STAT',   'Muon ID Medium (Stat)',
                        '({})'.format('tau_1_'+scale_factors['Muon']['ID']['stat_up']),
                        '({})'.format('tau_1_'+scale_factors['Muon']['ID']['stat_down']),
                        '({})'.format('tau_1_'+scale_factors['Muon']['ID']['nominal']))

sys_sublead_mu_id_stat_lowpt = Systematics.weightSystematics('MUON_EFF_RECO_STAT_LOWPT',   'Muon ID Medium (Stat)',
                        '({})'.format('tau_1_'+scale_factors['Muon']['ID']['stat_lowpt_up']),
                        '({})'.format('tau_1_'+scale_factors['Muon']['ID']['stat_lowpt_down']),
                        '({})'.format('tau_1_'+scale_factors['Muon']['ID']['nominal']))

sys_sublead_mu_id_syst       = Systematics.weightSystematics( 'MUON_EFF_RECO_SYS',  'Muon ID Medium (Syst)',
                        '({})'.format('tau_1_'+scale_factors['Muon']['ID']['syst_up']),
                        '({})'.format('tau_1_'+scale_factors['Muon']['ID']['syst_down']),
                        '({})'.format('tau_1_'+scale_factors['Muon']['ID']['nominal']))

sys_sublead_mu_id_syst_lowpt = Systematics.weightSystematics( 'MUON_EFF_RECO_SYS_LOWPT',  'Muon ID Medium (Syst)',
                        '({})'.format('tau_1_'+scale_factors['Muon']['ID']['syst_lowpt_up']),
                        '({})'.format('tau_1_'+scale_factors['Muon']['ID']['syst_lowpt_down']),
                        '({})'.format('tau_1_'+scale_factors['Muon']['ID']['nominal']))

sys_sublead_mu_iso_stat    = Systematics.weightSystematics( 'MUON_EFF_ISO_STAT',  'Muon Iso IsoFCTightTrackOnly (Stat)',
                        '({})'.format('tau_1_'+scale_factors['Muon']['Iso']['stat_up']),
                        '({})'.format('tau_1_'+scale_factors['Muon']['Iso']['stat_down']),
                        '({})'.format('tau_1_'+scale_factors['Muon']['Iso']['nominal']))

sys_sublead_mu_iso_syst    = Systematics.weightSystematics( 'MUON_EFF_ISO_SYS',  'Muon Iso IsoFCTightTrackOnly (Syst)',
                        '({})'.format('tau_1_'+scale_factors['Muon']['Iso']['syst_up']),
                        '({})'.format('tau_1_'+scale_factors['Muon']['Iso']['syst_down']),
                        '({})'.format('tau_1_'+scale_factors['Muon']['Iso']['nominal']))


##################
# mumu channel
##################

sys_mumu_id_stat         = Systematics.weightSystematics('MUON_EFF_RECO_STAT',   'Muon ID Medium (Stat)',
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['ID']['stat_up'],'tau_1_'+scale_factors['Muon']['ID']['stat_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['ID']['stat_down'],'tau_1_'+scale_factors['Muon']['ID']['stat_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['ID']['nominal'],'tau_1_'+scale_factors['Muon']['ID']['nominal']))

sys_mumu_id_stat_lowpt   = Systematics.weightSystematics('MUON_EFF_RECO_STAT_LOWPT',   'Muon ID Medium (Stat)',
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['ID']['stat_lowpt_up'],'tau_1_'+scale_factors['Muon']['ID']['stat_lowpt_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['ID']['stat_lowpt_down'],'tau_1_'+scale_factors['Muon']['ID']['stat_lowpt_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['ID']['nominal'],'tau_1_'+scale_factors['Muon']['ID']['nominal']))

sys_mumu_id_syst         = Systematics.weightSystematics( 'MUON_EFF_RECO_SYS',  'Muon ID Medium (Syst)',
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['ID']['syst_up'],'tau_1_'+scale_factors['Muon']['ID']['syst_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['ID']['syst_down'],'tau_1_'+scale_factors['Muon']['ID']['syst_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['ID']['nominal'],'tau_1_'+scale_factors['Muon']['ID']['nominal']))

sys_mumu_id_syst_lowpt   = Systematics.weightSystematics( 'MUON_EFF_RECO_SYS_LOWPT',  'Muon ID Medium (Syst)',
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['ID']['syst_lowpt_up'],'tau_1_'+scale_factors['Muon']['ID']['syst_lowpt_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['ID']['syst_lowpt_down'],'tau_1_'+scale_factors['Muon']['ID']['syst_lowpt_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['ID']['nominal'],'tau_1_'+scale_factors['Muon']['ID']['nominal'] ))

sys_mumu_iso_stat        = Systematics.weightSystematics( 'MUON_EFF_ISO_STAT',  'Muon Iso IsoFCTightTrackOnly (Stat)',
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['Iso']['stat_up'],'tau_1_'+scale_factors['Muon']['Iso']['stat_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['Iso']['stat_down'],'tau_1_'+scale_factors['Muon']['Iso']['stat_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['Iso']['nominal'],'tau_1_'+scale_factors['Muon']['Iso']['nominal']))

sys_mumu_iso_syst       = Systematics.weightSystematics( 'MUON_EFF_ISO_SYS',  'Muon Iso IsoFCTightTrackOnly (Syst)',
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['Iso']['syst_up'],'tau_1_'+scale_factors['Muon']['Iso']['syst_up']),
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['Iso']['syst_down'],'tau_1_'+scale_factors['Muon']['Iso']['syst_down']),
                        '({}*{})'.format('tau_0_'+scale_factors['Muon']['Iso']['nominal'],'tau_1_'+scale_factors['Muon']['Iso']['nominal']))




weight_lead_mu_syst = DecoratedSystematics(
                      'lead_mu_weight_syst', 
                      'Leading muon weight systematic collection',
                      systSet = SystematicsSet(set([ sys_lead_mu_id_stat,
                                                     sys_lead_mu_id_stat_lowpt,
                                                     sys_lead_mu_id_syst,
                                                     sys_lead_mu_id_syst_lowpt,
                                                     sys_lead_mu_iso_stat,
                                                     sys_lead_mu_iso_syst])),
                      channels=['mue']) 
SYSTBOOK.append(weight_lead_mu_syst)


weight_sublead_mu_id_syst = DecoratedSystematics(
                      'sublead_mu_id_weight_syst',
                      'Subleading muon weight systematic collection',
                      systSet = SystematicsSet(set([ sys_sublead_mu_id_stat,
                                                     sys_sublead_mu_id_stat_lowpt,
                                                     sys_sublead_mu_id_syst,
                                                     sys_sublead_mu_id_syst_lowpt])),
                      channels=['emu','mu1p','mu3p', 'muhad', 'mumu'])
SYSTBOOK.append(weight_sublead_mu_id_syst)


weight_sublead_mu_iso_syst = DecoratedSystematics(
                      'sublead_mu_iso_weight_syst',
                      'Subleading Iso muon weight systematic collection',
                      systSet = SystematicsSet(set([ sys_sublead_mu_iso_stat,
                                                     sys_sublead_mu_iso_syst])),
                      channels=['emu','mu1p','mu3p', 'muhad', 'mumu'],
                      regions=filter(lambda t: t not in ('qcd_lh', 'qcd_lh_same_sign', 'qcd_lh_anti_tau', 'qcd_lh_same_sign_anti_tai', 'qcd_lh_anti_tau_truth_lep', 'qcd_lh_same_sign_anti_tau_truth_lep', 'anti_isol', 'same_sign_anti_isol', 'same_sign_top_anti_isol', 'iso_fact_lh_same_sign_anti_tau_den','iso_fact_lh_os_anti_tau_den'), REGIONS))
SYSTBOOK.append(weight_sublead_mu_iso_syst)


weight_mumu_syst = DecoratedSystematics(
                      'mumu_weight_syst', 
                      'weight systematic collection for ZCR mumu final state',
                      systSet = SystematicsSet(set([ sys_mumu_id_stat,
                                                     sys_mumu_id_stat_lowpt,
                                                     sys_mumu_id_syst,
                                                     sys_mumu_id_syst_lowpt,
                                                     sys_mumu_iso_stat,
                                                     sys_mumu_iso_syst])),
                      channels=['mumu'])
SYSTBOOK.append(weight_mumu_syst)


                        
