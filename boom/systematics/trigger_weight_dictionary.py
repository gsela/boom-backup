trigger_weights = {
  
        'hh' : {
            'tt' : {
                '15' :  {  
                         'nominal'       : 'tau_0_NOMINAL_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_NOMINAL_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'statdata_down' : '1',
                         'statdata_up'   : '1',
                         'statmc_down'   : '1',
                         'statmc_up'     : '1',
                         'syst_down'     : '1',
                         'syst_up'       : '1',
                         'syst_mu_down'  : '1',
                         'syst_mu_up'    : '1',
                         
                         },
                '16' :  {  
                         'nominal'       : 'tau_0_NOMINAL_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_NOMINAL_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'statdata_down' : '1',
                         'statdata_up'   : '1',
                         'statmc_down'   : '1',
                         'statmc_up'     : '1',
                         'syst_down'     : '1',
                         'syst_up'       : '1',
                         'syst_mu_down'  : '1',
                         'syst_mu_up'    : '1',
                         }, 
                '17' :  {
                         'nominal'       : 'tau_0_NOMINAL_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_NOMINAL_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'statdata_down' : '1',
                         'statdata_up'   : '1',
                         'statmc_down'   : '1',
                         'statmc_up'     : '1',
                         'syst_down'     : '1',
                         'syst_up'       : '1',
                         'syst_mu_down'  : '1',
                         'syst_mu_up'    : '1',
                         },
                '18' :  {
                         'nominal'       : 'tau_0_NOMINAL_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_NOMINAL_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'statdata_down' : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018_1down_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018_1down_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'statdata_up'   : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018_1up_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018_1up_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'statmc_down'   : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018_1down_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018_1down_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'statmc_up'     : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018_1up_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018_1up_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'syst_down'     : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018_1down_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018_1down_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'syst_up'       : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018_1up_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018_1up_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'syst_mu_down'  : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018_1down_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018_1down_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'syst_mu_up'    : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018_1up_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018_1up_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         },

                    },
             },
        'lh' : {
            'slt' : {
	        'eh' : {
		    '15' : {  
                        'nominal' : 'tau_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient',  
		        'total_down' : 'tau_1_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient',
                        'total_up'   : 'tau_1_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient',
                          },
	            '16' : { 
                        'nominal'    :  'tau_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient',  
			'total_down' : 'tau_1_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient',
                        'total_up'   : 'tau_1_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient',
			   },
                    '17' : { 
                        'nominal'    : 'tau_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient', 
                        'total_down' : 'tau_1_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient',
                        'total_up'   : 'tau_1_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient',
                           },
                    '18' : {
                        'nominal' : 'tau_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient',  
                        'total_down' : 'tau_1_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient',
                        'total_up'   : 'tau_1_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient',
                          },
                        },
	        'mh' : {
		    '15' : {
                        'nominal'   : 'tau_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium', 
		        'stat_down' : 'tau_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium',
                        'stat_up'   : 'tau_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium',
			'syst_down' : 'tau_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium',
                        'syst_up'   : 'tau_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium',
			   },
		    '16' : {
                        'nominal'    : 'tau_1_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium', 
			'stat_down'  : 'tau_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'stat_up'    : 'tau_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
			'syst_down'  : 'tau_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'syst_up'    : 'tau_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
			   },
                    '17' : { 
                        'nominal'    : 'tau_1_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium', 
                        'stat_down'  : 'tau_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'stat_up'    : 'tau_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'syst_down'  : 'tau_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'syst_up'    : 'tau_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                           },
                    '18' : {
                        'nominal'    : 'tau_1_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'stat_down'  : 'tau_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'stat_up'    : 'tau_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'syst_down'  : 'tau_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'syst_up'    : 'tau_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                           },
                        },
		    },
               },
       	'll' : {
            'trig-OR' : {
                'nominal'         : 'triggerGlobal_SF_NOMINAL',
                'elec_total_down' : 'triggerGlobal_SF_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down',
                'elec_total_up'   : 'triggerGlobal_SF_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up',
                'muon_stat_down'  : 'triggerGlobal_SF_MUON_EFF_TrigStatUncertainty__1down',
                'muon_stat_up'    : 'triggerGlobal_SF_MUON_EFF_TrigStatUncertainty__1up',
                'muon_syst_down'  : 'triggerGlobal_SF_MUON_EFF_TrigSysttUncertainty__1down',
                'muon_syst_up'    : 'triggerGlobal_SF_MUON_EFF_TrigSystUncertainty__1up',                 
                        },
            'slt' : {
                'ee' : {
                    '15' : {
            'nominal'          : '(tau_0_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose)',
            'elec_total_down'  : '(tau_0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose)',
            'elec_total_up'    : '(tau_0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose)',
                           },
                     '16' : {
            'nominal'          : '(tau_0_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose)',
            'elec_total_down'  : '(tau_0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose)',
            'elec_total_up'    : '(tau_0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose)',
                       },
                     '17' : {
            'nominal'          : '(tau_0_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose)',
            'elec_total_down'  : '(tau_0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose)',
            'elec_total_up'    : '(tau_0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose)',
                       },
                     '18' : {
            'nominal'          : '(tau_0_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose)',
            'elec_total_down'  : '(tau_0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose)',
            'elec_total_up'    : '(tau_0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose)',
                       }, 
                      },      
             'mumu' : {
                    '15' : {
                        'nominal'   : 'tau_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium',
                        'muon_stat_down' : 'tau_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium',
                        'muon_stat_up'   : 'tau_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium',
                        'muon_syst_down' : 'tau_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium',
                        'muon_syst_up'   : 'tau_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium',
                           },
                    '16' : {
                        'nominal'    : 'tau_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'muon_stat_down'  : 'tau_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'muon_stat_up'    : 'tau_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'muon_syst_down'  : 'tau_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'muon_syst_up'    : 'tau_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                           },
                    '17' : {
                        'nominal'    : 'tau_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'muon_stat_down'  : 'tau_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'muon_stat_up'    : 'tau_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'muon_syst_down'  : 'tau_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'muon_syst_up'    : 'tau_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                           },
                     '18' : {
                        'nominal'    : 'tau_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'muon_stat_down'  : 'tau_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'muon_stat_up'    : 'tau_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'muon_syst_down'  : 'tau_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                        'muon_syst_up'    : 'tau_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium',
                           },
                     },    
             },              
        },
} 

