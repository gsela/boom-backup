scale_factors = {
           'Elec' : {
                    'Reco' : { 
                             'nominal'   : 'NOMINAL_EleEffSF_offline_RecoTrk', 
                             'total_down': 'EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_offline_RecoTrk',
                             'total_up'  : 'EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_offline_RecoTrk',
                             },
                    'looseID'  : {
                                  'nominal'   : 'NOMINAL_EleEffSF_offline_LooseAndBLayerLLH_d0z0_v13',  
                                  'total_down': 'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_offline_LooseAndBLayerLLH_d0z0_v13',
                                  'total_up'  : 'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_offline_LooseAndBLayerLLH_d0z0_v13',
                                 },
                    'mediumID' : {
                                  'nominal'   : 'NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v13', 	
                                  'total_down': 'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_offline_MediumLLH_d0z0_v13',
                                  'total_up'  : 'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_offline_MediumLLH_d0z0_v13',
                                 },
                    'Iso' : {
                             'nominal'   : 'NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v13_Gradient',
                             'total_down': 'EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_Isolation_MediumLLH_d0z0_v13_Gradient',
                             'total_up'  : 'EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_Isolation_MediumLLH_d0z0_v13_Gradient',
                             },

                    'ChargeID' : {
                             'nominal'   : 'NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_Gradient',
                             'stat_down' : 'EL_CHARGEID_STAT_1down_EleEffSF_ChargeID_MediumLLH_d0z0_v13_Gradient',
                             'stat_up'   : 'EL_CHARGEID_STAT_1up_EleEffSF_ChargeID_MediumLLH_d0z0_v13_Gradient', 
                             'syst_down' : 'EL_CHARGEID_SYStotal_1down_EleEffSF_ChargeID_MediumLLH_d0z0_v13_Gradient',
                             'syst_up'   : 'EL_CHARGEID_SYStotal_1down_EleEffSF_ChargeID_MediumLLH_d0z0_v13_Gradient',
                             },

                    'Lead_nonPrompt' : {
                             'nominal'   : '(0.051779/(exp(-0.141434*(tau_0_p4.Pt()-13.4))+1)+0.923748+0.000110131*tau_0_p4.Pt())',
                             'up'        : '(pow((0.051779/(exp(-0.141434*(tau_0_p4.Pt()-13.4))+1)+0.923748+0.000110131*tau_0_p4.Pt()),2))',
                                       }, 
                    'Sublead_nonPrompt' : {
                             'nominal'   : '(0.051779/(exp(-0.141434*(tau_1_p4.Pt()-13.4))+1)+0.923748+0.000110131*tau_1_p4.Pt())',
                             'up'        : '(pow((0.051779/(exp(-0.141434*(tau_1_p4.Pt()-13.4))+1)+0.923748+0.000110131*tau_1_p4.Pt()),2))',
                                       }, 
                                 
                    },
           'Muon' : {
                    'ID'   : {
                             'nominal'         : 'NOMINAL_MuEffSF_Reco_QualMedium', 
                             'stat_down'       : 'MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium', 
                             'stat_up'         : 'MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium',
                             'stat_lowpt_down' : 'MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium',
                             'stat_lowpt_up'   : 'MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium',
                             'syst_down'       : 'MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium',
                             'syst_up'         : 'MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium',
                             'syst_lowpt_down' : 'MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium', 
                             'syst_lowpt_up'   : 'MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium', 
                             } ,
                    'Iso' : {
                             'nominal'   : 'NOMINAL_MuEffSF_IsoFCTight_FixedRad', 
                             'stat_down' : 'MUON_EFF_ISO_STAT_1down_MuEffSF_IsoFCTight_FixedRad', 
                             'stat_up'   : 'MUON_EFF_ISO_STAT_1up_MuEffSF_IsoFCTight_FixedRad',
                             'syst_down' : 'MUON_EFF_ISO_SYS_1down_MuEffSF_IsoFCTight_FixedRad',
                             'syst_up'   : 'MUON_EFF_ISO_SYS_1up_MuEffSF_IsoFCTight_FixedRad',
                             },
                    },
           'Tau'  : { 
                    'Reco'   : {
                             'nominal'    : 'NOMINAL_TauEffSF_reco', 
                             'total_down' : 'TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco', 
                             'total_up'   : 'TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco', 
                               },

                    'mediumID' : {
                             'nominal'        : 'NOMINAL_TauEffSF_JetRNNtight',
                             '1p_pt2025_down' : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight',
                             '1p_pt2025_up'   : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight',
                             '1p_pt2530_down' : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight',
                             '1p_pt2530_up'   : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight',
                             '1p_pt3040_down' : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight',
                             '1p_pt3040_up'   : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight',
                             '1p_pt40_down'   : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight',
                             '1p_pt40_up'     : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight', 
                             '3p_pt2025_down' : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight',
                             '3p_pt2025_up'   : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight',
                             '3p_pt2530_down' : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight',
                             '3p_pt2530_up'   : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight',
                             '3p_pt3040_down' : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight',
                             '3p_pt3040_up'   : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight',
                             '3p_pt40_down'   : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight',
                             '3p_pt40_up'     : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight',
                             'syst_down'      : 'TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNtight',
                             'syst_up'        : 'TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNtight',
                             'highpt_down'    : 'TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNtight',
                             'highpt_up'      : 'TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNtight',
                                },

                    'BDTEleOLR' : { 
                                   'nominal'             : 'NOMINAL_TauEffSF_MediumEleBDT_electron',
                                   'trueelec_stat_down'  : 'TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_MediumEleBDT_electron',      
                                   'trueelec_stat_up'    : 'TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_MediumEleBDT_electron',
                                   'trueelec_syst_down'  : 'TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDT_electron',       
                                   'trueelec_syst_up'    : 'TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDT_electron',
                                  }, 
                    'LooseBDTEleOLR' : { 
                                   'nominal'             : 'NOMINAL_TauEffSF_LooseEleBDT_electron',
                                   'trueelec_stat_down'  : 'TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_LooseEleBDT_electron',      
                                   'trueelec_stat_up'    : 'TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_LooseEleBDT_electron',
                                   'trueelec_syst_down'  : 'TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDT_electron',       
                                   'trueelec_syst_up'    : 'TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDT_electron',
                                  },
                    'TauOLR'    : {
                                   'nominal'         : 'NOMINAL_TauEffSF_HadTauEleOLR_tauhad', 
                                   'truehadtau_down' : 'TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad',
                                   'truehadtau_up'   : 'TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad'
                                  },
                    },
           'Jets' : {
                    'BTag_DL1r_85' : { 
                               'nominal'    : 'jet_NOMINAL_global_effSF_DL1r_FixedCutBEff_85 * jet_NOMINAL_global_ineffSF_DL1r_FixedCutBEff_85',
                               'BNP0_down'  : 'jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1r_FixedCutBEff_85',
                               'BNP0_up'    : 'jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1r_FixedCutBEff_85',
                               'BNP1_down'  : 'jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1r_FixedCutBEff_85',
                               'BNP1_up'    : 'jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1r_FixedCutBEff_85',
                               'BNP2_down'  : 'jet_FT_EFF_Eigen_B_2_1down_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_B_2_1down_global_ineffSF_DL1r_FixedCutBEff_85',
                               'BNP2_up'    : 'jet_FT_EFF_Eigen_B_2_1up_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_B_2_1up_global_ineffSF_DL1r_FixedCutBEff_85',
                               'CNP0_down'  : 'jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1r_FixedCutBEff_85',
                               'CNP0_up'    : 'jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1r_FixedCutBEff_85',
                               'CNP1_down'  : 'jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1r_FixedCutBEff_85',
                               'CNP1_up'    : 'jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1r_FixedCutBEff_85',
                               'CNP2_down'  : 'jet_FT_EFF_Eigen_C_2_1down_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_C_2_1down_global_ineffSF_DL1r_FixedCutBEff_85',
                               'CNP2_up'    : 'jet_FT_EFF_Eigen_C_2_1up_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_C_2_1up_global_ineffSF_DL1r_FixedCutBEff_85',
                               'LNP0_down'  : 'jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1r_FixedCutBEff_85',
                               'LNP0_up'    : 'jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1r_FixedCutBEff_85',
                               'LNP1_down'  : 'jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1r_FixedCutBEff_85',
                               'LNP1_up'    : 'jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1r_FixedCutBEff_85',
                               'LNP2_down'  : 'jet_FT_EFF_Eigen_Light_2_1down_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_Light_2_1down_global_ineffSF_DL1r_FixedCutBEff_85',
                               'LNP2_up'    : 'jet_FT_EFF_Eigen_Light_2_1up_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_Light_2_1up_global_ineffSF_DL1r_FixedCutBEff_85',
                               'LNP3_down'  : 'jet_FT_EFF_Eigen_Light_3_1down_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_Light_3_1down_global_ineffSF_DL1r_FixedCutBEff_85',
                               'LNP3_up'    : 'jet_FT_EFF_Eigen_Light_3_1up_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_Eigen_Light_3_1up_global_ineffSF_DL1r_FixedCutBEff_85',
                               'extr_down'  : 'jet_FT_EFF_extrapolation_1down_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_extrapolation_1down_global_ineffSF_DL1r_FixedCutBEff_85',
                               'extr_up'    : 'jet_FT_EFF_extrapolation_1up_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_extrapolation_1up_global_ineffSF_DL1r_FixedCutBEff_85',
                               'extrc_down' : 'jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1r_FixedCutBEff_85',
                               'extrc_up'   : 'jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1r_FixedCutBEff_85 * jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1r_FixedCutBEff_85',
                              },

 
                    'BTag_DL1r_70' : {             
                               'nominal'    : 'jet_NOMINAL_global_effSF_DL1r_FixedCutBEff_70 * jet_NOMINAL_global_ineffSF_DL1r_FixedCutBEff_70',
                               'BNP0_down'  : 'jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1r_FixedCutBEff_70',
                               'BNP0_up'    : 'jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1r_FixedCutBEff_70',
                               'BNP1_down'  : 'jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1r_FixedCutBEff_70',
                               'BNP1_up'    : 'jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1r_FixedCutBEff_70',
                               'BNP2_down'  : 'jet_FT_EFF_Eigen_B_2_1down_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_B_2_1down_global_ineffSF_DL1r_FixedCutBEff_70',
                               'BNP2_up'    : 'jet_FT_EFF_Eigen_B_2_1up_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_B_2_1up_global_ineffSF_DL1r_FixedCutBEff_70',
                               'CNP0_down'  : 'jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1r_FixedCutBEff_70',
                               'CNP0_up'    : 'jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1r_FixedCutBEff_70',
                               'CNP1_down'  : 'jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1r_FixedCutBEff_70',
                               'CNP1_up'    : 'jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1r_FixedCutBEff_70',
                               'CNP2_down'  : 'jet_FT_EFF_Eigen_C_2_1down_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_C_2_1down_global_ineffSF_DL1r_FixedCutBEff_70',
                               'CNP2_up'    : 'jet_FT_EFF_Eigen_C_2_1up_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_C_2_1up_global_ineffSF_DL1r_FixedCutBEff_70',
                               'LNP0_down'  : 'jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1r_FixedCutBEff_70',
                               'LNP0_up'    : 'jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1r_FixedCutBEff_70',
                               'LNP1_down'  : 'jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1r_FixedCutBEff_70',
                               'LNP1_up'    : 'jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1r_FixedCutBEff_70',
                               'LNP2_down'  : 'jet_FT_EFF_Eigen_Light_2_1down_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_Light_2_1down_global_ineffSF_DL1r_FixedCutBEff_70',
                               'LNP2_up'    : 'jet_FT_EFF_Eigen_Light_2_1up_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_Light_2_1up_global_ineffSF_DL1r_FixedCutBEff_70',
                               'LNP3_down'  : 'jet_FT_EFF_Eigen_Light_3_1down_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_Light_3_1down_global_ineffSF_DL1r_FixedCutBEff_70',
                               'LNP3_up'    : 'jet_FT_EFF_Eigen_Light_3_1up_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_Light_3_1up_global_ineffSF_DL1r_FixedCutBEff_70',
                               'LNP4_down'  : 'jet_FT_EFF_Eigen_Light_4_1down_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_Light_4_1down_global_ineffSF_DL1r_FixedCutBEff_70',
                               'LNP4_up'    : 'jet_FT_EFF_Eigen_Light_4_1up_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_Eigen_Light_4_1up_global_ineffSF_DL1r_FixedCutBEff_70',
                               'extr_down'  : 'jet_FT_EFF_extrapolation_1down_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_extrapolation_1down_global_ineffSF_DL1r_FixedCutBEff_70',
                               'extr_up'    : 'jet_FT_EFF_extrapolation_1up_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_extrapolation_1up_global_ineffSF_DL1r_FixedCutBEff_70',
                               'extrc_down' : 'jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1r_FixedCutBEff_70',
                               'extrc_up'   : 'jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1r_FixedCutBEff_70 * jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1r_FixedCutBEff_70',
                              },


                    'JVT'   : {
                               'nominal'    : 'jet_NOMINAL_central_jets_global_effSF_JVT * jet_NOMINAL_central_jets_global_ineffSF_JVT', 
                               'syst_down'  : 'jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT * jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT',
                               'syst_up'    : 'jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT * jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT'
                              }, 
                    'fJVT'  : {
                               'nominal'    : 'MC_Norm::check_0_weight(jet_NOMINAL_forward_jets_global_effSF_JVT) * MC_Norm::check_0_weight(jet_NOMINAL_forward_jets_global_ineffSF_JVT)', 
                               'syst_down'  : 'MC_Norm::NaNProtectBetterThanRoot(jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT) * MC_Norm::NaNProtectBetterThanRoot(jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT)',
                               'syst_up'    : 'MC_Norm::NaNProtectBetterThanRoot(jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT) * MC_Norm::NaNProtectBetterThanRoot(jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT)'
                              }, 

	            },
}


