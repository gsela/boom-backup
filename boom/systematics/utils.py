from .summary import SUMMARY, embedding_systematics

#--------
def _is_valid_systematic(
    name, channel, year, process_group, 
    is_signal=False, z_cr=False, z_cr_flavour=None):
    """Helper function to decide if a systematic is valid

    Parameters
    ----------
    name: str
    channel: str
    year: str
    process_group: str
    is_signal: bool
    z_cr: bool
    """

    if name in SUMMARY.keys():
        _info = SUMMARY[name]
    else:
        if z_cr:
            _embedding_systs = embedding_systematics()
            if name in _embedding_systs.keys():
                _info = _embedding_systs[name]
            else:
                raise ValueError(name)
        else:
            raise ValueError(name)
    
    # channel
    if not isinstance(channel, (list, tuple)):
        _channels = [channel]
    else:
        _channels = channel

    _skip_channel = False
    if z_cr:
        #  z_cr and ll have same systs
        if not 'll' in _info['channels']:
            _skip_channel = True
    else:
        _considered_channels = filter(lambda c: c in _info['channels'], _channels)
        if len(_considered_channels) == 0:
            _skip_channel = True
    if _skip_channel == True:
        return False

    # ee / mumu for zcr
    if z_cr:
        _skip_flavour = False
        if 'z_cr_flavour' in _info.keys():
            if z_cr_flavour != None:
                if not isinstance(z_cr_flavour, (list, tuple)):
                    _z_cr_flavour = [z_cr_flavour]
                else:
                    _z_cr_flavour = z_cr_flavour
                _considered_flavours = filter(lambda f: f in _info['z_cr_flavour'], _z_cr_flavour)
                if len(_considered_flavours) == 0:
                    _skip_flavour = True
        if _skip_flavour:
            return False


    # years
    if not isinstance(year, (list, tuple)):
        _years = [year]
    else:
        _years = year

    _skip_year = False
    _considered_years = filter(lambda y: y in _info['years'], _years)
    if len(_considered_years) == 0:
        _skip_year = True

    if _skip_year == True:
        return False

    # process
    _skip_process = False
    if process_group in ('Data', 'Fake',):
        if not process_group in _info['processes']:
            _skip_process = True
    else:
        if not 'all_mc' in _info['processes']:
            if not process_group in _info['processes']:
                _skip_process = True
    if is_signal and 'Fake' in _info['processes']:
        _skip_process = True

    if _skip_process == True:
        return False

    # is_signal
    _skip_signal = False
    if 'signal_only' in _info.keys():
        if not is_signal:
            if _info['signal_only']:
                _skip_signal = True
    if _skip_signal:
        return False

    return True

# ---------
def _is_valid_list(full_list, *args, **kwargs):
    """filter a list of systematics
    
    Parameters
    ----------
    full_list: list(str)
    """
    _valid_list = []
    for _syst in full_list:
        if _is_valid_systematic(_syst, *args, **kwargs):
            _valid_list.append(_syst)
    return _valid_list

