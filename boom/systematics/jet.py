from .base import DecoratedSystematics, SYSTBOOK
from ..cuts import REGIONS
from .object_weight_dictionary import scale_factors
from happy.systematics import SystematicsSet, Systematics

########################
# JET weight systematics
########################

#########################
# B tag syst for DL1r 85
#########################

sys_jet_btag_b_np0_DL1r_85      = Systematics.weightSystematics( 'FT_EFF_Eigen_B_0',  'Jets B-tag B 0',
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['BNP0_up']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['BNP0_down']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['nominal']))

sys_jet_btag_b_np1_DL1r_85      = Systematics.weightSystematics( 'FT_EFF_Eigen_B_1',  'Jets B-tag B 1',
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['BNP1_up']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['BNP1_down']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['nominal']))

sys_jet_btag_b_np2_DL1r_85      = Systematics.weightSystematics( 'FT_EFF_Eigen_B_2',  'Jets B-tag B 2',
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['BNP2_up']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['BNP2_down']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['nominal']))

sys_jet_btag_c_np0_DL1r_85      = Systematics.weightSystematics( 'FT_EFF_Eigen_C_0',  'Jets B-tag C 0',
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['CNP0_up']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['CNP0_down']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['nominal']))

sys_jet_btag_c_np1_DL1r_85      = Systematics.weightSystematics( 'FT_EFF_Eigen_C_1',  'Jets B-tag C 1',
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['CNP1_up']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['CNP1_down']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['nominal']))

sys_jet_btag_c_np2_DL1r_85      = Systematics.weightSystematics( 'FT_EFF_Eigen_C_2',  'Jets B-tag C 2',
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['CNP2_up']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['CNP2_down']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['nominal']))

sys_jet_btag_light_np0_DL1r_85  = Systematics.weightSystematics( 'FT_EFF_Eigen_Light_0',  'Jets B-tag Light 0',
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['LNP0_up']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['LNP0_down']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['nominal']))

sys_jet_btag_light_np1_DL1r_85  = Systematics.weightSystematics( 'FT_EFF_Eigen_Light_1',  'Jets B-tag Light 1',
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['LNP1_up']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['LNP1_down']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['nominal']))

sys_jet_btag_light_np2_DL1r_85  = Systematics.weightSystematics( 'FT_EFF_Eigen_Light_2',  'Jets B-tag Light 2',
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['LNP2_up']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['LNP2_down']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['nominal']))

sys_jet_btag_light_np3_DL1r_85  = Systematics.weightSystematics( 'FT_EFF_Eigen_Light_3',  'Jets B-tag Light 3',
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['LNP3_up']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['LNP3_down']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['nominal']))

sys_jet_btag_extr_DL1r_85       = Systematics.weightSystematics( 'FT_EFF_extrapolation',  'Jets B-tag Extrapolation',
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['extr_up']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['extr_down']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['nominal']))

sys_jet_btag_extr_c_DL1r_85     = Systematics.weightSystematics( 'FT_EFF_extrapolation_from_charm',  'Jets B-tag Extr from Charm',
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['extrc_up']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['extrc_down']),
                        '({})'.format(scale_factors['Jets']['BTag_DL1r_85']['nominal']))

#########################
# B tag syst for DL1r 70
#########################

sys_jet_btag_b_np0_DL1r_70      = Systematics.weightSystematics( 'FT_EFF_Eigen_B_0',  'Jets B-tag B 0',
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['BNP0_up']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['BNP0_down']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['nominal']))

sys_jet_btag_b_np1_DL1r_70      = Systematics.weightSystematics( 'FT_EFF_Eigen_B_1',  'Jets B-tag B 1',
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['BNP1_up']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['BNP1_down']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['nominal']))

sys_jet_btag_b_np2_DL1r_70      = Systematics.weightSystematics( 'FT_EFF_Eigen_B_2',  'Jets B-tag B 2',
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['BNP2_up']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['BNP2_down']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['nominal']))

sys_jet_btag_c_np0_DL1r_70      = Systematics.weightSystematics( 'FT_EFF_Eigen_C_0',  'Jets B-tag C 0',
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['CNP0_up']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['CNP0_down']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['nominal']))

sys_jet_btag_c_np1_DL1r_70      = Systematics.weightSystematics( 'FT_EFF_Eigen_C_1',  'Jets B-tag C 1',
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['CNP1_up']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['CNP1_down']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['nominal']))

sys_jet_btag_c_np2_DL1r_70      = Systematics.weightSystematics( 'FT_EFF_Eigen_C_2',  'Jets B-tag C 2',
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['CNP2_up']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['CNP2_down']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['nominal']))

sys_jet_btag_light_np0_DL1r_70  = Systematics.weightSystematics( 'FT_EFF_Eigen_Light_0',  'Jets B-tag Light 0',
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['LNP0_up']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['LNP0_down']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['nominal']))

sys_jet_btag_light_np1_DL1r_70  = Systematics.weightSystematics( 'FT_EFF_Eigen_Light_1',  'Jets B-tag Light 1',
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['LNP1_up']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['LNP1_down']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['nominal']))

sys_jet_btag_light_np2_DL1r_70  = Systematics.weightSystematics( 'FT_EFF_Eigen_Light_2',  'Jets B-tag Light 2',
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['LNP2_up']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['LNP2_down']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['nominal']))

sys_jet_btag_light_np3_DL1r_70  = Systematics.weightSystematics( 'FT_EFF_Eigen_Light_3',  'Jets B-tag Light 3',
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['LNP3_up']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['LNP3_down']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['nominal']))

sys_jet_btag_light_np4_DL1r_70  = Systematics.weightSystematics( 'FT_EFF_Eigen_Light_4',  'Jets B-tag Light 4',
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['LNP4_up']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['LNP4_down']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['nominal']))

sys_jet_btag_extr_DL1r_70       = Systematics.weightSystematics( 'FT_EFF_extrapolation',  'Jets B-tag Extrapolation',
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['extr_up']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['extr_down']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['nominal']))

sys_jet_btag_extr_c_DL1r_70     = Systematics.weightSystematics( 'FT_EFF_extrapolation_from_charm',  'Jets B-tag Extr from Charm',
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['extrc_up']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['extrc_down']),
                                                                '({})'.format(scale_factors['Jets']['BTag_DL1r_70']['nominal']))




sys_jet_jvt             = Systematics.weightSystematics( 'JET_JvtEfficiency',  'Jet JVT Eff',
                        '({})'.format(scale_factors['Jets']['JVT']['syst_up']),
                        '({})'.format(scale_factors['Jets']['JVT']['syst_down']),
                        '({})'.format(scale_factors['Jets']['JVT']['nominal']))

sys_jet_fjvt            = Systematics.weightSystematics( 'JET_fJvtEfficiency',  'Jet fJVT Eff',
                        '({})'.format(scale_factors['Jets']['fJVT']['syst_up']),
                        '({})'.format(scale_factors['Jets']['fJVT']['syst_down']),
                        '({})'.format(scale_factors['Jets']['fJVT']['nominal']))

weight_btag_DL1r_85 = DecoratedSystematics(
           'jet_weight_btag_systematics_DL1r_85',
           'Jet weight Btagging systematics collection DL1r_85', 
           systSet = SystematicsSet(set([ sys_jet_btag_b_np0_DL1r_85,
                                          sys_jet_btag_b_np1_DL1r_85,
                                          sys_jet_btag_b_np2_DL1r_85,
                                          sys_jet_btag_c_np0_DL1r_85,
                                          sys_jet_btag_c_np1_DL1r_85,
                                          sys_jet_btag_c_np2_DL1r_85,
                                          sys_jet_btag_light_np0_DL1r_85,
                                          sys_jet_btag_light_np1_DL1r_85,
                                          sys_jet_btag_light_np2_DL1r_85,
                                          sys_jet_btag_light_np3_DL1r_85,
                                          sys_jet_btag_extr_DL1r_85,
                                          sys_jet_btag_extr_c_DL1r_85,
                                         ])),
           channels=['ee','mumu','emu','mue','e1p','e3p','mu1p','mu3p','ehad','muhad'],
           regions=filter(lambda t: t not in ('Ztt_cr_corr_hh','Ztt_cr_hh'), REGIONS))
SYSTBOOK.append(weight_btag_DL1r_85)

weight_btag_DL1r_70_hh = DecoratedSystematics(
           'jet_weight_btag_systematics_DL1r_70',
           'Jet weight Btagging systematics collection DL1r_70',
           systSet = SystematicsSet(set([ sys_jet_btag_b_np0_DL1r_70,
                                          sys_jet_btag_b_np1_DL1r_70,
                                          sys_jet_btag_b_np2_DL1r_70,
                                          sys_jet_btag_c_np0_DL1r_70,
                                          sys_jet_btag_c_np1_DL1r_70,
                                          sys_jet_btag_c_np2_DL1r_70,
                                          sys_jet_btag_light_np0_DL1r_70,
                                          sys_jet_btag_light_np1_DL1r_70,
                                          sys_jet_btag_light_np2_DL1r_70,
                                          sys_jet_btag_light_np3_DL1r_70,
                                          sys_jet_btag_light_np4_DL1r_70,
                                          sys_jet_btag_extr_DL1r_70,
                                          sys_jet_btag_extr_c_DL1r_70,
                                         ])),
           channels=['1p1p','1p3p','3p1p','3p3p'])
SYSTBOOK.append(weight_btag_DL1r_70_hh)

weight_btag_DL1r_70_hh_zcr = DecoratedSystematics(
           'jet_weight_btag_systematics_DL1r_70',
           'Jet weight Btagging systematics collection DL1r_70',
           systSet = SystematicsSet(set([ sys_jet_btag_b_np0_DL1r_70,
                                          sys_jet_btag_b_np1_DL1r_70,
                                          sys_jet_btag_b_np2_DL1r_70,
                                          sys_jet_btag_c_np0_DL1r_70,
                                          sys_jet_btag_c_np1_DL1r_70,
                                          sys_jet_btag_c_np2_DL1r_70,
                                          sys_jet_btag_light_np0_DL1r_70,
                                          sys_jet_btag_light_np1_DL1r_70,
                                          sys_jet_btag_light_np2_DL1r_70,
                                          sys_jet_btag_light_np3_DL1r_70,
                                          sys_jet_btag_light_np4_DL1r_70,
                                          sys_jet_btag_extr_DL1r_70,
                                          sys_jet_btag_extr_c_DL1r_70,
                                         ])),
           channels=['ee','mumu'],
           regions=filter(lambda t: t in ('Ztt_cr_corr_hh','Ztt_cr_hh'), REGIONS))
SYSTBOOK.append(weight_btag_DL1r_70_hh_zcr)


weight_jvt = DecoratedSystematics(
           'jet_weight_jvt_systematics',
           'Jet weight (f)JVT systematics collection',
           systSet = SystematicsSet(set([ sys_jet_jvt,
                                          sys_jet_fjvt,
                                        ])))
SYSTBOOK.append(weight_jvt)





