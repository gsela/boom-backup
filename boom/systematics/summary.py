"""
"""
SUMMARY = {
    'MET_SoftTrk_ResoPara': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_l',
    },

    'MET_SoftTrk_ResoPerp': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_l',
    },

    'MET_SoftTrk_Scale': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_l',
    },

    'TAUS_TRUEHADTAU_SME_TES_INSITUEXP': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'sys_l',
    },

    'TAUS_TRUEHADTAU_SME_TES_INSITUFIT': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'sys_l',
    },

    'TAUS_TRUEHADTAU_SME_TES_MODEL_CLOSURE': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'sys_l',
    },

    'TAUS_TRUEHADTAU_SME_TES_PHYSICSLIST': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'sys_l',
    },

    'EG_RESOLUTION_ALL': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['ee'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'sys_l',
    },

    'EG_SCALE_ALL': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['ee'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'sys_l',
    },

    'MUON_ID': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['mumu'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'sys_l',
    },

    'MUON_MS': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['mumu'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'sys_l',
    },

    'MUON_SAGITTA_RESBIAS': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['mumu'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'sys_l',
    },

    'MUON_SAGITTA_RHO': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['mumu'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'sys_l',
    },

    'MUON_SCALE': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['mumu'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'sys_l',
    },

    'JET_BJES_Response': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j1',
    },

    'JET_EffectiveNP_Statistical1': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j1',
    },

    'JET_EffectiveNP_Statistical2': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j1',
    },

    'JET_EffectiveNP_Statistical3': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j1',
    },

    'JET_EffectiveNP_Statistical4': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j1',
    },

    'JET_EffectiveNP_Statistical5': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j1',
    },

    'JET_EffectiveNP_Statistical6': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j1',
    },

    'JET_EtaIntercalibration_Modelling': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j1',
    },
  
    'JET_EtaIntercalibration_NonClosure_2018data': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j1',
    },

    'JET_EtaIntercalibration_NonClosure_highE': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j1',
    },

    'JET_EtaIntercalibration_NonClosure_negEta': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j1',
    },

    'JET_EtaIntercalibration_NonClosure_posEta': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j1',
    },

    'JET_EtaIntercalibration_TotalStat': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j1',
    },

    'JET_PunchThrough_MC16': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j1',
    },

    'JET_SingleParticle_HighPt': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j1',
    },

    'JET_Flavor_Composition': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j6',
    },

    'JET_Flavor_Response': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j6',
    },

    'SMC_JET_JER_EffectiveNP_1': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j4',
    },
   
    'SMC_JET_JER_EffectiveNP_2': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j4',
    },

    'SMC_JET_JER_EffectiveNP_3': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j4',
    },

    'SMC_JET_JER_EffectiveNP_4': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j4',
    }, 

    'SMC_JET_JER_EffectiveNP_5': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j4',
    },

    'SMC_JET_JER_EffectiveNP_6': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j4',
    },

    'SMC_JET_JER_EffectiveNP_7': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j4',
    },

    'SMC_JET_JER_EffectiveNP_8': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j4',
    },

    'SMC_JET_JER_EffectiveNP_9': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j4',
    },

    'SMC_JET_JER_EffectiveNP_10': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j4',
    },

    'SMC_JET_JER_EffectiveNP_11': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j4',
    },

    'SMC_JET_JER_EffectiveNP_12restTerm': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j4',
    },

    'SMC_JET_JER_DataVsMC_MC16': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j4',
    },

    'SPD_JET_JER_EffectiveNP_1': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j5',
    },

    'SPD_JET_JER_EffectiveNP_2': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j5',
    },

    'SPD_JET_JER_EffectiveNP_3': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j5',
    },

    'SPD_JET_JER_EffectiveNP_4': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j5',
    },

    'SPD_JET_JER_EffectiveNP_5': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j5',
    },

    'SPD_JET_JER_EffectiveNP_6': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j5',
    },

    'SPD_JET_JER_EffectiveNP_7': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j5',
    },

    'SPD_JET_JER_EffectiveNP_8': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j5',
    },

    'SPD_JET_JER_EffectiveNP_9': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j5',
    },

    'SPD_JET_JER_EffectiveNP_10': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j5',
    },

    'SPD_JET_JER_EffectiveNP_11': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j5',
    },

    'SPD_JET_JER_EffectiveNP_12restTerm': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j5',
    },

    'SPD_JET_JER_DataVsMC_MC16': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j5',
    },

    'JET_EffectiveNP_Mixed1': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j2',
    },

    'JET_EffectiveNP_Mixed2': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j2',
    },

    'JET_EffectiveNP_Mixed3': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j2',
    },

    'JET_Pileup_OffsetMu': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j2',
    },

    'JET_Pileup_OffsetNPV': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j2',
    },

    'JET_Pileup_PtTerm': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j2',
    },

    'JET_Pileup_RhoTopology': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j2',
    },

    'JET_EffectiveNP_Detector1': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j2',
    },

    'JET_EffectiveNP_Detector2': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j2',
    },

    'JET_EffectiveNP_Modelling1': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j2',
    },

    'JET_EffectiveNP_Modelling2': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j2',
    },

    'JET_EffectiveNP_Modelling3': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j2',
    },

    'JET_EffectiveNP_Modelling4': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'sys_j2',
    },

    'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR': {
        'channels': ['ll', 'lh',],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['ee'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'ELEC_SF',
    },

    'EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR': {
        'channels': ['ll', 'lh',],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['ee'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'ELEC_SF',
    },

    'EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR': {
        'channels': ['ll', 'lh',],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['ee'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'ELEC_SF',
    },

    'EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR': {
        'channels': ['ll', 'lh',],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['ee'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'ELEC_SF',
    },

    'EL_CHARGEID_STAT': {
        'channels': ['ll', 'lh',],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['ee'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'ELEC_SF',
    },

    'EL_CHARGEID_SYStotal': {
        'channels': ['ll', 'lh',],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['ee'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'ELEC_SF',
    },

    'FT_EFF_Eigen_B_0': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'FT_SF',
    },

    'FT_EFF_Eigen_B_1': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'FT_SF',
    },

    'FT_EFF_Eigen_B_2': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'FT_SF',
    },

    'FT_EFF_Eigen_C_0': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'FT_SF',
    },

    'FT_EFF_Eigen_C_1': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'FT_SF',
    },

    'FT_EFF_Eigen_C_2': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'FT_SF',
    },

    'FT_EFF_Eigen_Light_0': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'FT_SF',
    },

    'FT_EFF_Eigen_Light_1': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'FT_SF',
    },

    'FT_EFF_Eigen_Light_2': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'FT_SF',
    },

    'FT_EFF_Eigen_Light_3': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'FT_SF',
    },

    'FT_EFF_Eigen_Light_4': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'FT_SF',
    },

    'FT_EFF_extrapolation': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'FT_SF',
    },

    'FT_EFF_extrapolation_from_charm': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'FT_SF',
    },

    'JET_JvtEfficiency': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'JETPRW_SF',
    },

    'JET_fJvtEfficiency': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'JETPRW_SF',
    },


    'PRW_DATASF': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'JETPRW_SF',
    },

    'MUON_EFF_ISO_STAT': {
        'channels': ['ll', 'lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['mumu'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'MUON_SF',
    },

    'MUON_EFF_ISO_SYS': {
        'channels': ['ll', 'lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['mumu'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'MUON_SF',
    },

    'MUON_EFF_RECO_STAT': {
        'channels': ['ll', 'lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['mumu'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'MUON_SF',
    },

    'MUON_EFF_RECO_STAT_LOWPT': {
        'channels': ['ll', 'lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['mumu'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'MUON_SF',
    },

    'MUON_EFF_RECO_SYS': {
        'channels': ['ll', 'lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['mumu'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'MUON_SF',
    },
    
    'MUON_EFF_RECO_SYS_LOWPT': {
        'channels': ['ll', 'lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['mumu'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'MUON_SF',
    },

    'MUON_EFF_TrigStatUncertainty': {
        'channels': ['ll', 'lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['mumu'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'MUON_SF',
    },

    'MUON_EFF_TrigSystUncertainty': {
        'channels': ['ll', 'lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'z_cr_flavour': ['mumu'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'MUON_SF',
    },

    'TAUS_TRUEELECTRON_EFF_ELEBDT_STAT': {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'TAU_SF',
    },

    'TAUS_TRUEELECTRON_EFF_ELEBDT_SYST': {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'TAU_SF',
    },

    'TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL': {
        'channels': ['lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'group': 'TAU_SF',
    },

    'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025': {
        'channels': ['lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_SF',
    },

    'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530': {
        'channels': ['lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_SF',
    },

    'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040': {
        'channels': ['lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_SF',
    },

    'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40': {
        'channels': ['lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_SF',
    },
 
    'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025': {
        'channels': ['lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_SF',
    },

    'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530': {
        'channels': ['lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_SF',
    },

    'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040': {
        'channels': ['lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_SF',
    },

    'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40': {
        'channels': ['lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_SF',
    },

    'TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT': {
        'channels': ['lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_SF',
    },

    'TAUS_TRUEHADTAU_EFF_RNNID_SYST': {
        'channels': ['lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_SF',
    },

    'TAUS_TRUEHADTAU_EFF_RECO_TOTAL': {
        'channels': ['lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_SF',
    },

    'TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_TRIGGER_SF',
    },

    'TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_TRIGGER_SF',
    },

    'TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_TRIGGER_SF',
    },

    'TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_TRIGGER_SF',
    },
    
    'TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_TRIGGER_SF',
    },

    'TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_TRIGGER_SF',
    },

    'TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_TRIGGER_SF',
    },
 
    'TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'embedding': True,
        'group': 'TAU_TRIGGER_SF',
    },

    #theory systematics for Z+jets
    'theory_z_qsf' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_ZJETS',
    },

    'theory_z_ckk' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_ZJETS',
    },

    'theory_z_lhe3weight_mur05_muf05_pdf261000' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_ZJETS',
    },

    'theory_z_lhe3weight_mur05_muf1_pdf261000' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_ZJETS',
    },
   
    'theory_z_lhe3weight_mur1_muf05_pdf261000' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_ZJETS',
    },

    'theory_z_lhe3weight_mur1_muf2_pdf261000' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_ZJETS',
    },

    'theory_z_lhe3weight_mur2_muf1_pdf261000' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_ZJETS',
    },
    
    'theory_z_lhe3weight_mur2_muf2_pdf261000' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_ZJETS',
    },

    'theory_z_CT14_pdfset' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_ZJETS',
    },

    'theory_z_MMHT_pdfset' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_ZJETS',
    },

    'theory_z_alphaS' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_ZJETS',
    },

    'theory_z_pdf_0'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_0',
    }, 

    'theory_z_pdf_1'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_0',
    }, 
   
    'theory_z_pdf_2'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_0',
    },
   
    'theory_z_pdf_3'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_0',
    },

    'theory_z_pdf_4'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_0',
    },

    'theory_z_pdf_5'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_0',
    },

    'theory_z_pdf_6'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_0',
    },

    'theory_z_pdf_7'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_0',
    },

    'theory_z_pdf_8'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_0',
    },

    'theory_z_pdf_9'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_0',
    },

    'theory_z_pdf_10'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_1',
    },

    'theory_z_pdf_11'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_1',
    },

    'theory_z_pdf_12'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_1',
    },

    'theory_z_pdf_13'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_1',
    },

    'theory_z_pdf_14'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_1',
    },

    'theory_z_pdf_15'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_1',
    },

    'theory_z_pdf_16'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_1',
    },

    'theory_z_pdf_17'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_1',
    },

    'theory_z_pdf_18'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_1',
    },

    'theory_z_pdf_19'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_1',
    },
 
    'theory_z_pdf_20'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_2',
    },

    'theory_z_pdf_21'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_2',
    },

    'theory_z_pdf_22'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_2',
    },

    'theory_z_pdf_23'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_2',
    },

    'theory_z_pdf_24'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_2',
    },

    'theory_z_pdf_25'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_2',
    },

    'theory_z_pdf_26'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_2',
    },

    'theory_z_pdf_27'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_2',
    },

    'theory_z_pdf_28'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_2',
    },

    'theory_z_pdf_29'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_2',
    },

    'theory_z_pdf_30'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_3',
    },

    'theory_z_pdf_31'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_3',
    },

    'theory_z_pdf_32'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_3',
    },

    'theory_z_pdf_33'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_3',
    },

    'theory_z_pdf_34'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_3',
    },

    'theory_z_pdf_35'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_3',
    },

    'theory_z_pdf_36'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_3',
    },

    'theory_z_pdf_37'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_3',
    },

    'theory_z_pdf_38'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_3',
    },

    'theory_z_pdf_39'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_3',
    },

    'theory_z_pdf_40'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_4',
    },

    'theory_z_pdf_41'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_4',
    },

    'theory_z_pdf_42'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_4',
    },

    'theory_z_pdf_43'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_4',
    },

    'theory_z_pdf_44'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_4',
    },

    'theory_z_pdf_45'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_4',
    },

    'theory_z_pdf_46'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_4',
    },

    'theory_z_pdf_47'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_4',
    },

    'theory_z_pdf_48'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_4',
    },

    'theory_z_pdf_49'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_4',
    },

    'theory_z_pdf_50'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_5',
    },

    'theory_z_pdf_51'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_5',
    },

    'theory_z_pdf_52'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_5',
    },

    'theory_z_pdf_53'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_5',
    },

    'theory_z_pdf_54'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_5',
    },

    'theory_z_pdf_55'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_5',
    },

    'theory_z_pdf_56'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_5',
    },

    'theory_z_pdf_57'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_5',
    },

    'theory_z_pdf_58'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_5',
    },

    'theory_z_pdf_59'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_5',
    },

    'theory_z_pdf_60'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_6',
    },

    'theory_z_pdf_61'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_6',
    },

    'theory_z_pdf_62'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_6',
    },

    'theory_z_pdf_63'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_6',
    },

    'theory_z_pdf_64'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_6',
    },

    'theory_z_pdf_65'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_6',
    },

    'theory_z_pdf_66'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_6',
    },

    'theory_z_pdf_67'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_6',
    },

    'theory_z_pdf_68'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_6',
    },

    'theory_z_pdf_69'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_6',
    },

    'theory_z_pdf_70'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_7',
    },

    'theory_z_pdf_71'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_7',
    },

    'theory_z_pdf_72'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_7',
    },

    'theory_z_pdf_73'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_7',
    },

    'theory_z_pdf_74'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_7',
    },

    'theory_z_pdf_75'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_7',
    },

    'theory_z_pdf_76'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_7',
    },

    'theory_z_pdf_77'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_7',
    },

    'theory_z_pdf_78'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_7',
    },

    'theory_z_pdf_79'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_7',
    },

    'theory_z_pdf_80'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_8',
    },

    'theory_z_pdf_81'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_8',
    },

    'theory_z_pdf_82'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_8',
    },

    'theory_z_pdf_83'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_8',
    },

    'theory_z_pdf_84'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_8',
    },

    'theory_z_pdf_85'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_8',
    },

    'theory_z_pdf_86'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_8',
    },

    'theory_z_pdf_87'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_8',
    },

    'theory_z_pdf_88'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_8',
    },

    'theory_z_pdf_89'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_8',
    },

    'theory_z_pdf_90'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_9',
    },

    'theory_z_pdf_91'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_9',
    },

    'theory_z_pdf_92'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_9',
    },

    'theory_z_pdf_93'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_9',
    },

    'theory_z_pdf_94'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_9',
    },

    'theory_z_pdf_95'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_9',
    },

    'theory_z_pdf_96'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_9',
    },

    'theory_z_pdf_97'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_9',
    },

    'theory_z_pdf_98'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_9',
    },

    'theory_z_pdf_99'  : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD','ZllQCD'],
        'group': 'THEORY_SYST_PDF_ZJETS_9',
    },

    # theory systematics for signal
    'theory_sig_alphaS' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'signal_only': True,
        'group': 'THEORY_SYST_QCD_SGN',
    },

    'theory_sig_qcd_0' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'signal_only': True,
        'group': 'THEORY_SYST_QCD_SGN',
    },

     'theory_sig_qcd_1' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc',
        'signal_only': True,
        'group': 'THEORY_SYST_QCD_SGN',
     },

    'theory_sig_qcd_2' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_QCD_SGN',
    },

    'theory_sig_qcd_3' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_QCD_SGN',
    },

    'theory_sig_qcd_4' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_QCD_SGN',
    },

    'theory_sig_qcd_5' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_QCD_SGN',
    },

    'theory_sig_qcd_6' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_QCD_SGN',
    },

    'theory_sig_qcd_7' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_QCD_SGN',
    },

    'theory_sig_qcd_8' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH', 'ggH_stxs1', 'ggH_stxs12', 'ggH_stxs12_fine'],
        'signal_only': True,
        'group': 'THEORY_SYST_QCD_SGN',
    },

    'theory_sig_qcd_9' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['WH', 'WH_stxs1', 'WH_stxs12', 'WH_stxs12_fine', 
                      'ZH', 'ZH_stxs1', 'ZH_stxs12', 'ZH_stxs12_fine',
                      'VBFH', 'VBFH_stxs1', 'VBFH_stxs12', 'VBFH_stxs12_fine', 'VBFHWW',
                      'ggH', 'ggH_stxs1', 'ggH_stxs12', 'ggH_stxs12_fine', 'ggHWW'],
        'signal_only': True,
        'group': 'THEORY_SYST_QCD_SGN',
    },

    'theory_sig_qcd_10' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH', 'ggH_stxs1', 'ggH_stxs12', 'ggH_stxs12_fine', 'ggHWW'],
        'signal_only': True,
        'group': 'THEORY_SYST_QCD_SGN',
    },

    'theory_sig_qcd_11' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH', 'ggH_stxs1', 'ggH_stxs12', 'ggH_stxs12_fine', 'ggHWW'],
        'signal_only': True,
        'group': 'THEORY_SYST_QCD_SGN',
    },

    'theory_sig_qcd_12' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH', 'ggH_stxs1', 'ggH_stxs12', 'ggH_stxs12_fine', 'ggHWW'],
        'signal_only': True,
        'group': 'THEORY_SYST_QCD_SGN',
    },

    'theory_sig_qcd_13' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH', 'ggH_stxs1', 'ggH_stxs12', 'ggH_stxs12_fine', 'ggHWW'],
>>>>>>> b66050bc22e3f13a443fc827d053a0b009b8fcba
        'signal_only': True,
        'group': 'THEORY_SYST_QCD_SGN',
    },

    'theory_sig_pdf_0' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_0',
    },

    'theory_sig_pdf_1' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_0',
    },
   
    'theory_sig_pdf_2' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_0',
    },
 
    'theory_sig_pdf_3' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_0',
    },
 
    'theory_sig_pdf_4' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_0',
    },

    'theory_sig_pdf_5' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_0',
    },

    'theory_sig_pdf_6' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_0',
    },

    'theory_sig_pdf_7' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_0',
    },

    'theory_sig_pdf_8' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_0',
    },

    'theory_sig_pdf_9' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_0',
    },

    'theory_sig_pdf_10' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_1',
    },

    'theory_sig_pdf_11' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_1',
    },

    'theory_sig_pdf_12' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_1',
    },

    'theory_sig_pdf_13' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_1',
    },

    'theory_sig_pdf_14' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_1',
    },

    'theory_sig_pdf_15' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_1',
    },

    'theory_sig_pdf_16' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_1',
    },

    'theory_sig_pdf_17' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_1',
    },
  
    'theory_sig_pdf_18' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_1',
    },

    'theory_sig_pdf_19' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_1',
    },
    
    'theory_sig_pdf_20' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_2',
    },

    'theory_sig_pdf_21' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_2',
    },

    'theory_sig_pdf_22' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_2',
    },

    'theory_sig_pdf_23' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_2',
    },

    'theory_sig_pdf_24' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_2',
    },

    'theory_sig_pdf_25' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_2',
    },

    'theory_sig_pdf_26' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_2',
    },
    
    'theory_sig_pdf_27' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_2',
    },

    'theory_sig_pdf_28' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_2',
    },

    'theory_sig_pdf_29' : {
        'channels': ['ll','lh','hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc', 
        'signal_only': True,
        'group': 'THEORY_SYST_PDF_SGN_2',
    },

    # Fake systematics for leplep
    'll_fake_STAT_real_elec' : {
        'channels' : ['ll'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'LL_FAKE',
    },

    'll_fake_STAT_real_elec_bjet' : {
        'channels' : ['ll'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'LL_FAKE',
    },

    'll_fake_STAT_fake_elec' : {
        'channels' : ['ll'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'LL_FAKE',
    },

    'll_fake_STAT_fake_elec_bjet' : {
        'channels' : ['ll'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'LL_FAKE',
    },

    'll_fake_STAT_real_muon' : {
        'channels' : ['ll'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'LL_FAKE',
    },

    'll_fake_STAT_real_muon_bjet' : {
        'channels' : ['ll'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'LL_FAKE',
    },

    'll_fake_STAT_fake_muon' : {
        'channels' : ['ll'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'LL_FAKE',
    },

    'll_fake_STAT_fake_muon_bjet' : {
        'channels' : ['ll'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'LL_FAKE',
    },

    'll_fake_SYST_prompt' : {
        'channels' : ['ll'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'LL_FAKE',
    },

    'll_fake_SYST_CF' : {
        'channels' : ['ll'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'LL_FAKE',
    },

    'll_fake_SYST_source' : {
        'channels' : ['ll'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'LL_FAKE',
    },

    'll_fake_SYST_comp_fake_elec' : {
        'channels' : ['ll'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'LL_FAKE',
    },

    'll_fake_SYST_comp_fake_muon' : {
        'channels' : ['ll'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'LL_FAKE',
    },

    'll_fake_SYST_njets_elec' : {
        'channels' : ['ll'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'LL_FAKE',
    },

    'll_fake_SYST_njets_muon' : {
        'channels' : ['ll'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'LL_FAKE',
    },

>>>>>>> b66050bc22e3f13a443fc827d053a0b009b8fcba
    # Fake systematics for lephad
    'lh_fake_FF_QCD_1prong_stat' : { 
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake'],
        'group': 'LH_FAKE',
    },
 
    'lh_fake_FF_W_1prong_stat' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake'],
        'group': 'LH_FAKE',
    },

    'lh_fake_RQCD_1prong_stat' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake'],
        'group': 'LH_FAKE',
    },

    'lh_fake_RQCD_1prong_isofac_stat' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake'],
        'group': 'LH_FAKE',
    },

    'lh_fake_RQCD_1prong_isofac_syst' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake'],
        'group': 'LH_FAKE',
    },
   
    'lh_fake_closure_1prong' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake'],
        'group': 'LH_FAKE',
    },

    'lh_fake_mc_subtr_1prong' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake'],
        'group': 'LH_FAKE',
    },

    'lh_fake_FF_QCD_3prong_stat' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake'],
        'group': 'LH_FAKE',
    },

    'lh_fake_FF_W_3prong_stat' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake'],
        'group': 'LH_FAKE',
    },

    'lh_fake_RQCD_3prong_stat' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake'],
        'group': 'LH_FAKE',
    },

    'lh_fake_RQCD_3prong_isofac_stat' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake'],
        'group': 'LH_FAKE',
    },

    'lh_fake_RQCD_3prong_isofac_syst' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake'],
        'group': 'LH_FAKE',
    },

    'lh_fake_closure_3prong' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake'],
        'group': 'LH_FAKE',
    },

    'hh_fake_extrapolation' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['Fake'],
        'group': 'HH_FAKE',
    },
    
}

#-----
def embedding_systematics():
    _embedding_systs = {}
    for k, v in SUMMARY.items():
        if not 'embedding' in v.keys():
            continue
        if not v['embedding']:
            continue
        for _v in ('__1up', '__1down'):
            _embedding_systs['embed_' + k + _v] = {
                'channels': ['ll'],
                'years': ['15', '16', '15 16', '15_16', '17', '18'],
                'processes' : ['ZllQCD', 'ZllEWK', 'Data', 'VV', 'W', 'Top', 'ZttQCD', 'ZttEWK'],
                'z_cr_flavour': ['ee', 'mumu'],
                }
    return _embedding_systs


