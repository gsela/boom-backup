from .base import DecoratedSystematics, SYSTBOOK
from .object_weight_dictionary import scale_factors
from happy.systematics import SystematicsSet, Systematics
from ..zll_pt import _random_offset, tau_0_4vec, tau_1_4vec


sys_pu              = Systematics.weightSystematics( 'PRW_DATASF', 'Pileup',
                                        '({})'.format('PRW_DATASF_1up_pileup_combined_weight'),
                                        '({})'.format('PRW_DATASF_1down_pileup_combined_weight'),
                                        '({})'.format('NOMINAL_pileup_combined_weight'))

sys_weight_mc_bkg   = Systematics.weightSystematics('weight-bkg',   'MC Background Weight', 
                                                 'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'weight_mc'),
                                                 'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'weight_mc'),
                                                 'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'weight_mc'))

sys_weight_mc_signal   = Systematics.weightSystematics('weight-sign',  'MC Signal Weight', 
                                                 '({})'.format('theory_weights_nominal'), 
                                                 '({})'.format('theory_weights_nominal'), 
                                                 '({})'.format('theory_weights_nominal'))

weight_prw    = DecoratedSystematics(
               'prw_weight', 
               'Pileup weight', 
               systSet = SystematicsSet(set([sys_pu])),
               years = ['15','16','17','18'])
SYSTBOOK.append(weight_prw)

weight_mc_bkg = DecoratedSystematics(
               'mc_weight_bkg',
               'MC Weight for Bkg samples',
               SystematicsSet(set([sys_weight_mc_bkg])),
               bkg_only=True)
SYSTBOOK.append(weight_mc_bkg)

weight_mc_signal = DecoratedSystematics(
               'mc_weight_signal',
               'MC Weight for Signal samples',
               systSet = SystematicsSet(set([ sys_weight_mc_signal])),
               signal_only=True)
SYSTBOOK.append(weight_mc_signal)
