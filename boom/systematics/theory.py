from .base import DecoratedSystematics
from happy.systematics import SystematicsSet, Systematics

__all__ = [
    'weight_z_theory_syst',
    'weight_signal_theory_syst',
    'weight_vbf_theory_syst',
    'weight_vh_theory_syst',
    'weight_ggh_theory_syst',
]    

# Ztt Theory systematics
# UPDATED
sys_mur05_muf05   = Systematics.weightSystematics( 'theory_z_lhe3weight_mur05_muf05_pdf261000',  'theory_z_lhe3weight_mur05_muf05_pdf261000',  
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_LHE3Weight_MUR05_MUF05_PDF261000*MC_Norm::correct_weightZpdf(mc_channel_number,100,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_mur05_muf1    = Systematics.weightSystematics( 'theory_z_lhe3weight_mur05_muf1_pdf261000',   'theory_z_lhe3weight_mur05_muf1_pdf261000',   
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_LHE3Weight_MUR05_MUF1_PDF261000*MC_Norm::correct_weightZpdf(mc_channel_number,101,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_mur1_muf05    = Systematics.weightSystematics( 'theory_z_lhe3weight_mur1_muf05_pdf261000',   'theory_z_lhe3weight_mur1_muf05_pdf261000',   
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_LHE3Weight_MUR1_MUF05_PDF261000*MC_Norm::correct_weightZpdf(mc_channel_number,102,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_mur1_muf2     = Systematics.weightSystematics( 'theory_z_lhe3weight_mur1_muf2_pdf261000',    'theory_z_lhe3weight_mur1_muf2_pdf261000',    
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_LHE3Weight_MUR1_MUF2_PDF261000*MC_Norm::correct_weightZpdf(mc_channel_number,103,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_mur2_muf1     = Systematics.weightSystematics( 'theory_z_lhe3weight_mur2_muf1_pdf261000',    'theory_z_lhe3weight_mur2_muf1_pdf261000',    
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_LHE3Weight_MUR2_MUF1_PDF261000*MC_Norm::correct_weightZpdf(mc_channel_number,104,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_mur2_muf2     = Systematics.weightSystematics( 'theory_z_lhe3weight_mur2_muf2_pdf261000',    'theory_z_lhe3weight_mur2_muf2_pdf261000',    
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_LHE3Weight_MUR2_MUF2_PDF261000*MC_Norm::correct_weightZpdf(mc_channel_number,105,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

#sys_pdf_central   = Systematics.weightSystematics( 'theory_z_PDF',  'theory_z_PDF', 
#                                                   '({})'.format('(theory_weights_PDF_central_value+theory_weights_PDF_error_up)/theory_weights_PDF_central_value'),
#                                                   '({})'.format('(theory_weights_PDF_central_value-theory_weights_PDF_error_down)/theory_weights_PDF_central_value'),
#                                                   '({})'.format('1'))

sys_pdf_0         = Systematics.weightSystematics(  'theory_z_pdf_0', 'theory_z_pdf_0',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_0*MC_Norm::correct_weightZpdf(mc_channel_number,0,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_1         = Systematics.weightSystematics(  'theory_z_pdf_1', 'theory_z_pdf_1',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_1*MC_Norm::correct_weightZpdf(mc_channel_number,1,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_2         = Systematics.weightSystematics(  'theory_z_pdf_2', 'theory_z_pdf_2',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_2*MC_Norm::correct_weightZpdf(mc_channel_number,1,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))        

sys_pdf_3         = Systematics.weightSystematics(  'theory_z_pdf_3', 'theory_z_pdf_3',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_3*MC_Norm::correct_weightZpdf(mc_channel_number,1,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_4         = Systematics.weightSystematics(  'theory_z_pdf_4', 'theory_z_pdf_4',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_4*MC_Norm::correct_weightZpdf(mc_channel_number,1,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_5         = Systematics.weightSystematics(  'theory_z_pdf_5', 'theory_z_pdf_5',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_5*MC_Norm::correct_weightZpdf(mc_channel_number,1,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_6         = Systematics.weightSystematics(  'theory_z_pdf_6', 'theory_z_pdf_6',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_6*MC_Norm::correct_weightZpdf(mc_channel_number,1,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_7         = Systematics.weightSystematics(  'theory_z_pdf_7', 'theory_z_pdf_7',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_7*MC_Norm::correct_weightZpdf(mc_channel_number,1,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_8         = Systematics.weightSystematics(  'theory_z_pdf_8', 'theory_z_pdf_8',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_8*MC_Norm::correct_weightZpdf(mc_channel_number,1,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_9         = Systematics.weightSystematics(  'theory_z_pdf_9', 'theory_z_pdf_9',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_9*MC_Norm::correct_weightZpdf(mc_channel_number,9,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_10         = Systematics.weightSystematics(  'theory_z_pdf_10', 'theory_z_pdf_10',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_10*MC_Norm::correct_weightZpdf(mc_channel_number,10,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_11         = Systematics.weightSystematics(  'theory_z_pdf_11', 'theory_z_pdf_11',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_11*MC_Norm::correct_weightZpdf(mc_channel_number,11,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))
        
sys_pdf_12         = Systematics.weightSystematics(  'theory_z_pdf_12', 'theory_z_pdf_12',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_12*MC_Norm::correct_weightZpdf(mc_channel_number,12,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_13         = Systematics.weightSystematics(  'theory_z_pdf_13', 'theory_z_pdf_13',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_13*MC_Norm::correct_weightZpdf(mc_channel_number,13,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_14         = Systematics.weightSystematics(  'theory_z_pdf_14', 'theory_z_pdf_14',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_14*MC_Norm::correct_weightZpdf(mc_channel_number,14,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_15         = Systematics.weightSystematics(  'theory_z_pdf_15', 'theory_z_pdf_15',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_15*MC_Norm::correct_weightZpdf(mc_channel_number,15,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_16         = Systematics.weightSystematics(  'theory_z_pdf_16', 'theory_z_pdf_16',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_16*MC_Norm::correct_weightZpdf(mc_channel_number,16,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_17         = Systematics.weightSystematics(  'theory_z_pdf_17', 'theory_z_pdf_17',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_17*MC_Norm::correct_weightZpdf(mc_channel_number,17,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_18         = Systematics.weightSystematics(  'theory_z_pdf_18', 'theory_z_pdf_18',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_18*MC_Norm::correct_weightZpdf(mc_channel_number,18,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_19         = Systematics.weightSystematics(  'theory_z_pdf_19', 'theory_z_pdf_19',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_19*MC_Norm::correct_weightZpdf(mc_channel_number,19,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_20         = Systematics.weightSystematics(  'theory_z_pdf_20', 'theory_z_pdf_20',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_20*MC_Norm::correct_weightZpdf(mc_channel_number,20,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_21         = Systematics.weightSystematics(  'theory_z_pdf_21', 'theory_z_pdf_21',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_21*MC_Norm::correct_weightZpdf(mc_channel_number,21,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))
        
sys_pdf_22         = Systematics.weightSystematics(  'theory_z_pdf_22', 'theory_z_pdf_22',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_22*MC_Norm::correct_weightZpdf(mc_channel_number,22,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_23         = Systematics.weightSystematics(  'theory_z_pdf_23', 'theory_z_pdf_23',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_23*MC_Norm::correct_weightZpdf(mc_channel_number,23,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_24         = Systematics.weightSystematics(  'theory_z_pdf_24', 'theory_z_pdf_24',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_24*MC_Norm::correct_weightZpdf(mc_channel_number,24,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_25         = Systematics.weightSystematics(  'theory_z_pdf_25', 'theory_z_pdf_25',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_25*MC_Norm::correct_weightZpdf(mc_channel_number,25,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_26         = Systematics.weightSystematics(  'theory_z_pdf_26', 'theory_z_pdf_26',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_26*MC_Norm::correct_weightZpdf(mc_channel_number,26,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_27         = Systematics.weightSystematics(  'theory_z_pdf_27', 'theory_z_pdf_27',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_27*MC_Norm::correct_weightZpdf(mc_channel_number,27,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_28         = Systematics.weightSystematics(  'theory_z_pdf_28', 'theory_z_pdf_28',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_28*MC_Norm::correct_weightZpdf(mc_channel_number,28,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_29         = Systematics.weightSystematics(  'theory_z_pdf_29', 'theory_z_pdf_29',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_29*MC_Norm::correct_weightZpdf(mc_channel_number,29,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_30         = Systematics.weightSystematics(  'theory_z_pdf_30', 'theory_z_pdf_30',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_30*MC_Norm::correct_weightZpdf(mc_channel_number,30,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_31         = Systematics.weightSystematics(  'theory_z_pdf_31', 'theory_z_pdf_31',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_31*MC_Norm::correct_weightZpdf(mc_channel_number,31,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))
        
sys_pdf_32         = Systematics.weightSystematics(  'theory_z_pdf_32', 'theory_z_pdf_32',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_32*MC_Norm::correct_weightZpdf(mc_channel_number,32,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_33         = Systematics.weightSystematics(  'theory_z_pdf_33', 'theory_z_pdf_33',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_33*MC_Norm::correct_weightZpdf(mc_channel_number,33,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_34         = Systematics.weightSystematics(  'theory_z_pdf_34', 'theory_z_pdf_34',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_34*MC_Norm::correct_weightZpdf(mc_channel_number,34,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_35         = Systematics.weightSystematics(  'theory_z_pdf_35', 'theory_z_pdf_35',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_35*MC_Norm::correct_weightZpdf(mc_channel_number,35,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_36         = Systematics.weightSystematics(  'theory_z_pdf_36', 'theory_z_pdf_36',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_36*MC_Norm::correct_weightZpdf(mc_channel_number,36,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_37         = Systematics.weightSystematics(  'theory_z_pdf_37', 'theory_z_pdf_37',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_37*MC_Norm::correct_weightZpdf(mc_channel_number,37,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_38         = Systematics.weightSystematics(  'theory_z_pdf_38', 'theory_z_pdf_38',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_38*MC_Norm::correct_weightZpdf(mc_channel_number,38,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_39         = Systematics.weightSystematics(  'theory_z_pdf_39', 'theory_z_pdf_39',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_39*MC_Norm::correct_weightZpdf(mc_channel_number,39,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_40         = Systematics.weightSystematics(  'theory_z_pdf_40', 'theory_z_pdf_40',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_40*MC_Norm::correct_weightZpdf(mc_channel_number,40,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_41         = Systematics.weightSystematics(  'theory_z_pdf_41', 'theory_z_pdf_41',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_41*MC_Norm::correct_weightZpdf(mc_channel_number,41,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))
        
sys_pdf_42         = Systematics.weightSystematics(  'theory_z_pdf_42', 'theory_z_pdf_42',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_42*MC_Norm::correct_weightZpdf(mc_channel_number,42,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_43         = Systematics.weightSystematics(  'theory_z_pdf_43', 'theory_z_pdf_43',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_43*MC_Norm::correct_weightZpdf(mc_channel_number,43,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_44         = Systematics.weightSystematics(  'theory_z_pdf_44', 'theory_z_pdf_44',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_44*MC_Norm::correct_weightZpdf(mc_channel_number,44,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_45         = Systematics.weightSystematics(  'theory_z_pdf_45', 'theory_z_pdf_45',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_45*MC_Norm::correct_weightZpdf(mc_channel_number,45,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_46         = Systematics.weightSystematics(  'theory_z_pdf_46', 'theory_z_pdf_46',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_46*MC_Norm::correct_weightZpdf(mc_channel_number,46,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_47         = Systematics.weightSystematics(  'theory_z_pdf_47', 'theory_z_pdf_47',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_47*MC_Norm::correct_weightZpdf(mc_channel_number,47,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_48         = Systematics.weightSystematics(  'theory_z_pdf_48', 'theory_z_pdf_48',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_48*MC_Norm::correct_weightZpdf(mc_channel_number,48,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_49         = Systematics.weightSystematics(  'theory_z_pdf_49', 'theory_z_pdf_49',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_49*MC_Norm::correct_weightZpdf(mc_channel_number,49,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_50         = Systematics.weightSystematics(  'theory_z_pdf_50', 'theory_z_pdf_50',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_50*MC_Norm::correct_weightZpdf(mc_channel_number,50,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_51         = Systematics.weightSystematics(  'theory_z_pdf_51', 'theory_z_pdf_51',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_51*MC_Norm::correct_weightZpdf(mc_channel_number,51,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))
        
sys_pdf_52         = Systematics.weightSystematics(  'theory_z_pdf_52', 'theory_z_pdf_52',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_52*MC_Norm::correct_weightZpdf(mc_channel_number,52,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_53         = Systematics.weightSystematics(  'theory_z_pdf_53', 'theory_z_pdf_53',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_53*MC_Norm::correct_weightZpdf(mc_channel_number,53,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_54         = Systematics.weightSystematics(  'theory_z_pdf_54', 'theory_z_pdf_54',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_54*MC_Norm::correct_weightZpdf(mc_channel_number,54,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_55         = Systematics.weightSystematics(  'theory_z_pdf_55', 'theory_z_pdf_55',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_55*MC_Norm::correct_weightZpdf(mc_channel_number,55,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_56         = Systematics.weightSystematics(  'theory_z_pdf_56', 'theory_z_pdf_56',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_56*MC_Norm::correct_weightZpdf(mc_channel_number,56,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_57         = Systematics.weightSystematics(  'theory_z_pdf_57', 'theory_z_pdf_57',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_57*MC_Norm::correct_weightZpdf(mc_channel_number,57,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_58         = Systematics.weightSystematics(  'theory_z_pdf_58', 'theory_z_pdf_58',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_58*MC_Norm::correct_weightZpdf(mc_channel_number,58,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_59         = Systematics.weightSystematics(  'theory_z_pdf_59', 'theory_z_pdf_59',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_59*MC_Norm::correct_weightZpdf(mc_channel_number,59,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_60         = Systematics.weightSystematics(  'theory_z_pdf_60', 'theory_z_pdf_60',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_60*MC_Norm::correct_weightZpdf(mc_channel_number,60,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_61         = Systematics.weightSystematics(  'theory_z_pdf_61', 'theory_z_pdf_61',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_61*MC_Norm::correct_weightZpdf(mc_channel_number,61,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))
        
sys_pdf_62         = Systematics.weightSystematics(  'theory_z_pdf_62', 'theory_z_pdf_62',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_62*MC_Norm::correct_weightZpdf(mc_channel_number,62,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_63         = Systematics.weightSystematics(  'theory_z_pdf_63', 'theory_z_pdf_63',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_63*MC_Norm::correct_weightZpdf(mc_channel_number,63,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_64         = Systematics.weightSystematics(  'theory_z_pdf_64', 'theory_z_pdf_64',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_64*MC_Norm::correct_weightZpdf(mc_channel_number,64,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_65         = Systematics.weightSystematics(  'theory_z_pdf_65', 'theory_z_pdf_65',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_65*MC_Norm::correct_weightZpdf(mc_channel_number,65,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_66         = Systematics.weightSystematics(  'theory_z_pdf_66', 'theory_z_pdf_66',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_66*MC_Norm::correct_weightZpdf(mc_channel_number,66,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_67         = Systematics.weightSystematics(  'theory_z_pdf_67', 'theory_z_pdf_67',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_67*MC_Norm::correct_weightZpdf(mc_channel_number,67,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_68         = Systematics.weightSystematics(  'theory_z_pdf_68', 'theory_z_pdf_68',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_68*MC_Norm::correct_weightZpdf(mc_channel_number,68,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_69         = Systematics.weightSystematics(  'theory_z_pdf_69', 'theory_z_pdf_69',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_69*MC_Norm::correct_weightZpdf(mc_channel_number,69,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_70         = Systematics.weightSystematics(  'theory_z_pdf_70', 'theory_z_pdf_70',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_70*MC_Norm::correct_weightZpdf(mc_channel_number,70,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_71         = Systematics.weightSystematics(  'theory_z_pdf_71', 'theory_z_pdf_71',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_71*MC_Norm::correct_weightZpdf(mc_channel_number,71,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))
        
sys_pdf_72         = Systematics.weightSystematics(  'theory_z_pdf_72', 'theory_z_pdf_72',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_72*MC_Norm::correct_weightZpdf(mc_channel_number,72,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_73         = Systematics.weightSystematics(  'theory_z_pdf_73', 'theory_z_pdf_73',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_73*MC_Norm::correct_weightZpdf(mc_channel_number,73,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_74         = Systematics.weightSystematics(  'theory_z_pdf_74', 'theory_z_pdf_74',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_74*MC_Norm::correct_weightZpdf(mc_channel_number,74,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_75         = Systematics.weightSystematics(  'theory_z_pdf_75', 'theory_z_pdf_75',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_75*MC_Norm::correct_weightZpdf(mc_channel_number,75,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_76         = Systematics.weightSystematics(  'theory_z_pdf_76', 'theory_z_pdf_76',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_76*MC_Norm::correct_weightZpdf(mc_channel_number,76,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_77         = Systematics.weightSystematics(  'theory_z_pdf_77', 'theory_z_pdf_77',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_77*MC_Norm::correct_weightZpdf(mc_channel_number,77,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_78         = Systematics.weightSystematics(  'theory_z_pdf_78', 'theory_z_pdf_78',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_78*MC_Norm::correct_weightZpdf(mc_channel_number,78,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_79         = Systematics.weightSystematics(  'theory_z_pdf_79', 'theory_z_pdf_79',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_79*MC_Norm::correct_weightZpdf(mc_channel_number,79,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_80         = Systematics.weightSystematics(  'theory_z_pdf_80', 'theory_z_pdf_80',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_80*MC_Norm::correct_weightZpdf(mc_channel_number,80,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_81         = Systematics.weightSystematics(  'theory_z_pdf_81', 'theory_z_pdf_81',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_81*MC_Norm::correct_weightZpdf(mc_channel_number,81,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))
        
sys_pdf_82         = Systematics.weightSystematics(  'theory_z_pdf_82', 'theory_z_pdf_82',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_82*MC_Norm::correct_weightZpdf(mc_channel_number,82,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_83         = Systematics.weightSystematics(  'theory_z_pdf_83', 'theory_z_pdf_83',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_83*MC_Norm::correct_weightZpdf(mc_channel_number,83,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_84         = Systematics.weightSystematics(  'theory_z_pdf_84', 'theory_z_pdf_84',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_84*MC_Norm::correct_weightZpdf(mc_channel_number,84,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_85         = Systematics.weightSystematics(  'theory_z_pdf_85', 'theory_z_pdf_85',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_85*MC_Norm::correct_weightZpdf(mc_channel_number,85,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_86         = Systematics.weightSystematics(  'theory_z_pdf_86', 'theory_z_pdf_86',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_86*MC_Norm::correct_weightZpdf(mc_channel_number,86,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_87         = Systematics.weightSystematics(  'theory_z_pdf_87', 'theory_z_pdf_87',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_87*MC_Norm::correct_weightZpdf(mc_channel_number,87,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_88         = Systematics.weightSystematics(  'theory_z_pdf_88', 'theory_z_pdf_88',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_88*MC_Norm::correct_weightZpdf(mc_channel_number,88,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_89         = Systematics.weightSystematics(  'theory_z_pdf_89', 'theory_z_pdf_89',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_89*MC_Norm::correct_weightZpdf(mc_channel_number,89,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_90         = Systematics.weightSystematics(  'theory_z_pdf_90', 'theory_z_pdf_90',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_90*MC_Norm::correct_weightZpdf(mc_channel_number,90,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_91         = Systematics.weightSystematics(  'theory_z_pdf_91', 'theory_z_pdf_91',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_91*MC_Norm::correct_weightZpdf(mc_channel_number,91,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))
        
sys_pdf_92         = Systematics.weightSystematics(  'theory_z_pdf_92', 'theory_z_pdf_92',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_92*MC_Norm::correct_weightZpdf(mc_channel_number,92,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_93         = Systematics.weightSystematics(  'theory_z_pdf_93', 'theory_z_pdf_93',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_93*MC_Norm::correct_weightZpdf(mc_channel_number,93,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_94         = Systematics.weightSystematics(  'theory_z_pdf_94', 'theory_z_pdf_94',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_94*MC_Norm::correct_weightZpdf(mc_channel_number,94,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_95         = Systematics.weightSystematics(  'theory_z_pdf_95', 'theory_z_pdf_95',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_95*MC_Norm::correct_weightZpdf(mc_channel_number,95,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_96         = Systematics.weightSystematics(  'theory_z_pdf_96', 'theory_z_pdf_96',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_96*MC_Norm::correct_weightZpdf(mc_channel_number,96,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_97         = Systematics.weightSystematics(  'theory_z_pdf_97', 'theory_z_pdf_97',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_97*MC_Norm::correct_weightZpdf(mc_channel_number,97,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_98         = Systematics.weightSystematics(  'theory_z_pdf_98', 'theory_z_pdf_98',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_98*MC_Norm::correct_weightZpdf(mc_channel_number,98,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_99         = Systematics.weightSystematics(  'theory_z_pdf_99', 'theory_z_pdf_99',
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_pdf_ztt_weight_99*MC_Norm::correct_weightZpdf(mc_channel_number,99,NOMINAL_pileup_random_run_number)/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_ct14      = Systematics.weightSystematics( 'theory_z_CT14_pdfset',    'theory_z_CT14_pdfset',   
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_CT14_pdfset/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_pdf_mmht      = Systematics.weightSystematics( 'theory_z_MMHT_pdfset',    'theory_z_MMHT_pdfset',   
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_MMHT_pdfset/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'),
                                                   '({})'.format('1'))

sys_alpha         = Systematics.weightSystematics( 'theory_z_alphaS',      'theory_z_alphaS',     
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_alphaS_up/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'), 
                                                   'MC_Norm::get_mc_weight({dsid},{weight})'.format( dsid = 'mc_channel_number', weight = 'theory_weights_alphaS_down/theory_weights_LHE3Weight_MUR1_MUF1_PDF261000'),
                                                   '({})'.format('1'))

# Signal Theory systematics 
# UPDATED
sys_sig_alpha      = Systematics.weightSystematics( 'theory_sig_alphaS', 'theory_sig_alphaS', 
                                                    '({})'.format('theory_weights_alphaS_up'),
                                                    '({})'.format('theory_weights_alphaS_down'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_0   = Systematics.weightSystematics( 'theory_sig_pdf_0',  'theory_sig_pdf_0',
                                                    '({})'.format('theory_weights_pdf_signal_weight_0)'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_1   = Systematics.weightSystematics( 'theory_sig_pdf_1',  'theory_sig_pdf_1',  
                                                    '({})'.format('theory_weights_pdf_signal_weight_1'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_2   = Systematics.weightSystematics( 'theory_sig_pdf_2',  'theory_sig_pdf_2',  
                                                    '({})'.format('theory_weights_pdf_signal_weight_2'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_3   = Systematics.weightSystematics( 'theory_sig_pdf_3',  'theory_sig_pdf_3',  
                                                    '({})'.format('theory_weights_pdf_signal_weight_3'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_4   = Systematics.weightSystematics( 'theory_sig_pdf_4',  'theory_sig_pdf_4',  
                                                    '({})'.format('theory_weights_pdf_signal_weight_4'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_5   = Systematics.weightSystematics( 'theory_sig_pdf_5',  'theory_sig_pdf_5',
                                                    '({})'.format('theory_weights_pdf_signal_weight_5'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_6   = Systematics.weightSystematics( 'theory_sig_pdf_6',  'theory_sig_pdf_6',
                                                    '({})'.format('theory_weights_pdf_signal_weight_6'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_7   = Systematics.weightSystematics( 'theory_sig_pdf_7',  'theory_sig_pdf_7',
                                                    '({})'.format('theory_weights_pdf_signal_weight_7'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_8   = Systematics.weightSystematics( 'theory_sig_pdf_8',  'theory_sig_pdf_8',
                                                    '({})'.format('theory_weights_pdf_signal_weight_8'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_9   = Systematics.weightSystematics( 'theory_sig_pdf_9',  'theory_sig_pdf_9',
                                                    '({})'.format('theory_weights_pdf_signal_weight_9'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_10  = Systematics.weightSystematics( 'theory_sig_pdf_10',  'theory_sig_pdf_10',
                                                    '({})'.format('theory_weights_pdf_signal_weight_10'),
                                                    '({})'.format('theory_weights_nominal'),  
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_11  = Systematics.weightSystematics( 'theory_sig_pdf_11',  'theory_sig_pdf_11',
                                                    '({})'.format('theory_weights_pdf_signal_weight_11'),
                                                    '({})'.format('theory_weights_nominal'),  
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_12  = Systematics.weightSystematics( 'theory_sig_pdf_12',  'theory_sig_pdf_12',
                                                    '({})'.format('theory_weights_pdf_signal_weight_12'),
                                                    '({})'.format('theory_weights_nominal'),  
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_13  = Systematics.weightSystematics( 'theory_sig_pdf_13',  'theory_sig_pdf_13',
                                                    '({})'.format('theory_weights_pdf_signal_weight_13'),
                                                    '({})'.format('theory_weights_nominal'),  
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_14  = Systematics.weightSystematics( 'theory_sig_pdf_14',  'theory_sig_pdf_14',
                                                    '({})'.format('theory_weights_pdf_signal_weight_14'),
                                                    '({})'.format('theory_weights_nominal'),  
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_15  = Systematics.weightSystematics( 'theory_sig_pdf_15',  'theory_sig_pdf_15',
                                                    '({})'.format('theory_weights_pdf_signal_weight_15'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_16  = Systematics.weightSystematics( 'theory_sig_pdf_16',  'theory_sig_pdf_16',
                                                    '({})'.format('theory_weights_pdf_signal_weight_16'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_17  = Systematics.weightSystematics( 'theory_sig_pdf_17',  'theory_sig_pdf_17',
                                                    '({})'.format('theory_weights_pdf_signal_weight_17'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_18  = Systematics.weightSystematics( 'theory_sig_pdf_18',  'theory_sig_pdf_18',
                                                    '({})'.format('theory_weights_pdf_signal_weight_18'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_19  = Systematics.weightSystematics( 'theory_sig_pdf_19',  'theory_sig_pdf_19',
                                                    '({})'.format('theory_weights_pdf_signal_weight_19'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_20  = Systematics.weightSystematics( 'theory_sig_pdf_20',  'theory_sig_pdf_20',
                                                    '({})'.format('theory_weights_pdf_signal_weight_20'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_21  = Systematics.weightSystematics( 'theory_sig_pdf_21',  'theory_sig_pdf_21',
                                                    '({})'.format('theory_weights_pdf_signal_weight_21'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_22  = Systematics.weightSystematics( 'theory_sig_pdf_22',  'theory_sig_pdf_22',
                                                    '({})'.format('theory_weights_pdf_signal_weight_22'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_23  = Systematics.weightSystematics( 'theory_sig_pdf_23',  'theory_sig_pdf_23',
                                                    '({})'.format('theory_weights_pdf_signal_weight_23'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_24  = Systematics.weightSystematics( 'theory_sig_pdf_24',  'theory_sig_pdf_24',
                                                    '({})'.format('theory_weights_pdf_signal_weight_24'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_25  = Systematics.weightSystematics( 'theory_sig_pdf_25',  'theory_sig_pdf_25',
                                                    '({})'.format('theory_weights_pdf_signal_weight_25'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_26  = Systematics.weightSystematics( 'theory_sig_pdf_26',  'theory_sig_pdf_26',
                                                    '({})'.format('theory_weights_pdf_signal_weight_26'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_27  = Systematics.weightSystematics( 'theory_sig_pdf_27',  'theory_sig_pdf_27',
                                                    '({})'.format('theory_weights_pdf_signal_weight_27'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_28  = Systematics.weightSystematics( 'theory_sig_pdf_28',  'theory_sig_pdf_28',
                                                    '({})'.format('theory_weights_pdf_signal_weight_28'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_pdf_weight_29  = Systematics.weightSystematics( 'theory_sig_pdf_29',  'theory_sig_pdf_29',
                                                    '({})'.format('theory_weights_pdf_signal_weight_29'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_qcd_np0   = Systematics.weightSystematics( 'theory_sig_qcd_0',  'theory_sig_qcd_0',  
                                                    '({})'.format('theory_weights_qcd_weight_0'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_qcd_np1   = Systematics.weightSystematics( 'theory_sig_qcd_1',  'theory_sig_qcd_1',  
                                                    '({})'.format('theory_weights_qcd_weight_1'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_qcd_np2   = Systematics.weightSystematics( 'theory_sig_qcd_2',  'theory_sig_qcd_2',  
                                                    '({})'.format('theory_weights_qcd_weight_2'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_qcd_np3   = Systematics.weightSystematics( 'theory_sig_qcd_3',  'theory_sig_qcd_3',  
                                                    '({})'.format('theory_weights_qcd_weight_3'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_qcd_np4   = Systematics.weightSystematics( 'theory_sig_qcd_4',  'theory_sig_qcd_4',  
                                                    '({})'.format('theory_weights_qcd_weight_4'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_qcd_np5   = Systematics.weightSystematics( 'theory_sig_qcd_5',  'theory_sig_qcd_5',    
                                                    '({})'.format('theory_weights_qcd_weight_5'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_qcd_np6   = Systematics.weightSystematics( 'theory_sig_qcd_6',  'theory_sig_qcd_6',    
                                                    '({})'.format('theory_weights_qcd_weight_6'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_qcd_np7   = Systematics.weightSystematics( 'theory_sig_qcd_7',  'theory_sig_qcd_7',
                                                    '({})'.format('theory_weights_qcd_weight_7'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

sys_qcd_np8   = Systematics.weightSystematics( 'theory_sig_qcd_8',  'theory_sig_qcd_8',
                                                    '({})'.format('theory_weights_qcd_weight_8'),
                                                    '({})'.format('theory_weights_nominal'),
                                                    '({})'.format('theory_weights_nominal'))

# dictionary structure to change normalisation bin in the h_metadata_theory_weights
norm_pdf_theoy_syst = {

                  'theory_sig_pdf_0'  : ['h_metadata_theory_weights', 1],
                  'theory_sig_pdf_1'  : ['h_metadata_theory_weights', 2],
                  'theory_sig_pdf_2'  : ['h_metadata_theory_weights', 3],
                  'theory_sig_pdf_3'  : ['h_metadata_theory_weights', 4],
                  'theory_sig_pdf_4'  : ['h_metadata_theory_weights', 5],
                  'theory_sig_pdf_5'  : ['h_metadata_theory_weights', 6],
                  'theory_sig_pdf_6'  : ['h_metadata_theory_weights', 7],
                  'theory_sig_pdf_7'  : ['h_metadata_theory_weights', 8],
                  'theory_sig_pdf_8'  : ['h_metadata_theory_weights', 9],
                  'theory_sig_pdf_9'  : ['h_metadata_theory_weights', 10],
                  'theory_sig_pdf_10' : ['h_metadata_theory_weights', 11],
                  'theory_sig_pdf_11' : ['h_metadata_theory_weights', 12],
                  'theory_sig_pdf_12' : ['h_metadata_theory_weights', 13],
                  'theory_sig_pdf_13' : ['h_metadata_theory_weights', 14],
                  'theory_sig_pdf_14' : ['h_metadata_theory_weights', 15],
                  'theory_sig_pdf_15' : ['h_metadata_theory_weights', 16],
                  'theory_sig_pdf_16' : ['h_metadata_theory_weights', 17],
                  'theory_sig_pdf_17' : ['h_metadata_theory_weights', 18],
                  'theory_sig_pdf_18' : ['h_metadata_theory_weights', 19],
                  'theory_sig_pdf_19' : ['h_metadata_theory_weights', 20],
                  'theory_sig_pdf_20' : ['h_metadata_theory_weights', 21],
                  'theory_sig_pdf_21' : ['h_metadata_theory_weights', 22],
                  'theory_sig_pdf_22' : ['h_metadata_theory_weights', 23],
                  'theory_sig_pdf_23' : ['h_metadata_theory_weights', 24],
                  'theory_sig_pdf_24' : ['h_metadata_theory_weights', 25],
                  'theory_sig_pdf_25' : ['h_metadata_theory_weights', 26],
                  'theory_sig_pdf_26' : ['h_metadata_theory_weights', 27],
                  'theory_sig_pdf_27' : ['h_metadata_theory_weights', 28],
                  'theory_sig_pdf_28' : ['h_metadata_theory_weights', 29],
                  'theory_sig_pdf_29' : ['h_metadata_theory_weights', 30],

                  'theory_sig_alphaS_low'  : ['h_metadata_theory_weights', 31],
                  'theory_sig_alphaS_high' : ['h_metadata_theory_weights', 32],
                  }


norm_mur_muf_theoy_syst = {

                
                  'VBFHtt' : {
                              'theory_sig_qcd_0'  : ['h_metadata_theory_weights', 33],
                              'theory_sig_qcd_1'  : ['h_metadata_theory_weights', 38],
                              'theory_sig_qcd_2'  : ['h_metadata_theory_weights', 34],
                              'theory_sig_qcd_3'  : ['h_metadata_theory_weights', 39],
                              'theory_sig_qcd_4'  : ['h_metadata_theory_weights', 35],
                              'theory_sig_qcd_5'  : ['h_metadata_theory_weights', 40],
                          },
                           
                  'ZHtt' :  {
                              'theory_sig_qcd_0'  : ['h_metadata_theory_weights', 33],
                              'theory_sig_qcd_1'  : ['h_metadata_theory_weights', 36],
                              'theory_sig_qcd_2'  : ['h_metadata_theory_weights', 38],
                              'theory_sig_qcd_3'  : ['h_metadata_theory_weights', 34],
                              'theory_sig_qcd_4'  : ['h_metadata_theory_weights', 39],
                              'theory_sig_qcd_5'  : ['h_metadata_theory_weights', 35],
                              'theory_sig_qcd_6'  : ['h_metadata_theory_weights', 37],
                              'theory_sig_qcd_7'  : ['h_metadata_theory_weights', 40],
                          },

                  'WHtt' :  {
                              'theory_sig_qcd_0'  : ['h_metadata_theory_weights', 33],
                              'theory_sig_qcd_1'  : ['h_metadata_theory_weights', 36],
                              'theory_sig_qcd_2'  : ['h_metadata_theory_weights', 38],
                              'theory_sig_qcd_3'  : ['h_metadata_theory_weights', 34],
                              'theory_sig_qcd_4'  : ['h_metadata_theory_weights', 39],
                              'theory_sig_qcd_5'  : ['h_metadata_theory_weights', 35],
                              'theory_sig_qcd_6'  : ['h_metadata_theory_weights', 37],
                              'theory_sig_qcd_7'  : ['h_metadata_theory_weights', 40],
                          },
                  }
Z_pdf_theory_syst = {

                  'nominal'          : ['h_metadata_theory_weights', 8],
                  'theory_z_pdf_0'   : ['h_metadata_theory_weights', 12],
                  'theory_z_pdf_1'   : ['h_metadata_theory_weights', 13],
                  'theory_z_pdf_2'   : ['h_metadata_theory_weights', 14],
                  'theory_z_pdf_3'   : ['h_metadata_theory_weights', 15],
                  'theory_z_pdf_4'   : ['h_metadata_theory_weights', 16],
                  'theory_z_pdf_5'   : ['h_metadata_theory_weights', 17],
                  'theory_z_pdf_6'   : ['h_metadata_theory_weights', 18],
                  'theory_z_pdf_7'   : ['h_metadata_theory_weights', 19],
                  'theory_z_pdf_8'   : ['h_metadata_theory_weights', 20],
                  'theory_z_pdf_9'   : ['h_metadata_theory_weights', 21],

                  'theory_z_pdf_10'  : ['h_metadata_theory_weights', 22],
                  'theory_z_pdf_11'  : ['h_metadata_theory_weights', 23],
                  'theory_z_pdf_12'  : ['h_metadata_theory_weights', 24],
                  'theory_z_pdf_13'  : ['h_metadata_theory_weights', 25],
                  'theory_z_pdf_14'  : ['h_metadata_theory_weights', 26],
                  'theory_z_pdf_15'  : ['h_metadata_theory_weights', 27],
                  'theory_z_pdf_16'  : ['h_metadata_theory_weights', 28],
                  'theory_z_pdf_17'  : ['h_metadata_theory_weights', 29],
                  'theory_z_pdf_18'  : ['h_metadata_theory_weights', 30],
                  'theory_z_pdf_19'  : ['h_metadata_theory_weights', 31],

                  'theory_z_pdf_20'  : ['h_metadata_theory_weights', 32],
                  'theory_z_pdf_21'  : ['h_metadata_theory_weights', 33],
                  'theory_z_pdf_22'  : ['h_metadata_theory_weights', 34],
                  'theory_z_pdf_23'  : ['h_metadata_theory_weights', 35],
                  'theory_z_pdf_24'  : ['h_metadata_theory_weights', 36],
                  'theory_z_pdf_25'  : ['h_metadata_theory_weights', 37],
                  'theory_z_pdf_26'  : ['h_metadata_theory_weights', 38],
                  'theory_z_pdf_27'  : ['h_metadata_theory_weights', 39],
                  'theory_z_pdf_28'  : ['h_metadata_theory_weights', 40],
                  'theory_z_pdf_29'  : ['h_metadata_theory_weights', 41],

                  'theory_z_pdf_30'  : ['h_metadata_theory_weights', 42],
                  'theory_z_pdf_31'  : ['h_metadata_theory_weights', 43],
                  'theory_z_pdf_32'  : ['h_metadata_theory_weights', 44],
                  'theory_z_pdf_33'  : ['h_metadata_theory_weights', 45],
                  'theory_z_pdf_34'  : ['h_metadata_theory_weights', 46],
                  'theory_z_pdf_35'  : ['h_metadata_theory_weights', 47],
                  'theory_z_pdf_36'  : ['h_metadata_theory_weights', 48],
                  'theory_z_pdf_37'  : ['h_metadata_theory_weights', 49],
                  'theory_z_pdf_38'  : ['h_metadata_theory_weights', 50],
                  'theory_z_pdf_39'  : ['h_metadata_theory_weights', 51],

                  'theory_z_pdf_40'  : ['h_metadata_theory_weights', 52],
                  'theory_z_pdf_41'  : ['h_metadata_theory_weights', 53],
                  'theory_z_pdf_42'  : ['h_metadata_theory_weights', 54],
                  'theory_z_pdf_43'  : ['h_metadata_theory_weights', 55],
                  'theory_z_pdf_44'  : ['h_metadata_theory_weights', 56],
                  'theory_z_pdf_45'  : ['h_metadata_theory_weights', 57],
                  'theory_z_pdf_46'  : ['h_metadata_theory_weights', 58],
                  'theory_z_pdf_47'  : ['h_metadata_theory_weights', 59],
                  'theory_z_pdf_48'  : ['h_metadata_theory_weights', 60],
                  'theory_z_pdf_49'  : ['h_metadata_theory_weights', 61],

                  'theory_z_pdf_50'  : ['h_metadata_theory_weights', 62],
                  'theory_z_pdf_51'  : ['h_metadata_theory_weights', 63],
                  'theory_z_pdf_52'  : ['h_metadata_theory_weights', 64],
                  'theory_z_pdf_53'  : ['h_metadata_theory_weights', 65],
                  'theory_z_pdf_54'  : ['h_metadata_theory_weights', 66],
                  'theory_z_pdf_55'  : ['h_metadata_theory_weights', 67],
                  'theory_z_pdf_56'  : ['h_metadata_theory_weights', 68],
                  'theory_z_pdf_57'  : ['h_metadata_theory_weights', 69],
                  'theory_z_pdf_58'  : ['h_metadata_theory_weights', 70],
                  'theory_z_pdf_59'  : ['h_metadata_theory_weights', 71],

                  'theory_z_pdf_60'  : ['h_metadata_theory_weights', 72],
                  'theory_z_pdf_61'  : ['h_metadata_theory_weights', 73],
                  'theory_z_pdf_62'  : ['h_metadata_theory_weights', 74],
                  'theory_z_pdf_63'  : ['h_metadata_theory_weights', 75],
                  'theory_z_pdf_64'  : ['h_metadata_theory_weights', 76],
                  'theory_z_pdf_65'  : ['h_metadata_theory_weights', 77],
                  'theory_z_pdf_66'  : ['h_metadata_theory_weights', 78],
                  'theory_z_pdf_67'  : ['h_metadata_theory_weights', 79],
                  'theory_z_pdf_68'  : ['h_metadata_theory_weights', 80],
                  'theory_z_pdf_69'  : ['h_metadata_theory_weights', 81],

                  'theory_z_pdf_70'  : ['h_metadata_theory_weights', 82],
                  'theory_z_pdf_71'  : ['h_metadata_theory_weights', 83],
                  'theory_z_pdf_72'  : ['h_metadata_theory_weights', 84],
                  'theory_z_pdf_73'  : ['h_metadata_theory_weights', 85],
                  'theory_z_pdf_74'  : ['h_metadata_theory_weights', 86],
                  'theory_z_pdf_75'  : ['h_metadata_theory_weights', 87],
                  'theory_z_pdf_76'  : ['h_metadata_theory_weights', 88],
                  'theory_z_pdf_77'  : ['h_metadata_theory_weights', 89],
                  'theory_z_pdf_78'  : ['h_metadata_theory_weights', 90],
                  'theory_z_pdf_79'  : ['h_metadata_theory_weights', 91],

                  'theory_z_pdf_80'  : ['h_metadata_theory_weights', 92],
                  'theory_z_pdf_81'  : ['h_metadata_theory_weights', 93],
                  'theory_z_pdf_82'  : ['h_metadata_theory_weights', 94],
                  'theory_z_pdf_83'  : ['h_metadata_theory_weights', 95],
                  'theory_z_pdf_84'  : ['h_metadata_theory_weights', 96],
                  'theory_z_pdf_85'  : ['h_metadata_theory_weights', 97],
                  'theory_z_pdf_86'  : ['h_metadata_theory_weights', 98],
                  'theory_z_pdf_87'  : ['h_metadata_theory_weights', 99],
                  'theory_z_pdf_88'  : ['h_metadata_theory_weights', 100],
                  'theory_z_pdf_89'  : ['h_metadata_theory_weights', 101],

                  'theory_z_pdf_90'  : ['h_metadata_theory_weights', 102],
                  'theory_z_pdf_91'  : ['h_metadata_theory_weights', 103],
                  'theory_z_pdf_92'  : ['h_metadata_theory_weights', 104],
                  'theory_z_pdf_93'  : ['h_metadata_theory_weights', 105],
                  'theory_z_pdf_94'  : ['h_metadata_theory_weights', 106],
                  'theory_z_pdf_95'  : ['h_metadata_theory_weights', 107],
                  'theory_z_pdf_96'  : ['h_metadata_theory_weights', 108],
                  'theory_z_pdf_97'  : ['h_metadata_theory_weights', 109],
                  'theory_z_pdf_98'  : ['h_metadata_theory_weights', 110],
                  'theory_z_pdf_99'  : ['h_metadata_theory_weights', 111],

                  'theory_z_lhe3weight_mur05_muf05_pdf261000' : ['h_metadata_theory_weights', 5],
                  'theory_z_lhe3weight_mur05_muf1_pdf261000'  : ['h_metadata_theory_weights', 6],
                  'theory_z_lhe3weight_mur1_muf05_pdf261000'  : ['h_metadata_theory_weights', 7],
                  'theory_z_lhe3weight_mur1_muf2_pdf261000'   : ['h_metadata_theory_weights', 9],
                  'theory_z_lhe3weight_mur2_muf1_pdf261000'   : ['h_metadata_theory_weights', 10],
                  'theory_z_lhe3weight_mur2_muf2_pdf261000'   : ['h_metadata_theory_weights', 11],








                   }
weight_z_theory_syst = DecoratedSystematics(
           'ztt_theory_systematics',
           'Ztt theory systematics collection',
           systSet = SystematicsSet(set([ sys_mur05_muf05,
                                          sys_mur05_muf1,
                                          sys_mur1_muf05,
                                          sys_mur1_muf2,
                                          sys_mur2_muf1,
                                          sys_mur2_muf2,
                                          #sys_pdf_central,
                                          sys_pdf_0,
                                          sys_pdf_1,
                                          sys_pdf_2,
                                          sys_pdf_3,
                                          sys_pdf_4,
                                          sys_pdf_5,
                                          sys_pdf_6,
                                          sys_pdf_7,
                                          sys_pdf_8,
                                          sys_pdf_9,
                                          sys_pdf_10,
                                          sys_pdf_11,
                                          sys_pdf_12,
                                          sys_pdf_13,
                                          sys_pdf_14,
                                          sys_pdf_15,
                                          sys_pdf_16,
                                          sys_pdf_17,
                                          sys_pdf_18,
                                          sys_pdf_19,
                                          sys_pdf_20,
                                          sys_pdf_21,
                                          sys_pdf_22,
                                          sys_pdf_23,
                                          sys_pdf_24,
                                          sys_pdf_25,
                                          sys_pdf_26,
                                          sys_pdf_27,
                                          sys_pdf_28,
                                          sys_pdf_29,
                                          sys_pdf_30,
                                          sys_pdf_31,
                                          sys_pdf_32,
                                          sys_pdf_33,
                                          sys_pdf_34,
                                          sys_pdf_35,
                                          sys_pdf_36,
                                          sys_pdf_37,
                                          sys_pdf_38,
                                          sys_pdf_39,
                                          sys_pdf_40,
                                          sys_pdf_41,
                                          sys_pdf_42,
                                          sys_pdf_43,
                                          sys_pdf_44,
                                          sys_pdf_45,
                                          sys_pdf_46,
                                          sys_pdf_47,
                                          sys_pdf_48,
                                          sys_pdf_49,
                                          sys_pdf_50,
                                          sys_pdf_51,
                                          sys_pdf_52,
                                          sys_pdf_53,
                                          sys_pdf_54,
                                          sys_pdf_55,
                                          sys_pdf_56,
                                          sys_pdf_57,
                                          sys_pdf_58,
                                          sys_pdf_59,
                                          sys_pdf_60,
                                          sys_pdf_61,
                                          sys_pdf_62,
                                          sys_pdf_63,
                                          sys_pdf_64,
                                          sys_pdf_65,
                                          sys_pdf_66,
                                          sys_pdf_67,
                                          sys_pdf_68,
                                          sys_pdf_69,
                                          sys_pdf_70,
                                          sys_pdf_71,
                                          sys_pdf_72,
                                          sys_pdf_73,
                                          sys_pdf_74,
                                          sys_pdf_75,
                                          sys_pdf_76,
                                          sys_pdf_77,
                                          sys_pdf_78,
                                          sys_pdf_79,
                                          sys_pdf_80,
                                          sys_pdf_81,
                                          sys_pdf_82,
                                          sys_pdf_83,
                                          sys_pdf_84,
                                          sys_pdf_85,
                                          sys_pdf_86,
                                          sys_pdf_87,
                                          sys_pdf_88,
                                          sys_pdf_89,
                                          sys_pdf_90,
                                          sys_pdf_91,
                                          sys_pdf_92,
                                          sys_pdf_93,
                                          sys_pdf_94,
                                          sys_pdf_95,
                                          sys_pdf_96,
                                          sys_pdf_97,
                                          sys_pdf_98,
                                          sys_pdf_99,
                                          sys_pdf_ct14,
                                          sys_pdf_mmht,
                                          sys_alpha,
                                        ])))

weight_signal_theory_syst = DecoratedSystematics(
           'signal_theory_syst',
           'Commong signal theory systematics',
           systSet = SystematicsSet(set([ sys_sig_alpha,
                                          sys_pdf_weight_0,
                                          sys_pdf_weight_1,
                                          sys_pdf_weight_2,
                                          sys_pdf_weight_3,
                                          sys_pdf_weight_4,
                                          sys_pdf_weight_5,
                                          sys_pdf_weight_6,
                                          sys_pdf_weight_7,
					  sys_pdf_weight_8,
					  sys_pdf_weight_9,
					  sys_pdf_weight_10,
					  sys_pdf_weight_11,
					  sys_pdf_weight_12,
 					  sys_pdf_weight_13,
 					  sys_pdf_weight_14,
					  sys_pdf_weight_15,
					  sys_pdf_weight_16,
					  sys_pdf_weight_17,
 					  sys_pdf_weight_18,
					  sys_pdf_weight_19,
					  sys_pdf_weight_20,
					  sys_pdf_weight_21,
					  sys_pdf_weight_22,
					  sys_pdf_weight_23,
					  sys_pdf_weight_24,
					  sys_pdf_weight_25,
					  sys_pdf_weight_26,
 					  sys_pdf_weight_27,
					  sys_pdf_weight_28,
					  sys_pdf_weight_29,
                                        ])),
          signal_only=False)

weight_vbf_theory_syst = DecoratedSystematics(
          'vbf_theory_syst',
          'VBF signal theory systematics',
          systSet = SystematicsSet(set([  sys_qcd_np0,
                                          sys_qcd_np1,
                                          sys_qcd_np2,
                                          sys_qcd_np3,
                                          sys_qcd_np4,
                                          sys_qcd_np5,
                                          sys_qcd_np6,
                                          sys_qcd_np7,
                                         ])),
          signal_only=False)

weight_vh_theory_syst = DecoratedSystematics(
         'vh_theory_syst',
         'VH signal theory systematics',
         systSet = SystematicsSet(set([ sys_qcd_np0,
                                          sys_qcd_np1, 
                                          sys_qcd_np2,
                                          sys_qcd_np3,
                                          sys_qcd_np4,
                                          sys_qcd_np5,
                                          sys_qcd_np6,
                                          sys_qcd_np7,
                                        ])),
         signal_only=False)

weight_ggh_theory_syst = DecoratedSystematics(
         'ggH_theory_syst',
         'ggH signal theory systematics',
         systSet = SystematicsSet(set([ sys_qcd_np0,
                                        sys_qcd_np1,
                                        sys_qcd_np2,
                                        sys_qcd_np3,
                                        sys_qcd_np4,
                                        sys_qcd_np5,
                                        sys_qcd_np6,
                                        sys_qcd_np7,
                                        sys_qcd_np8,
                                       ])),
        signal_only=False)
