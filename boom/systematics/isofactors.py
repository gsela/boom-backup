from happy.systematics import SystematicsSet, Systematics
from .base import DecoratedSystematics, SYSTBOOK
from ..fakefactors import isofac_expr

# 1 prong
_systs_1p = []
for _syst in ['stat_1prong', 'syst_1prong']:
    _nom_expr = isofac_expr('nominal')
    _up_expr = isofac_expr(_syst + '_up')
    _do_expr = isofac_expr(_syst + '_down')
    
    _weight_syst = Systematics.weightSystematics(
        'isofac_' + _syst,
        'isofac_' + _syst,
        _up_expr,
        _do_expr,
        _nom_expr)
    _systs_1p.append(_weight_syst)

weight_isofac_1prong = DecoratedSystematics(
    'isofac_1prong',
    'isofac 1prong uncertainty',
    systSet = SystematicsSet(set(_systs_1p)),
    channels=['e1p','mu1p','ehad','muhad'],
    regions=['qcd_lh_anti_tau_truth_lep'],
    force_data_application=True)
SYSTBOOK.append(weight_isofac_1prong)

# 1 prong closure
_systs_1p_closure = []
for _syst in ['stat_1prong', 'syst_1prong']:
    _nom_expr = isofac_expr('nominal',do_closure=True)
    _up_expr = isofac_expr(_syst + '_up',do_closure=True)
    _do_expr = isofac_expr(_syst + '_down',do_closure=True)

    _weight_syst = Systematics.weightSystematics(
        'isofac_' + _syst,
        'isofac_' + _syst,
        _up_expr,
        _do_expr,
        _nom_expr)
    _systs_1p_closure.append(_weight_syst)

weight_isofac_1prong_closure = DecoratedSystematics(
    'isofac_1prong_closure',
    'isofac 1prong uncertainty closure',
    systSet = SystematicsSet(set(_systs_1p_closure)),
    channels=['e1p','mu1p','ehad','muhad'],
    regions=['qcd_lh_same_sign_anti_tau_truth_lep'],
    force_data_application=True)
SYSTBOOK.append(weight_isofac_1prong_closure)

# 3 prong
_systs_3p = []
for _syst in ['stat_3prong', 'syst_3prong']:
    _nom_expr = isofac_expr('nominal')
    _up_expr = isofac_expr(_syst + '_up')
    _do_expr = isofac_expr(_syst + '_down')
    
    _weight_syst = Systematics.weightSystematics(
        'isofac_' + _syst,
        'isofac_' + _syst,
        _up_expr,
        _do_expr,
        _nom_expr)
    _systs_3p.append(_weight_syst)

weight_isofac_3prong = DecoratedSystematics(
    'isofac_3prong',
    'isofac 3prong uncertainty',
    systSet = SystematicsSet(set(_systs_3p)),
    channels=['e3p','mu3p','ehad','muhad'],
    regions=['qcd_lh_anti_tau_truth_lep'],
    force_data_application=True)
SYSTBOOK.append(weight_isofac_3prong)

# 3 prong closure
_systs_3p_closure = []
for _syst in ['stat_3prong', 'syst_3prong']:
    _nom_expr = isofac_expr('nominal',do_closure=True)
    _up_expr = isofac_expr(_syst + '_up',do_closure=True)
    _do_expr = isofac_expr(_syst + '_down',do_closure=True)

    _weight_syst = Systematics.weightSystematics(
        'isofac_' + _syst,
        'isofac_' + _syst,
        _up_expr,
        _do_expr,
        _nom_expr)
    _systs_3p_closure.append(_weight_syst)

weight_isofac_3prong_closure = DecoratedSystematics(
    'isofac_3prong_closure',
    'isofac 3prong uncertainty closure',
    systSet = SystematicsSet(set(_systs_3p_closure)),
    channels=['e3p','mu3p','ehad','muhad'],
    regions=['qcd_lh_same_sign_anti_tau_truth_lep'],
    force_data_application=True)
SYSTBOOK.append(weight_isofac_3prong_closure)
