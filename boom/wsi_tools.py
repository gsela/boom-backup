import ROOT
import os

class workspace_input(object):
    def __init__(self, rfile, verbose=False):
        self._channels = {}
        self._build(rfile, verbose=verbose)

    def _build(self, rfile, verbose=False):
        for _k in rfile.GetListOfKeys():
            print 'BOOM: reading {}'.format(_k.GetName())
            _chan = channel(_k.ReadObj())
            self._channels[_k.GetName()] = _chan

    def dump(self, output_rfile, verbose=False):
        for _channel in self.channels:
            if verbose:
                print 'BOOM: --> dumping {}'.format(_channel.name)
            output_rfile.mkdir(_channel.name)
            output_rfile.cd(_channel.name)
            _channel.lumi.Write()
            for _sample in _channel.samples:
                output_rfile.mkdir(os.path.join(_channel.name, _sample.name))
                output_rfile.cd(os.path.join(_channel.name, _sample.name))
                for _name in _sample.hist_names:
                    _sample.hist(_name).Write()

    def channel(self, name):
        if name in self._channels.keys():
            return self._channels[name]
        else:
            raise ValueError('{} does not exist in the WSI'.format(name))

    @property
    def channel_names(self):
        return self._channels.keys()

    @property
    def channels(self):
        return [_v for _, _v in self._channels.items()]

class channel(object):
    def __init__(self, rdir):
        self._name = rdir.GetName()
        self._samples = {}
        self._lumi = None
        self._build(rdir)

        self._finalstate = self._name.split('__')[0].replace('chan_', '')
        self._category = self._name.split('__')[1].replace('cat_', '')
        self._region = self._name.split('__')[2]

    def _build(self, rdir):
        for _k in rdir.GetListOfKeys():
            _obj = _k.ReadObj()
            if isinstance(_obj, ROOT.TH1):
                _obj.SetDirectory(0)
                self._lumi = _obj
            else:
                self._samples[_k.GetName()] = sample(_obj)

    def sample(self, name):
        if name in self._samples.keys():
            return self._samples[name]
        else:
            raise ValueError('{} not stored'.format(name))

    def remove_sample(self, name, verbose=False):
        if name in self._samples.keys():
            if verbose:
                print 'BOOM: --> {} - removing {}'.format(self._name, name)
            self._samples.pop(name)
        else:
            print 'BOOM: {} not in channel, do nothing'.format(name)

    def add_sample(self, sample, name=None, verbose=False):
        if name == None:
            _name = sample.name
        else:
            _name = name
            sample._name = name
        if _name in self._samples.keys():
            raise ValueError('{} already exist!'.format(_name))
        else:
            if verbose:
                print 'BOOM: --> {} - adding {}'.format(self._name, _name)
            self._samples[_name] = sample

    @property
    def name(self):
        return self._name

    @property
    def lumi(self):
        return self._lumi

    @property
    def category(self):
        return self._category

    @property
    def region(self):
        return self._region

    @property
    def finalstate(self):
        return self._finalstate

    @property
    def sr(self):
        if self._region == 'sr':
            return True
        else:
            return False

    @property
    def cr(self):
        if 'cr' in self._region:
            return True
        else:
            return False

    @property
    def samples(self):
        return [_s for _, _s in self._samples.items()]

    @property
    def sample_names(self):
        return self._samples.keys()


class sample(object):
    def __init__(self, rdir):
        self._name = rdir.GetName()
        self._hists = {}
        self._build(rdir)

    def _build(self, rdir):
        for _k in rdir.GetListOfKeys():
            _hist = _k.ReadObj()
            if isinstance(_hist, ROOT.TH1):
                _hist.SetDirectory(0)
                self._hists[_k.GetName()] = _hist
            else:
                return TypeError(type(_hist))

    def __add__(self, other):
        """
        """
        self._name += '_plus_{}'.format(other.name)

        _hist_names = self.hist_names
        # Build all the variations from the other
        _self_nom = self.nominal.Clone()
        for _name in other.hist_names:
            _hist = other.hist(_name).Clone()
            if _name in self.hist_names:
                _hist.Add(self.hist(_name))
            else:
                _hist.Add(_self_nom)
            _hist.SetName(_name)
            self._hists[_name] = _hist

        # Build all the variations from Data
        _other_nom = other.nominal.Clone()
        for _name in _hist_names:
            # skip nominal
            if _name == 'nominal':
                continue
            _hist = self.hist(_name).Clone()
            if _name in other.hist_names:
                continue
            else:
                _hist.Add(_other_nom)
            _hist.SetName(_name)
            self._hists[_name] = _hist
        return self

    @property
    def name(self):
        return self._name

    @property
    def nominal(self):
        return self._hists['nominal']

    @property
    def integral_and_error(self):
        # TODO: memoize the result
        _error = ROOT.Double(0.)
        _integral = self.nominal.IntegralAndError(0, self.nominal.GetNbinsX() + 1, _error)
        return _integral, _error

    @property
    def color(self):
        return self.nominal.GetLineColor()

    @property
    def hist_names(self):
        return self._hists.keys()

    def hist(self, name):
        if name in self._hists.keys():
            return self._hists[name]
        else:
            return ValueError
        
    def integral(self, name):
        # TODO: memoize the result
        return self.hist(name).Integral(0, self.hist(name).GetNbinsX() + 1)


    def nuisance_parameters(self, key=None):
        if key is None:
            return filter(lambda n: n != 'nominal', self.hist_names)
        else:
            return filter(lambda n: key in n and n != 'nominal', self.hist_names)
