"""
"""
import ROOT

vbf_tagger_expr = 'VBF_Tagger::vbf_bdt_score({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9})'.format(
    'pt_total', 
    'jet_0_p4.Pt()', 
    'jet_0_p4.Eta()', 
    'jet_0_p4.Phi()', 
    'jet_0_p4.M()', 
    'jet_1_p4.Pt()', 
    'jet_1_p4.Eta()', 
    'jet_1_p4.Phi()', 
    'jet_1_p4.M()',
    'Rnd::mva_random_number(event_number)')
"""vbf tagger expression"""

pt_jj_expr = 'VBF_Tagger::pt_jj({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})'.format(
    'jet_0_p4.Pt()', 
    'jet_0_p4.Eta()', 
    'jet_0_p4.Phi()', 
    'jet_0_p4.M()', 
    'jet_1_p4.Pt()', 
    'jet_1_p4.Eta()', 
    'jet_1_p4.Phi()', 
    'jet_1_p4.M()')


dr_jj_expr = 'VBF_Tagger::dr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})'.format(
    'jet_0_p4.Pt()', 
    'jet_0_p4.Eta()', 
    'jet_0_p4.Phi()', 
    'jet_0_p4.M()', 
    'jet_1_p4.Pt()', 
    'jet_1_p4.Eta()', 
    'jet_1_p4.Phi()', 
    'jet_1_p4.M()')

dr_dijetditau_expr = 'VBF_Tagger::dr({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})'.format(
    'dijet_p4.Pt()', 
    'dijet_p4.Eta()', 
    'dijet_p4.Phi()', 
    'dijet_p4.M()', 
    'ditau_p4.Pt()', 
    'ditau_p4.Eta()', 
    'ditau_p4.Phi()', 
    'ditau_p4.M()')



vh_tagger_expr = 'VH_Tagger::vh_bdt_score({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14})'.format(
    'jet_0_p4.Pt()',
    'jet_0_p4.Eta()',
    'jet_0_p4.Phi()',
    'jet_0_p4.M()',
    'jet_1_p4.Pt()',
    'jet_1_p4.Eta()',
    'jet_1_p4.Phi()',
    'jet_1_p4.M()',
    'ditau_p4.Pt()',
    'ditau_p4.Eta()',
    'ditau_p4.Phi()',
    'ditau_p4.M()',
    'met_p4.Pt()',
    'ditau_higgspt',
    'Rnd::mva_random_number(event_number)')


tth_tt_expr = 'ttH_BDTs::tth_bdt_tt_score({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8})'.format(
    "ditau_dr",
    "ditau_deta",
    "met_p4.Pt()",
    "mWbest",
    "HTjets",
    "mTopWbest",
    "ditau_p4.Pt()",
    "jjdrmin",
    "Rnd::mva_random_number(event_number)"
    )
"""tth vs ttbar tagger expression"""

tth_Z_expr = 'ttH_BDTs::tth_bdt_Z_score({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11})'.format(
    "met_p4.Pt()",
    "tau_1_p4.Pt()",
    "mWbest",
    "mTopWbest",
    "SumPtBjet",
    "ditau_deta",
    "tau_0_p4.Eta()",
    "jet_1_p4.Eta()",
    "jet_0_p4.Eta()",
    "ditau_met_min_dphi",
    "HTjets",
    "Rnd::mva_random_number(event_number)"
    )
"""tth vs ttZ tagger expression"""


