"""
helper functions to build the list of selections needed in the analysis
"""
__all__ = [
    'get_selections',
]
import sys

def filter_selections(
    selections,
    channels=None,
    categories=None,
    years=None,
    regions=None,
    triggers=None):
    """Function to filter a list of selections

    Arguments
    _________
    selections : list(selection)
       selection objects to filter on
    channels: str or list(str) 
       see CHANNELS in cuts/__init__.py
    categories: str or list(str) 
       see CATEGORIES in cuts/__init__.py
    years: str or list(str) 
       see YEARS in cuts/__init__.py
    regions: str or list(str) 
       see REGIONS in cuts/__init__.py
    triggers: str or list(str) 
       see TRIGGERS in cuts/__init__.py

    Returns
    _______
    _sels: list(selection)
       Filtered list of selection objects
    """

    _sels = selections
    if channels != None:
        if not isinstance(channels, (list, tuple)):
            channels = [channels]
        _sels = filter(lambda s: s.channel in channels, _sels)

    if categories != None:
        if not isinstance(categories, (list, tuple)):
            categories = [categories]
        _sels = filter(lambda s: s.category in categories, _sels)

    if years != None:
        if not isinstance(years, (list, tuple)):
            years = [years]
        _sels = filter(lambda s: s.year in years, _sels)

    if regions != None:
        if not isinstance(regions, (list, tuple)):
            regions = [regions]
        _sels = filter(lambda s: s.region in regions, _sels)

    if triggers != None:
        if not isinstance(triggers, (list, tuple)):
            triggers = [triggers]
        _sels = filter(lambda s: s.trigger in triggers, _sels)
    
    return _sels



def print_selection(selection, save_to_file=False):
    """Print the relevant information of a selection"""
    if save_to_file:
        orig_stdout = sys.stdout
        sys.stdout = open(selection.name + '.md', 'w')
        print '```'

    print 
    print
    print 'BOOM: --> {}'.format(selection.name)
    print 'BOOM:'
    print 'BOOM: arguments'
    print 'BOOM: ---------'
    print 'BOOM: \t channel:  {}'.format(selection.channel)
    print 'BOOM: \t year:     {}'.format(selection.year)
    print 'BOOM: \t category: {}'.format(selection.category)
    print 'BOOM: \t trigger:  {}'.format(selection.trigger)
    print 'BOOM: \t region:   {}'.format(selection.region)
    print 'BOOM: \t fake cr:  {}'.format(selection._fake_region)
    print 'BOOM:'
    print 'BOOM: list of cuts'
    print 'BOOM: ------------'
    print 'BOOM: \t Data only'
    print '      \t ---------'
    for cut in selection._cut_list_data:
        if not cut in selection._cut_list_mc:
            print 'BOOM: \t \t {}'.format(cut.cut.cut)
    print 'BOOM: \t MC only'
    print 'BOOM: \t -------'
    for cut in selection._cut_list_mc:
        if not cut in selection._cut_list_data:
            print 'BOOM: \t \t {}'.format(cut.cut.cut)
    print 'BOOM: \t Data and Simulation samples'
    print 'BOOM: \t ---------------------------'
    for cut in selection._cut_list_mc:
        if cut in selection._cut_list_data:
            print 'BOOM: \t \t {}'.format(cut.cut.cut)
            
    print 'BOOM:'
    print 'BOOM: list of nominal weights'
    print 'BOOM: -----------------------'
#     for syst in selection._weight_list:
#         _nominal_weights = []
#         for w in syst.systSet:
#             if not w.nominal.weightExpression in _nominal_weights:
#                 _nominal_weights.append(w.nominal.weightExpression)
#                 print 'BOOM: \t {}'.format(w.nominal.weightExpression)
    # embedding
    systSet_data = selection.get_weight('Data', True, False)
    # regular MC
    systSet_mc = selection.get_weight('VV', False, False)

    if systSet_data != None:
        print 'BOOM: \t MC and Data weights '
        _nominal_weights = []
        for w in systSet_data:
            if w in systSet_mc:
                if not w.nominal.weightExpression in _nominal_weights:
                    _nominal_weights.append(w.nominal.weightExpression)
                    print 'BOOM: \t \t {}'.format(w.nominal.weightExpression)

    print 'BOOM: \t MC nominal weights'
    _nominal_weights = []
    for w in systSet_mc:
        # check if weight also applies to data (embedding and some fake calc)
        if systSet_data != None and w in systSet_data:
            continue
        if not w.nominal.weightExpression in _nominal_weights:
            _nominal_weights.append(w.nominal.weightExpression)
            print 'BOOM: \t \t {}'.format(w.nominal.weightExpression)


    # print fake weights
    if selection.channel not in ('ee', 'mumu',):
        systSet_fake = selection.get_weight('Fake', True, False)
        systSet_fake_mc = selection.get_weight('Fake', False, False)
        print 'BOOM: \t Fake MC and Data nominal weights'
        _nominal_weights = []
        for w in systSet_fake:
            if w in systSet_fake_mc:
                if not w.nominal.weightExpression in _nominal_weights:
                    _nominal_weights.append(w.nominal.weightExpression)
                    print 'BOOM: \t \t {}'.format(w.nominal.weightExpression)

        print 'BOOM: \t Fake MC only'
        _nominal_weights = []
        for w in systSet_fake_mc:
            if not w in systSet_fake and not w in systSet_mc:
                if not w.nominal.weightExpression in _nominal_weights:
                    _nominal_weights.append(w.nominal.weightExpression)
                    print 'BOOM: \t \t {}'.format(w.nominal.weightExpression)

    print 'BOOM: \t Signal Only nominal MC weights'
    # signal
    systSet_signal = selection.get_weight('LFVggH', False, True)
    _nominal_weights = []
    for w in systSet_signal:
        if not w in systSet_mc:
            if not w.nominal.weightExpression in _nominal_weights:
                _nominal_weights.append(w.nominal.weightExpression)
                print 'BOOM: \t \t {}'.format(w.nominal.weightExpression)

    # Ztt, Zll
    print 'BOOM: \t Z+jets Only nominal MC weights'
    systSet_z = selection.get_weight('ZttQCD', False, False)
    _nominal_weights = []
    for w in systSet_z:
        if not w in systSet_mc:
            if not w.nominal.weightExpression in _nominal_weights:
                _nominal_weights.append(w.nominal.weightExpression)
                print 'BOOM: \t \t {}'.format(w.nominal.weightExpression)


    if save_to_file:
        print '```'
        sys.stdout.close()
        sys.stdout = orig_stdout
        
from .selection import selection, ALL_SELECTIONS_DICT
from cuts import CATEGORIES, YEARS

_ALL_SELECTIONS = []
# Lephad SRs and CRs
for chan in ('e1p', 'e3p', 'mu1p', 'mu3p', 'ehad', 'muhad'):
    if chan in ('e1p', 'e3p', 'ehad'):
        trig = 'single-e'
    else:
        trig = 'single-mu'

    for cat in CATEGORIES:
        # irrelevant categorise for lephad
        if cat in ('vbf_lowdr', 'qcd_fit', 'tth'):
            continue
        for y in YEARS:
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'anti_tau', y))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'SR', y, fake_region='anti_tau'))

            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'TM_SR', y, fake_region='anti_tau'))

            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'TM_same_sign_SR', y, fake_region='anti_tau'))

            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'j_fake_tau', y, fake_region='anti_tau'))

            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'j_fake_tau_same_sign', y, fake_region='anti_tau'))

            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'mc_fakes', y, fake_region='anti_tau'))

            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'mc_fakes_same_sign', y, fake_region='anti_tau'))

            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'fake_lep', y, fake_region='anti_tau'))

            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'fake_lep_same_sign', y, fake_region='anti_tau'))

            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'all_fakes', y, fake_region='anti_tau'))

            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'all_fakes_same_sign_SR', y, fake_region='anti_tau'))

            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'e_fakes', y, fake_region='anti_tau'))

            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'mu_fakes', y, fake_region='anti_tau'))
            
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'non_lep_fakes', y, fake_region='anti_tau'))

            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'qcd_lh', y, fake_region='qcd_lh_anti_tau'))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'qcd_lh_same_sign', y, fake_region='qcd_lh_same_sign_anti_tau'))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'top', y, fake_region='top_anti_tau'))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'W_lh', y, fake_region='W_lh_anti_tau'))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'W_lh_same_sign', y, fake_region='W_lh_same_sign_anti_tau'))
            # necessary for FF calculation
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'qcd_lh_anti_tau', y))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'qcd_lh_same_sign_anti_tau', y))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'qcd_lh_anti_tau_truth_lep', y))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'qcd_lh_same_sign_anti_tau_truth_lep', y))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'top_anti_tau', y))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'W_lh_anti_tau', y))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'W_lh_same_sign_anti_tau', y))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'iso_fact_lh_same_sign_anti_tau_num', y))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'iso_fact_lh_same_sign_anti_tau_den', y))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'iso_fact_lh_os_anti_tau_num', y))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'iso_fact_lh_os_anti_tau_den', y))
            # necessary for fake closure test
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'same_sign_SR', y, fake_region='same_sign_anti_tau_lh'))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'same_sign_anti_tau_lh', y))

# LepLep SRs and CRs
for chan in ('emu', 'mue'):
    for cat in CATEGORIES:
        # irrelevant categorise for lephad
        if cat in ('vbf_lowdr', 'qcd_fit', 'tth'):
            continue
        for y in YEARS:
            _ALL_SELECTIONS.append(selection(
                    chan, 'trig-OR', cat, 'SR', y, fake_region='anti_isol'))
            _ALL_SELECTIONS.append(selection(
                    chan, 'trig-OR', cat, 'anti_isol', y))
            _ALL_SELECTIONS.append(selection(
                    chan, 'trig-OR', cat, 'same_sign', y, fake_region='same_sign_anti_isol'))
            _ALL_SELECTIONS.append(selection(
                    chan, 'trig-OR', cat, 'same_sign_anti_isol', y))
            _ALL_SELECTIONS.append(selection(
                    chan, 'trig-OR', cat, 'top', y, fake_region='top_anti_isol'))
            _ALL_SELECTIONS.append(selection(
                    chan, 'trig-OR', cat, 'top_anti_isol', y))
            _ALL_SELECTIONS.append(selection(
                    chan, 'trig-OR', cat, 'same_sign_top', y))
            _ALL_SELECTIONS.append(selection(
                    chan, 'trig-OR', cat, 'same_sign_top_anti_isol', y))      
# HadHad SRs
for chan in ('1p1p', '1p3p', '3p1p', '3p3p'):
    for cat in CATEGORIES:
        for y in YEARS:
            _ALL_SELECTIONS.append(selection(
                    chan, 'di-had', cat, 'SR', y, fake_region='nos'))
            _ALL_SELECTIONS.append(selection(
                    chan, 'di-had', cat, 'nos', y, fake_region='nos'))
            _ALL_SELECTIONS.append(selection(
                    chan, 'di-had', cat, 'anti_tau', y, fake_region='nos'))
            _ALL_SELECTIONS.append(selection(
                    chan, 'di-had', cat, 'nos_anti_tau', y, fake_region='nos'))
            _ALL_SELECTIONS.append(selection(
                    chan, 'di-had', cat, 'same_sign', y, fake_region='nos'))
            _ALL_SELECTIONS.append(selection(
                    chan, 'di-had', cat, 'same_sign_anti_tau', y, fake_region='nos'))

# Zll VR
for chan in ('ee', 'mumu'):
    if chan == 'ee':
        trig = 'single-e'
    elif chan == 'mumu':
        trig = 'single-mu'

    # mimic hadhad
    for cat in CATEGORIES:
        for y in YEARS:
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'Ztt_cr_hh', y))
            _ALL_SELECTIONS.append(selection(
                    chan, trig, cat, 'Ztt_cr_corr_hh', y))

    # mimic lephad / leplep
    for cat in CATEGORIES:
        # irrelevant categorise for lephad
        if cat in ('vbf_lowdr', 'qcd_fit', 'tth'):
            continue
        for y in YEARS:
            for reg in ('Ztt_cr_inc', 'Ztt_cr_lh', 'Ztt_cr_ll', 'Ztt_cr_corr_lh', 'Ztt_cr_corr_ll',):
                _ALL_SELECTIONS.append(selection(
                        chan, trig, cat, reg, y, fake_region='anti_isol'))

# Build the selection dictionary from the ALL_SELECTION list
for sel in _ALL_SELECTIONS:
    if sel.name not in ALL_SELECTIONS_DICT.keys():
        ALL_SELECTIONS_DICT[sel.name] = sel


def get_selections(**kwargs):
    _sels = filter_selections(_ALL_SELECTIONS, **kwargs)
    return _sels


