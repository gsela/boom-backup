"""
Implementation of the Fake weights
"""
from happy.systematics import Systematics, SystematicsSet

fake_systs = [ 'lh_fake_FF_QCD_1prong_stat',
	       'lh_fake_FF_W_1prong_stat',
	       'lh_fake_RQCD_1prong_stat',
               'lh_fake_RQCD_1prong_isofac_stat',
               'lh_fake_RQCD_1prong_isofac_syst',
	       'lh_fake_FF_QCD_3prong_stat',
	       'lh_fake_FF_W_3prong_stat',
               'lh_fake_RQCD_3prong_stat',
               'lh_fake_RQCD_3prong_isofac_stat',
               'lh_fake_RQCD_3prong_isofac_syst',
               'lh_fake_closure_1prong',
               'lh_fake_closure_3prong',
             ]



def get_fake_weights(channel, category, year, region, trigger, is_data):
    """Retrieve the Fake weights

    Parameters
    __________
    channel : str
       see CHANNELS in cuts/__init__.py
    trigger : str
       see TRIGGERS in cuts/__init__.py
    year: str
       see YEARS in cuts/__init__.py
    category : str
       see CATEGORIES in cuts/__init__.py
    region: str or list(str) 
       see REGIONS in cuts/__init__.py
    is_data : bool
       switch between data and MC

    Returns
    _______
    sys_set : HAPPy SystematicsSet
       set of weights to normalise the Fake template

    Raises
    ______
    NotImplementedError
       If there is no weight for a given combination of input parameters
    """

    sys_set = SystematicsSet()
    # hadhad
    if channel in ('1p1p', '1p3p', '3p1p', '3p3p'):
        # change for proper rQCD
        #val = 0.518737316132 #+/- 0.0704439544647
        val = 0.61549 #+/- 0.0135231221016
        # val = 1.
        w = Systematics.weightSystematics(
            'fake', 'fake', str(val), str(val), str(val))
        sys_set.add(w)
  
        from .hhfake import hh_fakesyst_expr

        if 'preselection' in category: 
            cat = 'preselection'
        elif 'boost' in category:
            cat = 'boost'
        elif 'vh' in category:
            cat = 'vh'
        else :
            cat = 'vbf'

        hh_fakesyst = Systematics.weightSystematics('hh_fake_extrapolation', 'hh_fake_extrapolation',
                                                    '('+hh_fakesyst_expr(cat)+')',  
                                                    '(2-'+hh_fakesyst_expr(cat)+')',
                                                    '(1)')
        sys_set.add(hh_fakesyst)
    
    #lephad
    elif channel in ('e1p', 'mu1p', 'e3p', 'mu3p', 'ehad', 'muhad'):
        from .fakefactors import ff_expr, closure_expr

        _ff_expr_nom  = ff_expr(category, region, 'nominal')
        for syst_name in fake_systs:
            if 'closure' in syst_name :
                clos_weight = closure_expr(category, region, syst_name)
                w = Systematics.weightSystematics(
                    syst_name, 
                    syst_name,
                    '('+clos_weight+')',
                    '(2-'+clos_weight+')',
                    '(1)')                    
            else:           
                _ff_expr_syst = ff_expr(category, region, syst_name)
 
                w = Systematics.weightSystematics(
                    syst_name,
                    syst_name,
                    _ff_expr_syst,
                    _ff_expr_syst,
                    _ff_expr_nom)
                
            #sys_set.add(w)

    # leplep 
    elif channel in ('emu', 'mue'):
        if channel == 'mue':
            if 'top' in region:
               _val = 0.549980
               _err = 0.039777 
            else:
               _val = 0.809522 
               _err = 0.086002 
        else:
            if 'top' in region:
               _val = 0.314697
               _err = 0.017285
            else:
               _val = 0.527135
               _err = 0.029125
 
        w = Systematics.weightSystematics(
            'fake', 
            'fake', 
            str(_val + _err),
            str(_val - _err),
            str(_val))
        sys_set.add(w)
    
    else:
        raise NotImplementedError

    if not is_data:
        substraction = Systematics.weightSystematics(
            '-1', '-1', '-1', '-1', '-1')
        sys_set.add(substraction)

    return sys_set
        
