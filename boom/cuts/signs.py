"""
cuts on the charge products of the two leptons
"""

from . import CHANNELS, YEARS, CATEGORIES, REGIONS, SYSTEMATICS
from .base import DecoratedCut, CUTBOOK

CutOS = DecoratedCut(
    'OS', 'OS', 
    'ditau_qxq == -1',
    regions=filter(lambda t: not t in ('j_fake_tau_same_sign',
                                       'fake_lep_same_sign',
                                       'mc_fakes_same_sign',
                                       'same_sign_SR',
                                       'TM_same_sign_SR',
                                       'same_sign', 
                                       'same_sign_anti_isol', 
                                       'same_sign_top',
                                       'same_sign_top_anti_isol', 
                                       'same_sign_anti_tau', 
                                       'same_sign_anti_tau_lh',
                                       'nos', 
                                       'nos_anti_tau', 
                                       'W_lh_same_sign',
                                       'W_lh_same_sign_anti_tau',
                                       'qcd_lh_same_sign',
                                       'qcd_lh_same_sign_anti_tau',
                                       'iso_fact_lh_same_sign_anti_tau_num', 
                                       'iso_fact_lh_same_sign_anti_tau_den',
                                       'all_fakes_same_sign_SR'), REGIONS))
CUTBOOK.append(CutOS)

CutSS = DecoratedCut(
    'SS', 'SS', 
    'ditau_qxq == 1', 
    regions=['j_fake_tau_same_sign',
             'mc_fakes_same_sign',
             'fake_lep_same_sign',
             'same_sign_SR',
             'TM_same_sign_SR',
             'same_sign', 
             'same_sign_top',
             'same_sign_anti_isol',
             'same_sign_top_anti_isol', 
             'same_sign_anti_tau', 
             'same_sign_anti_tau_lh',
             'W_lh_same_sign',
             'W_lh_same_sign_anti_tau',
             'qcd_lh_same_sign',
             'qcd_lh_same_sign_anti_tau',
             'iso_fact_lh_same_sign_anti_tau_num', 
             'iso_fact_lh_same_sign_anti_tau_den',
             'all_fakes_same_sign_SR'])
CUTBOOK.append(CutSS)

CutnOS = DecoratedCut(
    'nOS', 'nOS', 
    '!(tau_0_q*tau_1_q==-1)', 
    regions = ['nos', 'nos_anti_tau'])
CUTBOOK.append(CutnOS)
