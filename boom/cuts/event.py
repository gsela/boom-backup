from .base import DecoratedCut, CUTBOOK
import gconfig

if gconfig.UseFilteredTree==0:
    CutCleaning_mc = DecoratedCut(
    'All (after cleaning)', 
    'event_is_bad_batman == 0 && NOMINAL_pileup_random_run_number > 0',
    'event_is_bad_batman == 0 && NOMINAL_pileup_random_run_number > 0',
        mc_only=True)
    CUTBOOK.append(CutCleaning_mc)

    CutCleaning_data = DecoratedCut(
    'All (after cleaning)', 
    'event_is_bad_batman == 0 && run_number > 0',
    'event_is_bad_batman == 0 && run_number > 0',
        data_only=True)
    CUTBOOK.append(CutCleaning_data)

