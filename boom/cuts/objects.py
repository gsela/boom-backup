"""
Object-level cuts (counting, pt, eta, ID, isolation)
"""
from . import CHANNELS, YEARS, CATEGORIES, REGIONS, SYSTEMATICS
from .base import DecoratedCut, CUTBOOK
import gconfig

# mu had channel
CutCountMUTAU = DecoratedCut(
    'Channel',
    'n_electrons == 0 && n_muons == 1',
    'n_electrons == 0 && n_muons == 1',
    channels =['mu1p', 'mu3p', 'muhad'])
CUTBOOK.append(CutCountMUTAU)

CutCountETAU = DecoratedCut(
    'Channel',
    'n_electrons == 1 && n_muons == 0',
    'n_electrons == 1 && n_muons == 0',
    channels =['e1p', 'e3p', 'ehad'])
CUTBOOK.append(CutCountETAU)

TruthTau = DecoratedCut(
    'Hadronic Tau (MC Only)',
    '|tau_0_matched_pdgId| == 15',
    '(mc_channel_number==345124 || mc_channel_number==345125 || mc_channel_number==346194 || mc_channel_number==346195 || mc_channel_number==345213 || mc_channel_number==345214 || mc_channel_number==345215 || mc_channel_number==345216 || mc_channel_number==345218 || mc_channel_number==345219) ? (abs(tau_0_matched_pdgId) == 15) : 1',
    channels=['mu1p','mu3p', 'e1p', 'e3p', 'muhad', 'ehad'],
    mc_only=True)
CUTBOOK.append(TruthTau)

# CutBVeto = DecoratedCut(
#     'b-jet Veto',
#     'n_bjets_DL1r_FixedCutBEff_85 == 0',
#     'n_bjets_DL1r_FixedCutBEff_85 == 0',
#     channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
# CUTBOOK.append(CutBVeto)

# CutOS = DecoratedCut(
#     'Opposite Sign',
#     'ditau_qxq == -1', 
#     'ditau_qxq == -1',
#     channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
# CUTBOOK.append(CutOS)

CutTauOneProng = DecoratedCut(
    '1-prong tau', 
    'tau_0_n_charged_tracks == 1',
    'tau_0_n_charged_tracks == 1',
    channels=['mu1p', 'e1p'])
CUTBOOK.append(CutTauOneProng)

CutTauThreeProng = DecoratedCut(
    '3-prong tau', 
    'tau_0_n_charged_tracks == 3',
    'tau_0_n_charged_tracks == 3',
    channels=['mu3p', 'e3p'])
CUTBOOK.append(CutTauThreeProng)

if gconfig.UseFilteredTree==0:
    CutTauQuality = DecoratedCut(
        'Tau Quality',
        'n_taus > 0 && |tau_0_q| == 1 && (tau_0_n_charged_tracks == 1 || tau_0_n_charged_tracks == 3) && |tau_0_p4.Eta()| < 2.4 && !(|tau_0_p4.Eta()|>1.37 && |tau_0_p4.Eta()|<1.52) && tau_0_p4.Pt() > 25.0',
        'n_taus > 0 && abs(tau_0_q) == 1 && (tau_0_n_charged_tracks == 1 || tau_0_n_charged_tracks == 3) && abs(tau_0_p4.Eta()) < 2.4 && !(abs(tau_0_p4.Eta())>1.37 && abs(tau_0_p4.Eta())<1.52) && tau_0_p4.Pt() > 25.0',
        channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
    CUTBOOK.append(CutTauQuality)

    Cut_npv = DecoratedCut(
        'Primary Vertex Existence', 
        'n_pvx > 0',
        'n_pvx > 0',
        channels =['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
    CUTBOOK.append(Cut_npv)




# # Object counting
# CutCountEMU = DecoratedCut(
#     'count_emu', '1 el, 1 mu, 0 taus',
#     'n_electrons == 1 && n_muons == 1 && n_taus == 0',
#     channels=['emu', 'mue'])
# #CUTBOOK.append(CutCountEMU)

# CutEMU = DecoratedCut(
#     'lead_e_sublead_mu', 'pt(e) > pt(mu)',
#     'tau_0 == 2 && tau_1 == 1',
#     channels=['emu'])
# #CUTBOOK.append(CutEMU)

# CutMUE = DecoratedCut(
#     'lead_mu_sublead_e', 'pt(mu) > pt(e)',
#     'tau_0 == 1 && tau_1 == 2',
#     channels=['mue'])
# #CUTBOOK.append(CutMUE)

# CutCountEE = DecoratedCut(
#     'count_ee', '2 el, 0 mu, 0 taus',
#     'n_electrons == 2 && n_muons == 0 && n_taus == 0',
#     channels=['ee'])
# #CUTBOOK.append(CutCountEE)

# CutCountMM = DecoratedCut(
#     'count_mm', '0 el, 2 mu, 0 taus',
#     'n_electrons == 0 && n_muons == 2 && n_taus == 0',
#     channels=['mumu'])
# #CUTBOOK.append(CutCountMM)

# CutCountETAU = DecoratedCut(
#     'count_etau', '1 el, 0 muon, 1 tau',
#     'n_electrons == 1 && n_muons == 0 && n_taus == 1',
#     channels =['e1p', 'e3p'])
# #CUTBOOK.append(CutCountETAU)

# CutCountTAUTAU = DecoratedCut(
#     'count_tautau', '0 el, 0 muon, 2 taus',
#     'n_electrons == 0 && n_muons == 0 && n_taus == 2',
#     channels =['1p1p', '1p3p', '3p1p', '3p3p'])
# #CUTBOOK.append(CutCountTAUTAU)

####
# Object level cuts
####

# CutTauID_antitau_LH = DecoratedCut(
#     'tau_not_medium_id',
#     'tau is not medium',
#     'tau_0_jet_rnn_medium == 0 && n_taus_rnn_medium == 0 && tau_0_jet_rnn_score_trans > 0.01',
#     channels=['e1p', 'e3p', 'mu1p', 'mu3p'],
#     regions=filter(lambda t: 'anti_tau' in t, REGIONS))
# #CUTBOOK.append(CutTauID_antitau_LH)


# # lephad lepton cuts
# CutElPt_lh_15 = DecoratedCut(
#     'el_pt_lh_15', 'pt(e) > 25',
#     'tau_1_p4.Pt() > 25',
#     channels=['e1p', 'e3p'],
#     years=['15'])
# #CUTBOOK.append(CutElPt_lh_15)

# CutElPt_lh = DecoratedCut(
#     'el_pt_lh', 'pt(e) > 27',
#     'tau_1_p4.Pt() > 27',
#     channels=['e1p', 'e3p'],
#     years=['16', '17', '18'])
# #CUTBOOK.append(CutElPt_lh)

# had-had pt cuts
# CutTauPt_hh = DecoratedCut(
#     'tau_pt_hh', 
#     'pt(tau_0) > 40 and pt(tau_1) > 30',
#     'tau_0_p4.Pt() > 40 && tau_1_p4.Pt() > 30',
#     channels=['1p1p', '1p3p', '3p1p', '3p3p'])
# #CUTBOOK.append(CutTauPt_hh)

# # ID and isolation cuts
# CutLeadLepID = DecoratedCut(
#     'lead_lepton_id', 
#     'lead lepton ID',  
#     'tau_0_id_medium == 1',
#     channels=['emu', 'mue'])
# #CUTBOOK.append(CutLeadLepID)

# CutLeadLepIsoElec = DecoratedCut(
#     'lead_lepton_iso', 
#     'Lead lepton isolation',  
#     'tau_0_iso_FCLoose == 1',
#     channels=['emu','ee'])
# #CUTBOOK.append(CutLeadLepIsoElec)

# CutLeadLepIsoMuon = DecoratedCut(
#     'lead_lepton_iso',
#     'Lead lepton isolation',
#     'tau_0_iso_FCTightTrackOnly == 1',
#     channels=['mue','mumu'])
# #CUTBOOK.append(CutLeadLepIsoMuon)

#CutSubLeadLepID = DecoratedCut(
#    'sublead_lepton_id', 
#    'sublead lepton ID',
#    'tau_1_id_medium == 1',
#    channels=['e1p', 'e3p', 'mu1p', 'mu3p', 'emu', 'mue'],
#    regions=filter(lambda t: t != 'anti_id', REGIONS))
#CUTBOOK.append(CutSubLeadLepID)


# CutLepAntiIsoElec = DecoratedCut(
#     'anti_iso', 'Not Isolated',  
# #    'tau_1_iso_FCLoose != 1',
#     'tau_1 == 2 && tau_1_iso_Gradient != 1',
#     channels=['e1p', 'e3p', 'mue'],
#     regions=['qcd_lh', 
#              'qcd_lh_same_sign',
#              'qcd_lh_anti_tau', 
#              'qcd_lh_same_sign_anti_tau',
#              'qcd_lh_anti_tau_truth_lep', 
#              'qcd_lh_same_sign_anti_tau_truth_lep',
#              'anti_isol', 
#              'top_anti_isol',
#              'same_sign_anti_isol', 
#              'same_sign_top_anti_isol', 
#              'iso_fact_lh_same_sign_anti_tau_den',
#              'iso_fact_lh_os_anti_tau_den'])
# #CUTBOOK.append(CutLepAntiIsoElec)

# CutLepAntiIsoMuon = DecoratedCut(
#     'anti_iso', 'Not Isolated',
# #    'tau_1_iso_FCTightTrackOnly != 1',
#     'tau_1 == 1 && tau_1_iso_FCTight_FixedRad != 1',
#     channels=['mu1p', 'mu3p', 'emu'],
#     regions=['qcd_lh',
#              'qcd_lh_same_sign', 
#              'qcd_lh_anti_tau', 
#              'qcd_lh_same_sign_anti_tau',
#              'qcd_lh_anti_tau_truth_lep', 
#              'qcd_lh_same_sign_anti_tau_truth_lep',
#              'anti_isol', 
#              'top_anti_isol',
#              'same_sign_anti_isol', 
#              'same_sign_top_anti_isol', 
#              'iso_fact_lh_same_sign_anti_tau_den',
#              'iso_fact_lh_os_anti_tau_den'])
# #CUTBOOK.append(CutLepAntiIsoMuon)

# CutTauCharge_HH = DecoratedCut(
#     'tau-charge', 
#     '|q(tau_0)| == 1 and |q(tau_1)| == 1',
#     'abs(tau_0_q) == 1 && abs(tau_1_q) == 1',
#     channels=['1p1p', '1p3p', '3p1p', '3p3p'],
#     regions=filter(lambda t: t not in ('nos', 'nos_anti_tau'), REGIONS))
# #CUTBOOK.append(CutTauCharge_HH)

# CutTauIDMedium = DecoratedCut(
#     'tau-id-medium', 'medium-mediun',
#     'tau_0_jet_rnn_medium == 1 && tau_1_jet_rnn_medium == 1',
#     channels=['1p1p', '1p3p', '3p1p', '3p3p'],
#     regions=filter(lambda t: not t in ('anti_tau', 'same_sign_anti_tau', 'nos_anti_tau'), REGIONS))
# #CUTBOOK.append(CutTauIDMedium)

# CutTauIDAntiMedium = DecoratedCut(
#     'tau-id-anti-medium', 'not medium - not medium',
#     'tau_0_jet_rnn_medium != 1 && tau_1_jet_rnn_medium != 1',
#     channels=['1p1p', '1p3p', '3p1p', '3p3p'],
#     regions=filter(lambda t: t in ('anti_tau', 'same_sign_anti_tau', 'nos_anti_tau'), REGIONS))
# #CUTBOOK.append(CutTauIDAntiMedium)


# # eta, ID and isolation cuts
# CutCrackLead = DecoratedCut(
#     'eta_crack_lead',
#     'Leading tau not in crack',
#     'tau_0_p4.Eta() < 1.37 || tau_0_p4.Eta() > 1.52',
#     regions=['Ztt_cr_corr_hh', 'Ztt_cr_corr_lh', 'Ztt_cr_corr_ll'])
# CUTBOOK.append(CutCrackLead)

# CutCrackSubLead = DecoratedCut(
#     'eta_crack_sublead',
#     'Subleading tau not in crack',
#     'tau_1_p4.Eta() < 1.37 || tau_1_p4.Eta() > 1.52',
#     regions=['Ztt_cr_corr_hh', 'Ztt_cr_corr_lh', 'Ztt_cr_corr_ll'])
# CUTBOOK.append(CutCrackSubLead)


# # for building nOS, we had 2p taus to 1p bin
# CutLeadTau1P2P = DecoratedCut(
#     'lead_1p_2p', 'lead_1p_2p', 
#     'tau_0_n_charged_tracks == 1 || tau_0_n_charged_tracks == 2',
#     channels=['1p1p', '1p3p'],
#     regions=filter(lambda t: t in ('nos', 'nos_anti_tau'), REGIONS))
# #CUTBOOK.append(CutLeadTau1P2P)

# # for building nOS, we add 2p taus to 1p bin
# CutSubLeadTau1P = DecoratedCut(
#     'sublead_1p', 'sublead 1p', 
#     'tau_1_n_charged_tracks == 1',
#     channels=['1p1p', '3p1p'],
#     regions=filter(lambda t: t not in ('nos', 'nos_anti_tau'), REGIONS))
# #CUTBOOK.append(CutSubLeadTau1P)

# CutSubLeadTau3P = DecoratedCut(
#     'sublead_3p', 'sublead 3p', 
#     'tau_1_n_charged_tracks == 3',
#     channels=['1p3p', '3p3p'])
# #CUTBOOK.append(CutSubLeadTau3P)

# CutSubLeadTau1P2P = DecoratedCut(
#     'sublead_1p_2p', 'sublead_1p_2p', 
#     'tau_1_n_charged_tracks == 1 || tau_1_n_charged_tracks == 2',
#     channels=['1p1p', '3p1p'],
#     regions=filter(lambda t: t in ('nos', 'nos_anti_tau'), REGIONS))
# #CUTBOOK.append(CutSubLeadTau1P2P)

# CutTauVeto = DecoratedCut(
#     'hadronic_tau_veto',
#     'no medium taus',
#     'n_taus == 0',
#     channels=['ee', 'mumu', 'emu', 'mue'])
# #CUTBOOK.append(CutTauVeto)

# CutEveto = DecoratedCut(
#     'tau_eveto', 'Tau electron veto',
#     'tau_0_ele_bdt_score_trans_retuned > 0.15',
#     channels=['e1p'])
# #CUTBOOK.append(CutEveto)

# CutEvetoLeading = DecoratedCut(
#     'tau_eveto', 'Tau electron veto',
#     'tau_0_ele_bdt_score_trans_retuned > 0.05',
#     channels=['1p1p', '1p3p'],
#     categories=['tth','tth_0','tth_1'])
# #CUTBOOK.append(CutEvetoLeading)

# CutEvetoSubLeading = DecoratedCut(
#     'tau_eveto', 'Tau electron veto',
#     'tau_1_ele_bdt_score_trans_retuned > 0.05',
#     channels=['1p1p', '3p1p'])
# #CUTBOOK.append(CutEvetoSubLeading)

# CutBJets = DecoratedCut(
#     'BJets', 'BJets', 
#     'n_bjets_DL1r_FixedCutBEff_85 > 0',
#     channels=['ee', 'mumu', 'emu', 'mue', 'e1p', 'e3p', 'mu1p', 'mu3p'],
#     regions=['top','top_anti_tau','top_anti_iso','same_sign_top','same_sign_top_anti_isol'])
# #CUTBOOK.append(CutBJets)