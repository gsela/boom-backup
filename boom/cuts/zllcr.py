import ROOT
import os
from ..zll_pt import tau_0_pt_corr, tau_1_pt_corr, met_corr, _random_offset
from ..zll_pt import x0_corr, x1_corr, dr_corr, deta_corr, mT_corr, ditau_m_corr, lepton_pt_cuts_15_lh, lepton_pt_cuts_lh, lepton_pt_cuts_ll, eta_crack_cuts

from .base import DecoratedCut, CUTBOOK


Cut_zllcr_ll = DecoratedCut(
    'split_events_zllcr_ll',
    'split_events_zllcr_ll',
    'Rnd::zll_split_cut(event_number, 0, 0.33) == 1',
    regions=('Ztt_cr_ll', 'Ztt_cr_corr_ll',))
CUTBOOK.append(Cut_zllcr_ll)

Cut_zllcr_lh = DecoratedCut(
    'split_events_zllcr_lh',
    'split_events_zllcr_lh',
    'Rnd::zll_split_cut(event_number, 0.33, 0.66) == 1',
    regions=('Ztt_cr_lh', 'Ztt_cr_corr_lh',))
CUTBOOK.append(Cut_zllcr_lh)

Cut_zllcr_hh = DecoratedCut(
    'split_events_zllcr_hh',
    'split_events_zllcr_hh',
    'Rnd::zll_split_cut(event_number, 0.66, 1) == 1',
    regions=('Ztt_cr_hh', 'Ztt_cr_corr_hh',))
CUTBOOK.append(Cut_zllcr_hh)

# eta crack cut for electron and hadronic taus
Cut_zllcr_eta_crack = DecoratedCut(
    'cut_zllcr_eta_crack',
    'cut_zllcr_eta_crack',
    '{} == 1'.format(eta_crack_cuts),
    regions=('Ztt_cr_corr_hh', 'Ztt_cr_corr_lh', 'Ztt_cr_corr_ll',))
CUTBOOK.append(Cut_zllcr_eta_crack)

# HAD-HAD Cuts

Cut_zllcr_pt_0_hh = DecoratedCut(
    'cut_zllcr_pt_0_hh',
    'cut_zllcr_pt_0_hh',
    '{} > 40'.format(tau_0_pt_corr),
     regions='Ztt_cr_corr_hh')
CUTBOOK.append(Cut_zllcr_pt_0_hh)

Cut_zllcr_pt_1_hh = DecoratedCut(
    'cut_zllcr_pt_1_hh',
    'cut_zllcr_pt_1_hh',
    '{} > 30'.format(tau_1_pt_corr),
     regions='Ztt_cr_corr_hh')
CUTBOOK.append(Cut_zllcr_pt_1_hh)


Cut_zllcr_met = DecoratedCut(
    'cut_zllcr_met',
    'cut_zllcr_met',
    '{} > 20'.format(met_corr),
     regions=('Ztt_cr_corr_hh', 'Ztt_cr_corr_lh', 'Ztt_cr_corr_ll',))
CUTBOOK.append(Cut_zllcr_met)

Cut_zllcr_x0_hh = DecoratedCut(
    'cut_zll_cr_x0_hh',
    'cut_zll_cr_x0_hh',
    '{} < 1.4'.format(x0_corr),
    regions='Ztt_cr_corr_hh')
CUTBOOK.append(Cut_zllcr_x0_hh)

Cut_zllcr_x1_hh = DecoratedCut(
    'cut_zll_cr_x1_hh',
    'cut_zll_cr_x1_hh',
    '{} < 1.4'.format(x1_corr),
    regions='Ztt_cr_corr_hh')
CUTBOOK.append(Cut_zllcr_x1_hh)


Cut_zllcr_x0_hh_low = DecoratedCut(
    'cut_zll_cr_x0_hh_low',
    'cut_zll_cr_x0_hh_low',
    '{} > 0.1'.format(x0_corr),
    regions='Ztt_cr_corr_hh')
CUTBOOK.append(Cut_zllcr_x0_hh_low)

Cut_zllcr_x1_hh_low = DecoratedCut(
    'cut_zll_cr_x1_hh_low',
    'cut_zll_cr_x1_hh_low',
    '{} > 0.1'.format(x1_corr),
    regions='Ztt_cr_corr_hh')
CUTBOOK.append(Cut_zllcr_x1_hh_low)

CutDiTauDr_zll_vr_hh_low = DecoratedCut(
    'ditau_dr_zll_vr_hh_low', 
    '0.6 < DR(t_h, t_h)',
    '{} > 0.6'.format(dr_corr),
    regions='Ztt_cr_corr_hh')
CUTBOOK.append(CutDiTauDr_zll_vr_hh_low)

CutDiTauDr_zll_vr_hh_high = DecoratedCut(
    'ditau_dr_zll_vr_hh_high', 
    'DR(t_vis, t_vis) < 2.5',
    '{} < 2.5'.format(dr_corr),
    regions=('Ztt_cr_corr_hh', 'Ztt_cr_corr_lh',))
CUTBOOK.append(CutDiTauDr_zll_vr_hh_high)

CutDiTauDeta_zll_vr = DecoratedCut(
    'ditau_deta_zll_vr', 
    '|deta(t_vis, t_vis)| < 1.5',
    'fabs({}) < 1.5'.format(deta_corr),
    regions=('Ztt_cr_corr_lh', 'Ztt_cr_corr_hh', 'Ztt_cr_corr_ll',))
CUTBOOK.append(CutDiTauDeta_zll_vr)


# LEP-HAD Cuts

Cut_zllcr_pt_0_lh = DecoratedCut(
    'cut_zllcr_pt_0_lh',
    'cut_zllcr_pt_0_lh',
    '{} > 30'.format(tau_0_pt_corr),
     regions='Ztt_cr_corr_lh')
CUTBOOK.append(Cut_zllcr_pt_0_lh)

Cut_zllcr_pt_1_15_lh = DecoratedCut(
    'cut_zllcr_pt_1_15_lh',
    'cut_zllcr_pt_1_15_lh',
    '{} == 1'.format(lepton_pt_cuts_15_lh),  
    regions='Ztt_cr_corr_lh',
    years=['15'])
CUTBOOK.append(Cut_zllcr_pt_1_15_lh)

Cut_zllcr_pt_1_lh = DecoratedCut(
    'cut_zllcr_pt_1_lh',
    'cut_zllcr_pt_1_lh',
    '{} == 1'.format(lepton_pt_cuts_lh),  
    regions='Ztt_cr_corr_lh',
    years=['16','17','18'])
CUTBOOK.append(Cut_zllcr_pt_1_lh)

Cut_zllcr_x0_lh = DecoratedCut(
    'cut_zll_cr_x0_lh',
    'cut_zll_cr_x0_lh',
    '{} < 1.4'.format(x0_corr),
    regions='Ztt_cr_corr_lh')
CUTBOOK.append(Cut_zllcr_x0_lh)

Cut_zllcr_x1_lh = DecoratedCut(
    'cut_zll_cr_x1_lh',
    'cut_zll_cr_x1_lh',
    '{} < 1.2'.format(x1_corr),
    regions='Ztt_cr_corr_lh')
CUTBOOK.append(Cut_zllcr_x1_lh)

Cut_zllcr_x0_lh_low = DecoratedCut(
    'cut_zll_cr_x0_lh_low',
    'cut_zll_cr_x0_lh_low',
    '{} > 0.1'.format(x0_corr),
    regions='Ztt_cr_corr_lh')
CUTBOOK.append(Cut_zllcr_x0_lh_low)

Cut_zllcr_x1_lh_low = DecoratedCut(
    'cut_zll_cr_x1_lh_low',
    'cut_zll_cr_x1_lh_low',
    '{} > 0.1'.format(x1_corr),
    regions='Ztt_cr_corr_lh')
CUTBOOK.append(Cut_zllcr_x1_lh_low)

CutDiTauDr_zll_vr_lh_low = DecoratedCut(
    'ditau_dr_zll_vr_lh_low', 
    '0.4 < DR(t_h, t_l)',
    '{} > 0.4'.format(dr_corr),
    regions='Ztt_cr_corr_lh')
CUTBOOK.append(CutDiTauDr_zll_vr_lh_low)

CutDiTauDr_zll_vr_lh = DecoratedCut(
    'ditau_dr_zll_vr_lh', 
    'DR(t_l, t_l) < 2.5',
    '{} < 2.5'.format(dr_corr),
    regions='Ztt_cr_corr_lh')
CUTBOOK.append(CutDiTauDr_zll_vr_lh)

# Unique tau-jet OLR for Lep-Had
CutDiTauDrjt_zll_vr_lh = DecoratedCut(
    'ditau_drjt_zll_vr_lh',
    '0.6 < DR(t_h, j_0)',
    'PtCorrHelper::jet_tau_0_dr_corr(jet_0_p4.Pt(), jet_0_p4.Eta(), jet_0_p4.Phi(), tau_0_p4.Pt(), tau_0_p4.Eta(), tau_0_p4.Phi(), tau_1_p4.Pt(), tau_1_p4.Eta(), tau_1_p4.Phi(), event_number + 666, PtCorrHelper::zll_choose_channel(event_number)) > 0.6',
    regions='Ztt_cr_corr_lh')
CUTBOOK.append(CutDiTauDrjt_zll_vr_lh)

Cut_zllcr_mt = DecoratedCut(
    'ditau_mt_zll_vr_lh',
    'MT(l,met) < 70 GeV',
    '{} < 70'.format(mT_corr),
    regions='Ztt_cr_corr_lh')
CUTBOOK.append(Cut_zllcr_mt)


# LEP-LEP Cuts
Cut_lepton_pt_zcr_ll = DecoratedCut(
    'lepton_pt_zcr_ll',
    'lepton pt cuts',
    '{} == 1'.format(lepton_pt_cuts_ll),
    regions='Ztt_cr_corr_ll')
CUTBOOK.append(Cut_lepton_pt_zcr_ll)

Cut_visible_mass_low_zcr_ll = DecoratedCut(
    'visible_mass_low_zcr_ll',
    'm_vis corrected > 30',
    '{} > 30'.format(ditau_m_corr),
    regions='Ztt_cr_corr_ll')
CUTBOOK.append(Cut_visible_mass_low_zcr_ll)    

Cut_visible_mass_high_zcr_ll = DecoratedCut(
    'visible_mass_high_zcr_ll',
    'm_vis corrected < 100',
    '{} < 100'.format(ditau_m_corr),
    regions='Ztt_cr_corr_ll')
CUTBOOK.append(Cut_visible_mass_high_zcr_ll)    

CutDiTauDr_zll_vr_ll = DecoratedCut(
    'ditau_dr_zll_vr_ll', 
    'DR(t_l, t_l) < 2.0',
    '{} < 2.0'.format(dr_corr),
    regions='Ztt_cr_corr_ll')
CUTBOOK.append(CutDiTauDr_zll_vr_ll)

Cut_zllcr_x0_ll = DecoratedCut(
    'cut_zll_cr_x0_ll',
    'cut_zll_cr_x0_ll',
    '{} < 1.0'.format(x0_corr),
    regions='Ztt_cr_corr_ll')
CUTBOOK.append(Cut_zllcr_x0_ll)

Cut_zllcr_x1_ll = DecoratedCut(
    'cut_zll_cr_x1_ll',
    'cut_zll_cr_x1_ll',
    '{} < 1.0'.format(x1_corr),
    regions='Ztt_cr_corr_ll')
CUTBOOK.append(Cut_zllcr_x1_ll)


Cut_zllcr_x0_ll_low = DecoratedCut(
    'cut_zll_cr_x0_ll_low',
    'cut_zll_cr_x0_ll_low',
    '{} > 0.1'.format(x0_corr),
    regions='Ztt_cr_corr_ll')
CUTBOOK.append(Cut_zllcr_x0_ll_low)

Cut_zllcr_x1_ll_low = DecoratedCut(
    'cut_zll_cr_x1_ll_low',
    'cut_zll_cr_x1_ll_low',
    '{} > 0.1'.format(x1_corr),
    regions='Ztt_cr_corr_ll')
CUTBOOK.append(Cut_zllcr_x1_ll_low)
