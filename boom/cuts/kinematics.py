"""
Event kinematic cuts
"""

from . import CHANNELS, YEARS, CATEGORIES, REGIONS, SYSTEMATICS
from .base import DecoratedCut, CUTBOOK
import gconfig

if gconfig.UseFilteredTree==0:
    Cut_truth_passedVBFFilter  = DecoratedCut(
        'ZllVBF filter (MC Only)',
        'truth_passedVBFFilter ',
        'truth_passedVBFFilter == 0',
        mc_only=True)
    CUTBOOK.append(Cut_truth_passedVBFFilter)


    CutEveto = DecoratedCut(
        'tau_eveto', 'Tau electron veto',
        '(n_electrons == 1 && tau_0_n_charged_tracks == 1) ? tau_0_ele_bdt_score_trans_retuned > 0.15 : 1',
        channels=['e1p', 'ehad'])
    CUTBOOK.append(CutEveto)


    CutDiTauAngle_lh = DecoratedCut(
        'Angle cut', 
        '|deta(l, t_h)|< 2.0',
        'ditau_deta < 2.0',
        channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
    CUTBOOK.append(CutDiTauAngle_lh)

    CutLightLeptonQuality = DecoratedCut(
        'Light Lepton Quality', 
        'tau_1_id_medium == 1 && ((tau_1 == 1 && tau_1_iso_FCTight_FixedRad == 1 ) || (tau_1 == 2 && tau_1_iso_Gradient == 1 ))',
        'tau_1_id_medium == 1 && ((tau_1 == 1 && tau_1_iso_FCTight_FixedRad == 1 ) || (tau_1 == 2 && tau_1_iso_Gradient == 1 ))',
        channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'],
        regions=filter(lambda t: not t in ('fake_lep','fake_lep_same_sign'), REGIONS))
    CUTBOOK.append(CutLightLeptonQuality)
    
    # # # mass cuts
    # CutTransverseMass = DecoratedCut(
    #     'transverse_mass', 
    #     'm_T < 70',
    #     'ditau_mt_lep1_met < 70',
    #     channels=['e1p', 'e3p', 'mu1p', 'mu3p'],
    #     regions=filter(lambda t: t not in ('W_lh', 'W_lh_same_sign', 'W_lh_anti_tau', 'W_lh_same_sign_anti_tau', 'top', 'top_anti_tau'), REGIONS))
    # #CUTBOOK.append(CutTransverseMass)

    # CutTransverseMass_TopCR = DecoratedCut(
    #     'transverse_mass_top', 
    #     'm_T > 40',
    #     'ditau_mt_lep1_met > 40',
    #     channels=['e1p', 'e3p', 'mu1p', 'mu3p'],
    #     regions=filter(lambda t: t in ('top', 'top_anti_tau'), REGIONS))
    # #CUTBOOK.append(CutTransverseMass_TopCR )

    # CutTransverseMass_WCR = DecoratedCut(
    #     'transverse_mass_w_cr', 
    #     'm_T > 70',
    #     'ditau_mt_lep1_met > 70',
    #     channels=['e1p', 'e3p', 'mu1p', 'mu3p'],
    #     regions=filter(lambda t: t in ('W_lh', 'W_lh_same_sign', 'W_lh_anti_tau', 'W_lh_same_sign_anti_tau'), REGIONS))
    #CUTBOOK.append(CutTransverseMass_WCR )

    # invariant mass cut 
    # CutInvariantMass = DecoratedCut(
    #     'inv_mass_emu', 
    #     '30 < m(emu) < 100',
    #     'ditau_p4.M() > 30 && ditau_p4.M() < 100',
    #     channels=['emu', 'mue'])
    #CUTBOOK.append(CutInvariantMass)





    # # invariant mass cut zll vr
    # CutInvariantMass_zll_vr = DecoratedCut(
    #     'inv_mass_ztt_cr', 
    #     'm(ll) > 80',
    #     'ditau_p4.M() > 80',
    #     channels=['ee', 'mumu'],
    #     regions=filter(lambda t: t.startswith('Ztt_cr'),  REGIONS))
    #CUTBOOK.append(CutInvariantMass_zll_vr)

    ### 
    # Lead jet pt cut
    # Cut_njet = DecoratedCut(
    #     'at_least_one_jet',
    #     'n_jets > 0',
    #     'n_jets > 0')
    #CUTBOOK.append(Cut_njet)

    # CutJetPt = DecoratedCut(
    #     'lead_jet', 
    #     'p_T(leading jet) > 40',
    #     'jet_0_p4.Pt() >= 40',
    #     channels=['ee', 'mumu', 'emu', 'mue', 'e1p', 'e3p', 'mu1p', 'mu3p'])
    #CUTBOOK.append(CutJetPt)

    # CutJetPt_hh = DecoratedCut(
    #     'lead_jet_hh', 
    #     'pT(leading jet) > 70 and |eta(leading jet)| < 3.2',
    #     'jet_0_p4.Pt() > 70 && fabs(jet_0_p4.Eta()) < 3.2',
    #     channels=['1p1p', '1p3p', '3p1p', '3p3p'])
    # #CUTBOOK.append(CutJetPt_hh)

    # CutJetPt_zll_vr_hh = DecoratedCut(
    #     'lead_jet_zcr_hh', 
    #     'pT(leading jet) > 70 and |eta(leading jet)| < 3.2',
    #     'jet_0_p4.Pt() > 70 && fabs(jet_0_p4.Eta()) < 3.2',
    #     regions=('Ztt_cr_hh', 'Ztt_cr_corr_hh'))
    # #CUTBOOK.append(CutJetPt_zll_vr_hh)

    # # MET Cuts
    # CutMET = DecoratedCut(
    #     'met_cut', 
    #     'met > 20',
    #     'met_p4.Pt() > 20',
    #     regions=filter(lambda t: not t.startswith('Ztt_cr'),  REGIONS))
    # #CUTBOOK.append(CutMET)

    # # X0. X1 Cuts
    # CutX0X1_low = DecoratedCut(
    #     'x0x1_low', 
    #     'lower cut on Collinear approx of the two visible leptons',
    #     'ditau_coll_approx_x0 > 0.1 && ditau_coll_approx_x1 > 0.1',
    #     regions=filter(lambda t: not t.startswith('Ztt_cr'),  REGIONS))
    # #CUTBOOK.append(CutX0X1_low)

    # CutX0X1_high_ll = DecoratedCut(
    #     'x0x1_ll', 
    #     'higher cut of Collinear approx of the two visible leptons',
    #     'ditau_coll_approx_x0 < 1.0 && ditau_coll_approx_x1 < 1.0',
    #     channels=['ee', 'mumu', 'emu', 'mue'],
    #     regions=filter(lambda t: not t.startswith('Ztt_cr'),  REGIONS))
    # #CUTBOOK.append(CutX0X1_high_ll)

    # CutX0X1_high_lh = DecoratedCut(
    #     'x0x1_lh', 
    #     'higher cut of Collinear approx of the two visible leptons',
    #     'ditau_coll_approx_x0 < 1.4 && ditau_coll_approx_x1 < 1.2',
    #     channels=['e1p', 'e3p', 'mu1p', 'mu3p'])
    # #CUTBOOK.append(CutX0X1_high_lh)

    # CutX0X1_high_hh = DecoratedCut(
    #     'x0x1_ll', 
    #     'higher cut of Collinear approx of the two visible leptons',
    #     'ditau_coll_approx_x0 < 1.4 && ditau_coll_approx_x1 < 1.4',
    #     channels=['1p1p', '1p3p', '3p1p', '3p3p'])
    # #CUTBOOK.append(CutX0X1_high_hh)

    # # ditau angular cuts
    # CutDiTauAngle_ll = DecoratedCut(
    #     'ditau_dr_ll', 
    #     'DR(t_l, t_l) < 2.0 and |deta(l, l)| < 1.5',
    #     'ditau_dr < 2.0 && fabs(ditau_deta) < 1.5',
    #     channels=['ee', 'mumu', 'emu', 'mue'],
    #     regions=filter(lambda t: not t.startswith('Ztt_cr'),  REGIONS))
    # #CUTBOOK.append(CutDiTauAngle_ll)

    # CutDiTauAngle_hh = DecoratedCut(
    #     'ditau_dr_hh', 
    #     '0.6 < DR(t_l, t_l) < 2.5 and |deta(t_h, t_h)| < 1.5',
    #     'ditau_dr > 0.6 && ditau_dr < 2.5 && fabs(ditau_deta) < 1.5',
    #     channels=['1p1p', '1p3p', '3p1p', '3p3p'],
    #     categories=filter(lambda t: t != 'qcd_fit', CATEGORIES))
    # #CUTBOOK.append(CutDiTauAngle_hh)

    # CutDiTauAngle_hh_qcdfit = DecoratedCut(
    #     'ditau_dr_hh_fake', 
    #     '0.6 < DR(t_l, t_l) < 2.5 and |deta(t_h, t_h)| < 2.0',
    #     'ditau_dr > 0.6 && ditau_dr < 2.5 && fabs(ditau_deta) < 2.0',
    #     channels=['1p1p', '1p3p', '3p1p', '3p3p'],
    #     categories='qcd_fit')
    #CUTBOOK.append(CutDiTauAngle_hh_qcdfit)

    # coll mass leplep (away from the Z)
    # CutCollinearMass = DecoratedCut(
    #     'coll_mass', 
    #     'm(col) > m(Z) - 25',
    #     'ditau_coll_approx_m >= 66.1876',
    #     channels=['emu', 'mue'])
    #CUTBOOK.append(CutCollinearMass)

    # MMC status
    # Cut_MMCStatus = DecoratedCut(
    #     'mmc_status',
    #     'mmc_status',
    #     'ditau_mmc_mlm_fit_status == 1',
    #     regions=filter(lambda t: not t.startswith('Ztt_cr'),  REGIONS))
    #CUTBOOK.append(Cut_MMCStatus)

    # gal: all below was added by me:

    # CutMET = DecoratedCut(
    #     'met_cut', 
    #     'met > 20',
    #     'met_p4.Pt() > 20',
    #     channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
    #CUTBOOK.append(CutMET)

    #if not AC, use symmetric cuts:
    if gconfig.isAC==0:
        print 'Symmetric Cuts'

        CutEta = DecoratedCut(
            'Cut Eta', 
            'Cut Eta for symmetry',
            '!(abs(tau_1_p4.Eta()) >= 1.37 && abs(tau_1_p4.Eta()) <= 1.52)',
            channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
        CUTBOOK.append(CutEta)

        CutEta2 = DecoratedCut(
            'Cut Eta', 
            'Cut Eta for symmetry',
            '(abs(tau_1_p4.Eta()) < 2.47)',
            channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
        CUTBOOK.append(CutEta2)

        CutEta3 = DecoratedCut(
            'Cut Eta', 
            'Cut Eta for symmetry',
            '(abs(tau_1_p4.Eta()) > 0.1)',
            channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
        CUTBOOK.append(CutEta3)

        Cutpt = DecoratedCut(
            'Cut pt', 
            'Cut pt for symmetry',
            'tau_1_p4.Pt() >= 30',
            channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
        CUTBOOK.append(Cutpt)

        # gal: add eveto to muons for symmetry:
        CutEvetoMu = DecoratedCut(
            'tau_evetoMu', 'Tau electron veto for muhad',
            '(n_muons == 1 && tau_0_n_charged_tracks == 1) ? tau_0_ele_bdt_score_trans_retuned > 0.15 : 1',
            channels=['mu1p', 'muhad'])
        CUTBOOK.append(CutEvetoMu)

        # add Zee nonVBF e-veto cut:
        CutEvents_Zee2 = DecoratedCut(
            'reduce_Zee_bkg', 
            '30 < ditau_vis_mass < 100',
            'ditau_p4.M() <= 90 || ditau_p4.M() >= 100',
            channels=['e1p', 'e3p', 'ehad','mu1p', 'mu3p', 'muhad'],
            categories=['vbf_0'])
        CUTBOOK.append(CutEvents_Zee2)

        # Change met to manual met:
        CutEvents_WJets = DecoratedCut(
            'Reject W+jets', 
            'ditau_met_sum_cos_dphi > -0.35',
            'PtCorrHelper::manual_met_cos(tau_0_p4.Pt(), tau_0_p4.Eta(),tau_0_p4.Phi(),tau_1_p4.Pt(), tau_1_p4.Eta(),tau_1_p4.Phi(), jet_0_p4.Pt(), jet_0_p4.Eta(), jet_0_p4.Phi() ,jet_1_p4.Pt(), jet_1_p4.Eta(), jet_1_p4.Phi() ,jet_2_p4.Pt(), jet_2_p4.Eta(),jet_2_p4.Phi(), n_jets) > -0.35',
            channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
        CUTBOOK.append(CutEvents_WJets)

    else:
        # reject W+jets events
        CutEvents_WJets = DecoratedCut(
            'Reject W+jets', 
            'ditau_met_sum_cos_dphi > -0.35',
            'ditau_met_sum_cos_dphi > -0.35',
            channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
        CUTBOOK.append(CutEvents_WJets)

        # reduce Zee bkg (only to etau channel and nonVBF region)

        CutEvents_Zee = DecoratedCut(
            'reduce_Zee_bkg', 
            '30 < ditau_vis_mass < 100',
            'ditau_p4.M() <= 90 || ditau_p4.M() >= 100',
            channels=['e1p', 'e3p', 'ehad'],
            categories='vbf_0')
        CUTBOOK.append(CutEvents_Zee)

        CutEta_e = DecoratedCut(
            'Cut Eta', 
            'Cut Eta for symmetry',
            '!(abs(tau_1_p4.Eta()) >= 1.37 && abs(tau_1_p4.Eta()) <= 1.52)',
            channels=['e1p', 'e3p', 'ehad'])
        CUTBOOK.append(CutEta_e)

        CutEta_e2 = DecoratedCut(
            'Cut Eta', 
            'Cut Eta for symmetry',
            '(abs(tau_1_p4.Eta()) < 2.47)',
            channels=['e1p', 'e3p', 'ehad'])
        CUTBOOK.append(CutEta_e2)

        CutEta_mu = DecoratedCut(
            'Cut Eta', 
            'Cut Eta for symmetry',
            '(abs(tau_1_p4.Eta()) < 2.5)',
            channels=['mu1p', 'mu3p', 'muhad'])
        CUTBOOK.append(CutEta_mu)

        Cutpt_ac = DecoratedCut(
            'Cut pt', 
            'Cut pt for symmetry',
            'tau_1_p4.Pt() >= 27.3',
            channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
        CUTBOOK.append(Cutpt_ac)



        # CutEvetoMu = DecoratedCut(
        #     'tau_evetoMu', 'Tau electron veto for muhad',
        #     '(n_muons == 1 && tau_0_n_charged_tracks == 1) ? tau_0_ele_bdt_score_trans_retuned > 0.15 : 1',
        #     channels=['mu1p', 'muhad'])
        # CUTBOOK.append(CutEvetoMu)


    #no longer needed:
    # if gconfig.isMC==1 :

    #     CutNum = DecoratedCut(
    #         'runNum', 
    #         'runNum',
    #         '!(NOMINAL_pileup_random_run_number==0)',
    #         channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
    #     CUTBOOK.append(CutNum)


    CutLightLeptonQuality4 = DecoratedCut(
        'Light Lepton anti Quality', 
        'anti id/iso',
        '(abs(tau_1_matched_pdgId) != 13 || abs(tau_0_matched_pdgId) != 15 ) && ( ( ((abs(tau_1_matched_pdgId) < 7 || tau_1_matched_pdgId == 21) == 0) && (mc_channel_number!=344774 && mc_channel_number!=344778 && mc_channel_number!=344781) ) || ( ((abs(tau_1_matched_pdgId) == 5 || abs(tau_1_matched_pdgId) == 4 ) == 0) && (mc_channel_number==344774 || mc_channel_number==344778 || mc_channel_number==344781) ) )',
        channels=['mu1p', 'mu3p', 'muhad'],
        regions=['fake_lep','fake_lep_same_sign'],
        mc_only=True)
    CUTBOOK.append(CutLightLeptonQuality4)

    CutLightLeptonQuality5 = DecoratedCut(
        'Light Lepton anti Quality', 
        'anti id/iso',
        '(abs(tau_1_matched_pdgId) != 13 || abs(tau_0_matched_pdgId) != 15 ) && ( ( ((abs(tau_1_matched_pdgId) < 7 || tau_1_matched_pdgId == 21) == 0) && (mc_channel_number!=344774 && mc_channel_number!=344778 && mc_channel_number!=344781) ) || ( ((abs(tau_1_matched_pdgId) == 5 || abs(tau_1_matched_pdgId) == 4 ) == 0) && (mc_channel_number==344774 || mc_channel_number==344778 || mc_channel_number==344781) ) )',
        channels=['e1p', 'e3p', 'ehad'],
        regions=['fake_lep','fake_lep_same_sign'],
        mc_only=True)
    CUTBOOK.append(CutLightLeptonQuality5)

    #for fake leptons from data:
    


    CutLightLeptonQuality2 = DecoratedCut(
        'Light Lepton anti Quality', 
        'anti id/iso',
        'tau_1_iso_FCTight_FixedRad != 1 ',
        channels=['mu1p', 'mu3p', 'muhad'],
        regions=['fake_lep','fake_lep_same_sign'])
    CUTBOOK.append(CutLightLeptonQuality2)

    CutLightLeptonQuality3 = DecoratedCut(
        'Light Lepton anti Quality', 
        'anti id/iso',
        'tau_1_id_medium != 1 ||  tau_1_iso_Gradient != 1 ',
        # ' tau_1_iso_Gradient != 1 ',
        channels=['e1p', 'e3p', 'ehad'],
        regions=['fake_lep','fake_lep_same_sign'])
    CUTBOOK.append(CutLightLeptonQuality3)

    # CutLightLeptonQuality4 = DecoratedCut(
    #     'Light Lepton anti Quality', 
    #     'anti id/iso',
    #     '((abs(tau_1_matched_pdgId) < 7 || tau_1_matched_pdgId == 21) == 0) && (mc_channel_number!=344774 && mc_channel_number!=344778 && mc_channel_number!=344781)',
    #     channels=['mu1p', 'mu3p', 'muhad'],
    #     regions=['fake_lep','fake_lep_same_sign'],
    #     mc_only=True)
    # CUTBOOK.append(CutLightLeptonQuality4)

    # CutLightLeptonQuality5 = DecoratedCut(
    #     'Light Lepton anti Quality', 
    #     'anti id/iso',
    #     '((abs(tau_1_matched_pdgId) < 7 || tau_1_matched_pdgId == 21) == 0) && (mc_channel_number!=344774 && mc_channel_number!=344778 && mc_channel_number!=344781)',
    #     channels=['e1p', 'e3p', 'ehad'],
    #     regions=['fake_lep','fake_lep_same_sign'],
    #     mc_only=True)
    # CUTBOOK.append(CutLightLeptonQuality5)

    # CutLightLeptonQuality6 = DecoratedCut(
    #     'Light Lepton anti Quality', 
    #     'anti id/iso',
    #     '((abs(tau_1_matched_pdgId) == 5 || abs(tau_1_matched_pdgId) == 4 ) == 0) && (mc_channel_number==344774 || mc_channel_number==344778 || mc_channel_number==344781)',
    #     channels=['mu1p', 'mu3p', 'muhad'],
    #     regions=['fake_lep','fake_lep_same_sign'],
    #     mc_only=True)
    # CUTBOOK.append(CutLightLeptonQuality6)

    # CutLightLeptonQuality7 = DecoratedCut(
    #     'Light Lepton anti Quality', 
    #     'anti id/iso',
    #     '((abs(tau_1_matched_pdgId) == 5 || abs(tau_1_matched_pdgId) == 4 ) == 0) && (mc_channel_number==344774 || mc_channel_number==344778 || mc_channel_number==344781)',
    #     channels=['e1p', 'e3p', 'ehad'],
    #     regions=['fake_lep','fake_lep_same_sign'],
    #     mc_only=True)
    # CUTBOOK.append(CutLightLeptonQuality7)





#if gconfig.truthMatch==1 :
#print 'Doing TruthMatching'
CutTauPdg = DecoratedCut(
'pdg Id', 
'pdg Id',
'abs(tau_0_matched_pdgId) == 15',
mc_only=True,
channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'],
regions=['TM_SR','TM_same_sign_SR'])
CUTBOOK.append(CutTauPdg)

CutElPdg = DecoratedCut(
'pdg Id', 
'pdg Id',
'abs(tau_1_matched_pdgId) == 11',
mc_only=True,
channels=['e1p', 'e3p', 'ehad'],
regions=['TM_SR','TM_same_sign_SR'])
CUTBOOK.append(CutElPdg)

CutMuPdg = DecoratedCut(
'pdg Id', 
'pdg Id',
'abs(tau_1_matched_pdgId) == 13',
mc_only=True,
channels=['mu1p', 'mu3p', 'muhad'],
regions=['TM_SR','TM_same_sign_SR'])
CUTBOOK.append(CutMuPdg)



CutAntiElPdg = DecoratedCut(
'anti pdg Id', 
'anti pdg Id',
'abs(tau_1_matched_pdgId) != 11 || abs(tau_0_matched_pdgId) != 15 ',
channels=['e1p', 'e3p', 'ehad'],
regions=['all_fakes','all_fakes_same_sign_SR'])
CUTBOOK.append(CutAntiElPdg)

CutAntiMuPdg = DecoratedCut(
'anti pdg Id', 
'anti pdg Id',
'abs(tau_1_matched_pdgId) != 13 || abs(tau_0_matched_pdgId) != 15 ',
channels=['mu1p', 'mu3p', 'muhad'],
regions=['all_fakes','all_fakes_same_sign_SR'])
CUTBOOK.append(CutAntiMuPdg)

# for data jet faking leptons
CutAntiElPdg2 = DecoratedCut(
'anti pdg Id', 
'anti pdg Id',
'(abs(tau_1_matched_pdgId) != 11 || abs(tau_0_matched_pdgId) != 15 ) && (( ((abs(tau_1_matched_pdgId) < 7 || tau_1_matched_pdgId == 21) == 0) && (mc_channel_number!=344774 && mc_channel_number!=344778 && mc_channel_number!=344781) ) || ( ((abs(tau_1_matched_pdgId) == 5 || abs(tau_1_matched_pdgId) == 4 ) == 0) && (mc_channel_number==344774 || mc_channel_number==344778 || mc_channel_number==344781) ) )',
channels=['e1p', 'e3p', 'ehad'],
regions=['mc_fakes','mc_fakes_same_sign'],
mc_only=True)
CUTBOOK.append(CutAntiElPdg2)

CutAntiMuPdg2 = DecoratedCut(
'anti pdg Id', 
'anti pdg Id',
'(abs(tau_1_matched_pdgId) != 13 || abs(tau_0_matched_pdgId) != 15 ) && ( ( ((abs(tau_1_matched_pdgId) < 7 || tau_1_matched_pdgId == 21) == 0) && (mc_channel_number!=344774 && mc_channel_number!=344778 && mc_channel_number!=344781) ) || ( ((abs(tau_1_matched_pdgId) == 5 || abs(tau_1_matched_pdgId) == 4 ) == 0) && (mc_channel_number==344774 || mc_channel_number==344778 || mc_channel_number==344781) ) )',
channels=['mu1p', 'mu3p', 'muhad'],
regions=['mc_fakes','mc_fakes_same_sign'],
mc_only=True)
CUTBOOK.append(CutAntiMuPdg2)

# CutAntiElPdg3 = DecoratedCut(
# 'anti pdg Id', 
# 'anti pdg Id',
# '(abs(tau_1_matched_pdgId) != 11 || abs(tau_0_matched_pdgId) != 15 ) && ((abs(tau_1_matched_pdgId) == 5 || abs(tau_1_matched_pdgId) == 4 ) == 0) && (mc_channel_number==344774 || mc_channel_number==344778 || mc_channel_number==344781)',
# channels=['e1p', 'e3p', 'ehad'],
# regions=['mc_fakes','mc_fakes_same_sign'],
# mc_only=True)
# CUTBOOK.append(CutAntiElPdg3)

# CutAntiMuPdg3 = DecoratedCut(
# 'anti pdg Id', 
# 'anti pdg Id',
# '(abs(tau_1_matched_pdgId) != 13 || abs(tau_0_matched_pdgId) != 15 ) && ((abs(tau_1_matched_pdgId) == 5 || abs(tau_1_matched_pdgId) == 4 ) == 0) && (mc_channel_number==344774 || mc_channel_number==344778 || mc_channel_number==344781)',
# channels=['mu1p', 'mu3p', 'muhad'],
# regions=['mc_fakes','mc_fakes_same_sign'],
# mc_only=True)
# CUTBOOK.append(CutAntiMuPdg3)



#e fake tau:

CutAntiElPdg2 = DecoratedCut(
'anti pdg Id', 
'anti pdg Id',
'abs(tau_0_matched_pdgId) == 11',
channels=['e1p', 'e3p', 'ehad','mu1p', 'mu3p', 'muhad'],
regions=['e_fakes'])
CUTBOOK.append(CutAntiElPdg2)

#mu fake tau:

CutAntiElPdg3 = DecoratedCut(
'anti pdg Id', 
'anti pdg Id',
'abs(tau_0_matched_pdgId) == 13',
channels=['e1p', 'e3p', 'ehad','mu1p', 'mu3p', 'muhad'],
regions=['mu_fakes'])
CUTBOOK.append(CutAntiElPdg3)

#non_lepton_fakes:

CutAntiElPdg4 = DecoratedCut(
'anti pdg Id', 
'anti pdg Id',
'abs(tau_0_matched_pdgId) != 15 && abs(tau_0_matched_pdgId) != 13 && abs(tau_0_matched_pdgId) != 11 ',
channels=['e1p', 'e3p', 'ehad','mu1p', 'mu3p', 'muhad'],
regions=['non_lep_fakes'])
CUTBOOK.append(CutAntiElPdg4)




# if gconfig.anti_tau_ff==1:

CutTauIDTig_LH2 = DecoratedCut(
'anti Tau RNN ID (Tight)', 
'tau_0_jet_rnn_tight!=1',
'tau_0_jet_rnn_tight != 1',
channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'],
regions=['j_fake_tau','j_fake_tau_same_sign'])
CUTBOOK.append(CutTauIDTig_LH2)


CutTauIDTig_LH = DecoratedCut(
    'Tau RNN ID (Tight)', 
    'tau_0_jet_rnn_tight==1',
    'tau_0_jet_rnn_tight==1',
    channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'],
    categories=filter(lambda t: not t in ('filter_tree'), CATEGORIES),
    regions=filter(lambda t: not t in ('j_fake_tau','j_fake_tau_same_sign'), REGIONS))
CUTBOOK.append(CutTauIDTig_LH)


if gconfig.ExtraCuts == 1 :

    ExtraCut1 = DecoratedCut(
        'lep_pt', 
        'lep_pt_cut',
        'tau_1_p4.Pt() >= 40',
        channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
    CUTBOOK.append(ExtraCut1)

    ExtraCut2 = DecoratedCut(
        'dphi_lep_met', 
        'dphi_lep_met',
        'abs(TVector2::Phi_mpi_pi(tau_1_p4.Phi() - PtCorrHelper::manual_met_phi(tau_0_p4.Pt(), tau_0_p4.Eta(),tau_0_p4.Phi(),tau_1_p4.Pt(), tau_1_p4.Eta(),tau_1_p4.Phi(), jet_0_p4.Pt(), jet_0_p4.Eta(), jet_0_p4.Phi() ,jet_1_p4.Pt(), jet_1_p4.Eta(), jet_1_p4.Phi() ,jet_2_p4.Pt(), jet_2_p4.Eta(),jet_2_p4.Phi(), n_jets))) > 1',
        channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'])
    CUTBOOK.append(ExtraCut2)







