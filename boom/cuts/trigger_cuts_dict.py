
triggers = {

        'period' : {
            'data' : {
                '15' : '(run_number > 0 && run_number <= 284484)',
                '16' : '(run_number > 284484 && run_number <= 311481)',
                '17' : '(run_number >= 324320 && run_number <= 341649)',
                '18' : '(run_number > 341650)',
                     },
            'mc' : {
                '15' : '(NOMINAL_pileup_random_run_number > 0 && NOMINAL_pileup_random_run_number <= 284484)',
                '16' : '(NOMINAL_pileup_random_run_number >  284484 && NOMINAL_pileup_random_run_number <= 311481)',
                '17' : '(NOMINAL_pileup_random_run_number >= 324320 && NOMINAL_pileup_random_run_number <= 341649)',
                '18' : '(NOMINAL_pileup_random_run_number > 341650)', 
                   },
                  },

        'lh' :  {
            'slt'  :  {
                'eh' : {
                    '15' : '((HLT_e24_lhmedium_L1EM20VH && tau_1_electron_trig_HLT_e24_lhmedium_L1EM20VH) || (HLT_e60_lhmedium && tau_1_electron_trig_HLT_e60_lhmedium) || (HLT_e120_lhloose && tau_1_electron_trig_HLT_e120_lhloose))', 
	            '16' : '((HLT_e26_lhtight_nod0_ivarloose && tau_1_electron_trig_HLT_e26_lhtight_nod0_ivarloose) || (HLT_e60_lhmedium_nod0 && tau_1_electron_trig_HLT_e60_lhmedium_nod0) ||( HLT_e140_lhloose_nod0 && tau_1_electron_trig_HLT_e140_lhloose_nod0))',
                    '17' : '((HLT_e26_lhtight_nod0_ivarloose && tau_1_electron_trig_HLT_e26_lhtight_nod0_ivarloose) || (HLT_e60_lhmedium_nod0 && tau_1_electron_trig_HLT_e60_lhmedium_nod0) ||( HLT_e140_lhloose_nod0 && tau_1_electron_trig_HLT_e140_lhloose_nod0))',
                    '18' : '((HLT_e26_lhtight_nod0_ivarloose && tau_1_electron_trig_HLT_e26_lhtight_nod0_ivarloose) || (HLT_e60_lhmedium_nod0 && tau_1_electron_trig_HLT_e60_lhmedium_nod0) ||( HLT_e140_lhloose_nod0 && tau_1_electron_trig_HLT_e140_lhloose_nod0))',
		         },
	        'mh' : {
		    '15' :  '((HLT_mu20_iloose_L1MU15 && tau_1_muon_trig_HLT_mu20_iloose_L1MU15) || (HLT_mu50 && tau_1_muon_trig_HLT_mu50))', 
	            '16' :  '((HLT_mu26_ivarmedium && tau_1_muon_trig_HLT_mu26_ivarmedium) || (HLT_mu50 && tau_1_muon_trig_HLT_mu50))',
                    '17' :  '((HLT_mu26_ivarmedium && tau_1_muon_trig_HLT_mu26_ivarmedium) || (HLT_mu50 && tau_1_muon_trig_HLT_mu50))',
                    '18' :  '((HLT_mu26_ivarmedium && tau_1_muon_trig_HLT_mu26_ivarmedium) || (HLT_mu50 && tau_1_muon_trig_HLT_mu50))',
     	            	 },
  	              },
               },
        'll' : {
            'slt' :   { 
                'emu' :  { 
                    '15' :  '((( tau_0 == 2 && ((HLT_e24_lhmedium_L1EM20VH && tau_0_electron_trig_HLT_e24_lhmedium_L1EM20VH) || (HLT_e60_lhmedium && tau_0_electron_trig_HLT_e60_lhmedium) || (HLT_e120_lhloose && tau_0_electron_trig_HLT_e120_lhloose) ) && tau_0_p4.Pt() >= 25) || ( tau_1 == 1 && ((HLT_mu20_iloose_L1MU15 && tau_1_muon_trig_HLT_mu20_iloose_L1MU15) || (HLT_mu50 && tau_1_muon_trig_HLT_mu50)) && tau_1_p4.Pt() > 21 && tau_0_p4.Pt() < 25)))',
                    '16' :  '((( tau_0 == 2 && ((HLT_e26_lhtight_nod0_ivarloose && tau_0_electron_trig_HLT_e26_lhtight_nod0_ivarloose) || (HLT_e60_lhmedium_nod0 && tau_0_electron_trig_HLT_e60_lhmedium_nod0) || ( HLT_e140_lhloose_nod0 && tau_0_electron_trig_HLT_e140_lhloose_nod0) ) && tau_0_p4.Pt() >= 27) || ( tau_1 == 1 && ((HLT_mu26_ivarmedium && tau_1_muon_trig_HLT_mu26_ivarmedium) || (HLT_mu50 && tau_1_muon_trig_HLT_mu50)) && tau_1_p4.Pt() > 27.3 && tau_0_p4.Pt() < 27)))',
                    '17' :  '((( tau_0 == 2 && ((HLT_e26_lhtight_nod0_ivarloose && tau_0_electron_trig_HLT_e26_lhtight_nod0_ivarloose) || (HLT_e60_lhmedium_nod0 && tau_0_electron_trig_HLT_e60_lhmedium_nod0) || ( HLT_e140_lhloose_nod0 && tau_0_electron_trig_HLT_e140_lhloose_nod0) ) && tau_0_p4.Pt() >= 27) || ( tau_1 == 1 && ((HLT_mu26_ivarmedium && tau_1_muon_trig_HLT_mu26_ivarmedium) || (HLT_mu50 && tau_1_muon_trig_HLT_mu50)) && tau_1_p4.Pt() > 27.3 && tau_0_p4.Pt() < 27)))',
                    '18' :  '((( tau_0 == 2 && ((HLT_e26_lhtight_nod0_ivarloose && tau_0_electron_trig_HLT_e26_lhtight_nod0_ivarloose) || (HLT_e60_lhmedium_nod0 && tau_0_electron_trig_HLT_e60_lhmedium_nod0) || ( HLT_e140_lhloose_nod0 && tau_0_electron_trig_HLT_e140_lhloose_nod0) ) && tau_0_p4.Pt() >= 27) || ( tau_1 == 1 && ((HLT_mu26_ivarmedium && tau_1_muon_trig_HLT_mu26_ivarmedium) || (HLT_mu50 && tau_1_muon_trig_HLT_mu50)) && tau_1_p4.Pt() > 27.3 && tau_0_p4.Pt() < 27)))',
                         },
                'mue' :  {
                    '15' : '((( tau_1 == 2 && ((HLT_e24_lhmedium_L1EM20VH && tau_1_electron_trig_HLT_e24_lhmedium_L1EM20VH) || (HLT_e60_lhmedium && tau_1_electron_trig_HLT_e60_lhmedium) || (HLT_e120_lhloose && tau_1_electron_trig_HLT_e120_lhloose) ) && tau_1_p4.Pt() >= 25) || ( tau_0 == 1 && ((HLT_mu20_iloose_L1MU15 && tau_0_muon_trig_HLT_mu20_iloose_L1MU15) || (HLT_mu50 && tau_0_muon_trig_HLT_mu50)) && tau_0_p4.Pt() > 21 && tau_1_p4.Pt() < 25)))',
                    '16' : '((( tau_1 == 2 && ((HLT_e26_lhtight_nod0_ivarloose && tau_1_electron_trig_HLT_e26_lhtight_nod0_ivarloose) || (HLT_e60_lhmedium_nod0 && tau_1_electron_trig_HLT_e60_lhmedium_nod0) || ( HLT_e140_lhloose_nod0 && tau_1_electron_trig_HLT_e140_lhloose_nod0) ) && tau_1_p4.Pt() >= 27) || ( tau_0 == 1 && ((HLT_mu26_ivarmedium && tau_0_muon_trig_HLT_mu26_ivarmedium) || (HLT_mu50 && tau_0_muon_trig_HLT_mu50)) && tau_0_p4.Pt() > 27.3 && tau_1_p4.Pt() < 27)))',  
                    '17' : '((( tau_1 == 2 && ((HLT_e26_lhtight_nod0_ivarloose && tau_1_electron_trig_HLT_e26_lhtight_nod0_ivarloose) || (HLT_e60_lhmedium_nod0 && tau_1_electron_trig_HLT_e60_lhmedium_nod0) || ( HLT_e140_lhloose_nod0 && tau_1_electron_trig_HLT_e140_lhloose_nod0) ) && tau_1_p4.Pt() >= 27) || ( tau_0 == 1 && ((HLT_mu26_ivarmedium && tau_0_muon_trig_HLT_mu26_ivarmedium) || (HLT_mu50 && tau_0_muon_trig_HLT_mu50)) && tau_0_p4.Pt() > 27.3 && tau_1_p4.Pt() < 27)))',
                    '18' : '((( tau_1 == 2 && ((HLT_e26_lhtight_nod0_ivarloose && tau_1_electron_trig_HLT_e26_lhtight_nod0_ivarloose) || (HLT_e60_lhmedium_nod0 && tau_1_electron_trig_HLT_e60_lhmedium_nod0) || ( HLT_e140_lhloose_nod0 && tau_1_electron_trig_HLT_e140_lhloose_nod0) ) && tau_1_p4.Pt() >= 27) || ( tau_0 == 1 && ((HLT_mu26_ivarmedium && tau_0_muon_trig_HLT_mu26_ivarmedium) || (HLT_mu50 && tau_0_muon_trig_HLT_mu50)) && tau_0_p4.Pt() > 27.3 && tau_1_p4.Pt() < 27)))',
                        },
                     },
            'dlt':   { 
                'emu' : {
                    '15' : '(HLT_e17_lhloose_mu14 &&  tau_0_emu_trig_HLT_e17_lhloose_mu14 && tau_1_emu_trig_HLT_e17_lhloose_mu14 && tau_0 == 2 && tau_1 == 1 && tau_0_p4.Pt() > 18 && tau_0_p4.Pt() < 25 && tau_1_p4.Pt() > 15 && tau_1_p4.Pt() < 21)',
                    '16' : '(HLT_e17_lhloose_nod0_mu14 &&  tau_0_emu_trig_HLT_e17_lhloose_nod0_mu14 && tau_1_emu_trig_HLT_e17_lhloose_nod0_mu14 && tau_0 == 2 && tau_1 == 1 && tau_0_p4.Pt() > 18 && tau_0_p4.Pt() < 27 && tau_1_p4.Pt() > 15 && tau_1_p4.Pt() < 27.3)',
                    '17' : '(HLT_e17_lhloose_nod0_mu14 &&  tau_0_emu_trig_HLT_e17_lhloose_nod0_mu14 && tau_1_emu_trig_HLT_e17_lhloose_nod0_mu14 && tau_0 == 2 && tau_1 == 1 && tau_0_p4.Pt() > 18 && tau_0_p4.Pt() < 27 && tau_1_p4.Pt() > 15 && tau_1_p4.Pt() < 27.3)',
                    '18' : '(HLT_e17_lhloose_nod0_mu14 &&  tau_0_emu_trig_HLT_e17_lhloose_nod0_mu14 && tau_1_emu_trig_HLT_e17_lhloose_nod0_mu14 && tau_0 == 2 && tau_1 == 1 && tau_0_p4.Pt() > 18 && tau_0_p4.Pt() < 27 && tau_1_p4.Pt() > 15 && tau_1_p4.Pt() < 27.3)',
                        },
                'mue' : {
                     '15' : '(HLT_e17_lhloose_mu14 &&  tau_0_emu_trig_HLT_e17_lhloose_mu14 && tau_1_emu_trig_HLT_e17_lhloose_mu14 && tau_0 == 1 && tau_1 == 2 && tau_0_p4.Pt() > 15 && tau_0_p4.Pt() < 21 && tau_1_p4.Pt() > 18 && tau_1_p4.Pt() < 25)',
                    '16' : '(HLT_e17_lhloose_nod0_mu14 &&  tau_0_emu_trig_HLT_e17_lhloose_nod0_mu14 && tau_1_emu_trig_HLT_e17_lhloose_nod0_mu14 && tau_0 == 1 && tau_1 == 2 && tau_0_p4.Pt() > 15 && tau_0_p4.Pt() < 27.3 && tau_1_p4.Pt() > 18 && tau_1_p4.Pt() < 27)',
                    '17' : '(HLT_e17_lhloose_nod0_mu14 &&  tau_0_emu_trig_HLT_e17_lhloose_nod0_mu14 && tau_1_emu_trig_HLT_e17_lhloose_nod0_mu14 && tau_0 == 1 && tau_1 == 2 && tau_0_p4.Pt() > 15 && tau_0_p4.Pt() < 27.3 && tau_1_p4.Pt() > 18 && tau_1_p4.Pt() < 27)', 
                    '18' : '(HLT_e17_lhloose_nod0_mu14 &&  tau_0_emu_trig_HLT_e17_lhloose_nod0_mu14 && tau_1_emu_trig_HLT_e17_lhloose_nod0_mu14 && tau_0 == 1 && tau_1 == 2 && tau_0_p4.Pt() > 15 && tau_0_p4.Pt() < 27.3 && tau_1_p4.Pt() > 18 && tau_1_p4.Pt() < 27)',
                        }, 
                     },
                },              
}

