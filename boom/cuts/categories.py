"""
Implementation of the inclusive categories and R20.7 style 13 SRs
"""

from .base import DecoratedCut, CUTBOOK
from . import REGIONS, CATEGORIES
#_vbf_incl_title = 'pt(j1) > 30, mjj > 350, |Deltaeta(j, j)| > 3, eta(j0) x eta(j1) < 0, dijet_centrality'
_vbf_incl_title = 'n_jets_30 > 1, pt(j0) > 40, pt(j1) > 30, |Deltaeta(j, j)| > 3, mjj > 400'
#_vbf_incl_expr = 'jet_1_p4.Pt() > 30 && dijet_p4.M() > 350 && fabs(jet_0_p4.Eta()-jet_1_p4.Eta()) > 3 && jet_0_p4.Eta() * jet_1_p4.Eta() < 0 && is_dijet_centrality == 1'
_vbf_incl_expr = 'n_jets_30 > 1 && jet_0_p4.Pt() > 40 && jet_1_p4.Pt()>30 && fabs(jet_0_p4.Eta()-jet_1_p4.Eta()) > 3 && dijet_p4.M() > 400'

# VBF inclusive category (see vbf.py for final SRs)
CutVBFInclusive = DecoratedCut(
    'vbf inclusive', 
    _vbf_incl_title,
    _vbf_incl_expr,
    categories=filter(lambda c: 'vbf' in c, CATEGORIES),
    regions=filter(lambda t: t not in ('Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh') , REGIONS))
#CUTBOOK.append(CutVBFInclusive)

# VH inclusive category (see vh.py for final SRs)
CutVHInclusive = DecoratedCut(
    'VH inclusive',
    '60 < mjj <120',
    'dijet_p4.M() > 60 && dijet_p4.M() < 120 && n_jets_30 > 1',
    categories=['vh', 'vh_0', 'vh_1'])
CUTBOOK.append(CutVHInclusive)


# Boost inclusive category (see boost.py for final SRs)
CutNotVHInclusive = DecoratedCut(
    'Not VH inclusive',
    'Not VH inclusive',
    CutVHInclusive.cut.NOT().cut,
    categories=filter(lambda c: 'boost' in c, CATEGORIES))
CUTBOOK.append(CutNotVHInclusive)


CutNotVBFInclusive = DecoratedCut(
    'Not VBF inclusive', 
    'NOT vbf inclusive',
    CutVBFInclusive.cut.NOT().cut,
    categories=filter(lambda c: 'boost' in c, CATEGORIES),
    regions=filter(lambda r: r not in ('Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'), REGIONS))
#CUTBOOK.append(CutNotVBFInclusive)

CutBoostInclusive = DecoratedCut(
    'boost inclusive', 
    'pt(h) > 100',
    'ditau_higgspt > 100',
    categories=filter(lambda c: 'boost' in c, CATEGORIES),
    regions=filter(lambda t: t not in ('Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'), REGIONS))
CUTBOOK.append(CutBoostInclusive)


### Zll VR cuts #####

# Boost
CutBoostInclusive_zll_vr = DecoratedCut(
    'boost inclusive',
    'pt(ll) > 100',
    'ditau_p4.Pt() > 100',
    categories=filter(lambda c: 'boost' in c, CATEGORIES),
    regions=['Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'])
CUTBOOK.append(CutBoostInclusive_zll_vr)

# ttH
CutTTH = DecoratedCut(
    'tth inclusive',
    '6j1b_or_5j2b',
    '((n_jets >= 6 && n_bjets_DL1r_FixedCutBEff_70 >= 1) || (n_jets >= 5 && n_bjets_DL1r_FixedCutBEff_70 >= 2))',
    categories=['tth','tth_0','tth_1','tth_low','tth_high'],
    channels=['1p1p', '1p3p', '3p1p', '3p3p'],
    regions=filter(lambda t: t not in ('Zll_nomet_inc', 'Zll_nomet_ll', 'Zll_nomet_lh', 'Zll_nomet_hh') , REGIONS)
    )
CUTBOOK.append(CutTTH)

CutTTH_mass_window_low = DecoratedCut(
    'tth mass window tth_low',
    'tth mass window tth_low',
    'ditau_mmc_mlm_m < 100',
    categories=['tth_low'],
    channels=['1p1p', '1p3p', '3p1p', '3p3p'],
    regions=filter(lambda t: t not in ('Zll_nomet_inc', 'Zll_nomet_ll', 'Zll_nomet_lh', 'Zll_nomet_hh') , REGIONS)
    )
CUTBOOK.append(CutTTH_mass_window_low)

CutTTH_mass_window_high = DecoratedCut(
    'tth mass window tth_high',
    'tth mass window tth_high',
    'ditau_mmc_mlm_m > 150',
    categories=['tth_high'],
    channels=['1p1p', '1p3p', '3p1p', '3p3p'],
    regions=filter(lambda t: t not in ('Zll_nomet_inc', 'Zll_nomet_ll', 'Zll_nomet_lh', 'Zll_nomet_hh') , REGIONS)
    )
CUTBOOK.append(CutTTH_mass_window_high)

