"""
Implementation of the ttH categories
"""

from .base import DecoratedCut, CUTBOOK
from . import REGIONS
from ..mva_expr import tth_tt_expr, tth_Z_expr

Cut_ttH_1_hh = DecoratedCut(
    'tth_1_hh',
    'tth_1_hh',
    '{bdt_tt} > 0.5 && {bdt_Z} > 0.3 '.format(bdt_tt=tth_tt_expr,bdt_Z=tth_Z_expr),
    channels=['1p1p', '1p3p', '3p1p', '3p3p'],
    categories=['tth_1'])
CUTBOOK.append(Cut_ttH_1_hh)

Cut_ttH_0_hh = DecoratedCut(
    'tth_0_hh',
    'tth_0_hh',
    Cut_ttH_1_hh.cut.NOT().cut,
    channels=['1p1p', '1p3p', '3p1p', '3p3p'],
    categories=['tth_0'])
CUTBOOK.append(Cut_ttH_0_hh)

