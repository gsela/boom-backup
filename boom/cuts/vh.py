"""
Implementation of the VH categories
"""

from .base import DecoratedCut, CUTBOOK
from . import REGIONS, CATEGORIES

from ..mva_expr import vh_tagger_expr
from ..zll_pt import vh_tagger_corr_expr

_bdt_cut_ll = 0.0
_bdt_cut_lh = 0.125
_bdt_cut_hh = 0.1

CutVH_1_ll = DecoratedCut(
    'VH_1_ll',
    'LL VH BDT > {}'.format(_bdt_cut_ll),
    '{bdt} > {cut}'.format(bdt=vh_tagger_expr('ll'), cut=_bdt_cut_ll),
    regions=filter(lambda r: 'Ztt_cr' not in r, REGIONS),
    channels=['emu', 'mue'],
    categories=['vh_1'])
CUTBOOK.append(CutVH_1_ll)

CutVH_0_ll = DecoratedCut(
    'VH_0_ll',
    'not VH_1_ll',
    CutVH_1_ll.cut.NOT().cut,
    regions=filter(lambda r: 'Ztt_cr' not in r, REGIONS),
    channels=['emu', 'mue'],
    categories=['vh_0'])
CUTBOOK.append(CutVH_0_ll)

CutVH_1_lh = DecoratedCut(
    'VH_1_lh',
    'LH VH BDT > {}'.format(_bdt_cut_lh),
    '{bdt} > {cut}'.format(bdt=vh_tagger_expr('lh'), cut=_bdt_cut_lh),
    regions=filter(lambda r: 'Ztt_cr' not in r, REGIONS),
    channels=['e1p', 'e3p', 'mu1p', 'mu3p'],
    categories=['vh_1'])
CUTBOOK.append(CutVH_1_lh)

CutVH_0_lh = DecoratedCut(
    'VH_0_lh',
    'not VH_1_lh',
    CutVH_1_lh.cut.NOT().cut,
    regions=filter(lambda r: 'Ztt_cr' not in r, REGIONS),
    channels=['e1p', 'e3p', 'mu1p', 'mu3p'],
    categories=['vh_0'])
CUTBOOK.append(CutVH_0_lh)

CutVH_1_hh = DecoratedCut(
    'VH_1_hh',
    'HH VH BDT > {}'.format(_bdt_cut_hh),
    '{bdt} > {cut}'.format(bdt=vh_tagger_expr('hh'), cut=_bdt_cut_hh),
    regions=filter(lambda r: 'Ztt_cr' not in r, REGIONS),
    channels=['1p1p', '1p3p', '3p1p', '3p3p'],
    categories=['vh_1'])
CUTBOOK.append(CutVH_1_hh)

CutVH_0_hh = DecoratedCut(
    'VH_0_hh',
    'not VH_1_hh',
    CutVH_1_hh.cut.NOT().cut,
    regions=filter(lambda r: 'Ztt_cr' not in r, REGIONS),
    channels=['1p1p', '1p3p', '3p1p', '3p3p'],
    categories=['vh_0'])
CUTBOOK.append(CutVH_0_hh)

#### ZCR
CutVH_1_ll_zcr = DecoratedCut(
    'VH_1_ll_zcr',
    'LL VH BDT > {}'.format(_bdt_cut_ll),
    '{bdt} > {cut}'.format(bdt=vh_tagger_expr('ll'), cut=_bdt_cut_ll),
    regions=['Ztt_cr_ll'],
    channels=['ee', 'mumu'],
    categories=['vh_1'])
CUTBOOK.append(CutVH_1_ll_zcr)

CutVH_0_ll_zcr = DecoratedCut(
    'VH_0_ll_zcr',
    'not VH_1_ll_zcr',
    CutVH_1_ll_zcr.cut.NOT().cut,
    regions=['Ztt_cr_ll'],
    channels=['ee', 'mumu'],
    categories=['vh_0'])
CUTBOOK.append(CutVH_0_ll_zcr)

CutVH_1_ll_zcr_corr = DecoratedCut(
    'VH_1_ll_zcr_corr',
    'LL VH BDT > {}'.format(_bdt_cut_ll),
    '{bdt} > {cut}'.format(bdt=vh_tagger_corr_expr('ll'), cut=_bdt_cut_ll),
    regions=['Ztt_cr_corr_ll'],
    channels=['ee', 'mumu'],
    categories=['vh_1'])
CUTBOOK.append(CutVH_1_ll_zcr_corr)

CutVH_0_ll_zcr_corr = DecoratedCut(
    'VH_0_ll_zcr_corr',
    'not VH_1_ll_zcr_corr',
    CutVH_1_ll_zcr_corr.cut.NOT().cut,
    regions=['Ztt_cr_corr_ll'],
    channels=['ee', 'mumu'],
    categories=['vh_0'])
CUTBOOK.append(CutVH_0_ll_zcr_corr)

# inclusive Zll VR will use lephad BDT
CutVH_1_lh_zcr = DecoratedCut(
    'VH_1_lh_zcr',
    'LH VH BDT > {}'.format(_bdt_cut_lh),
    '{bdt} > {cut}'.format(bdt=vh_tagger_expr('lh'), cut=_bdt_cut_lh),
    regions=['Ztt_cr_lh', 'Ztt_cr_inc'],
    channels=['ee', 'mumu'],
    categories=['vh_1'])
CUTBOOK.append(CutVH_1_lh_zcr)

CutVH_0_lh_zcr = DecoratedCut(
    'VH_0_lh_zcr',
    'not VH_1_lh_zcr',
    CutVH_1_lh_zcr.cut.NOT().cut,
    regions=['Ztt_cr_lh', 'Ztt_cr_inc'],
    channels=['ee', 'mumu'],
    categories=['vh_0'])
CUTBOOK.append(CutVH_0_lh_zcr)

CutVH_1_lh_zcr_corr = DecoratedCut(
    'VH_1_lh_zcr_corr',
    'LH VH BDT > {}'.format(_bdt_cut_lh),
    '{bdt} > {cut}'.format(bdt=vh_tagger_corr_expr('lh'), cut=_bdt_cut_lh),
    regions=['Ztt_cr_corr_lh'],
    channels=['ee', 'mumu'],
    categories=['vh_1'])
CUTBOOK.append(CutVH_1_lh_zcr_corr)

CutVH_0_lh_zcr_corr = DecoratedCut(
    'VH_0_lh_zcr_corr',
    'not VH_1_lh_zcr_corr',
    CutVH_1_lh_zcr_corr.cut.NOT().cut,
    regions=['Ztt_cr_corr_lh'],
    channels=['ee', 'mumu'],
    categories=['vh_0'])
CUTBOOK.append(CutVH_0_lh_zcr_corr)


CutVH_1_hh_zcr = DecoratedCut(
    'VH_1_hh_zcr',
    'HH VH BDT > {}'.format(_bdt_cut_hh),
    '{bdt} > {cut}'.format(bdt=vh_tagger_expr('hh'), cut=_bdt_cut_hh),
    regions=['Ztt_cr_hh'],
    channels=['ee', 'mumu'],
    categories=['vh_1'])
CUTBOOK.append(CutVH_1_hh_zcr)

CutVH_0_hh_zcr = DecoratedCut(
    'VH_0_hh_zcr',
    'not VH_1_hh_zcr',
    CutVH_1_hh_zcr.cut.NOT().cut,
    regions=['Ztt_cr_hh'],
    channels=['ee', 'mumu'],
    categories=['vh_0'])
CUTBOOK.append(CutVH_0_hh_zcr)


CutVH_1_hh_zcr_corr = DecoratedCut(
    'VH_1_hh_zcr_corr',
    'HH VH BDT > {}'.format(_bdt_cut_hh),
    '{bdt} > {cut}'.format(bdt=vh_tagger_corr_expr('hh'), cut=_bdt_cut_hh),
    regions=['Ztt_cr_corr_hh'],
    channels=['ee', 'mumu'],
    categories=['vh_1'])
CUTBOOK.append(CutVH_1_hh_zcr_corr)

CutVH_0_hh_zcr_corr = DecoratedCut(
    'VH_0_hh_zcr_corr',
    'not VH_1_hh_zcr_corr',
    CutVH_1_hh_zcr_corr.cut.NOT().cut,
    regions=['Ztt_cr_corr_hh'],
    channels=['ee', 'mumu'],
    categories=['vh_0'])
CUTBOOK.append(CutVH_0_hh_zcr_corr)


