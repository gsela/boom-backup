"""
Implementation of the categories from the 36fb-1 analysis
"""

from .base import DecoratedCut, CUTBOOK
from . import REGIONS, CATEGORIES

# signal region cuts

# vbf lephad
CutVBFTight_LH = DecoratedCut(
    'lh_vbf_tight', 
    'mjj > 500 and pt(h) > 100',
    'dijet_p4.M() > 500 && ditau_higgspt > 100',
    categories=['vbf_tight'],
    channels=['e1p', 'e3p', 'mu1p', 'mu3p'])
CUTBOOK.append(CutVBFTight_LH)

CutVBFLoose_LH = DecoratedCut(
    'lh_vbf_loose', 
    'NOT lh_vbf_tight',
    CutVBFTight_LH.cut.NOT().cut,
    categories=['vbf_loose'],
    channels=['e1p', 'e3p', 'mu1p', 'mu3p'])
CUTBOOK.append(CutVBFLoose_LH)

# vbf leplep
CutVBFTight_LL = DecoratedCut(
    'll_vbf_tight', 
    'mjj > 800',
    'dijet_p4.M() > 800',
    categories=['vbf_tight'],
    channels=['emu', 'mue', 'ee', 'mumu'],
    regions=filter(lambda t: t not in ('Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'), REGIONS))
CUTBOOK.append(CutVBFTight_LL)

CutVBFLoose_LL = DecoratedCut(
    'll_vbf_loose', 
    'NOT ll_vbf_tight',
    CutVBFTight_LL.cut.NOT().cut,
    categories=['vbf_loose'],
    channels=['emu', 'mue', 'ee', 'mumu'],
    regions=filter(lambda t: t not in ('Ztt_cr_inc', 'Ztt_cr_ll','Ztt_cr_lh', 'Ztt_cr_hh'), REGIONS))
CUTBOOK.append(CutVBFLoose_LL)

# vbf hadhad
CutVBFLowDR_HH = DecoratedCut(
    'hh_vbf_lowdr', 
    'pt(h) > 140 and dr(tau, tau) < 1.5',
    'ditau_higgspt > 140 && ditau_dr < 1.5',
    categories=['vbf_lowdr'],
    channels=['1p1p', '1p3p', '3p1p', '3p3p'])
CUTBOOK.append(CutVBFLowDR_HH)

CutVBFNOTLowDR_HH = DecoratedCut(
    'not_hh_vbf_lowdr', 
    'NOT hh_vbf_lowdr',
    CutVBFLowDR_HH.cut.NOT().cut,
    categories=['vbf_tight', 'vbf_loose'],
    channels=['1p1p', '1p3p', '3p1p', '3p3p'])
CUTBOOK.append(CutVBFNOTLowDR_HH)

CutVBFTight_HH = DecoratedCut(
    'hh_vbf_tight', 
    'm(jj) > (1550 - 250 * DeltaEta(j,j))',
    'dijet_p4.M() > (1550 - 250 * abs(jet_0_p4.Eta()-jet_1_p4.Eta()))',
    categories=['vbf_tight'],
    channels=['1p1p', '1p3p', '3p1p', '3p3p'])
CUTBOOK.append(CutVBFTight_HH)

CutVBFLoose_HH = DecoratedCut(
    'hh_vbf_loose', 
    'not hh_vbf_tight',
    CutVBFTight_HH.cut.NOT().cut,
    categories=['vbf_loose'],
    channels=['1p1p', '1p3p', '3p1p', '3p3p'])
CUTBOOK.append(CutVBFLoose_HH)


# boost 
CutBoostTight = DecoratedCut(
    'boost_tight', 
    'pt(h) > 140 and dr(tau, l) < 1.5',
    'ditau_higgspt > 140 && ditau_dr < 1.5',
    categories=['boost_tight'],
    regions=filter(lambda t: t not in ('Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'), REGIONS))
CUTBOOK.append(CutBoostTight)

CutBoostLoose = DecoratedCut(
    'boost_loose', 
    'NOT boost_tight',
    CutBoostTight.cut.NOT().cut,
    categories=['boost_loose'],
    regions=filter(lambda t: t not in ('Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'), REGIONS))
CUTBOOK.append(CutBoostLoose)



# Uncorrected Ztt cr (Zll events)

# vbf lephad
CutVBFTight_LH_zll_vr = DecoratedCut(
    'lh_vbf_tight',
    'mjj > 500 and pt(ll) > 100',
    'dijet_p4.M() > 500 && ditau_p4.Pt() > 100',
    categories=['vbf_tight'],
    channels=['ee','mumu'],
    regions=['Ztt_cr_inc', 'Ztt_cr_lh'])
CUTBOOK.append(CutVBFTight_LH_zll_vr)

CutVBFLoose_LH_zll_vr = DecoratedCut(
    'lh_vbf_loose',
    'NOT lh_vbf_tight',
    CutVBFTight_LH_zll_vr.cut.NOT().cut,
    categories=['vbf_loose'],
    channels=['ee','mumu'],
    regions=['Ztt_cr_inc', 'Ztt_cr_lh'])
CUTBOOK.append(CutVBFLoose_LH_zll_vr)

# vbf leplep
CutVBFTight_LL_zll_vr = DecoratedCut(
    'll_vbf_tight',
    'mjj > 800',
    'dijet_p4.M() > 800',
    categories=['vbf_tight'],
    channels=['ee', 'mumu'],
    regions='Ztt_cr_ll')
CUTBOOK.append(CutVBFTight_LL_zll_vr)

CutVBFLoose_LL_zll_vr = DecoratedCut(
    'll_vbf_loose',
    'NOT ll_vbf_tight',
    CutVBFTight_LL_zll_vr.cut.NOT().cut,
    categories=['vbf_loose'],
    channels=['ee', 'mumu'],
    regions='Ztt_cr_ll')
CUTBOOK.append(CutVBFLoose_LL_zll_vr)

# vbf hadhad
CutVBFLowDR_zll_vr_HH = DecoratedCut(
    'zll_vr_hh_vbf_lowdr',
    'pt(ll) > 140 and dr(tau, tau) < 1.5',
    'ditau_p4.Pt() > 140 && ditau_dr < 1.5',
    categories=['vbf_lowdr'],
    regions='Ztt_cr_hh')
CUTBOOK.append(CutVBFLowDR_zll_vr_HH)

CutVBFNOTLowDR_zll_vr_HH = DecoratedCut(
    'zll_vr_hh_vbf_lowdr',
    'pt(ll) > 140 and dr(tau, tau) < 1.5',
    CutVBFLowDR_zll_vr_HH.cut.NOT().cut,
    categories=['vbf_loose', 'vbf_tight'],
    regions='Ztt_cr_hh')
CUTBOOK.append(CutVBFNOTLowDR_zll_vr_HH)

CutVBFTight_zll_vr_HH = DecoratedCut(
    'zll_vr_hh_vbf_tight',
    'm(jj) > (1550 - 250 * DeltaEta(j,j))',
    'dijet_p4.M() > (1550 - 250 * abs(jet_0_p4.Eta()-jet_1_p4.Eta()))',
    categories=['vbf_tight'],
    regions='Ztt_cr_hh')
CUTBOOK.append(CutVBFTight_zll_vr_HH)

CutVBFLoose_zll_vr_HH = DecoratedCut(
    'zll_vr_hh__vbf_loose',
    'not zll_vr_hh_vbf_tight',
    CutVBFTight_zll_vr_HH.cut.NOT().cut,
    categories=['vbf_loose'],
    regions='Ztt_cr_hh')
CUTBOOK.append(CutVBFLoose_zll_vr_HH)


# boost 
CutBoostTight_zll_vr = DecoratedCut(
    'boost_tight',
    'pt(ll) > 140 and dr(tau, l) < 1.5',
    'ditau_p4.Pt() > 140 && ditau_dr < 1.5',
    categories=['boost_tight'],
    regions=['Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'])
CUTBOOK.append(CutBoostTight_zll_vr)

CutBoostLoose_zll_vr = DecoratedCut(
    'boost_loose',
    'NOT boost_tight',
    CutBoostTight_zll_vr.cut.NOT().cut,
    categories=['boost_loose'],
    regions=['Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'])
CUTBOOK.append(CutBoostLoose_zll_vr)


