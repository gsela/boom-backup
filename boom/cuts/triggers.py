"""
Trigger decisions and trigger matching
"""

from .base import DecoratedCut, CUTBOOK


###
# Trigger
###

# lep-lep
Cut_trig_ee_set_15 = DecoratedCut(
    'trig_ee_set_15', 
    'trig ee single e 15',
    'tau_0_electron_trig_HLT_e24_lhmedium_L1EM20VH || tau_0_electron_trig_HLT_e60_lhmedium || tau_0_electron_trig_HLT_e120_lhloose',
    years=['15'],
    triggers=['single-e'],
    channels=['ee'])
CUTBOOK.append(Cut_trig_ee_set_15)

Cut_trig_mumu_smut_15 = DecoratedCut(
    'trig_mumu_smut_15', 
    'trig mumu single mu 15',
    'tau_0_muon_trig_HLT_mu20_iloose_L1MU15 || tau_0_muon_trig_HLT_mu50',
    years=['15'],
    triggers=['single-mu'],
    channels=['mumu'])
CUTBOOK.append(Cut_trig_mumu_smut_15)

Cut_trig_ee_set_16 = DecoratedCut(
    'trig_ee_set_16', 
    'trig ee single e 16',
    'tau_0_electron_trig_HLT_e26_lhtight_nod0_ivarloose || tau_0_electron_trig_HLT_e60_lhmedium_nod0 || tau_0_electron_trig_HLT_e140_lhloose_nod0',
    years=['16', '17', '18'],
    triggers=['single-e'],
    channels=['ee'])
CUTBOOK.append(Cut_trig_ee_set_16)

Cut_trig_mumu_smut_16 = DecoratedCut(
    'trig_mumu_smut_16', 
    'trig mumu single mu 16',
    'tau_0_muon_trig_HLT_mu26_ivarmedium || tau_0_muon_trig_HLT_mu50',
    years=['16', '17', '18'],
    triggers=['single-mu'],
    channels=['mumu'])
CUTBOOK.append(Cut_trig_mumu_smut_16)

Cut_trig_OR_ll = DecoratedCut(
    'trig_or_emu_mue',
    'trig OR emu mue channels',
    'triggerGlobal_match',
    years=['15', '16', '17', '18'],
    triggers=['trig-OR'],
    channels=['emu','mue'])
CUTBOOK.append(Cut_trig_OR_ll)

# lep-had
Cut_trig_eh_15 = DecoratedCut(
    'single electron trigger (2015)',
    'tau_1_p4.Pt() > 25.0 && (tau_1_electron_trig_HLT_e24_lhmedium_L1EM20VH || tau_1_electron_trig_HLT_e60_lhmedium || tau_1_electron_trig_HLT_e120_lhloose)',
    'tau_1_p4.Pt() > 25.0 && (tau_1_electron_trig_HLT_e24_lhmedium_L1EM20VH || tau_1_electron_trig_HLT_e60_lhmedium || tau_1_electron_trig_HLT_e120_lhloose)',
    years=['15'],
    triggers=['single-e'],
    channels=['e1p', 'e3p', 'ehad'])
CUTBOOK.append(Cut_trig_eh_15)

Cut_trig_mh_15 = DecoratedCut(
    'single muon trigger (2015)',
    'tau_1_p4.Pt() > 21.0 && (tau_1_muon_trig_HLT_mu20_iloose_L1MU15 || tau_1_muon_trig_HLT_mu50)',
    'tau_1_p4.Pt() > 21.0 && (tau_1_muon_trig_HLT_mu20_iloose_L1MU15 || tau_1_muon_trig_HLT_mu50)',
    years=['15'],
    triggers=['single-mu'],
    channels=['mu1p', 'mu3p', 'muhad'])
CUTBOOK.append(Cut_trig_mh_15)

Cut_trig_eh_16 = DecoratedCut(
    'single electron trigger (2016, 2017, 2018)',
    'tau_1_p4.Pt() > 27.0 && (tau_1_electron_trig_HLT_e26_lhtight_nod0_ivarloose || tau_1_electron_trig_HLT_e60_lhmedium_nod0 || tau_1_electron_trig_HLT_e140_lhloose_nod0)',
    'tau_1_p4.Pt() > 27.0 && (tau_1_electron_trig_HLT_e26_lhtight_nod0_ivarloose || tau_1_electron_trig_HLT_e60_lhmedium_nod0 || tau_1_electron_trig_HLT_e140_lhloose_nod0)',
    years=['16', '17', '18'],
    triggers=['single-e'],
    channels=['e1p', 'e3p', 'ehad'])
CUTBOOK.append(Cut_trig_eh_16)

Cut_trig_mh_16 = DecoratedCut(
    'single muon trigger (2016, 2017, 2018)',
    'tau_1_p4.Pt() > 27.3 && (tau_1_muon_trig_HLT_mu26_ivarmedium || tau_1_muon_trig_HLT_mu50)',
    'tau_1_p4.Pt() > 27.3 && (tau_1_muon_trig_HLT_mu26_ivarmedium || tau_1_muon_trig_HLT_mu50)',
    triggers=['single-mu'],
    years=['16', '17', '18'],
    channels =['mu1p', 'mu3p', 'muhad'])
CUTBOOK.append(Cut_trig_mh_16)

# had-had
Cut_trig_hh_15 = DecoratedCut(
    'trig_hh_15', 'trigger hadhad 15',
    'tau_0_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM && tau_1_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM',
    years=['15'],
    channels=['1p1p', '1p3p', '3p1p', '3p3p'])
CUTBOOK.append(Cut_trig_hh_15)

Cut_trig_hh_16 = DecoratedCut(
    'trig_hh_16', 'trigger hadhad 16',
    'tau_0_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo && tau_1_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo',
    years=['16'],
    channels=['1p1p', '1p3p', '3p1p', '3p3p'])
CUTBOOK.append(Cut_trig_hh_16)

Cut_trig_hh_17 = DecoratedCut(
    'trig_hh_17', 'trigger hadhad 17',
    'tau_0_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_03dR30_L1DR_TAU20ITAU12I_J25 && tau_1_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_03dR30_L1DR_TAU20ITAU12I_J25',
    years=['17'],
    channels=['1p1p', '1p3p', '3p1p', '3p3p'])
CUTBOOK.append(Cut_trig_hh_17)

Cut_trig_hh_18 = DecoratedCut(
    'trig_hh_18', 'trigger hadhad 18',
    'tau_0_trig_HLT_tau35_medium1_tracktwoEF_tau25_medium1_tracktwoEF_03dR30_L1DR_TAU20ITAU12I_J25 && tau_1_trig_HLT_tau35_medium1_tracktwoEF_tau25_medium1_tracktwoEF_03dR30_L1DR_TAU20ITAU12I_J25',
    years=['18'],
    channels=['1p1p', '1p3p', '3p1p', '3p3p'])
CUTBOOK.append(Cut_trig_hh_18)

