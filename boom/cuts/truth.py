"""
Truth-level cuts for the MC samples
"""

from . import CHANNELS, YEARS, CATEGORIES, REGIONS, SYSTEMATICS
from .base import DecoratedCut, CUTBOOK
import gconfig

# Truth matching cuts
if gconfig.UseFilteredTree==0:
    CutLeadTau_NotAJet = DecoratedCut(
        'NOT Tau Fake Jet (MC Only)',
        'lead tau is not truth matched to a quark or a gluon',
        '((abs(tau_0_matched_pdgId) < 7 || tau_0_matched_pdgId == 21) == 0)',
        channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'],
        mc_only=True)
    CUTBOOK.append(CutLeadTau_NotAJet)

# CutLeadOrSubLeadTau_NotAJet = DecoratedCut(
#     'LeadSubLeadTau_NotAJet',
#     'lead tau or sublead tau is not truth matched to a quark or a gluon',
#     '((abs(tau_0_matched_pdgId) < 7 || tau_0_matched_pdgId == 21) == 0) || ((abs(tau_1_matched_pdgId) < 7 || tau_1_matched_pdgId == 21) == 0)',
#     channels=['1p1p', '1p3p', '3p1p', '3p3p'],
#     mc_only=True)
# #CUTBOOK.append(CutLeadOrSubLeadTau_NotAJet)


# LeadMotherWorTau = DecoratedCut(
#     "LeadMotherWOrTau",
#     'The mother of the lead lepton is a W or a tau',
#     'abs(tau_0_matched_mother_pdgId) == 15 || abs(tau_0_matched_mother_pdgId) == 24',
#     channels=['emu', 'mue'],
#     regions=['anti_isol', 'same_sign', 'same_sign_anti_isol'],
#     mc_only=True)
# CUTBOOK.append(LeadMotherWorTau)

# SubLeadMotherWorTau = DecoratedCut(
#     "SubLeadMotherWOrTau",
#     'The mother of the sublead lepton is a W or a tau',
#     'abs(tau_1_matched_mother_pdgId) == 15 || abs(tau_1_matched_mother_pdgId) == 24',
#     channels=['emu', 'mue'],
#     regions=['anti_isol', 'same_sign', 'same_sign_anti_isol'],
#     mc_only=True)
# CUTBOOK.append(SubLeadMotherWorTau)

# TruthETruthMu = DecoratedCut(
#     'TruthETruthMu',
#     'tau_0 is truth e, tau_1 is truth mu',
#     'abs(tau_0_matched_pdgId) == 11 && abs(tau_1_matched_pdgId) == 13',
#     channels=['emu'],
#     regions=['anti_isol', 'same_sign', 'same_sign_anti_isol'],
#     mc_only=True)
# CUTBOOK.append(TruthETruthMu)

# TruthMuTruthE = DecoratedCut(
#     'TruthMuTruthE',
#     'tau_0 is truth mu, tau_1 is truth e',
#     'abs(tau_0_matched_pdgId) == 13 && abs(tau_1_matched_pdgId) == 11',
#     channels=['mue'],
#     regions=['anti_isol', 'same_sign', 'same_sign_anti_isol'],
#     mc_only=True)
# CUTBOOK.append(TruthMuTruthE)

# SubLeadTruthE = DecoratedCut(
#     'TruthMuOrTruthE',
#     'tau_1 is truth e',
#     'abs(tau_1_matched_pdgId) == 11',
#     channels=['e1p','e3p'],
#     regions=['qcd_lh_anti_tau_truth_lep','qcd_lh_same_sign_anti_tau_truth_lep','iso_fact_lh_same_sign_anti_tau_num','iso_fact_lh_os_anti_tau_num','iso_fact_lh_same_sign_anti_tau_den','iso_fact_lh_os_anti_tau_den'],
#     mc_only=True)
# #CUTBOOK.append(SubLeadTruthE)

# SubLeadTruthMu = DecoratedCut(
#     'TruthMuOrTruthMu',
#     'tau_1 is truth muon',
#     'abs(tau_1_matched_pdgId) == 13',
#     channels=['mu1p','mu3p'],
#     regions=['qcd_lh_anti_tau_truth_lep','qcd_lh_same_sign_anti_tau_truth_lep','iso_fact_lh_same_sign_anti_tau_num','iso_fact_lh_os_anti_tau_num','iso_fact_lh_same_sign_anti_tau_den','iso_fact_lh_os_anti_tau_den'],
#     mc_only=True)
# #CUTBOOK.append(SubLeadTruthMu)

