"""
Implementation of the Boost categories
"""

from .base import DecoratedCut, CUTBOOK
from . import REGIONS, CATEGORIES


CutBoost0 = DecoratedCut(
    'boost_0', 
    'boost_0',
    'ditau_higgspt < 120',
    categories=['boost_0', 'boost_0_1J', 'boost_0_ge2J'],
    regions=filter(lambda t: t not in ('Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'), REGIONS))
CUTBOOK.append(CutBoost0)

CutBoost1 = DecoratedCut(
    'boost_1', 
    'boost_1',
    'ditau_higgspt > 120 && ditau_higgspt < 200',
    categories=['boost_1', 'boost_1_1J', 'boost_1_ge2J'],
    regions=filter(lambda t: t not in ('Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'), REGIONS))
CUTBOOK.append(CutBoost1)

CutBoost2 = DecoratedCut(
    'boost_2', 
    'boost_2',
    'ditau_higgspt > 200 && ditau_higgspt < 300',
    categories=['boost_2', 'boost_2_1J', 'boost_2_ge2J'],
    regions=filter(lambda t: t not in ('Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'), REGIONS))
CUTBOOK.append(CutBoost2)

CutBoost3 = DecoratedCut(
    'boost_3', 
    'boost_3',
    'ditau_higgspt > 300 ',
    categories=['boost_3', 'boost_3_1J', 'boost_3_ge2J'],
    regions=filter(lambda t: t not in ('Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'), REGIONS))
CUTBOOK.append(CutBoost3)

CutBoost0_zll_vr = DecoratedCut(
    'boost_0_zll_vr',
    'boost_0_zll_vr',
    'ditau_p4.Pt() < 120',
    categories=['boost_0', 'boost_0_1J', 'boost_0_ge2J'],
    regions=['Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'])
CUTBOOK.append(CutBoost0_zll_vr)

CutBoost1_zll_vr = DecoratedCut(
    'boost_1_zll_vr',
    'boost_1_zll_vr',
    'ditau_p4.Pt() > 120 && ditau_p4.Pt() < 200',
    categories=['boost_1', 'boost_1_1J', 'boost_1_ge2J'],
    regions=['Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'])
CUTBOOK.append(CutBoost1_zll_vr)

CutBoost2_zll_vr = DecoratedCut(
    'boost_2_zll_vr',
    'boost_2_zll_vr',
    'ditau_p4.Pt() > 200 && ditau_p4.Pt() < 300',
    categories=['boost_2', 'boost_2_1J', 'boost_2_ge2J'],
    regions=['Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'])
CUTBOOK.append(CutBoost2_zll_vr)

CutBoost3_zll_vr = DecoratedCut(
    'boost_3_zll_vr',
    'boost_3_zll_vr',
    'ditau_p4.Pt() > 300',
    categories=['boost_3', 'boost_3_1J', 'boost_3_ge2J'],
    regions=['Ztt_cr_inc', 'Ztt_cr_ll', 'Ztt_cr_lh', 'Ztt_cr_hh'])
CUTBOOK.append(CutBoost3_zll_vr)


CutBoost_1J = DecoratedCut(
    'boost_1J',
    'njets(30 GeV) = 1',
    'n_jets_30 == 1',
    categories=['boost_0_1J', 'boost_1_1J', 'boost_2_1J', 'boost_3_1J'])
CUTBOOK.append(CutBoost_1J)

CutBoost_ge2J = DecoratedCut(
    'boost_ge2J',
    'njets(30 GeV) = 1',
    'n_jets_30 > 1',
    categories=['boost_0_ge2J', 'boost_1_ge2J', 'boost_2_ge2J', 'boost_3_ge2J'])
CUTBOOK.append(CutBoost_ge2J)
