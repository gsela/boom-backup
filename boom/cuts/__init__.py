"""
Module to define all the cuts needed 
in the Htautau couplings analysis
"""

CHANNELS = [
    'ee',
    'mumu',
    'emu',
    'mue',
    'ehad',
    'muhad',
    'e1p',
    'e3p',
    'mu1p',
    'mu3p',
    '1p1p',
    '1p3p',
    '3p1p',
    '3p3p',
    ]
"""
list(str): channels used for boom internal computation.
need to be combined according to plotting needs.
"""


YEARS = [
    '15',
    '16',
    '17',
    '18',
]
"""
list(str): years used for boom internal computation.
need to be combined according to plotting needs.
"""

TRIGGERS = [
    'single-e',
    'single-mu',
    'trig-OR',
    'di-had',
]
"""
list(str): trigger split used for boom internal computation.
need to be combined according to plotting needs.
"""

CATEGORIES = [
    'vbf',
    'vbf_0',
    'vbf_1',
    'vbf_tight',
    'vbf_loose',
    'vbf_lowdr',
    'boost',
    'boost_loose',
    'boost_tight',
    'boost_0',
    'boost_1',
    'boost_2',
    'boost_3',
    'boost_0_1J',
    'boost_1_1J',
    'boost_2_1J',
    'boost_3_1J',
    'boost_0_ge2J',
    'boost_1_ge2J',
    'boost_2_ge2J',
    'boost_3_ge2J',
    'vh',
    'vh_1',
    'vh_0',
    'tth',
    'tth_1',
    'tth_0',
    'tth_low',
    'tth_high',
    'preselection',
    'filter_tree', #preselection without tight taus.
    'qcd_fit',
    'allSRs',
]
"""
list(str): categories used for boom internal computation.
need to be combined according to plotting needs.
"""

REGIONS = [
    'SR',
    'TM_SR',
    'TM_same_sign_SR',
    'all_fakes',
    'all_fakes_same_sign_SR',
    'j_fake_tau',
    'j_fake_tau_same_sign',
    'fake_lep',
    'fake_lep_same_sign',
    'mc_fakes',
    'mc_fakes_same_sign',
    'e_fakes',
    'mu_fakes',
    'non_lep_fakes',
    # used for lh closure test
    'same_sign_SR', 
    # Fake CR
    'anti_tau',
    'anti_isol',
    'nos',
    # Fake VR / calculation
    'nos_anti_tau',
    'same_sign',
    'same_sign_anti_isol',
    'same_sign_top',
    'same_sign_top_anti_isol',
    'same_sign_anti_tau',
    # used for lh closure test
    'same_sign_anti_tau_lh',
    # Top 
    'top',
    'top_anti_tau',
    'top_anti_isol',
    'Zll_lh',
    'Zll_ll',
    # Ztt control regions
    'Ztt_cr_inc',
    'Ztt_cr_ll',
    'Ztt_cr_lh',
    'Ztt_cr_hh',
    'Ztt_cr_corr_ll',
    'Ztt_cr_corr_lh',
    'Ztt_cr_corr_hh',
    #lephad WCR
    'W_lh',
    'W_lh_same_sign',
    'W_lh_anti_tau',
    'W_lh_same_sign_anti_tau',
    # lephad qcd CR
    'qcd_lh',
    'qcd_lh_same_sign',
    'qcd_lh_anti_tau',
    'qcd_lh_same_sign_anti_tau',
    'qcd_lh_anti_tau_truth_lep',
    'qcd_lh_same_sign_anti_tau_truth_lep',
    # iso factor OS region
    'iso_fact_lh_os_anti_tau_num',
    'iso_fact_lh_os_anti_tau_den',
    # Iso factor SS region 
    'iso_fact_lh_same_sign_anti_tau_num',
    'iso_fact_lh_same_sign_anti_tau_den',
    
]
"""
list(str): regions (sr and various crs).
need to be combined according to plotting needs.
"""
 
SYSTEMATICS = [
    'NOMINAL',
]


from . import cutbook
from .base import CUTBOOK
