"""
Data and MC period splits
"""

from .base import DecoratedCut, CUTBOOK

####
# Periods
#####
CutMC16a_15 = DecoratedCut(
    'mc16a 2015 period', 'mc16a 2015 period',
    'NOMINAL_pileup_random_run_number > 0 && NOMINAL_pileup_random_run_number <= 284484',
    ###'NOMINAL_pileup_random_run_number <= 284484',
    mc_only=True,
    years=['15'])
CUTBOOK.append(CutMC16a_15)

CutData15 = DecoratedCut(
    'Data 2015 period', 'Data 2015 period',
    'run_number > 0 && run_number <= 284484',
    ###'run_number <= 284484',
    data_only=True,
    years=['15'])
CUTBOOK.append(CutData15)

CutMC16a_16 = DecoratedCut(
    'mc16a 2016 period', 'mc16a 2016 period',
#    'NOMINAL_pileup_random_run_number >  284484 && NOMINAL_pileup_random_run_number <= 311563',
    'NOMINAL_pileup_random_run_number >= 297730 && NOMINAL_pileup_random_run_number <= 311481',
    ###'NOMINAL_pileup_random_run_number >  284484 && NOMINAL_pileup_random_run_number <= 324320',
    mc_only=True,
    years=['16'])
CUTBOOK.append(CutMC16a_16)

CutData16 = DecoratedCut(
    'Data 2016 period', 'Data 2016 period',
    ###'run_number > 284484 && run_number <= 311563',
    'run_number >= 297730 && run_number <= 311481',
    data_only=True,
    years=['16'])
CUTBOOK.append(CutData16)

CutMC16d_17 = DecoratedCut(
    'mc16d_17', 'mc16d 2017 period',
    ###'NOMINAL_pileup_random_run_number > 311563 && NOMINAL_pileup_random_run_number <= 341649',
    'NOMINAL_pileup_random_run_number >= 324320 && NOMINAL_pileup_random_run_number <= 341649',
    mc_only=True,
    years=['17'])
CUTBOOK.append(CutMC16d_17)

CutData17 = DecoratedCut(
    'Data17', 'Data 2017 period',
    ###'run_number >= 311563 && run_number <= 341649',
    'run_number >= 324320 && run_number <= 341649',
    data_only=True,
    years=['17'])
CUTBOOK.append(CutData17)

CutMC16e_18 = DecoratedCut(
    'mc16e_18', 'mc16e 2018 period',
    'NOMINAL_pileup_random_run_number >= 341650',
    mc_only=True,
    years=['18'])
CUTBOOK.append(CutMC16e_18)

CutData18 = DecoratedCut(
    'Data18', 'Data 2018 period',
    'run_number >=  341650',
    data_only=True,
    years=['18'])
CUTBOOK.append(CutData18)

