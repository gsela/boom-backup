"""
Implementation of the VBF categories
"""

from .base import DecoratedCut, CUTBOOK
from . import REGIONS
from ..mva_expr import vbf_tagger_expr

_vbf_incl_expr = 'n_jets_30 > 1 && jet_0_p4.Pt() > 40 && jet_1_p4.Pt()>30 && fabs(jet_0_p4.Eta()-jet_1_p4.Eta()) > 3 && dijet_p4.M() > 400'
# _vbf_incl_expr = 'isVBF==1'


Cut_VBF_1_hh = DecoratedCut(
    'vbf_1_hh',
    'vbf_1_hh',
#    '{tagger} > 0.15'.format(tagger=vbf_tagger_expr),
    _vbf_incl_expr,
    channels=['1p1p', '1p3p', '3p1p', '3p3p'],
    categories=['vbf_1'])
#CUTBOOK.append(Cut_VBF_1_hh)

Cut_VBF_0_hh = DecoratedCut(
    'vbf_0_hh',
    'vbf_0_hh',
    Cut_VBF_1_hh.cut.NOT().cut,
    channels=['1p1p', '1p3p', '3p1p', '3p3p'],
    categories=['vbf_0'])
#CUTBOOK.append(Cut_VBF_0_hh)

Cut_VBF_1_lh = DecoratedCut(
    'VBF',
    'vbf_1_lh',
    _vbf_incl_expr,
    channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'],
    categories=['vbf_1'])
CUTBOOK.append(Cut_VBF_1_lh)

Cut_VBF_0_lh = DecoratedCut(
    'nonVBF',
    'vbf_0_lh',
    Cut_VBF_1_lh.cut.NOT().cut,
    channels=['mu1p', 'mu3p', 'e1p', 'e3p', 'muhad', 'ehad'],
    categories=['vbf_0'])
CUTBOOK.append(Cut_VBF_0_lh)

Cut_VBF_1_ll = DecoratedCut(
    'vbf_1_ll',
    'vbf_1_ll',
#    '{tagger} > 0.15'.format(tagger=vbf_tagger_expr),
    _vbf_incl_expr,
    channels=['emu', 'mue', 'ee', 'mumu'],
    categories=['vbf_1'])
#CUTBOOK.append(Cut_VBF_1_ll)

Cut_VBF_0_ll = DecoratedCut(
    'vbf_0_ll',
    'vbf_0_ll',
    Cut_VBF_1_ll.cut.NOT().cut,
    channels=['emu', 'mue', 'ee', 'mumu'],
    categories=['vbf_0'])
#CUTBOOK.append(Cut_VBF_0_ll)

