#LUMINOSITY stored in pb
LUMI = {
    '15': 3219.56,
    # '16': 32988.1,
    # '17': 43587.3,
    '16': 32440.50,
    '17': 44307.4,
    '18': 58450.1,
}
"""dict: report the integrated pp luminosity for each year of LHC Run2"""

#LUMI_SCALE factor
LUMI_SCALE = {
    #  '15': (3219.56+32988.1)/3219.56,
    #  '16': (3219.56+32988.1)/32988.1,
    '15': (3219.56+32440.50)/3219.56,
    '16': (3219.56+32440.50)/32440.50,
    '17': 1,
    '18': 1,
}
"""dict: luminosity scaling factor to be applied to the MC campaigns"""

LUMI_YEAR={
    'mc16a': '15',
    'mc16d': '17',
    'mc16e': '18',
    '_15': '15',
    '_16': '16',
    '_17': '17',
    '_18': '18',
}
"""gal added dict: aid for scheduler create_tree func"""