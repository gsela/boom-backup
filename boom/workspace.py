# ROOT / python imports
import ROOT
import os
import multiprocessing

# happy imports
from happy.parallel import run_pool, FuncWorker

# local imports
from .utils import lumi_calc

DEFAULT_SAMPLES = [
    'ggH', 'VBFH', 'ZH', 'WH', 'ttH', 
    'Ztt', 'ZttEWK', 'Fake', 
    'Top', 'W', 'Zll', 'VV'
]

class channel(object):
    """Simple class to hold a workspace input channel"""
    def __init__(self, name):
        self.name = name
        self._samples = DEFAULT_SAMPLES
        self._selections = []
        self._histograms = []

    @property
    def samples(self):
        """list(str): list of samples"""
        return self._samples

    @samples.setter
    def samples(self, _samples):
        self._samples = _samples

    @property
    def selections(self):
        """list(selection): list of boom selection objects"""
        return self._selections

    @selections.setter
    def selections(self, _sels):
        self._selections = _sels

    @property
    def lumi(self):
        """float: return integrated lumi corresponding to the selections"""
        return lumi_calc(self._selections)

    @property
    def histograms(self):
        """list(ROOT.TH1): return integrated lumi corresponding to the selections"""
        return self._histograms

    @histograms.setter
    def histograms(self, _hists):
        self._histograms = _hists


def make_skeleton(path, filename, channels):
    """
    Helper function to create the skeleton of the workspace input file

    Parameters
    __________
    path : str
       path to store the workspace input
    filename : str
       name of the workspace input
    channels: list(channel)
       channels to include in the workspace
       
    """
    print 'BOOM: make workspace input file skeleton'
    wsi_file = ROOT.TFile(os.path.join(path, filename), 'recreate')
    for chan in channels:
        d = wsi_file.mkdir(chan.name)
        for sample in chan.samples:
            wsi_file.mkdir(os.path.join(chan.name, sample))
    wsi_file.Close()


def lumi_hist(channel):
    """
    Create the 1 bin histogram for the luminosity of a given channel
    
    Parameters
    __________
    channel : channel
       input channel
    
    Returns
    _______
    h : ROOT.TH1F
       1-bin histogram named lumiininvpb
    """

    h = ROOT.TH1F('lumiininvpb', 'lumiininvpb', 1, 0, 1)
    h.SetBinContent(1, channel.lumi)
    return h


def fill_channel(channel, variable, processor, syst_name=None, syst_type='up', multi_processing=False):
    """
    Helper function to fill the histograms for a given channel
    
    Parameters
    __________
    channel : channel
       input channel
    variable : HAPPy Variable
       input variable
    processor : boom_processor
       processor
    multi_processing : bool
       True to enable multi-core running, False otherwise
    """
    print 'BOOM: \t fill channel {}'.format(channel.name)
    if multi_processing:
        # small helper function for the FuncWorker
        def hist_sample(_samp, _sels, _var, _proc, _syst_name, _syst_type):
            h = _proc.get_hist_physics_process(_samp, _sels, _var, _syst_name, _syst_type)
            return h
        # create the list of workers
        workers = []
        for sample in channel.samples:
            w = FuncWorker(hist_sample, sample, channel.selections, variable, processor, syst_name, syst_type)
            workers.append(w)
        # run on multiprocessing.cpu_count() cores
        run_pool(workers)
        # retrieve the histograms from the workers and et them to the channel
        channel.histograms = [w.output for w in workers]
    else:
        hists = []
        for sample in channel.samples:
            h = processor.get_hist_physics_process(
                sample, channel.selections, variable, syst_name, syst_type)
            hists.append(h)
        channel.histograms = hists

def fill_wsi(path, filename, channels, variable, processor, write_lumi=True, hist_name='nominal', **kwargs):
    """
    Helper function to fill the workspace input (run make_skeleton first)

    Parameters
    __________
    path : str
       path to store the workspace input
    filename : str
       name of the workspace input
    channels: list(channel)
       channels to include in the workspace
    variable : HAPPy Variable
       input variable
    processor : boom_processor
       processor
       
    """
    print 'BOOM: fill workspace input file'
    stop_watch = ROOT.TStopwatch()
    wsi_file = ROOT.TFile.Open(os.path.join(path, filename), 'update')
    for channel in channels:
        fill_channel(channel, variable, processor, **kwargs)

    stop_watch.Stop()
    stop_watch.Print()
 
    for chan in channels:
        print 'BOOM: \t flush channel {}'.format(chan.name)
        wsi_file.cd('{}'.format(chan.name))
        if write_lumi:
            print 'BOOM: \t\t write lumi'
            h_lumi = lumi_hist(chan)
            h_lumi.Write(h_lumi.GetName())
        for samp, hist in zip(chan.samples, chan.histograms):
            print 'BOOM: \t\t flush sample {}'.format(samp)
            # don't write variation hists for Data
            if samp == 'Data':
                if hist_name != 'nominal' and not 'embed_' in hist_name:
                    continue
            hist.SetName(hist_name)
            wsi_file.cd('{}/{}'.format(chan.name, samp))
            hist.Write(hist.GetName())

    wsi_file.Close()

