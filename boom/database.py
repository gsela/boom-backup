# python imports
import os
import gconfig
# happy imports
from happy.crossSectionDB import crossSectionDB, PmgCrossSectionFile
from happy.style import Style
from happy.systematics import WeightSystematicVariation
from happy.dataset import RootDataset, CombinedDataset

# local imports
from boom.process_generator import process_generator

__all__ = [
    'get_processes',
    'sum_of_weight_bin',
]

# Need to initialize the database singleton
crossSectionFile = os.path.join(os.path.dirname(os.path.abspath(__file__ )), "../data/PMGxsecDB_mc16.txt")
crossSectionDB.readTextFile(crossSectionFile, 'new')

# Look for pre-defined environment variable, default to EOS

if gconfig.UseFilteredTree==1:
    PATH = os.getenv("BOOM_NTUPLE_PATH", "/eos/user/g/gsela/Trees/SmallTree_2")
# elif gconfig.CreateSmallTree==1:
#     PATH = os.getenv("BOOM_NTUPLE_PATH", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4/LFV_Htaulep_R21/V05")
elif gconfig.UseRatio==0:
    PATH = os.getenv("BOOM_NTUPLE_PATH", "/eos/user/g/gsela/LFV_V05/")
    # PATH = os.getenv("BOOM_NTUPLE_PATH", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4/LFV_Htaulep_R21/V05")
elif gconfig.UseRatio==1:
    PATH = os.getenv("BOOM_NTUPLE_PATH", "/eos/user/g/gsela/LFV_V05_ratio/")
else :
    print '$$$ Set Path To Trees!!!!! check /database.py'
#PATH = os.getenv("BOOM_NTUPLE_PATH", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4/LFV_Htaulep_R21/V05")

# Arrays of DSIDs
# Ztautau
ZttQCD = [
    344772, 
    344774, 344775, 344776, 
    344778, 344779, 344780, 344781, 344782, 
    364137, 364138, 364139, 364140, 364141, 
    364210, 364211, 364212, 364213, 364214, 364215
    ]
ZttEWK = [
    308094
]
Ztt = ZttQCD + ZttEWK

ZttMad = [
   361510, 361511, 361512, 361513, 361514,
   361638, 361639, 361640, 361641, 361642,
   ]

ttV = [
#    410155, #ttW
#    410218, 410219, 410220, #ttZ
#    412043, 304014 # 4t,3t
    ]

# Diboson
Diboson = [
    363355, 363356, 363357, 363358, 363359, 363360,
    363489, 
    363494, 
    364250, 
    364253, 364254, 364255, 
    364288, 364289, 364290
]

# Single-Top and ttbar
Top = [
    410470, 410471, 410472, 
    410644, 410645, 410646, 410647, 
    410658, 410659
]

# W(tau/mu/e + nu) + jets
W_Jets = [
    364156, 364157, 364158, 364159, 364160, 364161, 
    364162, 364163, 364164, 364165, 364166, 364167, 
    364168, 364169, 364170, 364171, 364172, 364173, 
    364174, 364175, 364176, 364177, 364178, 364179, 
    364180, 364181, 364182, 364183, 364184, 364185, 
    364186, 364187, 364188, 364189, 364190, 364191, 
    364192, 364193, 364194, 364195, 364196, 364197, 
    308096, 308097, 308098
]

# Zll + jets
ZllQCD = [
    345099, 345100, 345101, 345102, #zmmvbf zmmvbf zeevbf zeevbf
    364100, 364101, 364102, 364103, 364104, 364105, 364106, #zmm
    364107, 364108, 364109, 364110, 364111, 364112, 364113, #zmm
    364114, 364115, 364116, 364117, 364118, 364119, 364120, #Zee
    364121, 364122, 364123, 364124, 364125, 364126, 364127, #Zee
    364198, 364199, 364200, 364201, 364202, 364203, #zmm
    364204, 364205, 364206, 364207, 364208, 364209, #Zee
]

Zee=[     345101, 345102, # zeevbf zeevbf
    364114, 364115, 364116, 364117, 364118, 364119, 364120, #Zee
    364121, 364122, 364123, 364124, 364125, 364126, 364127, #Zee
    364204, 364205, 364206, 364207, 364208, 364209, #Zee
]

Zmumu=[    345099, 345100, #zmmvbf zmmvbf 
    364100, 364101, 364102, 364103, 364104, 364105, 364106, #zmm
    364107, 364108, 364109, 364110, 364111, 364112, 364113, #zmm
    364198, 364199, 364200, 364201, 364202, 364203, #zmm
]


ZllEWK = [
    308092, 308093
]
Zll = ZllQCD + ZllEWK

# SM Higgs
ggHtt    = [
    345120, 345121, 345122, 345123
]
ggHWW  = [
    345324
]
VBFHtt   = [
    346190, 346191, 346192, 346193
]
VBFHWW = [
    345948
]
WHtt     = [
    345211, 345212
]
ZHtt     = [
    345217
]
ttHtt    = [
    346343,346344,346345
]

# Signal samples
LFVggHETAU    = [345124]
LFVVBFHETAU   = [346194]
LFVWHETAU     = [345213, 345214]
LFVZHETAU     = [345218]

LFVggHMUTAU    = [345125]
LFVVBFHMUTAU   = [346195]
LFVWHMUTAU     = [345215, 345216]
LFVZHMUTAU     = [345219]

# Process Generator Singleton
generator = process_generator(PATH, do_zll_vr=True)

def _load_xsec_sumofweights(ntuples_block='nom'):
    """Helper function to load xsecxsumofweights

    Parameters
    ----------
    ntuples_block: str
    """
    from .xsec_sumofweights import build
    try:
        build(ntuples_block=ntuples_block)
    except:
        print 'BOOM: c++ weights map are not properly loaded'
        raise RuntimeError


def _squash_process(generator, process):
    """Helper function to squash DSIDs into one process
    
    Parameters
    ----------
    generator: process_generator
    process: happy.dataset.CombinedDataset
    """
    # leave data untouched
    if process.isData:
        return process

    # build a dictionnary of empty lists to store the files to squash
    squashing_files = {}
    for _campaign in generator.process_info(process)['campaigns']:
        squashing_files[_campaign] = {}
        for _stream in generator.process_info(process)['streams']:
            squashing_files[_campaign][_stream] = []

    # fill the dictionary with the list of files to squash
    for dataset in process.datasets:
        info_name = dataset.name.split('_')
        campaign = info_name[0]
        if len(info_name) == 3:
            stream = info_name[2]
        elif len(info_name) == 4:
            stream = info_name[2] + '_' + info_name[3]
        else:
            raise ValueError('dataset name %s is not recognized' % dataset.name)

        for file in dataset.fileNames:
            squashing_files[campaign][stream].append(file)
            
    # Flush the squashing_files dict in dataset list
    _dataset_list = []
    for _campaign in sorted(squashing_files.keys()):
        for _stream in sorted(squashing_files[_campaign].keys()):
            _files = squashing_files[_campaign][_stream]
            if len(_files) != 0:
                _dataset_list.append(RootDataset(
                        _campaign + '_' + process.name + '_' + _stream,
                        fileNames=_files,
                        weightExpression=process.datasets[0].weightExpression, 
                        isSignal=process.isSignal))
    # Define a combined dataset from the squashing
    _squashed_process = CombinedDataset(
        process.name, 
        process.title, 
        style=process.style, 
        datasets=_dataset_list, 
        isSignal=process.isSignal)
    return _squashed_process


def _build_processes(
    stxs='stxs0', 
    squash=True, 
    split_ztt=False,
    split_ztt_mtt=False,
    split_zll=False,
    split_ttbar=False,
    years=[15, 16, 17, 18],
    campaigns=['mc16a', 'mc16d', 'mc16e'],
    ntuples_block='nom',
    xsec_weights_loading=True):
    """return list of processes"""
    
    if xsec_weights_loading:
        # load xsec*sumofweight map
        _load_xsec_sumofweights(ntuples_block=ntuples_block)

    # Generate all processes in plotting order
    ##########################################

    # Data
    generator.generate_data(
        years=years, 
        style=Style(color=1, markerSize=1.2))

    # MC Backgrounds
    # ttV
#    generator.generate_mc(
#        'ttV, 4t, 3t', 'ttV, 4t, 3t', ttV,
#        isSignal=False,
#        campaigns=campaigns,
#        style=Style(color=601),
#        ntuples_block=ntuples_block)

    # Diboson
    generator.generate_mc(
        'Diboson', 'Diboson', Diboson, 
        isSignal=False, 
        campaigns=campaigns,
        style=Style(color=880),
        ntuples_block=ntuples_block)
    # Top
    if split_ttbar:
        generator.generate_mc(
            'TopRealTaus', 'Top (real #tau)', Top, 
            isSignal=False, 
            campaigns=campaigns,
            weightExpression="tau_0_matched_isHadTau==1&&tau_1_matched_isHadTau==1",
            style=Style(color=801),
            ntuples_block=ntuples_block)

        generator.generate_mc(
            'TopWithFakeTaus', 'Top (fake #tau)', Top,
            isSignal=False,
            campaigns=campaigns,
            weightExpression="tau_0_matched_isHadTau==0||tau_1_matched_isHadTau==0",
            style=Style(color=28),
            ntuples_block=ntuples_block)
        
    else:
        generator.generate_mc(
            'Top', 'Top', Top, 
            isSignal=False, 
            campaigns=campaigns,
            style=Style(color=801),
            ntuples_block=ntuples_block)

    # W + jets
    generator.generate_mc(
        'W_Jets', 'W_Jets', W_Jets, 
        isSignal=False, 
        campaigns=campaigns,
        style=Style(color=3), 
        ignoreChannel=[['leplep', [308097]]], #Skip all for this channel except listed one
        ntuples_block=ntuples_block)

    #Gal: Add Zee and Zmumu

    # generator.generate_mc(
    #         'Zee', 'Z#rightarrowll (QCD)', Zee, 
    #         campaigns=campaigns,
    #         isSignal=False,
    #         style=Style(color=422),
    #         ntuples_block=ntuples_block)

    # generator.generate_mc(
    #         'Zmumu', 'Z#rightarrowll (QCD)', Zmumu, 
    #         campaigns=campaigns,
    #         isSignal=False,
    #         style=Style(color=400),
    #         ntuples_block=ntuples_block)

    # Zll ( split in truth categories as done for Ztt)
    if split_zll:
        truth_cuts = {
            'truth_0': 'boson_0_truth_p4.Pt() < 120',
            'truth_1': 'boson_0_truth_p4.Pt() > 120 && boson_0_truth_p4.Pt() < 200',
            'truth_2': 'boson_0_truth_p4.Pt() > 200 && boson_0_truth_p4.Pt() < 300',
            'truth_3': 'boson_0_truth_p4.Pt() > 300',
            }
        for key in sorted(truth_cuts.keys()):
            generator.generate_mc(
                'ZllQCD_' + key,
                'Z#rightarrowll',
                ZllQCD,
                isSignal=False,
                campaigns=campaigns,
                weightExpression=truth_cuts[key],
                style=Style(color=422),
                ntuples_block=ntuples_block)
            generator.generate_mc(
                'ZllEWK_' + key,
                'Z#rightarrowll',
                ZllEWK,
                isSignal=False,
                weightExpression=truth_cuts[key],
                campaigns=campaigns,
                style=Style(color=420),
                ntuples_block=ntuples_block)
    else:
        generator.generate_mc(
            'ZllQCD', 'Z#rightarrowll (QCD)', ZllQCD, 
            campaigns=campaigns,
            isSignal=False,
            style=Style(color=422),
            ntuples_block=ntuples_block)

        generator.generate_mc(
            'ZllEWK', 'Z#rightarrowll (EWK)', ZllEWK,
            campaigns=campaigns,
            isSignal=False,
            style=Style(color=420),
            ntuples_block=ntuples_block)


    # HWW  
    generator.generate_mc( 
        'ggHWW', 'H#rightarrowWW', ggHWW,
        campaigns=campaigns, 
        isSignal=True, 
#        isSignal=False, 
        style=Style(color=910),
        ntuples_block=ntuples_block)

    
    generator.generate_mc(
        'VBFHWW', 'H#rightarrowWW', VBFHWW,
        campaigns=campaigns, 
        isSignal=True, 
#        isSignal=False, 
        style=Style(color=910),
        ntuples_block=ntuples_block)


    # Fakes
    generator.generate_data(
        name="Fake", 
        title="Fake", 
        years=years, 
        style=Style(color=400), 
        prefix="anti" )

    # More MC Backgrounds
    if split_ztt:
        truth_cuts = {
            'truth_0': 'boson_0_truth_p4.Pt() < 120',
            'truth_1': 'boson_0_truth_p4.Pt() > 120 && boson_0_truth_p4.Pt() < 200',
            'truth_2': 'boson_0_truth_p4.Pt() > 200 && boson_0_truth_p4.Pt() < 300',
            'truth_3': 'boson_0_truth_p4.Pt() > 300',
            }
            
        for key in sorted(truth_cuts.keys()):
            generator.generate_mc(
                'ZttQCD_' + key, 
                'Z#rightarrow#tau#tau', 
                ZttQCD, 
                isSignal=False, 
                campaigns=campaigns,
                weightExpression=truth_cuts[key],
                style=Style(color=861),
                ntuples_block=ntuples_block)

            generator.generate_mc(
                'ZttEWK_' + key, 
                'Z#rightarrow#tau#tau', 
                ZttEWK, 
                isSignal=False, 
                weightExpression=truth_cuts[key],
                campaigns=campaigns,
                style=Style(color=862),
                ntuples_block=ntuples_block)

            generator.generate_mc(
                'ZttMad_' + key,
                'Z#rightarrow#tau#tau',
                ZttMad,
                isSignal=False,
                weightExpression=truth_cuts[key],
                campaigns=campaigns,
                style=Style(color=863),
                ntuples_block=ntuples_block)

    else:
        generator.generate_mc(
            'ZttQCD', 'Z#rightarrow#tau#tau (QCD)', ZttQCD, 
            isSignal=False, 
            campaigns=campaigns,
            style=Style(color=861),
            ntuples_block=ntuples_block)

        generator.generate_mc(
            'ZttEWK', 'Z#rightarrow#tau#tau (EWK)', ZttEWK, 
            isSignal=False, 
            campaigns=campaigns,
            style=Style(color=862),
            ntuples_block=ntuples_block)
 
        generator.generate_mc(
            'ZttMad', 'Z#rightarrow#tau#tau', ZttMad,
            isSignal=False,
            campaigns=campaigns,
            style=Style(color=863),
            ntuples_block=ntuples_block)

    # SM Higgs
#    for prod_mod, dsids in zip(('ggH', 'VBFH', 'WH', 'ZH', 'ttH'), (ggH, VBFH, WH, ZH, ttH)):

#        if stxs == 'stxs12_fine':
#            from stxs import STXS_STAGE12_FINE
#            for stxs_bin in sorted(STXS_STAGE12_FINE[prod_mod].keys()):
#                _name = stxs_bin
#                generator.generate_mc(
#                    _name,
#                    'H#rightarrow#tau#tau',
#                    dsids,
#                    isSignal=True,
#                    isSignal=False,
#                    campaigns=campaigns,
#                    weightExpression=STXS_STAGE12_FINE[prod_mod][stxs_bin],
#                    style=Style(lineColor=600),
#                    ntuples_block=ntuples_block)       
#        elif stxs == 'stxs12':
#            from stxs import STXS_STAGE12
#            for stxs_bin in sorted(STXS_STAGE12[prod_mod].keys()):
#                _name = prod_mod + stxs_bin
#                generator.generate_mc(
#                    _name, 
#                    'H#rightarrow#tau#tau', 
#                    dsids,
#                    isSignal=True,
#                    isSignal=False,
#                    campaigns=campaigns,
#                    weightExpression=STXS_STAGE12[prod_mod][stxs_bin],
#                    style=Style(lineColor=600),
#                    ntuples_block=ntuples_block)   

#        elif stxs == 'stxs1':
#            if 'ttH' in prod_mod:
#                continue
#            from stxs import STXS_STAGE1
#            for stxs_bin in sorted(STXS_STAGE1[prod_mod].keys()):
#                _name = prod_mod + stxs_bin
#                generator.generate_mc(
#                    _name, 
#                    'H#rightarrow#tau#tau', 
#                    dsids, 
#                    isSignal=True, 
#                    isSignal=False, 
#                    campaigns=campaigns,
#                    weightExpression=STXS_STAGE1[prod_mod][stxs_bin],
#                    style=Style(lineColor=600),
#                    ntuples_block=ntuples_block)

#        else:
#            generator.generate_mc(
#                prod_mod, 
#                'H#rightarrow#tau#tau', 
#                dsids,   
#                isSignal=True, 
#                isSignal=True, 
#                campaigns=campaigns,
#                style=Style(lineColor=600),
#                ntuples_block=ntuples_block)

    generator.generate_mc( 
        'ggHtt', 'H#rightarrow#tau#tau', ggHtt,
        campaigns=campaigns, 
        isSignal=True, 
        style=Style(color=600),
        ntuples_block=ntuples_block)

    generator.generate_mc( 
        'VBFHtt', 'H#rightarrow#tau#tau', VBFHtt,
        campaigns=campaigns, 
        isSignal=True, 
        style=Style(color=600),
        ntuples_block=ntuples_block)

    generator.generate_mc( 
        'WHtt', 'H#rightarrow#tau#tau', WHtt,
        campaigns=campaigns, 
        isSignal=True, 
        style=Style(color=600),
        ntuples_block=ntuples_block)

    generator.generate_mc( 
        'ZHtt', 'H#rightarrow#tau#tau', ZHtt,
        campaigns=campaigns, 
        isSignal=True, 
        style=Style(color=600),
        ntuples_block=ntuples_block)

    generator.generate_mc( 
        'ttHtt', 'H#rightarrow#tau#tau', ttHtt,
        campaigns=campaigns, 
        isSignal=True, 
        style=Style(color=600),
        ntuples_block=ntuples_block)

    # Signal samples
    generator.generate_mc( 
        'LFVggHETAU', 'H#rightarrow#taue', LFVggHETAU,
        campaigns=campaigns, 
        isSignal=True, 
        style=Style(color=901),
        ntuples_block=ntuples_block)

    generator.generate_mc( 
        'LFVVBFHETAU', 'H#rightarrow#taue', LFVVBFHETAU,
        campaigns=campaigns, 
        isSignal=True, 
        style=Style(color=901),
        ntuples_block=ntuples_block)

    generator.generate_mc( 
        'LFVWHETAU', 'H#rightarrow#taue', LFVWHETAU,
        campaigns=campaigns, 
        isSignal=True, 
        style=Style(color=901),
        ntuples_block=ntuples_block)

    generator.generate_mc( 
        'LFVZHETAU', 'H#rightarrow#taue', LFVZHETAU,
        campaigns=campaigns, 
        isSignal=True, 
        style=Style(color=901),
        ntuples_block=ntuples_block)
    
    generator.generate_mc( 
        'LFVggHMUTAU', 'H#rightarrow#tau#mu', LFVggHMUTAU,
        campaigns=campaigns, 
        isSignal=True, 
        style=Style(color=901),
        ntuples_block=ntuples_block)

    generator.generate_mc( 
        'LFVVBFHMUTAU', 'H#rightarrow#tau#mu', LFVVBFHMUTAU,
        campaigns=campaigns, 
        isSignal=True, 
        style=Style(color=901),
        ntuples_block=ntuples_block)

    generator.generate_mc( 
        'LFVWHMUTAU', 'H#rightarrow#tau#mu', LFVWHMUTAU,
        campaigns=campaigns, 
        isSignal=True, 
        style=Style(color=901),
        ntuples_block=ntuples_block)

    generator.generate_mc( 
        'LFVZHMUTAU', 'H#rightarrow#tau#mu', LFVZHMUTAU,
        campaigns=campaigns, 
        isSignal=True, 
        style=Style(color=901),
        ntuples_block=ntuples_block)
    
    if not squash:
        return generator.processes

    if squash == True:
        _squashed_processes = []
        for process in generator.processes:
            squashed_process = _squash_process(generator, process)
            _squashed_processes.append(squashed_process)

        # overwrite the sumOfWeights
        for _process in _squashed_processes:
            if not _process.isData:
                _process.sumOfWeights = 1
                for _dataset in _process.datasets:
                    _dataset.sumOfWeights = 1
        return _squashed_processes

def get_processes(
        signal_only=False, 
        mc_only=False, 
        mc_only_wanted_processes=False,
        data_only=False,
        no_signal=False, 
        no_fake=False, 
        add_ztt_madgraph=False, 
        add_ztt_powheg=False, 
        wanted_processes='',
        **kwargs):
    """
    Build the database and return it to the user
    Argument
    --------
       signal_only : bool
          only return signal processes
       mc_only: bool
          only return MC samples
       no_signal: bool
          only return all processes but signal
       add_ztt_madgraph: bool
          add the Ztautau MadGraph process to the database
       add_ztt_powheg: bool
          add the Ztautau PowhegPythia process to the database
    Returns
    -------
        _processes: list(happy.dataset.CombinedDataset)
    """
    _processes = _build_processes(**kwargs)

    if not add_ztt_madgraph:
        _processes = filter(lambda p: p.name != 'ZttMad', _processes)

    if not add_ztt_powheg:
        _processes = filter(lambda p: p.name != 'ZttPow', _processes)

    if mc_only_wanted_processes: #added by gal
        _processes = filter(lambda p: not p.isData and (p.name in wanted_processes), _processes)
        return _processes

    if signal_only:
        _processes = filter(lambda p: p.isSignal, _processes)
        return _processes

    if mc_only:
        _processes = filter(lambda p: not p.isData, _processes)
        return _processes

    if data_only: #added by gal
        _processes = filter(lambda p: p.name == 'Data', _processes)
        return _processes
        
    if no_signal:
       _processes = filter(lambda p: not p.isSignal, _processes)

    if no_fake:
        _processes = filter(lambda p: p.name != 'Fake', _processes)
        return _processes

    return _processes


def sum_of_weight_bin(process, sumweight_sys=''):
    """
    Parameters
    __________
    process: HAPPy combined dataset
    """
    from happy.dataset import HistogramSumOfWeightsCalculator
    if process.isSignal:
        for dataset in process.datasets:
            if 'ggH' in process.name :
                dataset.sumOfWeightsCalculator = HistogramSumOfWeightsCalculator('h_metadata_theory_weights', 152)
            else :
                dataset.sumOfWeightsCalculator = HistogramSumOfWeightsCalculator('h_metadata_theory_weights', 110)

    if ('ZttQCD' in process.name or 'ZllQCD' in process.name) and sumweight_sys!='':
       for dataset in process.datasets:
          histbin = Z_pdf_theory_syst[sumweight_sys][1]
          # re-assign the bin for alternative pdf syst for Zmm 
          if dataset.dsid in xrange(364100,364114):
              if sumweight_sys == 'theory_z_CT14_pdfset':
                  histbin = 13
              elif sumweight_sys == 'theory_z_MMHT_pdfset':
                  histbin = 12 
          
          dataset.sumOfWeightsCalculator = HistogramSumOfWeightsCalculator(Z_pdf_theory_syst[sumweight_sys][0],histbin)

