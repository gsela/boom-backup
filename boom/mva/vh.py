"""
module to apply VH BDTs
"""
import ROOT
import os

    
_VH_ALL_BDT_FILE_TRAINED_BELOW_05_TESTED_ABOVE_05 = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../../data/vh_xml/vh_tagger_all_trained_below_05_tested_above_05.xml")
_VH_ALL_BDT_FILE_TRAINED_ABOVE_05_TESTED_BELOW_05 = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../../data/vh_xml/vh_tagger_all_trained_above_05_tested_below_05.xml")

_vh_variables = [
    "dijet_m",
    "dr_dijet_ditau",
    "ditau_higgspt",
    "dijet_pt",
    "ditau_higgspt_over_dijet_pt",
    "met",
    "dijet_dr",
    "dijet_dEta",
]

print ('BOOM: \t loading VH BDTs')
from .utils import _load_bdt
_load_bdt(_vh_variables, ROOT.mvaDict_vh.reader_vh_all_trained_below_05_tested_above_05, _VH_ALL_BDT_FILE_TRAINED_BELOW_05_TESTED_ABOVE_05)
_load_bdt(_vh_variables, ROOT.mvaDict_vh.reader_vh_all_trained_above_05_tested_below_05, _VH_ALL_BDT_FILE_TRAINED_ABOVE_05_TESTED_BELOW_05)


