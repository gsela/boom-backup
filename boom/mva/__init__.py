"""
Module to load and configure TMVA Readers
needed for bdt evaluation throughout BOOM
"""

import ROOT
ROOT.TMVA.gConfig().SetSilent(True)

# vbf bdts
import vbf

# vh bdts
import vh

# tth bdts
import tth
