"""
module to apply ttH BDTs
"""
import ROOT
import os


_BDT_FILE_TT = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../../data/tth_tt_xml/TMVACrossValidation_BDTG")
_BDT_FILE_Z = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../../data/tth_Z_xml/TMVACrossValidation_BDTG")

def _load_bdt_ttH_tt(variable_list, name='BDTG method'):
    """
    """
    import array
    dummy_val = array.array('f', [0.])
    for v in variable_list:
        ROOT.mvaDictTTH.reader_tth_tt_0.AddVariable(v, dummy_val)
        ROOT.mvaDictTTH.reader_tth_tt_1.AddVariable(v, dummy_val)
        ROOT.mvaDictTTH.reader_tth_tt_2.AddVariable(v, dummy_val)
        ROOT.mvaDictTTH.reader_tth_tt_3.AddVariable(v, dummy_val)
        ROOT.mvaDictTTH.reader_tth_tt_4.AddVariable(v, dummy_val)
    ROOT.mvaDictTTH.reader_tth_tt_0.AddSpectator("randm := mva_random_number_fix*1000", dummy_val);
    ROOT.mvaDictTTH.reader_tth_tt_1.AddSpectator("randm := mva_random_number_fix*1000", dummy_val);
    ROOT.mvaDictTTH.reader_tth_tt_2.AddSpectator("randm := mva_random_number_fix*1000", dummy_val);
    ROOT.mvaDictTTH.reader_tth_tt_3.AddSpectator("randm := mva_random_number_fix*1000", dummy_val);
    ROOT.mvaDictTTH.reader_tth_tt_4.AddSpectator("randm := mva_random_number_fix*1000", dummy_val);
 
    ROOT.mvaDictTTH.reader_tth_tt_0.BookMVA( name, _BDT_FILE_TT+"_fold1.weights.xml" )
    ROOT.mvaDictTTH.reader_tth_tt_1.BookMVA( name, _BDT_FILE_TT+"_fold2.weights.xml" )
    ROOT.mvaDictTTH.reader_tth_tt_2.BookMVA( name, _BDT_FILE_TT+"_fold3.weights.xml" )
    ROOT.mvaDictTTH.reader_tth_tt_3.BookMVA( name, _BDT_FILE_TT+"_fold4.weights.xml" )
    ROOT.mvaDictTTH.reader_tth_tt_4.BookMVA( name, _BDT_FILE_TT+"_fold5.weights.xml" )

def _load_bdt_ttH_Z(variable_list, name='BDTG method'):
    """
    """
    import array
    dummy_val = array.array('f', [0.])
    for v in variable_list:
        ROOT.mvaDictTTH.reader_tth_Z_0.AddVariable(v, dummy_val)
        ROOT.mvaDictTTH.reader_tth_Z_1.AddVariable(v, dummy_val)
        ROOT.mvaDictTTH.reader_tth_Z_2.AddVariable(v, dummy_val)
        ROOT.mvaDictTTH.reader_tth_Z_3.AddVariable(v, dummy_val)
        ROOT.mvaDictTTH.reader_tth_Z_4.AddVariable(v, dummy_val)
    ROOT.mvaDictTTH.reader_tth_Z_0.AddSpectator("randm := mva_random_number_fix*1000", dummy_val);
    ROOT.mvaDictTTH.reader_tth_Z_1.AddSpectator("randm := mva_random_number_fix*1000", dummy_val);
    ROOT.mvaDictTTH.reader_tth_Z_2.AddSpectator("randm := mva_random_number_fix*1000", dummy_val);
    ROOT.mvaDictTTH.reader_tth_Z_3.AddSpectator("randm := mva_random_number_fix*1000", dummy_val);
    ROOT.mvaDictTTH.reader_tth_Z_4.AddSpectator("randm := mva_random_number_fix*1000", dummy_val);
 

    ROOT.mvaDictTTH.reader_tth_Z_0.BookMVA( name, _BDT_FILE_Z+"_fold1.weights.xml" )
    ROOT.mvaDictTTH.reader_tth_Z_1.BookMVA( name, _BDT_FILE_Z+"_fold2.weights.xml" )
    ROOT.mvaDictTTH.reader_tth_Z_2.BookMVA( name, _BDT_FILE_Z+"_fold3.weights.xml" )
    ROOT.mvaDictTTH.reader_tth_Z_3.BookMVA( name, _BDT_FILE_Z+"_fold4.weights.xml" )
    ROOT.mvaDictTTH.reader_tth_Z_4.BookMVA( name, _BDT_FILE_Z+"_fold5.weights.xml" )

print('BOOM: \t loading ttH vs ttbar BDTs')
_load_bdt_ttH_tt([
    "ditau_dr",
    "ditau_deta",
    "met_reco_et",
    "(mWbest>0)*mWbest",
    "HTjets",
    "(mTopWbest>0)*mTopWbest",
    "ditau_pt",
    "jjdrmin",
    ])

print('BOOM: \t loading ttH vs ttZ BDTs')
_load_bdt_ttH_Z([
    "met_reco_et",
    "tau_pt_1",
    "(mWbest>0)*mWbest",
    "(mTopWbest>0)*mTopWbest",
    "SumPtBjet",
    "ditau_deta",
    "tau_eta_0",
    "jet_1_eta",
    "jet_0_eta",
    "ditau_met_min_dphi",
    "HTjets",
    ])

