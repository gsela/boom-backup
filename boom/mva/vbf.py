"""
module to apply VBF BDTs
"""
import ROOT
import os

_VBF_BDT_FILE_0P00_0P25 = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../../data/vbf_xml/TMVA_2nd_BDT_all_7vars_ntrees1000trained_nrnd_0p00_to_0p25_BDT.weights.xml")
_VBF_BDT_FILE_0P25_0P50 = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../../data/vbf_xml/TMVA_2nd_BDT_all_7vars_ntrees1000trained_nrnd_0p25_to_0p50_BDT.weights.xml")
_VBF_BDT_FILE_0P50_0P75 = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../../data/vbf_xml/TMVA_2nd_BDT_all_7vars_ntrees1000trained_nrnd_0p50_to_0p75_BDT.weights.xml")
_VBF_BDT_FILE_0P75_1P00 = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../../data/vbf_xml/TMVA_2nd_BDT_all_7vars_ntrees1000trained_nrnd_0p75_to_1p00_BDT.weights.xml")


_vbf_variables = [
    'pt_total',
    'dijet_prod_eta',
    'dijet_pt',
    'dijet_deta',
    'dijet_m',
    'jet_1_pt',
    'dijet_dphi',
]

print ('BOOM: \t loading VBF BDTs')
from .utils import _load_bdt
_load_bdt(_vbf_variables, ROOT.mvaDict.reader_0p00_to_0p25, _VBF_BDT_FILE_0P00_0P25)
_load_bdt(_vbf_variables, ROOT.mvaDict.reader_0p25_to_0p50, _VBF_BDT_FILE_0P25_0P50)
_load_bdt(_vbf_variables, ROOT.mvaDict.reader_0p50_to_0p75, _VBF_BDT_FILE_0P50_0P75)
_load_bdt(_vbf_variables, ROOT.mvaDict.reader_0p75_to_1p00, _VBF_BDT_FILE_0P75_1P00)

