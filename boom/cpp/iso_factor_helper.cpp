#include <TH1D.h>
#include <TH2D.h>
#include <unordered_map>

class isofactor_histDict
{
public:
  static std::unordered_map<int, TH2D> h_nominal_1prong;
  static std::unordered_map<int, TH2D> h_stat_1prong_up;
  static std::unordered_map<int, TH2D> h_stat_1prong_down;
  static std::unordered_map<int, TH2D> h_syst_1prong_up;
  static std::unordered_map<int, TH2D> h_syst_1prong_down;
  static std::unordered_map<int, TH2D> h_nominal_3prong;
  static std::unordered_map<int, TH2D> h_stat_3prong_up;
  static std::unordered_map<int, TH2D> h_stat_3prong_down;
  static std::unordered_map<int, TH2D> h_syst_3prong_up;
  static std::unordered_map<int, TH2D> h_syst_3prong_down;

  // historgrams for closure
  static std::unordered_map<int, TH2D> h_nominal_closure_1prong;
  static std::unordered_map<int, TH2D> h_stat_closure_1prong_up;
  static std::unordered_map<int, TH2D> h_stat_closure_1prong_down;
  static std::unordered_map<int, TH2D> h_syst_closure_1prong_up;
  static std::unordered_map<int, TH2D> h_syst_closure_1prong_down;
  static std::unordered_map<int, TH2D> h_nominal_closure_3prong;
  static std::unordered_map<int, TH2D> h_stat_closure_3prong_up;
  static std::unordered_map<int, TH2D> h_stat_closure_3prong_down;
  static std::unordered_map<int, TH2D> h_syst_closure_3prong_up;
  static std::unordered_map<int, TH2D> h_syst_closure_3prong_down; 

  enum syst{ nominal, stat_1prong_up, stat_1prong_down, syst_1prong_up, syst_1prong_down, stat_3prong_up, stat_3prong_down, syst_3prong_up, syst_3prong_down}; 

};

std::unordered_map<int, TH2D> createEmptyMapTH2D() 
{
  std::unordered_map<int, TH2D> internalMap;
  return internalMap;
}

std::unordered_map<int, TH2D> isofactor_histDict::h_nominal_1prong   = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_stat_1prong_up   = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_stat_1prong_down = createEmptyMapTH2D(); 
std::unordered_map<int, TH2D> isofactor_histDict::h_syst_1prong_up   = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_syst_1prong_down = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_nominal_3prong   = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_stat_3prong_up   = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_stat_3prong_down = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_syst_3prong_up   = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_syst_3prong_down = createEmptyMapTH2D();

std::unordered_map<int, TH2D> isofactor_histDict::h_nominal_closure_1prong   = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_stat_closure_1prong_up   = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_stat_closure_1prong_down = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_syst_closure_1prong_up   = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_syst_closure_1prong_down = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_nominal_closure_3prong   = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_stat_closure_3prong_up   = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_stat_closure_3prong_down = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_syst_closure_3prong_up   = createEmptyMapTH2D();
std::unordered_map<int, TH2D> isofactor_histDict::h_syst_closure_3prong_down = createEmptyMapTH2D();


namespace IsoFactorHelper {

  float get_isofactor(float lep_pt, float lep_eta, int ntracks, int version, int do_closure, int syst)
  {
    
    if(do_closure){
        if (ntracks > 1) {
            if( syst == isofactor_histDict::syst::stat_3prong_up) {
                int bin = isofactor_histDict::h_stat_closure_3prong_up.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_stat_closure_3prong_up.at(version).GetBinContent(bin);
            } else if ( syst == isofactor_histDict::syst::stat_3prong_down) {
                int bin = isofactor_histDict::h_stat_closure_3prong_down.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_stat_closure_3prong_down.at(version).GetBinContent(bin);
            } else if ( syst == isofactor_histDict::syst::syst_3prong_up) {
                int bin = isofactor_histDict::h_syst_closure_3prong_up.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_syst_closure_3prong_up.at(version).GetBinContent(bin);
            } else if ( syst == isofactor_histDict::syst::syst_3prong_down) {
                int bin = isofactor_histDict::h_syst_closure_3prong_down.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_syst_closure_3prong_down.at(version).GetBinContent(bin);
            } else {
                int bin = isofactor_histDict::h_nominal_closure_3prong.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_nominal_closure_3prong.at(version).GetBinContent(bin);
           }
        } else{
            if( syst == isofactor_histDict::syst::stat_1prong_up) {
                int bin = isofactor_histDict::h_stat_closure_1prong_up.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_stat_closure_1prong_up.at(version).GetBinContent(bin);
            } else if ( syst == isofactor_histDict::syst::stat_1prong_down) {
                int bin = isofactor_histDict::h_stat_closure_1prong_down.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_stat_closure_1prong_down.at(version).GetBinContent(bin);
            } else if ( syst == isofactor_histDict::syst::syst_1prong_up) {
                int bin = isofactor_histDict::h_syst_closure_1prong_up.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_syst_closure_1prong_up.at(version).GetBinContent(bin);
            } else if ( syst == isofactor_histDict::syst::syst_1prong_down) {
                int bin = isofactor_histDict::h_syst_closure_1prong_down.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_syst_closure_1prong_down.at(version).GetBinContent(bin);
            } else {
                int bin = isofactor_histDict::h_nominal_closure_1prong.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_nominal_closure_1prong.at(version).GetBinContent(bin);
            }    
        }
     } else {
        if (ntracks > 1) {
            if( syst == isofactor_histDict::syst::stat_3prong_up) {
                int bin = isofactor_histDict::h_stat_3prong_up.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_stat_3prong_up.at(version).GetBinContent(bin);
            } else if ( syst == isofactor_histDict::syst::stat_3prong_down) {
                int bin = isofactor_histDict::h_stat_3prong_down.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_stat_3prong_down.at(version).GetBinContent(bin);
            } else if ( syst == isofactor_histDict::syst::syst_3prong_up) {
                int bin = isofactor_histDict::h_syst_3prong_up.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_syst_3prong_up.at(version).GetBinContent(bin);
            } else if ( syst == isofactor_histDict::syst::syst_3prong_down) {
                int bin = isofactor_histDict::h_syst_3prong_down.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_syst_3prong_down.at(version).GetBinContent(bin);
            } else {
                int bin = isofactor_histDict::h_nominal_3prong.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_nominal_3prong.at(version).GetBinContent(bin);
           }
        } else{
            if( syst == isofactor_histDict::syst::stat_1prong_up) {
                int bin = isofactor_histDict::h_stat_1prong_up.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_stat_1prong_up.at(version).GetBinContent(bin);
            } else if ( syst == isofactor_histDict::syst::stat_1prong_down) {
                int bin = isofactor_histDict::h_stat_1prong_down.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_stat_1prong_down.at(version).GetBinContent(bin);
            } else if ( syst == isofactor_histDict::syst::syst_1prong_up) {
                int bin = isofactor_histDict::h_syst_1prong_up.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_syst_1prong_up.at(version).GetBinContent(bin);
            } else if ( syst == isofactor_histDict::syst::syst_1prong_down) {
                int bin = isofactor_histDict::h_syst_1prong_down.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_syst_1prong_down.at(version).GetBinContent(bin);
            } else {
                int bin = isofactor_histDict::h_nominal_1prong.at(version).FindBin(lep_pt, lep_eta);
                return isofactor_histDict::h_nominal_1prong.at(version).GetBinContent(bin);
            }
        }
     } 
  }

} // end the namespace
