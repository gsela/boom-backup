#include <TH1F.h>
#include <TH2D.h>
#include <unordered_map>
#include <iostream>

class hhfake_histDict
{
public:
  // sameSign FFs
  static std::unordered_map<int, TH2D> h_ff_2dobj_1p;
  static std::unordered_map<int, TH2D> h_ff_2dobj_3p;
  static std::unordered_map<int, TH1F> h_ff_2dobj_1p_lead_lnm;
  static std::unordered_map<int, TH1F> h_ff_2dobj_3p_lead_lnm;
  static std::unordered_map<int, TH1F> h_ff_2dobj_1p_sublead_lnm;
  static std::unordered_map<int, TH1F> h_ff_2dobj_3p_sublead_lnm;

  // highDeta FFs
  static std::unordered_map<int, TH2D> h_ff_2dobj_1p_hd_lead;
  static std::unordered_map<int, TH2D> h_ff_2dobj_3p_hd_lead;
  static std::unordered_map<int, TH2D> h_ff_2dobj_1p_hd_lead_lnm;
  static std::unordered_map<int, TH2D> h_ff_2dobj_3p_hd_lead_lnm;

  static std::unordered_map<int, TH2D> h_ff_2dobj_1p_hd_sublead;
  static std::unordered_map<int, TH2D> h_ff_2dobj_3p_hd_sublead;
  static std::unordered_map<int, TH2D> h_ff_2dobj_1p_hd_sublead_lnm;
  static std::unordered_map<int, TH2D> h_ff_2dobj_3p_hd_sublead_lnm;

  // WCR FFs
  static std::unordered_map<int, TH2D> h_ff_2dobj_1p_w;
  static std::unordered_map<int, TH2D> h_ff_2dobj_3p_w;
  static std::unordered_map<int, TH2D> h_ff_2dobj_1p_w_lnm;
  static std::unordered_map<int, TH2D> h_ff_2dobj_3p_w_lnm;

  // parametrization uncertainty weights
  static std::unordered_map<int, TH1F> h_ff_param_weights;


  enum category{ preselection, boost, vbf, vh };
  enum syst{
    nominal,
    hh_fake_ff_stat_1p_nm,
    hh_fake_ff_stat_3p_nm,
    hh_fake_ff_stat_1p_lnm,
    hh_fake_ff_stat_3p_lnm,
    hh_fake_ff_param,
    hh_fake_ff_composition_ss,
    hh_fake_ff_composition_highdeta};

};

std::unordered_map<int, TH1F> createEmptyTH1F()
{
  std::unordered_map<int, TH1F> internalMap;
  return internalMap;
}

std::unordered_map<int, TH1D> createEmptyTH1D()
{
  std::unordered_map<int, TH1D> internalMap;
  return internalMap;
}

std::unordered_map<int, TH2D> createEmptyTH2D()
{
  std::unordered_map<int, TH2D> internalMap;
  return internalMap;
}

// sameSign FF hist initialization
std::unordered_map<int, TH2D> hhfake_histDict::h_ff_2dobj_1p = createEmptyTH2D();
std::unordered_map<int, TH2D> hhfake_histDict::h_ff_2dobj_3p = createEmptyTH2D();
std::unordered_map<int, TH1F> hhfake_histDict::h_ff_2dobj_1p_lead_lnm = createEmptyTH1F();
std::unordered_map<int, TH1F> hhfake_histDict::h_ff_2dobj_3p_lead_lnm = createEmptyTH1F();
std::unordered_map<int, TH1F> hhfake_histDict::h_ff_2dobj_1p_sublead_lnm = createEmptyTH1F();
std::unordered_map<int, TH1F> hhfake_histDict::h_ff_2dobj_3p_sublead_lnm = createEmptyTH1F();

// highDeta FF hist initialization
std::unordered_map<int, TH2D> hhfake_histDict::h_ff_2dobj_1p_hd_lead = createEmptyTH2D();
std::unordered_map<int, TH2D> hhfake_histDict::h_ff_2dobj_3p_hd_lead = createEmptyTH2D();
std::unordered_map<int, TH2D> hhfake_histDict::h_ff_2dobj_1p_hd_lead_lnm = createEmptyTH2D();
std::unordered_map<int, TH2D> hhfake_histDict::h_ff_2dobj_3p_hd_lead_lnm = createEmptyTH2D();

std::unordered_map<int, TH2D> hhfake_histDict::h_ff_2dobj_1p_hd_sublead = createEmptyTH2D();
std::unordered_map<int, TH2D> hhfake_histDict::h_ff_2dobj_3p_hd_sublead = createEmptyTH2D();
std::unordered_map<int, TH2D> hhfake_histDict::h_ff_2dobj_1p_hd_sublead_lnm = createEmptyTH2D();
std::unordered_map<int, TH2D> hhfake_histDict::h_ff_2dobj_3p_hd_sublead_lnm = createEmptyTH2D();

// WCR FF hist initialization
std::unordered_map<int, TH2D> hhfake_histDict::h_ff_2dobj_1p_w = createEmptyTH2D();
std::unordered_map<int, TH2D> hhfake_histDict::h_ff_2dobj_3p_w = createEmptyTH2D();
std::unordered_map<int, TH2D> hhfake_histDict::h_ff_2dobj_1p_w_lnm = createEmptyTH2D();
std::unordered_map<int, TH2D> hhfake_histDict::h_ff_2dobj_3p_w_lnm = createEmptyTH2D();

// Parametrization uncertainty hist initialization
std::unordered_map<int, TH1F> hhfake_histDict::h_ff_param_weights = createEmptyTH1F();

namespace HHFakeSystHelper {
  // helper functions for accessing FF histograms, first sameSign, then highDeta then WCR

  float _read_ff_lnm_lead(float taupt, float taueta, int tauntracks, int version){
    // using tau eta parametrization for now
    if(tauntracks == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p_lead_lnm.at(version).FindBin(taueta);
      return hhfake_histDict::h_ff_2dobj_1p_lead_lnm.at(version).GetBinContent(bin);
    }
    else{
      int bin = hhfake_histDict::h_ff_2dobj_3p_lead_lnm.at(version).FindBin(taueta);
      return hhfake_histDict::h_ff_2dobj_3p_lead_lnm.at(version).GetBinContent(bin);
    }
  }

  float _read_ff_lnm_sublead(float taupt, float taueta, int tauntracks, int version){
    if(tauntracks == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p_sublead_lnm.at(version).FindBin(taueta);
      return hhfake_histDict::h_ff_2dobj_1p_sublead_lnm.at(version).GetBinContent(bin);
    }
    else{
      int bin = hhfake_histDict::h_ff_2dobj_3p_sublead_lnm.at(version).FindBin(taueta);
      return hhfake_histDict::h_ff_2dobj_3p_sublead_lnm.at(version).GetBinContent(bin);
    }
  }

  float _read_ff_nm(float taupt, float taueta, int tauntracks, int version){
    if(tauntracks == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p.at(version).FindBin(taupt, taueta);
      return hhfake_histDict::h_ff_2dobj_1p.at(version).GetBinContent(bin);
    }
    else{
      int bin = hhfake_histDict::h_ff_2dobj_3p.at(version).FindBin(taupt, taueta);
      return hhfake_histDict::h_ff_2dobj_3p.at(version).GetBinContent(bin);
    }
  }

  // copy of previous functions, but accessing high-deta FFs
  float _read_ff_hd_lead_nm(float taupt, float taueta, int tauntracks, int version){
    if(tauntracks == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p_hd_lead.at(version).FindBin(taupt, taueta);
      return hhfake_histDict::h_ff_2dobj_1p_hd_lead.at(version).GetBinContent(bin);
    }
    else{
      int bin = hhfake_histDict::h_ff_2dobj_3p_hd_lead.at(version).FindBin(taupt, taueta);
      return hhfake_histDict::h_ff_2dobj_3p_hd_lead.at(version).GetBinContent(bin);
    }
  }
  float _read_ff_hd_lead_lnm(float taupt, float taueta, int tauntracks, int version){
    if(tauntracks == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p_hd_lead_lnm.at(version).FindBin(taupt, taueta);
      return hhfake_histDict::h_ff_2dobj_1p_hd_lead_lnm.at(version).GetBinContent(bin);
    }
    else{
      int bin = hhfake_histDict::h_ff_2dobj_3p_hd_lead_lnm.at(version).FindBin(taupt, taueta);
      return hhfake_histDict::h_ff_2dobj_3p_hd_lead_lnm.at(version).GetBinContent(bin);
    }
  }

  float _read_ff_hd_sublead_nm(float taupt, float taueta, int tauntracks, int version){
    if(tauntracks == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p_hd_sublead.at(version).FindBin(taupt, taueta);
      return hhfake_histDict::h_ff_2dobj_1p_hd_sublead.at(version).GetBinContent(bin);
    }
    else{
      int bin = hhfake_histDict::h_ff_2dobj_3p_hd_sublead.at(version).FindBin(taupt, taueta);
      return hhfake_histDict::h_ff_2dobj_3p_hd_sublead.at(version).GetBinContent(bin);
    }
  }
  float _read_ff_hd_sublead_lnm(float taupt, float taueta, int tauntracks, int version){
    if(tauntracks == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p_hd_sublead_lnm.at(version).FindBin(taupt, taueta);
      return hhfake_histDict::h_ff_2dobj_1p_hd_sublead_lnm.at(version).GetBinContent(bin);
    }
    else{
      int bin = hhfake_histDict::h_ff_2dobj_3p_hd_sublead_lnm.at(version).FindBin(taupt, taueta);
      return hhfake_histDict::h_ff_2dobj_3p_hd_sublead_lnm.at(version).GetBinContent(bin);
    }
  }

  // and for the FFs from the lephad w region
  float _read_ff_w_nm(float taupt, float taueta, int tauntracks, int version, int syst){
    if(tauntracks == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p_w.at(version).FindBin(taupt, taueta);
      if(syst == hhfake_histDict::syst::hh_fake_ff_stat_1p_nm){
        return hhfake_histDict::h_ff_2dobj_1p_w.at(version).GetBinContent(bin) + hhfake_histDict::h_ff_2dobj_1p_w.at(version).GetBinError(bin);
      }
      else{
        return hhfake_histDict::h_ff_2dobj_1p_w.at(version).GetBinContent(bin);
      }
    }
    else{
      int bin = hhfake_histDict::h_ff_2dobj_3p_w.at(version).FindBin(taupt, taueta);
      if(syst == hhfake_histDict::syst::hh_fake_ff_stat_3p_nm){
        return hhfake_histDict::h_ff_2dobj_3p_w.at(version).GetBinContent(bin) + hhfake_histDict::h_ff_2dobj_3p_w.at(version).GetBinError(bin);
      }
      else{
        return hhfake_histDict::h_ff_2dobj_3p_w.at(version).GetBinContent(bin);
      }
    }
  }

  float _read_ff_w_lnm(float taupt, float taueta, int tauntracks, int version, int syst){
    if(tauntracks == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p_w_lnm.at(version).FindBin(taupt, taueta);
      if(syst == hhfake_histDict::syst::hh_fake_ff_stat_1p_lnm){
        return hhfake_histDict::h_ff_2dobj_1p_w_lnm.at(version).GetBinContent(bin) + hhfake_histDict::h_ff_2dobj_1p_w_lnm.at(version).GetBinError(bin);
      }
      else{
        return hhfake_histDict::h_ff_2dobj_1p_w_lnm.at(version).GetBinContent(bin);
      }
    }
    else{
      int bin = hhfake_histDict::h_ff_2dobj_3p_w_lnm.at(version).FindBin(taupt, taueta);
      if(syst == hhfake_histDict::syst::hh_fake_ff_stat_3p_lnm){
        return hhfake_histDict::h_ff_2dobj_3p_w_lnm.at(version).GetBinContent(bin) + hhfake_histDict::h_ff_2dobj_3p_w_lnm.at(version).GetBinError(bin);
      }
      else{
        return hhfake_histDict::h_ff_2dobj_3p_w_lnm.at(version).GetBinContent(bin);
      }
    }
  }

  // param unc

  float _read_param_unc_weight(float mmc, int version){
    int bin = hhfake_histDict::h_ff_param_weights.at(version).FindBin(mmc);
    return hhfake_histDict::h_ff_param_weights.at(version).GetBinContent(bin);
  }

  // full FF application functions

  float _read_ff_objlvl_single_fake_same_sign(float tau1pt, float tau2pt, float tau1eta, float tau2eta, int tau1ntracks, int tau2ntracks, int tau1id, int tau2id, int version){
    // id: tau_loose_rnn + tau_medium_rnn, so it is 0 for notloose, 1 for loosenotmedium, 2 for medium
    if(tau1id == 1 and tau2id == 0){
      return -.5*_read_ff_lnm_lead(tau1pt, tau1eta, tau1ntracks, version)*_read_ff_nm(tau2pt, tau2eta, tau2ntracks, version); // factor 0.5 to allow for using bigger template
    }
    else if(tau1id == 0 and tau2id == 1){
      return -.5*_read_ff_nm(tau1pt, tau1eta, tau1ntracks, version)*_read_ff_lnm_sublead(tau2pt, tau2eta, tau2ntracks, version); // factor 0.5 to allow for using bigger template
    }
    else if(tau1id == 1 and tau2id == 1){ //loose-loose case, overlap of two templates, need to "double-count" that by adding up previous FF expressions
      return -.5*(_read_ff_nm(tau1pt, tau1eta, tau1ntracks, version)*_read_ff_lnm_sublead(tau2pt, tau2eta, tau2ntracks, version) + _read_ff_lnm_lead(tau1pt, tau1eta, tau1ntracks, version)*_read_ff_nm(tau2pt, tau2eta, tau2ntracks, version)); // factor 0.5 to allow for using bigger template
    }
    else if(tau1id == 2 and tau2id < 2){
      return _read_ff_nm(tau2pt, tau2eta, tau2ntracks, version);
    }
    else if(tau1id < 2 and tau2id == 2){
      return _read_ff_nm(tau1pt, tau1eta, tau1ntracks, version);
    }
    else{
      return 0.;
    }
  }

  float _read_ff_objlvl_single_fake_high_deta(float tau1pt, float tau2pt, float tau1eta, float tau2eta, int tau1ntracks, int tau2ntracks, int tau1id, int tau2id, int version){
    // id: tau_loose_rnn + tau_medium_rnn, so it is 0 for notloose, 1 for loosenotmedium, 2 for medium
    if(tau1id == 1 and tau2id == 0){
      return -.5*_read_ff_hd_lead_lnm(tau1pt, tau1eta, tau1ntracks, version)*_read_ff_hd_sublead_nm(tau2pt, tau2eta, tau2ntracks, version); // factor 0.5 to allow for using bigger template
    }
    else if(tau1id == 0 and tau2id == 1){
      return -.5*_read_ff_hd_lead_nm(tau1pt, tau1eta, tau1ntracks, version)*_read_ff_hd_sublead_lnm(tau2pt, tau2eta, tau2ntracks, version); // factor 0.5 to allow for using bigger template
    }
    else if(tau1id == 1 and tau2id == 1){ //loose-loose case, overlap of two templates, need to "double-count" that by adding up previous FF expressions
      return -.5*(_read_ff_hd_lead_nm(tau1pt, tau1eta, tau1ntracks, version)*_read_ff_hd_sublead_lnm(tau2pt, tau2eta, tau2ntracks, version) + _read_ff_hd_lead_lnm(tau1pt, tau1eta, tau1ntracks, version)*_read_ff_hd_sublead_nm(tau2pt, tau2eta, tau2ntracks, version)); // factor 0.5 to allow for using bigger template
    }
    else if(tau1id == 2 and tau2id < 2){
      return _read_ff_hd_sublead_nm(tau2pt, tau2eta, tau2ntracks, version);
    }
    else if(tau1id < 2 and tau2id == 2){
      return _read_ff_hd_lead_nm(tau1pt, tau1eta, tau1ntracks, version);
    }
    else{
      return 0.;
    }
  }

  float _read_ff_objlvl_single_fake_wcr(float tau1pt, float tau2pt, float tau1eta, float tau2eta, int tau1ntracks, int tau2ntracks, int tau1id, int tau2id, int version, int syst){
    // id: tau_loose_rnn + tau_medium_rnn, so it is 0 for notloose, 1 for loosenotmedium, 2 for medium
    if(tau1id == 1 and tau2id == 0){
      return -.5*_read_ff_w_lnm(tau1pt, tau1eta, tau1ntracks, version, syst)*_read_ff_w_nm(tau2pt, tau2eta, tau2ntracks, version, syst); // factor 0.5 to allow for using bigger template
    }
    else if(tau1id == 0 and tau2id == 1){
      return -.5*_read_ff_w_nm(tau1pt, tau1eta, tau1ntracks, version, syst)*_read_ff_w_lnm(tau2pt, tau2eta, tau2ntracks, version, syst);
    }
    else if(tau1id == 1 and tau2id == 1){ //loose-loose case, overlap of two templates, need to "double-count" that by adding up previous FF expressions
      return -.5*(_read_ff_w_nm(tau1pt, tau1eta, tau1ntracks, version, syst)*_read_ff_w_lnm(tau2pt, tau2eta, tau2ntracks, version, syst) + _read_ff_w_lnm(tau1pt, tau1eta, tau1ntracks, version, syst)*_read_ff_w_nm(tau2pt, tau2eta, tau2ntracks, version, syst));
    }
    else if(tau1id == 2 and tau2id < 2){
      return _read_ff_w_nm(tau2pt, tau2eta, tau2ntracks, version, syst);
    }
    else if(tau1id < 2 and tau2id == 2){
      return _read_ff_w_nm(tau1pt, tau1eta, tau1ntracks, version, syst);
    }
    else{
      return 0.;
    }
  }

  // wrapper function that decides which FFs to apply
  float read_hh_fakefactors(float tau1pt, float tau2pt, float tau1eta, float tau2eta, int tau1ntracks, int tau2ntracks, int tau1id, int tau2id, float mmc, int version, int syst){
    if(syst == hhfake_histDict::syst::hh_fake_ff_composition_ss){
      return _read_ff_objlvl_single_fake_same_sign(tau1pt, tau2pt, tau1eta, tau2eta, tau1ntracks, tau2ntracks, tau1id, tau2id, version);
    }
    else if(syst == hhfake_histDict::syst::hh_fake_ff_composition_highdeta){
      return _read_ff_objlvl_single_fake_high_deta(tau1pt, tau2pt, tau1eta, tau2eta, tau1ntracks, tau2ntracks, tau1id, tau2id, version);
    }
    else{
      float mmcWeight = 1.;
      if(syst == hhfake_histDict::syst::hh_fake_ff_param){
        mmcWeight = _read_param_unc_weight(mmc, version); // insert some function reading a histogram here
      }
      return mmcWeight*_read_ff_objlvl_single_fake_wcr(tau1pt, tau2pt, tau1eta, tau2eta, tau1ntracks, tau2ntracks, tau1id, tau2id, version, syst);
    }
  }
}; // end the namespace
