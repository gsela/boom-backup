#include <TH2F.h>
#include <unordered_map>
#include <iostream>
using std::cout;

class evetofactor_histDict
{
public:
  static std::unordered_map<std::string, TH2F> h_sf_eveto;
  static std::unordered_map<std::string, TH1F> h_sf_ff;
  static std::unordered_map<std::string, TH2D> h_sf_ff_lep;
};

std::unordered_map<std::string, TH2F> createEmptyMapTH2F() 
{
  std::unordered_map<std::string, TH2F> internalMap;
  return internalMap;
}

std::unordered_map<std::string, TH2D> gal_createEmptyMapTH2D() 
{
  std::unordered_map<std::string, TH2D> internalMap;
  return internalMap;
}

std::unordered_map<std::string, TH1F> createEmptyMapTH1F() 
{
  std::unordered_map<std::string, TH1F> internalMap;
  return internalMap;
}

std::unordered_map<std::string, TH2F> evetofactor_histDict::h_sf_eveto   = createEmptyMapTH2F();
std::unordered_map<std::string, TH1F> evetofactor_histDict::h_sf_ff   = createEmptyMapTH1F();
std::unordered_map<std::string, TH2D> evetofactor_histDict::h_sf_ff_lep   = gal_createEmptyMapTH2D();

namespace EvetoFactorHelper {

  float get_evetofactor(float lep_pt, float lep_eta, int nelectrons, int ntracks, int pdgId, int decaymode)
  {
    float weight = 1.0;    
    if(nelectrons==1 && ntracks==1){
      if(std::abs(pdgId)==11){ // request it is an electron
	int binx, biny, nbinxs, nbinys;
        if( decaymode==0 ){
	  binx = evetofactor_histDict::h_sf_eveto["sf_eleBDTMedium_r1p0n"].GetXaxis()->FindBin(lep_pt);
	  biny = evetofactor_histDict::h_sf_eveto["sf_eleBDTMedium_r1p0n"].GetYaxis()->FindBin(std::abs(lep_eta));
	  nbinxs = evetofactor_histDict::h_sf_eveto["sf_eleBDTMedium_r1p0n"].GetNbinsX();
	  nbinys = evetofactor_histDict::h_sf_eveto["sf_eleBDTMedium_r1p0n"].GetNbinsY();
	  if(binx<1) binx = 1;
	  if(binx>nbinxs) binx = nbinxs;
	  if(biny<1) biny = 1;
	  if(biny>nbinys) biny = nbinys;
	  weight =  evetofactor_histDict::h_sf_eveto["sf_eleBDTMedium_r1p0n"].GetBinContent(binx,biny);
        }else if( decaymode==1 ){
	  binx = evetofactor_histDict::h_sf_eveto["sf_eleBDTMedium_r1p1n"].GetXaxis()->FindBin(lep_pt);
	  biny = evetofactor_histDict::h_sf_eveto["sf_eleBDTMedium_r1p1n"].GetYaxis()->FindBin(std::abs(lep_eta));
	  nbinxs = evetofactor_histDict::h_sf_eveto["sf_eleBDTMedium_r1p1n"].GetNbinsX();
	  nbinys = evetofactor_histDict::h_sf_eveto["sf_eleBDTMedium_r1p1n"].GetNbinsY();
	  if(binx<1) binx = 1;
	  if(binx>nbinxs) binx = nbinxs;
	  if(biny<1) biny = 1;
	  if(biny>nbinys) biny = nbinys;
	  weight =  evetofactor_histDict::h_sf_eveto["sf_eleBDTMedium_r1p1n"].GetBinContent(binx,biny);
        }else if( decaymode==2 ){
	  binx = evetofactor_histDict::h_sf_eveto["sf_eleBDTMedium_r1pXn"].GetXaxis()->FindBin(lep_pt);
	  biny = evetofactor_histDict::h_sf_eveto["sf_eleBDTMedium_r1pXn"].GetYaxis()->FindBin(std::abs(lep_eta));
	  nbinxs = evetofactor_histDict::h_sf_eveto["sf_eleBDTMedium_r1pXn"].GetNbinsX();
	  nbinys = evetofactor_histDict::h_sf_eveto["sf_eleBDTMedium_r1pXn"].GetNbinsY();
	  if(binx<1) binx = 1;
	  if(binx>nbinxs) binx = nbinxs;
	  if(biny<1) biny = 1;
	  if(biny>nbinys) biny = nbinys;
	  weight =  evetofactor_histDict::h_sf_eveto["sf_eleBDTMedium_r1pXn"].GetBinContent(binx,biny);
        }
      }
    }
    return weight;
  } 

  float get_anti_tau_nonVBF_ff(float tau_pt, float tau_eta, UInt_t tau_1, UInt_t prongness){

    float RQCD = 1.0, FFQCD = 1.0, FFW = 1.0, weight = 1.0;
	if (tau_pt>199){tau_pt=199;}


      if (tau_1==1){
        RQCD = evetofactor_histDict::h_sf_ff["R_QCDmap_mutau"].GetBinContent(evetofactor_histDict::h_sf_ff["R_QCDmap_mutau"].FindBin(tau_pt));

        if(prongness==1) {
          FFQCD = evetofactor_histDict::h_sf_ff["FF_QCDmap_nonVBF_mu1p"].GetBinContent(evetofactor_histDict::h_sf_ff["FF_QCDmap_nonVBF_mu1p"].FindBin(tau_pt));
		  FFW = evetofactor_histDict::h_sf_ff["FF_Wmap_nonVBF_mu1p"].GetBinContent(evetofactor_histDict::h_sf_ff["FF_Wmap_nonVBF_mu1p"].FindBin(tau_pt));

        } else{
		  FFQCD = evetofactor_histDict::h_sf_ff["FF_QCDmap_nonVBF_mu3p"].GetBinContent(evetofactor_histDict::h_sf_ff["FF_QCDmap_nonVBF_mu3p"].FindBin(tau_pt));
		  FFW = evetofactor_histDict::h_sf_ff["FF_Wmap_nonVBF_mu3p"].GetBinContent(evetofactor_histDict::h_sf_ff["FF_Wmap_nonVBF_mu3p"].FindBin(tau_pt));
        }
		
      } else if (tau_1==2){
		RQCD = evetofactor_histDict::h_sf_ff["R_QCDmap_etau"].GetBinContent(evetofactor_histDict::h_sf_ff["R_QCDmap_etau"].FindBin(tau_pt));

        if(prongness==1) {
		  FFQCD = evetofactor_histDict::h_sf_ff["FF_QCDmap_nonVBF_e1p"].GetBinContent(evetofactor_histDict::h_sf_ff["FF_QCDmap_nonVBF_e1p"].FindBin(tau_pt));
		  FFW = evetofactor_histDict::h_sf_ff["FF_Wmap_nonVBF_e1p"].GetBinContent(evetofactor_histDict::h_sf_ff["FF_Wmap_nonVBF_e1p"].FindBin(tau_pt));

        } else{
          FFQCD = evetofactor_histDict::h_sf_ff["FF_QCDmap_nonVBF_e3p"].GetBinContent(evetofactor_histDict::h_sf_ff["FF_QCDmap_nonVBF_e3p"].FindBin(tau_pt));
		  FFW = evetofactor_histDict::h_sf_ff["FF_Wmap_nonVBF_e3p"].GetBinContent(evetofactor_histDict::h_sf_ff["FF_Wmap_nonVBF_e3p"].FindBin(tau_pt));
        }

      } 

      weight = RQCD*FFQCD + (1-RQCD)*FFW;
      //weight = RQCD*FFQCD;
      
    return weight;
  }

  double get_anti_lep_ff(float lep_pt, float lep_eta, UInt_t tau_1){

    double weight = 1.0;
    int binx, biny;
	if (lep_pt>999){lep_pt=999;}

      if (tau_1==1){
        binx = evetofactor_histDict::h_sf_ff_lep["FF_lep_mu_map"].GetXaxis()->FindBin(lep_pt);
        biny = evetofactor_histDict::h_sf_ff_lep["FF_lep_mu_map"].GetYaxis()->FindBin(std::abs(lep_eta));
        weight =  evetofactor_histDict::h_sf_ff_lep["FF_lep_mu_map"].GetBinContent(binx,biny);

      } else if (tau_1==2){
		    binx = evetofactor_histDict::h_sf_ff_lep["FF_lep_el_map"].GetXaxis()->FindBin(lep_pt);
        biny = evetofactor_histDict::h_sf_ff_lep["FF_lep_el_map"].GetYaxis()->FindBin(std::abs(lep_eta));
        weight =  evetofactor_histDict::h_sf_ff_lep["FF_lep_el_map"].GetBinContent(binx,biny);

      } 

    return weight;
  }

} // end the namespace
