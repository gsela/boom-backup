#include <TH1D.h>
#include <TH2D.h>
#include <unordered_map>

class lhfake_histDict
{
public:
  static std::unordered_map<int, TH1D> h_ff_qcd_1p;
  static std::unordered_map<int, TH1D> h_ff_qcd_3p;
  static std::unordered_map<int, TH1D> h_ff_wcr_1p;
  static std::unordered_map<int, TH1D> h_ff_wcr_3p;
  //static std::unordered_map<int, TH1D> h_ff_top_1p;
  //static std::unordered_map<int, TH1D> h_ff_top_3p;
  static std::unordered_map<int, TH2D> h_rqcd_1p;
  static std::unordered_map<int, TH2D> h_rqcd_3p;

  // histograms for stat/syst uncertainties
  static std::unordered_map<int, TH1D> h_ff_qcd_1p_stat;
  static std::unordered_map<int, TH1D> h_ff_qcd_3p_stat;
  static std::unordered_map<int, TH1D> h_ff_wcr_1p_stat;
  static std::unordered_map<int, TH1D> h_ff_wcr_3p_stat;
  static std::unordered_map<int, TH2D> h_rqcd_1p_stat;
  static std::unordered_map<int, TH2D> h_rqcd_3p_stat;
  static std::unordered_map<int, TH2D> h_rqcd_1p_isofac_stat;
  static std::unordered_map<int, TH2D> h_rqcd_3p_isofac_stat;
  static std::unordered_map<int, TH2D> h_rqcd_1p_isofac_syst;
  static std::unordered_map<int, TH2D> h_rqcd_3p_isofac_syst;
 
  // histograms for mc subtraction uncertainties 
  static std::unordered_map<int, TH1D> h_ff_qcd_1p_mc_subtr;
  static std::unordered_map<int, TH1D> h_ff_qcd_3p_mc_subtr;
  static std::unordered_map<int, TH1D> h_ff_wcr_1p_mc_subtr;
  static std::unordered_map<int, TH1D> h_ff_wcr_3p_mc_subtr;
  static std::unordered_map<int, TH2D> h_rqcd_1p_mc_subtr;
  static std::unordered_map<int, TH2D> h_rqcd_3p_mc_subtr;

  // histograms for closure tests
  static std::unordered_map<int, TH1D> h_ff_qcd_1p_closure;
  static std::unordered_map<int, TH1D> h_ff_qcd_3p_closure;
  static std::unordered_map<int, TH1D> h_ff_wcr_1p_closure;
  static std::unordered_map<int, TH1D> h_ff_wcr_3p_closure;
  //static std::unordered_map<int, TH1D> h_ff_top_1p_closure;
  //static std::unordered_map<int, TH1D> h_ff_top_3p_closure;
  static std::unordered_map<int, TH2D> h_rqcd_1p_closure;
  static std::unordered_map<int, TH2D> h_rqcd_3p_closure;

  // histograms for closure tests
  static std::unordered_map<int, TH1F> h_fake_closure_1p;
  static std::unordered_map<int, TH1F> h_fake_closure_3p;

  // systematics for closure tests
  enum syst{ 
    nominal, 
    lh_fake_FF_QCD_1prong_stat, 
    lh_fake_FF_W_1prong_stat, 
    lh_fake_RQCD_1prong_stat, 
    lh_fake_RQCD_1prong_isofac_stat, 
    lh_fake_RQCD_1prong_isofac_syst,     
    lh_fake_FF_QCD_3prong_stat, 
    lh_fake_FF_W_3prong_stat, 
    lh_fake_RQCD_3prong_stat, 
    lh_fake_RQCD_3prong_isofac_stat, 
    lh_fake_RQCD_3prong_isofac_syst,
    lh_fake_closure_1prong, 
    lh_fake_closure_3prong,
    lh_fake_mc_subtr_1prong,
    lh_fake_mc_subtr_3prong};
     
};


std::unordered_map<int, TH1D> createEmptyMap1D() 
{
  std::unordered_map<int, TH1D> internalMap;
  return internalMap;
}

std::unordered_map<int, TH1F> createEmptyMap1F()
{
  std::unordered_map<int, TH1F> internalMap;
  return internalMap;
}

std::unordered_map<int, TH2D> createEmptyMap2D() 
{
  std::unordered_map<int, TH2D> internalMap;
  return internalMap;
}

std::unordered_map<int, TH1D> lhfake_histDict::h_ff_qcd_1p = createEmptyMap1D();
std::unordered_map<int, TH1D> lhfake_histDict::h_ff_qcd_3p = createEmptyMap1D();
std::unordered_map<int, TH1D> lhfake_histDict::h_ff_wcr_1p = createEmptyMap1D();
std::unordered_map<int, TH1D> lhfake_histDict::h_ff_wcr_3p = createEmptyMap1D();
//std::unordered_map<int, TH1D> histDict::h_ff_top_1p = createEmptyMap1D();
//std::unordered_map<int, TH1D> histDict::h_ff_top_3p = createEmptyMap1D();
std::unordered_map<int, TH2D> lhfake_histDict::h_rqcd_1p = createEmptyMap2D();
std::unordered_map<int, TH2D> lhfake_histDict::h_rqcd_3p = createEmptyMap2D();

std::unordered_map<int, TH1D> lhfake_histDict::h_ff_qcd_1p_stat = createEmptyMap1D();
std::unordered_map<int, TH1D> lhfake_histDict::h_ff_qcd_3p_stat = createEmptyMap1D();
std::unordered_map<int, TH1D> lhfake_histDict::h_ff_wcr_1p_stat = createEmptyMap1D();
std::unordered_map<int, TH1D> lhfake_histDict::h_ff_wcr_3p_stat = createEmptyMap1D();
std::unordered_map<int, TH2D> lhfake_histDict::h_rqcd_1p_stat = createEmptyMap2D();
std::unordered_map<int, TH2D> lhfake_histDict::h_rqcd_3p_stat = createEmptyMap2D();
std::unordered_map<int, TH2D> lhfake_histDict::h_rqcd_1p_isofac_stat = createEmptyMap2D();
std::unordered_map<int, TH2D> lhfake_histDict::h_rqcd_3p_isofac_stat = createEmptyMap2D();
std::unordered_map<int, TH2D> lhfake_histDict::h_rqcd_1p_isofac_syst = createEmptyMap2D();
std::unordered_map<int, TH2D> lhfake_histDict::h_rqcd_3p_isofac_syst = createEmptyMap2D();

std::unordered_map<int, TH1D> lhfake_histDict::h_ff_qcd_1p_mc_subtr = createEmptyMap1D();
std::unordered_map<int, TH1D> lhfake_histDict::h_ff_qcd_3p_mc_subtr = createEmptyMap1D();
std::unordered_map<int, TH1D> lhfake_histDict::h_ff_wcr_1p_mc_subtr = createEmptyMap1D();
std::unordered_map<int, TH1D> lhfake_histDict::h_ff_wcr_3p_mc_subtr = createEmptyMap1D();
std::unordered_map<int, TH2D> lhfake_histDict::h_rqcd_1p_mc_subtr = createEmptyMap2D();
std::unordered_map<int, TH2D> lhfake_histDict::h_rqcd_3p_mc_subtr = createEmptyMap2D();

std::unordered_map<int, TH1D> lhfake_histDict::h_ff_qcd_1p_closure = createEmptyMap1D();
std::unordered_map<int, TH1D> lhfake_histDict::h_ff_qcd_3p_closure = createEmptyMap1D();
std::unordered_map<int, TH1D> lhfake_histDict::h_ff_wcr_1p_closure = createEmptyMap1D();
std::unordered_map<int, TH1D> lhfake_histDict::h_ff_wcr_3p_closure = createEmptyMap1D();
//std::unordered_map<int, TH1D> histDict::h_ff_top_1p_closure = createEmptyMap1D();
//std::unordered_map<int, TH1D> histDict::h_ff_top_3p_closure = createEmptyMap1D();
std::unordered_map<int, TH2D> lhfake_histDict::h_rqcd_1p_closure = createEmptyMap2D();
std::unordered_map<int, TH2D> lhfake_histDict::h_rqcd_3p_closure = createEmptyMap2D();

std::unordered_map<int, TH1F> lhfake_histDict::h_fake_closure_1p = createEmptyMap1F();
std::unordered_map<int, TH1F> lhfake_histDict::h_fake_closure_3p = createEmptyMap1F();

namespace FakeHelper {

  // -------------------------------------------------
  float read_ff_qcd(float pt, int ntracks, int version, int syst, int closure) 
  {
    if( pt > 200){
       pt = 190;
    }

    if (ntracks > 1) {
      if(closure) {
         int bin_x = lhfake_histDict::h_ff_qcd_3p_closure.at(version).FindBin(pt);
         return lhfake_histDict::h_ff_qcd_3p_closure.at(version).GetBinContent(bin_x);
      }
      else {
         if( syst == lhfake_histDict::syst::lh_fake_FF_QCD_3prong_stat) {
             int bin_x = lhfake_histDict::h_ff_qcd_3p_stat.at(version).FindBin(pt);
             return lhfake_histDict::h_ff_qcd_3p_stat.at(version).GetBinContent(bin_x);
         } else if( syst == lhfake_histDict::syst::lh_fake_mc_subtr_3prong){ 
             int bin_x = lhfake_histDict::h_ff_qcd_3p_mc_subtr.at(version).FindBin(pt);
             return lhfake_histDict::h_ff_qcd_3p_mc_subtr.at(version).GetBinContent(bin_x);
         } else{
             int bin_x = lhfake_histDict::h_ff_qcd_3p.at(version).FindBin(pt);
             return lhfake_histDict::h_ff_qcd_3p.at(version).GetBinContent(bin_x);
         }
      }
    } else {
      if(closure) {
         int bin_x = lhfake_histDict::h_ff_qcd_1p_closure.at(version).FindBin(pt);
         return lhfake_histDict::h_ff_qcd_1p_closure.at(version).GetBinContent(bin_x);
      }
      else {
         if( syst == lhfake_histDict::syst::lh_fake_FF_QCD_1prong_stat) {
             int bin_x = lhfake_histDict::h_ff_qcd_1p_stat.at(version).FindBin(pt);
             return lhfake_histDict::h_ff_qcd_1p_stat.at(version).GetBinContent(bin_x);
         } else if( syst == lhfake_histDict::syst::lh_fake_mc_subtr_1prong){
             int bin_x = lhfake_histDict::h_ff_qcd_1p_mc_subtr.at(version).FindBin(pt);
             return lhfake_histDict::h_ff_qcd_1p_mc_subtr.at(version).GetBinContent(bin_x);
         } else{
             int bin_x = lhfake_histDict::h_ff_qcd_1p.at(version).FindBin(pt);
             return lhfake_histDict::h_ff_qcd_1p.at(version).GetBinContent(bin_x);
         }
      }
    }
  }

  // ------------------------------------
  float read_ff_wcr(float pt, int ntracks, int version, int syst, int closure) 
  {
    if( pt > 200)
        pt = 190;

    if (ntracks > 1) {
      if(closure) {
           int bin_x = lhfake_histDict::h_ff_wcr_3p_closure.at(version).FindBin(pt);
           return lhfake_histDict::h_ff_wcr_3p_closure.at(version).GetBinContent(bin_x);
      } 
      else {
          if( syst == lhfake_histDict::syst::lh_fake_FF_W_3prong_stat) {
              int bin_x = lhfake_histDict::h_ff_wcr_3p_stat.at(version).FindBin(pt);
              return lhfake_histDict::h_ff_wcr_3p_stat.at(version).GetBinContent(bin_x);
           } else if( syst == lhfake_histDict::syst::lh_fake_mc_subtr_3prong){
              int bin_x = lhfake_histDict::h_ff_wcr_3p_mc_subtr.at(version).FindBin(pt);
              return lhfake_histDict::h_ff_wcr_3p_mc_subtr.at(version).GetBinContent(bin_x); 
           } else {
              int bin_x = lhfake_histDict::h_ff_wcr_3p.at(version).FindBin(pt);
              return lhfake_histDict::h_ff_wcr_3p.at(version).GetBinContent(bin_x);
           }
      }
    } else {
      if(closure) {
           int bin_x = lhfake_histDict::h_ff_wcr_1p_closure.at(version).FindBin(pt);
           return lhfake_histDict::h_ff_wcr_1p_closure.at(version).GetBinContent(bin_x);
      }
      else {
          if( syst == lhfake_histDict::syst::lh_fake_FF_W_1prong_stat) {
              int bin_x = lhfake_histDict::h_ff_wcr_1p_stat.at(version).FindBin(pt);
              return lhfake_histDict::h_ff_wcr_1p_stat.at(version).GetBinContent(bin_x);
          } else if( syst == lhfake_histDict::syst::lh_fake_mc_subtr_1prong){
              int bin_x = lhfake_histDict::h_ff_wcr_1p_mc_subtr.at(version).FindBin(pt);
              return lhfake_histDict::h_ff_wcr_1p_mc_subtr.at(version).GetBinContent(bin_x);
          } else {
              int bin_x = lhfake_histDict::h_ff_wcr_1p.at(version).FindBin(pt);
              return lhfake_histDict::h_ff_wcr_1p.at(version).GetBinContent(bin_x);
          }
      }
    }
  }

  // -------------------------------------------------
  /*float read_ff_top(float pt, int ntracks, int version) 
  {
    if (ntracks > 1) {
      int bin_x = histDict::h_ff_top_3p.at(version).FindBin(pt);
      return histDict::h_ff_top_3p.at(version).GetBinContent(bin_x);
    } else {
      int bin_x = histDict::h_ff_top_1p.at(version).FindBin(pt);
      return histDict::h_ff_top_1p.at(version).GetBinContent(bin_x);
    }
  }
  */
  // ---------------------------------------------------------------
  float rqcd(float tau_pt, int ntracks, float tau_phi, float met_phi, int version, int syst, int closure) 
  {
    if(tau_pt > 200) 
        tau_pt = 190; 

    float _x = tau_pt;
    float _y = 3.1415 - fabs(fabs(tau_phi - met_phi) - 3.1415);

    if (ntracks > 1) {
        if(closure){
            int bin = lhfake_histDict::h_rqcd_3p_closure.at(version).FindBin(_x, _y);
            return lhfake_histDict::h_rqcd_3p_closure.at(version).GetBinContent(bin);
        }
        else {
            if( syst == lhfake_histDict::syst::lh_fake_RQCD_3prong_stat) {
                int bin = lhfake_histDict::h_rqcd_3p_stat.at(version).FindBin(_x, _y);
                return lhfake_histDict::h_rqcd_3p_stat.at(version).GetBinContent(bin);
            } 
            else if ( syst == lhfake_histDict::syst::lh_fake_RQCD_3prong_isofac_stat) {
                int bin = lhfake_histDict::h_rqcd_3p_isofac_stat.at(version).FindBin(_x, _y);
                return lhfake_histDict::h_rqcd_3p_isofac_stat.at(version).GetBinContent(bin);
            }
            else if ( syst == lhfake_histDict::syst::lh_fake_RQCD_3prong_isofac_syst) {
                int bin = lhfake_histDict::h_rqcd_3p_isofac_syst.at(version).FindBin(_x, _y);
                return lhfake_histDict::h_rqcd_3p_isofac_syst.at(version).GetBinContent(bin);
            }
            else if ( syst == lhfake_histDict::syst::lh_fake_mc_subtr_3prong){
                int bin = lhfake_histDict::h_rqcd_3p_mc_subtr.at(version).FindBin(_x, _y);
                return lhfake_histDict::h_rqcd_3p_mc_subtr.at(version).GetBinContent(bin);
            }
            else {
                int bin = lhfake_histDict::h_rqcd_3p.at(version).FindBin(_x, _y);
                return lhfake_histDict::h_rqcd_3p.at(version).GetBinContent(bin);
            }
        }     
    } else {
        if(closure){
            int bin = lhfake_histDict::h_rqcd_1p_closure.at(version).FindBin(_x, _y);
            return lhfake_histDict::h_rqcd_1p_closure.at(version).GetBinContent(bin);
        }
        else{
            if( syst == lhfake_histDict::syst::lh_fake_RQCD_1prong_stat) {
                int bin = lhfake_histDict::h_rqcd_1p_stat.at(version).FindBin(_x, _y);
                return lhfake_histDict::h_rqcd_1p_stat.at(version).GetBinContent(bin);
            } 
            else if ( syst == lhfake_histDict::syst::lh_fake_RQCD_1prong_isofac_stat) {
                int bin = lhfake_histDict::h_rqcd_1p_isofac_stat.at(version).FindBin(_x, _y);
                return lhfake_histDict::h_rqcd_1p_isofac_stat.at(version).GetBinContent(bin);
            }
            else if ( syst == lhfake_histDict::syst::lh_fake_RQCD_1prong_isofac_syst) {
                int bin = lhfake_histDict::h_rqcd_1p_isofac_syst.at(version).FindBin(_x, _y);
                return lhfake_histDict::h_rqcd_1p_isofac_syst.at(version).GetBinContent(bin);
            }
            else if ( syst == lhfake_histDict::syst::lh_fake_mc_subtr_1prong){
                int bin = lhfake_histDict::h_rqcd_1p_mc_subtr.at(version).FindBin(_x, _y);
                return lhfake_histDict::h_rqcd_1p_mc_subtr.at(version).GetBinContent(bin);
            }
            else{
                int bin = lhfake_histDict::h_rqcd_1p.at(version).FindBin(_x, _y);
                return lhfake_histDict::h_rqcd_1p.at(version).GetBinContent(bin);
            }
        }
     }
  }

  //-----------------------
  float weight(float tau_pt, 
	       int tau_ntracks, 
	       float tau_phi, 
	       float lep_pt, 
	       float lep_eta, 
	       float met_phi, 
	       int phase_space,
	       int syst,
               int closure)
  {
    float corr = rqcd(tau_pt, tau_ntracks, tau_phi, met_phi, phase_space, syst, closure);

    float ff_qcd = read_ff_qcd(tau_pt, tau_ntracks, phase_space, syst, closure);
    float ff_wcr = read_ff_wcr(tau_pt, tau_ntracks, phase_space, syst, closure);

    float final_weight = corr * ff_qcd + (1 - corr) * ff_wcr;
    return final_weight;
  }
  

  // get the syst weight for closure test
  float closuresyst( float mmc,
                     int tau_ntracks,  
                     int phase_space,
                     int syst)
  {
     if( tau_ntracks > 1 && syst == lhfake_histDict::syst::lh_fake_closure_3prong) {
          int bin = lhfake_histDict::h_fake_closure_3p.at(phase_space).FindBin(mmc);
          return lhfake_histDict::h_fake_closure_3p.at(phase_space).GetBinContent(bin);
     }
     else if ( tau_ntracks == 1 && syst == lhfake_histDict::syst::lh_fake_closure_1prong) {
          int bin = lhfake_histDict::h_fake_closure_1p.at(phase_space).FindBin(mmc);
          return lhfake_histDict::h_fake_closure_1p.at(phase_space).GetBinContent(bin);
     }
     else {
          return 1;
     }
  }


} // end the namespace
