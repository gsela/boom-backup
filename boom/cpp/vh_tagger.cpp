#include <TLorentzVector.h>
#include <TMVA/Reader.h>

////////////////
class mvaDict_vh
{
public:
  static TMVA::Reader* reader_vh_all_trained_above_05_tested_below_05;
  static TMVA::Reader* reader_vh_all_trained_below_05_tested_above_05;

};

TMVA::Reader* mvaDict_vh::reader_vh_all_trained_above_05_tested_below_05 = new TMVA::Reader();
TMVA::Reader* mvaDict_vh::reader_vh_all_trained_below_05_tested_above_05 = new TMVA::Reader();



namespace VH_Tagger {

  enum Channel
    {
      hh, // 0,
      lh, // 1
      ll,
    };

  float vh_bdt_score(float jet_0_pt,
		     float jet_0_eta,
		     float jet_0_phi,
		     float jet_0_m,
		     float jet_1_pt,
		     float jet_1_eta,
		     float jet_1_phi,
		     float jet_1_m,
		     float ditau_pt,
		     float ditau_eta,
		     float ditau_phi,
		     float ditau_m,
		     float met,
		     float ditau_higgspt,
		     float mva_random_number)
  {

    TLorentzVector jet_0_p4;
    TLorentzVector jet_1_p4;
    jet_0_p4.SetPtEtaPhiM(jet_0_pt, jet_0_eta, jet_0_phi, jet_0_m);
    jet_1_p4.SetPtEtaPhiM(jet_1_pt, jet_1_eta, jet_1_phi, jet_1_m);
    TLorentzVector dijet_p4 = jet_0_p4 + jet_1_p4;

    TLorentzVector ditau_p4;
    ditau_p4.SetPtEtaPhiM(ditau_pt, ditau_eta, ditau_phi, ditau_m);

    std::vector<double> inputs;
    // dijet_m
    // dr_dijet_ditau
    // dijet_pt
    // ditau_higgs_pt / dijet_pt
    // dijet_deta
    // met
    // ditau_dr
    // dijet_dr
    // pt_total
    inputs.push_back(dijet_p4.M());
    inputs.push_back(dijet_p4.DeltaR(ditau_p4));
    inputs.push_back(ditau_higgspt);
    inputs.push_back(dijet_p4.Pt());
    inputs.push_back(dijet_p4.Pt() == 0. ? 0. : ditau_higgspt / dijet_p4.Pt());
    inputs.push_back(met);
    inputs.push_back(jet_0_p4.DeltaR(jet_1_p4));
    inputs.push_back(fabs(jet_0_eta - jet_1_eta));

    if (mva_random_number <= 0.5) {
      return  mvaDict_vh::reader_vh_all_trained_above_05_tested_below_05->EvaluateMVA(inputs, "BDT");
    } else {
      return  mvaDict_vh::reader_vh_all_trained_below_05_tested_above_05->EvaluateMVA(inputs, "BDT");
    }
    
}		   
		     


		     


}// end the namespace
