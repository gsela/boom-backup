#include <TH2F.h>
#include <unordered_map>

class ztheo_histDict
{
public:
  static std::unordered_map<int, TH2F> h_qsf;
  static std::unordered_map<int, TH2F> h_ckkw;
 
  enum syst{ qsf, ckkw };
  enum channel { leplep, lephad, hadhad };

};

std::unordered_map<int, TH2F> createEmptyTH2F()
{
  std::unordered_map<int, TH2F> internalMap;
  return internalMap;
}

std::unordered_map<int, TH2F> ztheo_histDict::h_qsf   = createEmptyTH2F();
std::unordered_map<int, TH2F> ztheo_histDict::h_ckkw  = createEmptyTH2F();

namespace ZTheoSystHelper {

  float theo_weight(float Z_pt, unsigned int n_jets, int syst, int channel, int version)
  {  

    // correct the number of jets for hadhad and lephad
    if(channel == ztheo_histDict::channel::hadhad){
      n_jets = n_jets -2;
    } else if( channel == ztheo_histDict::channel::lephad){
      n_jets = n_jets -1;
    }

    if( syst == ztheo_histDict::syst::qsf){
      int bin = ztheo_histDict::h_qsf.at(version).FindBin(Z_pt, n_jets);
      return ztheo_histDict::h_qsf.at(version).GetBinContent(bin);
    } else{
      int bin = ztheo_histDict::h_ckkw.at(version).FindBin(Z_pt, n_jets);
      return ztheo_histDict::h_ckkw.at(version).GetBinContent(bin);
    }
  }

}; // end the namespace
