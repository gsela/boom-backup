#include <TH1D.h>
#include <TFile.h>
#include <TRandom3.h>
#include <TVector2.h>
#include <TLorentzVector.h>
#include <TEfficiency.h>
#include <unordered_map>
#include <algorithm>
#include <string>
#include <vector>
#include <ostream>
#include <sstream>
#include <iostream>
#include <map>


class histPtDict
{
public:
  static std::unordered_map<int, TH1D> h_pt_corr_h;
  static std::unordered_map<int, TH1D> h_pt_corr_h_eveto;
  static std::unordered_map<int, TH1D> h_pt_corr_e;
  static std::unordered_map<int, TH1D> h_pt_corr_mu;
  static std::vector<TLorentzVector> m_taus;
  static std::map<std::string, TEfficiency> efficiencies;
//   static std::unordered_map<int, TEfficiency> efficiencies;
  static std::vector<int> m_taus_type;
  static int m_currentEvent;
  static int m_passEvents;
  static int n_oversampling;
  static float m_leplep_frac;
  static float m_lephad_frac;
};

std::unordered_map<int, TH1D> createEmptyMapPt1D() 
{
  std::unordered_map<int, TH1D> internalMap;
  return internalMap;
}

std::vector<TLorentzVector> createEmptyVectors()
{
  std::vector<TLorentzVector> internalVector;
  return internalVector;
}

std::vector<int> createEmptyIntegers()
{
  std::vector<int> internalVector;
  return internalVector;
}

// std::unordered_map<int, TEfficiency> createEmptyEfficiency()
// {
//   std::unordered_map<int, TEfficiency> internalEfficiencies;
//   return internalEfficiencies;
// }

std::map<std::string, TEfficiency> createEmptyEfficiencyMap()
{
  std::map<std::string, TEfficiency> internalEfficiencies;
  return internalEfficiencies;
}

std::unordered_map<int, TH1D> histPtDict::h_pt_corr_h = createEmptyMapPt1D();
std::unordered_map<int, TH1D> histPtDict::h_pt_corr_h_eveto = createEmptyMapPt1D();
std::unordered_map<int, TH1D> histPtDict::h_pt_corr_e = createEmptyMapPt1D();
std::unordered_map<int, TH1D> histPtDict::h_pt_corr_mu = createEmptyMapPt1D();
std::vector<TLorentzVector> histPtDict::m_taus = createEmptyVectors();
std::vector<int> histPtDict::m_taus_type = createEmptyIntegers();
int histPtDict::m_currentEvent = -1;
int histPtDict::m_passEvents = 0;
int histPtDict::n_oversampling = 10;
float histPtDict::m_leplep_frac = 0.333;
float histPtDict::m_lephad_frac = 0.333;

// std::unordered_map<int, TEfficiency> histPtDict::efficiencies = createEmptyEfficiency();
std::map<std::string, TEfficiency> histPtDict::efficiencies = createEmptyEfficiencyMap();

namespace PtCorrHelper {
  
  double manual_met( double tau_0_pt,double tau_0_eta,double tau_0_phi,
  double tau_1_pt,double tau_1_eta,double tau_1_phi,
  double jet_0_pt,double jet_0_eta,double jet_0_phi,
  double jet_1_pt,double jet_1_eta,double jet_1_phi,
  double jet_2_pt,double jet_2_eta,double jet_2_phi, int n_jets)

  {
    TLorentzVector tau_0, tau_1, jet_0, jet_1, jet_2, met;
    tau_0.SetPtEtaPhiM(tau_0_pt, tau_0_eta, tau_0_phi, 0);
    tau_1.SetPtEtaPhiM(tau_1_pt, tau_1_eta, tau_1_phi, 0);
    jet_0.SetPtEtaPhiM(jet_0_pt, jet_0_eta, jet_0_phi, 0);
    jet_1.SetPtEtaPhiM(jet_1_pt, jet_1_eta, jet_1_phi, 0);
    jet_2.SetPtEtaPhiM(jet_2_pt, jet_2_eta, jet_2_phi, 0);

    met=tau_0+tau_1;
    if (n_jets==0) {}
    else if (n_jets==1) {
      met=met+jet_0;
    } else if (n_jets==2) {
      met=met+jet_0+jet_1;
    } else {
      met=met+jet_0+jet_1+jet_2;
    }

    met=-met;

    return met.Pt();
  };

  double manual_met_phi( double tau_0_pt,double tau_0_eta,double tau_0_phi,
  double tau_1_pt,double tau_1_eta,double tau_1_phi,
  double jet_0_pt,double jet_0_eta,double jet_0_phi,
  double jet_1_pt,double jet_1_eta,double jet_1_phi,
  double jet_2_pt,double jet_2_eta,double jet_2_phi, int n_jets)

  {
    TLorentzVector tau_0, tau_1, jet_0, jet_1, jet_2, met;
    tau_0.SetPtEtaPhiM(tau_0_pt, tau_0_eta, tau_0_phi, 0);
    tau_1.SetPtEtaPhiM(tau_1_pt, tau_1_eta, tau_1_phi, 0);
    jet_0.SetPtEtaPhiM(jet_0_pt, jet_0_eta, jet_0_phi, 0);
    jet_1.SetPtEtaPhiM(jet_1_pt, jet_1_eta, jet_1_phi, 0);
    jet_2.SetPtEtaPhiM(jet_2_pt, jet_2_eta, jet_2_phi, 0);

    met=tau_0+tau_1;
    if (n_jets==0) {}
    else if (n_jets==1) {
      met=met+jet_0;
    } else if (n_jets==2) {
      met=met+jet_0+jet_1;
    } else {
      met=met+jet_0+jet_1+jet_2;
    }
    met=-met;
    return met.Phi();
  };

  double manual_met_cos( double tau_0_pt,double tau_0_eta,double tau_0_phi,
  double tau_1_pt,double tau_1_eta,double tau_1_phi,
  double jet_0_pt,double jet_0_eta,double jet_0_phi,
  double jet_1_pt,double jet_1_eta,double jet_1_phi,
  double jet_2_pt,double jet_2_eta,double jet_2_phi, int n_jets)

  {
    TLorentzVector tau_0, tau_1, jet_0, jet_1, jet_2, met;
    double dphi_cos;
    tau_0.SetPtEtaPhiM(tau_0_pt, tau_0_eta, tau_0_phi, 0);
    tau_1.SetPtEtaPhiM(tau_1_pt, tau_1_eta, tau_1_phi, 0);
    jet_0.SetPtEtaPhiM(jet_0_pt, jet_0_eta, jet_0_phi, 0);
    jet_1.SetPtEtaPhiM(jet_1_pt, jet_1_eta, jet_1_phi, 0);
    jet_2.SetPtEtaPhiM(jet_2_pt, jet_2_eta, jet_2_phi, 0);

    met=tau_0+tau_1;
    if (n_jets==0) {}
    else if (n_jets==1) {
      met=met+jet_0;
    } else if (n_jets==2) {
      met=met+jet_0+jet_1;
    } else {
      met=met+jet_0+jet_1+jet_2;
    }
    met=-met;

    dphi_cos=cos(tau_1_phi - met.Phi())+cos(tau_0_phi - met.Phi());

    return dphi_cos;
  };

  double manual_met_coll( double tau_0_pt,double tau_0_eta,double tau_0_phi,
  double tau_1_pt,double tau_1_eta,double tau_1_phi,
  double jet_0_pt,double jet_0_eta,double jet_0_phi,
  double jet_1_pt,double jet_1_eta,double jet_1_phi,
  double jet_2_pt,double jet_2_eta,double jet_2_phi, int n_jets,
  double deta, double dphi)

  {
    TLorentzVector tau_0, tau_1, jet_0, jet_1, jet_2, met;
    double m_coll;
    tau_0.SetPtEtaPhiM(tau_0_pt, tau_0_eta, tau_0_phi, 0);
    tau_1.SetPtEtaPhiM(tau_1_pt, tau_1_eta, tau_1_phi, 0);
    jet_0.SetPtEtaPhiM(jet_0_pt, jet_0_eta, jet_0_phi, 0);
    jet_1.SetPtEtaPhiM(jet_1_pt, jet_1_eta, jet_1_phi, 0);
    jet_2.SetPtEtaPhiM(jet_2_pt, jet_2_eta, jet_2_phi, 0);

    met=tau_0+tau_1;
    if (n_jets==0) {}
    else if (n_jets==1) {
      met=met+jet_0;
    } else if (n_jets==2) {
      met=met+jet_0+jet_1;
    } else {
      met=met+jet_0+jet_1+jet_2;
    }
    met=-met;
    m_coll=sqrt(2 * tau_1_pt * ( met.Pt() + tau_0_pt) * (cosh(deta) - cos(dphi)));


    return m_coll;
  };

  double manual_met_trans_lep( double tau_0_pt,double tau_0_eta,double tau_0_phi,
  double tau_1_pt,double tau_1_eta,double tau_1_phi,
  double jet_0_pt,double jet_0_eta,double jet_0_phi,
  double jet_1_pt,double jet_1_eta,double jet_1_phi,
  double jet_2_pt,double jet_2_eta,double jet_2_phi, int n_jets)

  {
    TLorentzVector tau_0, tau_1, jet_0, jet_1, jet_2, met;
    double m_trans,dphi;
    tau_0.SetPtEtaPhiM(tau_0_pt, tau_0_eta, tau_0_phi, 0);
    tau_1.SetPtEtaPhiM(tau_1_pt, tau_1_eta, tau_1_phi, 0);
    jet_0.SetPtEtaPhiM(jet_0_pt, jet_0_eta, jet_0_phi, 0);
    jet_1.SetPtEtaPhiM(jet_1_pt, jet_1_eta, jet_1_phi, 0);
    jet_2.SetPtEtaPhiM(jet_2_pt, jet_2_eta, jet_2_phi, 0);

    met=tau_0+tau_1;
    if (n_jets==0) {}
    else if (n_jets==1) {
      met=met+jet_0;
    } else if (n_jets==2) {
      met=met+jet_0+jet_1;
    } else {
      met=met+jet_0+jet_1+jet_2;
    }
    met=-met;
    dphi=TVector2::Phi_mpi_pi(tau_1_phi - met.Phi());
    m_trans=sqrt(2 * tau_1_pt * met.Pt() * (1-cos(dphi)));


    return m_trans;
  };

  double manual_met_trans_tau( double tau_0_pt,double tau_0_eta,double tau_0_phi,
  double tau_1_pt,double tau_1_eta,double tau_1_phi,
  double jet_0_pt,double jet_0_eta,double jet_0_phi,
  double jet_1_pt,double jet_1_eta,double jet_1_phi,
  double jet_2_pt,double jet_2_eta,double jet_2_phi, int n_jets)

  {
    TLorentzVector tau_0, tau_1, jet_0, jet_1, jet_2, met;
    double m_trans,dphi;
    tau_0.SetPtEtaPhiM(tau_0_pt, tau_0_eta, tau_0_phi, 0);
    tau_1.SetPtEtaPhiM(tau_1_pt, tau_1_eta, tau_1_phi, 0);
    jet_0.SetPtEtaPhiM(jet_0_pt, jet_0_eta, jet_0_phi, 0);
    jet_1.SetPtEtaPhiM(jet_1_pt, jet_1_eta, jet_1_phi, 0);
    jet_2.SetPtEtaPhiM(jet_2_pt, jet_2_eta, jet_2_phi, 0);

    met=tau_0+tau_1;
    if (n_jets==0) {}
    else if (n_jets==1) {
      met=met+jet_0;
    } else if (n_jets==2) {
      met=met+jet_0+jet_1;
    } else {
      met=met+jet_0+jet_1+jet_2;
    }
    met=-met;
    dphi=TVector2::Phi_mpi_pi(tau_0_phi - met.Phi());
    m_trans=sqrt(2 * tau_0_pt * met.Pt() * (1-cos(dphi)));


    return m_trans;
  };

  double control( double tau_0_pt,double tau_0_eta,double tau_0_phi,
  double tau_1_pt,double tau_1_eta,double tau_1_phi,
  double jet_0_pt,double jet_0_eta,double jet_0_phi,
  double jet_1_pt,double jet_1_eta,double jet_1_phi,
  double jet_2_pt,double jet_2_eta,double jet_2_phi, int n_jets)

  {
    TLorentzVector tau_0, tau_1, jet_0, jet_1, jet_2, met;
    tau_0.SetPtEtaPhiM(tau_0_pt, tau_0_eta, tau_0_phi, 0);
    tau_1.SetPtEtaPhiM(tau_1_pt, tau_1_eta, tau_1_phi, 0);
    jet_0.SetPtEtaPhiM(jet_0_pt, jet_0_eta, jet_0_phi, 0);
    jet_1.SetPtEtaPhiM(jet_1_pt, jet_1_eta, jet_1_phi, 0);
    jet_2.SetPtEtaPhiM(jet_2_pt, jet_2_eta, jet_2_phi, 0);

    met=tau_0+tau_1;
    if (n_jets==0) {}
    else if (n_jets==1) {
      met=met+jet_0;
    } else if (n_jets==2) {
      met=met+jet_0+jet_1;
    } else {
      met=met+jet_0+jet_1+jet_2;
    }
    met=-met;
    return tau_0.Pt();
  };

  enum RecoParticleType {
    none, // = 0
    muon, // 1,
    electron, //2
    tau, // = 3
  };

  enum Channel
    {
      hh, // = 0
      eh, // = 1
      muh, // = 2
      ee, // = 3
      mumu, // = 4
      emu , // = 5
    };

  Channel zll_choose_channel(int event_number)
  {
    TRandom3 generator(event_number);
    float rnd = generator.Rndm();

    // Calculate cut-off points
    float cutoff1 = histPtDict::m_leplep_frac;
    float cutoff2 = cutoff1 + 0.5*(histPtDict::m_lephad_frac);
    float cutoff3 = cutoff1 + histPtDict::m_lephad_frac;

    if(rnd >= 0 && rnd < cutoff1)
      return Channel::emu;
    if(rnd >= cutoff1 && rnd < cutoff2)
      return Channel::eh;
    if(rnd >= cutoff2 && rnd < cutoff3)
      return Channel::muh;
    if(rnd >= cutoff3 && rnd < 1.00)
      return Channel::hh;

    // All else fails, it's had-had :)
    return Channel::hh;
  }
    

  int get_pt_bin(float momentum) {

    std::vector<int> pt;
    pt.push_back(0);
    pt.push_back(30);
    pt.push_back(40);
    pt.push_back(50);
    pt.push_back(60);
    pt.push_back(70);
    pt.push_back(80);
    pt.push_back(100);
    pt.push_back(150);
    pt.push_back(50000);

    int momentum_int = int(momentum);

    std::vector<int>::iterator bin;
    bin = std::upper_bound(pt.begin(), pt.end(), momentum_int);

    int index = bin - pt.begin() - 1;

    // protect against negative bin
    if (index < 0)
      return 0;

    // protect against overflow
    if (index > (int)(pt.size() - 2))
      return pt.size() - 2;

    //int index = 2;
    return index;
  }

  float weight_ele_trig(float tau_0_pt, float tau_0_eta, bool fold)
  {
    float weight = 1;
    if (tau_0_pt >= 26) {
      if (fold) {
	int bin = histPtDict::efficiencies["efficiency_electron_fold"].FindFixBin(tau_0_eta, tau_0_pt);
	weight = histPtDict::efficiencies["efficiency_electron_fold"].GetEfficiency(bin);
      } else {
	int bin = histPtDict::efficiencies["efficiency_electron_trig"].FindFixBin(tau_0_eta, tau_0_pt);
	weight = histPtDict::efficiencies["efficiency_electron_trig"].GetEfficiency(bin);
      }
    }
//     if(tau_0_pt >= 26 && tau_0_pt < 28)
//       weight *= 0.74;
//     if(tau_0_pt >= 28 && tau_0_pt < 30)
//       weight *= 0.82;
//     if(tau_0_pt >= 30 && tau_0_pt < 37.5)
//       weight *= 0.85;
//     if(tau_0_pt >= 37.5 && tau_0_pt < 45)
//       weight *= 0.89;
//     if(tau_0_pt >= 45 && tau_0_pt < 60)
//       weight *= 0.92;
//     if(tau_0_pt >= 60)
//       weight *= 0.97;

    return weight;
  }

  float weight_diele_trig(float tau_0_pt, float tau_0_eta)
  {
    float weight = 1;

    int bin = histPtDict::efficiencies["efficiency_electron_ditrig"].FindFixBin(tau_0_eta, tau_0_pt);
    weight = histPtDict::efficiencies["efficiency_electron_ditrig"].GetEfficiency(bin);

    return weight;
  }

  float weight_dimu_trig(float tau_0_pt, float tau_0_eta)
  {
    float weight = 1;

    int bin = histPtDict::efficiencies["efficiency_muon_ditrig"].FindFixBin(tau_0_eta, tau_0_pt);
    weight = histPtDict::efficiencies["efficiency_muon_ditrig"].GetEfficiency(bin);

    return weight;
  }

  float weight_tau_trig(float tau_0_pt, float tau_0_eta, bool lead)
  {
    float weight = 1;
    // Actually factors in the 1p to 3p ratio
    if (lead) 
      {
	if (tau_0_pt >= 28) {
	  int bin = histPtDict::efficiencies["tau35"].FindFixBin(tau_0_eta, tau_0_pt);
	  weight = histPtDict::efficiencies["tau35"].GetEfficiency(bin);
	}
      } else 
      {
	if (tau_0_pt >= 28) {
	  int bin = histPtDict::efficiencies["tau25"].FindFixBin(tau_0_eta, tau_0_pt);
	  weight = histPtDict::efficiencies["tau25"].GetEfficiency(bin);
	}
      }
//     // Actually factors in the 1p to 3p ratio
//     if(lead)
//       {
//       	if(tau_0_pt >= 40 && tau_0_pt < 45)
// 	  weight *= 0.74;
// 	if(tau_0_pt >= 45 && tau_0_pt < 50)
// 	  weight *= 0.825;
// 	if(tau_0_pt >= 50 && tau_0_pt < 55)
// 	  weight *= 0.875;
// 	if(tau_0_pt >= 55 && tau_0_pt < 60)
// 	  weight *= 0.91;
// 	if(tau_0_pt >= 60)
// 	  weight *= 0.94;
//       } else 
//       {
// 	if(tau_0_pt >= 28 && tau_0_pt < 32)
// 	  weight *= 0.74;
// 	if(tau_0_pt >= 32 && tau_0_pt < 36)
// 	  weight *= 0.85;
// 	if(tau_0_pt >= 36 && tau_0_pt < 40)
// 	  weight *= 0.88;
// 	if(tau_0_pt >= 40 && tau_0_pt < 48)
// 	  weight *= 0.92;
// 	if(tau_0_pt >= 48)
// 	  weight *= 0.94;
//       }
    return weight;
  }
  
  float weight_mu_trig(float tau_0_pt, float tau_0_eta, bool fold)
  {
    float weight = 1;
    
    if (fold) {
      int bin = histPtDict::efficiencies["efficiency_muon_fold"].FindFixBin(tau_0_eta, tau_0_pt);
      weight = histPtDict::efficiencies["efficiency_muon_fold"].GetEfficiency(bin);
    } else {
      int bin = histPtDict::efficiencies["efficiency_muon_trig"].FindFixBin(tau_0_eta, tau_0_pt);
      weight = histPtDict::efficiencies["efficiency_muon_trig"].GetEfficiency(bin);
    }
//     // Also hard-code turn-on efficiency, 0.66 in central
//     if(fabs(tau_0_eta) < 0.1)
//       weight *= 0.52;
//     else if(fabs(tau_0_eta) < 1.05)
//       weight *= 0.73;
//     else
//       weight *= 0.87;
    
    return weight;
  }

  float weight_unfold_mu_trig(float tau_0_pt, float tau_0_eta, float tau_1_pt, float tau_1_eta)
  {
    float weight_lead = weight_mu_trig(tau_0_pt, tau_0_eta, false);
    float weight_sublead = weight_mu_trig(tau_1_pt, tau_1_eta, false);
    // both pass: weight_lead * weight_sublead
    // lead pass, sublead fail: weight_lead * (1 - weight_sublead)
    // lead fail, sublead pass: (1 - weight_lead) * weight_sublead
    // both fail: (1 - weight_lead) * (1 - weight_sublead)
    // the sum of the 4 scenario is 1
    // unfolding weight = 1 - ( (1 - weight_lead) * (1 - weight_sublead))
    float weight = 1 - (1 - weight_lead) * (1 - weight_sublead);
    return weight;
  }

  float weight_unfold_ele_trig(float tau_0_pt, float tau_0_eta, float tau_1_pt, float tau_1_eta)
  {
    float weight_lead = weight_ele_trig(tau_0_pt, tau_0_eta, false);
    float weight_sublead = weight_ele_trig(tau_1_pt, tau_1_eta, false);
    // both pass: weight_lead * weight_sublead
    // lead pass, sublead fail: weight_lead * (1 - weight_sublead)
    // lead fail, sublead pass: (1 - weight_lead) * weight_sublead
    // both fail: (1 - weight_lead) * (1 - weight_sublead)
    // the sum of the 4 scenario is 1
    // unfolding weight = 1 - ( (1 - weight_lead) * (1 - weight_sublead))
    // implementation without the boolean
    // unfolding weight = 1 - ( (1 - weight_lead) * (1 - weight_sublead))
    float weight = 1 - (1 - weight_lead) * (1 - weight_sublead);
    return weight;
  }



  float weight_mu_reco(float tau_0_pt, float tau_0_eta)
  {
    float weight = 1;

    int bin = histPtDict::efficiencies["efficiency_muon_reco"].FindFixBin(tau_0_eta, tau_0_pt);
    weight = histPtDict::efficiencies["efficiency_muon_reco"].GetEfficiency(bin);

    // Hard-code for now just to see how it works
    // 62% in the crack, 98% everywhere
//     if(fabs(tau_0_eta) < 0.1)
//       weight *= 0.62;
//     else
//       weight *= 0.98;
//    // Slight pT dependence for isolation, but probably not worth handling right now
    return weight;
  }
  
  float weight_ele_reco(float tau_0_pt, float tau_0_eta)
  {
    float weight = 1;

    if (tau_0_pt >= 26) {
      int bin = histPtDict::efficiencies["efficiency_electron_reco"].FindFixBin(tau_0_eta, tau_0_pt);
      weight = histPtDict::efficiencies["efficiency_electron_reco"].GetEfficiency(bin);
    }


//     // Gradient Iso numbers, sloppy parameterization
//     if(tau_0_pt >= 26 && tau_0_pt < 30)
//       weight *= 0.89;
//     if(tau_0_pt >= 30 && tau_0_pt < 35)
//       weight *= 0.91;
//     if(tau_0_pt >= 35 && tau_0_pt < 40)
//       weight *= 0.92;
//     if(tau_0_pt >= 40 && tau_0_pt < 45)
//       weight *= 0.93;
//     if(tau_0_pt >= 45 && tau_0_pt < 60)
//       weight *= 0.95;
//     if(tau_0_pt >= 60)
//       weight *= 0.98;

    return weight;
  }


  // forwar decelaration for simplicity
  void corrected_momentums(float tau_0_pt, float tau_0_eta, float tau_0_phi,
                           float tau_1_pt, float tau_1_eta, float tau_1_phi,
                           int event_number, int channel, int offset);  

  void correct_oversample(float tau_0_pt, float tau_0_eta, float tau_0_phi,
			  float tau_1_pt, float tau_1_eta, float tau_1_phi,
			  int event_number, int channel)
  {
    // The logic here is we will keep rolling the dice to generate different
    // four-momenta, everything will be hard-coded for now (no need for steering)

    // Book-keeping
    histPtDict::m_passEvents = 0;
    bool foundOne = false;
    std::vector<TLorentzVector> firstTaus;
    std::vector<int> firstTypes;

    float cut_0 = 0;
    float cut_1 = 0;

    // Assess channel:
    if(channel == PtCorrHelper::hh)
      {
	cut_0 = 40;
	cut_1 = 30;
      }
    
    if(channel == PtCorrHelper::eh || channel == PtCorrHelper::muh)
      {
	cut_0 = 30;
	cut_1 = 20;
      }
    else //leplep
      {
	cut_0 = 17;
	cut_1 = 10;
      }

    for(int i=0; i<histPtDict::n_oversampling; i++)
      {
	// Roll one instance of embedding
	corrected_momentums(tau_0_pt, tau_0_eta, tau_0_phi,
			    tau_1_pt, tau_1_eta, tau_1_phi,
			    event_number, channel, i);
	  
	// Does this event pass?
	if( histPtDict::m_taus[0].Pt() > cut_0 && histPtDict::m_taus[1].Pt() > cut_1)
	  {
	    // Increment filter efficiency
	    histPtDict::m_passEvents++;
	    
	    // First event that passes: store its properties
	    if (!foundOne)
	      {
		firstTaus = histPtDict::m_taus;
		firstTypes = histPtDict::m_taus_type;
		
	      }
	    foundOne = true;
	  }
      }

    // Hard-code to first event which passed selection, otherwise use last event randomly
    if(foundOne)
      {
	histPtDict::m_taus = firstTaus;
	histPtDict::m_taus_type = firstTypes;
      }
  }


  void corrected_momentums(float tau_0_pt, float tau_0_eta, float tau_0_phi,
			   float tau_1_pt, float tau_1_eta, float tau_1_phi,
			   int event_number, int channel, int offset)
  {
    //std::cout << "Channel is: " << channel << std::endl;

    float pt_0 = tau_0_pt;
    float pt_1 = tau_1_pt;

    TRandom3 random_object_picker(event_number+offset);
    float rnd_number = random_object_picker.Rndm();

    int bin_0 = get_pt_bin(pt_0);
    int bin_1 = get_pt_bin(pt_1);
    
    // Correct pt_bin for eta
    int eta_0 = 5;
    int eta_1 = 5;

    if (fabs(tau_0_eta) < 1.52) eta_0 = 4;
    if (fabs(tau_1_eta) < 1.52) eta_1 = 4;
    if( fabs(tau_0_eta) < 1.37) eta_0 = 3;
    if( fabs(tau_1_eta) < 1.37) eta_1 = 3;
    if( fabs(tau_0_eta) < 0.8 ) eta_0 = 2;
    if( fabs(tau_1_eta) < 0.8 ) eta_1 = 2;
    if( fabs(tau_0_eta) < 0.4 ) eta_0 = 1;
    if( fabs(tau_1_eta) < 0.4 ) eta_1 = 1;
    if( fabs(tau_0_eta) < 0.1 ) eta_0 = 0;
    if( fabs(tau_1_eta) < 0.1 ) eta_1 = 0;

    bin_0 = bin_0 * 6 + eta_0;
    bin_1 = bin_1 * 6 + eta_1;


    if (bin_0 > (int)(histPtDict::h_pt_corr_h.size() - 1) || bin_0 < 0) {
      std::cout << "bin = " << bin_0
		<< ", pt = " << pt_0
		<< ", eta = " << tau_0_eta
		<< ", event number = " << event_number
		<< std::endl;
    }
    
    if (bin_1 > (int)(histPtDict::h_pt_corr_h.size() - 1) || bin_1 < 0) {
      std::cout << "bin = " << bin_1
		<< ", pt = " << pt_1
		<< ", eta = " << tau_1_eta
		<< ", event number = " << event_number
		<< std::endl;
    }




    // std::cout << "pt/eta/bin tau0: " << pt_0 << "/" << tau_0_eta << "/" << bin_0 << std::endl;
    // std::cout << "pt/eta/bin:tau1 " << pt_1 << "/" << tau_1_eta << "/" << bin_1 << std::endl;
    // std::cout << "channel: " << channel << std::endl;
    // std::cout << "bin0 : " << bin_0 << std::endl;
    // std::cout << "bin1 : " << bin_1 << std::endl;

    TH1D* hist_0 = 0;
    TH1D* hist_1 = 0;
    if (channel == PtCorrHelper::hh) {
      hist_0 = &histPtDict::h_pt_corr_h.at(bin_0);
      hist_1 = &histPtDict::h_pt_corr_h.at(bin_1);
    } else if (channel == PtCorrHelper::eh) {
      if (rnd_number > 0.5) {
	hist_0 = &histPtDict::h_pt_corr_e.at(bin_0);
	hist_1 = &histPtDict::h_pt_corr_h_eveto.at(bin_1);
      } else {
	hist_0 = &histPtDict::h_pt_corr_h_eveto.at(bin_0);
	hist_1 = &histPtDict::h_pt_corr_e.at(bin_1);
      }
    } else if (channel == PtCorrHelper::muh) {
      if (rnd_number > 0.5) {
	hist_0 = &histPtDict::h_pt_corr_mu.at(bin_0);
	hist_1 = &histPtDict::h_pt_corr_h.at(bin_1);
      } else {
	hist_0 = &histPtDict::h_pt_corr_h.at(bin_0);
	hist_1 = &histPtDict::h_pt_corr_mu.at(bin_1);
      }
    } else if (channel == PtCorrHelper::ee) {
      hist_0 = &histPtDict::h_pt_corr_e.at(bin_0);
      hist_1 = &histPtDict::h_pt_corr_e.at(bin_1);
    } else if (channel == PtCorrHelper::mumu) {
      hist_0 = &histPtDict::h_pt_corr_mu.at(bin_0);
      hist_1 = &histPtDict::h_pt_corr_mu.at(bin_1);
    } else if (channel == PtCorrHelper::emu) {
      if (rnd_number > 0.5) {
	hist_0 = &histPtDict::h_pt_corr_e.at(bin_0);
	hist_1 = &histPtDict::h_pt_corr_mu.at(bin_1);
      } else {
	hist_0 = &histPtDict::h_pt_corr_mu.at(bin_0);
	hist_1 = &histPtDict::h_pt_corr_e.at(bin_1);
      }
    } else {
      std::cout << "Wrong channel!" << std::endl;
    }
    
    if(gRandom) delete gRandom;
    gRandom = new TRandom3(0);
    gRandom->SetSeed(event_number + 1001 + offset);

    float corr_0 = hist_0->GetRandom();
    float pt_0_corr = pt_0 * corr_0;

    if(gRandom) delete gRandom;
    gRandom = new TRandom3(0);
    gRandom->SetSeed(event_number + 2002 + offset);

    float corr_1 = hist_1->GetRandom();
    float pt_1_corr = pt_1 * corr_1;

    // Order the taus correctly
    TLorentzVector tau_0;
    TLorentzVector tau_1;
    
    // Correct eta values as well
    //float tau_0_eta_corr = asinh((tau_0_pt/pt_0_corr)*sinh(tau_0_eta));
    //float tau_1_eta_corr = asinh((tau_1_pt/pt_1_corr)*sinh(tau_1_eta));

    float tau_0_eta_corr = tau_0_eta;
    float tau_1_eta_corr = tau_1_eta;


    // non-hadronic channels: order in pt
    if (channel == PtCorrHelper::hh || channel == PtCorrHelper::ee || channel == PtCorrHelper::mumu || channel == PtCorrHelper::emu) 
      {
	if(pt_0_corr > pt_1_corr) {
	  tau_0.SetPtEtaPhiM(pt_0_corr, tau_0_eta_corr, tau_0_phi, 0);
	  tau_1.SetPtEtaPhiM(pt_1_corr, tau_1_eta_corr, tau_1_phi, 0);
	} else {
	  tau_0.SetPtEtaPhiM(pt_1_corr, tau_1_eta_corr, tau_1_phi, 0);
	  tau_1.SetPtEtaPhiM(pt_0_corr, tau_0_eta_corr, tau_0_phi, 0);
	}   
      } 
    // different flavour channels: order had second
    else //if (channel == PtCorrHelper::eh || channel == PtCorrHelper::muh)
      {
	if (rnd_number < 0.5) {
	  tau_0.SetPtEtaPhiM(pt_0_corr, tau_0_eta_corr, tau_0_phi, 0);
	  tau_1.SetPtEtaPhiM(pt_1_corr, tau_1_eta_corr, tau_1_phi, 0);
	} else {
	  tau_0.SetPtEtaPhiM(pt_1_corr, tau_1_eta_corr, tau_1_phi, 0);
	  tau_1.SetPtEtaPhiM(pt_0_corr, tau_0_eta_corr, tau_0_phi, 0);
	}
      }

    // Fill vector
    histPtDict::m_taus.clear();
    histPtDict::m_taus.push_back(tau_0);
    histPtDict::m_taus.push_back(tau_1);
    
    // Fill indices
    histPtDict::m_taus_type.clear();
    if (channel == PtCorrHelper::hh) {
      histPtDict::m_taus_type.push_back(PtCorrHelper::tau);
      histPtDict::m_taus_type.push_back(PtCorrHelper::tau);
    } else if (channel == PtCorrHelper::eh) {
      histPtDict::m_taus_type.push_back(PtCorrHelper::tau);
      histPtDict::m_taus_type.push_back(PtCorrHelper::electron);
    } else if (channel == PtCorrHelper::muh) {
      histPtDict::m_taus_type.push_back(PtCorrHelper::tau);
      histPtDict::m_taus_type.push_back(PtCorrHelper::muon);
    } else if (channel == PtCorrHelper::ee) {
      histPtDict::m_taus_type.push_back(PtCorrHelper::electron);
      histPtDict::m_taus_type.push_back(PtCorrHelper::electron);
    } else if (channel == PtCorrHelper::mumu) {
      histPtDict::m_taus_type.push_back(PtCorrHelper::muon);
      histPtDict::m_taus_type.push_back(PtCorrHelper::muon);
    } else { // emu
      if (rnd_number > 0.5) {
	// tau_0 randomised as electron, tau_1 as muon
	if (pt_0_corr > pt_1_corr) {
	  // uncorrected lepton ordering kept
	  histPtDict::m_taus_type.push_back(PtCorrHelper::electron);
	  histPtDict::m_taus_type.push_back(PtCorrHelper::muon);
	} else {
	  histPtDict::m_taus_type.push_back(PtCorrHelper::muon);
	  histPtDict::m_taus_type.push_back(PtCorrHelper::electron);
	}
      } else {
	// tau_0 randomised as muon, tau_1 as electron
	if (pt_0_corr > pt_1_corr) {
	  // uncorrected lepton ordering kept
	  histPtDict::m_taus_type.push_back(PtCorrHelper::muon);
	  histPtDict::m_taus_type.push_back(PtCorrHelper::electron);
	} else {
	  histPtDict::m_taus_type.push_back(PtCorrHelper::electron);
	  histPtDict::m_taus_type.push_back(PtCorrHelper::muon);
	}
      }
    }
      
    // Internal book-keeping for event number
    histPtDict::m_currentEvent = event_number;

    // Debug statement
    //std::cout << pt_0 << ", " << pt_1 << ", " << corr_0 << ", " << corr_1 << ", "<< histPtDict::m_taus[0].Pt() << ", "<< histPtDict::m_taus[1].Pt() << ", " << histPtDict::m_currentEvent << std::endl;

  }

  float weight_folded_event(float tau_0_pt, float tau_0_eta, float tau_0_phi,
			    float tau_1_pt, float tau_1_eta, float tau_1_phi,
			    int event_number, int channel=PtCorrHelper::hh) {

    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    // Total weight
    float weight = 1;

    // Fold in filter efficiency weight
    weight *= float(histPtDict::m_passEvents) / float(histPtDict::n_oversampling);

    // Include new scaling based on channel branching ratio
    if(channel == PtCorrHelper::hh)
      weight *= 0.42/(1-histPtDict::m_lephad_frac-histPtDict::m_leplep_frac)/2.0;
    if(channel == PtCorrHelper::eh || channel == PtCorrHelper::muh)
      weight *= 0.45/histPtDict::m_lephad_frac/2.0;
    // Divide by 4 since we only generate 2 possibilities out of 4
    if(channel == PtCorrHelper::emu)
      weight *= 0.13/histPtDict::m_leplep_frac/4.0;

    // Start by determining channel

    // had-had
    if (histPtDict::m_taus_type[0] == PtCorrHelper::tau && histPtDict::m_taus_type[1] == PtCorrHelper::tau)
      {
	// New calculation, takes into account swapping
	double ELead0 = weight_tau_trig(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Eta(), true);
	double ESub0 = weight_tau_trig(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Eta(), false);
	double ELead1 = weight_tau_trig(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Eta(), true);
	double ESub1 = weight_tau_trig(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Eta(), false);
	
	// First term, both legs passing the correct trigger
	// Second term, swapped legs
	weight *= (ELead0 * ESub1) + ((ESub0-ELead0)*ELead1);
	
	//weight *= weight_tau_trig(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Eta(), true);
	//weight *= weight_tau_trig(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Eta(), false);
      }
    // mu-had: Note: we only take in trigger dependence, reco dependence included in ratio functions
    if (histPtDict::m_taus_type[0] == PtCorrHelper::tau && histPtDict::m_taus_type[1] == PtCorrHelper::muon)
      {
	weight *= weight_mu_trig(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Eta(), true);
      }
    // e-had:  Note: we only take in trigger dependence, reco dependence included in ratio functions
    if (histPtDict::m_taus_type[0] == PtCorrHelper::tau && histPtDict::m_taus_type[1] == PtCorrHelper::electron)
      {
	weight *= weight_ele_trig(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Eta(), true);
      }
    // e-mu && mu-e: Note: This now accommodates using both single triggers & di-object triggers
    if ((histPtDict::m_taus_type[0] == PtCorrHelper::electron && histPtDict::m_taus_type[1] == PtCorrHelper::muon) || 
	(histPtDict::m_taus_type[0] == PtCorrHelper::muon && histPtDict::m_taus_type[1] == PtCorrHelper::electron))
      {
	// Need to factor in both weights for single lepton triggers
	// Probability of either or both triggers passing is: E1*E2 + (1-E1)*E2 + E1*(1-E2)
	float E1 = histPtDict::m_taus_type[0] == PtCorrHelper::muon ? weight_mu_trig(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Eta(), true) : weight_ele_trig(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Eta(), true);
	float E2 = histPtDict::m_taus_type[1] == PtCorrHelper::muon ? weight_mu_trig(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Eta(), true) : weight_ele_trig(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Eta(), true);
	// Then we need to consider the di-lepton trigger case, the logic is as such:
	// Store turn-on curves, but only for the population where the single lepton trigger has failed
	// Then the expression becomes (1-E1)*(1-E2)*(diE1*diE2)
	float diE1 = histPtDict::m_taus_type[0] == PtCorrHelper::muon ? weight_dimu_trig(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Eta()) : weight_diele_trig(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Eta());
	float diE2 = histPtDict::m_taus_type[1] == PtCorrHelper::muon ? weight_dimu_trig(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Eta()) : weight_diele_trig(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Eta());

	weight *= (E1*E2) + E1*(1-E2) + E2*(1-E1) + ((1-E1)*(1-E2)*(diE1*diE2));
      }
    
    return weight;

  }
  
  float weight_qxq(float tau_0_pt, float tau_0_eta, float tau_0_phi,
		   float tau_1_pt, float tau_1_eta, float tau_1_phi,
		   int event_number, int channel)
  {
    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    // efficiency  of first tau
    float charge_id_eff_0;
    if (histPtDict::m_taus_type[0] == PtCorrHelper::electron) {
      int bin = histPtDict::efficiencies["efficiency_electron_charge"].FindFixBin(histPtDict::m_taus[0].Eta(), histPtDict::m_taus[0].Pt());
      charge_id_eff_0 = histPtDict::efficiencies["efficiency_electron_charge"].GetEfficiency(bin);
    } else if (histPtDict::m_taus_type[0] == PtCorrHelper::muon) {
      int bin = histPtDict::efficiencies["efficiency_muon_charge"].FindFixBin(histPtDict::m_taus[0].Eta(), histPtDict::m_taus[0].Pt());
      charge_id_eff_0 = histPtDict::efficiencies["efficiency_muon_charge"].GetEfficiency(bin);
    } else if (histPtDict::m_taus_type[0] == PtCorrHelper::tau) {
      int bin = histPtDict::efficiencies["efficiency_charge"].FindFixBin(histPtDict::m_taus[0].Eta(), histPtDict::m_taus[0].Pt());
      charge_id_eff_0 = histPtDict::efficiencies["efficiency_charge"].GetEfficiency(bin);
    } else {
      std::cerr << "wrong tau type!!! tau type = " << histPtDict::m_taus_type[0] << std::endl;
      charge_id_eff_0 = 1;
    }

    // efficiency  of first tau
    float charge_id_eff_1;
    if (histPtDict::m_taus_type[1] == PtCorrHelper::electron) {
      int bin = histPtDict::efficiencies["efficiency_electron_charge"].FindFixBin(histPtDict::m_taus[1].Eta(), histPtDict::m_taus[1].Pt());
      charge_id_eff_1 = histPtDict::efficiencies["efficiency_electron_charge"].GetEfficiency(bin);
    } else if (histPtDict::m_taus_type[1] == PtCorrHelper::muon) {
      int bin = histPtDict::efficiencies["efficiency_muon_charge"].FindFixBin(histPtDict::m_taus[1].Eta(), histPtDict::m_taus[1].Pt());
      charge_id_eff_1 = histPtDict::efficiencies["efficiency_muon_charge"].GetEfficiency(bin);
    } else if (histPtDict::m_taus_type[1] == PtCorrHelper::tau) {
      int bin = histPtDict::efficiencies["efficiency_charge"].FindFixBin(histPtDict::m_taus[1].Eta(), histPtDict::m_taus[1].Pt());
      charge_id_eff_1 = histPtDict::efficiencies["efficiency_charge"].GetEfficiency(bin);
    } else {
      std::cerr << "wrong tau type!!! tau type = " << histPtDict::m_taus_type[1] << std::endl;
      charge_id_eff_1 = 1;
    }
    
    // qxq = -1 if both lepton have reco charge = true charge 
    // or if both have reco charge != truth charge
    float weight = charge_id_eff_0 * charge_id_eff_1 + (1 - charge_id_eff_0) * (1 - charge_id_eff_1);
    return weight;
  }

  float tau_corr_0(float tau_0_pt, float tau_0_eta, float tau_0_phi,
		   float tau_1_pt, float tau_1_eta, float tau_1_phi,
		   int event_number, int channel=PtCorrHelper::hh) {
    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    // Re-ordering
    return  histPtDict::m_taus_type[0];

  }

  float tau_corr_1(float tau_0_pt, float tau_0_eta, float tau_0_phi,
		   float tau_1_pt, float tau_1_eta, float tau_1_phi,
		   int event_number, int channel=PtCorrHelper::hh) {
    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    // Re-ordering
    return  histPtDict::m_taus_type[1];

  }

  bool lepton_pt_cuts_ll(float tau_0_pt, float tau_0_eta, float tau_0_phi,
			  float tau_1_pt, float tau_1_eta, float tau_1_phi,
			  int event_number, int channel=PtCorrHelper::emu) {

    // only implement cuts from 2016-2018
    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    // We will handle the trigger combination from the weights function
    // Here only apply the 'minimal cuts'

    // Calculate all 4 possibilites

    // If channel is emu, 27-10, single trig
    if (histPtDict::m_taus_type[0] == PtCorrHelper::electron)
      if (histPtDict::m_taus_type[1] == PtCorrHelper::muon)
        if (histPtDict::m_taus[0].Pt() > 27.0)
          if (histPtDict::m_taus[1].Pt() > 10.0)
            return true;

    // If channel is mue, 27-15, single trig
    if (histPtDict::m_taus_type[0] == PtCorrHelper::muon)
      if (histPtDict::m_taus_type[1] == PtCorrHelper::electron)
	if (histPtDict::m_taus[0].Pt() > 27.0) 
	  if (histPtDict::m_taus[1].Pt() > 15.0)
	    return true;

    // If channel is emu, di-trig
    if (histPtDict::m_taus_type[0] == PtCorrHelper::electron)
      if (histPtDict::m_taus_type[1] == PtCorrHelper::muon)
	if (histPtDict::m_taus[0].Pt() > 17.0)
          if (histPtDict::m_taus[1].Pt() > 14.0)
            return true;

    // If channel is mue, di-trig (this one is ironically redundant)
    if (histPtDict::m_taus_type[0] == PtCorrHelper::muon)
      if (histPtDict::m_taus_type[1] == PtCorrHelper::electron)
	if (histPtDict::m_taus[0].Pt() > 14.0)
          if (histPtDict::m_taus[1].Pt() > 17.0)
            return true;



    // The main subtlety is that, if the electron is >27, the single electron
    // trigger is more efficient, so we'll need a special case for this in
    // the 'efficiency folding' weighting function

    return false;
  }

  bool lepton_pt_cuts_15_lh(float tau_0_pt, float tau_0_eta, float tau_0_phi,
			  float tau_1_pt, float tau_1_eta, float tau_1_phi,
			  int event_number, int channel=PtCorrHelper::eh) {

    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    // If channel is eh
    if (histPtDict::m_taus_type[0] == PtCorrHelper::tau)
      if (histPtDict::m_taus_type[1] == PtCorrHelper::electron)
        if (histPtDict::m_taus[1].Pt() > 25.0)
	  return true;
    
    // If channel is muhad
    if (histPtDict::m_taus_type[0] == PtCorrHelper::tau)
      if (histPtDict::m_taus_type[1] == PtCorrHelper::muon)
	if (histPtDict::m_taus[1].Pt() > 21.0) 
	    return true;

    return false;
  }

  bool lepton_pt_cuts_lh(float tau_0_pt, float tau_0_eta, float tau_0_phi,
			  float tau_1_pt, float tau_1_eta, float tau_1_phi,
			  int event_number, int channel=PtCorrHelper::eh) {

    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    // If channel is eh
    if (histPtDict::m_taus_type[0] == PtCorrHelper::tau)
      if (histPtDict::m_taus_type[1] == PtCorrHelper::electron)
        if (histPtDict::m_taus[1].Pt() > 27.0)
	  return true;
    
    // If channel is muhad
    if (histPtDict::m_taus_type[0] == PtCorrHelper::tau)
      if (histPtDict::m_taus_type[1] == PtCorrHelper::muon)
	if (histPtDict::m_taus[1].Pt() > 27.3) 
	    return true;

    return false;
  }

  bool eta_crack_cuts(float tau_0_pt, float tau_0_eta, float tau_0_phi,
		      float tau_1_pt, float tau_1_eta, float tau_1_phi,
		      int event_number, int channel=PtCorrHelper::emu) {

    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);


    // If first tau is an electron or a tau, check if it's in the crack
    // If it is, trash the event
    if (histPtDict::m_taus_type[0] != PtCorrHelper::muon) {
      if (fabs(histPtDict::m_taus[0].Eta()) > 1.37 && fabs(histPtDict::m_taus[0].Eta()) < 1.52)
	return false;
    }

    // If second tau is an electron or a tau, check if it's in the crack
    // If it is, trash the event
    if (histPtDict::m_taus_type[1] != PtCorrHelper::muon) {
      if (fabs(histPtDict::m_taus[1].Eta()) > 1.37 && fabs(histPtDict::m_taus[1].Eta()) < 1.52)
	return false;
    }

    return true;
  }

  float read_pt_corr_0(float tau_0_pt, float tau_0_eta, float tau_0_phi,
		       float tau_1_pt, float tau_1_eta, float tau_1_phi,
		       int event_number, int channel=PtCorrHelper::hh) {
    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    // Re-ordering
    return  histPtDict::m_taus[0].Pt();
  }

  float read_pt_corr_1(float tau_0_pt, float tau_0_eta, float tau_0_phi,
		       float tau_1_pt, float tau_1_eta, float tau_1_phi,
		       int event_number, int channel=PtCorrHelper::hh) {
    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    return  histPtDict::m_taus[1].Pt();
  }

  float eta_corr_0(float tau_0_pt, float tau_0_eta, float tau_0_phi,
		   float tau_1_pt, float tau_1_eta, float tau_1_phi,
		   int event_number, int channel=PtCorrHelper::hh) {
    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    
    // Re-ordering
    return  histPtDict::m_taus[0].Eta();
  }

  float eta_corr_1(float tau_0_pt, float tau_0_eta, float tau_0_phi,
		   float tau_1_pt, float tau_1_eta, float tau_1_phi,
		   int event_number, int channel=PtCorrHelper::hh) {
    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    
    // Re-ordering
    return  histPtDict::m_taus[1].Eta();
  }

  float dijet_ditau_dr_corr(float dijet_pt, float dijet_eta, float dijet_phi, float dijet_m,
			    float tau_0_pt, float tau_0_eta, float tau_0_phi,
			    float tau_1_pt, float tau_1_eta, float tau_1_phi,
			    int event_number, int channel=PtCorrHelper::hh) {
    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    
    // Calculate quantity
    TLorentzVector jet_0_p4;
    TLorentzVector jet_1_p4;
    TLorentzVector tau_0_p4;
    TLorentzVector tau_1_p4;

    // Sum up both taus
    tau_0_p4.SetPtEtaPhiM(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Eta(), histPtDict::m_taus[0].Phi(), 0);
    tau_1_p4.SetPtEtaPhiM(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Eta(), histPtDict::m_taus[1].Phi(), 0);

    // First vector of the calculation = dijet input
    jet_0_p4.SetPtEtaPhiM(dijet_pt, dijet_eta, dijet_phi, dijet_m);

    // Second vector of the calculation = ditau input
    jet_1_p4 = tau_0_p4 + tau_1_p4;

    // Return dR between both quantities
    return jet_0_p4.DeltaR(jet_1_p4);

  }

  float ditau_pt_corr(float met_et, float met_phi, 
		      float tau_0_pt, float tau_0_eta, float tau_0_phi,
		      float tau_1_pt, float tau_1_eta, float tau_1_phi,
		      int event_number, int channel=PtCorrHelper::hh)
  {
    // Check if buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    
    TVector2 met;
    met.SetMagPhi(met_et, met_phi);

    TVector2 lep_0;
    TVector2 lep_1;

    lep_0.SetMagPhi(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Phi());
    lep_1.SetMagPhi(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Phi());

    //return (lep_0 + lep_1 + met).Mod();
    // Attempt to fix definition: the MET should not be included here, only the di-tau pair
    return (lep_0 + lep_1).Mod();

  }
  
  float ditau_m_corr(float tau_0_pt, float tau_0_eta, float tau_0_phi,
		     float tau_1_pt, float tau_1_eta, float tau_1_phi,
		     int event_number, int channel=PtCorrHelper::hh)
  {

    // Check if buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    
    return (histPtDict::m_taus[0] + histPtDict::m_taus[1]).M();

  }

  float ditau_phi_corr(float tau_0_pt, float tau_0_eta, float tau_0_phi,
		     float tau_1_pt, float tau_1_eta, float tau_1_phi,
		     int event_number, int channel=PtCorrHelper::hh)
  {

    // Check if buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    
    return (histPtDict::m_taus[0] + histPtDict::m_taus[1]).Phi();

  }

  float ditau_eta_corr(float tau_0_pt, float tau_0_eta, float tau_0_phi,
		     float tau_1_pt, float tau_1_eta, float tau_1_phi,
		     int event_number, int channel=PtCorrHelper::hh)
  {

    // Check if buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    
    return (histPtDict::m_taus[0] + histPtDict::m_taus[1]).Eta();

  }

  float ditau_pt_corr(float tau_0_pt, float tau_0_eta, float tau_0_phi,
		     float tau_1_pt, float tau_1_eta, float tau_1_phi,
		     int event_number, int channel=PtCorrHelper::hh)
  {

    // Check if buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    
    return (histPtDict::m_taus[0] + histPtDict::m_taus[1]).Pt();

  }

  float MET_corr(float met_et, float met_phi, 
		 float tau_0_pt, float tau_0_eta, float tau_0_phi,
		 float tau_1_pt, float tau_1_eta, float tau_1_phi,
		 int event_number, int channel=PtCorrHelper::hh)
  {


    // Check if buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    TVector2 met;
    met.SetMagPhi(met_et, met_phi);

    float lep_et_0_corr = histPtDict::m_taus[0].Pt();
    float lep_et_1_corr = histPtDict::m_taus[1].Pt();

    TVector2 lep_0;
    TVector2 lep_1;

    lep_0.SetMagPhi(tau_0_pt, tau_0_phi);
    lep_1.SetMagPhi(tau_1_pt, tau_1_phi);

    // Calculate met with original leptons removed
    TVector2 met_nomu;
    met_nomu = met + lep_0 + lep_1;

    // Replace the lepton 4-vectors with corrected version
    lep_0.SetMagPhi(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Phi());
    lep_1.SetMagPhi(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Phi());

    // Calculate the final MET (taus added back in)
    return (met_nomu - lep_0 - lep_1).Mod();
  }

  float dphi_MET_corr_0(float met_et, float met_phi,
			float tau_0_pt, float tau_0_eta, float tau_0_phi,
			float tau_1_pt, float tau_1_eta, float tau_1_phi,
			int event_number, int channel=PtCorrHelper::hh) {
    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    //get corrected MET
    TVector2 met;
    met.SetMagPhi(met_et, met_phi);

    float lep_et_0_corr = histPtDict::m_taus[0].Pt();
    float lep_et_1_corr = histPtDict::m_taus[1].Pt();

    TVector2 lep_0;
    TVector2 lep_1;

    lep_0.SetMagPhi(tau_0_pt, tau_0_phi);
    lep_1.SetMagPhi(tau_1_pt, tau_1_phi);

    // Calculate met with original leptons removed
    TVector2 met_nomu;
    met_nomu = met + lep_0 + lep_1;

    // Replace the lepton 4-vectors with corrected version
    lep_0.SetMagPhi(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Phi());
    lep_1.SetMagPhi(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Phi());

    TVector2 met_final;
    met_final = met_nomu - lep_0 - lep_1;
    
    // Re-ordering
    return  lep_0.DeltaPhi(met_final);
  }

  float dphi_MET_corr_1(float met_et, float met_phi,
			float tau_0_pt, float tau_0_eta, float tau_0_phi,
			float tau_1_pt, float tau_1_eta, float tau_1_phi,
			int event_number, int channel=PtCorrHelper::hh) {
    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    //get corrected MET
    TVector2 met;
    met.SetMagPhi(met_et, met_phi);

    float lep_et_0_corr = histPtDict::m_taus[0].Pt();
    float lep_et_1_corr = histPtDict::m_taus[1].Pt();

    TVector2 lep_0;
    TVector2 lep_1;

    lep_0.SetMagPhi(tau_0_pt, tau_0_phi);
    lep_1.SetMagPhi(tau_1_pt, tau_1_phi);

    // Calculate met with original leptons removed
    TVector2 met_nomu;
    met_nomu = met + lep_0 + lep_1;

    // Replace the lepton 4-vectors with corrected version
    lep_0.SetMagPhi(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Phi());
    lep_1.SetMagPhi(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Phi());

    TVector2 met_final;
    met_final = met_nomu - lep_0 - lep_1;
    
    // Re-ordering
    return lep_1.DeltaPhi(met_final);
  }

  float dphi_MET_0(float met_et, float met_phi,
		   float tau_0_pt, float tau_0_eta, float tau_0_phi,
		   float tau_1_pt, float tau_1_eta, float tau_1_phi,
		   int event_number, int channel=PtCorrHelper::hh) {
    TVector2 met;
    met.SetMagPhi(met_et, met_phi);

    TVector2 lep_0;
    TVector2 lep_1;

    lep_0.SetMagPhi(tau_0_pt, tau_0_phi);
    lep_1.SetMagPhi(tau_1_pt, tau_1_phi);

    return lep_0.DeltaPhi(met);
  }

  float dphi_MET_1(float met_et, float met_phi,
		   float tau_0_pt, float tau_0_eta, float tau_0_phi,
		   float tau_1_pt, float tau_1_eta, float tau_1_phi,
		   int event_number, int channel=PtCorrHelper::hh) {
    TVector2 met;
    met.SetMagPhi(met_et, met_phi);

    TVector2 lep_0;
    TVector2 lep_1;

    lep_0.SetMagPhi(tau_0_pt, tau_0_phi);
    lep_1.SetMagPhi(tau_1_pt, tau_1_phi);

    return lep_1.DeltaPhi(met);
  }

  
  float dijet_met_dphi(float met_et, float met_phi,
		       float tau_0_pt, float tau_0_eta, float tau_0_phi,
		       float tau_1_pt, float tau_1_eta, float tau_1_phi,
		       float dijet_pt, float dijet_eta, float dijet_phi, float dijet_m,
		       int event_number, int channel=PtCorrHelper::hh) {
    TVector2 met;
    met.SetMagPhi(met_et, met_phi);

    TVector2 dijet_p4;
    dijet_p4.SetMagPhi(dijet_pt, dijet_phi);

    return dijet_p4.DeltaPhi(met);
  }

  float dijet_met_dphi_corr(float met_et, float met_phi,
			    float tau_0_pt, float tau_0_eta, float tau_0_phi,
			    float tau_1_pt, float tau_1_eta, float tau_1_phi,
			    float dijet_pt, float dijet_eta, float dijet_phi, float dijet_m,
			    int event_number, int channel=PtCorrHelper::hh) {
    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    //get corrected MET
    TVector2 met;
    met.SetMagPhi(met_et, met_phi);

    float lep_et_0_corr = histPtDict::m_taus[0].Pt();
    float lep_et_1_corr = histPtDict::m_taus[1].Pt();

    TVector2 lep_0;
    TVector2 lep_1;

    lep_0.SetMagPhi(tau_0_pt, tau_0_phi);
    lep_1.SetMagPhi(tau_1_pt, tau_1_phi);

    // Calculate met with original leptons removed
    TVector2 met_nomu;
    met_nomu = met + lep_0 + lep_1;

    // Replace the lepton 4-vectors with corrected version
    lep_0.SetMagPhi(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Phi());
    lep_1.SetMagPhi(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Phi());

    TVector2 met_final;
    met_final = met_nomu - lep_0 - lep_1;

    TVector2 dijet_p4;
    dijet_p4.SetMagPhi(dijet_pt, dijet_phi);

    return dijet_p4.DeltaPhi(met_final);
  }

  float dijetditau_pt(float met_et, float met_phi,
		      float tau_0_pt, float tau_0_eta, float tau_0_phi,
		      float tau_1_pt, float tau_1_eta, float tau_1_phi,
		      float dijet_pt, float dijet_eta, float dijet_phi, float dijet_m,
		      int event_number, int channel=PtCorrHelper::hh)
  {
    TLorentzVector dijet_p4;
    TLorentzVector tau_0;
    TLorentzVector tau_1;

    dijet_p4.SetPtEtaPhiM(dijet_pt, dijet_eta, dijet_phi, dijet_m);
    tau_0.SetPtEtaPhiM(tau_0_pt, tau_0_eta, tau_0_phi, 0);
    tau_1.SetPtEtaPhiM(tau_1_pt, tau_1_eta, tau_1_phi, 0);

    TLorentzVector dijetditau;
    dijetditau = dijet_p4 + tau_0 + tau_1;

    return dijetditau.Pt();
  }

  float dijetditau_pt_corr(float met_et, float met_phi,
			   float tau_0_pt, float tau_0_eta, float tau_0_phi,
			   float tau_1_pt, float tau_1_eta, float tau_1_phi,
			   float dijet_pt, float dijet_eta, float dijet_phi, float dijet_m,
			   int event_number, int channel=PtCorrHelper::hh)
  {
    // So, let's check if our buffered values are correct                                                                                                                                                                                     
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    TLorentzVector tau_0;
    TLorentzVector tau_1;

    tau_0.SetPtEtaPhiM(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Eta(), histPtDict::m_taus[0].Phi(), 0);
    tau_1.SetPtEtaPhiM(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Eta(), histPtDict::m_taus[1].Phi(), 0);

    TLorentzVector dijet_p4;
    dijet_p4.SetPtEtaPhiM(dijet_pt, dijet_eta, dijet_phi, dijet_m);

    TLorentzVector dijetditau;
    dijetditau = dijet_p4 + tau_0 + tau_1;

    return dijetditau.Pt();
  }

  float x2_corr(float met_et, float met_phi, 
		float tau_0_pt, float tau_0_eta, float tau_0_phi,
		float tau_1_pt, float tau_1_eta, float tau_1_phi,
		int event_number, int channel=PtCorrHelper::hh)
  {
    // Check if buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    
    TVector2 met;
    met.SetMagPhi(met_et, met_phi);

    TVector2 lep_0;
    TVector2 lep_1;

    lep_0.SetMagPhi(tau_0_pt, tau_0_phi);
    lep_1.SetMagPhi(tau_1_pt, tau_1_phi);

    // Calculate met with original leptons removed
    TVector2 met_nomu;
    met_nomu = met + lep_0 + lep_1;

    // Replace the lepton 4-vectors with corrected version
    lep_0.SetMagPhi(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Phi());
    lep_1.SetMagPhi(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Phi());
    
    // Store newly calculated values
    TVector2 met_final = met_nomu - lep_0 - lep_1;

    float new_met_et = met_final.Mod();
    float new_met_phi = met_final.Phi();

    // Calculate x2
    float numerator = new_met_et*sin(new_met_phi) - new_met_et*cos(new_met_phi)*tan(histPtDict::m_taus[0].Phi());
    float denominator = sin(histPtDict::m_taus[1].Phi()) - cos(histPtDict::m_taus[1].Phi())*tan(histPtDict::m_taus[0].Phi());

    // Sub-leading neutrino pT
    float col_lep2_pt = numerator / denominator;
    
    return histPtDict::m_taus[1].Pt() / (histPtDict::m_taus[1].Pt()+col_lep2_pt);
  }

  float x1_corr(float met_et, float met_phi, 
                float tau_0_pt, float tau_0_eta, float tau_0_phi,
		float tau_1_pt, float tau_1_eta, float tau_1_phi,
                int event_number, int channel=PtCorrHelper::hh)
  {
    // Check if buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    
    TVector2 met;
    met.SetMagPhi(met_et, met_phi);

    TVector2 lep_0;
    TVector2 lep_1;

    lep_0.SetMagPhi(tau_0_pt, tau_0_phi);
    lep_1.SetMagPhi(tau_1_pt, tau_1_phi);

    // Calculate met with original leptons removed
    TVector2 met_nomu;
    met_nomu = met + lep_0 + lep_1;

    // Replace the lepton 4-vectors with corrected version
    lep_0.SetMagPhi(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Phi());
    lep_1.SetMagPhi(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Phi());

    // Store newly calculated values
    TVector2 met_final = met_nomu - lep_0 - lep_1;

    float new_met_et = met_final.Mod();
    float new_met_phi = met_final.Phi();

    // Calculate x1
    float numerator = new_met_et*sin(new_met_phi) - new_met_et*cos(new_met_phi)*tan(histPtDict::m_taus[0].Phi());
    float denominator = sin(histPtDict::m_taus[1].Phi()) - cos(histPtDict::m_taus[1].Phi())*tan(histPtDict::m_taus[0].Phi());

    // Sub-leading neutrino pT
    float col_lep2_pt = numerator / denominator;

    // Convert to leading neutrino pT
    float col_lep1_pt = (new_met_et*cos(new_met_phi) - col_lep2_pt*cos(histPtDict::m_taus[1].Phi()))/cos(histPtDict::m_taus[0].Phi());
    
    return histPtDict::m_taus[0].Pt() / (histPtDict::m_taus[0].Pt()+col_lep1_pt);
  }

  float dr_corr( float tau_0_pt, float tau_0_eta, float tau_0_phi,
		 float tau_1_pt, float tau_1_eta, float tau_1_phi,
		 int event_number, int channel=PtCorrHelper::hh)
  {
    // Check if buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    // Fun, easy & interactive!
    return histPtDict::m_taus[0].DeltaR(histPtDict::m_taus[1]);
  }

  float deta_corr( float tau_0_pt, float tau_0_eta, float tau_0_phi,
		   float tau_1_pt, float tau_1_eta, float tau_1_phi,
		   int event_number, int channel=PtCorrHelper::hh)
  {
    // Check if buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    return histPtDict::m_taus[0].Eta() - histPtDict::m_taus[1].Eta();
  }

  float mT_corr(float met_et, float met_phi,
		float tau_0_pt, float tau_0_eta, float tau_0_phi,
		float tau_1_pt, float tau_1_eta, float tau_1_phi,
		int event_number, int channel=PtCorrHelper::hh)
  {
    // Check if buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);

    // Correct the MET
    TVector2 met;
    met.SetMagPhi(met_et, met_phi);

    TVector2 lep_0;
    TVector2 lep_1;

    lep_0.SetMagPhi(tau_0_pt, tau_0_phi);
    lep_1.SetMagPhi(tau_1_pt, tau_1_phi);

    // Calculate met with original leptons removed
    TVector2 met_nomu;
    met_nomu = met + lep_0 + lep_1;

    // Replace the lepton 4-vectors with corrected version
    lep_0.SetMagPhi(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Phi());
    lep_1.SetMagPhi(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Phi());

    // Store newly calculated values
    TVector2 met_final = met_nomu - lep_0 - lep_1;

    float new_met_et = met_final.Mod();
    float new_met_phi = met_final.Phi();
    
    // By construction, we use tau_1 for the light lepton
    
    float CosDeltaPhi = cos(met_final.Phi()-histPtDict::m_taus[1].Phi());
    return std::sqrt(2 * histPtDict::m_taus[1].Pt() * new_met_et * (1 - CosDeltaPhi));
    
  }

  float jet_tau_0_dr_corr(float jet_pt, float jet_eta, float jet_phi,
			  float tau_0_pt, float tau_0_eta, float tau_0_phi,
			  float tau_1_pt, float tau_1_eta, float tau_1_phi,
			  int event_number, int channel=PtCorrHelper::hh) {
    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    
    TLorentzVector jet_0_p4;
    TLorentzVector tau_0_p4;

    jet_0_p4.SetPtEtaPhiM(jet_pt, jet_eta, jet_phi, 0);
    tau_0_p4.SetPtEtaPhiM(histPtDict::m_taus[0].Pt(), histPtDict::m_taus[0].Eta(), histPtDict::m_taus[0].Phi(), 0);

    // Return dR between both quantities
    if(jet_0_p4.Pt() > 20)
      return jet_0_p4.DeltaR(tau_0_p4);
    else
      return 4.5;
  }

  float jet_tau_1_dr_corr(float jet_pt, float jet_eta, float jet_phi,
			  float tau_0_pt, float tau_0_eta, float tau_0_phi,
			  float tau_1_pt, float tau_1_eta, float tau_1_phi,
			  int event_number, int channel=PtCorrHelper::hh) {
    // So, let's check if our buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    
    TLorentzVector jet_0_p4;
    TLorentzVector tau_0_p4;

    jet_0_p4.SetPtEtaPhiM(jet_pt, jet_eta, jet_phi, 0);
    tau_0_p4.SetPtEtaPhiM(histPtDict::m_taus[1].Pt(), histPtDict::m_taus[1].Eta(), histPtDict::m_taus[1].Phi(), 0);

    // Return dR between both quantities
    if(jet_0_p4.Pt() > 20)
      return jet_0_p4.DeltaR(tau_0_p4);
    else
      return 4.5;
  }

  float dr_helper(float pt_0, float eta_0, float phi_0, float eta_1, float phi_1)
  {
    TLorentzVector jet_0_p4;
    TLorentzVector jet_1_p4;
    jet_0_p4.SetPtEtaPhiM(5, eta_0, phi_0, 0);
    jet_1_p4.SetPtEtaPhiM(5, eta_1, phi_1, 0);

    if(pt_0 > 20)
      return jet_0_p4.DeltaR(jet_1_p4);
    else
      return 4.5;
  }

  float m_coll_corr(float met_et, float met_phi,
		    float tau_0_pt, float tau_0_eta, float tau_0_phi,
		    float tau_1_pt, float tau_1_eta, float tau_1_phi,
		    int event_number, int channel=PtCorrHelper::hh)
  {
    // Check if buffered values are correct
    if(event_number!=histPtDict::m_currentEvent)
      correct_oversample(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    
    float x1 = x1_corr(met_et, met_phi, tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    float x2 = x2_corr(met_et, met_phi, tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    float m_vis = ditau_m_corr(tau_0_pt, tau_0_eta, tau_0_phi, tau_1_pt, tau_1_eta, tau_1_phi, event_number, channel);
    
    if( x1 * x2 != 0)
      return m_vis / std::sqrt(x1*x2);
    else
      return -1;
  }
    
}
