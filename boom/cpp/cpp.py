__all__ = [
    '_load_vbf_tagger_cpp',
    '_load_ztheosys_helper_cpp',
    '_load_mc_norm_mult_cpp',
    '_load_ff_helper',
    '_load_if_helper',
    '_load_eveto_helper',
    '_load_random_cpp',
    '_load_hhfakesyst_helper_cpp',
    '_load_zll_pt_cpp',
    '_load_tth_bdts_cpp',
    '_load_vh_tagger_cpp',
    ]
  

import ROOT


def _load_vbf_tagger_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/vbf_tagger.cpp', 'k-', '', 'cache')
        print 'BOOM: \t vbf tagger loaded!'
    except:
        raise RuntimeError
    
    try:
        ROOT.VBF_Tagger()
    except:
        print "VBF Tagger not compiled"


def _load_ztheosys_helper_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/ztheosyst_helper.cpp', 'k-', '', 'cache')
        print 'BOOM:\t Z theory syst helper tool loaded!'
    except:
        raise RuntimeError

    # this is needed because of
    # https://sft.its.cern.ch/jira/browse/ROOT-7216
    try:
        ROOT.ZTheoSystHelper()
    except:
        print 'Z Theo Syst tool not compiled'


def _load_mc_norm_mult_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/mc_norm_mult.cpp', 'k-', '', 'cache')
        print 'BOOM:\t cross-section * sum-ok-weights tool loaded!'
    except:
        raise RuntimeError

    try:
        ROOT.MC_Norm()
    except:
        print "Norm functions not compiled"
        raise RuntimeError


def _load_ff_helper():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/ff_helper.cpp', 'k-', '', 'cache')
        print 'BOOM:\t fake factor helper tool loaded!'
    except:
        raise RuntimeError

    # this is needed because of
    # https://sft.its.cern.ch/jira/browse/ROOT-7216
    try:
        ROOT.FakeHelper()
    except:
        print 'Fake Factor tool not compiled'


def _load_if_helper():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/iso_factor_helper.cpp', 'k-', '', 'cache')
        print 'BOOM:\t iso factor helper tool loaded!'
    except:
        raise RuntimeError

    # this is needed because of
    # https://sft.its.cern.ch/jira/browse/ROOT-7216
    try:
        ROOT.IsoFactorHelper()
    except:
        print 'Iso Factor tool not compiled'


def _load_eveto_helper():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/eveto_factor_helper.cpp', 'k-', '', 'cache')
        print 'BOOM:\t eveto scale factor helper tool loaded!'
    except:
        raise RuntimeError

    # this is needed because of
    # https://sft.its.cern.ch/jira/browse/ROOT-7216
    try:
        ROOT.EvetoFactorHelper()
    except:
        print 'Eveto Scale Factor tool not compiled'

def _load_random_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/random.cpp', 'k-', '', 'cache')
        print 'BOOM:\t randomizer loaded!'
    except:
        raise RuntimeError

    # this is needed because of
    # https://sft.its.cern.ch/jira/browse/ROOT-7216
    try:
        ROOT.Rnd()
    except:
        print 'randomizer not compiled!'

def _load_hhfakesyst_helper_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/hhfakesyst_helper.cpp', 'k-', '', 'cache')
        print 'BOOM:\t hhfakesyst_helper tool loaded!'
    except:
        raise RuntimeError
    try:
        ROOT.HHFakeSystHelper()
    except:
        print "HH Fake syst helper function not compiled"
        raise RuntimeError



def _load_zll_pt_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/zll_pt.cpp', 'k-', '', 'cache')
        print 'BOOM:\t zll_pt tool loaded!'
    except:
        print 'BOOM:\t Zll pt correction helper tool NOT LOADED!'
        raise RuntimeError

    try:
        ROOT.PtCorrHelper()
    except:
        print 'Zll pt correction tool not compiled'
        raise RuntimeError

def _load_tth_bdts_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/tth_bdts.cpp', 'k-', '', 'cache')
        print 'BOOM: \t tth bdts loaded!'
    except:
        raise RuntimeError
    
    try:
        ROOT.ttH_BDTs()
    except:
        print "ttH BDTs not compiled"

def _load_vh_tagger_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/vh_tagger.cpp', 'k-', '', 'cache')
        print 'BOOM: \t vh tagger loaded!'
    except:
        raise RuntimeError
    
    try:
        ROOT.VH_Tagger()
    except:
        print "ttH BDTs not compiled"

