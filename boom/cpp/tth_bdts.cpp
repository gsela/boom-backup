#include <TLorentzVector.h>
#include <TMVA/Reader.h>
#include <iostream>

////////////////

class mvaDictTTH
{
public:
  const static int nFolds = 5;
  static TMVA::Reader* reader_tth_tt_0;
  static TMVA::Reader* reader_tth_tt_1;
  static TMVA::Reader* reader_tth_tt_2;
  static TMVA::Reader* reader_tth_tt_3;
  static TMVA::Reader* reader_tth_tt_4;

  static TMVA::Reader* reader_tth_Z_0;
  static TMVA::Reader* reader_tth_Z_1;
  static TMVA::Reader* reader_tth_Z_2;
  static TMVA::Reader* reader_tth_Z_3;
  static TMVA::Reader* reader_tth_Z_4;
    
};

TMVA::Reader* mvaDictTTH::reader_tth_tt_0 = new TMVA::Reader("",true);
TMVA::Reader* mvaDictTTH::reader_tth_tt_1 = new TMVA::Reader("",true);
TMVA::Reader* mvaDictTTH::reader_tth_tt_2 = new TMVA::Reader("",true);
TMVA::Reader* mvaDictTTH::reader_tth_tt_3 = new TMVA::Reader("",true);
TMVA::Reader* mvaDictTTH::reader_tth_tt_4 = new TMVA::Reader("",true);

TMVA::Reader* mvaDictTTH::reader_tth_Z_0 = new TMVA::Reader("",true);
TMVA::Reader* mvaDictTTH::reader_tth_Z_1 = new TMVA::Reader("",true);
TMVA::Reader* mvaDictTTH::reader_tth_Z_2 = new TMVA::Reader("",true);
TMVA::Reader* mvaDictTTH::reader_tth_Z_3 = new TMVA::Reader("",true);
TMVA::Reader* mvaDictTTH::reader_tth_Z_4 = new TMVA::Reader("",true);


namespace ttH_BDTs {

  float tth_bdt_tt_score(float ditau_dr,   
			 float ditau_deta, 
			 float met_reco_et, 
			 float mWbest,      
			 float HTjets,     
			 float mTopWbest,  
			 float ditau_pt,    
			 float jjdrmin,
                         float mva_random_number
			 ){
    
    std::vector<double> inputs;
    inputs.push_back((double)ditau_dr);
    inputs.push_back((double)ditau_deta);
    inputs.push_back((double)(met_reco_et));
    inputs.push_back((double)((mWbest>0)*mWbest));
    inputs.push_back((double)HTjets);
    inputs.push_back((double)((mTopWbest>0)*mTopWbest));
    inputs.push_back((double)ditau_pt);
    inputs.push_back((double)jjdrmin);

    int _split = int(mva_random_number*1000)%int(mvaDictTTH::nFolds);

    float ret = -1;
    switch(_split){
      case 0: ret = mvaDictTTH::reader_tth_tt_0->EvaluateMVA(inputs, "BDTG method"); break;
      case 1: ret = mvaDictTTH::reader_tth_tt_1->EvaluateMVA(inputs, "BDTG method"); break;
      case 2: ret = mvaDictTTH::reader_tth_tt_2->EvaluateMVA(inputs, "BDTG method"); break;
      case 3: ret = mvaDictTTH::reader_tth_tt_3->EvaluateMVA(inputs, "BDTG method"); break;
      case 4: ret = mvaDictTTH::reader_tth_tt_4->EvaluateMVA(inputs, "BDTG method"); break;
      }
          
    return ret;
  }
  
  float tth_bdt_Z_score(float met_reco_et, 
			float tau_pt_1,
			float mWbest,
			float mTopWbest,
			float SumPtBjet,
			float ditau_deta,
			float tau_eta_0,
			float jet_1_eta,
			float jet_0_eta,
			float ditau_met_min_dphi,
			float HTjets,
			float mva_random_number
			){
    

    std::vector<double> inputs;


    inputs.push_back((double)met_reco_et);
    inputs.push_back((double)tau_pt_1);
    inputs.push_back((double)((mWbest>0)*mWbest));
    inputs.push_back((double)((mTopWbest>0)*mTopWbest));
    inputs.push_back((double)SumPtBjet);
    inputs.push_back((double)ditau_deta);
    inputs.push_back((double)tau_eta_0);
    inputs.push_back((double)jet_1_eta);
    inputs.push_back((double)jet_0_eta);
    inputs.push_back((double)ditau_met_min_dphi);
    inputs.push_back((double)HTjets);
    
    int _split = int(mva_random_number*1000)%int(mvaDictTTH::nFolds);

    float ret = -1;
    switch(_split){
    case 0: ret = mvaDictTTH::reader_tth_Z_0->EvaluateMVA(inputs, "BDTG method"); break;
    case 1: ret = mvaDictTTH::reader_tth_Z_1->EvaluateMVA(inputs, "BDTG method"); break;
    case 2: ret = mvaDictTTH::reader_tth_Z_2->EvaluateMVA(inputs, "BDTG method"); break;
    case 3: ret = mvaDictTTH::reader_tth_Z_3->EvaluateMVA(inputs, "BDTG method"); break;
    case 4: ret = mvaDictTTH::reader_tth_Z_4->EvaluateMVA(inputs, "BDTG method"); break;
    }
    
    return ret;
  }


}// end the namespace
