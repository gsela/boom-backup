from ROOT import *
from utils import *
from array import array
from random import randint
import math

def gettrunchist(h,truncbins=[0,1]): 
    # Get clone of hist without truncbins[0] first bins and truncbins[1] last bins
    edges=[]
    ax=h.GetXaxis()
    nbins=ax.GetNbins()
    for b in range(truncbins[0]+1,nbins+2-truncbins[1]):
        edges.append(ax.GetBinLowEdge(b))
    hnew=make_hist([h.GetName()+"_trunc","",len(edges)-1,array('f',edges)])
    for i,b in enumerate(range(truncbins[0]+1,nbins+2-truncbins[1])):
        hnew.SetBinContent(i,h.GetBinContent(b))
        hnew.SetBinError(i,h.GetBinError(b))
    return hnew

def check_setup_rucio():
    out,err,rc=cmdline("rucio whoami")
    return (rc==0)

def get_x2(h1,h2):
    nx=h1.GetNbinsX()
    summ=0
    for bx in range(1,nx+1):
        val1=h1.GetBinContent(bx)
        val2=h2.GetBinContent(bx)
        if val1==0 and val2==0:
            continue
        # summ+=(val1-val2)**2/val2**2
        err1=h1.GetBinError(bx)
        err2=h2.GetBinError(bx)
        summ+=(val1-val2)**2/(err1**2+err2**2)
    return summ/float(nx)
    

def get_eff_hist(hnum,hden):
    # For now assuming 2D
    hnum=getnonzerobinhist(hnum)
    hden=getnonzerobinhist(hden)
    hnum,hden=getcommonbinhists(hnum,hden,False)
    axx=hnum.GetXaxis()
    axy=hnum.GetYaxis()
    nx=axx.GetNbins()
    ny=axy.GetNbins()
    for bx in range(0,nx+2):
        for by in range(0,ny+2):
            if hnum.GetBinContent(bx,by)>hden.GetBinContent(bx,by):
                warn("More events in num than den! %s / %s"%(hnum.GetBinContent(bx,by),hden.GetBinContent(bx,by)))
                print "  binx: %s - [%s, %s)"%(bx,axx.GetBinLowEdge(bx),axx.GetBinUpEdge(bx))
                print "  biny: %s - [%s, %s)"%(by,axy.GetBinLowEdge(by),axy.GetBinUpEdge(by))
                bynew=hnum.GetYaxis().FindBin(-hnum.GetYaxis().GetBinCenter(by))
                if hnum.GetBinContent(bx,by)+hnum.GetBinContent(bx,bynew)<hden.GetBinContent(bx,by)+hden.GetBinContent(bx,bynew):
                    print "  PATCH: Adding the contents of bin -by: %s / %s"%(hnum.GetBinContent(bx,bynew),hden.GetBinContent(bx,bynew))
                    hnum.SetBinContent(bx,by,hnum.GetBinContent(bx,by)+hnum.GetBinContent(bx,bynew))
                    hden.SetBinContent(bx,by,hden.GetBinContent(bx,by)+hden.GetBinContent(bx,bynew))
                elif hnum.GetBinContent(bx,bynew)<hden.GetBinContent(bx,bynew):
                    print "  PATCH: Switching the contents to bin -by: %s / %s"%(hnum.GetBinContent(bx,bynew),hden.GetBinContent(bx,bynew))
                    hnum.SetBinContent(bx,by,hnum.GetBinContent(bx,bynew))
                    hden.SetBinContent(bx,by,hden.GetBinContent(bx,bynew))
                else:
                    print "  PATCH: Switching the contents between num and den..."
                    hnum.SetBinContent(bx,by,hden.GetBinContent(bx,by))
                    hden.SetBinContent(bx,by,hnum.GetBinContent(bx,by))
    heff=grab_hist(hnum)
    heff.Divide(hden)
    teff=TEfficiency(hnum,hden)
    teff.SetStatisticOption(TEfficiency.kFNormal)
    for bx in range(0,nx+2):
        for by in range(0,ny+2):
            heff.SetBinError(bx,by,teff.GetEfficiencyErrorUp(teff.GetGlobalBin(bx,by)))
    return heff

def print_yields(f,h,doprint=True,verbose=True):
    if isinstance(f,basestring):
        f=TFile.Open(f)
    if isinstance(h,basestring):
        h=f.Get(h)
    if doprint and verbose:
        print "Raw events, weighted events: ",
    raw=h.GetEntries()
    weighted=h.Integral(-1,-1)
    if doprint:
        print "%.0f, %.2f"%(raw,weighted)
    return raw,weighted

def treedraw(tree,draw,weight="1",cut="",hlist=None,style="",show=False):
    if cut!="":
        weight="(%s)*(%s)"%(weight,cut)
    if not show:
        style+=" goff"
    if hlist!=None:
        draw+=">>%s(%s,%s,%s)"%(hlist[0],hlist[1],hlist[2],hlist[3])
    tree.Draw(draw,weight,style)
    if hlist==None:
        h=gPad.GetPrimitive("htemp")
    else:
        h=gDirectory.Get(hlist[0])
    h.SetDirectory(0)
    return h
    
def make_hist(hlist):
    if len(hlist)==5: #TH1F normal
        return TH1F(hlist[0],hlist[1],hlist[2],hlist[3],hlist[4])
    elif len(hlist)==8 and not isinstance(hlist[3],array): #TH2F normal
        return TH2F(hlist[0],hlist[1],hlist[2],hlist[3],hlist[4],hlist[5],hlist[6],hlist[7])
    elif len(hlist)==11: #TH3F normal
        return TH3F(hlist[0],hlist[1],hlist[2],hlist[3],hlist[4],hlist[5],hlist[6],hlist[7],hlist[8],hlist[9],hlist[10])
    elif len(hlist)==4: #TH1F edges
        return TH1F(hlist[0],hlist[1],hlist[2],hlist[3]) 
    elif len(hlist)==6: #TH2F edges
        return TH2F(hlist[0],hlist[1],hlist[2],hlist[3],hlist[4],hlist[5])
    elif len(hlist)==8: #TH3F edges
        return TH3F(hlist[0],hlist[1],hlist[2],hlist[3],hlist[4],hlist[5],hlist[6],hlist[7])

def make_dir(f,name):
    newdir=f.mkdir(name)
    return newdir
    
def getcommonbins(ax1,ax2,doprint=True):
    bins1=[]
    for b1 in range(1,ax1.GetNbins()+1):
        binlow1=ax1.GetBinLowEdge(b1)
        binup1=ax1.GetBinUpEdge(b1)
        bins1.append("%.2f,%.2f"%(binlow1,binup1))
    bins2=[]
    for b2 in range(1,ax2.GetNbins()+1):
        binlow2=ax2.GetBinLowEdge(b2)
        binup2=ax2.GetBinUpEdge(b2)
        bins2.append("%.2f,%.2f"%(binlow2,binup2))
    common=[b for b in bins1 if b in bins2]
    for bins in [bins1,bins2]:
        for b in [k for k in bins]:
            if b in common:
                bins.remove(b)
    if bins1!=[] or bins2!=[]:
        if doprint:
            print "Common bins found"
            pretty_print(common)
            if bins1!=[]:
                print "remove from list1"
                pretty_print(bins1)
            if bins2!=[]:
                print "remove from list2"
                pretty_print(bins2)
    axbins=array('d',[])
    for b in common:
        [low,up]=[float(k) for k in b.split(',')]
        if b!=common[0] and low!=lastup:
            warn("Hole in bins, bridging for now",common)
            axbins.append(lastup)
        axbins.append(low)
        if b==common[-1]:
            axbins.append(up)
        lastup=up
    return axbins,len(axbins)-1

def findbininax(ax,val):
    return ax.FindBin(val)
    # nbins=ax.GetNbins()
    # if val<ax.GetBinLowEdge(1):
    #     ans=0
    # elif val>=ax.GetBinLowEdge(nbins+1):
    #     ans=nbins+1
    # else:
    #     ans=None
    #     for b in range(1,nbins+1):
    #         if ax.GetBinLowEdge(b)<=val<ax.GetBinUpEdge(b):
    #             ans=b
    #             break
    # return ans

def printbins(h):
    ax=h.GetXaxis()
    print "title: ",ax.GetTitle()
    for b in range(ax.GetNbins()+2):
        print b,getbinedges(ax,b),h.GetBinContent(b)
    
def printaxisbins(ax):
    print "title: ",ax.GetTitle()
    for b in range(ax.GetNbins()+2):
        print b,getbinedges(ax,b)
        
def getbinedges(ax,b):
    # print b
    return ax.GetBinLowEdge(b),ax.GetBinUpEdge(b)

def seterrorsfromhist(h,hstat,hsyst=None):
    h.Sumw2()
    xax=h.GetXaxis()
    yax=h.GetYaxis()
    zax=h.GetZaxis()
    nx=xax.GetNbins()
    ny=yax.GetNbins()
    nz=zax.GetNbins()
    if nz>1:
        for bx in range(1,nx+1):
            for by in range(1,ny+1):
                for bz in range(1,nz+1):
                    if hsyst!=None:
                        err=(hstat.GetBinContent(bx,by,bz)**2+hsyst.GetBinContent(bx,by,bz)**2)**0.5
                    else:
                        err=hstat.GetBinContent(bx,by,bz)
                    h.SetBinError(bx,by,bz,err)
    elif ny>1:
        for bx in range(1,nx+1):
            for by in range(1,ny+1):
                if hsyst!=None:
                    err=(hstat.GetBinContent(bx,by)**2+hsyst.GetBinContent(bx,by)**2)**0.5
                else:
                    err=hstat.GetBinContent(bx,by)
                h.SetBinError(bx,by,err)
    else:
        for bx in range(1,nx+1):
            if hsyst!=None:
                err=(hstat.GetBinContent(bx)**2+hsyst.GetBinContent(bx)**2)**0.5
            else:
                err=hstat.GetBinContent(bx)
            h.SetBinError(bx,err)

def getnonzerobins(h):
    ax=h.GetXaxis()
    n=ax.GetNbins()
    binsmin=[]
    binsmax=[]
    foundmin=False
    foundmax=False
    for b in range(1,n+1):
        if foundmin or h.GetBinContent(b)>0:
            foundmin=True
            binsmin.append(b)
        B=n-b+1
        if B<=b:
            break
        if foundmax or h.GetBinContent(B)>0:
            foundmax=True
            binsmax.append(B)
    bins=list(set(binsmin+binsmax))
    bins.sort()
    axbins=array('d',[])
    for b in bins:
        axbins.append(ax.GetBinLowEdge(b))
        if b==bins[-1]:
            axbins.append(ax.GetBinUpEdge(b))
    return axbins,len(axbins)-1
            
def getnonzerobinhist(h):
    xax1=h.GetXaxis()
    yax1=h.GetYaxis()
    zax1=h.GetZaxis()
    nx1=xax1.GetNbins()
    ny1=yax1.GetNbins()
    nz1=zax1.GetNbins()
    if nz1>1:
        xaxarr,nx=getnonzerobins(h.ProjectionX())
        yaxarr,ny=getnonzerobins(h.ProjectionY())
        zaxarr,nz=getnonzerobins(h.ProjectionZ())
        hn=TH3F(h.GetName()+"%s"%randint(0,10000000),h.GetTitle(),nx,xaxarr,ny,yaxarr,nz,zaxarr)
        hn.Sumw2()
        xax=hn.GetXaxis()
        yax=hn.GetYaxis()
        zax=hn.GetZaxis()
        nx=xax.GetNbins()
        ny=yax.GetNbins()
        nz=zax.GetNbins()
        for bx in range(1,nx+1):
            for by in range(1,ny+1):
                for bz in range(1,nz+1):
                    bx1=findbininax(xax1,xax.GetBinCenter(bx))
                    by1=findbininax(yax1,yax.GetBinCenter(by))
                    bz1=findbininax(zax1,zax.GetBinCenter(bz))
                    hn.SetBinContent(bx,by,bz,h.GetBinContent(bx1,by1,bz1))
                    hn.SetBinError(bx,by,bz,h.GetBinError(bx1,by1,bz1))
    elif ny1>1:
        xaxarr,nx=getnonzerobins(h.ProjectionX())
        yaxarr,ny=getnonzerobins(h.ProjectionY())
        # print h.GetName()+"%s"%randint(0,10000000),h.GetTitle(),nx,xaxarr,ny,yaxarr
        hn=TH2F(h.GetName()+"%s"%randint(0,10000000),h.GetTitle(),nx,xaxarr,ny,yaxarr)
        hn.Sumw2()
        xax=hn.GetXaxis()
        yax=hn.GetYaxis()
        nx=xax.GetNbins()
        ny=yax.GetNbins()
        for bx in range(1,nx+1):
            for by in range(1,ny+1):
                bx1=findbininax(xax1,xax.GetBinCenter(bx))
                by1=findbininax(yax1,yax.GetBinCenter(by))
                hn.SetBinContent(bx,by,h.GetBinContent(bx1,by1))
                hn.SetBinError(bx,by,h.GetBinError(bx1,by1))
    else:
        xaxarr,nx=getnonzerobins(h.ProjectionX())
        hn=TH1F(h.GetName()+"%s"%randint(0,10000000),h.GetTitle(),nx,xaxarr)
        hn.Sumw2()
        xax=hn.GetXaxis()
        nx=xax.GetNbins()
        for bx in range(1,nx+1):
            bx1=findbininax(xax1,xax.GetBinCenter(bx))
            hn.SetBinContent(bx,by,h.GetBinContent(bx1,by1))
            hn.SetBinError(bx,by,h.GetBinError(bx1,by1))
    return hn
    
def getcommonbinhists(h1,h2,doprint=True):
    xax1=h1.GetXaxis()
    yax1=h1.GetYaxis()
    zax1=h1.GetZaxis()
    nx1=xax1.GetNbins()
    ny1=yax1.GetNbins()
    nz1=zax1.GetNbins()
    xax2=h2.GetXaxis()
    yax2=h2.GetYaxis()
    zax2=h2.GetZaxis()
    nx2=xax2.GetNbins()
    ny2=yax2.GetNbins()
    nz2=zax2.GetNbins()
    if nz1>1:
        xaxarr,nx=getcommonbins(xax1,xax2)
        yaxarr,ny=getcommonbins(yax1,yax2)
        zaxarr,nz=getcommonbins(zax1,zax2)
        h1n=TH3F(h1.GetName()+"%s"%randint(0,10000000),h1.GetTitle(),nx,xaxarr,ny,yaxarr,nz,zaxarr)
        h1n.Sumw2()
        h2n=TH3F(h2.GetName()+"%s"%randint(0,10000000),h2.GetTitle(),nx,xaxarr,ny,yaxarr,nz,zaxarr)
        h2n.Sumw2()
        xax=h1n.GetXaxis()
        yax=h1n.GetYaxis()
        zax=h1n.GetZaxis()
        nx=xax.GetNbins()
        ny=yax.GetNbins()
        nz=zax.GetNbins()
        for bx in range(1,nx+1):
            for by in range(1,ny+1):
                for bz in range(1,nz+1):
                    bx1=findbininax(xax1,xax.GetBinCenter(bx))
                    bx2=findbininax(xax2,xax.GetBinCenter(bx))
                    by1=findbininax(yax1,yax.GetBinCenter(by))
                    by2=findbininax(yax2,yax.GetBinCenter(by))
                    bz1=findbininax(zax1,zax.GetBinCenter(bz))
                    bz2=findbininax(zax2,zax.GetBinCenter(bz))
                    h1n.SetBinContent(bx,by,bz,h1.GetBinContent(bx1,by1,bz1))
                    h2n.SetBinContent(bx,by,bz,h2.GetBinContent(bx2,by2,bz2))
                    h1n.SetBinError(bx,by,bz,h1.GetBinError(bx1,by1,bz1))
                    h2n.SetBinError(bx,by,bz,h2.GetBinError(bx2,by2,bz2))
    elif ny1>1:
        xaxarr,nx=getcommonbins(xax1,xax2)
        yaxarr,ny=getcommonbins(yax1,yax2)
        h1n=TH2F(h1.GetName()+"%s"%randint(0,10000000),h1.GetTitle(),nx,xaxarr,ny,yaxarr)
        h1n.Sumw2()
        h2n=TH2F(h2.GetName()+"%s"%randint(0,10000000),h2.GetTitle(),nx,xaxarr,ny,yaxarr)
        h2n.Sumw2()
        xax=h1n.GetXaxis()
        yax=h1n.GetYaxis()
        nx=xax.GetNbins()
        ny=yax.GetNbins()
        for bx in range(1,nx+1):
            for by in range(1,ny+1):
                bx1=findbininax(xax1,xax.GetBinCenter(bx))
                bx2=findbininax(xax2,xax.GetBinCenter(bx))
                by1=findbininax(yax1,yax.GetBinCenter(by))
                by2=findbininax(yax2,yax.GetBinCenter(by))
                h1n.SetBinContent(bx,by,h1.GetBinContent(bx1,by1))
                h2n.SetBinContent(bx,by,h2.GetBinContent(bx2,by2))
                h1n.SetBinError(bx,by,h1.GetBinError(bx1,by1))
                h2n.SetBinError(bx,by,h2.GetBinError(bx2,by2))
    else:
        xaxarr,nx=getcommonbins(xax1,xax2)
        h1n=TH1F(h1.GetName()+"%s"%randint(0,10000000),h1.GetTitle(),nx,xaxarr)
        h1n.Sumw2()
        h2n=TH1F(h2.GetName()+"%s"%randint(0,10000000),h2.GetTitle(),nx,xaxarr)
        h2n.Sumw2()
        xax=h1n.GetXaxis()
        nx=xax.GetNbins()
        for bx in range(1,nx+1):
            bx1=findbininax(xax1,xax.GetBinCenter(bx))
            bx2=findbininax(xax2,xax.GetBinCenter(bx))
            h1n.SetBinContent(bx,h1.GetBinContent(bx1))
            h2n.SetBinContent(bx,h2.GetBinContent(bx2))
            h1n.SetBinError(bx,h1.GetBinError(bx1))
            h2n.SetBinError(bx,h2.GetBinError(bx2))
    return h1n,h2n

def getnonzerorange(hists):
    ax=hists[0].GetXaxis()
    n=ax.GetNbins()
    xmin=None
    for b in range(0,n+2):
        if xmin!=None:
            break
        for h in hists:
            if h.GetBinContent(b)>0:
                xmin=ax.GetBinLowEdge(b)
    xmax=None
    for b1 in range(0,n+2):
        if xmax!=None:
            break
        b=n+1-b1
        for h in hists:
            if h.GetBinContent(b)>0:
                xmax=ax.GetBinUpEdge(b)
    return xmin,xmax

def varbinwidthlabels(h,skiplastbin=True):
    xax=h.GetXaxis()
    xax.SetLabelSize(0)
    xax.SetTickLength(0)
    ymin=h.GetMinimum()
    ymax=h.GetMaximum()
    dy=ymax-ymin
    nbins=h.GetNbinsX() if skiplastbin else h.GetNbinsX()+1
    labs=[]
    ticks=[]
    for b in range(1,nbins+1):
        x=xax.GetBinLowEdge(b)
        # if x%1000==0:
        #     x=x/1000
        t=TLatex(x,ymin-0.05*dy,"%.0f"%(x/1000))
        t.SetTextSize(0.033)
        t.SetTextFont(42)
        t.SetTextAlign(21)
        l=TLine(x,ymin,x,ymin+0.03*dy)
        labs.append(l)
        ticks.append(t)
    return h,labs,ticks
    
def showplots(objs,titles=[],savefile=None,legside='right',xtitle=0,ytitle=0,title=0,range_by_error=True,xmax=None,ylims=[],dovarbinwidthlabels=False):
    colors=[kBlack,kBlue,kRed,kGreen,kOrange,kViolet,kGreen+3,kCyan]
    c=TCanvas("c","")
    if range_by_error:
        objs[0].Draw()
    else:
        objs[0].Draw("hist")
    if ylims==[]:
        warn("using only N-2 bins to get min/max")
        maxv=0#objs[0].GetMaximum()
        minv=1e99#objs[0].GetMinimum()
        for obj in objs:
            for b in range(1,obj.GetNbinsX()-1): #PATCH FOR EFF HISTS
                if xmax!=None and obj.GetXaxis().GetBinLowEdge(b+2)>=xmax:
                    break
            # for b in range(1,obj.GetNbinsX()+1):
                # if xmax!=None and obj.GetXaxis().GetBinLowEdge(b)>=xmax:
                #     break
                if range_by_error:
                    up=obj.GetBinContent(b)+obj.GetBinError(b)
                    low=obj.GetBinContent(b)-obj.GetBinError(b)
                else:
                    up=obj.GetBinContent(b)
                    low=obj.GetBinContent(b)
                if up!=0 and up>maxv:
                    # print "up",b,up
                    maxv=up
                if low!=0 and low<minv:
                    # print "low",b,low
                    minv=low
            if range_by_error:
                obj.Draw("same")
            else:
                # obj.Draw("hist same")
                obj.Draw("same")
        newmax=maxv+0.25*(maxv-minv)/0.7 # max at 1/4 of canvas, min at 5% of canvas, 0.7=1-0.25-0.05
        newmin=minv-0.05*(maxv-minv)/0.7
        objs[0].SetMinimum(newmin)
        objs[0].SetMaximum(newmax)
    else:
        for obj in objs:
            obj.Draw("same")
        objs[0].SetMinimum(ylims[0])
        objs[0].SetMaximum(ylims[1])
    if xmax!=None:
        objs[0].GetXaxis().SetRangeUser(objs[0].GetXaxis().GetBinLowEdge(1),xmax)
    # print minv,maxv
    # print newmin,newmax
    # objs[0].SetMinimum(minv)
    # objs[0].SetMaximum(maxv)
    if dovarbinwidthlabels:
        objs[0],labs,ticks=varbinwidthlabels(objs[0])
    else:
        labs,ticks=[[],[]]
    for l,t in zip(labs,ticks):
        l.Draw('same')
        t.Draw('same')
    for i in range(len(objs)):
        try:
            objs[i].SetLineColor(colors[i])
            objs[i].SetLineWidth(2)
        except:
            pass
    # objs[0].SetLineWidth(3)
    if titles!=[]:
        if legside=='right':
            leg=TLegend(.8,.71,1,.9)
        elif legside=='left':
            leg=TLegend(.1,.7,.3,.9)
        elif legside=='down':
            leg=TLegend(.78,.11,.89,.31)
        for i in range(len(objs)):
            leg.AddEntry(objs[i],titles[i],'l')
        leg.Draw('same')
    if xtitle!=0:
        objs[0].GetXaxis().SetTitle(xtitle)
        objs[0].GetXaxis().SetTitleSize(23)
        objs[0].GetXaxis().SetTitleFont(43)
        objs[0].GetXaxis().SetTitleOffset(.87)
    if ytitle!=0:
        objs[0].GetYaxis().SetTitle(ytitle)
        objs[0].GetYaxis().SetTitleSize(25)
        objs[0].GetYaxis().SetTitleFont(43)
        objs[0].GetYaxis().SetTitleOffset(.9)
    if title!=0:
        objs[0].SetTitle(title)
    if savefile is None:
        c.Modified()
        c.Update()
        raw_input()
    else:
        c.SaveAs(savefile)
    c.Close()

def comp_plot2D(h1,h2,title='',xtitle='',ytitle='',minx=None,maxx=None,draw=True,zrange=[],returnaxranges=False):
    if draw:
        c=TCanvas("c","")
    h1,h2=getcommonbinhists(h1,h2)
    hr=get_ratio_hist(h1,h2)
    xax=hr.GetXaxis()
    hr.SetTitle(title)
    xax.SetTitle(xtitle)
    hr.GetYaxis().SetTitle(ytitle)
    xmin,xmax=getnonzerorange([hr.ProjectionX()])
    if minx:
        xmin=max(xmin,minx)
    if maxx:
        xmax=min(xmax,maxx)
    xax.SetRangeUser(xmin,xmax)
    ymin,ymax=getnonzerorange([hr.ProjectionY("somename",findbininax(xax,xmin),findbininax(xax,xmax))])
    hr.GetYaxis().SetRangeUser(ymin,ymax)
    if zrange==[]:
        maxdiff=0
        for bx in range(findbininax(xax,xmin),findbininax(xax,xmax)):
            for by in range(1,hr.GetYaxis().GetNbins()+1):
                if hr.GetBinContent(bx,by)<=0:
                    continue
                maxdiff=max(maxdiff,abs(hr.GetBinContent(bx,by)-1))
        zrange=[1-maxdiff,1+maxdiff]
    hr.SetMinimum(zrange[0])
    hr.SetMaximum(zrange[1])
    if draw:
        hr.Draw("colz")
        raw_input()
        if returnaxranges:
            return xmin,xmax,ymin,ymax
    else:
        if returnaxranges:
            return hr,xmin,xmax,ymin,ymax
        else:
            return hr
    
def comp_plot(h1,h2,title='',labels=[],xtitle='',maxx=None,miny=None,maxy=None,minr=None,maxr=None,p1=None,p2=None):
    draw=True if p1==None else False
    h1,h2=getcommonbinhists(h1,h2)
    if draw:
        c=TCanvas("c","")
        p1=TPad("p1","",0,.3,1,1)
        p2=TPad("p2","",0,0.05,1,.3)
        p1.Draw()
        p2.Draw()
    p1.SetMargin(0.12,0.01,0.06,0.01)
    p2.SetMargin(0.12,0.01,0.15,0.06)
    if miny==None or maxy==None:
        minv=max(h1.GetMaximum(),h2.GetMaximum())
        maxv=min(h1.GetMinimum(),h2.GetMinimum())
        for b in range(1,h1.GetNbinsX()+1):
            if maxx!=None and h1.GetXaxis().GetBinLowEdge(b)>=maxx:
                continue
            if h1.GetBinContent(b)==0 or h2.GetBinContent(b)==0:
                continue
            maxv=max(maxv,h1.GetBinContent(b)+h1.GetBinError(b),h2.GetBinContent(b)+h2.GetBinError(b))
            minv=min(minv,h1.GetBinContent(b)-h1.GetBinError(b),h2.GetBinContent(b)-h2.GetBinError(b))
    if not miny==None:
        minv=miny
    if not maxy==None:
        maxv=maxy
    newmax=maxv+0.25*(maxv-minv)/0.7 # max at 1/4 of canvas, min at 5% of canvas, 0.7=1-0.25-0.05
    newmin=minv-0.05*(maxv-minv)/0.7
    h1.SetMinimum(newmin)
    h1.SetMaximum(newmax)
    xmin,xmax=getnonzerorange([h1,h2])
    print xmin
    if maxx:
        xmax=min(xmax,maxx)
    h1.GetXaxis().SetRangeUser(xmin,xmax)
    h2.GetXaxis().SetRangeUser(xmin,xmax)
    xax=h1.GetXaxis()
    yax=h1.GetYaxis()
    xax.SetLabelSize(13)
    xax.SetLabelFont(43)
    yax.SetLabelSize(13)
    yax.SetLabelFont(43)
    # h1.SetTitle(title)
    h1.SetTitle('')
    h1.SetLineColor(kRed)
    h1.SetLineWidth(2)
    h2.SetLineColor(kBlue)
    h2.SetLineWidth(2)
    leg=None
    if labels!=[]:
        leg=TLegend(.25,.8,.85,.99)
        leg.SetHeader(title)
        leg.GetListOfPrimitives().First().SetTextAlign(22)
        leg.SetNColumns(2)
        leg.AddEntry(h1,labels[0],'l')
        leg.AddEntry(h2,labels[1],'l')
        if len(labels)==4:
            leg.AddEntry('',labels[2],'')
            leg.AddEntry('',labels[3],'')
    if draw:
        p1.cd()
        h1.Draw()
        h2.Draw("same")
        if leg!=None:
            leg.Draw("same")
    hr=get_ratio_hist(h1,h2)    
    if miny==None or maxy==None:
        maxdiff=0
        for b in range(1,h1.GetNbinsX()-1):
            if hr.GetBinContent(b)==0:
                continue
        maxdiff=max(maxdiff,hr.GetBinContent(b)+hr.GetBinError(b)-1,1-hr.GetBinContent(b)+hr.GetBinError(b))
        maxv=1+maxdiff
        minv=1-maxdiff
    if minr!=None:
        minv=minr
    if maxr!=None:
        maxv=maxr
    print minv,maxv
    hr.SetMinimum(minv)
    hr.SetMaximum(maxv)
    hr.SetMarkerStyle(20)
    hr.SetMarkerColor(1)
    hr.SetLineColor(kBlack)
    hr.SetLineWidth(2)
    xaxr=hr.GetXaxis()
    xaxr.SetTitle(xtitle)
    xaxr.SetTitleSize(0.12)
    xaxr.SetTitleOffset(0.4)
    xaxr.SetLabelSize(0.)
    yaxr=hr.GetYaxis()
    yaxr.SetNdivisions(6,0)
    yaxr.SetLabelSize(13)
    yaxr.SetLabelFont(43)
    p2.SetGridy()
    if draw:
        p2.cd()
        hr.Draw()
        raw_input()
    return h1,h2,leg,hr
    
def make_hist_from_list(lis,nbins=None,xmin=None,xmax=None):
    nbins=20 if nbins is None else nbins
    xmin=min(lis) if xmin is None else xmin
    xmax=max(lis) if xmax is None else xmax
    h=TH1F("htmp","",nbins,xmin,xmax)
    for v in lis:
        h.Fill(v)
    return h
    
def get_leafs_from_tree(treename,files,leafs):
    t=TChain(treename)
    for f in files:
        t.Add(f)
    outdct={}
    pointers={}
    for leaf in leafs:
        outdct[leaf]=[]
        pointers[leaf]=t.GetLeaf(leaf)
        # print leaf,pointers[leaf]
    for i,e in enumerate(t):
        for leaf in leafs:
            try:
                outdct[leaf].append(pointers[leaf].GetValue())
            except:
                warn("Invalid value for leaf, index leaf","%s %s"%(i,leaf))
                continue
    return outdct
    
def rebin_hists(hists,rebin):
    for h in hists:
        h.Rebin(rebin)
        
def get_ratio_hist(h1,h2):
    hr=h1.Clone()
    hr.SetName(h1.GetName()+'_ratio')
    hr.SetDirectory(0)
    hr.Divide(h2)
    return hr
 
def showcanvas(c):
    # gROOT.SetBatch(0)
    c.Modified()
    c.Update()
    raw_input()
    # gROOT.SetBatch(1)

def showplot(h):
    # gROOT.SetBatch(0)
    c=TCanvas("c","")
    h.Draw()
    showcanvas(c)
    # gROOT.SetBatch(1)
    
def grab_hist(h,reset=False):
    hnew=h.Clone()
    hnew.SetDirectory(0)
    if reset:
        hnew.Reset()
    return hnew

def get1DYrange(hists,islog=False):
    ymin=1e20
    ymax=0
    for h in hists:
        ymin=h.GetMinimum() if h.GetMinimum()<ymin else ymin
        ymax=h.GetMaximum() if h.GetMaximum()>ymax else ymax
    h=hists[0]
    # h.SetMinimum(min_offset*ymin)
    # h.SetMaximum(max_offset*ymax)
    return ymin,ymax
    
def get1DXrange(hists,minstat=0.5):
    minstat*=len(hists)
    xmin=-1
    xmax=-1
    h=hists[0].Clone()
    h.SetDirectory(0)
    for hi in hists:
        h.Add(hi)
    xax=h.GetXaxis()
    N=h.GetNbinsX()
    seed_low=0
    seed_high=0
    for b in range(1,N+1):
        if seed_low==0 and h.GetBinContent(b)>minstat:
            seed_low=1
        elif seed_low==1 and h.GetBinContent(b)>minstat:
            seed_low=-1
            xmin=xax.GetBinLowEdge(b-1)
            bmin=b-1
            # print "MIN",bmin,xmin
        elif seed_low==1 and h.GetBinContent(b)<minstat:
            seed_low=0
        # print "low",b,h.GetBinContent(b),h.GetBinContent(b)>minstat,seed_low
        if seed_high==0 and h.GetBinContent(N+1-b)>minstat:
            seed_high=1
        elif seed_high==1 and h.GetBinContent(N+1-b)>minstat:
            seed_high=-1
            xmax=xax.GetBinUpEdge(N+2-b)
            bmax=N+2-b
            # print "MAX",bmax,xmax
        elif seed_high==1 and h.GetBinContent(N+1-b)<minstat:
            seed_high=0
        # print "high",N+1-b,h.GetBinContent(N+1-b),h.GetBinContent(N+1-b)>minstat,seed_high
        if seed_low<0 and seed_high<0:
            break
    # for h in hists:
    #     h.GetXaxis().SetRangeUser(xmin,xmax)
    # print xmin,xmax
    return xmin,xmax

def get2DprojX(h2,h2stat=False,h2syst=False,systistot=False,firstbin=0,lastbin=-1):
    # > h2stat -> False - no weight
    # > h2stat -> True - weight from bin errors
    # > h2stat -> TH2F(stat) - weight from bin values of TH2F
    # > h2stat+h2syst -> TH2F(stat),TH2F(syst) - weight from bin values of h2stat[0], errors from both 
    xax=h2.GetXaxis()
    yax=h2.GetYaxis()
    nx=xax.GetNbins()
    ny=yax.GetNbins()
    h=h2.ProjectionX()
    h.SetDirectory(0)
    h.Reset()
    for bx in range(1,nx+1):
        xcont=0
        xstat=0
        xstat2=0
        xsyst=0
        xwei=0
        xerr=0
        if h2stat==False:
            for by in range(0,ny+2):
                if firstbin>0 and by<firstbin:
                    continue
                if lastbin>0 and by>lastbin:
                    continue
                cont=h2.GetBinContent(bx,by)
                stat=h2.GetBinError(bx,by)
                xcont+=cont
                xstat2+=stat**2
            xerr=xstat2**0.5
        else:
            for by in range(0,ny+2):
                if firstbin>0 and by<firstbin:
                    continue
                if lastbin>0 and by>lastbin:
                    continue
                cont=h2.GetBinContent(bx,by)
                stat=h2.GetBinError(bx,by) if h2stat==True else h2stat.GetBinContent(bx,by)
                if math.isnan(stat) or stat==0:
                    continue
                wei=1/stat**2
                xcont+=cont*wei
                xstat2+=(stat*wei)**2
                xwei+=wei
                if h2syst!=False:
                    syst=h2syst.GetBinContent(bx,by)
                    xsyst+=syst*wei # assuming completely correlated
            if xcont==0:
                continue
            xcont/=xwei
            xstat=(xstat2**0.5)/xwei
            if h2syst!=False:
                xsyst/=xwei
                xerr=(xstat**2 + xsyst**2)**0.5 if systistot==False else xsyst
            else:
                xerr=xstat
        h.SetBinContent(bx,xcont)
        h.SetBinError(bx,xerr)
    return h

def get2DprojY(h2,h2stat=False,h2syst=False,systistot=False,firstbin=0,lastbin=-1):
    # > h2stat -> False - no weight
    # > h2stat -> True - weight from bin errors
    # > h2stat -> TH2F(stat) - weight from bin values of TH2F
    # > h2stat+h2syst -> TH2F(stat),TH2F(syst) - weight from bin values of h2stat[0], errors from both 
    xax=h2.GetXaxis()
    yax=h2.GetYaxis()
    nx=xax.GetNbins()
    ny=yax.GetNbins()
    h=h2.ProjectionY()
    h.SetDirectory(0)
    h.Reset()
    for by in range(1,ny+1):
        ycont=0
        ystat=0
        ystat2=0
        ysyst=0
        ywei=0
        yerr=0
        if h2stat==False:
            for bx in range(0,nx+2):
                if firstbin>0 and bx<firstbin:
                    continue
                if lastbin>0 and bx>lastbin:
                    continue
                cont=h2.GetBinContent(bx,by)
                stat=h2.GetBinError(bx,by)
                ycont+=cont
                ystat2+=stat**2
            yerr=ystat2**0.5
        else:
            for bx in range(0,nx+2):
                if firstbin>0 and bx<firstbin:
                    continue
                if lastbin>0 and bx>lastbin:
                    continue
                cont=h2.GetBinContent(bx,by)
                stat=h2.GetBinError(bx,by) if h2stat==True else h2stat.GetBinContent(bx,by)
                if math.isnan(stat) or stat==0:
                    continue
                wei=1/stat**2
                ycont+=cont*wei
                ystat2+=(stat*wei)**2
                ywei+=wei
                if h2syst!=False:
                    syst=h2syst.GetBinContent(bx,by)
                    ysyst+=syst*wei # assuming completely correlated
            if ycont==0:
                continue
            ycont/=ywei
            ystat=(ystat2**0.5)/ywei
            if h2syst!=False:
                ysyst/=ywei
                yerr=(ystat**2 + ysyst**2)**0.5 if systistot==False else ysyst
            else:
                yerr=ystat
        h.SetBinContent(by,ycont)
        h.SetBinError(by,yerr)
    return h
