"""
"""
import uuid
import ROOT
from happy.plot import Plot, TextElement
from happy.plotDecorator import AtlasTitleDecorator

from ..selection_utils import filter_selections
from ..utils import blinded_hist, yield_table, lumi_calc, plot_title_extension, hist_max

def make_stxs_bin_comparison_plot(
    processor,
    selections,
    variable,
    print_lumi=True,    
    **kwargs):
    """
    """
    _sels = filter_selections(selections, **kwargs)
    _title, _ext = plot_title_extension(_sels, **kwargs)
    
    plot = Plot(
        uuid.uuid4().hex,
        variable)

    for ip, process in enumerate(processor.physics_processes):
        h = processor.get_hist_physics_process(process.name, _sels, variable)
        h.SetTitle(process.name)
        h.SetLineColor(ip + 1)
        plot.addHistogram(h)

    plot.drawStatError = True

    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.02
    plot.legendDecorator.maxEntriesPerColumn = 5

    te = TextElement(_title, x=0.26, y=0.96)
    te.size = 0.8 * te.size
    plot.textElements.append(te)

    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )

    plot.draw()
    plot.saveAs('./plots/plot_compare_stxs_{}_{}.pdf'.format(variable.name, _ext))


def compare_selections_plot(
    processor,
    process_name,
    selections_1,
    selections_2,
    variable,
    sel_names=('vbf_0', 'vbf_1'),
    print_lumi=True,    
    **kwargs):
    """
    """
    print 'BOOM: compare_selections_plot for', variable.name
    if len(sel_names) != 2:
        raise ValueError
    
    _sels_1 = filter_selections(selections_1, **kwargs)
    _sels_2 = filter_selections(selections_2, **kwargs)
    _title, _ext = plot_title_extension(_sels_1 + _sels_2, **kwargs)
    
    h_sels_1 = processor.get_hist_physics_process(process_name, _sels_1, variable)
    h_sels_2 = processor.get_hist_physics_process(process_name, _sels_2, variable)

    h_sels_1.SetTitle(sel_names[0])
    h_sels_2.SetTitle(sel_names[1])
    
    if h_sels_1.Integral() != 0:
        h_sels_1.Scale(1. / h_sels_1.Integral())

    if h_sels_2.Integral() != 0:
        h_sels_2.Scale(1. / h_sels_2.Integral())

    h_sels_1.SetLineColor(ROOT.kBlue)
    h_sels_2.SetLineColor(ROOT.kRed)

    plot = Plot(
        uuid.uuid4().hex,
        variable)
    plot.addHistogram(h_sels_1)
    plot.addHistogram(h_sels_2)
    plot.drawStatError = True

    if print_lumi:
        luminosity = lumi_calc(_sels_1 + _sels_2)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )

    plot.titles.append('{}'.format(process_name))
    plot.draw()
    plot.saveAs('./plots/plot_compare_selections_{}_{}_{}_vs_{}.pdf'.format(variable.name, process_name, sel_names[0], sel_names[1]))
