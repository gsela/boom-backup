"""
"""
import ROOT
import uuid
# HAPPy imports
from happy.plot import Plot, TextElement
from happy.dataMcPlot import DataMcPlot
from happy.dataMcRatioPlot import DataMcRatioPlot
from happy.variable import Binning, Variable, var_Ratio
from happy.style import Style
from happy.plotDecorator import AtlasTitleDecorator

from ..selection_utils import filter_selections
from ..utils import blinded_hist, yield_table, lumi_calc, plot_title_extension, hist_max

def make_ff_plot(
    selections,
    hist,
    variable, 
    region,
    print_lumi=True,
    **kwargs):
    """
    """
    _sels = filter_selections(selections, **kwargs)
    _title, _ext = plot_title_extension(_sels, **kwargs)

    print 'BOOM: make FF plot for', _title, variable.name

    #     maxi = hist_max(hist_list)
    maxi = 1.2 # hack


    var_ff = Variable('var_ff', '', 'FF_{'+region+'}', '', Binning(1, 0, 1.2 * maxi))
    plot = Plot(
        uuid.uuid4().hex,
        variable,
        var_ff)

    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 3
    plot.showBinWidthY = False
    te = TextElement(_title, x=0.26, y=0.96)
    print te.size
    te.size = 0.8 * te.size
    plot.textElements.append(te)
   
    hist.SetTitle('FF_{'+region+'}')
    hist.SetLineColor(ROOT.kRed)
    hist.SetMarkerColor(ROOT.kRed)
    plot.addHistogram(hist, 'PE')

    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )
    plot.draw()
    plot.saveAs('plots/FF_{}_{}_{}.pdf'.format(region, variable.name, _ext))
