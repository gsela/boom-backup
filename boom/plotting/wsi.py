import ROOT
import uuid

def _decorate_hist(hist, sample):
    hist.SetMarkerStyle(sample.nominal.GetMarkerStyle())
    hist.SetMarkerColor(sample.nominal.GetMarkerColor())
    hist.SetMarkerSize(sample.nominal.GetMarkerSize())
    hist.SetLineStyle(sample.nominal.GetLineStyle())
    hist.SetLineWidth(sample.nominal.GetLineWidth())
    hist.SetLineColor(sample.nominal.GetLineColor())
    hist.SetFillStyle(sample.nominal.GetFillStyle())
    hist.SetFillColor(sample.nominal.GetFillColor())


def plot_z_migration(wsi, finalstate='hh', cat_key='boost', corrected_zcr=False, zcr_channel=None):
    _channels = filter(lambda _c: _c.finalstate == finalstate and cat_key in _c.category, wsi.channels)
    _srs = filter(lambda _c: _c.sr, _channels)
    _crs = filter(lambda _c: _c.cr and 'cr_ztt' in _c.region, _channels)

    if corrected_zcr:
        _crs = filter(lambda _c: _c.cr and 'corr' in _c.region, _channels)
    else:
        _crs = filter(lambda _c: _c.cr and not 'corr' in _c.region, _channels)

    if zcr_channel != None:
        _crs = filter(lambda _c: zcr_channel in _c.region, _crs)
        
    

    _recon_cats = sorted([_c.category for _c in _srs])
    _truth_cats = [
        'truth_0',
        'truth_1',
        'truth_2',
        'truth_3',
        ]


    # Ztt
    _map_ztt = ROOT.TH2F(
        'bla', '', 
        len(_truth_cats), 0, len(_truth_cats), 
        len(_recon_cats), 0, len(_recon_cats))
    for i_x in xrange(_map_ztt.GetNbinsX()):
        _map_ztt.GetXaxis().SetBinLabel(i_x + 1, _truth_cats[i_x])

    for i_y in xrange(_map_ztt.GetNbinsY()):
        _map_ztt.GetYaxis().SetBinLabel(i_y + 1, _recon_cats[i_y])
                     
    _map_ztt.GetXaxis().SetTitle('Truth p_{T}(Z) Binning')
    _map_ztt.GetYaxis().SetTitle('Reconstructed Category')
    _map_ztt.GetYaxis().SetTitleOffset(1.55 * _map_ztt.GetYaxis().GetTitleOffset())
    _map_ztt.GetZaxis().SetRangeUser(0, 1)
    _map_ztt.GetXaxis().ChangeLabel(1, -1, -1, -1, -1, -1,'[100, 120]')
    _map_ztt.GetXaxis().ChangeLabel(2, -1, -1, -1, -1, -1,'[120, 200]')
    _map_ztt.GetXaxis().ChangeLabel(3, -1, -1, -1, -1, -1,'[200, 300]')
    _map_ztt.GetXaxis().ChangeLabel(4, -1, -1, -1, -1, -1,'[300, #infty[')
    _map_ztt.GetXaxis().SetDefaults()

    _norm_ztt_0 = sum([_c.sample('ZttQCD_truth_0').integral('nominal') for _c in _srs])
    _norm_ztt_1 = sum([_c.sample('ZttQCD_truth_1').integral('nominal') for _c in _srs])
    _norm_ztt_2 = sum([_c.sample('ZttQCD_truth_2').integral('nominal') for _c in _srs])
    _norm_ztt_3 = sum([_c.sample('ZttQCD_truth_3').integral('nominal') for _c in _srs])

    for _c in _srs:
        _map_ztt.Fill(
            'truth_0', _c.category,
            _c.sample('ZttQCD_truth_0').integral('nominal')/ _norm_ztt_0)
        _map_ztt.Fill(
            'truth_1', _c.category,
            _c.sample('ZttQCD_truth_1').integral('nominal') / _norm_ztt_1)
        _map_ztt.Fill(
            'truth_2', _c.category,
            _c.sample('ZttQCD_truth_2').integral('nominal') / _norm_ztt_2)
        _map_ztt.Fill(
            'truth_3', _c.category,
            _c.sample('ZttQCD_truth_3').integral('nominal') / _norm_ztt_3)



    # Zll
    _map_zll = ROOT.TH2F(
        'bla_zll', '', 
        len(_truth_cats), 0, len(_truth_cats), 
        len(_recon_cats), 0, len(_recon_cats))
    for i_x in xrange(_map_zll.GetNbinsX()):
        _map_zll.GetXaxis().SetBinLabel(i_x + 1, _truth_cats[i_x])

    for i_y in xrange(_map_zll.GetNbinsY()):
        _map_zll.GetYaxis().SetBinLabel(i_y + 1, _recon_cats[i_y])
                     
    _map_zll.GetXaxis().SetTitle('Truth p_{T}(Z) Binning')
    _map_zll.GetYaxis().SetTitle('Reconstructed Category')
    _map_zll.GetYaxis().SetTitleOffset(1.55 * _map_zll.GetYaxis().GetTitleOffset())
    _map_zll.GetXaxis().ChangeLabel(1, -1, -1, -1, -1, -1,'[100, 120]')
    _map_zll.GetXaxis().ChangeLabel(2, -1, -1, -1, -1, -1,'[120, 200]')
    _map_zll.GetXaxis().ChangeLabel(3, -1, -1, -1, -1, -1,'[200, 300]')
    _map_zll.GetXaxis().ChangeLabel(4, -1, -1, -1, -1, -1,'[300, #infty[')
    _map_zll.GetXaxis().SetTitleSize(_map_zll.GetXaxis().GetLabelSize())
    _map_zll.GetYaxis().SetTitleSize(_map_zll.GetYaxis().GetLabelSize())
    _map_zll.GetXaxis().SetDefaults()
    _map_zll.GetZaxis().SetRangeUser(0, 1)

    _norm_zll_0 = sum([_c.sample('ZllQCD_truth_0').integral('nominal') for _c in _crs])
    _norm_zll_1 = sum([_c.sample('ZllQCD_truth_1').integral('nominal') for _c in _crs])
    _norm_zll_2 = sum([_c.sample('ZllQCD_truth_2').integral('nominal') for _c in _crs])
    _norm_zll_3 = sum([_c.sample('ZllQCD_truth_3').integral('nominal') for _c in _crs])

    for _c in _crs:
        _map_zll.Fill(
            'truth_0', _c.category,
            _c.sample('ZllQCD_truth_0').integral('nominal')/ _norm_zll_0)
        _map_zll.Fill(
            'truth_1', _c.category,
            _c.sample('ZllQCD_truth_1').integral('nominal') / _norm_zll_1)
        _map_zll.Fill(
            'truth_2', _c.category,
            _c.sample('ZllQCD_truth_2').integral('nominal') / _norm_zll_2)
        _map_zll.Fill(
            'truth_3', _c.category,
            _c.sample('ZllQCD_truth_3').integral('nominal') / _norm_zll_3)


    c_zll = ROOT.TCanvas()
    c_zll.SetTopMargin(0.1)
    c_zll.SetLeftMargin(0.22)
    c_zll.SetRightMargin(0.12)
    _map_zll.Draw('colzTEXT')
    
    _label_zll = ROOT.TLatex(
        c_zll.GetLeftMargin(),
        1 - c_zll.GetTopMargin() + 0.02,
        'Sherpa Z(#rightarrowll) + jets, Z + jets {} for {} channel'.format(
            'corrected control regions' if corrected_zcr else 'control regions', finalstate))
    _label_zll.SetNDC(True)
    _label_zll.SetTextSize(0.65 * _label_zll.GetTextSize())
    _label_zll.Draw()

    c_zll.RedrawAxis()
    c_zll.SaveAs('./plots/z_migration_summary_{}_{}_{}.pdf'.format(
            'cr_corrected' if corrected_zcr else 'cr',
            cat_key, finalstate))

    c_ztt = ROOT.TCanvas()
    c_ztt.SetTopMargin(0.1)
    c_ztt.SetLeftMargin(0.22)
    c_ztt.SetRightMargin(0.12)
    _map_ztt.Draw('colzTEXT')
    _label_ztt = ROOT.TLatex(
        c_ztt.GetLeftMargin(),
        1 - c_ztt.GetTopMargin() + 0.02,
        'Sample: Sherpa Z(#rightarrow#tau#tau) + jets, Categories: {} {} signal regions'.format(cat_key, finalstate))
    _label_ztt.SetNDC(True)
    _label_ztt.SetTextSize(0.65 * _label_ztt.GetTextSize())
    _label_ztt.Draw()
    c_ztt.RedrawAxis()
    c_ztt.SaveAs('./plots/z_migration_summary_sr_{}_{}.pdf'.format(
            cat_key, finalstate))


#    ROOT.gStyle.SetPalette(ROOT.kTemperatureMap)
    import array
    _colors = [ROOT.kRed - i for i in xrange(10)]
    _colors += [0]
    _colors += [ROOT.kBlue - 10 + i for i in xrange(10)]

    ROOT.gStyle.SetPalette(len(_colors), array.array('i',_colors))
    c_residuals = ROOT.TCanvas()
    c_residuals.SetTopMargin(0.1)
    c_residuals.SetLeftMargin(0.22)
    c_residuals.SetRightMargin(0.12)
    _map_residuals = _map_ztt.Clone()
    _map_residuals.Add(_map_zll, -1)
    _map_residuals.GetZaxis().SetRangeUser(-0.1, 0.1)
    _map_residuals.GetXaxis().ChangeLabel(1, -1, -1, -1, -1, -1,'[100, 120]')
    _map_residuals.GetXaxis().ChangeLabel(2, -1, -1, -1, -1, -1,'[120, 200]')
    _map_residuals.GetXaxis().ChangeLabel(3, -1, -1, -1, -1, -1,'[200, 300]')
    _map_residuals.GetXaxis().ChangeLabel(4, -1, -1, -1, -1, -1,'[300, #infty[')
    _map_residuals.GetXaxis().SetDefaults()
    _map_residuals.Draw('colzTEXT')
    
    _label_residuals = ROOT.TLatex(
        c_residuals.GetLeftMargin(),
        1 - c_residuals.GetTopMargin() + 0.02,
        'Z(#rightarrow#tau#tau) in SR -  Z(#rightarrowll) + jets {} for {} channel'.format(
            'corrected control regions' if corrected_zcr else 'control regions', finalstate))
    _label_residuals.SetNDC(True)
    _label_residuals.SetTextSize(0.65 * _label_residuals.GetTextSize())
    _label_residuals.Draw()

    c_residuals.RedrawAxis()
    c_residuals.SaveAs('./plots/z_migration_residuals_{}_{}_{}.pdf'.format(
            'cr_corrected' if corrected_zcr else 'cr',
            cat_key, finalstate))
    ROOT.gStyle.SetPalette(ROOT.kCherry)
    ROOT.TColor.InvertPalette()

#     for _c in _crs:
#         if not 'boost' in _c.name:
#             continue
#         print _c.category
#         print _c.sample('ZllQCD_truth_0').integral('nominal')
#         print _c.sample('ZllQCD_truth_1').integral('nominal')
#         print _c.sample('ZllQCD_truth_2').integral('nominal')
#         print _c.sample('ZllQCD_truth_3').integral('nominal')


def plot_channel(channel, sample_key=None, np_key=None, threshold=0.5, title=''):
    """
    """
    print 'name:', channel.name
    print 'category:', channel.category
    print 'region:', channel.region
    print 'is sr:', channel.sr
    print 'is cr:', channel.cr
    print 'final state:', channel.finalstate
    print 'sample key:', sample_key
    print 'np key:', np_key
    print 'title:', title
    if sample_key == None:
        _sample_names = sorted(channel.sample_names)
    else:
        _sample_names = sorted(filter(lambda n: sample_key in n, channel.sample_names))

    _variations = {}
    _all_variations = []
    for _sample in channel.samples:
        if _sample.name not in _sample_names:
            continue
        _variations[_sample.name] = {}
        for _var in _sample.nuisance_parameters(key=np_key):
            if not _var in _all_variations:
                _all_variations += [_var]
            _var_yield = _sample.integral(_var)
            _nominal, _error = _sample.integral_and_error
            _variations[_sample.name][_var] = {
                'nominal': _nominal,
                'stat_err': _error,
                'variation': _var_yield,
                }
                         

    _all_variations = sorted(_all_variations)

    _name = channel.name
    if sample_key != None:
        _name += '_' + sample_key

    if np_key != None:
        _name += '_' + np_key

    # fill map
    _map = ROOT.TH2F(
        'map_' + _name, _name, 
        len(_sample_names), 0, len(_sample_names), 
        len(_all_variations), 0, len(_all_variations))
    for _samp in _sample_names:
        for _var in _all_variations:
            try:
                _info = _variations[_samp][_var]
                if _info['variation'] == 0:
                    _rel = 0
                else:
                    if _info['nominal'] != 0:
                        _rel = abs(_info['nominal'] - _info['variation']) / abs(_info['nominal'])
                        _rel_stat = (abs(_info['nominal']) + _info['stat_err']) / abs(_info['nominal'])
                        if _rel < threshold * _rel_stat:
                            _rel = 0
                    else:
                        _rel = 0
                _map.Fill(_samp, _var, _rel)
                if abs(_rel) > 1:
                    print name, _samp, _var, _info['nominal'], _info['stat_err'], _info['variation']
            except:
                _map.Fill(_samp, _var, 0)


    c = ROOT.TCanvas('plot_' + channel.name, channel.name, 600, 800)
    c.SetLeftMargin(0.2)
    c.SetBottomMargin(0.2)
    c.SetRightMargin(0.2)
    _map.GetYaxis().SetLabelSize(0.15 * _map.GetYaxis().GetLabelSize())
    _map.GetXaxis().SetLabelSize(0.4 * _map.GetXaxis().GetLabelSize())
    _map.GetZaxis().SetRangeUser(-2, 2)
    _map.Draw('COLZ')
    _title = ROOT.TText(
        c.GetLeftMargin(),
        1 - c.GetTopMargin() / 2,
        title)
    _title.SetTextSize(0.6 * _title.GetTextSize())
    _title.SetNDC(True)
    _title.Draw()
    _cat_title = ROOT.TText(
        0.02, 0.6 * c.GetBottomMargin(), 
        'category: {}'.format(channel.category))
    _cat_title.SetNDC(True)
    _cat_title.SetTextSize(0.6 * _cat_title.GetTextSize())
    _cat_title.Draw()

    _cat_region = ROOT.TText(
        0.02, 0.4 * c.GetBottomMargin(), 
        'region: {}'.format(channel.region))
    _cat_region.SetNDC(True)
    _cat_region.SetTextSize(0.6 * _cat_region.GetTextSize())
    _cat_region.Draw()

    _cat_finalstate = ROOT.TText(
        0.02, 0.2 * c.GetBottomMargin(), 
        'final state: {}'.format(channel.finalstate))
    _cat_finalstate.SetNDC(True)
    _cat_finalstate.SetTextSize(0.6 * _cat_finalstate.GetTextSize())
    _cat_finalstate.Draw()


    c.SaveAs('plots/wsi_inspection_syst_' + _name + '_' + title + '.pdf')





def _min_max_err(_nom, _low, _high):
    if _nom == 0:
        return 0, 0
    r_low = _low / _nom
    r_high = _high / _nom
    return min(r_low, r_high), max(r_low, r_high)

#def plot_migrations(categories, yields_zll, yields_ztt, uncert_source='PDF', channel_name='hadhad'):
def plot_migrations(cr_sr_pairs, uncert_source='ztheory_ckkw'):

    _categories = []
    for _cr, _ in cr_sr_pairs:
        if not _cr.category in _categories:
            _categories += [_cr.category]
    print _categories
    import uuid
    _template = ROOT.TH1F(uuid.uuid4().hex, "", len(cr_sr_pairs), 0, len(cr_sr_pairs))
    _template.GetXaxis().SetTitle('Category')
    _template.SetLineWidth(0)
    for _ibin in xrange(_template.GetNbinsX()):
        _template.GetXaxis().SetBinLabel(_ibin + 1, cr_sr_pairs[_ibin][0].finalstate)
        _template.SetBinContent(_ibin + 1, 1)
    _template.GetYaxis().SetTitle('{} / nominal'.format(uncert_source))
    _template.GetYaxis().SetRangeUser(0.7, 1.3)

    _template_2 = _template.Clone()
    _template_2.GetYaxis().SetTitle('Stat. Uncert. on nominal pred.')
    _template_2.GetYaxis().SetRangeUser(0.85, 1.15)

    _template.GetYaxis().SetTitleSize(1.2 * _template.GetTitleSize())
    _template.SetTitleOffset(0.5 * _template.GetTitleOffset('Y'), 'Y')

    _template_2.SetLabelSize(0.8 * _template_2.GetLabelSize())
    _template_2.SetTitleOffset(0.6 * _template_2.GetTitleOffset('Y'), 'Y')

    _gr_zll = ROOT.TGraphAsymmErrors()
    _gr_ztt = ROOT.TGraphAsymmErrors()
    _gr_ratio = ROOT.TGraphAsymmErrors()

    _gr_zll_nom_stat_err  = ROOT.TGraphErrors()
    _gr_ztt_nom_stat_err  = ROOT.TGraphErrors()

    for i_l, (_cr, _sr) in enumerate(cr_sr_pairs):
        print _cr.name, _sr.name
        Zll = _cr.sample('ZllQCD_truth_0') + _cr.sample('ZllQCD_truth_1') + _cr.sample('ZllQCD_truth_2') + _cr.sample('ZllQCD_truth_3')  
        Ztt = _sr.sample('ZttQCD_truth_0') + _sr.sample('ZttQCD_truth_1') + _sr.sample('ZttQCD_truth_2') + _sr.sample('ZttQCD_truth_3')  

        _gr_ztt.SetPoint(i_l, i_l + 0.3, 1)
        _gr_zll.SetPoint(i_l, i_l + 0.5, 1)
        _gr_ratio.SetPoint(i_l, i_l + 0.7, 1)

        
        _low, _high = _min_max_err(
            Zll.integral_and_error[0],
            Zll.integral(uncert_source + '_low'),
            Zll.integral(uncert_source + '_high'))
        _gr_zll.SetPointError(i_l, 0.1, 0.1, 1 - _low, _high - 1)

        _low, _high = _min_max_err(
            Ztt.integral_and_error[0],
            Ztt.integral(uncert_source + '_low'),
            Ztt.integral(uncert_source + '_high'))
        _gr_ztt.SetPointError(i_l, 0.1, 0.1, 1 - _low, _high - 1)

        _low, _high = _min_max_err(
            Ztt.integral_and_error[0] / Zll.integral_and_error[0],
            Ztt.integral(uncert_source + '_low') / Zll.integral(uncert_source + '_low'),
            Ztt.integral(uncert_source + '_high') / Zll.integral(uncert_source + '_high'))
        _gr_ratio.SetPointError(i_l, 0.1, 0.1, 1 - _low, _high - 1)
        
        _gr_ztt_nom_stat_err  .SetPoint(i_l, i_l + 0.4, 1)
        _gr_zll_nom_stat_err  .SetPoint(i_l, i_l + 0.7, 1)

        _gr_zll_nom_stat_err  .SetPointError(i_l, 0.05, Zll.integral_and_error[1] / Zll.integral_and_error[0])
        _gr_ztt_nom_stat_err  .SetPointError(i_l, 0.05, Ztt.integral_and_error[1] / Ztt.integral_and_error[0])

        del Zll
        del Ztt

    _gr_zll.SetFillColor(ROOT.kCyan - 10)
    _gr_ztt.SetFillColor(ROOT.kAzure+1)
    _gr_ratio.SetFillColor(ROOT.kBlue)
    _gr_zll_nom_stat_err  .SetFillColor(ROOT.kCyan - 10)
    _gr_ztt_nom_stat_err  .SetFillColor(ROOT.kAzure + 1)


    c = ROOT.TCanvas(uuid.uuid4().hex, "", 900, 800)
    c.SetTopMargin(0.12)
    c.SetLeftMargin(0.7 * c.GetLeftMargin())
    c.Divide(1, 2, 0, 0)
    #
    _p1 = c.cd(1)
    _template.Draw('HIST')
    _gr_zll.Draw('sameE2')
    _gr_ztt.Draw('sameE2')
    _gr_ratio.Draw('sameE2')
    _template.Draw('sameHIST')
    _p1.SetGridx()
    #
    _p2 = c.cd(2)
    _template_2.Draw('HIST')
    _gr_zll_nom_stat_err  .Draw('sameE2')
    _gr_ztt_nom_stat_err  .Draw('sameE2')
    _p2.SetGridx()

    c.cd(0)
    c.RedrawAxis()
    
#     _label = ROOT.TText(
#         c.GetLeftMargin(),
#         1 - c.GetTopMargin() / 2,
#         'Source: {} - Channel: {}'.format(uncert_source, channel_name))
#     _label.SetNDC(True)
#     _label.SetTextSize(0.6 * _label.GetTextSize())
#     _label.Draw()

#     c.cd(1)
    _legend = ROOT.TLegend(0.5, 0.95, 1 - c.GetRightMargin(), 1.)
    _legend.AddEntry(_gr_zll, 'Z#rightarrowll in CR', 'f')
    _legend.AddEntry(_gr_ztt, 'Z#rightarrow#tau#tau in SR ', 'f')
    _legend.AddEntry(_gr_ratio, 'Z#rightarrow#tau#tau in SR / Z#rightarrowll in CR', 'f')
    _legend.SetNColumns(3)
#     _legend.SetFillStyle(0)
    _legend.SetFillColor(0)
    _legend.SetBorderSize(0)
    _legend.SetTextSize(1.1 *_legend.GetTextSize())
    _legend.Draw()

#     c.cd(2)
#     _legend_2 = ROOT.TLegend(0.15, 0.85, 0.9, 1.)
#     _legend_2.AddEntry(_gr_zll_nom_stat_err, 'Z#rightarrowee nom', 'f')    
#     _legend_2.AddEntry(_gr_ztt_nom_stat_err , 'Z#rightarrow#tau#tau nom ', 'f') 
#     _legend_2.SetFillStyle(0)
#     _legend_2.SetFillColor(0)
#     _legend_2.SetBorderSize(0)
#     _legend_2.Draw()
    c.RedrawAxis()

    _lines_up = []
    _lines_do = []
    for ibin in xrange(_template.GetNbinsX()):
        if _template.GetXaxis().GetBinLabel(ibin) == 'll':
            _l_up = ROOT.TLine(
                 _template.GetBinLowEdge(ibin) + 1,
                 _template.GetMinimum(),
                 _template.GetBinLowEdge(ibin) + 1,
                 _template.GetMaximum())

            _l_do = ROOT.TLine(
                 _template.GetBinLowEdge(ibin) + 1,
                 _template_2.GetMinimum(),
                 _template.GetBinLowEdge(ibin) + 1,
                 _template_2.GetMaximum())

            _lines_up += [_l_up]
            _lines_do += [_l_do]

    c.cd(1)
    [_l.Draw('same') for _l in _lines_up]
    c.cd(2)
    [_l.Draw('same') for _l in _lines_do]

    _category_labels = []
    for i_label, _cat in enumerate(_categories):
        x_pos =  i_label * len(cr_sr_pairs) / len(_categories) + len(cr_sr_pairs) / len(_categories) * 0.5
        _l = ROOT.TText(x_pos, _template_2.GetMinimum() - 0.02, _cat)
        _l.SetTextAlign(22)
        _l.SetTextSize(0.6 * _l.GetTextSize())
        _category_labels += [_l]
    [_l.Draw('same') for _l in _category_labels]
            

    c.SaveAs('zllztt_variation_hh_{}.pdf'.format(uncert_source))

def plot_zcr(wsi, finalstate='hh', zcr_channel=None, print_lumi=True, ztt_zll_norm=False):
    """
    """

#     ROOT.gStyle.SetPadBottomMargin(0.3)
    _channels = filter(lambda c: 'ztt' in c.name and c.cr, wsi.channels)
    _channels = filter(lambda c: c.finalstate == finalstate, _channels)

    _channels_sr = filter(lambda c: c.sr and c.finalstate==finalstate, wsi.channels)

    if zcr_channel != None:
        _channels = filter(lambda c: zcr_channel in c.region, _channels)
    _channels = sorted(_channels, key=lambda c: c.category)
    _channels_sr = sorted(_channels_sr, key=lambda c: c.category)

    _categories = []
    for _c in _channels:
        if not _c.category in _categories:
            _categories += [_c.category]

    h_data = ROOT.TH1F('hd_' + finalstate, 'hd', len(_categories), 0, len(_categories))
    h_zll_qcd   = ROOT.TH1F('h_zll_qcd_' + finalstate, 'h_zll_qcd', len(_categories), 0, len(_categories))
    h_zll_ewk   = ROOT.TH1F('h_zll_ewk_' + finalstate, 'h_zll_ewk', len(_categories), 0, len(_categories))
    h_ztt   = ROOT.TH1F('h_ztt' + finalstate, 'h_ztt', len(_categories), 0, len(_categories))
#     h_ztt_ewk   = ROOT.TH1F('h_ztt_ewk_' + finalstate, 'h_ztt_ewk', len(_categories), 0, len(_categories))
    
    h_data.SetTitle(_channels[0].sample('Data').nominal.GetTitle())
    h_data.SetMarkerStyle(_channels[0].sample('Data').nominal.GetMarkerStyle())
    h_data.SetMarkerSize(_channels[0].sample('Data').nominal.GetMarkerSize())
    h_data.SetMarkerColor(_channels[0].sample('Data').nominal.GetMarkerColor())
    h_data.SetLineStyle(_channels[0].sample('Data').nominal.GetLineStyle())
    h_data.SetLineWidth(_channels[0].sample('Data').nominal.GetLineWidth())
    h_data.SetLineColor(_channels[0].sample('Data').nominal.GetLineColor())

    h_zll_qcd.SetTitle('QCD ' + _channels[0].sample('ZllQCD_truth_0').nominal.GetTitle())
    if zcr_channel != None:
        h_zll_qcd.SetTitle(h_zll_qcd.GetTitle() + '({})'.format(zcr_channel))
    _decorate_hist(h_zll_qcd, _channels[0].sample('ZllQCD_truth_0'))

    h_zll_ewk.SetTitle('EWK ' +_channels[0].sample('ZllEWK_truth_0').nominal.GetTitle())
    if zcr_channel != None:
        h_zll_ewk.SetTitle(h_zll_ewk.GetTitle() + '({})'.format(zcr_channel))
    _decorate_hist(h_zll_ewk, _channels[0].sample('ZllEWK_truth_0'))


    h_ztt.SetTitle(_channels_sr[0].sample('ZttQCD_truth_0').nominal.GetTitle())
    _decorate_hist(h_ztt, _channels_sr[0].sample('ZttQCD_truth_0'))


    for i_c, _cat in enumerate(_categories):
        _selected_channels = filter(lambda c: c.category == _cat, _channels)
        _data = []
        _zll_ewk = []
        _zll_qcd = []
        for _c in _selected_channels:
            _data.append(_c.sample('Data'))
            _zll_ewk.append(_c.sample('ZllEWK_truth_0'))
            _zll_ewk.append(_c.sample('ZllEWK_truth_1'))
            _zll_ewk.append(_c.sample('ZllEWK_truth_2'))
            _zll_ewk.append(_c.sample('ZllEWK_truth_3'))
            _zll_qcd.append(_c.sample('ZllQCD_truth_0'))
            _zll_qcd.append(_c.sample('ZllQCD_truth_1'))
            _zll_qcd.append(_c.sample('ZllQCD_truth_2'))
            _zll_qcd.append(_c.sample('ZllQCD_truth_3'))


        _selected_channels_sr = filter(lambda c: c.category == _cat, _channels_sr)
        _ztt = []
        for _c in _selected_channels_sr:
            _ztt.append(_c.sample('ZttQCD_truth_0'))
            _ztt.append(_c.sample('ZttQCD_truth_1'))
            _ztt.append(_c.sample('ZttQCD_truth_2'))
            _ztt.append(_c.sample('ZttQCD_truth_3'))
            _ztt.append(_c.sample('ZttEWK_truth_0'))
            _ztt.append(_c.sample('ZttEWK_truth_1'))
            _ztt.append(_c.sample('ZttEWK_truth_2'))
            _ztt.append(_c.sample('ZttEWK_truth_3'))

        _data_sample = _data[0]
        for _s in _data[1:]:
            _data_sample += _s

        _zll_ewk_sample = _zll_ewk[0]
        for _s in _zll_ewk[1:]:
            _zll_ewk_sample += _s

        _zll_qcd_sample = _zll_qcd[0]
        for _s in _zll_qcd[1:]:
            _zll_qcd_sample += _s

        _ztt_sample = _ztt[0]
        for _s in _ztt[1:]:
            _ztt_sample += _s


        _data, _err_data = _data_sample.integral_and_error

        h_data.SetBinContent(i_c + 1, _data)
        h_data.SetBinError(i_c + 1, _err_data)

        _zll_qcd, _err_zll_qcd = _zll_qcd_sample.integral_and_error
        h_zll_qcd.SetBinContent(i_c + 1, _zll_qcd)
        h_zll_qcd.SetBinError(i_c + 1, _err_zll_qcd)

        _zll_ewk, _err_zll_ewk = _zll_ewk_sample.integral_and_error
        h_zll_ewk.SetBinContent(i_c + 1, _zll_ewk)
        h_zll_ewk.SetBinError(i_c + 1, _err_zll_ewk)

        _ztt, _err_ztt = _ztt_sample.integral_and_error
        h_ztt.SetBinContent(i_c + 1, _ztt)
        h_ztt.SetBinError(i_c + 1, _err_ztt)

    if ztt_zll_norm:
        if h_ztt.Integral() != 0:
            _scaling_factor = (h_zll_qcd.Integral() + h_zll_ewk.Integral()) / h_ztt.Integral()
            h_ztt.Scale(_scaling_factor)
            h_ztt.SetTitle(h_ztt.GetTitle() + '(x{0:1.3f})'.format(_scaling_factor))

    if zcr_channel != None and not ztt_zll_norm:
        h_ztt.Scale(0.5)
        h_ztt.SetTitle(h_ztt.GetTitle() + '(x0.5)')
    h_zll = h_zll_qcd.Clone()
    h_zll.Add(h_zll_ewk)



    from happy.dataMcRatioPlot import DataMcRatioPlot
    from happy.dataMcPlot import DataMcPlot
    from happy.variable import Binning, Variable, var_Ratio
    from happy.plotDecorator import AtlasTitleDecorator
    from happy.style import Style

    variable = Variable(
        'Yields', 'Yields', 
        binning=Binning(h_data.GetNbinsX(), 0, h_data.GetNbinsX(), _categories))
    ratio_range = 0.5
    var_dataMc = Variable('Data / MC', binning=Binning(low=1 - ratio_range, high=1 + ratio_range))
    var_dataMc.binning.nDivisions = 5
    plot = DataMcRatioPlot(
        uuid.uuid4().hex, 
        variable, 
        yVariableDown=var_dataMc, 
        upperFraction=0.70, 
        lowerFraction=0.30)
#     plot.sizeX = 1600
#     plot.referenceSizeX = 1600
#     plot = DataMcPlot(
#         uuid.uuid4().hex, 
#         variable)

    # plot style alteration
    plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 4
#     plot.topMargin = 0.25
    plot.topMargin = 0.4
    
    plot.addHistogram(h_zll_qcd)
    plot.addHistogram(h_zll_ewk)
    plot.addHistogram(h_ztt, 'HIST', stacked=False)
    plot.setDataHistogram(h_data)
    plot.plotDown.addRatioHistogram(h_ztt, h_zll)

#     te = TextElement(_title, x=0.26, y=0.94)
#     plot.textElements.append(te)
    print_lumi = True
    if print_lumi:
        luminosity = _channels[0].lumi.GetBinContent(1)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )
    plot.titles.append('finalstate: {}'.format(finalstate) )
    
    _name = 'wsi_yields_zll_cr_{}'.format(finalstate)
    if zcr_channel != None:
        _name += '_' + zcr_channel
    if ztt_zll_norm:
        _name += '_norm_constraint'

    plot.draw()
    plot.saveAs('plots/' + _name + '.pdf')
