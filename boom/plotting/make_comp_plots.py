from ROOT import *
import root_utils as ru
import os
# gROOT.SetBatch(1)

class Comp_plot():#,hes,hms,decoside,lumi,title,xtitle,ytitle,minstat,sigscale,rebin,maxx,forPub,chans):
    def __init__(self,h1,h2,plot_title,save_dir='',save_title='',xtitle='',showstats=True,labelsinratio=True):
        self.h1=ru.grab_hist(h1)
        self.h2=ru.grab_hist(h2)
        self.title=plot_title
        self.save_dir=save_dir
        self.save_title=plot_title if not save_title else save_title
        self.xtitle=xtitle
        self.ytitle="events / %s GeV"%(int(round(h1.GetBinWidth(1)))/1000) if h1.GetBinWidth(1)>=1000 else 'events / bin'
        self.xlog=False
        self.ylog=False
        self.rebin=''
        self.labelsinratio=labelsinratio
        self.minstat=0.1
        self.max_offset=1
        self.legside='right'
        self.lumi='138.42' #'36.074'
        self.saveas='png'
        self.channels=["em","me"]
        self.hrmin=0.6
        self.hrmax=1.4
        self.userxmin=''
        self.userxmax=''
        self.userymin=''
        self.userymax=''
        self.showstats=showstats
        self.syshlist=[] #[h1up,h1down,h2up,h2down]
        self.syslabel=""
        self.ch_labels=[]
        self.hsigs={} # {'sig_label':{'channel':h}} 
#ex - only taumu, both channels: {"H #rightarrow #tau #mu" : {"em" : hem, "me": hme}}
#ex - no cont, both sigs: {"H #rightarrow #tau #mu" : {"me": hme}, "H #rightarrow #tau e" : {"em": hem}}
        self.sigcolors=[kOrange,kGreen+3]
        self.sigscale=1
        self.drawdecos=True
        self.up_width=800
        self.up_height=800
        self.ylabelsize=20
        self.xlabelsize=20
        self.decotextsize=0.04

    def get_canvas_objects(self):
        xmin,xmax,ymin,ymax=self.set_ranges()
        self.hr=ru.get_ratio_hist(self.h1,self.h2)
        self.set_up_axes()
        self.set_ratio_axes()
        self.set_hist_props()
        if self.syshlist:
            self.get_var_bands()
        self.leg,self.deco1,self.deco2=self.get_up_decos()
        self.hup,self.hlow,self.line=self.get_ratio_decos(xmin,xmax)
        return self.set_canvas()

    def get_var_bands(self):
        g1up=TGraphAsymmErrors(self.h1)
        g1down=TGraphAsymmErrors(self.h1)
        g1up.SetFillColor(kOrange-5)
        g1up.SetFillStyle(3144)
        g1down.SetFillColor(kOrange-5)
        g1down.SetFillStyle(3144)
        g2up=TGraphAsymmErrors(self.h2)
        g2down=TGraphAsymmErrors(self.h2)
        g2up.SetFillColor(kOrange-5)
        g2up.SetFillStyle(3144)
        g2down.SetFillColor(kOrange-5)
        g2down.SetFillStyle(3144)
        hones=ru.grab_hist(self.h1)
        hones.Reset()
        for i in range(0,hones.GetNbinsX()):
            hones.SetBinContent(i+1,1)
        grup=TGraphAsymmErrors(hones)
        grdown=TGraphAsymmErrors(hones)
        grup.SetFillColor(kOrange-5)
        # grup.SetFillColor(kGreen+3)
        grup.SetLineWidth(0)
        grup.SetFillStyle(3144)
        self.grup=grup
        grdown.SetFillColor(kOrange-5)
        grdown.SetFillStyle(3144)
        grdown.SetLineWidth(0)
        h1nom=self.h1
        h2nom=self.h2
        [h1up,h1down,h2up,h2down]=self.syshlist
        for i in range(0,h1nom.GetNbinsX()):
            nom1=h1nom.GetBinContent(i+1)
            nom2=h2nom.GetBinContent(i+1)
            if nom1<=0 or nom2<=0:
                continue
            up1=h1up.GetBinContent(i+1)
            down1=h1down.GetBinContent(i+1)
            devup1=abs(up1-nom1)
            devdown1=abs(nom1-down1)
            up2=h2up.GetBinContent(i+1)
            down2=h2down.GetBinContent(i+1)
            devup2=abs(up2-nom2)
            devdown2=abs(nom2-down2)
            g1up.SetPointEYhigh(i,devup1)
            g1up.SetPointEYlow(i,0)
            g1down.SetPointEYhigh(i,0)
            g1down.SetPointEYlow(i,devdown1)
            g2up.SetPointEYhigh(i,devup2)
            g2up.SetPointEYlow(i,0)
            g2down.SetPointEYhigh(i,0)
            g2down.SetPointEYlow(i,devdown2)
            grup.SetPointEYhigh(i,(nom1/nom2)*((devup1/nom1)**2+(devup2/nom2)**2)**0.5)
            grup.SetPointEYlow(i,0)
            grdown.SetPointEYhigh(i,0)
            grdown.SetPointEYlow(i,(nom1/nom2)*((devdown1/nom1)**2+(devdown2/nom2)**2)**0.5)
        self.g1up=g1up
        self.g1down=g1down
        self.g2up=g2up
        self.g2down=g2down
        self.grup=grup
        self.grdown=grdown
        
    def show_canvas(self):
        c=self.get_canvas_objects()
        ru.showcanvas(c)
        c.Close()
        
    def save_canvas(self):
        if not os.path.isdir(self.save_dir):
            print "Creating new directory:",self.save_dir
            os.makedirs(self.save_dir)
        c=self.get_canvas_objects()
        c.SaveAs(self.save_dir+"/%s.%s"%(self.save_title.replace(' ','_'),self.saveas))
        c.Close()
        
    def set_ranges(self,scale=True,rebin=True):
        # print "rebin:",rebin,self.rebin
        if rebin and self.rebin:
            ru.rebin_hists([self.h1,self.h2]+self.syshlist,self.rebin)
            for sig in self.hsigs:
                for ch,h in self.hsigs[sig].iteritems():
                    h.Rebin(rebin)
        # print self.h1.GetMaximum()
        # print self.h2.GetMaximum()
        sighists=[]
        for key in self.hsigs:
            for key1,h in self.hsigs[key].iteritems():
                sighists.append(h)
        xmin,xmax=ru.get1DXrange([self.h1,self.h2]+sighists,self.minstat)
        ymin,ymax=ru.get1DYrange([self.h1,self.h2]+sighists)
        ymin=max(0,ymin)
        if ymax<=0:
            ymax_order=1
        else:
            ymax_order=10**int(TMath.Log10(ymax))
        ymin=ymin_round=int(ymin)/ymax_order*ymax_order
        ymax=ymax_round=ymin_round+self.max_offset*(ymax-ymin_round)*3/2
        if self.userxmin!='':
            xmin=self.userxmin
        if self.userxmax!='':
            xmax=self.userxmax
        if self.userymin!='':
            ymin=ymin_round=self.userymin
        if self.userymax!='':
            ymax=ymax_round=self.userymax
        if scale:
            self.h1.GetXaxis().SetRangeUser(xmin,xmax)
            self.h1.SetMaximum(ymax)
            self.h1.SetMinimum(ymin)
        return xmin,xmax,ymin,ymax
        
    def set_up_axes(self):
        xax=self.h1.GetXaxis()
        yax=self.h1.GetYaxis()
        self.h1.SetTitle(self.title)
        gStyle.SetTitleFontSize(self.decotextsize+0.01)
        xax.SetTitle('')
        xax.SetLabelSize(0.)
        yax.SetTitle(self.ytitle)
        # yax.CenterTitle()
        yax.SetLabelSize(self.ylabelsize)
        yax.SetLabelFont(43)
        yax.SetTitleSize(25)
        yax.SetTitleFont(43)
        yax.SetTitleOffset(1.7)

    def set_ratio_axes(self):
        self.hr.SetMinimum(self.hrmin)
        self.hr.SetMaximum(self.hrmax)
        xax=self.hr.GetXaxis()
        yax=self.hr.GetYaxis()
        xax.SetTitle(self.xtitle)
        # xax.CenterTitle()
        xax.SetLabelSize(self.xlabelsize)
        xax.SetLabelFont(43)
        xax.SetTitleSize(25)
        xax.SetTitleFont(43)
        xax.SetTitleOffset(3.6)
        if self.ch_labels==[]:
            self.ch_labels=[k.replace('m','#mu').replace('#muu','mu') for k in self.channels]
        if self.labelsinratio:
            yax.SetTitle("ratio %s/%s"%(self.ch_labels[0].split('_')[0],self.ch_labels[1].split('_')[0]))
        else:
            yax.SetTitle("ratio")
        yax.CenterTitle()
        yax.SetNdivisions(6)
        yax.SetLabelSize(self.ylabelsize)
        yax.SetLabelFont(43)
        yax.SetTitleSize(25)
        yax.SetTitleFont(43)
        yax.SetTitleOffset(1.7)
        self.hr.SetTitle('')
        
    def set_hist_props(self):
        self.h1.SetLineWidth(2)
        self.h1.SetLineColor(kRed)
        self.h1.SetMarkerStyle(20)
        self.h1.SetMarkerSize(0.9)
        self.h1.SetMarkerColor(kRed)
        self.h2.SetLineColor(kBlue)
        self.h2.SetLineWidth(2)
        self.h2.SetMarkerStyle(20)
        self.h2.SetMarkerSize(0.9)
        self.h2.SetMarkerColor(kBlue)
        self.hr.SetMarkerStyle(20)
        self.hr.SetMarkerColor(1)
        self.hr.SetLineColor(kBlack)
        self.hr.SetLineWidth(2)
        if self.hsigs!={}:
            self.set_sighist_props()

    def set_sighist_props(self):
        count=-1
        for sig in self.hsigs:
            for ch,h in self.hsigs[sig].iteritems():
                count+=1
                color=self.sigcolors[count]
                h.SetLineWidth(2)
                h.SetLineColor(color)
                if self.sigscale!=1:
                    h.Scale(self.sigscale)
                # h.SetMarkerStyle(20)
                # h.SetMarkerSize(0.9)
                # h.SetMarkerColor(color)
    
    def get_up_decos(self):
        if self.legside=="right":
            # leg=TLegend(.6,.55,.85,.85)
            # deco1=TLegend(.15,.78,.55,.88)
            # deco2=TLegend(.15,.68,.55,.78)
            leg=TLegend(.65,.60,.90,.90)
            deco1=TLegend(.20,.83,.60,.93)
            deco2=TLegend(.20,.73,.60,.83)

        else:
            leg=TLegend(.15,.55,.4,.85)
            deco1=TLegend(.5,.78,.85,.88)
            deco2=TLegend(.5,.68,.85,.78)
        leg.SetFillColor(kWhite)
        leg.SetBorderSize(0)
        leg.SetTextSize(self.decotextsize)
        if self.ch_labels==[]:
            self.ch_labels=[k.replace('m','#mu').replace('#muu','mu') for k in self.channels]
        if self.showstats:
            leg.AddEntry(self.h1,"%s (%.2f)"%(self.ch_labels[0],self.h1.Integral()),"ep")
            leg.AddEntry(self.h2,"%s (%.2f)"%(self.ch_labels[1],self.h2.Integral()),"ep")
        else:
            leg.AddEntry(self.h1,self.ch_labels[0],"ep")
            leg.AddEntry(self.h2,self.ch_labels[1],"ep")
        if self.hsigs!={}:
            for sig in self.hsigs:
                for ch,h in self.hsigs[sig].iteritems():
                    label="(%s)_{%s}"%(sig,ch.replace('m','#mu').replace('#muu','mu'))
                    if self.showstats:
                        label+=" (%.2f)"%h.Integral(1,h.GetNbinsX())
                    if self.sigscale!=1:
                        label="%sx"%self.sigscale+label
                    leg.AddEntry(h,label,"l")
        if self.syshlist:
            leg.AddEntry(self.grup,self.syslabel,"f")
        deco1.SetFillColor(kWhite)
        deco1.SetHeader("#bf{#it{ATLAS}} Internal")
        deco1.SetTextAlign(12)
        deco1.SetBorderSize(0)
        deco1.SetTextSize(self.decotextsize)
        deco2.SetFillColor(kWhite)
        deco2.SetHeader("#intLdt = %s fb^{-1}, #sqrt{s} = 13 TeV"%self.lumi)
        deco2.SetTextAlign(12)
        deco2.SetBorderSize(0)
        deco2.SetTextSize(self.decotextsize)
        return leg,deco1,deco2

    def get_ratio_decos(self,xmin,xmax):
        hup=self.hr.Clone()
        hup.SetDirectory(0)
        hup.SetMarkerStyle(22)
        hup.SetMarkerColor(kRed)
        hlow=self.hr.Clone()
        hlow.SetDirectory(0)
        hlow.SetMarkerStyle(23)
        hlow.SetMarkerColor(kRed)
        for b in range(1,self.hr.GetNbinsX()+1):
            if self.hr.GetBinContent(b)>self.hrmax:
                hup.SetBinContent(b,self.hrmax-0.03*(self.hrmax-self.hrmin))
                # hup.SetBinError(b,0)
                hlow.SetBinContent(b,-10)
            elif self.hr.GetBinContent(b)<self.hrmin:
                hlow.SetBinContent(b,self.hrmin+0.025*(self.hrmax-self.hrmin))
                # hlow.SetBinError(b,0)
                hup.SetBinContent(b,-10)
            else:
                hup.SetBinContent(b,-10)
                hlow.SetBinContent(b,-10)
        l=TLine(xmin,1,xmax,1)
        l.SetLineColor(2)
        l.SetLineWidth(2)
        l.SetLineStyle(2)
        return hup,hlow,l

    def set_canvas(self):
        gStyle.SetOptStat(0)
        #PrepareCanvas
        c=TCanvas("c","",self.up_width,self.up_height)
        up=TPad("up","",0,.3,1,1)
        up.SetBottomMargin(0.03)
        # up.SetLogy()
        if self.ylog:
            up.SetLogy()
        up.SetTickx()
        up.SetTicky()
        up.Draw()
        down=TPad("down","",0,.05,1,.3)
        down.SetTopMargin(0.04)
        down.SetBottomMargin(0.3)
        down.SetTickx()
        down.SetTicky()
        down.Draw()
        #DrawPlot
        up.cd()
        self.h1.Draw("P")
        self.h2.Draw("P same")
        self.leg.Draw("same")
        if self.drawdecos:
            self.deco1.Draw("same")
            self.deco2.Draw("same")
        if self.syshlist:
            self.g1up.Draw("2 same")
            self.g1down.Draw("2 same")
            self.g2up.Draw("2 same")
            self.g2down.Draw("2 same")
        if self.hsigs!={}:
            for sig in self.hsigs:
                for ch,h in self.hsigs[sig].iteritems():
                    h.Draw("hist same")
        self.h1.Draw("P same")
        self.h2.Draw("P same")
        up.RedrawAxis()
        down.cd()
        self.hr.Draw("P")
        if self.syshlist:
            self.grup.Draw("2 same")
            self.grdown.Draw("2 same")
        self.line.Draw("P0 same")
        self.hup.Draw("same E X0")
        self.hlow.Draw("same E X0")
        self.hr.Draw("P same")
        down.SetGridy(1)
        down.RedrawAxis()
        return c


