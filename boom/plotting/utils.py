import os
import json
from subprocess import PIPE, Popen
import pprint
import shutil
import datetime 
import socket
import time
# import numpy as np

def find_and_replace_infile(infile,replacedct,outfile=None):
    if not outfile:
        outfile=infile
    with open(infile) as f:
        fdata=f.read()
    for key,val in replacedct.iteritems():
        fdata=fdata.replace(key,val)
    with open(outfile,'w') as f:
        f.write(fdata)

def isclose(a, b, rel_tol=1e-05, abs_tol=0.0):
    # print max(rel_tol * max(abs(a), abs(b)), abs_tol)
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

# def isclose(a,b):
#     astr=str(a)
#     aprec=len(astr.split('.')[1]) if '.' in astr else 0
#     bstr=str(b)
#     bprec=len(bstr.split('.')[1]) if '.' in bstr else 0
#     if 'e' in astr or 'e' in bstr:
#         warn("Number has exponent, case not treated","%s %s"%(astr,bstr))
#         return None
#     prec=min(aprec,bprec)
# #    print a,b,round(a,prec),round(b,prec)
#     return round(a,prec)==round(b,prec)

def getextremeindexes(vals,allowedstd=1):
    avg=np.mean(vals)
    std=np.std(vals)
    print "nominal: %s +/- %s"%(avg,std)
    idxs=[]
    i=-1
    for val in vals:
        i+=1
        if avg-allowedstd*std<=val<=avg+allowedstd*std:
            continue
        idxs.append(i)
    return idxs
    
def runlocaljob(cmd,logfile,runpath="."):
    curpath=os.getcwd()
    os.chdir(runpath)
    start=getdate(True)
    out,err,returncode=cmdline(cmd)
    end=getdate(True)
    os.chdir(curpath)
    makelocallog(logfile,out,err,returncode,start,end,[cmd])

def makelocallog(flog,out,err,returncode,start,end,cmds=[],append=False):
    if append:
        flog=open(flog,'a')
    else:
        flog=open(flog,'w')
    hostname=socket.gethostname()
    writeline(flog,"Starting on %s, %s"%(hostname,start))
    writeline(flog,"job running localy")
    if cmds!=[]:
        writeline(flog,"\n- COMMANDS:")
        for cmd in cmds:
            writeline(flog,cmd)
    writeline(flog,"\n- RETURNCODE:%s"%returncode)
    writeline(flog,"\n- STDOUT:\n%s"%out)
    writeline(flog,"\n- STDERR:\n%s"%err)
    writeline(flog,"\nDone, %s"%end)
    flog.close()

def makedir(d):
    if not os.path.isdir(d):
        os.makedirs(d)

def print_latex_table(rows):
    print '\\toprule'
    for row in rows:
        for i,val in enumerate(row):
            val=str(val)
            val=val.replace('+/-',"$\\pm$")
            if i==len(row)-1:
                print val+" \\\\"
            else:
                print str(val)+" & ",
        if row!=rows[-1]:
            print '\\midrule'
    print '\\bottomrule'
    
def print_copy_table(rows):
    for row in rows:
        for i,val in enumerate(row):
            if i==len(row)-1:
                print val
            else:
                print str(val)+",",

def print_table(rows):
    print
    rows=[[str(val) for val in row] for row in rows]
    widths = [max(map(len, col)) for col in zip(*rows)]
    for row in rows:
        print "  ".join((val.ljust(width) for val, width in zip(row, widths)))
    print 

def stripe_dir(path,mega=4,oss=10):
    cmdline("lfs setstripe -s %sM -c %s %s"%(mega,oss,path))
    
def writeline(f,l):
    f.write(l+"\n")

def getdate(logstyle=False):
    if logstyle:
        return str(datetime.datetime.now().strftime("%a %b %d %X %Z %Y")).replace("  20"," IST 20")
    return str(datetime.datetime.now()).rsplit(':',1)[0]

def report_node(logline,problem):
    dct=loadjsonfile("/srv01/agrp/mattiasb/badnodes.py")
    badnode=logline.split('.')[0].split()[-1]
    date=logline.split(',')[1]
    addkey(dct,problem)
    dct[problem][badnode]=date
    savejsonfile(dct,"/srv01/agrp/mattiasb/badnodes.py")

def appendkey(dct,key,val,unique=True):
    if not key in dct:
        dct[key]=[val]
    else:
        if not unique or (unique and val not in dct[key]): 
            dct[key].append(val)
            
def delkey(dct,key):
    if key in dct:
        del dct[key]

def delval(lis,val):
    if val in lis:
        lis.remove(val)

def inkey(dct,key,val):
    if key in dct and val in dct[key]:
        return True
    else:
        return False

def addkey(dct,key,val='dictionnary_never_a_string'):
    if val=='dictionnary_never_a_string':
        val={}
    if not key in dct:
        dct[key]=val
        
def appenduniq(lst,val):
    if val not in lst:
        lst.append(val)
    
def make_submit_file(job_tag,setup_path,log_file,fsub,queue,command_lines,setup_lines=None,farm_lines=None,finish_lines=None,setup_atlas=True,doarray=[]):
    if farm_lines==None:
        farm_lines=[
            "#!/bin/zsh",
            "",
            "#PBS -j oe",
            "#PBS -m n",
            "#PBS -o %s"%log_file,
            "#PBS -q %s"%queue,
            "#PBS -N %s"%job_tag,
        ]
        if doarray!=[]:
            # farm_lines+=["#PBS -t %s-%s%%%s"%(doarray[0],doarray[1],doarray[2])]
            farm_lines+=["#PBS -t %s-%s"%(doarray[0],doarray[1])]
        farm_lines+=[
            "",
            "checknode=`\ls /storage/agrp/mattiasb`",
            "if [[ $? != 0 ]]; then",
            "    echo 'BADNODE'",
            "    echo \"`hostname`, `date`\" >> /srv01/agrp/mattiasb/badnodes.txt",
            "    break",
            "fi",
            "echo \"Starting on `hostname`, `date`\"",
            "echo \"jobs id: ${PBS_JOBID}\"",
            ""
        ]
    for l in farm_lines:
        writeline(fsub,l)
    writeline(fsub,"cd %s"%setup_path)
    if setup_atlas:
        writeline(fsub,"export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase")
        writeline(fsub,"source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh")
    if setup_lines==None:
        setup_lines=[
            "source rcSetup.sh",
            "echo \"using root: `which root`\"",
            "echo \"using rootcoredir: $ROOTCOREDIR\"",
            ""
        ]
    for l in setup_lines:
        writeline(fsub,l)
    writeline(fsub,"#---------------------------------------------------------------------------------#")
    # command_lines=[
    #     "cd %s"%cfg.ana_path,
    #     "./runAnalysis.py %s/%s --restrict \'%s\' --option outputFile:%s/outputs/%s.root:samples"%(cfg.out_path,cfgfile,restrict_str,cfg.out_path,job),
    #     ""
    # ]
    for l in command_lines:
        writeline(fsub,l)
    writeline(fsub,"#---------------------------------------------------------------------------------#")
    if finish_lines==None:
        finish_lines=["echo \"Done, `date`\""]
    for l in finish_lines:
        writeline(fsub,l)

def copy_file(inf,outf):
    shutil.copyfile(inf,outf)
    
def copy_and_sed(infile,outfile,replace_dct=None):
    lines=open(infile).readlines()
    fout=open(outfile,"w")
    if replace_dct:
        for l in lines:
            for pattern,rule in replace_dct.iteritems():
                l=l.replace(pattern,rule)
            fout.write(l)
    else:
        for l in lines:
            fout.write(l)
    fout.close()
    
def get_size(path):
    total_size=0
    if os.path.isfile(path):
        total_size=os.path.getsize(path)
    else:
        for dirpath,dirnames,filenames in os.walk(path):
            for f in filenames:
                fp=os.path.join(dirpath, f)
                total_size+=os.path.getsize(fp)
    return total_size*0.00000095367432

def pretty_print(obj,title='',Indent=2):
    if title:
        info1(title)
    pp=pprint.PrettyPrinter(indent=Indent)
    pp.pprint(obj)
    print

def loadjsonfile(pathfile):
    if not os.path.isfile(pathfile):
        f=open(pathfile,'w')
        f.write('{}')
        f.close()
    with open(pathfile) as f:
        return byteify(json.load(f))

def savejsonfile(obj,pathfile,indent=2,sort=True):
    with open(pathfile,"w") as f:
        json.dump(obj,f,indent=indent,sort_keys=sort)
    
def warn(warnstr1,warnstr2=""):
    if warnstr2: print "WARNING:",warnstr1+':',warnstr2
    else: print "WARNING:",warnstr1

def info(warnstr1,warnstr2=""):
    if warnstr2: print "\n>>>>",warnstr1+':',warnstr2
    else: print "\n>>>>",warnstr1
def info1(warnstr1,warnstr2=""):
    if warnstr2: print "  >>",warnstr1+':',warnstr2
    else: print "  >>",warnstr1
def info0(warnstr1,warnstr2=""):
    if warnstr2: print warnstr1+':',warnstr2
    else: print warnstr1

def byteify(input):
   # Encodes any input from unicode to normal string
    if isinstance(input, dict):
        return {byteify(key):byteify(value) for key,value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

def submit2farm(fname,nodes=1,mem=None,vmem=None,speed=None,cores=None):
    returncode=""
    while returncode!=0:
        if returncode!="":
            print returncode,err
        subcmd="qsub -l nodes=%s"%nodes # Default in farm is 1
        if speed!=None:
            subcmd+=",speed%s"%speed # Minimum is 4 anyways
        if cores!=None:
            subcmd+=",ppn=%s"%cores 
        if mem!=None:
            subcmd+=",mem=%sg"%mem # Default in farm is 2g
        if vmem!=None:
            subcmd+=",vmem=%sg"%vmem # Default in farm is 4g
        subcmd+=" %s"%fname
        out,err,returncode=cmdline(subcmd)
        if returncode==228:
            warn("Too many jobs submitted")
            return None
    jobID=out.split('.')[0].rstrip()
    print jobID,fname
    return jobID

# def check_running(jobid,user="mattiasb"):
#     returncode=""
#     while(returncode!=0):
#         if returncode!="": 
#             print returncode,err
#         out,err,returncode=cmdline("qstat -u %s | grep %s  | cut -d\".\" -f1"%(user,jobid))
#     if out:
#         return out
#     else:
#         return 0

def check_running_all(user="mattiasb"):
    returncode=""
    while(returncode not in [0,1]):
        if returncode!="":
            print returncode,err
        out,err,returncode=cmdline("qstat -u %s | grep %s"%(user,user))
    if returncode==1:
        return [],[]
    jobids=[]
    elap_times=[]
    for l in out.splitlines():
        jobid=l.split('.')[0]
        elap_time=l.split()[-1].strip()
        if jobid.endswith('[]'): #job array
            returncode1=""
            while(returncode1!=0):
                if returncode1!="":
                    print "qstat error",returncode1,err1
                out1,err1,returncode1=cmdline("qstat -t \'%s\'"%jobid)
            for l1 in out1.splitlines():
                jobid=l1.split('.')[0]
                elap_time=l1.split()[-1].strip()
                jobids.append(jobid)
                elap_times.append(elap_time)
        else:
            jobids.append(jobid)
            elap_times.append(elap_time)
    return jobids,elap_times
    
def cmdline(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        stderr=PIPE,
	shell=True
    )
    out,err=process.communicate()
    returncode=process.returncode
    return out,err,returncode

def runshell(command):
    info0("Running shell command: %s"%command)
    dct={}
    dct["out"],dct["err"],dct["returncode"]=cmdline(command)
    pretty_print(dct)

def go_to_dir(path1,path2=''):
    ## Note: to get current dir - os.path.dirname(os.path.realpath(__file__))
    os.chdir(path1+'/'+path2)
