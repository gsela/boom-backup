"""
"""
import ROOT
import uuid
# HAPPy imports
from happy.plot import Plot, TextElement
from happy.dataMcPlot import DataMcPlot
from happy.doublePlot import DoublePlot
from happy.dataMcRatioPlot import DataMcRatioPlot
from happy.variable import Binning, Variable, var_Ratio, var_Entries
from happy.style import Style
from happy.plotDecorator import AtlasTitleDecorator

from ..selection_utils import filter_selections
from ..utils import blinded_hist, yield_table, lumi_calc, plot_title_extension, hist_max

def make_rqcd_plot(
    selections,
    hist_list,
    variable, 
    print_lumi=True,
    **kwargs):
    """
    """
    _sels = filter_selections(selections, **kwargs)
    _title, _ext = plot_title_extension(_sels, **kwargs)

    print 'BOOM: make rQCD plot for', _title, variable.name

    #     maxi = hist_max(hist_list)
    maxi = 0.4 # hack


    var_rqcd = Variable('var_rqcd', '', 'r_{QCD}', '', Binning(1, 0, 1.2 * maxi))
    plot = Plot(
        uuid.uuid4().hex,
        variable,
        var_rqcd)

    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 3
    plot.showBinWidthY = False
    te = TextElement(_title, x=0.26, y=0.96)
    print te.size
    te.size = 0.8 * te.size
    plot.textElements.append(te)

    colors = [
        ROOT.kBlack,
        ROOT.kOrange,
        ROOT.kRed,
        ROOT.kViolet,
        ROOT.kGreen,
        ]

    for h, col in zip(hist_list, colors):
        h.SetLineColor(col)
        h.SetMarkerColor(col)
        plot.addHistogram(h, 'PE')
    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )
    plot.draw()
    plot.saveAs('plots/rqcd_{}_{}.pdf'.format(variable.name, _ext))




def derive_syst_unc(
    antitau_histo,
    nos_histo,
    variable,
    category):

    """
    """

    antitau_histo.SetTitle("Anti-ID SR")
    antitau_histo.SetLineColor(ROOT.kRed)
    antitau_histo.SetMarkerColor(ROOT.kRed)
   
    nos_histo.SetTitle("nOS")
    nos_histo.SetLineColor(ROOT.kBlue)
    nos_histo.SetMarkerColor(ROOT.kBlue)
     
    # normalis histogram to 1 -> shape uncertainty 
    antitau_histo.Scale(1/antitau_histo.Integral())
    nos_histo.Scale(1/nos_histo.Integral())

    var_ratio = Variable('Ratio', binning=Binning(low=0.5, high=1.5))
    var_ratio.binning.nDivisions = 5

    plot = DoublePlot(
        uuid.uuid4().hex,
        variable,
        var_Entries,
        var_ratio, 0.8, 0.2)

    # plot style alteration
    plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 4
    plot.topMargin = 0.4

    plot.plotUp.addHistogram(antitau_histo, 'E0')
    plot.plotUp.addHistogram(nos_histo, 'E0')

    h_ratio  = antitau_histo.Clone()
    

    h_ratio.SetLineColor(ROOT.kBlack)
    h_ratio.SetMarkerColor(ROOT.kBlack)
    h_ratio.Divide(nos_histo)
    plot.plotDown.addHistogram(h_ratio, 'E0')

    plot.draw()
    plot.saveAs('plots/hh_fake_uncert_{}_{}.pdf'.format(variable.name, category ))

    outfile = ROOT.TFile('hh_fake_uncert_{}_{}.root'.format(variable.name, category),'recreate')
    h_ratio.SetName('hh_fake_uncert_{}_{}'.format(variable.name, category))
    h_ratio.Write()
    outfile.Close()    




