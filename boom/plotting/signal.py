import ROOT
import uuid
from ..selection_utils import filter_selections
from ..utils import plot_title_extension, lumi_calc
from happy.plotDecorator import AtlasTitleDecorator
from happy.plot import Plot, TextElement
from happy.style import Style

def make_signal_production_comparison_plot(
    processor,
    selections,
    variable,
    title=None,
    unit_norm=False,
    print_lumi=True,
    **kwargs):
    

    _sels = filter_selections(selections, **kwargs)

    # plot title, file extension
    if title is None:
        _title, _ext = plot_title_extension(_sels, **kwargs)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    print 'BOOM: make plot for', _title

    _MERGING_SCHEME = {
        'VH': [('WH', 'ZH'), 'VH#rightarrow#tau#tau'],
        'Ztt': [('ZttQCD', 'ZttEWK'), 'Z#rightarrow#tau#tau'],
        'Zll': [('ZllQCD', 'ZllEWK'), 'Z#rightarrowll'], 
        'HWW': [('VBFHWW', 'ggHWW'),  'H#rightarrowWW'],
        }

    processor._process_merging_scheme = _MERGING_SCHEME

    h_vbf = processor.get_hist_physics_process('VBFH', _sels, variable)
    h_ggh = processor.get_hist_physics_process('ggH', _sels, variable)
    h_vh  = processor.get_hist_physics_process('VH', _sels, variable)
    h_tth = processor.get_hist_physics_process('ttH', _sels, variable)
  
    h_vbf.SetTitle('VBFH#rightarrow#tau#tau')
    h_ggh.SetTitle('ggH#rightarrow#tau#tau')
    h_tth.SetTitle('ttH#rightarrow#tau#tau')

    if unit_norm:
        h_vbf.Scale(1. / h_vbf.Integral())
        h_ggh.Scale(1. / h_ggh.Integral())
        h_vh.Scale(1. / h_vh.Integral())
        h_tth.Scale(1. / h_tth.Integral())

    h_vbf.SetLineColor(ROOT.kBlue)
    h_ggh.SetLineColor(ROOT.kRed)
    h_vh.SetLineColor(ROOT.kViolet)
    h_tth.SetLineColor(ROOT.kGreen)

    plot = Plot(
        uuid.uuid4().hex,
        variable)
    plot.addHistogram(h_vbf)
    plot.addHistogram(h_ggh)
    plot.addHistogram(h_vh)
    plot.addHistogram(h_tth)
    plot.drawStatError = True

    # plot style alteration
    plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Simulation')
    plot.titleDecorator.textSize = 0.04
    plot.titleDecorator.labelTextSize = 0.04
    plot.legendDecorator.textSize = 0.04
    plot.legendDecorator.maxEntriesPerColumn = 3
#     plot.topMargin = 0.25
    plot.topMargin = 0.5

    te = TextElement(_title, x=0.26, y=0.96)
    te.size = 0.75 * te.size
    plot.textElements.append(te)
    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )

#     plot.titles.append('{}: {}'.format(title, process[0].title))
    plot.draw()
    plot.saveAs('./plots/plot_compare_signal_production_{}_{}.pdf'.format(variable.name, _ext))

