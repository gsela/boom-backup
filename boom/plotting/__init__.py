"""
Package holding the plotting functions
"""
from .basic  import basic_plot
from .basic  import make_fakefactor_plot
from .data_mc import make_data_mc_plot
from .fake import make_fake_template_plot
from .fake import make_region_comparison_plot
from .ff import make_ff_plot
from .jer import make_sys_nom_plot
from .jer import make_yield_comparison_plot
from .roc import make_vbf_roc_plot
from .rqcd import make_rqcd_plot
from .rqcd import derive_syst_unc
from .signal import make_signal_production_comparison_plot
from .stxs import make_stxs_bin_comparison_plot
from .stxs import compare_selections_plot
from .zll import make_zllvr_proxy_comparison_plot, make_zllvr_proxy_comparison_plot_2D
from .zll import make_Quentin_plot, make_Quentin_plot_from_condor, make_Quentin_plot_from_condor_with_data
from .zll import make_zllvr_closure_plot, make_zllvr_closure_plot_2D
from .zll import make_channel_comparison_plot
from .zll import make_process_plot, make_nf_plot
from .ztt import make_mad_vs_she_plot
from gal_plot_compare import g_plot_mattias
from gal_plot_compare import g_plot_mattias_mc
from gal_plot_compare import g_plot_compare_EB
__all__ = [
    'basic_plot',
    'make_fakefactor_plot',
    'make_data_mc_plot',
    'make_fake_template_plot',
    'make_ff_plot',
    'make_vbf_roc_plot',
    'make_rqcd_plot',
    'derive_syst_unc',
    'make_signal_production_comparison_plot',
    'make_stxs_bin_comparison_plot',
    'compare_selections_plot',
    'make_zllvr_proxy_comparison_plot',
    'make_zllvr_closure_plot',
    'make_Quentin_plot',
    'make_Quentin_plot_from_condor',
    'make_zllvr_proxy_comparison_plot_2D',
    'make_zllvr_closure_plot_2D',
    'make_channel_comparison_plot',
    'make_region_comparison_plot',
    'make_region_comparison_plot_hh',
    'make_prong_comparison_plot_hh',
    'make_sys_nom_plot',
    'make_yield_comparison_plot',
    'make_process_plot',
    'make_nf_plot',
    'make_mad_vs_she_plot',
    'g_plot_mattias',
    'g_plot_compare_EB',
    'g_plot_mattias_mc',

]
