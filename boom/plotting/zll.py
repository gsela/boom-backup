"""
"""
import ROOT
import uuid
# HAPPy imports
from happy.plot import Plot, TextElement
from happy.doublePlot import DoublePlot, PlotMatrix
from happy.dataMcPlot import DataMcPlot
from happy.dataMcRatioPlot import DataMcRatioPlot
from happy.variable import Binning, Variable, var_Ratio, var_Entries
from happy.style import Style
from happy.plotDecorator import AtlasTitleDecorator

from ..selection_utils import filter_selections
from ..utils import blinded_hist, yield_table, lumi_calc, plot_title_extension, hist_max
from ..variables import VARIABLES

def make_process_plot(
    processor,
    process_name,
    selections, 
    variable, 
    title=None,
    print_lumi=True,
    **kwargs):
    """
    """
    
    _sels = filter_selections(selections, **kwargs)

    # plot title, file extension
    if title is None:
        _title, _ext = plot_title_extension(_sels, **kwargs)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    print 'BOOM: make plot for', _title
    plot = Plot(
        uuid.uuid4().hex, 
        variable)

    # plot style alteration
    plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Simulation')
    plot.titleDecorator.textSize = 0.04
    plot.titleDecorator.labelTextSize = 0.04
    plot.legendDecorator.textSize = 0.04
    plot.legendDecorator.maxEntriesPerColumn = 3
#     plot.topMargin = 0.25
    plot.topMargin = 0.5
    # retrieve data histogram, blind if necessary and plot

    # retrieve histogram
    h = processor.get_hist_physics_process(process_name, _sels, variable)
    plot.addHistogram(h, stacked=True)
    
    te = TextElement(_title, x=0.26, y=0.96)
    te.size = 0.75 * te.size
    plot.textElements.append(te)
    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )

    plot.draw()
#     plot.saveAs('/afs/cern.ch/work/m/mojeda/HLep/boom/plots/plot_{}_{}_{}.pdf'.format(process_name, variable.name, _ext))
    plot.saveAs('plots/plot_{}_{}_{}.pdf'.format(process_name, variable.name, _ext))

def make_zllvr_proxy_comparison_plot(
    processor,
    selections_sr,
    selections_zllvr,
    variable,
    title='Boost SR'):
    """
    """
    print 'BOOM: make plot for', title, variable.name

    if variable.name == 'tau_0_pt_corr':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_0_pt'])
    elif variable.name == 'tau_1_pt_corr':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_1_pt'])
    elif variable.name == 'ditau_pt_corr':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['higgs_pt'])
    elif variable.name == 'met_corr':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['met'])
    elif variable.name == 'x0_corr':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['x0'])
    elif variable.name == 'x1_corr':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['x1'])
    elif variable.name == 'ditau_dr_corr':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['ditau_dr'])
    elif variable.name == 'ditau_deta_corr':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['ditau_deta'])
    elif variable.name == 'visible_mass_corr':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['visible_mass'])
    elif variable.name == 'taus_pt_ratio_corr':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['taus_pt_ratio'])
    elif variable.name == 'eta_corr_0':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['tau_0_eta'])
    elif variable.name == 'eta_corr_1':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['tau_1_eta'])
    elif variable.name == 'phi_corr_0':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['tau_0_phi'])
    elif variable.name == 'phi_corr_1':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['tau_1_phi'])
    elif variable.name == 'dphi_MET_corr_0':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['dphi_MET_0'])
    elif variable.name == 'dphi_MET_corr_1':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['dphi_MET_1'])
    else:
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    variable)

    h_zllvr    = processor.get_hist_physics_process('Zll', selections_zllvr,  variable)

        
    h_sr.SetTitle('SR')
    h_zllvr.SetTitle('Zll VR')

    h_sr.Scale(1. / h_sr.Integral())
    h_zllvr.Scale(1. / h_zllvr.Integral())

    h_sr.SetLineColor(ROOT.kBlue)
    h_zllvr.SetLineColor(ROOT.kRed)

    plot = Plot(
        uuid.uuid4().hex,
        variable)
    plot.addHistogram(h_sr)
    plot.addHistogram(h_zllvr)
    plot.drawStatError = True

    plot.titles.append('{}'.format(title))
#     plot.logY = True
    plot.draw()
#     plot.saveAs('/afs/cern.ch/user/m/mojeda/afswork/HLep/boom/plots/plot_zllvr_proxy_comparison_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))
    plot.saveAs('plots/plot_zllvr_proxy_comparison_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))


def make_Quentin_plot(
    processor,
    selections_sr,
    selections_cr,
    selections_zllvr,
    variable,
    title='Boost SR'):
    """
    """
    print 'BOOM: make plot for', title, variable.name

    if variable.name == 'tau_0_pt_corr':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_0_pt'])
        h_cr    = processor.get_hist_physics_process('Zll', selections_cr,    VARIABLES['tau_0_pt'])
    elif variable.name == 'tau_1_pt_corr':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_1_pt'])
        h_cr    = processor.get_hist_physics_process('Zll', selections_cr,    VARIABLES['tau_1_pt'])
    elif variable.name == 'higgs_pt':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['higgs_pt'])
        h_cr = processor.get_hist_physics_process('Zll', selections_cr, VARIABLES['ditau_pt'])
    elif variable.name == 'met_corr':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['met'])
        h_cr = processor.get_hist_physics_process('Zll', selections_cr, VARIABLES['met'])
    elif variable.name == 'x0_corr':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['x0'])
    elif variable.name == 'x1_corr':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['x1'])
    elif variable.name == 'ditau_dr_corr':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['ditau_dr'])
    elif variable.name == 'ditau_deta_corr':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['ditau_deta'])
    elif variable.name == 'visible_mass_corr':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['visible_mass'])
    elif variable.name == 'taus_pt_ratio_corr':
        h_sr = processor.get_hist_physics_process('Ztt', selections_sr, VARIABLES['taus_pt_ratio'])
    else:
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    variable)
        h_cr    = processor.get_hist_physics_process('Zll', selections_cr,    variable)

    h_zllvr    = processor.get_hist_physics_process('Zll', selections_zllvr,  variable)

        
    h_sr.SetTitle('Z#tau#tau')
    h_cr.SetTitle('Zll')
    h_zllvr.SetTitle('Zll corrected')

    h_sr.Scale(1. / h_sr.Integral())
    h_cr.Scale(1. / h_cr.Integral())
    h_zllvr.Scale(1. / h_zllvr.Integral())

    h_sr.SetLineColor(ROOT.kBlue)
    h_cr.SetLineColor(46)
    h_cr.SetLineStyle(7)
    h_zllvr.SetLineColor(ROOT.kRed)

    h_sr.SetMarkerSize(0)
    h_cr.SetMarkerSize(0)
    h_zllvr.SetMarkerSize(0)

    yVarDown = Variable ( 'Ratio', binning=Binning(low=0.1, high=1.9) )
    doubleTest = DoublePlot('testDoublePlot', variable, var_Entries, yVarDown, 474, 192)
    doubleTest.sizeY = 800
    doubleTest.referenceSizeY = 600
    doubleTest.titleDecorator = AtlasTitleDecorator('Internal')
    doubleTest.titleDecorator.textSize = 0.04
    doubleTest.titleDecorator.labelTextSize = 0.04
    doubleTest.legendDecorator.textSize = 0.04
    doubleTest.legendDecorator.maxEntriesPerColumn = 3
    doubleTest.topMargin = 0.5

    
#    plot = Plot(
#        uuid.uuid4().hex,
#        variable)

    # plot style alteration
    #plot.statErrorTitle = 'Stat.'
    #plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    #plot.drawStatError = True
#    plot.titleDecorator = AtlasTitleDecorator('Internal')
#    plot.titleDecorator.textSize = 0.04
#    plot.titleDecorator.labelTextSize = 0.04
#    plot.legendDecorator.textSize = 0.04
#    plot.legendDecorator.maxEntriesPerColumn = 3
#    plot.topMargin = 0.5
    
#    plot.addHistogram(h_cr, 'HIST E0', stacked=False)
    #    plot.addHistogram(h_sr, 'HIST E0')
#    plot.setDataHistogram(h_sr, 'HIST E0')
#    plot.addHistogram(h_zllvr, 'HIST E0', stacked=False)

    doubleTest.plotUp.addHistogram(h_cr, 'HIST E0', stacked=False)
    #    doubleTest.addHistogram(h_sr, 'HIST E0')
    doubleTest.plotUp.addHistogram(h_sr, 'HIST E0', stacked=False)
    doubleTest.plotUp.addHistogram(h_zllvr, 'HIST E0', stacked=False)

    nbins = h_cr.GetXaxis().GetNbins()
    maxbins = h_cr.GetXaxis().GetBinUpEdge(h_cr.GetXaxis().GetLast())
    minbins = h_cr.GetXaxis().GetBinLowEdge(h_cr.GetXaxis().GetFirst())
    
    hRatio = ROOT.TH1D('hRatio', 'Ratio', nbins, minbins, maxbins)
    hRatio.Add(h_cr)
    hRatio.Divide(h_sr)

    hRatio2 = ROOT.TH1D('hRatio2', 'Ratio', nbins, minbins, maxbins)
    hRatio2.Add(h_zllvr)
    hRatio2.Divide(h_sr)

    hRatio3 = ROOT.TH1D('hRatio3', 'Ratio', nbins, minbins, maxbins)
    hRatio3.Add(h_cr)
    hRatio3.Divide(h_cr)

    hRatio.SetMarkerColor(46)
    hRatio.SetLineColor(46)
    hRatio.SetMarkerStyle(ROOT.kMultiply)
    hRatio2.SetMarkerColor(ROOT.kRed)
    hRatio2.SetLineColor(ROOT.kRed)
    hRatio3.SetLineColor(ROOT.kBlue)
    
    doubleTest.plotDown.addHistogram( hRatio, 'E0', stacked=False )
    doubleTest.plotDown.addHistogram( hRatio2, 'E0', stacked=False )
    doubleTest.plotDown.addHistogram( hRatio3, 'HIST', stacked=False )
    
    #doubleTest.drawStatError=False
    
    # plot title, file extension
    if title is None:
        _title, _ext = plot_title_extension(selections_sr, **kwargs)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    te = TextElement(_title, x=0.26, y=0.96)
    te.size = 0.75 * te.size
    doubleTest.textElements.append(te)
    luminosity = lumi_calc(selections_sr)
    doubleTest.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.))
    
    #doubleTest.titles.append('{}'.format(title))
#     doubleTest.logY = True
    doubleTest.draw()
    doubleTest.saveAs('plots/plot_Quentin_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))

def make_zllvr_proxy_comparison_plot_2D(
    processor,
    selections_sr,
    selections_zllvr,
    variable_x,
    variable_y,
    title='Boost SR'):
    """                                                                                                                                                             
    """
    print 'BOOM: make plot for', title, variable_x.name, variable_y.name

    h_sr    = processor.get_hist_physics_process_2D('Ztt', selections_sr,    variable_x, variable_y)
    h_zllvr = processor.get_hist_physics_process_2D('Zll', selections_zllvr, variable_x, variable_y)

    h_sr.SetTitle('SR')
    h_zllvr.SetTitle('Zll VR')

    h_sr.Scale(1. / h_sr.Integral())
    h_zllvr.Scale(1. / h_zllvr.Integral())

    h_sr.SetLineColor(ROOT.kBlue)
    h_zllvr.SetLineColor(ROOT.kRed)

    h_sr.SetLineWidth(1)
    h_zllvr.SetLineWidth(1)

    plot = Plot(
        uuid.uuid4().hex,
        variable_x,
        variable_y)

    h_sr.SetContour(75)
    h_zllvr.SetContour(75)
    plot.addHistogram(h_sr,'CONT3')
    plot.addHistogram(h_zllvr, 'CONT3')
#    plot.addHistogram(h_sr,'scat')                                                                                                                                 
#    plot.addHistogram(h_zllvr, 'scat')                                                                                                                             
    plot.drawStatError = True

    plot.titles.append('{}'.format(title))
    plot.draw()
#     plot.saveAs('/afs/cern.ch/work/m/mojeda/HLep/boom/plots/plot_zllvr_proxy_comparison_{}_{}_{}.pdf'.format(variable_x.name, variable_y.name, title.replace(',', '').replace(' ', '_')))
    plot.saveAs('plots/plot_zllvr_proxy_comparison_{}_{}_{}.pdf'.format(variable_x.name, variable_y.name, title.replace(',', '').replace(' ', '_')))

    
def make_channel_comparison_plot(
    processor,
    selections,
    variable,
    process_name='Ztt',
    title=None,
    norm_unit=True):

    if title is None:
        _title, _ext = plot_title_extension(selections)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    print 'BOOM: make plot for', title, process_name, variable.name
    sels_ll = filter_selections(selections, channels=('emu', 'mue'))
    sels_lh = filter_selections(selections, channels=('e1p', 'e3p', 'mu1p', 'mu3p'))
    sels_hh = filter_selections(selections, channels=('1p1p', '1p3p', '3p1p', '3p3p'))

    process = processor.get_physics_process(process_name)
    h_ll = processor.get_hist_physics_process(process_name, sels_ll, variable)
    h_lh = processor.get_hist_physics_process(process_name, sels_lh, variable)
    h_hh = processor.get_hist_physics_process(process_name, sels_hh, variable)

    h_ll.SetTitle('leplep')
    h_lh.SetTitle('lephad')
    h_hh.SetTitle('hadhad')

    if norm_unit:
        h_ll.Scale(1. / h_ll.Integral())
        h_lh.Scale(1. / h_lh.Integral())
        h_hh.Scale(1. / h_hh.Integral())

    h_ll.SetLineColor(ROOT.kBlue)
    h_lh.SetLineColor(ROOT.kRed)
    h_hh.SetLineColor(ROOT.kViolet)

    plot = Plot(
        uuid.uuid4().hex,
        variable)
    plot.titleDecorator = AtlasTitleDecorator('Simulation')

    plot.addHistogram(h_ll)
    plot.addHistogram(h_lh)
    plot.addHistogram(h_hh)
    plot.drawStatError = True
    plot.titles.append('{}: {}'.format(title, process[0].title))

    plot.draw()
    plot.saveAs('plots/plot_compare_{}_{}_{}.pdf'.format(process_name, variable.name, title.replace(',', '').replace(' ', '_')))

def make_channel_comparison_plot_2D(
    processor,
    selections,
    variable_x,
    variable_y,
    process_name='Ztt',
    title='Boost SR'):

    print 'BOOM: make plot for', title, process_name, variable_x.name, variable_y.name
    sels_ll = filter_selections(selections, channels=('emu', 'mue'))
    sels_lh = filter_selections(selections, channels=('e1p', 'e3p', 'mu1p', 'mu3p'))
    sels_hh = filter_selections(selections, channels=('1p1p', '1p3p', '3p1p', '3p3p'))

    process = processor.get_physics_process(process_name)
    h_ll = processor.get_hist_physics_process_2D(process_name, sels_ll, variable_x, variable_y)
    h_lh = processor.get_hist_physics_process_2D(process_name, sels_lh, variable_x, variable_y)
    h_hh = processor.get_hist_physics_process_2D(process_name, sels_hh, variable_x, variable_y)

    h_ll.SetTitle('leplep')
    h_lh.SetTitle('lephad')
    h_hh.SetTitle('hadhad')

    h_ll.Scale(1. / h_ll.Integral())
    h_lh.Scale(1. / h_lh.Integral())
    h_hh.Scale(1. / h_hh.Integral())

    
    h_ll.SetMarkerColor(ROOT.kBlue)
    h_lh.SetMarkerColor(ROOT.kRed)
    h_hh.SetMarkerColor(ROOT.kGreen)

    h_ll.SetMarkerStyle(20)
    h_lh.SetMarkerStyle(20)
    h_hh.SetMarkerStyle(20)

    h_ll.SetMarkerSize(0.1)
    h_lh.SetMarkerSize(0.1)
    h_hh.SetMarkerSize(0.1)

    plot = Plot(
        uuid.uuid4().hex,
        variable_x,
        variable_y)
    plot.addHistogram(h_ll)
    plot.addHistogram(h_lh)
    plot.addHistogram(h_hh)
    plot.drawStatError = True

    plot.titles.append('{}: {}'.format(title, process[0].title))
    plot.draw()
    plot.saveAs('plots/plot_compare_{}_{}_{}_{}.pdf'.format(process_name, variable_x.name, variable_y.name, title.replace(',', '').replace(' ', '_')))

    
def make_zllvr_closure_plot(
    processor,
    selections_sr,
    selections_sr_truth,
    variable,
    title='Boost SR'):
    """
    """
    print 'BOOM: make plot for', title, variable.name

    if variable.name == 'tau_0_truth_pt_corr':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_0_pt'])
    elif variable.name == 'tau_1_truth_pt_corr':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_1_pt'])
    elif variable.name == 'ditau_dr_corr_truth':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['ditau_dr'])
    elif variable.name == 'ditau_deta_corr_truth':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['ditau_deta'])
    elif variable.name == 'tau_0_correction_truth_ratio':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_0_truth_ratio'])
    elif variable.name == 'tau_1_correction_truth_ratio':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_1_truth_ratio'])
    else:
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr, variable)

    h_corr    = processor.get_hist_physics_process('Ztt', selections_sr_truth,  variable)

    h_sr.SetTitle('SR')
    h_corr.SetTitle('Corrected Truth')

    h_sr.Scale(1. / h_sr.Integral())
    h_corr.Scale(1. / h_corr.Integral())

    h_sr.SetLineColor(ROOT.kBlue)
    h_corr.SetLineColor(ROOT.kRed)

    plot = Plot(
        uuid.uuid4().hex,
        variable)
    plot.addHistogram(h_sr)
    plot.addHistogram(h_corr)
    plot.drawStatError = True

    plot.titles.append('{}'.format(title))
#     plot.logY = True
    plot.draw()
#     plot.saveAs('/afs/cern.ch/user/m/mojeda/afswork/HLep/boom/plots/plot_zllvr_proxy_comparison_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))
    plot.saveAs('plots/plot_zllvr_closure_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))

def make_zllvr_closure_plot_2D(
    processor,
    selections_sr,
    selections_sr_truth,
    variable_x,
    variable_y,
    title='Boost SR'):
    """
    """
    print 'BOOM: make plot for', title, variable_x.name, variable_y.name

    #x-variables
    if variable_x.name == 'tau_0_truth_pt_corr':
        new_x = VARIABLES['tau_0_pt']
    elif variable_x.name == 'tau_1_truth_pt_corr':
        new_x = VARIABLES['tau_1_pt']
    elif variable_x.name == 'ditau_dr_corr_truth':
        new_x = VARIABLES['ditau_dr']
    elif variable_x.name == 'ditau_deta_corr_truth':
        new_x = VARIABLES['ditau_deta']
    else:
        new_x = variable_x
        
    #y-variables
    if variable_y.name == 'tau_0_truth_pt_corr':
        new_y = VARIABLES['tau_0_pt']
    elif variable_y.name == 'tau_1_truth_pt_corr':
        new_y = VARIABLES['tau_1_pt']
    elif variable_y.name == 'ditau_dr_corr_truth':
        new_y = VARIABLES['ditau_dr']
    elif variable_y.name == 'ditau_deta_corr_truth':
        new_y = VARIABLES['ditau_deta']
    else:
        new_y = variable_y

    h_sr    = processor.get_hist_physics_process_2D('Ztt', selections_sr, new_x, new_y)
    h_corr    = processor.get_hist_physics_process_2D('Ztt', selections_sr_truth,  variable_x, variable_y)

    h_sr.SetTitle('SR')
    h_corr.SetTitle('Corrected Truth')

    h_sr.Scale(1. / h_sr.Integral())
    h_corr.Scale(1. / h_corr.Integral())

    #h_sr.SetMarkerColor(ROOT.kBlue)
    #h_corr.SetMarkerColor(ROOT.kRed)

    h_sr.SetLineColor(ROOT.kBlue)
    h_corr.SetLineColor(ROOT.kRed)
    
    h_sr.SetMarkerStyle(20)
    h_corr.SetMarkerStyle(20)

    h_sr.SetMarkerSize(0.1)
    h_corr.SetMarkerSize(0.1)

    h_sr.SetContour(10)
    h_corr.SetContour(10)
    
    plot = Plot(
        uuid.uuid4().hex,
        variable_x,
        variable_y)
    plot.addHistogram(h_sr,"CONT3")
    plot.addHistogram(h_corr,"CONT3")
    plot.drawStatError = True

    plot.titles.append('{}'.format(title))
#     plot.logY = True
    plot.draw()
#     plot.saveAs('/afs/cern.ch/user/m/mojeda/afswork/HLep/boom/plots/plot_zllvr_proxy_comparison_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))
    plot.saveAs('plots/plot_zllvr_closure2D_{}_{}_{}.pdf'.format(variable_x.name, variable_y.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))


def make_Quentin_plot_from_condor_with_data(
    h_z_sr,
    h_z_cr,
    h_z_cr_corr,
    h_d_cr,
    h_d_cr_corr,
    variable,
    title='Boost SR',
    lumi=None,
    oversampling=1,
    ratio=False):
    """
    """
    print 'BOOM: make plot for', title, variable.name

    h_z_cr_corr.Scale(1. / oversampling)
    h_d_cr_corr.Scale(1. / oversampling)

    _sr_scale = h_z_cr_corr.Integral() / h_z_sr.Integral()
    h_z_sr.Scale(_sr_scale)

    _cr_scale = h_z_cr_corr.Integral() / h_z_cr.Integral()
    h_z_cr.Scale(_cr_scale)
    h_d_cr.Scale(_cr_scale)
    h_z_sr.SetTitle('Z#tau#tau x {0:1.2f}'.format(_sr_scale))
    h_z_cr.SetTitle('Zll x {0:1.2f}'.format(_cr_scale))
    h_z_cr_corr.SetTitle('Zll corrected')
    h_d_cr.SetTitle('Data x {0:1.2f}'.format(_cr_scale))
    h_d_cr_corr.SetTitle('Corrected Data')
    
    from ..extern.tabulate import tabulate
    print tabulate(
        [
            ['Uncorrected Z->ll', h_d_cr.Integral(0, h_d_cr.GetNbinsX() + 1) / h_z_cr.Integral(0, h_z_cr.GetNbinsX() + 1)],
            ['Corrected Z->ll', h_d_cr_corr.Integral(0, h_d_cr_corr.GetNbinsX() + 1) / h_z_cr_corr.Integral(0, h_z_cr_corr.GetNbinsX() + 1)],
            ['Z->tautau / Corrected Z->ll', h_z_sr.Integral(0, h_z_sr.GetNbinsX() + 1) / h_z_cr_corr.Integral(0, h_z_cr_corr.GetNbinsX() + 1)]
            ],
        headers=[
            'Scenario', 'Data / MC'])

#     h_sr.Scale(1. / h_sr.Integral())
#     h_cr.Scale(1. / h_cr.Integral())
#     h_zllvr.Scale(1. / h_zllvr.Integral())


    h_z_sr.SetLineColor(ROOT.kBlue)
    h_z_sr.SetLineStyle(7)
    h_z_cr.SetLineColor(46)
    h_z_cr.SetLineStyle(7)
    h_d_cr.SetLineColor(46)
    h_d_cr.SetMarkerColor(46)
    h_d_cr.SetMarkerStyle(4)
#     h_z_cr_corr.SetLineColor(ROOT.kRed)
#     h_d_cr_corr.SetMarkerColor(ROOT.kRed)
#     h_d_cr_corr.SetLineColor(ROOT.kRed)

    h_z_sr.SetMarkerSize(0)
    h_z_cr.SetMarkerSize(0)
    h_z_cr_corr.SetMarkerSize(0)

    ratio_zll_ztt = h_z_cr.Clone()
    ratio_zll_ztt.Divide(h_z_sr)

    ratio_zll_corr_ztt = h_z_cr_corr.Clone()
    ratio_zll_corr_ztt.Divide(h_z_sr)

    ratio_data_cr = h_d_cr.Clone()
    ratio_data_cr.Divide(h_z_cr)

    ratio_data_cr_corr = h_d_cr_corr.Clone()
    ratio_data_cr_corr.Divide(h_z_cr_corr)


    if ratio:
        y_zll_zcr = Variable(
            'Ratio_z', 
            title='Zll / Z#tau#tau', 
            binning=Binning(low=0.1, high=1.9))
        y_data_mc = Variable(
            'data_over_mc', 
            title='Data / MC', 
            binning=Binning(low=0.5, high=1.5))
        plot = PlotMatrix(
            'Plot',
            variablesX=[variable],
            variablesY=[y_data_mc, y_zll_zcr, var_Entries],
            padSizesY=[1, 1, 3])
        plot.sizeY = 800
        plot.referenceSizeY = 600
        plot.titleDecorator = AtlasTitleDecorator('Internal')
        plot.titleDecorator.textSize = 0.04
        plot.titleDecorator.labelTextSize = 0.04

        plot.plots[0][1].drawTitle = False
        plot.plots[0][1].drawLegend = False
        plot.plots[0][1].showBinWidthY = False
#        plot.plots[0][2].drawTitle = False
        plot.plots[0][0].drawLegend = False
        plot.plots[0][0].showBinWidthY = False



#         plot.legendDecorator.textSize = 0.04
#         plot.legendDecorator.maxEntriesPerColumn = 5
        
        plot.plots[0][2].addHistogram(h_z_cr, 'HIST E0')
        plot.plots[0][2].addHistogram(h_d_cr, 'PE0')
        plot.plots[0][2].addHistogram(h_z_cr_corr, 'HIST E0')
        plot.plots[0][2].addHistogram(h_d_cr_corr, 'PE0')
        plot.plots[0][2].addHistogram(h_z_sr, 'HIST E0')

        plot.plots[0][1].addHistogram(ratio_zll_ztt, 'PE0')
        plot.plots[0][1].addHistogram(ratio_zll_corr_ztt, 'PE0')

        plot.plots[0][0].addHistogram(ratio_data_cr, 'PE0')
        plot.plots[0][0].addHistogram(ratio_data_cr_corr, 'PE0')

        te = TextElement(title, x=0.26, y=0.96)
#         te.size = 0.75 * te.size
        plot.plots[0][2].textElements.append(te)

        if lumi != None:
            plot.plots[0][2].titles.append('L = %.3g fb^{-1}' % (lumi / 1000.) )



    else:

        plot = Plot(
            uuid.uuid4().hex,
            variable)

        # plot style alteration
        #plot.statErrorTitle = 'Stat.'
        #plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
        #plot.drawStatError = True
        plot.titleDecorator = AtlasTitleDecorator('Internal')
        plot.titleDecorator.textSize = 0.04
        plot.titleDecorator.labelTextSize = 0.04
        plot.legendDecorator.textSize = 0.04
        plot.legendDecorator.maxEntriesPerColumn = 5
        #     plot.topMargin = 0.25
        plot.topMargin = 0.5
        
        plot.addHistogram(h_z_cr, 'HIST E0')
        plot.addHistogram(h_d_cr, 'PE0')
        plot.addHistogram(h_z_cr_corr, 'HIST E0')
        plot.addHistogram(h_d_cr_corr, 'PE0')
        plot.addHistogram(h_z_sr, 'HIST E0')

        te = TextElement(title, x=0.26, y=0.96)
        te.size = 0.75 * te.size
        plot.textElements.append(te)
        #plot.titles.append('{}'.format(title))
        #     plot.logY = True


    plot.draw()
    plot.saveAs('plots/plot_Quentin_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))

def make_zllvr_proxy_comparison_plot_2D(
    processor,
    selections_sr,
    selections_zllvr,
    variable_x,
    variable_y,
    title='Boost SR'):
    """                                                                                                                                                             
    """
    print 'BOOM: make plot for', title, variable_x.name, variable_y.name

    h_sr    = processor.get_hist_physics_process_2D('Ztt', selections_sr,    variable_x, variable_y)
    h_zllvr = processor.get_hist_physics_process_2D('Zll', selections_zllvr, variable_x, variable_y)

    h_sr.SetTitle('SR')
    h_zllvr.SetTitle('Zll VR')

    h_sr.Scale(1. / h_sr.Integral())
    h_zllvr.Scale(1. / h_zllvr.Integral())

    h_sr.SetLineColor(ROOT.kBlue)
    h_zllvr.SetLineColor(ROOT.kRed)

    h_sr.SetLineWidth(1)
    h_zllvr.SetLineWidth(1)

    plot = Plot(
        uuid.uuid4().hex,
        variable_x,
        variable_y)

    h_sr.SetContour(75)
    h_zllvr.SetContour(75)
    plot.addHistogram(h_sr,'CONT3')
    plot.addHistogram(h_zllvr, 'CONT3')
#    plot.addHistogram(h_sr,'scat')                                                                                                                                 
#    plot.addHistogram(h_zllvr, 'scat')                                                                                                                             
    plot.drawStatError = True

    plot.titles.append('{}'.format(title))
    plot.draw()
#     plot.saveAs('/afs/cern.ch/work/m/mojeda/HLep/boom/plots/plot_zllvr_proxy_comparison_{}_{}_{}.pdf'.format(variable_x.name, variable_y.name, title.replace(',', '').replace(' ', '_')))
    plot.saveAs('plots/plot_zllvr_proxy_comparison_{}_{}_{}.pdf'.format(variable_x.name, variable_y.name, title.replace(',', '').replace(' ', '_')))

    
def make_channel_comparison_plot(
    processor,
    selections,
    variable,
    process_name='Ztt',
    title=None,
    norm_unit=True):

    if title is None:
        _title, _ext = plot_title_extension(selections)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    print 'BOOM: make plot for', title, process_name, variable.name
    sels_ll = filter_selections(selections, channels=('emu', 'mue'))
    sels_lh = filter_selections(selections, channels=('e1p', 'e3p', 'mu1p', 'mu3p'))
    sels_hh = filter_selections(selections, channels=('1p1p', '1p3p', '3p1p', '3p3p'))

    process = processor.get_physics_process(process_name)
    h_ll = processor.get_hist_physics_process(process_name, sels_ll, variable)
    h_lh = processor.get_hist_physics_process(process_name, sels_lh, variable)
    h_hh = processor.get_hist_physics_process(process_name, sels_hh, variable)

    h_ll.SetTitle('leplep')
    h_lh.SetTitle('lephad')
    h_hh.SetTitle('hadhad')

    if norm_unit:
        h_ll.Scale(1. / h_ll.Integral())
        h_lh.Scale(1. / h_lh.Integral())
        h_hh.Scale(1. / h_hh.Integral())

    h_ll.SetLineColor(ROOT.kBlue)
    h_lh.SetLineColor(ROOT.kRed)
    h_hh.SetLineColor(ROOT.kViolet)

    plot = Plot(
        uuid.uuid4().hex,
        variable)
    plot.titleDecorator = AtlasTitleDecorator('Simulation')

    plot.addHistogram(h_ll)
    plot.addHistogram(h_lh)
    plot.addHistogram(h_hh)
    plot.drawStatError = True
    plot.titles.append('{}: {}'.format(title, process[0].title))

    plot.draw()
    plot.saveAs('plots/plot_compare_{}_{}_{}.pdf'.format(process_name, variable.name, title.replace(',', '').replace(' ', '_')))

def make_channel_comparison_plot_2D(
    processor,
    selections,
    variable_x,
    variable_y,
    process_name='Ztt',
    title='Boost SR'):

    print 'BOOM: make plot for', title, process_name, variable_x.name, variable_y.name
    sels_ll = filter_selections(selections, channels=('emu', 'mue'))
    sels_lh = filter_selections(selections, channels=('e1p', 'e3p', 'mu1p', 'mu3p'))
    sels_hh = filter_selections(selections, channels=('1p1p', '1p3p', '3p1p', '3p3p'))

    process = processor.get_physics_process(process_name)
    h_ll = processor.get_hist_physics_process_2D(process_name, sels_ll, variable_x, variable_y)
    h_lh = processor.get_hist_physics_process_2D(process_name, sels_lh, variable_x, variable_y)
    h_hh = processor.get_hist_physics_process_2D(process_name, sels_hh, variable_x, variable_y)

    h_ll.SetTitle('leplep')
    h_lh.SetTitle('lephad')
    h_hh.SetTitle('hadhad')

    h_ll.Scale(1. / h_ll.Integral())
    h_lh.Scale(1. / h_lh.Integral())
    h_hh.Scale(1. / h_hh.Integral())

    
    h_ll.SetMarkerColor(ROOT.kBlue)
    h_lh.SetMarkerColor(ROOT.kRed)
    h_hh.SetMarkerColor(ROOT.kGreen)

    h_ll.SetMarkerStyle(20)
    h_lh.SetMarkerStyle(20)
    h_hh.SetMarkerStyle(20)

    h_ll.SetMarkerSize(0.1)
    h_lh.SetMarkerSize(0.1)
    h_hh.SetMarkerSize(0.1)

    plot = Plot(
        uuid.uuid4().hex,
        variable_x,
        variable_y)
    plot.addHistogram(h_ll)
    plot.addHistogram(h_lh)
    plot.addHistogram(h_hh)
    plot.drawStatError = True

    plot.titles.append('{}: {}'.format(title, process[0].title))
    plot.draw()
    plot.saveAs('plots/plot_compare_{}_{}_{}_{}.pdf'.format(process_name, variable_x.name, variable_y.name, title.replace(',', '').replace(' ', '_')))

    
def make_zllvr_closure_plot(
    processor,
    selections_sr,
    selections_sr_truth,
    variable,
    title='Boost SR'):
    """
    """
    print 'BOOM: make plot for', title, variable.name

    if variable.name == 'tau_0_truth_pt_corr':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_0_pt'])
    elif variable.name == 'tau_1_truth_pt_corr':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_1_pt'])
    elif variable.name == 'ditau_dr_corr_truth':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['ditau_dr'])
    elif variable.name == 'ditau_deta_corr_truth':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['ditau_deta'])
    elif variable.name == 'tau_0_correction_truth_ratio':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_0_truth_ratio'])
    elif variable.name == 'tau_1_correction_truth_ratio':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_1_truth_ratio'])
    else:
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr, variable)

    h_corr    = processor.get_hist_physics_process('Ztt', selections_sr_truth,  variable)

    h_sr.SetTitle('SR')
    h_corr.SetTitle('Corrected Truth')

    h_sr.Scale(1. / h_sr.Integral())
    h_corr.Scale(1. / h_corr.Integral())

    h_sr.SetLineColor(ROOT.kBlue)
    h_corr.SetLineColor(ROOT.kRed)

    plot = Plot(
        uuid.uuid4().hex,
        variable)
    plot.addHistogram(h_sr)
    plot.addHistogram(h_corr)
    plot.drawStatError = True

    plot.titles.append('{}'.format(title))
#     plot.logY = True
    plot.draw()
#     plot.saveAs('/afs/cern.ch/user/m/mojeda/afswork/HLep/boom/plots/plot_zllvr_proxy_comparison_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))
    plot.saveAs('plots/plot_zllvr_closure_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))

def make_zllvr_closure_plot_2D(
    processor,
    selections_sr,
    selections_sr_truth,
    variable_x,
    variable_y,
    title='Boost SR'):
    """
    """
    print 'BOOM: make plot for', title, variable_x.name, variable_y.name

    #x-variables
    if variable_x.name == 'tau_0_truth_pt_corr':
        new_x = VARIABLES['tau_0_pt']
    elif variable_x.name == 'tau_1_truth_pt_corr':
        new_x = VARIABLES['tau_1_pt']
    elif variable_x.name == 'ditau_dr_corr_truth':
        new_x = VARIABLES['ditau_dr']
    elif variable_x.name == 'ditau_deta_corr_truth':
        new_x = VARIABLES['ditau_deta']
    else:
        new_x = variable_x
        
    #y-variables
    if variable_y.name == 'tau_0_truth_pt_corr':
        new_y = VARIABLES['tau_0_pt']
    elif variable_y.name == 'tau_1_truth_pt_corr':
        new_y = VARIABLES['tau_1_pt']
    elif variable_y.name == 'ditau_dr_corr_truth':
        new_y = VARIABLES['ditau_dr']
    elif variable_y.name == 'ditau_deta_corr_truth':
        new_y = VARIABLES['ditau_deta']
    else:
        new_y = variable_y

    h_sr    = processor.get_hist_physics_process_2D('Ztt', selections_sr, new_x, new_y)
    h_corr    = processor.get_hist_physics_process_2D('Ztt', selections_sr_truth,  variable_x, variable_y)

    h_sr.SetTitle('SR')
    h_corr.SetTitle('Corrected Truth')

    h_sr.Scale(1. / h_sr.Integral())
    h_corr.Scale(1. / h_corr.Integral())

    #h_sr.SetMarkerColor(ROOT.kBlue)
    #h_corr.SetMarkerColor(ROOT.kRed)

    h_sr.SetLineColor(ROOT.kBlue)
    h_corr.SetLineColor(ROOT.kRed)
    
    h_sr.SetMarkerStyle(20)
    h_corr.SetMarkerStyle(20)

    h_sr.SetMarkerSize(0.1)
    h_corr.SetMarkerSize(0.1)

    h_sr.SetContour(10)
    h_corr.SetContour(10)
    
    plot = Plot(
        uuid.uuid4().hex,
        variable_x,
        variable_y)
    plot.addHistogram(h_sr,"CONT3")
    plot.addHistogram(h_corr,"CONT3")
    plot.drawStatError = True

    plot.titles.append('{}'.format(title))
#     plot.logY = True
    plot.draw()
#     plot.saveAs('/afs/cern.ch/user/m/mojeda/afswork/HLep/boom/plots/plot_zllvr_proxy_comparison_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))
    plot.saveAs('plots/plot_zllvr_closure2D_{}_{}_{}.pdf'.format(variable_x.name, variable_y.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))


def make_Quentin_plot_from_condor(
    h_sr,
    h_cr,
    h_zllvr,
    variable,
    title='Boost SR',
    ratio=False):
    """
    """
    print 'BOOM: make plot for', title, variable.name


        
    h_sr.SetTitle('Z#tau#tau')
    h_cr.SetTitle('Zll')
    h_zllvr.SetTitle('Zll corrected')

    h_sr.Scale(1. / h_sr.Integral())
    h_cr.Scale(1. / h_cr.Integral())
    h_zllvr.Scale(1. / h_zllvr.Integral())

    h_sr.SetLineColor(ROOT.kBlue)
    h_cr.SetLineColor(46)
    h_cr.SetLineStyle(7)
    h_zllvr.SetLineColor(ROOT.kRed)

    h_sr.SetMarkerSize(0)
    h_cr.SetMarkerSize(0)
    h_zllvr.SetMarkerSize(0)


    if ratio is True:
        yVarDown = Variable ( 'Ratio', binning=Binning(low=0.1, high=1.9) )
        plot = DoublePlot('testDoublePlot', variable, var_Entries, yVarDown, 474, 192)
        plot.sizeY = 800
        plot.referenceSizeY = 600
        plot.titleDecorator = AtlasTitleDecorator('Internal')
        plot.titleDecorator.textSize = 0.04
        plot.titleDecorator.labelTextSize = 0.04
        plot.legendDecorator.textSize = 0.04
        plot.legendDecorator.maxEntriesPerColumn = 3
        plot.topMargin = 0.5
        plot.plotUp.addHistogram(h_cr, 'HIST E0', stacked=False)
        plot.plotUp.addHistogram(h_sr, 'HIST E0', stacked=False)
        plot.plotUp.addHistogram(h_zllvr, 'HIST E0', stacked=False)

        nbins = h_sr.GetXaxis().GetNbins()
        maxbins = h_sr.GetXaxis().GetBinUpEdge(h_cr.GetXaxis().GetLast())
        minbins = h_sr.GetXaxis().GetBinLowEdge(h_cr.GetXaxis().GetFirst())

        hRatio = ROOT.TH1D('hRatio', 'Ratio', nbins, minbins, maxbins)
        hRatio.Add(h_cr)
        hRatio.Divide(h_sr)

        hRatio2 = ROOT.TH1D('hRatio2', 'Ratio', nbins, minbins, maxbins)
        hRatio2.Add(h_zllvr)
        hRatio2.Divide(h_sr)

        hRatio3 = ROOT.TH1D('hRatio3', 'Ratio', nbins, minbins, maxbins)
        #hRatio3.Add(h_sr)
        #hRatio3.Divide(h_sr)
        for i in range(nbins+1):
            hRatio3.SetBinContent(i,1)

        hRatio.SetMarkerColor(46)
        hRatio.SetLineColor(46)
        hRatio.SetMarkerStyle(ROOT.kMultiply)
        hRatio2.SetMarkerColor(ROOT.kRed)
        hRatio2.SetLineColor(ROOT.kRed)
        hRatio3.SetLineColor(ROOT.kBlue)

        plot.plotDown.addHistogram( hRatio, 'E0', stacked=False )
        plot.plotDown.addHistogram( hRatio2, 'E0', stacked=False )
        plot.plotDown.addHistogram( hRatio3, 'HIST', stacked=False )

        if title is None:
            _title, _ext = plot_title_extension(selections_sr, **kwargs)
        else:
            _title = title
            _ext = title.replace(',', '').replace(' ', '_')
            
        te = TextElement(_title, x=0.26, y=0.96)
        te.size = 0.75 * te.size
        plot.textElements.append(te)
        luminosity = 140000
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.))
    
    else:

        plot = Plot(
            uuid.uuid4().hex,
            variable)

        # plot style alteration
        #plot.statErrorTitle = 'Stat.'
        #plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
        #plot.drawStatError = True
        plot.titleDecorator = AtlasTitleDecorator('Internal')
        plot.titleDecorator.textSize = 0.04
        plot.titleDecorator.labelTextSize = 0.04
        plot.legendDecorator.textSize = 0.04
        plot.legendDecorator.maxEntriesPerColumn = 3
        #     plot.topMargin = 0.25
        plot.topMargin = 0.5
        
        plot.addHistogram(h_cr, 'HIST E0')
        plot.addHistogram(h_sr, 'HIST E0')
        plot.addHistogram(h_zllvr, 'HIST E0')

        te = TextElement(title, x=0.26, y=0.96)
        te.size = 0.75 * te.size
        plot.textElements.append(te)
        #plot.titles.append('{}'.format(title))
        #     plot.logY = True


    plot.draw()
    plot.saveAs('plots/plot_Quentin_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))

def make_zllvr_proxy_comparison_plot_2D(
    processor,
    selections_sr,
    selections_zllvr,
    variable_x,
    variable_y,
    title='Boost SR'):
    """                                                                                                                                                             
    """
    print 'BOOM: make plot for', title, variable_x.name, variable_y.name

    h_sr    = processor.get_hist_physics_process_2D('Ztt', selections_sr,    variable_x, variable_y)
    h_zllvr = processor.get_hist_physics_process_2D('Zll', selections_zllvr, variable_x, variable_y)

    h_sr.SetTitle('SR')
    h_zllvr.SetTitle('Zll VR')

    h_sr.Scale(1. / h_sr.Integral())
    h_zllvr.Scale(1. / h_zllvr.Integral())

    h_sr.SetLineColor(ROOT.kBlue)
    h_zllvr.SetLineColor(ROOT.kRed)

    h_sr.SetLineWidth(1)
    h_zllvr.SetLineWidth(1)

    plot = Plot(
        uuid.uuid4().hex,
        variable_x,
        variable_y)

    h_sr.SetContour(75)
    h_zllvr.SetContour(75)
    plot.addHistogram(h_sr,'CONT3')
    plot.addHistogram(h_zllvr, 'CONT3')
#    plot.addHistogram(h_sr,'scat')                                                                                                                                 
#    plot.addHistogram(h_zllvr, 'scat')                                                                                                                             
    plot.drawStatError = True

    plot.titles.append('{}'.format(title))
    plot.draw()
#     plot.saveAs('/afs/cern.ch/work/m/mojeda/HLep/boom/plots/plot_zllvr_proxy_comparison_{}_{}_{}.pdf'.format(variable_x.name, variable_y.name, title.replace(',', '').replace(' ', '_')))
    plot.saveAs('plots/plot_zllvr_proxy_comparison_{}_{}_{}.pdf'.format(variable_x.name, variable_y.name, title.replace(',', '').replace(' ', '_')))

    
def make_channel_comparison_plot(
    processor,
    selections,
    variable,
    process_name='Ztt',
    title=None,
    norm_unit=True):

    if title is None:
        _title, _ext = plot_title_extension(selections)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    print 'BOOM: make plot for', title, process_name, variable.name
    sels_ll = filter_selections(selections, channels=('emu', 'mue'))
    sels_lh = filter_selections(selections, channels=('e1p', 'e3p', 'mu1p', 'mu3p'))
    sels_hh = filter_selections(selections, channels=('1p1p', '1p3p', '3p1p', '3p3p'))

    process = processor.get_physics_process(process_name)
    h_ll = processor.get_hist_physics_process(process_name, sels_ll, variable)
    h_lh = processor.get_hist_physics_process(process_name, sels_lh, variable)
    h_hh = processor.get_hist_physics_process(process_name, sels_hh, variable)

    h_ll.SetTitle('leplep')
    h_lh.SetTitle('lephad')
    h_hh.SetTitle('hadhad')

    if norm_unit:
        h_ll.Scale(1. / h_ll.Integral())
        h_lh.Scale(1. / h_lh.Integral())
        h_hh.Scale(1. / h_hh.Integral())

    h_ll.SetLineColor(ROOT.kBlue)
    h_lh.SetLineColor(ROOT.kRed)
    h_hh.SetLineColor(ROOT.kViolet)

    plot = Plot(
        uuid.uuid4().hex,
        variable)
    plot.titleDecorator = AtlasTitleDecorator('Simulation')

    plot.addHistogram(h_ll)
    plot.addHistogram(h_lh)
    plot.addHistogram(h_hh)
    plot.drawStatError = True
    plot.titles.append('{}: {}'.format(title, process[0].title))

    plot.draw()
    plot.saveAs('plots/plot_compare_{}_{}_{}.pdf'.format(process_name, variable.name, title.replace(',', '').replace(' ', '_')))

def make_channel_comparison_plot_2D(
    processor,
    selections,
    variable_x,
    variable_y,
    process_name='Ztt',
    title='Boost SR'):

    print 'BOOM: make plot for', title, process_name, variable_x.name, variable_y.name
    sels_ll = filter_selections(selections, channels=('emu', 'mue'))
    sels_lh = filter_selections(selections, channels=('e1p', 'e3p', 'mu1p', 'mu3p'))
    sels_hh = filter_selections(selections, channels=('1p1p', '1p3p', '3p1p', '3p3p'))

    process = processor.get_physics_process(process_name)
    h_ll = processor.get_hist_physics_process_2D(process_name, sels_ll, variable_x, variable_y)
    h_lh = processor.get_hist_physics_process_2D(process_name, sels_lh, variable_x, variable_y)
    h_hh = processor.get_hist_physics_process_2D(process_name, sels_hh, variable_x, variable_y)

    h_ll.SetTitle('leplep')
    h_lh.SetTitle('lephad')
    h_hh.SetTitle('hadhad')

    h_ll.Scale(1. / h_ll.Integral())
    h_lh.Scale(1. / h_lh.Integral())
    h_hh.Scale(1. / h_hh.Integral())

    
    h_ll.SetMarkerColor(ROOT.kBlue)
    h_lh.SetMarkerColor(ROOT.kRed)
    h_hh.SetMarkerColor(ROOT.kGreen)

    h_ll.SetMarkerStyle(20)
    h_lh.SetMarkerStyle(20)
    h_hh.SetMarkerStyle(20)

    h_ll.SetMarkerSize(0.1)
    h_lh.SetMarkerSize(0.1)
    h_hh.SetMarkerSize(0.1)

    plot = Plot(
        uuid.uuid4().hex,
        variable_x,
        variable_y)
    plot.addHistogram(h_ll)
    plot.addHistogram(h_lh)
    plot.addHistogram(h_hh)
    plot.drawStatError = True

    plot.titles.append('{}: {}'.format(title, process[0].title))
    plot.draw()
    plot.saveAs('plots/plot_compare_{}_{}_{}_{}.pdf'.format(process_name, variable_x.name, variable_y.name, title.replace(',', '').replace(' ', '_')))

    
def make_zllvr_closure_plot(
    processor,
    selections_sr,
    selections_sr_truth,
    variable,
    title='Boost SR'):
    """
    """
    print 'BOOM: make plot for', title, variable.name

    if variable.name == 'tau_0_truth_pt_corr':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_0_pt'])
    elif variable.name == 'tau_1_truth_pt_corr':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_1_pt'])
    elif variable.name == 'ditau_dr_corr_truth':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['ditau_dr'])
    elif variable.name == 'ditau_deta_corr_truth':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['ditau_deta'])
    elif variable.name == 'tau_0_correction_truth_ratio':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_0_truth_ratio'])
    elif variable.name == 'tau_1_correction_truth_ratio':
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr,    VARIABLES['tau_1_truth_ratio'])
    else:
        h_sr    = processor.get_hist_physics_process('Ztt', selections_sr, variable)

    h_corr    = processor.get_hist_physics_process('Ztt', selections_sr_truth,  variable)

    h_sr.SetTitle('SR')
    h_corr.SetTitle('Corrected Truth')

    h_sr.Scale(1. / h_sr.Integral())
    h_corr.Scale(1. / h_corr.Integral())

    h_sr.SetLineColor(ROOT.kBlue)
    h_corr.SetLineColor(ROOT.kRed)

    plot = Plot(
        uuid.uuid4().hex,
        variable)
    plot.addHistogram(h_sr)
    plot.addHistogram(h_corr)
    plot.drawStatError = True

    plot.titles.append('{}'.format(title))
#     plot.logY = True
    plot.draw()
#     plot.saveAs('/afs/cern.ch/user/m/mojeda/afswork/HLep/boom/plots/plot_zllvr_proxy_comparison_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))
    plot.saveAs('plots/plot_zllvr_closure_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))

def make_zllvr_closure_plot_2D(
    processor,
    selections_sr,
    selections_sr_truth,
    variable_x,
    variable_y,
    title='Boost SR'):
    """
    """
    print 'BOOM: make plot for', title, variable_x.name, variable_y.name

    #x-variables
    if variable_x.name == 'tau_0_truth_pt_corr':
        new_x = VARIABLES['tau_0_pt']
    elif variable_x.name == 'tau_1_truth_pt_corr':
        new_x = VARIABLES['tau_1_pt']
    elif variable_x.name == 'ditau_dr_corr_truth':
        new_x = VARIABLES['ditau_dr']
    elif variable_x.name == 'ditau_deta_corr_truth':
        new_x = VARIABLES['ditau_deta']
    else:
        new_x = variable_x
        
    #y-variables
    if variable_y.name == 'tau_0_truth_pt_corr':
        new_y = VARIABLES['tau_0_pt']
    elif variable_y.name == 'tau_1_truth_pt_corr':
        new_y = VARIABLES['tau_1_pt']
    elif variable_y.name == 'ditau_dr_corr_truth':
        new_y = VARIABLES['ditau_dr']
    elif variable_y.name == 'ditau_deta_corr_truth':
        new_y = VARIABLES['ditau_deta']
    else:
        new_y = variable_y

    h_sr    = processor.get_hist_physics_process_2D('Ztt', selections_sr, new_x, new_y)
    h_corr    = processor.get_hist_physics_process_2D('Ztt', selections_sr_truth,  variable_x, variable_y)

    h_sr.SetTitle('SR')
    h_corr.SetTitle('Corrected Truth')

    h_sr.Scale(1. / h_sr.Integral())
    h_corr.Scale(1. / h_corr.Integral())

    #h_sr.SetMarkerColor(ROOT.kBlue)
    #h_corr.SetMarkerColor(ROOT.kRed)

    h_sr.SetLineColor(ROOT.kBlue)
    h_corr.SetLineColor(ROOT.kRed)
    
    h_sr.SetMarkerStyle(20)
    h_corr.SetMarkerStyle(20)

    h_sr.SetMarkerSize(0.1)
    h_corr.SetMarkerSize(0.1)

    h_sr.SetContour(10)
    h_corr.SetContour(10)
    
    plot = Plot(
        uuid.uuid4().hex,
        variable_x,
        variable_y)
    plot.addHistogram(h_sr,"CONT3")
    plot.addHistogram(h_corr,"CONT3")
    plot.drawStatError = True

    plot.titles.append('{}'.format(title))
#     plot.logY = True
    plot.draw()
#     plot.saveAs('/afs/cern.ch/user/m/mojeda/afswork/HLep/boom/plots/plot_zllvr_proxy_comparison_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))
    plot.saveAs('plots/plot_zllvr_closure2D_{}_{}_{}.pdf'.format(variable_x.name, variable_y.name, title.replace(',', '').replace(' ', '_').replace('.', '_').replace('/', '_').replace('(', '').replace(')', '')))


def _make_graph(_channels, _categories, shift=0):
    _graph = ROOT.TGraphErrors(len(_categories))
    print _categories
    for _c in _channels:
        print _c.name
    print
    for i_cat, _cat in enumerate(_categories):
        _chan = filter(lambda c: c.category == _cat, _channels)
        if len(_chan) == 0:
            print _cat
            raise ValueError
        print _cat, _chan[0].category
        _z = _chan[0].sample('ZllQCD_truth_0') 
        _z+= _chan[0].sample('ZllQCD_truth_1') 
        _z+= _chan[0].sample('ZllQCD_truth_2') 
        _z+= _chan[0].sample('ZllQCD_truth_3') 
        _z+= _chan[0].sample('ZllEWK_truth_0') 
        _z+= _chan[0].sample('ZllEWK_truth_1') 
        _z+= _chan[0].sample('ZllEWK_truth_2') 
        _z+= _chan[0].sample('ZllEWK_truth_3') 
        _data = _chan[0].sample('Data')
        for i in range(1, len(_chan)):
            _z+= _chan[i].sample('ZllQCD_truth_0') 
            _z+= _chan[i].sample('ZllQCD_truth_1') 
            _z+= _chan[i].sample('ZllQCD_truth_2') 
            _z+= _chan[i].sample('ZllQCD_truth_3') 
            _z+= _chan[i].sample('ZllEWK_truth_0') 
            _z+= _chan[i].sample('ZllEWK_truth_1') 
            _z+= _chan[i].sample('ZllEWK_truth_2') 
            _z+= _chan[i].sample('ZllEWK_truth_3') 
            _data += _chan[i].sample('Data')
        
        
        _z = _z.nominal
        _ratio = _data.nominal.Clone()
        _ratio.Divide(_z)
        print _ratio.GetNbinsX()
        _graph.SetPoint(i_cat, i_cat + shift, _ratio.GetBinContent(1))
        _graph.SetPointError(i_cat, 0, _ratio.GetBinError(1))
    return _graph
                              

def make_nf_plot(
    wsi,
    title=None,
    print_lumi=True,
    channel=None):
    
    _categories = []
    for _c in wsi.channels:
        if _c.category not in _categories:
            _categories += [_c.category]
    categories = sorted(_categories)

    uncorrected_crs_ll = filter(lambda c: c.finalstate == 'll' and c.cr  and 'corr' not in c.region, wsi.channels)
    corrected_crs_ll   = filter(lambda c: c.finalstate == 'll' and c.cr  and 'corr' in c.region, wsi.channels)
    uncorrected_crs_lh = filter(lambda c: c.finalstate == 'lh' and c.cr  and 'corr' not in c.region, wsi.channels)
    corrected_crs_lh   = filter(lambda c: c.finalstate == 'lh' and c.cr  and 'corr' in c.region, wsi.channels)
    uncorrected_crs_hh = filter(lambda c: c.finalstate == 'hh' and c.cr  and 'corr' not in c.region, wsi.channels)
    corrected_crs_hh   = filter(lambda c: c.finalstate == 'hh' and c.cr  and 'corr' in c.region, wsi.channels)


    if channel != None:
        uncorrected_crs_ll = filter(lambda c: channel in c.region, uncorrected_crs_ll)
        corrected_crs_ll   = filter(lambda c: channel in c.region, corrected_crs_ll  )
        uncorrected_crs_lh = filter(lambda c: channel in c.region, uncorrected_crs_lh)
        corrected_crs_lh   = filter(lambda c: channel in c.region, corrected_crs_lh  )
        uncorrected_crs_hh = filter(lambda c: channel in c.region, uncorrected_crs_hh)
        corrected_crs_hh   = filter(lambda c: channel in c.region, corrected_crs_hh  )  


    graph_uncorrected_crs_ll      = _make_graph(uncorrected_crs_ll      , categories, shift=0.1)
    graph_corrected_crs_ll       = _make_graph(corrected_crs_ll        , categories, shift=0.2)
    graph_uncorrected_crs_lh      = _make_graph(uncorrected_crs_lh      , categories, shift=0.4)
    graph_corrected_crs_lh       = _make_graph(corrected_crs_lh        , categories, shift=0.5)
    graph_uncorrected_crs_hh      = _make_graph(uncorrected_crs_hh      , categories, shift=0.7)
    graph_corrected_crs_hh       = _make_graph(corrected_crs_hh        , categories, shift=0.8)

    graph_uncorrected_crs_ll.SetMarkerStyle(24)
    graph_uncorrected_crs_lh.SetMarkerStyle(25)
    graph_uncorrected_crs_hh.SetMarkerStyle(26)

    graph_corrected_crs_ll.SetMarkerStyle(20)
    graph_corrected_crs_lh.SetMarkerStyle(21)
    graph_corrected_crs_hh.SetMarkerStyle(22)

    graph_uncorrected_crs_ll.SetLineColor(1)
    graph_corrected_crs_ll.SetLineColor(1)
    graph_uncorrected_crs_ll.SetMarkerColor(1)
    graph_corrected_crs_ll.SetMarkerColor(1)

    graph_uncorrected_crs_lh.SetLineColor(2)
    graph_corrected_crs_lh.SetLineColor(2)
    graph_uncorrected_crs_lh.SetMarkerColor(2)
    graph_corrected_crs_lh.SetMarkerColor(2)

    graph_uncorrected_crs_hh.SetLineColor(4)
    graph_corrected_crs_hh.SetLineColor(4)
    graph_uncorrected_crs_hh.SetMarkerColor(4)
    graph_corrected_crs_hh.SetMarkerColor(4)



    c = ROOT.TCanvas()
    c.SetTopMargin(0.15)
    c.SetGridx()
    c.SetGridy()

    h_template = ROOT.TH1F('h_temp', '', len(categories), 0, len(categories))
    for i_cat, _cat in enumerate(categories):
        h_template.GetXaxis().SetBinLabel(i_cat + 1, _cat)
        h_template.GetXaxis().SetTitle('Category')
        h_template.GetYaxis().SetTitle('Data / MC')
    # h_template.GetYaxis().SetRangeUser(0.90 * min(vals), 1.10 * max(vals))
#    h_template.GetYaxis().SetRangeUser(0., 2.)
    h_template.GetYaxis().SetRangeUser(0.8, 1.4)
    h_template.Draw()
    graph_uncorrected_crs_ll      .Draw('sameP')    
    graph_corrected_crs_ll        .Draw('sameP')
    graph_uncorrected_crs_lh      .Draw('sameP')    
    graph_corrected_crs_lh        .Draw('sameP')
    graph_uncorrected_crs_hh      .Draw('sameP')    
    graph_corrected_crs_hh        .Draw('sameP')

    leg = ROOT.TLegend(c.GetLeftMargin(), 1 - c.GetTopMargin(), 1 - c.GetRightMargin(), 1)
    leg.AddEntry(graph_uncorrected_crs_ll, 'leplep: Z#rightarrowll')
    leg.AddEntry(graph_uncorrected_crs_lh, 'lephad: Z#rightarrowll')
    leg.AddEntry(graph_uncorrected_crs_hh, 'hadhad: Z#rightarrowll')
    leg.AddEntry(graph_corrected_crs_ll, 'leplep: Corrected Z#rightarrowll')
    leg.AddEntry(graph_corrected_crs_lh, 'lephad: Corrected Z#rightarrowll')
    leg.AddEntry(graph_corrected_crs_hh, 'hadhad: Corrected Z#rightarrowll')
    leg.SetNColumns(3)
    leg.SetFillStyle(0)
    leg.SetFillColor(0)
    leg.SetBorderSize(0)
    leg.Draw()

    if print_lumi:
        _lumi = uncorrected_crs_ll[0].lumi.Integral()
        label = ROOT.TLatex(
            c.GetLeftMargin() + 0.02, 1 - c.GetTopMargin() - 0.07, 
            'L = {0:1.0f} fb^{{-1}}'.format(_lumi / 1000.))
        label.SetNDC()
        label.Draw()

    c.RedrawAxis()
    _file_name = 'plot_norm_embedding.pdf'
    if channel != None:
        _file_name = 'plot_norm_embedding_{}.pdf'.format(channel)

    c.SaveAs('plots/{}'.format(_file_name))
