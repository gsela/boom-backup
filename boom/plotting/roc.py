"""
"""
# python imports
import ROOT
import uuid
# HAPPy imports
from happy.plot import Plot
# local imports
from ..selection_utils import filter_selections
from ..utils import plot_title_extension, hist_sum, lumi_calc
from ..extern.tabulate import tabulate

def roc_graph(score_sig, score_bkg, verbose=False):
    """
    """
    if score_sig.GetNbinsX() != score_bkg.GetNbinsX():
        raise ValueError
    gr = ROOT.TGraph(score_sig.GetNbinsX())
    gr.GetXaxis().SetTitle('#epsilon_{S}')
    gr.GetYaxis().SetTitle('1 - #epsilon_{b}')
    lines = []
    for ibin in xrange(score_sig.GetNbinsX() + 2):
        eff_s = score_sig.Integral(ibin + 1, score_sig.GetNbinsX() + 2)
        eff_s /= score_sig.Integral(0, score_sig.GetNbinsX() + 2)
        eff_b = score_bkg.Integral(ibin + 1, score_bkg.GetNbinsX() + 2) 
        eff_b /= score_bkg.Integral(0, score_bkg.GetNbinsX() + 2)
        gr.SetPoint(ibin + 1, eff_s, 1 - eff_b)
        line = [ibin, score_sig.GetBinLowEdge(ibin + 1), eff_s, 1 - eff_b]
        lines.append(line)

    if verbose:
        print tabulate(lines, headers=['bin', 'score', 'Signal Eff.', '1 - Bkg. Eff'])

    return gr


def make_vbf_roc_plot(
    processor,
    selections,
    variable,
    title=None,
    **kwargs):
    """
    """
    _sels = filter_selections(selections, **kwargs)

    # plot title, file extension
    if title is None:
        _title, _ext = plot_title_extension(_sels, **kwargs)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')


    _sels_tight = filter_selections(_sels, categories=('vbf_tight', 'vbf_lowdr'))

    h_VBF = processor.get_hist_physics_process('VBFH', _sels, variable)
    h_ggH = processor.get_hist_physics_process('ggH', _sels, variable)

    h_VBF_tight = processor.get_hist_physics_process('VBFH', _sels_tight, variable)
    h_ggH_tight = processor.get_hist_physics_process('ggH', _sels_tight, variable)

    
    bkgs = []
    bkgs_tight = []
    for name in processor.merged_physics_process_names[::-1]:
        p = processor.get_physics_process(name)[0]
        if p.isSignal:
            continue
        if p.name == 'Data':
            continue
        h = processor.get_hist_physics_process(name, _sels, variable)
        bkgs.append(h)
        h_tight = processor.get_hist_physics_process(name, _sels_tight, variable)
        bkgs_tight.append(h_tight)
    h_bkg = hist_sum(bkgs)
    h_bkg_tight = hist_sum(bkgs_tight)

    roc_ggh = roc_graph(h_VBF, h_ggH)
    roc_ggh.SetLineWidth(2)
    roc_ggh.SetLineColor(ROOT.kBlue)

    print _title
    roc_bkg = roc_graph(h_VBF, h_bkg, verbose=True)
    roc_bkg.SetLineWidth(2)
    roc_bkg.SetLineColor(ROOT.kRed)


    vbf_cba = h_VBF_tight.Integral(0, h_VBF_tight.GetNbinsX() + 1)
    vbf_cba /= h_VBF.Integral(0, h_VBF.GetNbinsX() + 1)

    ggh_cba = h_ggH_tight.Integral(0, h_ggH_tight.GetNbinsX() + 1)
    ggh_cba /= h_ggH.Integral(0, h_ggH.GetNbinsX() + 1)

    bkg_cba = h_bkg_tight.Integral(0, h_bkg_tight.GetNbinsX() + 1)
    bkg_cba /= h_bkg.Integral(0, h_bkg.GetNbinsX() + 1)


    wp_1 = ROOT.TGraph(1)
    wp_1.SetPoint(0, vbf_cba, 1 - bkg_cba)
    wp_1.SetMarkerColor(ROOT.kRed)
    wp_1.SetMarkerSize(2)
    
    wp_2 = ROOT.TGraph(1)
    wp_2.SetPoint(0, vbf_cba, 1 - ggh_cba)
    wp_2.SetMarkerColor(ROOT.kBlue)
    wp_2.SetMarkerSize(2)



    plot = ROOT.TCanvas(uuid.uuid4().hex, "", 600, 600)
    plot.SetTopMargin(0.1)
    h_template = ROOT.TH1F(uuid.uuid4().hex, "", 1, 0, 1.1)
    h_template.GetXaxis().SetTitle('Signal Efficiency')
    h_template.GetYaxis().SetTitle('1 - Bkg. Efficiency')
    h_template.GetYaxis().SetRangeUser(0, 1.1)
    h_template.Draw('HIST')
    roc_ggh.Draw('sameL')
    roc_bkg.Draw('sameL')
    wp_1.Draw('sameP')
    wp_2.Draw('sameP')

    # legend
    leg_2 = ROOT.TLegend(
        plot.GetLeftMargin() + 0.05,
        1 - plot.GetTopMargin() - 0.5,
        0.5,
        1 - plot.GetTopMargin() - 0.7)
    leg_2.AddEntry(roc_bkg, 'VBF versus Bkg.', 'l')
    leg_2.AddEntry(roc_ggh, 'VBF versus ggH', 'l')
    leg_2.AddEntry(wp_1, 'CBA: VBF versus Bkg.', 'p')
    leg_2.AddEntry(wp_2, 'CBA: VBF versus ggH', 'p')
    leg_2.Draw('same')

    # Decoration
    label = ROOT.TLatex(
        0.0, 1.12, 
        _title)
    label.SetTextSize(0.8 * label.GetTextSize())
    label.Draw()

    # ATLAS label
    atlas_label = ROOT.TLatex(
        0.1,
        1 - plot.GetTopMargin() - 0.3,
        '#it{#bf{ATLAS}} Internal')
    atlas_label.SetTextFont(42)
    atlas_label.Draw()

    # lumi label
    luminosity = lumi_calc(_sels)
    lumi_label = ROOT.TLatex(
        0.1,
        1 - plot.GetTopMargin() - 0.37,
        'L = %.3g fb^{-1}' % (luminosity / 1000.))
    lumi_label.Draw()



    plot.Update()
    plot.RedrawAxis()
    plot.SaveAs('./plots/plot_roc_vbf_tagger_{}.pdf'.format(_ext))
    plot.SaveAs('./plots/plot_roc_vbf_tagger_{}.root'.format(_ext))

