"""
"""
import ROOT
import uuid
# HAPPy imports
from happy.plot import Plot, TextElement
from happy.style import Style
from happy.plotDecorator import AtlasTitleDecorator

from ..selection_utils import filter_selections
from ..utils import lumi_calc, plot_title_extension

def basic_plot(
    selections,
    hist,
    variable, 
    print_lumi=True,
    **kwargs):
    """
    """
    _sels = filter_selections(selections, **kwargs)
    _title, _ext = plot_title_extension(_sels, **kwargs)

    print 'BOOM: make plot for', _title, variable.name

    plot = Plot(
        uuid.uuid4().hex,
        variable
        )

    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 3
    plot.addHistogram(hist, 'PE')

    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )
    plot.draw()
    plot.saveAs('plots/basic_plot_{}_{}.pdf'.format(variable.name, _ext))

def make_fakefactor_plot(
    hist,
    variable,
    title,
    ):

    plot = Plot(
        uuid.uuid4().hex,
        variable
        )

    hist.GetYaxis().SetTitle("FF")
    hist.SetName('')
    hist.SetTitle('')

    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 3
    plot.addHistogram(hist, 'E0')
     
    label = 'FF'
    label += ' QCDCR' if 'qcd' in title else ' WCR'
    label += ' Presel' if 'Presel' in title else ' Boosted' if 'Boosted' in title else ' VBF' if 'VBF' in title else ' VH'
    label += ' 1prong' if '1p' in title else ' 3prong' 
    plot.titles.append(label)
    plot.draw()
    plot.saveAs('plots/{}.pdf'.format(title))

    


