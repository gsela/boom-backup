"""
"""
from ROOT import *
import uuid
import os
# HAPPy imports
from happy.plot import Plot, TextElement
from happy.doublePlot import DoublePlot, PlotMatrix
from happy.dataMcPlot import DataMcPlot
from happy.dataMcRatioPlot import DataMcRatioPlot
from happy.variable import Binning, Variable, var_Ratio, var_Entries
from happy.style import Style
from happy.plotDecorator import AtlasTitleDecorator

from ..selection_utils import filter_selections
from ..utils import blinded_hist, yield_table, lumi_calc, plot_title_extension, hist_max
from ..variables import VARIABLES
from make_comp_plots import *
from make_stack_plots import *
from root_utils import grab_hist
import gconfig

#Based on the compare plot from the zll.py file
def g_plot_mattias(
    processor,
    selections,
    variable,
    category,
    region,
    process_name='Data',
    title='LFV',
    plotdir='~/boom/plots',
    save_to_file='no_name_entered'):


    print 'BOOM: make plot for', title, process_name, variable.name
    sels_eh = filter_selections(selections, channels=('e1p', 'e3p'), regions=region, categories=category)
    sels_muh = filter_selections(selections, channels=('mu1p', 'mu3p'), regions=region, categories=category)

    ###changed process_name to take the data:
    ##????why do we need the first line here????
    process = processor.get_physics_process(process_name)
    h_eh = processor.get_hist_physics_process(process_name, sels_eh, variable)
    h_muh = processor.get_hist_physics_process(process_name, sels_muh, variable)
        
    
    h_eh.SetTitle('e_tau')
    h_muh.SetTitle('mu_tau')


    h_eh.SetLineColor(kBlue)
    h_muh.SetLineColor(kRed)

    xlabel=str(variable)
    xlabel.replace(',', '').replace(' ', '_')

    print "Integrals: eh {} and muh {}".format(h_eh.Integral(),h_muh.Integral())

    plt=Comp_plot(h_eh,h_muh,"Comparing Channels",plotdir,title,xlabel,True)
    plt.channels=["e#tau","#mu#tau"]
    plt.save_canvas()

    if gconfig.SaveHist==1:
        f=TFile.Open("///afs/cern.ch/user/g/gsela/boom/plots/"+str(save_to_file),"update")
        h_eltau=grab_hist(h_eh)
        h_eltau.SetName("eltau_"+str(title))
        h_eltau.Write()

        h_mutau=grab_hist(h_muh)
        h_mutau.SetName("mutau_"+str(title))
        h_mutau.Write()
    

def g_plot_compare_EB(
    processor,
    selections,
    variable,
    category,
    region,
    process_name='Data',
    title='LFV',
    norm_unit=False):

    if title is None:
        _title, _ext = plot_title_extension(selections)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')
    #change this to take etau mutau samples:
    print 'BOOM: make plot for', title, process_name, variable.name
    sels_eh = filter_selections(selections, channels=('e1p', 'e3p'))
    sels_muh = filter_selections(selections, channels=('mu1p', 'mu3p'))

    ###changed process_name to take the data:
    ##????why do we need the first line here????
    process = processor.get_physics_process(process_name)
    h_eh = processor.get_hist_physics_process(process_name, sels_eh, variable)
    h_muh = processor.get_hist_physics_process(process_name, sels_muh, variable)
    
    ##is blinding needed? hD is the data histogram from the mc plotting code

    # figure out if blinding is needed
    # if not force_unblind:
    #     if 'mmc' in variable.name:
    #         hD_blinded = blinded_hist(hD)
    #         plot.setDataHistogram(hD_blinded)
    #     elif 'visible_mass' in variable.name:
    #         hD_blinded = blinded_hist(hD, var_name='visible')
    #         plot.setDataHistogram(hD_blinded)
    #     elif 'ditau_coll_approx_m' in variable.name:
    #         hD_blinded = blinded_hist(hD, var_name='collinear')
    #         plot.setDataHistogram(hD_blinded)
    #     elif variable.name in [
    #             'vbf_tagger', 
    #             'tth_tt_bdt', 
    #             'tth_Z_bdt',
    #             'vh_tagger',
    #             'hh_vh_tagger', 
    #             'lh_vh_tagger', 
    #             'll_vh_tagger',
    #             'pt_total',
    #             'pt_total_low']:

    #         if 'pt_total' in variable.name:
    #             rtol_blind_range = True
    #         else :
    #             rtol_blind_range = False    

    #         hD_blinded = blinded_hist(hD, var_name=variable.name, signal=hH_no_scaling, rtol_blind_range = rtol_blind_range)
    #         plot.setDataHistogram(hD_blinded)
    #     else:
    #         plot.setDataHistogram(hD)
    # else:
    #     plot.setDataHistogram(hD)

        
    
    h_eh.SetTitle('e_tau')
    h_muh.SetTitle('mu_tau')


    h_eh.SetLineColor(kBlue)
    h_muh.SetLineColor(kRed)
   
    #what is the decorator? should change to 'data'?
    c1=TCanvas("c1","Plotting the plot",800,800)
    
    h_muh.GetXaxis().SetTitle("x-axis")
    h_muh.GetYaxis().SetTitle("y-axis")
    
    plot=TRatioPlot(h_muh,h_eh)
    #add a difference comparison at the bottom
    
    

    # leg=TLegend(.15,.55,.4,.85)
    # leg.AddEntry(self.h1,self.ch_labels[0],"ep")
    # leg.AddEntry(self.h2,self.ch_labels[1],"ep")



    
    
    c1.Update()
    c1.Draw()
    plot.Draw()

    plot.GetLowerRefGraph().SetMinimum(0.5)
    plot.GetLowerRefGraph().SetMaximum(1.5)
    #c1.Print("plots/recoEff/"+"visibleMass"+"_NoRecoCorr"+"_"+str(category)+"_"+str(region)+".png")
    c1.SaveAs("plots/recoEff/123.png")


def g_plot_mattias_mc(
    processor,
    selections,
    variable,
    category,
    region,
    samps=['Diboson','Top'],
    title='LFV',
    plotdir='~/boom/plots',
    save_to_file='no_name_entered',
    Sig=1 ):

    flag=False
    hbgs=[]
    samps2=[]
    sm_higgs_samps=['ggHtt', 'ggHWW', 'VBFHtt', 'VBFHWW','WHtt', 'ZHtt', 'ttHtt']
    
    print 'BOOM: make plot for', title, variable.name
    sels_eh = filter_selections(selections, channels=('e1p', 'e3p'), regions=region, categories=category)
    sels_muh = filter_selections(selections, channels=('mu1p', 'mu3p'), regions=region, categories=category)


    process = processor.get_physics_process(samps[0])
    

    if Sig==2:
        h_eh_total = processor.get_hist_physics_process(samps[0], sels_eh, variable)
        h_muh_0 = processor.get_hist_physics_process(samps[0], sels_muh, variable)
        hbgs.append(grab_hist(h_muh_0))
        samps2.append(samps[0])
        sm_h_hist=grab_hist(h_muh_0,reset=True)
        
        if len(samps)>1:
            for process_name in samps[1::]:

                if process_name in sm_higgs_samps:
                    process = processor.get_physics_process(process_name)
                    h_eh_one_p = processor.get_hist_physics_process(process_name, sels_eh, variable)
                    h_muh_one_p = processor.get_hist_physics_process(process_name, sels_muh, variable)

                    sm_h_hist.Add(h_muh_one_p)
                    h_eh_total.Add(h_eh_one_p)
                    flag=True
                else:
                    #???why do we need the first line here????
                    process = processor.get_physics_process(process_name)
                    h_eh_one_p = processor.get_hist_physics_process(process_name, sels_eh, variable)
                    h_muh_one_p = processor.get_hist_physics_process(process_name, sels_muh, variable)
                    
                    hbgs.append(grab_hist(h_muh_one_p))
                    h_eh_total.Add(h_eh_one_p)
                    samps2.append(process_name)
                    # h_eh.SetTitle('e_tau')
                    # h_muh.SetTitle('mu_tau')


                    # h_eh.SetLineColor(kBlue)
                    # h_muh.SetLineColor(kRed)

        hd=grab_hist(h_eh_total)
    
    else:
        h_muh_total = processor.get_hist_physics_process(samps[0], sels_muh, variable)
        h_eh_0 = processor.get_hist_physics_process(samps[0], sels_eh, variable)
        hbgs.append(grab_hist(h_eh_0))
        samps2.append(samps[0])
        sm_h_hist=grab_hist(h_eh_0,reset=True)
        
        if len(samps)>1:
            for process_name in samps[1::]:

                if process_name in sm_higgs_samps:
                    process = processor.get_physics_process(process_name)
                    h_eh_one_p = processor.get_hist_physics_process(process_name, sels_eh, variable)
                    h_muh_one_p = processor.get_hist_physics_process(process_name, sels_muh, variable)

                    sm_h_hist.Add(h_eh_one_p)
                    h_muh_total.Add(h_muh_one_p)
                    flag=True
                else:
                    #???why do we need the first line here????
                    process = processor.get_physics_process(process_name)
                    h_eh_one_p = processor.get_hist_physics_process(process_name, sels_eh, variable)
                    h_muh_one_p = processor.get_hist_physics_process(process_name, sels_muh, variable)
                    
                    hbgs.append(grab_hist(h_eh_one_p))
                    h_muh_total.Add(h_muh_one_p)
                    samps2.append(process_name)
                    # h_eh.SetTitle('e_tau')
                    # h_muh.SetTitle('mu_tau')


                    # h_eh.SetLineColor(kBlue)
                    # h_muh.SetLineColor(kRed)

        hd=grab_hist(h_muh_total)



    if flag==True:
        hbgs.append(grab_hist(sm_h_hist))
        samps2.append('SM_Higgs')


    
    hs=grab_hist(hd,reset=True)
    hc=grab_hist(hd,reset=True)

    
    xlabel=str(variable.name)
    xlabel.replace(',', '').replace(' ', '_')

    plt=stack_plot('channel',hd,hs,hc,hbgs,samps2,'',plotdir,title,xlabel)
    if Sig==2:
        plt.channels=['e#tau','#mu#tau']
    else:
        plt.channels=['#mu#tau','e#tau']

    # plt.lumi=lumidct[year]
    # plt.minstat=10
    plt.save_canvas()

    if gconfig.SaveHist==1:

        if Sig==2:
            f=TFile.Open("///afs/cern.ch/user/g/gsela/boom/plots/"+str(save_to_file),"update")
            h_eltau=grab_hist(hd)
            h_eltau.SetName("eltau_"+str(title))
            h_eltau.Write()

            h_mutau=grab_hist(hd,reset=True)
            h_mutau.SetName("mutau_"+str(title))
            for h in hbgs:
                h_mutau.Add(h)
            h_mutau.Write()

        else:
            f=TFile.Open("///afs/cern.ch/user/g/gsela/boom/plots/"+str(save_to_file),"update")
            h_eltau=grab_hist(hd,reset=True)
            h_eltau.SetName("eltau_"+str(title))
            for h in hbgs:
                h_eltau.Add(h)
            h_eltau.Write()

            h_mutau=grab_hist(hd)
            h_mutau.SetName("mutau_"+str(title))
            h_mutau.Write()
