import os
import root_utils as ru
from ROOT import *
from math import log10
# gROOT.SetBatch(0)

class stack_plot():#,hes,hms,decoside,lumi,title,xtitle,ytitle,minstat,sigscale,rebin,maxx,forPub,chans):
    def __init__(self,ch,hd,hs,hc,hbs,stacklist,plot_title,save_dir='',save_title='',xtitle=''):
        self.ch=ch
        self.hd=hd
        self.hbs=hbs
        self.stacklist=stacklist
        try:
            if 'TH1' in hs.Class_Name():
                self.showsig=True
                self.hs=hs
                self.hc=hc
        except:
            self.showsig=False
            pass
        # self.prefit_bkg_nfs() #already done in readQF
        self.hbsum=self.get_sum_of_bkgs()
        self.title=plot_title
        self.save_dir=save_dir
        self.save_title=plot_title if not save_title else save_title
        self.xtitle=xtitle
        self.ytitle="events / %s GeV"%(int(round(hd.GetBinWidth(1)))/1000) if hd.GetBinWidth(1)>=1000 else 'events / bin'
        self.xlog=False
        self.ylog=False
        self.rebin=''
        self.minstat=0.1
        self.max_offset=1
        self.legside='right'
        self.lumi='???'
        self.saveas='png'
        self.ch_label=ch.replace('m','#mu')
        self.ratio_min=0.6
        self.ratio_max=1.4
        self.userxmin=''
        self.userxmax=''
        self.userymin=''
        self.userymax=''
        self.syshlist=[] #[hup,hdown]
        self.syslabel=""
        self.channels=['Data','Bkg']

    def prefit_bkg_nfs(self):
        return
        # if self.ch=='em':
        #     self.hbs[self.stacklist.index('Top')].Scale(1.08)
        #     self.hbs[self.stacklist.index('Ztautau')].Scale(1.05)
        # else:
        #     self.hbs[self.stacklist.index('Top')].Scale(1.04)
        #     self.hbs[self.stacklist.index('Ztautau')].Scale(1.01)
        
    def get_canvas_objects(self):
        # if self.rebinauto>0:
        #     rebin=1.
        #     nbins0=self.hd.GetNbinsX()
        #     nbins=nbins0+0
        #     for rebin in [1.,2.,4.,5.,]:
        #         if 
        #     while(nbins>self.rebinauto):
        #         rebin+=1
        #         nbins=nbins0/rebin
        #     print rebin
        #     self.rebin=int(rebin)
        xmin,xmax,ymin,ymax=self.set_ranges()
        hr=ru.get_ratio_hist(self.hd,self.hbsum)
        self.set_up_axes()
        self.set_ratio_axes(hr)
        self.set_hist_props(hr)
        self.hb,self.contlist,self.colors=self.get_stack_hist()
        if self.syshlist:
            self.get_var_bands()
        leg,deco1,deco2=self.get_up_decos()
        hup,hlow,line=self.get_ratio_decos(hr,xmin,xmax)
        self.objects=[self.hd,self.hb,leg,deco1,deco2,hr,line,hup,hlow]
        if self.showsig:
            self.objects+=[self.hs,self.hc]
        return self.set_canvas()

    def get_var_bands(self):
        gup=TGraphAsymmErrors(self.hbsum)
        gdown=TGraphAsymmErrors(self.hbsum)
        gup.SetFillColor(kOrange-5)
        gup.SetFillStyle(3144)
        gdown.SetFillColor(kOrange-5)
        gdown.SetFillStyle(3144)
        hones=ru.grab_hist(self.hbsum)
        hones.Reset()
        for i in range(0,hones.GetNbinsX()):
            hones.SetBinContent(i+1,1)
        grup=TGraphAsymmErrors(hones)
        grdown=TGraphAsymmErrors(hones)
        grup.SetFillColor(kOrange-5)
        # grup.SetFillColor(kGreen+3)
        grup.SetLineWidth(0)
        grup.SetFillStyle(3144)
        grdown.SetFillColor(kOrange-5)
        grdown.SetFillStyle(3144)
        grdown.SetLineWidth(0)
        [hup,hdown]=self.syshlist
        for i in range(0,self.hbsum.GetNbinsX()):
            nom=self.hbsum.GetBinContent(i+1)
            nomd=self.hd.GetBinContent(i+1)
            if nom<=0 or nomd<=0:
                continue
            up=hup.GetBinContent(i+1)
            down=hdown.GetBinContent(i+1)
            devup=abs(up-nom)
            devdown=abs(nom-down)
            # print i+1,nom,nomd,up,down,devup,devdown,devup/nomd,devdown/nomd
            gup.SetPointEYhigh(i,devup)
            gup.SetPointEYlow(i,0)
            gdown.SetPointEYhigh(i,0)
            gdown.SetPointEYlow(i,devdown)
            grup.SetPointEYhigh(i,devup/nomd)
            grup.SetPointEYlow(i,0)
            grdown.SetPointEYhigh(i,0)
            grdown.SetPointEYlow(i,devdown/nomd)
        self.gup=gup
        self.gdown=gdown
        self.grup=grup
        self.grdown=grdown
        
    def show_canvas(self,c=None):
        if c==None:
            c=self.get_canvas_objects()
        ru.showcanvas(c)
        c.Close()
        
    def save_canvas(self,c=None):
        if c==None:
            c=self.get_canvas_objects()
        if not os.path.isdir(self.save_dir):
            print "Creating new directory:",self.save_dir
            os.makedirs(self.save_dir)
        c.SaveAs(self.save_dir+"/%s.%s"%(self.save_title,self.saveas))
        c.Close()
        
    def get_sum_of_bkgs(self):
        hbsum=ru.grab_hist(self.hbs[0])
        for h in self.hbs[1:]:
            hbsum.Add(h)
        return hbsum
        
    def get_stack_hist(self):
        colors=[kMagenta-8,kYellow-3,kAzure-3,46,kGreen-6,kGray+1,kAzure-9,kOrange+1]
        contlist=[h.Integral() for h in self.hbs]
        if self.ylog:
            contlist,self.hbs,self.stacklist,colors[0:len(self.hbs)]=(list(t) for t in zip(*sorted(zip(contlist,self.hbs,self.stacklist,colors))))
        else:
            contlist,self.hbs,self.stacklist,colors[0:len(self.hbs)]=(list(t)[::-1] for t in zip(*sorted(zip(contlist,self.hbs,self.stacklist,colors))))
        hb=THStack("hb","")
        for i in range(0,len(self.stacklist)):
            self.hbs[i].SetFillColor(colors[i])
            hb.Add(self.hbs[i])
        return hb,contlist,colors
    
    def set_ranges(self,scale=True,rebin=True):
        if rebin and self.rebin:
            ru.rebin_hists([self.hd,self.hbsum]+self.hbs+self.syshlist,self.rebin)
            if self.showsig:
                ru.rebin_hists([self.hs,self.hc],self.rebin)
        xmin,xmax=ru.get1DXrange([self.hd,self.hbsum],self.minstat)
        ymin,ymax=ru.get1DYrange([self.hd,self.hbsum])
        ymin=max(0,ymin)
        # ymax_order=10**int(log10(ymax))
        # ymin_round=int(ymin)/ymax_order*ymax_order
        ymin_round=0
        if self.showsig:
            hists=[self.hd,self.hbsum,self.hs,self.hc]+self.hbs
        else:
            hists=[self.hd,self.hbsum]+self.hbs
        ymax=ymin_round+self.max_offset*(ymax-ymin_round)*3/2
        ymin=ymin_round
        if self.ylog:
            ymin=max(0.8,ymin_round)
            ymax=ymax**(self.max_offset*1.5)
        if self.userxmin:
            xmin=self.userxmin
        if self.userxmax:
            xmax=self.userxmax
        if self.userymin:
            ymin=self.userymin
        if self.userymax:
            ymax=self.userymax
        if scale:
            # print xmin,xmax,ymin,ymax
            for h in hists:
                h.GetXaxis().SetRangeUser(xmin,xmax)            
                h.SetMaximum(ymax)
                h.SetMinimum(ymin)
        self.xmin=xmin
        self.xmax=xmax
        return xmin,xmax,ymin,ymax
        
    def set_up_axes(self):
        xax=self.hd.GetXaxis()
        yax=self.hd.GetYaxis()
        self.hd.SetTitle(self.title)
        xax.SetTitle('')
        xax.SetLabelSize(0.)
        yax.SetTitle(self.ytitle)
        # yax.CenterTitle()
        yax.SetLabelSize(20)
        yax.SetLabelFont(43)
        yax.SetTitleSize(20)
        yax.SetTitleFont(43)
        yax.SetTitleOffset(1.5)

    def set_ratio_axes(self,hr):
        hr.SetMinimum(self.ratio_min)
        hr.SetMaximum(self.ratio_max)
        xax=hr.GetXaxis()
        yax=hr.GetYaxis()
        xax.SetTitle(self.xtitle)
        # xax.CenterTitle()
        xax.SetLabelSize(20)
        xax.SetLabelFont(43)
        xax.SetTitleSize(25)
        xax.SetTitleFont(43)
        xax.SetTitleOffset(3.6)
        yax.SetTitle("ratio %s/%s"%(self.channels[0].split('_')[0],self.channels[1]))
        yax.CenterTitle()
        yax.SetNdivisions(6)
        yax.SetLabelSize(20)
        yax.SetLabelFont(43)
        yax.SetTitleSize(20)
        yax.SetTitleFont(43)
        yax.SetTitleOffset(1.5)
        hr.SetTitle('')
        
    def set_hist_props(self,hr):
        self.hd.SetLineWidth(2)
        self.hd.SetLineColor(kBlack)
        self.hd.SetMarkerStyle(20)
        self.hd.SetMarkerSize(0.9)
        self.hd.SetMarkerColor(kBlack)
        hr.SetMarkerStyle(20)
        hr.SetMarkerColor(1)
        hr.SetLineColor(kBlack)
        hr.SetLineWidth(2)
        if self.showsig:
            self.hs.SetLineColor(kBlack)
            self.hs.SetLineWidth(2)
            self.hs.SetLineStyle(9)
            self.hc.SetLineColor(kBlack)
            self.hc.SetLineWidth(2)
            self.hc.SetLineStyle(2)
    
    def get_up_decos(self):
        if self.legside=="right":
            # leg=TLegend(.55,.52,.85,.86)
            # deco1=TLegend(.15,.78,.55,.88)
            # deco2=TLegend(.15,.68,.55,.78)
            leg=TLegend(.65,.62,.95,.96)
            deco1=TLegend(.25,.88,.65,.98)
            deco2=TLegend(.25,.78,.65,.88)
            #from comp_plots:
            # leg=TLegend(.7,.65,.95,.95)
            # deco1=TLegend(.20,.83,.60,.93)
            # deco2=TLegend(.20,.73,.60,.83)
        else:
            leg=TLegend(.15,.52,.4,.86)
            deco1=TLegend(.5,.78,.85,.88)
            deco2=TLegend(.5,.68,.85,.78)
        leg.SetFillColor(kWhite)
        leg.SetBorderSize(0)
        leg.SetTextSize(0.035)
        leg.AddEntry(self.hd,"%s (%.2f)"%(self.channels[0],self.hd.Integral()),"ep")
        for i in range(len(self.stacklist)):
            # if self.ch=='em' and self.stacklist[i]=='Top':
            #     preNF='1.08x'
            # elif self.ch=='em' and self.stacklist[i]=='Ztautau':
            #     preNF='1.05x'
            # elif self.ch=='me' and self.stacklist[i]=='Top':
            #     preNF='1.04x'
            # elif self.ch=='me' and self.stacklist[i]=='Ztautau':
            #     preNF='1.01x'
            # else:
            #     preNF=''
            # #patch
            preNF=''
            leg.AddEntry(self.hbs[i],"%s %s%s (%.2f)"%(self.channels[1],preNF,self.stacklist[i],self.contlist[i]),"f")
        if self.showsig:
            if self.ch in ['em','el']:
                legsig="H #rightarrow #tau e (%.2f)"%self.hs.Integral()
                legcont="H #rightarrow #tau #mu (%.2f)"%self.hc.Integral()
            elif self.ch in ['me','mu']:
                legsig="H #rightarrow #tau #mu (%.2f)"%self.hs.Integral()
                legcont="H #rightarrow #tau e (%.2f)"%self.hc.Integral()
            else:
                legsig="Sig (%.2f)"%self.hs.Integral()
                legcont="Cont (%.2f)"%self.hc.Integral()
            leg.AddEntry(self.hs,legsig,"l")
            leg.AddEntry(self.hc,legcont,"l")
        if self.syshlist:
            leg.AddEntry(self.grup,self.syslabel,"f")
        deco1.SetFillColor(kWhite)
        deco1.SetHeader("#bf{#it{ATLAS}} Internal")
        deco1.SetTextAlign(12)
        deco1.SetBorderSize(0)
        deco1.SetTextSize(0.04)
        deco2.SetFillColor(kWhite)
        deco2.SetHeader("#intLdt = %s fb^{-1}, #sqrt{s} = 13 TeV"%self.lumi)
        deco2.SetTextAlign(12)
        deco2.SetBorderSize(0)
        deco2.SetTextSize(0.04)
        return leg,deco1,deco2

    def get_ratio_decos(self,hr,xmin,xmax):
        hup=hr.Clone()
        hup.SetDirectory(0)
        hup.SetMarkerStyle(22)
        hup.SetMarkerColor(kRed)
        hlow=hr.Clone()
        hlow.SetDirectory(0)
        hlow.SetMarkerStyle(23)
        hlow.SetMarkerColor(kRed)
        for b in range(1,hr.GetNbinsX()+1):
            if hr.GetBinContent(b)>self.ratio_max:
                hup.SetBinContent(b,self.ratio_max-0.03)
                # hup.SetBinError(b,0)
                hlow.SetBinContent(b,-10)
            elif hr.GetBinContent(b)<self.ratio_min:
                hlow.SetBinContent(b,self.ratio_min+0.02)
                # hlow.SetBinError(b,0)
                hup.SetBinContent(b,-10)
            else:
                hup.SetBinContent(b,-10)
                hlow.SetBinContent(b,-10)
        l=TLine(xmin,1,xmax,1)
        l.SetLineColor(2)
        l.SetLineWidth(2)
        l.SetLineStyle(2)
        return hup,hlow,l

    def set_canvas(self):
        gStyle.SetOptStat(0)
        # gROOT.SetBatch(1)
        #PrepareCanvas
        c=TCanvas("c","",800,800)
        up=TPad("up","",0,.3,1,1)
        up.SetBottomMargin(0.03)
        # up.SetLogy()
        up.SetTickx()
        up.SetTicky()
        if self.ylog:
            up.SetLogy()
        up.Draw()
        down=TPad("down","",0,.05,1,.3)
        down.SetTopMargin(0.04)
        down.SetBottomMargin(0.3)
        down.SetTickx()
        down.SetTicky()
        down.Draw()
        #DrawPlot
        up.cd()
        self.objects[0].Draw("P")
        self.objects[1].Draw("hist same")
        self.objects[0].Draw("P same")
        self.objects[2].Draw("same")
        self.objects[3].Draw("same")
        self.objects[4].Draw("same")
        if self.showsig:
            self.objects[9].Draw("hist same")
            self.objects[10].Draw("hist same")
        if self.syshlist:
            self.gup.Draw("2 same")
            self.gdown.Draw("2 same")
            self.objects[0].Draw("P same")
        up.RedrawAxis()
        down.cd()
        self.objects[5].Draw("P")
        self.objects[6].Draw("P0 same")
        self.objects[5].Draw("P same")
        self.objects[7].Draw("same E X0")
        self.objects[8].Draw("same E X0")
        if self.syshlist:
            self.grup.Draw("2 same")
            self.grdown.Draw("2 same")
        self.objects[5].Draw("P same")
        down.SetGridy(1)
        down.RedrawAxis()
        return c
