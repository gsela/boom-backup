"""
"""
import ROOT
import uuid
from happy.plot import Plot, TextElement, LegendElement
from happy.dataMcPlot import DataMcPlot
from happy.dataMcRatioPlot import DataMcRatioPlot
from happy.doublePlot import DoublePlot
from happy.variable import Binning, Variable, var_Ratio, var_Entries
from happy.style import Style
from happy.plotDecorator import AtlasTitleDecorator

from ..selection_utils import filter_selections
from ..utils import (blinded_hist, yield_table, lumi_calc, plot_title_extension, 
                     hist_max, significance_counting, hist_sum)

def make_mad_vs_she_plot(
    processor,
    selections,
    variable,
    title=None,
    print_lumi=True,
    ratio_range=0.5,
    **kwargs):
    """
    """

    _sels = filter_selections(selections, **kwargs)
   
    if title is None:
        _title, _ext = plot_title_extension(_sels, **kwargs)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    she = processor.get_hist_physics_process('ZttQCD', _sels, variable)
    mad = processor.get_hist_physics_process('ZttMad', _sels, variable)

    mad.SetLineColor(ROOT.kRed)
    mad.SetMarkerColor(ROOT.kRed)
    she.SetLineColor(ROOT.kBlue)
    she.SetMarkerColor(ROOT.kBlue)

    mad.SetTitle('Madgraph')
    she.SetTitle('Sherpa')

    var_ratio = Variable('Mad / She.', binning=Binning(low=1 - ratio_range, high=1 + ratio_range))
    var_ratio.binning.nDivisions = 5

    plot = DoublePlot(
        uuid.uuid4().hex,
        variable,
        var_Entries,
        var_ratio, 0.8, 0.2)

    plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Simulation')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 4

    plot.topMargin = 0.4

    plot.plotUp.addHistogram(she,   'E0')
    plot.plotUp.addHistogram(mad,   'E0')
    h_ratio   = mad.Clone("ratio")
    h_ratio.SetLineColor(ROOT.kBlack) 
    h_ratio.SetMarkerColor(ROOT.kBlack)
    h_ratio.Divide(she)
    plot.plotDown.addHistogram(h_ratio,   'E0')
    
    te = TextElement(_title, x=0.26, y=0.94)
    plot.textElements.append(te)

    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )

    plot.draw()
    plot.saveAs('plots/plot_mad_vs_she_{}_{}.pdf'.format(variable.name, _ext))

