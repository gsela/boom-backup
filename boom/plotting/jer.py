"""
"""
import ROOT
import uuid
# HAPPy imports
from happy.plot import Plot, TextElement, LegendElement
from happy.dataMcPlot import DataMcPlot
from happy.dataMcRatioPlot import DataMcRatioPlot
from happy.doublePlot import DoublePlot
from happy.variable import Binning, Variable, var_Ratio, var_Entries
from happy.style import Style
from happy.plotDecorator import AtlasTitleDecorator

from ..selection_utils import filter_selections
from ..utils import (blinded_hist, yield_table, lumi_calc, plot_title_extension, 
                     hist_max, significance_counting, hist_sum)



def make_yield_comparison_plot(
    processor,
    selections,
    variable,
    syst_name,
    categories,
    title=None,
    print_lumi=True,
    **kwargs):
    
    _sels = filter_selections(selections, **kwargs)
    c = ROOT.TCanvas()
    c.SetBottomMargin(0.3)
    h_template = ROOT.TH1F('h_temp', '', len(categories), 0, len(categories))
    for i_cat, _cat in enumerate(categories):
        h_template.GetXaxis().SetBinLabel(i_cat + 1, _cat)
        h_template.GetXaxis().SetTitle('Category')
        h_template.GetYaxis().SetTitle('{} / nom'.format(syst_name))
    # h_template.GetYaxis().SetRangeUser(0.90 * min(vals), 1.10 * max(vals))
    h_template.GetYaxis().SetRangeUser(0.8, 1.2)
    h_template.Draw()

    graph = ROOT.TGraphErrors(len(categories))
    for i_cat, _cat in enumerate(categories):
        print _cat
        _sels_cat = filter_selections(_sels, categories=_cat)
        ztt_nom = processor.get_hist_physics_process('Ztt', _sels_cat, variable)
        ztt_sys = processor.get_hist_physics_process('Ztt', _sels_cat, variable, syst_name=syst_name, syst_type='up')
        h_ratio = ztt_sys.Clone()
        h_ratio.Divide(ztt_nom)
        graph.SetPoint(i_cat, i_cat + 0.3, h_ratio.GetBinContent(1))
        graph.SetPointError(i_cat, 0, h_ratio.GetBinError(1))
    
    graph.SetLineColor(ROOT.kRed)
    graph.SetMarkerColor(ROOT.kRed)

    graph.Draw('samePE')
    # plot title, file extension
    if title is None:
        _title, _ext = plot_title_extension(_sels, **kwargs)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    if print_lumi:
        label = ROOT.TLatex(
            c.GetLeftMargin() + 0.02, 1 - c.GetTopMargin() - 0.07, 
            'Z#rightarrow#tau#tau, L = {0:1.0f} fb^{{-1}}'.format(lumi_calc(_sels) / 1000.))
        label.SetNDC()
        label.Draw()
    l_1 = ROOT.TLine(0, 1, len(categories), 1)
    l_1.Draw()
    c.RedrawAxis()
    c.SaveAs('plots/plot_norm_study_{}_{}_{}.pdf'.format(syst_name, variable.name, _ext))



def make_sys_nom_plot(
    processor,
    selections,
    variable,
    syst_name,
    title=None,
    print_lumi=True,
    ratio_range=0.5,
    process_name='Ztt',
    **kwargs):
    """
    """

    _sels = filter_selections(selections, **kwargs)

    # plot title, file extension
    if title is None:
        _title, _ext = plot_title_extension(_sels, **kwargs)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    nom      = processor.get_hist_physics_process(process_name, _sels, variable)
    sys_up   = processor.get_hist_physics_process(process_name, _sels, variable, syst_name=syst_name, syst_type='up')
    sys_down = processor.get_hist_physics_process(process_name, _sels, variable, syst_name=syst_name, syst_type='down')

    nom.SetLineColor(ROOT.kBlack)
    nom.SetMarkerColor(ROOT.kBlack)
    sys_up.SetLineColor(ROOT.kRed)
    sys_up.SetMarkerColor(ROOT.kRed)
    sys_down.SetLineColor(ROOT.kBlue)
    sys_down.SetMarkerColor(ROOT.kBlue)

    nom.SetTitle('nominal')
    sys_up.SetTitle(syst_name + 'up')
    sys_down.SetTitle(syst_name + 'down')

    var_ratio = Variable('var. / nom.', binning=Binning(low=1 - ratio_range, high=1 + ratio_range))
    var_ratio.binning.nDivisions = 5

    plot = DoublePlot(
        uuid.uuid4().hex,
        variable,
        var_Entries,
        var_ratio, 0.8, 0.2)


    # plot style alteration
    plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Simulation')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 4
#     plot.topMargin = 0.25
    plot.topMargin = 0.4

    plot.plotUp.addHistogram(nom,      'HIST')
    plot.plotUp.addHistogram(sys_up,   'HIST')
    plot.plotUp.addHistogram(sys_down, 'HIST')

    h_ratio_up   = sys_up.Clone()
    h_ratio_down = sys_down.Clone()
    h_ref  = sys_up.Clone()
    h_ratio_up.Divide(nom)
    h_ratio_down.Divide(nom)
    h_ref.Divide(sys_up)
    h_ref.SetTitle('MC Stat.')
    h_ref_line = h_ref.Clone()
    h_ref_line.SetLineColor(sys_up.GetLineColor())

    r = plot.plotDown.addHistogram(h_ref, 'E2')
    plot.statErrorStyle.applyTo(r)
    plot.plotDown.addHistogram(h_ref_line,   'HIST')
    plot.plotDown.addHistogram(h_ratio_up,   'HIST')
    plot.plotDown.addHistogram(h_ratio_down, 'HIST')
    te = TextElement(_title, x=0.26, y=0.94)
    plot.textElements.append(te)

    plot.legendElements.append(LegendElement(
            h_ref, 'MC Stat.', 'f'))
    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )

    plot.draw()
    plot.saveAs('plots/plot_{}_{}_{}_{}.pdf'.format(process_name, syst_name, variable.name, _ext))
