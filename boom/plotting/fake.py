"""
"""
import ROOT
import uuid
# HAPPy imports
from happy.plot import Plot, TextElement
from happy.doublePlot import DoublePlot
from happy.dataMcPlot import DataMcPlot
from happy.dataMcRatioPlot import DataMcRatioPlot
from happy.variable import Binning, Variable, var_Ratio, var_Entries
from happy.style import Style
from happy.plotDecorator import AtlasTitleDecorator

from ..selection_utils import filter_selections
from ..utils import hist_sum, blinded_hist, yield_table, lumi_calc, plot_title_extension, hist_max




def make_fake_template_plot(
    processor,
    selections, 
    variable, 
    title='All channels, All SRs',    
    print_lumi=True,
    **kwargs):
    """
    """
    
    _sels = filter_selections(selections, **kwargs)

    print 'BOOM: make fake template plot for', title
    plot = DataMcPlot(
        uuid.uuid4().hex, 
        variable) 
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 3
#     plot.topMargin = 0.25
    plot.topMargin = 0.4
    sgn_scale = 1

    hH = processor.get_hist_physics_process('Signal', _sels, variable, lumi_scale=sgn_scale)
    hH.SetName('Signal')
    print 'BOOM: \t Signal: {}'.format(hH.Integral()/sgn_scale)

    hD = processor.get_hist_physics_process('Data', _sels, variable)
    print 'BOOM: \t Data: {}'.format(hD.Integral())
    plot.setDataHistogram( hD )

    bkgs = []
    bkg_names = []
    for name in processor.merged_physics_process_names[::-1]:
        p = processor.get_physics_process(name)[0]
        if p.isSignal:
            continue
        if p.name == 'Data':
            continue
        h = processor.get_hist_physics_process(name, _sels, variable)
        plot.addHistogram(h)
        bkgs.append(h)
        bkg_names.append(name)
        print 'BOOM: \t {}: {}'.format(name, h.Integral())
  
    plot.addHistogram(hH, stacked=False)
    plot.titles.append(title)
    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )
    plot.draw()
    plot.saveAs('./plots/plot_fake_template_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_')))
    print yield_table(hD, hH, bkgs, bkg_names=bkg_names)


def make_region_comparison_plot(
    processor,
    selections,
    variable,
    process_name = '',
    category='boost'):

    print 'BOOM: make plot for', category, process_name, variable.name
    sels_sr  = filter_selections(selections, regions=('anti_tau'), categories=category)
    sels_qcd = filter_selections(selections, regions=('qcd_lh_anti_tau'), categories=category)
    sels_w   = filter_selections(selections, regions=('W_lh_anti_tau'), categories=category)

    # get data histogram
    h_data_sr  = processor.get_hist_physics_process('Data', sels_sr, variable)
    h_data_qcd = processor.get_hist_physics_process('Data', sels_qcd, variable)
    h_data_w   = processor.get_hist_physics_process('Data', sels_w, variable)

    h_mcs_sr  = []
    h_mcs_qcd = []
    h_mcs_w   = []

    for p in processor.mc_backgrounds:
        h_sr = processor.get_hist_physics_process(p.name, sels_sr, variable)
        h_qcd = processor.get_hist_physics_process(p.name, sels_qcd, variable)
        h_w = processor.get_hist_physics_process(p.name, sels_w, variable)

        h_mcs_sr.append(h_sr)
        h_mcs_qcd.append(h_qcd)
        h_mcs_w.append(h_w)

    h_mc_sr  = hist_sum(h_mcs_sr)
    h_mc_qcd = hist_sum(h_mcs_qcd)
    h_mc_w   = hist_sum(h_mcs_w)

    h_sr  = h_data_sr.Clone()
    h_qcd = h_data_qcd.Clone()
    h_w   = h_data_w.Clone()

    h_sr.Add(h_mc_sr, -1)
    h_qcd.Add(h_mc_qcd, -1)
    h_w.Add(h_mc_w, -1)
  
    h_sr.SetTitle('AntiID SR')
    h_qcd.SetTitle('AntiID QCD CR')
    h_w.SetTitle('AntiID W CR')

    h_sr.Scale(1. / h_sr.Integral())
    h_qcd.Scale(1. / h_qcd.Integral())
    h_w.Scale(1. / h_w.Integral())

    h_sr.SetLineColor(ROOT.kViolet)
    h_qcd.SetLineColor(ROOT.kRed)
    h_w.SetLineColor(ROOT.kBlue)

    h_sr.SetMarkerColor(ROOT.kViolet)
    h_qcd.SetMarkerColor(ROOT.kRed)
    h_w.SetMarkerColor(ROOT.kBlue)

    #get cumulative distribution
    h_sr_cum  = h_sr.GetCumulative(False)
    h_qcd_cum = h_qcd.GetCumulative(False)
    h_w_cum   = h_w.GetCumulative(False)

    luminosity = lumi_calc(sels_sr)

    yVarDown = Variable ( 'Ratio', binning=Binning(low=0.5, high=1.5) )
    plot = DoublePlot( uuid.uuid4().hex, variable, var_Entries, yVarDown, 474, 192 )
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )
    plot.titles.append('{} category'.format(category))
    plot.plotUp.topMargin = 0.5
    plot.plotUp.addHistogram(h_sr, 'E0')
    plot.plotUp.addHistogram(h_qcd, 'E0')
    plot.plotUp.addHistogram(h_w, 'E0')
    hratio_qcd = h_qcd.Clone('ratio_qcd')
    hratio_w   = h_w.Clone('ratio_w')
    hratio_qcd.Divide(h_sr)
    hratio_w.Divide(h_sr)
    plot.plotDown.addHistogram( hratio_qcd, 'E0' )
    plot.plotDown.addHistogram( hratio_w, 'E0' )
    plot.logX = True
    plot.plotDown.logX = True
    plot.draw()
    plot.saveAs('./plots/plot_compare_{}_{}.pdf'.format(variable.name, category.replace(',', '').replace(' ', '_')))

    yVarDown = Variable ( 'Ratio', binning=Binning(low=0.5, high=1.5) )
    plot = DoublePlot( uuid.uuid4().hex, variable, var_Entries, yVarDown, 474, 192 )
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )
    plot.titles.append('{} category'.format(category))
    plot.plotUp.topMargin = 0.5
    plot.plotUp.addHistogram(h_sr_cum, 'E0')
    plot.plotUp.addHistogram(h_qcd_cum, 'E0')
    plot.plotUp.addHistogram(h_w_cum, 'E0')
    hratio_qcd_cum = h_qcd_cum.Clone('ratio_qcd_cum')
    hratio_w_cum   = h_w_cum.Clone('ratio_w_cum')
    hratio_qcd_cum.Divide(h_sr_cum)
    hratio_w_cum.Divide(h_sr_cum)
    plot.plotDown.addHistogram( hratio_qcd_cum, 'E0' )
    plot.plotDown.addHistogram( hratio_w_cum, 'E0' )
    plot.logX = True
    plot.plotDown.logX = True
    plot.draw()
    plot.saveAs('./plots/plot_cumulative_{}_{}.pdf'.format(variable.name, category.replace(',', '').replace(' ', '_')))
 
