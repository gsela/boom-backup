"""
"""
import ROOT
import uuid
# HAPPy imports
from happy.plot import Plot, TextElement
from happy.dataMcPlot import DataMcPlot
from happy.dataMcRatioPlot import DataMcRatioPlot
from happy.variable import Binning, Variable, var_Ratio
from happy.style import Style
from happy.plotDecorator import AtlasTitleDecorator

from ..selection_utils import filter_selections
from ..utils import (blinded_hist, yield_table, lumi_calc, plot_title_extension, 
                     hist_max, significance_counting, hist_sum)


def make_data_mc_plot(
    processor,
    selections, 
    variable, 
    title=None,
    dir=None,
    print_lumi=True,
    signal_scaling_factor=10,
    force_unblind=True,
    compute_significance=False,
    split_signal=False,
    ratio_range=0.75,
    **kwargs):
    """
    """
    
    _sels = filter_selections(selections, **kwargs)

    # plot title, file extension
    if title is None:
        _title, _ext = plot_title_extension(_sels, **kwargs)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    # underflow/overflow bin
    if 'n_jets_30' in variable.name:
        variable.binning.includeOverflowBin = False
    else:
        variable.binning.includeOverflowBin = True

    print 'BOOM: make plot for', _title
    var_dataMc = Variable('Data / Pred.', binning=Binning(low=1 - ratio_range, high=1 + ratio_range))
    var_dataMc.binning.nDivisions = 5
    plot = DataMcRatioPlot(
        uuid.uuid4().hex, 
        variable, 
        yVariableDown=var_dataMc, 
        upperFraction=0.80, 
        lowerFraction=0.20)

    # plot style alteration
    plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    #plot.legendDecorator.maxEntriesPerColumn = 8
    plot.legendDecorator.maxEntriesPerColumn = 5
    plot.topMargin = 0.5
#    plot.topMargin = 1.0

    # retrieve background histograms
    bkgs = []
    bkg_names = []
    for name in processor.merged_physics_process_names[::-1]:
        if name == 'Data' or name =='ttHother':
            continue
        h = processor.get_hist_physics_process(name, _sels, variable)
        plot.addHistogram(h)
        if 'Signal' in name:
           h.SetFillColor(ROOT.kRed)
           h.SetTitle('Signal')
        else:
           bkgs.append(h)
           bkg_names.append(name)

    hBkg = hist_sum(bkgs)
    other_signals = []

    # retrieve signal histograms
    # hH_no_scaling = processor.get_hist_physics_process('Signal', _sels, variable)
    # hH_no_scaling.SetName('Signal')
    
    # hVH_no_scaling = processor.get_hist_physics_process('ZH', _sels, variable)
    # hVH_no_scaling.Add(processor.get_hist_physics_process('WH', _sels, variable))
    # hVH_no_scaling.SetName('VH')

    # hggH_no_scaling = processor.get_hist_physics_process('ggH', _sels, variable)
    # hggH_no_scaling.SetName('ggH')

    # hVBFH_no_scaling = processor.get_hist_physics_process('VBFH', _sels, variable)
    # hVBFH_no_scaling.SetName('VBFH')

    # httH_no_scaling = processor.get_hist_physics_process('ttHtt', _sels, variable)
    # httH_no_scaling.SetName('ttH')

    if split_signal:
        h_ZH = processor.get_hist_physics_process('ZH', _sels, variable, lumi_scale=signal_scaling_factor)
        h_WH = processor.get_hist_physics_process('WH', _sels, variable, lumi_scale=signal_scaling_factor)
        h_VH = h_ZH.Clone()
        h_VH.Add(h_WH)

        h_ttH = processor.get_hist_physics_process('ttHtt', _sels, variable, lumi_scale=signal_scaling_factor)
        h_VBF = processor.get_hist_physics_process('VBFH', _sels, variable, lumi_scale=signal_scaling_factor)
        h_ggH = processor.get_hist_physics_process('ggH', _sels, variable, lumi_scale=signal_scaling_factor)

        h_Signal = h_VBF.Clone()
        h_Signal.Add(h_ggH)
        h_Signal.Add(h_ZH)
        h_Signal.Add(h_WH)
        h_Signal.Add(h_ttH)
        
        h_ggH.SetLineStyle(2)
        h_VH.SetLineStyle(3)
        h_ttH.SetLineStyle(4)
        h_VBF.SetLineStyle(5)

        if signal_scaling_factor != 1:
            h_Signal.SetTitle('Tot. Signal (x{})'.format(signal_scaling_factor))
            h_VBF.SetTitle('VBFH (x{})'.format(signal_scaling_factor))
            h_ggH.SetTitle('ggH (x{})'.format(signal_scaling_factor))
            h_VH.SetTitle('VH (x{})'.format(signal_scaling_factor))
            h_ttH.SetTitle('ttH (x{})'.format(signal_scaling_factor))
        else:
            h_Signal.SetTitle('Tot. Signal')
            h_ggH.SetTitle('VBFH')
            h_ggH.SetTitle('ggH')
            h_VH.SetTitle('VH')
            h_ttH.SetTitle('ttH')

        _all_cats = [_sel.category for _sel in _sels]
        _vh_cats = filter(lambda c: 'vh' in c, _all_cats)
        if len(_vh_cats) != 0:
            plot.addHistogram(h_VH, stacked=False)

        _tth_cats = filter(lambda c: 'tth' in c, _all_cats)
        if len(_tth_cats) != 0:
            plot.addHistogram(h_ttH, stacked=False)

        _ggh_cats = filter(lambda c: 'boost' in c, _all_cats)
        if len(_ggh_cats) != 0:
            plot.addHistogram(h_ggH, stacked=False)

        _vbf_cats = filter(lambda c: 'vbf' in c, _all_cats)
        if len(_vbf_cats) != 0:
            plot.addHistogram(h_VBF, stacked=False)

        plot.addHistogram(h_Signal, 'HIST', stacked=False)
    #else:
        # hH = processor.get_hist_physics_process('Signal', _sels, variable, lumi_scale=signal_scaling_factor)
        # if signal_scaling_factor != 1:
        #     hH.SetTitle(hH.GetTitle() + '(x{})'.format(signal_scaling_factor))
        # plot.addHistogram(hH, stacked=False)

    # retrieve data histogram, blind if necessary and plot
    hD = processor.get_hist_physics_process('Data', _sels, variable)

    # figure out if blinding is needed
    if not force_unblind:
        if 'mmc' in variable.name:
            hD_blinded = blinded_hist(hD)
            plot.setDataHistogram(hD_blinded)
        elif 'visible_mass' in variable.name:
            hD_blinded = blinded_hist(hD, var_name='visible')
            plot.setDataHistogram(hD_blinded)
        elif 'ditau_coll_approx_m' in variable.name:
            hD_blinded = blinded_hist(hD, var_name='collinear')
            plot.setDataHistogram(hD_blinded)
        elif variable.name in [
                'vbf_tagger', 
                'tth_tt_bdt', 
                'tth_Z_bdt',
                'vh_tagger',
                'hh_vh_tagger', 
                'lh_vh_tagger', 
                'll_vh_tagger',
                'pt_total',
                'pt_total_low']:

            if 'pt_total' in variable.name:
                rtol_blind_range = True
            else :
                rtol_blind_range = False    

            hD_blinded = blinded_hist(hD, var_name=variable.name, signal=hH_no_scaling, rtol_blind_range = rtol_blind_range)
            plot.setDataHistogram(hD_blinded)
        else:
            plot.setDataHistogram(hD)
    else:
        plot.setDataHistogram(hD)

    # if compute_significance:
    #     if split_signal:
    #         hBkg_vh  = hist_sum([hBkg, hVBFH_no_scaling, hggH_no_scaling, httH_no_scaling]) 
    #         hBkg_tth = hist_sum([hBkg, hVBFH_no_scaling, hggH_no_scaling, hVH_no_scaling]) 
    #         hBkg_ggh = hist_sum([hBkg, hVBFH_no_scaling, httH_no_scaling, hVH_no_scaling]) 
    #         hBkg_vbf = hist_sum([hBkg, hggH_no_scaling, httH_no_scaling, hVH_no_scaling]) 
    #         _vh_significance = significance_counting(hVH_no_scaling, hBkg_vh)
    #         print 'BOOM: \t significance VH:',  _title, significance_counting(hVH_no_scaling, hBkg_vh)
    #         print 'BOOM: \t significance ttH:', _title, significance_counting(httH_no_scaling, hBkg_tth)
    #         print 'BOOM: \t significance VBF:', _title, significance_counting(hVBFH_no_scaling, hBkg_vbf)
    #         print 'BOOM: \t significance ggH:', _title, significance_counting(hggH_no_scaling, hBkg_ggh)
    #     else:
    #         print 'BOOM: \t significance:', _title, significance_counting(hH_no_scaling, hBkg)
    


    te = TextElement(_title, x=0.26, y=0.94)
    plot.textElements.append(te)
    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )

    plot.draw()
    plot.saveAs('./plots/'+dir+'/{}_{}.pdf'.format(_title, variable.name))
    print
    #print yield_table(hD, hH_no_scaling, bkgs, bkg_names=bkg_names)
    print yield_table(hD, hD, bkgs, bkg_names=bkg_names)
    print
