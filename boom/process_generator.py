"""
Database generation from the list of ntuples
"""
# happy imports
from happy.dataset import RootDataset, CombinedDataset

class process_generator(object):
    '''Class which auto-generates HAPPY.PhysicsProcesses from basic information'''

    def __init__(
        self, path, 
        do_zll_vr=True):

        # Internal dictionary containing physics processes
        self._processes = []

        # Local path for datasets
        self._path = path

        # Channel name book-keeping
        if do_zll_vr:
            self._short_streams = {
                'hadhad': 'hh', 
                'lephad': 'lh', 
                'leplep': 'll', 
                'zll_vr': 'zll_vr',}        
        else:
            self._short_streams = {
                'hadhad': 'hh', 
                'lephad': 'lh', 
                'leplep': 'll'}

        self._process_info = {}

    def process_info(self, process):
        try:
            return self._process_info[process.name]
        except KeyError:
            raise NotImplementedError('process %s does not exist' % process.name)

    @property
    def path(self):
        return self._path

    @property
    def processes(self):
        """return the generated list of processes"""
        return self._processes
    

    def generate_mc(
        self, name, title, dsids, 
        isSignal=False, 
        campaigns=['mc16a', 'mc16d', 'mc16e'], 
        style=None, 
        ignoreChannel=[],
        weightExpression='1',
        prefix=None,
        ntuples_block='nom'):
        """
        """
        if name in self._process_info.keys():
            raise RuntimeError('process with name %s already created' % name)

        self._process_info[name] = {
            'campaigns': [],
            'dsids': dsids,
            'streams': []
            }

        # internal list of datasets for process creation
        _dataset_list = []

        for _dsid in dsids:
            for _campaign in campaigns:
                for _stream in self._short_streams.keys():
                    _skip = False

                    # ttbar sample: 410470-410471 for hadhad/lephad/zll_vr, 410471-410472 for leplep
                    if _stream == 'leplep':
                        if _dsid == 410470:
                            _skip = True
                    else :
                        if _dsid == 410472:
                            _skip = True
        
                    # Ztt powheg not available for ZCR and hadhad
                    if _stream in ('hadhad','zll_vr'):
                        if _dsid == 361108:
                            _skip = True
         
                    for entry in ignoreChannel:
                        if _stream == entry[0] and _dsid not in entry[1]:
                            _skip = True
                    
                    if _skip == True:
                        continue

                    if not _campaign in self._process_info[name]['campaigns']:
                        self._process_info[name]['campaigns'].append(_campaign)

                    if not self._short_streams[_stream] in self._process_info[name]['streams']:
                        self._process_info[name]['streams'].append(self._short_streams[_stream])

                    # Generate filename & dataset name
                    _files = "".join((self._path, "/mc/", _stream, "/" + _campaign + "/" + ntuples_block + "/*", str(_dsid), "*/*.root*"))
                    _name = _campaign + "_" + str(_dsid) + "_" + self._short_streams[_stream]
                    if prefix != None:
                        _name = prefix + "_" + _name

                    # additional requirement due to xCompression applied only in kin syst ntuples
                    # look MR : https://gitlab.cern.ch/ATauLeptonAnalysiS/boom/-/merge_requests/400
                    if ntuples_block != 'nom':                    
                        weightExpression += ' *(useEvent==1)'

                    # Create dataset
                    _dataset_list += [RootDataset(
                            _name, 
                            fileNames=[_files], 
                            weightExpression=weightExpression, 
                            isSignal=isSignal)]
                    
                    # Auto-set cross section information
                    _dataset_list[-1].dsid = _dsid

        self._processes += [CombinedDataset(
                name, title, 
                style=style, 
                datasets=_dataset_list, 
                isSignal=isSignal)]

    def generate_data(
        self, 
        name="Data", 
        title="Data", 
        years=['15', '16', '17', '18'], 
        style=None, 
        prefix=None):
        """
        """
        if name in self._process_info.keys():
            raise RuntimeError('process with name %s already created' % name)

        self._process_info[name] = {
            'years': years,
            }

        # internal list of datasets for process creation        
        _dataset_list = []

        for _year in years:
            for _stream in self._short_streams.keys():
                # print '$$$stream:'
                # print _stream
                # Generate filename & dataset name
                _files = "".join((self._path, "/data/", _stream, "/data", str(_year), "/*/*.root*"))
                _name = "data_" + str(_year) + "_" + self._short_streams[_stream]

                if prefix != None:
                    _name = prefix + "_" + _name

                # Create dataset
                _dataset_list += [RootDataset(_name, fileNames=[_files], isData=True)]

        self._processes += [CombinedDataset(name, title, style=style, datasets=_dataset_list, isData=True)]
