"""
Module to define systematics blocks and 
process groups for running on the batch system
"""
from ..systematics.summary import SUMMARY, embedding_systematics

KINEMATIC_BLOCKS = [
    'sys_l',
    'sys_j1',
    'sys_j2',
    'sys_j4',
    'sys_j5',
    'sys_j6',
]
"""
Blocks of kinematic variations
""" 

WEIGHT_BLOCKS = [
    'ELEC_SF',
    'MUON_SF',
    'FT_SF_1',
    'FT_SF_2',
    'JETPRW_SF',
    'TAU_SF',
    'TAU_TRIGGER_SF',
    'THEORY_SYST_ZJETS',
    'THEORY_SYST_PDF_ZJETS_0',
    'THEORY_SYST_PDF_ZJETS_1',
    'THEORY_SYST_PDF_ZJETS_2',
    'THEORY_SYST_PDF_ZJETS_3',
    'THEORY_SYST_PDF_ZJETS_4',
    'THEORY_SYST_PDF_ZJETS_5',
    'THEORY_SYST_PDF_ZJETS_6',
    'THEORY_SYST_PDF_ZJETS_7',
    'THEORY_SYST_PDF_ZJETS_8',
    'THEORY_SYST_PDF_ZJETS_9',
    'THEORY_SYST_MUR_MUF_SGN',
    'THEORY_SYST_QCD_SGN',
    'THEORY_SYST_ST_GGH',
    'THEORY_SYST_PDF_SGN_0',
    'THEORY_SYST_PDF_SGN_1',
    'THEORY_SYST_PDF_SGN_2',
    'LL_FAKE',
    'LH_FAKE',
    'HH_FAKE',
]
"""
Defining blocks of weight systematics
"""


PROCESS_GROUPS = {
    'Data' : ['Data'],
    'ZttQCD': ['ZttQCD_truth_0', 'ZttQCD_truth_1', 'ZttQCD_truth_2', 'ZttQCD_truth_3'],
    'ZttEWK': ['ZttEWK_truth_0', 'ZttEWK_truth_1', 'ZttEWK_truth_2', 'ZttEWK_truth_3'],
    'ZllQCD': ['ZllQCD_truth_0', 'ZllQCD_truth_1', 'ZllQCD_truth_2', 'ZllQCD_truth_3'],
    'ZllEWK': ['ZllEWK_truth_0', 'ZllEWK_truth_1', 'ZllEWK_truth_2', 'ZllEWK_truth_3'],
    'Top': ['Top'],
    'ttV_4t_3t' : ['ttV_4t_3t'],
    'VV': ['VV'],
    'W': ['W'],
    'Fake': ['Fake'],
}
"""
Defining blocks of processes to be launched
"""

SIGNAL_PROCESS_GROUPS = {
    'ggH': ['ggH'],
    'ggHWW': ['ggHWW'],
    'VBFH': ['VBFH'],
    'VBFHWW': ['VBFHWW'],
    'WH': ['WH'],
    'ZH': ['ZH'],
    'ttHtt': ['ttHtt'],
    'ttHWW': ['ttHWW'],
    'ttHother': ['ttHother'],
}
"""
Defining blocks of signal processes to be launched
"""
# add STXS stage 1 to the list
from ..stxs import STXS_STAGE1, STXS_STAGE12, STXS_STAGE12_FINE 
for k, v in STXS_STAGE1.items():
    _p = []
    for key in v.keys():
        _p.append(k + key)
    SIGNAL_PROCESS_GROUPS[k + '_stxs1'] = _p

for k, v in STXS_STAGE12.items():
    _p = []
    for key in v.keys():
        if 'ttH' in k:
            # do split only for tt -> WW, other only for HComb WSI
            for decay in ('tt',):
                _p.append( k + '_' + decay + key )
        else:
            _p.append(key)
    
    if 'ttH' in k:
        for decay in ('tt',):
            SIGNAL_PROCESS_GROUPS[ k + '_' + decay + '_stxs12'] = _p
    else:
        SIGNAL_PROCESS_GROUPS[k + '_stxs12'] = _p

for k, v in STXS_STAGE12_FINE.items():
    _p = []
    for key in v.keys():
        if 'ttH' in k:
            # do split only for tt -> WW, other only for HComb WSI
            for decay in ('tt',):
                _p.append( k + decay + key[3:] )
        else: 
            _p.append(key)

    if 'ttH' in k:
        for decay in ('tt',):
            SIGNAL_PROCESS_GROUPS[ k + decay + '_stxs12_fine'] = _p
    else:
        SIGNAL_PROCESS_GROUPS[k + '_stxs12_fine'] = _p

# Add signal processes to the dict of processes
PROCESS_GROUPS.update(SIGNAL_PROCESS_GROUPS)


def get_variations(flavour='kinematics', z_cr=False):
    """
    """
    if flavour not in ('kinematics', 'weights', 'embedding'):
        raise ValueError

    if flavour == 'kinematics':
        _var_block = {}
        for _block in KINEMATIC_BLOCKS:
            _systs = []
            for k, v in SUMMARY.items():
                if v['group'] == _block:
                    _systs.append(k)
            _var_block[_block] = _systs
        if z_cr:
            _flattened_var_block = {}
            for k, v in SUMMARY.items():
                if v['group'] in KINEMATIC_BLOCKS:
                    _flattened_var_block[v['group'] + '__' + k] = [k]
            return _flattened_var_block
        else:
            return _var_block
        # return KINEMATICS
    elif flavour == 'weights':
        _var_block = {}
        for _block in WEIGHT_BLOCKS:
            _systs = []
            for k, v in SUMMARY.items():
                if v['group'] == _block:
                    _systs.append(k)
            _var_block[_block] = _systs
        return _var_block
        # return WEIGHT_VARIATIONS
    else:
        _flattened_dict = {}
        for k in embedding_systematics().keys():
            _flattened_dict[k] = [k]
        return _flattened_dict
