import os
from .definitions import get_variations
from .definitions import PROCESS_GROUPS
 
class JobLog(object):
    """JobLog

    Small helper class to interpret the result stored
    in the condor output folder as well as define the property of 
    the job to submit
    """

    def __init__(self, dir_name):
        self._channel = self._check_channel(dir_name)
        self._year = self._check_year(dir_name)
        self._region = self._check_region(dir_name)
        self._variation = self._check_variation(dir_name)
        self._process = self._check_process(dir_name)
        self._infos = []
        self._zllqcd_dsid = self._check_zllqcd_dsid(dir_name)
        self._oversampling = False
        self._output = None
        """
        Parameters
        ----------
        dir_name: str
        """

    def add(self, info_dict):
        """
        """
        self._infos.append(info_dict)
        self._determine_output()

    def _determine_output(self):
        """
        """
        if len(self._infos) == 0:
            self._output == None
        elif len(self._infos) == 1:
            self._output = self._infos[0]
        else:
            for _info in self._infos:
                _output_job_number = self._check_job_number(
                    self._output['dir'])
                _current_job_number = self._check_job_number(
                    _info['dir'])
                                # if not _info['corrupted']:
                if self._output['corrupted'] or self.status in ('held', 'evicted',):
                    if _current_job_number > _output_job_number:
                        self._output = _info


    def __eq__(self, other):
        """
        """
        if self.channel == other.channel:
            if self.year == other.year:
                if self.region == other.region:
                    if self.variation == other.variation:
                        if self.process == other.process:
                            if self.zllqcd_dsid == other.zllqcd_dsid:
                                return True
        return False

    def __str__(self):
        """
        """
        return '{} {} {} {} {} {}'.format(
            self._channel,
            self._year,
            self._region,
            self._process,
            self._variation,
            self.job_number)

    def __repr__(self):
        """
        """
        return self.__str__()

    @property
    def attempts(self):
        """
        """
        return len(self._infos)

    @property
    def status(self):
        """
        """
        if self._output != None:
            return self._output['status']
        else:
            return 'unknown'

    @property
    def corrupted(self):
        """
        """
        if len(self._infos) == 0:
            return False
        else:
            if self._oversampling:
                _corrupted = filter(lambda i: i['corrupted'], self._infos)
                if len(_corrupted) != 0:
                    return True
                else:
                    return False
            else:
                return self._output['corrupted']
                
    @property
    def done(self):
        """
        """
        try:
            log_file = os.path.join(
                self._output['path'],
                self._output['log'])
            with open(log_file) as f:
                _lines = f.readlines()
                for _l in _lines:
                    if 'Normal termination' in _l:
                        return True
            return False
        except:
            print log_file, ' does not exist!'
            return False

    @property
    def memory(self):
        return self._output['memory']

    @property
    def minutes(self):
        return self._output['totalruntime'].total_seconds() / 60.

    @property
    def best_queue(self):
        if self.minutes < 20.:
            return 'espresso'
        elif self.minutes < 60.:
            return 'microcentury'
        elif self.minutes < 2. * 60:
            return 'longlunch'
        elif self.minutes < 8 * 60.:
            return 'workday'
        elif self.minutes < 24 * 60.:
            return 'tomorrow'
        elif self.minutes < 3 * 24 * 60:
            return 'testmatch'
        else:
            return 'nextweek'

    @property
    def job_number(self):
        return self._check_job_number(
            self._output['dir'])


    @property
    def output(self):
        """ """
        return self._output


    @property
    def channel(self):
        """ """
        return self._channel
        
    @property
    def year(self):
        """ """
        return self._year

    @property
    def region(self):
        """ """
        return self._region

    @property
    def variation(self):
        """ """
        return self._variation

    @property
    def process(self):
        """ """
        return self._process
    
    @property
    def zcr(self):
        """ """
        if 'zcr' in self._region:
            return True
        return False

    @property
    def zcr_flavour(self):
        if not self.zcr:
            return None
        else:
            if 'ee' in self._region:
                return 'ee'
            elif 'mumu' in self._region:
                return 'mumu'
            else:
                return None

    @property
    def zllqcd_dsid(self):
        """ """
        return self._zllqcd_dsid


    def _check_year(self, name):
        """ """
        if 'year_15_16' in name:
            return '15 16'
        elif 'year_17' in name:
            return '17'
        elif 'year_18' in name:
            return '18'
        else:
            return None

    def _check_channel(self, name):
        """ """
        if 'channel_ll' in name:
            return 'll'
        elif 'channel_lh' in name:
            return 'lh'
        elif 'channel_hh' in name:
            return 'hh'
        elif 'channel_all' in name:
            return 'all'
        else:
            return None

    def _check_variation(self, name):
        """ """
        if 'nominal' in name:
            return 'nominal'
        
        _weights = get_variations('weights')
        _kinematics = get_variations('kinematics', z_cr=self.zcr)

        _accepted = ''
        for _group in _weights.keys():
            if _group in name:
                if len(_group) > len(_accepted):
                    _accepted = _group
        for _group in _kinematics.keys():
            if _group in name:
                if len(_group) > len(_accepted):
                    _accepted = _group
        if 'zcr' in self._region:
            _embedding = get_variations('embedding')
            for _group in _embedding.keys():
                if _group in name:
                    if len(_group) > len(_accepted):
                        _accepted = _group

        if _accepted != '':
            return _accepted
        else:
            return None

    def _check_process(self, name):
        """ """
        _accepted = ''
        for _group in PROCESS_GROUPS.keys():
            if _group in name:
                if len(_group) > len(_accepted):
                    _accepted = _group
        if _accepted != '':
            return _accepted
        else:
            return None
        
    def _check_region(self, name):
        """ """
        if '_zcr' in name:
            if '_ee' in name:
                if '_corr' in name:
                    return 'zcr_ee_corr'
                if not 'zcrdeactivatecorrections' in name:
                    return 'zcr_ee_corr'
                return 'zcr_ee'
            elif '_mumu' in name:
                if '_corr' in name:
                    return 'zcr_mumu_corr'
                if not 'zcrdeactivatecorrections' in name:
                    return 'zcr_mumu_corr'
                return 'zcr_mumu'
            else:
                return 'zcr'
        else:
            return 'sr'

    def _check_job_number(self, name):
        """ """
        _number = int(name.split('job')[-1])
        return _number


    def _check_zllqcd_dsid(self, name):
        if not 'zcrzllqcddsid' in name:
            return None
        _dsid = name.split('zllqcddsid_')[-1].split('_')[0]
        return _dsid




