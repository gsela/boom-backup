"""
Module holding small helper functions 
to scrap data from batch logs
"""
import datetime

def _str_to_time(str_time):
    """
    convert string to a date time object
    """
    h, m, s = str_time.replace(',', '').split(':')
    _time = datetime.timedelta(
        hours=int(h),
        minutes=int(m),
        seconds=int(s))
    return _time

def _runtime(log_file):
    """
    scrap time string from log file
    """
    try:
        with open(log_file) as f:
            _lines = f.readlines()
            for _l in _lines:
                if 'Total Remote Usage' in _l:
                    return _l
        return ''
    except:
        print log_file, ' does not exist!'
        return ''

def _memory(log_file):
    """
    scrap time string from log file
    """
    try:
        with open(log_file) as f:
            _lines = f.readlines()
            for _l in _lines:
                if 'Memory (MB)' in _l:
                    return _l
        return ''
    except:
        print log_file, ' does not exist!'
        return ''
