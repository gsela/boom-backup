"""
Module to gather functions needed by users to launch and analyse batch jobs
"""
import os
import re
import itertools
from .definitions import PROCESS_GROUPS, SIGNAL_PROCESS_GROUPS
from .definitions import get_variations

from .joblog import JobLog
from .inspect import determine_job_status, inspect_log
from .tools import _str_to_time, _runtime, _memory

from ..systematics.utils import _is_valid_list


def _expected_groups(
        channel, year, process_group, 
        is_signal=False, 
        z_cr=False, 
        z_cr_flavour=None, 
        z_cr_deactivate_corrections=False):
    """
    Small function to determine if a group of systematic is to 
    be expected for the given channel/year/process
    Parameters
    ----------
    channel: str
    year: str
    process_group: str
    is_signal: bool
       default = False
    z_cr: bool
       default = False
    z_cr_flavour: str
       default = None
    z_cr_deactivate_corrections: bool
       default = False

    Returns
    -------
    _groups: list(str)
    """
    _groups = []
    _kinematics = get_variations('kinematics', z_cr=z_cr)
    _weights = get_variations('weights')
    _variations = _kinematics.keys() + _weights.keys()
    if z_cr and not z_cr_deactivate_corrections:
        _embedding = get_variations('embedding')
        _variations += _embedding.keys()
    for _group in sorted(_variations):
        if _group in _weights.keys():
            _syst_list = _weights[_group]
        elif _group in _kinematics.keys():
            _syst_list = _kinematics[_group]
        else:
            _syst_list = _embedding[_group]

        _valid_list = _is_valid_list(
            _syst_list, channel, year,
            process_group, 
            is_signal=is_signal,
            z_cr=z_cr,
            z_cr_flavour=z_cr_flavour)

        if len(_valid_list) != 0:
            _groups.append(_group)

    return sorted(['nominal'] + _groups)


def expected_jobs(
    channel=None, 
    year=None, 
    process_group=None, 
    variation_group=None,
    z_cr=False, 
    z_cr_flavour=None, 
    z_cr_deactivate_corrections=False,
    stxs=None,
    z_cr_backgrounds=False):
    """
    Small function to return list of combination 
    (channel, year, process, variation group)
    Parameters
    ----------
    channel: str
    year: str
    process_group: str
    variatio_group :str
    z_cr: bool
    z_cr_flavour: str
    z_cr_deactivate_corrections: bool
    stxs: str
    """

    if channel == None:
        if z_cr:
            _channels = ['all']
        else:
            _channels = [
                'll', 
                'lh', 
                'hh', 
            ]
    else:
        if isinstance(channel, (list, tuple)):
            _channels = channel
        else:
            _channels = [channel]

    if year == None:
        _years = [
            '15 16', 
            '17', 
            '18',
        ]
    else:
        if year == ['15', '16']:
            _years = ['15 16']
        else:
            if isinstance(year, (list, tuple)):
                _years = year 
            else:
                _years = [year]
                
    if process_group == None:
        _processes = PROCESS_GROUPS.keys()
    else:
        if not isinstance(process_group, (list, tuple)):
            _processes = [process_group]
        else:
            _processes = process_group

    _variation_groups = variation_group
    if not isinstance(_variation_groups, (list, tuple)) and _variation_groups != None:
        _variation_groups = [_variation_groups]

    _z_cr_flavours = z_cr_flavour
    if not isinstance(_z_cr_flavours, (list, tuple)):
        _z_cr_flavours = [z_cr_flavour]


    if z_cr and not z_cr_deactivate_corrections:
        if z_cr_backgrounds == False:
            _processes = filter(lambda p: 'Zll' in p or p == 'Data', _processes)
        else:
            _processes = filter(lambda p: 'Zll' in p or p == 'Data' or 'Ztt' in p or p == 'W' or 'Top' in p or 'VV' in p, _processes)

    if z_cr:
        # manually skip Fake for zcr for now
        _processes = filter(lambda p: p != 'Fake', _processes)

    if stxs != None:
        _processes = filter(lambda _p: ('stxs' in _p and stxs in _p) or not 'stxs' in _p, _processes)

    # building list of jobs from all possible combination
    # of channel (all, ll, lh, hh), year, process and zcr_flavour (ee, mumu)
    _jobs = []
    for _chan, _year, _process, _z_cr_flavour in itertools.product(
        _channels, _years, _processes, _z_cr_flavours):
        _variations = _expected_groups(
            _chan, _year, _process, 
            is_signal=_process in SIGNAL_PROCESS_GROUPS.keys(),
            z_cr=z_cr, 
            z_cr_flavour=_z_cr_flavour,
            z_cr_deactivate_corrections=z_cr_deactivate_corrections)
        for _var in _variations:
            if _variation_groups != None and not _var in _variation_groups:
                continue

            # by default do not split per DSID
            _dsids = [None]
            if z_cr and not z_cr_deactivate_corrections and _process == 'ZllQCD':
                from ..database import ZllQCD
                _dsids = ZllQCD
            for _dsid in _dsids:
                # instantiate a job log
                _job = JobLog('')
                _job._year = _year
                _job._channel  = _chan
                _job._process = _process
                _job._variation = _var
                _job._region = 'zcr' if z_cr else 'sr'
                # print _year, _chan, _process, _var, _job._region, _dsid, _z_cr_flavour
                if z_cr and _z_cr_flavour != None:
                    if z_cr_deactivate_corrections:
                        _job._region = 'zcr_' + _z_cr_flavour
                    else:
                        _job._region = 'zcr_' + _z_cr_flavour + '_corr'
                if _dsid != None:
                    _job._zllqcd_dsid = str(_dsid)
                else:
                    _job._zllqcd_dsid = None

                _jobs.append(_job)

    # return list of jobs to expect
    return _jobs


def filter_jobs(
        jobs, 
        year=None, 
        channel=None, 
        process=None, 
        variation=None, 
        region=None, 
        zllqcd_dsid=None,
        corrupted=None,
        evicted=None,
        held=None,
        finished=None,
        verbose=False):
    """
    Helper function to filter a list of jobs

    Parameters
    ----------
    jobs: list(JobLog)
    year: str or list(str)
    channel: str or list(str)
    process: str or list(str)
    variation: str or list(str)
    region: str or list(str)
    corrupted: bool
    verbose: bool 

    Returns
    _______
    _jobs: list(JobLog)
    """
    _jobs = jobs
    if verbose:
        print 'initial length = {}'.format(len(_jobs))
    # year
    if year != None:
        if isinstance(year, (list, tuple)):
            _year = year
        else:
            _year = [year]
        _jobs = filter(lambda j: j.year in _year, _jobs)
        if verbose:
            print 'request year in {}: length = {}'.format(_year, len(_jobs))

    # channel
    if channel != None:
        if isinstance(channel, (list, tuple)):
            _channel = channel
        else:
            _channel = [channel]
        _jobs = filter(lambda j: j.channel in _channel, _jobs)
        if verbose:
            print 'request channel in {}: length = {}'.format(_channel, len(_jobs))

    # process
    if process != None:
        if isinstance(process, (list, tuple)):
            _process = process
        else:
            _process = [process]
        _jobs = filter(lambda j: j.process in _process, _jobs)
        if verbose:
            print 'request process in {}: length = {}'.format(_process, len(_jobs))

    # variation
    if variation != None:
        if isinstance(variation, (list, tuple)):
            _variation = variation
        else:
            _variation = [variation]
        _jobs = filter(lambda j: j.variation in _variation, _jobs)
        if verbose:
            print 'request variation in {}: length = {}'.format(_variation, len(_jobs))

    # region
    if region != None:
        if isinstance(region, (list, tuple)):
            _region = region
        else:
            _region = [region]
        _jobs = filter(lambda j: j.region in _region, _jobs)
        if verbose:
            print 'request region in {}: length = {}'.format(_region, len(_jobs))

    # zllqcd_dsid
    if zllqcd_dsid != None:
        if isinstance(zllqcd_dsid, (list, tuple)):
            _zllqcd_dsid = zllqcd_dsid
        else:
            _zllqcd_dsid = [zllqcd_dsid]
            
        _jobs = filter(lambda j: j.zllqcd_dsid in _zllqcd_dsid, _jobs)
        if verbose:
            print 'request zllqcd_dsid in {}: length = {}'.format(_zllqcd_dsid, len(_jobs))

    # corrupted
    if corrupted != None:
        _jobs = filter(lambda j: j.corrupted == corrupted, _jobs)
        if verbose:
            print 'request corruption status = {}: length = {}'.format(corrupted, len(_jobs))

    # evicted jobs
    if evicted == True:
        _jobs = filter(lambda j: j.status == 'evicted', _jobs)
        if verbose:
            print 'request evicted status: length = {}'.format(len(_jobs))

    # held jobs
    if held == True:
        _jobs = filter(lambda j: j.status == 'held', _jobs)
        if verbose:
            print 'request held status: length = {}'.format(len(_jobs))

    # finished jobs
    if finished == True:
        _jobs = filter(lambda j: j.status != 'running', _jobs)
        if verbose:
            print 'request finished status: length = {}'.format(len(_jobs))

    return _jobs


def build_jobs(_dir):
    """
    Function to loop over a directory and build the list of JobLob
    outputs
    Parameters
    ----------
    _dir: str
    
    Returns
    -------
    jobs: list(JobLog)
    """

    jobs = []
    print 'BOOM: building the list of jobs'
    for _d in os.listdir(_dir):
        if os.path.isdir(os.path.join(_dir, _d)):
            _rfile = os.listdir(os.path.join(_dir, _d))
            _info_dict = {
                'path': _dir,
                'dir': _d,
                'log': _d + '.log',
                'err': _d + '.err',
                'out': _d + '.out',}
            if len(_rfile) == 0:
                _info_dict['rfile'] = None
            elif len(_rfile) == 1:
                _info_dict['rfile'] = _rfile[0]
            else:
                print _rfile
                print _info_dict['dir']
                raise IndexError

            _err = inspect_log(_dir, _info_dict, 'err')
            _log = inspect_log(_dir, _info_dict, 'log')
            _out = inspect_log(_dir, _info_dict, 'out')
            _status = determine_job_status(_dir, _info_dict)
            _info_dict['status'] = _status
            if len(_err) !=0 or len(_log) != 0 or len(_out) != 0:
                _info_dict['msg'] = {
                    'err': _err[0].strip('\n') if len(_err) != 0 else '',
                    'out': _out[0].strip('\n') if len(_out) != 0 else '',
                    'log': _log[0].strip('\n') if len(_log) != 0 else '',
                }
                _info_dict['corrupted'] = True
            else:
                _info_dict['corrupted'] = False

            _runtime_line = _runtime(os.path.join(_dir, _d + '.log'))
            if not _info_dict['corrupted']:
                try:
                    _t_usr = _str_to_time(_runtime_line.split(' ')[2])
                    _t_sys = _str_to_time(_runtime_line.split(' ')[5])
                    _info_dict['totalruntime'] = _t_usr + _t_sys
                    _info_dict['userruntime'] = _t_usr 
                    _info_dict['sysruntime'] = _t_sys
                except:
                    _info_dict['totalruntime'] = None
                    _info_dict['userruntime'] = None
                    _info_dict['sysruntime'] = None

            _memory_line = _memory(os.path.join(_dir, _d + '.log'))
            # use regular expression to find integers
            _memory_info =  map(int, re.findall(r'\d+', _memory_line))
            if len(_memory_info) != 0:
                _info_dict['memory'] = _memory_info[0]
            else:
                _info_dict['memory'] = None
            #_memory_line.strip('\n').split(':')[-1]

            _job = JobLog(_d)
            _existing_jobs = filter(lambda j: j == _job, jobs)
            if len(_existing_jobs) == 0:
                _job.add(_info_dict)
                jobs.append(_job)
            elif len(_existing_jobs) == 1:
                _existing_jobs[0].add(_info_dict)
            else:
                raise IndexError
    print 'BOOM: ... done!'
    return jobs

