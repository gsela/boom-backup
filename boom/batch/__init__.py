"""
Subpackage holding the code to run, analyse and merge batch jobs
"""
from .definitions import PROCESS_GROUPS
from .definitions import SIGNAL_PROCESS_GROUPS


