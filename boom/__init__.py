"""
BOOM analysis package
"""
import ROOT
ROOT.gROOT.SetBatch(True)

print ''
print '------> BOOM!'
print ''

# importing cpp module (load all cpp helpers)
import cpp

# importing mva module (load and configure mva readers)
import mva

# importing and setuping embedding
from .embedding import setup_embedding
setup_embedding('NOMINAL')

from happy import scheduler
print 'BOOM: \t activate the HAPPy scheduler'
scheduler.isActive = True


