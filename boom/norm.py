"""
"""
_all__ = [
    'get_mc_norm',
]

# small helper to append the mc normalisation
from happy.systematics import Systematics, SystematicsSet
def get_mc_norm(channel, region):#, year, is_data):
    """Expression to compute the MC normalisation per tree entry

    Parameters
    __________
    channel: str
       see list in boom/cuts/__init__.py

    Returns
    _______
    weight: HAPPY Systematics.weightSystematics
       Expression to compute the MC normalisation per tree entry
    """

    if channel in ('1p1p', '1p3p', '3p1p', '3p3p'):
        _function = 'TotalWeight_hadhad'

    elif channel in ('e1p', 'e3p', 'mu1p', 'mu3p', 'ehad', 'muhad'):
        _function = 'TotalWeight_lephad'

    elif channel in ('ee', 'mumu'):
        if region.startswith('Ztt_cr'):
            _function = 'TotalWeight_zll_vr'
        else:
            _function = 'TotalWeight_leplep'

    elif channel in ('emu', 'mue'):
        _function = 'TotalWeight_leplep'

    else:
        raise NotImplementedError

    expression = 'MC_Norm::{function}(mc_channel_number, NOMINAL_pileup_random_run_number)'.format(
        function=_function)

    sys_set = SystematicsSet()
    weight = Systematics.weightSystematics(
        'mc_norm', 
        'mc_norm',
        expression,
        expression,
        expression)
    sys_set.add(weight)

    return sys_set
