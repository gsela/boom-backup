import ROOT
import os
import itertools


__all__ = [
    'ff_expr',
    'closure_expr',
#    'isofac_expr',
]

# open fake factors files
_FF_FILE_All = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/lhfake/FakeFactor_All.root")
_root_file = ROOT.TFile(_FF_FILE_All, "read")

# fake factors file for closure test
_FF_FILE_Closure_All = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/lhfake/FakeFactor_closure_All.root")
_root_file_closure = ROOT.TFile(_FF_FILE_Closure_All, "read")

# fake closure syst
_FF_FILE_Closure_Syst_All = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/lhfake/LH_closureSysts.root")
_root_file_closure_syst = ROOT.TFile(_FF_FILE_Closure_Syst_All, "read")

# mc subtraction syst
_FF_FILE_MC_Subtr_Syst_All = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/lhfake/FakeFactor_mcsubtr_All.root")
_root_file_mc_subtr_syst = ROOT.TFile(_FF_FILE_MC_Subtr_Syst_All, "read")

_ff_categories = ['Presel', 'Boosted', 'VBF', 'VH']
for ip, _cat in enumerate(_ff_categories):

    # nominal fake factors
    ROOT.lhfake_histDict.h_ff_qcd_1p[ip] = _root_file.Get("FF_QCDCR_{cat}_All_Comb_SLT_1prong".format(cat=_cat))
    ROOT.lhfake_histDict.h_ff_qcd_3p[ip] = _root_file.Get("FF_QCDCR_{cat}_All_Comb_SLT_3prong".format(cat=_cat))
    ROOT.lhfake_histDict.h_ff_wcr_1p[ip] = _root_file.Get("FF_WCR_{cat}_All_Comb_SLT_1prong".format(cat=_cat))
    ROOT.lhfake_histDict.h_ff_wcr_3p[ip] = _root_file.Get("FF_WCR_{cat}_All_Comb_SLT_3prong".format(cat=_cat))
    ROOT.lhfake_histDict.h_rqcd_1p[ip]   = _root_file.Get("RQCD_{cat}_All_Comb_SLT_1prong".format(cat=_cat))
    ROOT.lhfake_histDict.h_rqcd_3p[ip]   = _root_file.Get("RQCD_{cat}_All_Comb_SLT_3prong".format(cat=_cat))

    # stat and syst and mc subtr uncertainties
    ROOT.lhfake_histDict.h_ff_qcd_1p_stat[ip]      = _root_file.Get("FF_QCDCR_{cat}_All_Comb_SLT_1prong_stat_up".format(cat=_cat))
    ROOT.lhfake_histDict.h_ff_qcd_3p_stat[ip]      = _root_file.Get("FF_QCDCR_{cat}_All_Comb_SLT_3prong_stat_up".format(cat=_cat))
    ROOT.lhfake_histDict.h_ff_wcr_1p_stat[ip]      = _root_file.Get("FF_WCR_{cat}_All_Comb_SLT_1prong_stat_up".format(cat=_cat))
    ROOT.lhfake_histDict.h_ff_wcr_3p_stat[ip]      = _root_file.Get("FF_WCR_{cat}_All_Comb_SLT_3prong_stat_up".format(cat=_cat))
    ROOT.lhfake_histDict.h_rqcd_1p_stat[ip]        = _root_file.Get("RQCD_{cat}_All_Comb_SLT_1prong_stat_up".format(cat=_cat))
    ROOT.lhfake_histDict.h_rqcd_3p_stat[ip]        = _root_file.Get("RQCD_{cat}_All_Comb_SLT_3prong_stat_up".format(cat=_cat))
    ROOT.lhfake_histDict.h_rqcd_1p_isofac_stat[ip] = _root_file.Get("RQCD_{cat}_All_Comb_SLT_1prong_isofac_stat_up".format(cat=_cat))
    ROOT.lhfake_histDict.h_rqcd_3p_isofac_stat[ip] = _root_file.Get("RQCD_{cat}_All_Comb_SLT_3prong_isofac_stat_up".format(cat=_cat))
    ROOT.lhfake_histDict.h_rqcd_1p_isofac_syst[ip] = _root_file.Get("RQCD_{cat}_All_Comb_SLT_1prong_isofac_syst_up".format(cat=_cat))
    ROOT.lhfake_histDict.h_rqcd_3p_isofac_syst[ip] = _root_file.Get("RQCD_{cat}_All_Comb_SLT_3prong_isofac_syst_up".format(cat=_cat))
    ROOT.lhfake_histDict.h_fake_closure_1p[ip]     = _root_file_closure_syst.Get("lh_fake_closure_uncert_{cat}_1prong".format(cat=_cat))
    ROOT.lhfake_histDict.h_fake_closure_3p[ip]     = _root_file_closure_syst.Get("lh_fake_closure_uncert_{cat}_3prong".format(cat=_cat))

    ROOT.lhfake_histDict.h_ff_qcd_1p_mc_subtr[ip]  = _root_file_mc_subtr_syst.Get("FF_QCDCR_{cat}_All_Comb_SLT_1prong_mc_subtr_up".format(cat=_cat))
    ROOT.lhfake_histDict.h_ff_qcd_3p_mc_subtr[ip]  = _root_file_mc_subtr_syst.Get("FF_QCDCR_{cat}_All_Comb_SLT_3prong_mc_subtr_up".format(cat=_cat))
    ROOT.lhfake_histDict.h_ff_wcr_1p_mc_subtr[ip]  = _root_file_mc_subtr_syst.Get("FF_WCR_{cat}_All_Comb_SLT_1prong_mc_subtr_up".format(cat=_cat))
    ROOT.lhfake_histDict.h_ff_wcr_3p_mc_subtr[ip]  = _root_file_mc_subtr_syst.Get("FF_WCR_{cat}_All_Comb_SLT_3prong_mc_subtr_up".format(cat=_cat))
    ROOT.lhfake_histDict.h_rqcd_1p_mc_subtr[ip]    = _root_file_mc_subtr_syst.Get("RQCD_{cat}_All_Comb_SLT_1prong_mc_subtr_up".format(cat=_cat))
    ROOT.lhfake_histDict.h_rqcd_3p_mc_subtr[ip]    = _root_file_mc_subtr_syst.Get("RQCD_{cat}_All_Comb_SLT_3prong_mc_subtr_up".format(cat=_cat))

    # map for fake factor closure test
    ROOT.lhfake_histDict.h_ff_qcd_1p_closure[ip] = _root_file_closure.Get("FF_QCDCR_{cat}_All_Comb_SLT_1prong".format(cat=_cat))
    ROOT.lhfake_histDict.h_ff_qcd_3p_closure[ip] = _root_file_closure.Get("FF_QCDCR_{cat}_All_Comb_SLT_3prong".format(cat=_cat))
    ROOT.lhfake_histDict.h_ff_wcr_1p_closure[ip] = _root_file_closure.Get("FF_WCR_{cat}_All_Comb_SLT_1prong".format(cat=_cat))
    ROOT.lhfake_histDict.h_ff_wcr_3p_closure[ip] = _root_file_closure.Get("FF_WCR_{cat}_All_Comb_SLT_3prong".format(cat=_cat))
    ROOT.lhfake_histDict.h_rqcd_1p_closure[ip]   = _root_file_closure.Get("RQCD_{cat}_All_Comb_SLT_1prong".format(cat=_cat))
    ROOT.lhfake_histDict.h_rqcd_3p_closure[ip]   = _root_file_closure.Get("RQCD_{cat}_All_Comb_SLT_3prong".format(cat=_cat))

def _determine_ff_version(category):
    """
    Small helper to infer the category used for the fake-factor derivation
    from the category we are trying to apply it to
    """
    if 'boost' in category:
        _cat = 'Boosted'
    elif 'vbf' in category:
        _cat = 'VBF'
    elif 'vh' in category:
        _cat = 'VH'
    elif category == 'preselection':
        _cat = 'Presel'
    else:
        raise NotImplementedError
    return _cat

def ff_expr(category, region, syst):
    
    do_closure = 0
    if 'same_sign' in region :     
        do_closure = 1
   
    _ff_version = _determine_ff_version(category)
    if 'qcd_lh' in region:
        expr = 'FakeHelper::read_ff_qcd({0}, {1}, {2}, {3}, {4})'.format(
            'tau_0_p4.Pt()', 
            'tau_0_n_charged_tracks',
            _ff_categories.index(_ff_version),
            str(getattr(ROOT.lhfake_histDict, syst)),
            do_closure)
    elif region in ('top','W_lh'):
        expr = 'FakeHelper::read_ff_wcr({0}, {1}, {2}, {3}, {4})'.format(
            'tau_0_p4.Pt()', 
            'tau_0_n_charged_tracks',
            _ff_categories.index(_ff_version),
            str(getattr(ROOT.lhfake_histDict, syst)),
            do_closure)
    else:
        expr = 'FakeHelper::weight({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8})'.format(
            'tau_0_p4.Pt()', 
            'tau_0_n_charged_tracks',
            'tau_0_p4.Phi()',
            'tau_1_p4.Pt()',
            'tau_1_p4.Eta()',
            'met_p4.Phi()',
            _ff_categories.index(_ff_version),
	    str(getattr(ROOT.lhfake_histDict, syst)),
            do_closure)

    return expr


def closure_expr(category, region, syst):

     _ff_version = _determine_ff_version(category)
     clos_syst_value = 'FakeHelper::closuresyst({0}, {1}, {2}, {3})'.format(
            'ditau_mmc_mlm_m',
            'tau_0_n_charged_tracks',
            _ff_categories.index(_ff_version),
            str(getattr(ROOT.lhfake_histDict, syst)))
     return clos_syst_value
  

#iso factor part 
_IF_FILE_All = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/lhfake/IsoFactor_lephad.root")
_if_root_file = ROOT.TFile(_IF_FILE_All, "read")

_IF_FILE_CLOSURE_All = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/lhfake/IsoFactor_closure_lephad.root")
_if_root_file_closure = ROOT.TFile(_IF_FILE_CLOSURE_All, "read")  

ROOT.isofactor_histDict.h_nominal_1prong[0]   = _if_root_file.Get("Iso_Factor_Presel_All_Comb_SLT_1prong")
ROOT.isofactor_histDict.h_stat_1prong_up[0]   = _if_root_file.Get("Iso_Factor_Presel_All_Comb_SLT_1prong_stat_up")
ROOT.isofactor_histDict.h_stat_1prong_down[0] = _if_root_file.Get("Iso_Factor_Presel_All_Comb_SLT_1prong_stat_down")
ROOT.isofactor_histDict.h_syst_1prong_up[0]   = _if_root_file.Get("Iso_Factor_Presel_All_Comb_SLT_1prong_syst_up")
ROOT.isofactor_histDict.h_syst_1prong_down[0] = _if_root_file.Get("Iso_Factor_Presel_All_Comb_SLT_1prong_syst_down")
ROOT.isofactor_histDict.h_nominal_3prong[0]   = _if_root_file.Get("Iso_Factor_Presel_All_Comb_SLT_3prong")
ROOT.isofactor_histDict.h_stat_3prong_up[0]   = _if_root_file.Get("Iso_Factor_Presel_All_Comb_SLT_3prong_stat_up")
ROOT.isofactor_histDict.h_stat_3prong_down[0] = _if_root_file.Get("Iso_Factor_Presel_All_Comb_SLT_3prong_stat_down")
ROOT.isofactor_histDict.h_syst_3prong_up[0]   = _if_root_file.Get("Iso_Factor_Presel_All_Comb_SLT_3prong_syst_up")
ROOT.isofactor_histDict.h_syst_3prong_down[0] = _if_root_file.Get("Iso_Factor_Presel_All_Comb_SLT_3prong_syst_down")

ROOT.isofactor_histDict.h_nominal_closure_1prong[0]   = _if_root_file_closure.Get("Iso_Factor_Presel_All_Comb_SLT_1prong")
ROOT.isofactor_histDict.h_stat_closure_1prong_up[0]   = _if_root_file_closure.Get("Iso_Factor_Presel_All_Comb_SLT_1prong_stat_up")
ROOT.isofactor_histDict.h_stat_closure_1prong_down[0] = _if_root_file_closure.Get("Iso_Factor_Presel_All_Comb_SLT_1prong_stat_down")
ROOT.isofactor_histDict.h_syst_closure_1prong_up[0]   = _if_root_file_closure.Get("Iso_Factor_Presel_All_Comb_SLT_1prong_syst_up")
ROOT.isofactor_histDict.h_syst_closure_1prong_down[0] = _if_root_file_closure.Get("Iso_Factor_Presel_All_Comb_SLT_1prong_syst_down")
ROOT.isofactor_histDict.h_nominal_closure_3prong[0]   = _if_root_file_closure.Get("Iso_Factor_Presel_All_Comb_SLT_3prong")
ROOT.isofactor_histDict.h_stat_closure_3prong_up[0]   = _if_root_file_closure.Get("Iso_Factor_Presel_All_Comb_SLT_3prong_stat_up")
ROOT.isofactor_histDict.h_stat_closure_3prong_down[0] = _if_root_file_closure.Get("Iso_Factor_Presel_All_Comb_SLT_3prong_stat_down")
ROOT.isofactor_histDict.h_syst_closure_3prong_up[0]   = _if_root_file_closure.Get("Iso_Factor_Presel_All_Comb_SLT_3prong_syst_up")
ROOT.isofactor_histDict.h_syst_closure_3prong_down[0] = _if_root_file_closure.Get("Iso_Factor_Presel_All_Comb_SLT_3prong_syst_down")

def isofac_expr(syst, do_closure=False):
    """
    """
    expr = 'IsoFactorHelper::get_isofactor({0}, {1}, {2}, {3}, {4}, {5})'.format(
        'tau_1_p4.Pt()', 
        'tau_1_p4.Eta()',
        'tau_0_n_charged_tracks',
        '0',
        str(int(do_closure)),
        str(getattr(ROOT.isofactor_histDict, syst)))
        
    return expr


#  scale factors related to the eVeto
_EVETO_SF_FILE = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), "../data/eveto_sf/EvetoSF_decaymode_updated.root")
_eveto_sf_root_file = ROOT.TFile(_EVETO_SF_FILE, 'read')

# loading scale factors related to the eVeto
print('BOOM: \t embedding --> loading sf_eleBDTMedium_r1p0n')
ROOT.evetofactor_histDict.h_sf_eveto['sf_eleBDTMedium_r1p0n'] = _eveto_sf_root_file.Get("sf_eleBDTMedium_r1p0n")

print('BOOM: \t embedding --> loading sf_eleBDTMedium_r1p1n')
ROOT.evetofactor_histDict.h_sf_eveto['sf_eleBDTMedium_r1p1n'] = _eveto_sf_root_file.Get("sf_eleBDTMedium_r1p1n")

print('BOOM: \t embedding --> loading sf_eleBDTMedium_r1pXn')
ROOT.evetofactor_histDict.h_sf_eveto['sf_eleBDTMedium_r1pXn'] = _eveto_sf_root_file.Get("sf_eleBDTMedium_r1pXn")


# gal: upload FF maps for anti tau:
_tau_ff_file_path = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), "../data/ff_maps/FF_tight_V05_all.root")
_tau_ff_file = ROOT.TFile(_tau_ff_file_path, 'read')

_tau_R_file_path = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), "../data/ff_maps/R_tight_V05_all.root")
_tau_R_file = ROOT.TFile(_tau_R_file_path, 'read')

# loading scale factors related to the eVeto
print('BOOM: \t gal embedding --> loading FF and R maps')
ROOT.evetofactor_histDict.h_sf_ff['FF_QCDmap_nonVBF_e1p'] = _tau_ff_file.Get("etau_QCD_nonVBF_1Prong")
ROOT.evetofactor_histDict.h_sf_ff['FF_QCDmap_nonVBF_e3p'] = _tau_ff_file.Get("etau_QCD_nonVBF_3Prong")
ROOT.evetofactor_histDict.h_sf_ff['FF_QCDmap_nonVBF_mu1p'] = _tau_ff_file.Get("mutau_QCD_nonVBF_1Prong")
ROOT.evetofactor_histDict.h_sf_ff['FF_QCDmap_nonVBF_mu3p'] = _tau_ff_file.Get("mutau_QCD_nonVBF_3Prong")

ROOT.evetofactor_histDict.h_sf_ff['FF_Wmap_nonVBF_e1p'] = _tau_ff_file.Get("etau_W_nonVBF_1Prong")
ROOT.evetofactor_histDict.h_sf_ff['FF_Wmap_nonVBF_e3p'] = _tau_ff_file.Get("etau_W_nonVBF_3Prong")
ROOT.evetofactor_histDict.h_sf_ff['FF_Wmap_nonVBF_mu1p'] = _tau_ff_file.Get("mutau_W_nonVBF_1Prong")
ROOT.evetofactor_histDict.h_sf_ff['FF_Wmap_nonVBF_mu3p'] = _tau_ff_file.Get("mutau_W_nonVBF_3Prong")

ROOT.evetofactor_histDict.h_sf_ff['R_QCDmap_etau'] = _tau_R_file.Get("etau_QCD_nonVBF")
ROOT.evetofactor_histDict.h_sf_ff['R_QCDmap_mutau'] = _tau_R_file.Get("mutau_QCD_nonVBF")

# gal: upload FF maps for anti lep:
_lep_ff_file_path = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), "../data/ff_maps/fakeFactor_mattias.root")
_lep_ff_file = ROOT.TFile(_lep_ff_file_path, 'read')

ROOT.evetofactor_histDict.h_sf_ff_lep['FF_lep_el_map'] = _lep_ff_file.Get("FakeFactor2D_el_pt_eta")
ROOT.evetofactor_histDict.h_sf_ff_lep['FF_lep_mu_map'] = _lep_ff_file.Get("FakeFactor2D_mu_pt_eta")