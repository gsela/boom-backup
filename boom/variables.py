"""Definition of the variables to plot (VARIABLES dict)

We use HAPPy Variables to define the variable to plot.
The third argument in the constructor is the actual expression 
being evaluated (from the ntuples branches).
To add a variable, slot it in at the bottom of the dictionnary following:

     'key': Variable(
        'name', # user-defined

        'expr', # ntuple branch name or any TTreeFormula-able expression

        'title', # x-axis title on the plot (w/o units)

        '', # units - empty string '' if dimensionless

        Binning(nbins, xmin, xmax) # or any supported use of HAPPy.Binning

        ), 
"""
import ROOT
from happy.variable import Binning, Variable, VariableBinning
from .mva_expr import pt_jj_expr, vbf_tagger_expr, dr_jj_expr, dr_dijetditau_expr
from .mva_expr import vh_tagger_expr
from .mva_expr import tth_tt_expr, tth_Z_expr
from .zll_pt import _random_offset, tau_0_4vec, tau_1_4vec, tau_0_m_4vec, tau_1_m_4vec, met_4vec
from .zll_pt import tau_0_pt_corr, tau_1_pt_corr
from .zll_pt import x0_corr, x1_corr, dr_corr, deta_corr, met_corr
from .zll_pt import vh_tagger_corr_expr
from .zll_pt import jet_0_tau_0_dr_corr, jet_0_tau_1_dr_corr, jet_1_tau_0_dr_corr, jet_1_tau_1_dr_corr
from .zll_pt import m_coll_corr, ditau_m_corr

binningPi   = Binning(16, 0, ROOT.TMath.Pi())
binningPhi  = Binning(32, -ROOT.TMath.Pi(), ROOT.TMath.Pi())
binningPhiCoarse  = Binning(10, -ROOT.TMath.Pi(), ROOT.TMath.Pi())
binning2Pi  = Binning(45, 0, ROOT.TMath.TwoPi())

VARIABLES = {
    'norm': Variable(
        'norm', 
        '1', 
        'norm', 
        '', 
        Binning(1, 0, 2)),

    'mva_random': Variable(
        'mva_random',
        'Rnd::mva_random_number(event_number)',
        'MVA Random Number',
        '',
        Binning(10, 0, 1)),

    'tau_0_eta': Variable(
        'tau_0_eta', 
        'tau_0_p4.Eta()', 
        'leading #eta', 
        '', 
        Binning(25, -2.5, 2.5)),

    'abs_tau_1_eta': Variable(
        'abs_tau_1_eta', 
        'fabs(tau_1_p4.Eta())', 
        '|subleading #eta(#tau)|', 
        '',
        Binning(25, 0, 2.5)),

    'tau_0_phi': Variable(
        'tau_0_phi', 
        'tau_0_p4.Phi()', 
        'leading #phi', 
        '', 
        binningPhi),

    'tau_0_d0': Variable(
        'tau_0_d0',
        'tau_0_trk_d0',
        'Leading lepton d0',
        'mm',
        Binning(40, -0.1, 0.1)),

    'tau_0_topoetcone40': Variable(
        'tau_0_topoetcone40',
        'tau_0_iso_topoetcone40 / 1000.',
        'Leading lepton Isolation (TopoEtCone40)',
        'GeV',
        Binning(40, -5, 10)),

    'tau_1_topoetcone40': Variable(
        'tau_1_topoetcone40',
        'tau_1_iso_topoetcone40 / 1000.',
        'Subleading lepton Isol (TEtC40)',
        'GeV',
        Binning(40, -5, 10)),

    'tau_0_d0_sig': Variable(
        'tau_0_d0_sig',
        'tau_0_trk_d0_sig',
        'leading lepton d_{0} significance',
        '',
        Binning(40, -5, 5)),

    'tau_1_d0_sig': Variable(
        'tau_1_d0_sig',
        'tau_1_trk_d0_sig',
        'subleading lepton d_{0} significance',
        '',
        Binning(40, -5, 5)),

    'tau_1_d0': Variable(
        'tau_1_d0',
        'tau_1_trk_d0',
        'Subleading lepton d0',
        'mm',
        Binning(40, -0.1, 0.1)),

    'tau_0_tight': Variable(
        'tau_0_tight',
        'tau_0_id_tight',
        'Leading lepton Tight ID decision',
        '',
        Binning(2, 0, 2)),

    'tau_1_tight': Variable(
        'tau_1_tight',
        'tau_1_id_tight',
        'Subleading lepton Tight ID decision',
        '',
        Binning(2, 0, 2)),

    'tau_0_pt': Variable(
        'tau_0_pt', 
        'tau_0_p4.Pt()', 
        'leading p_{T}', 
        'GeV', 
        Binning(20, 20, 220)),

    'tau_had_pt': Variable(
        'tau_had_pt', 
        'tau_0_p4.Pt()', 
        'Hadronic Tau p_{T}', 
        'GeV', 
        Binning(50, 0, 250)),

    'tau_had_eta': Variable(
        'tau_had_eta', 
        'tau_0_p4.Eta()', 
        'Hadronic Tau #eta', 
        '', 
        Binning(25, -2.5, 2.5)),

    'tau_had_phi': Variable(
        'tau_had_phi', 
        'tau_0_p4.Phi()', 
        'Hadronic Tau #phi', 
        '', 
        binningPhi),

    'lepton_pt': Variable(
        'lepton_pt', 
        'tau_1_p4.Pt()', 
        'Light Lepton p_{T}', 
        'GeV', 
        Binning(50, 0, 250)),

    'lepton_eta': Variable(
        'lepton_eta', 
        'tau_1_p4.Eta()', 
        'Light Lepton #eta', 
        '', 
        Binning(25, -2.5, 2.5)),

    'lepton_phi': Variable(
        'lepton_phi', 
        'tau_1_p4.Phi()', 
        'Light Lepton #phi', 
        '', 
        binningPhi),

    'electron_pt': Variable(
        'electron_pt', 
        '(tau_0 == 2)*tau_0_p4.Pt() + (tau_1 == 2)*tau_1_p4.Pt()', 
        'Electron p_{T}', 
        'GeV', 
        Binning(160, 0, 160)),
    
    'electron_eta': Variable(
        'electron_eta', 
        '(tau_0 == 2)*tau_0_p4.Eta() + (tau_1 == 2)*tau_1_p4.Eta()', 
        'Electron #eta', 
        '', 
        Binning(50, -2.5, 2.5)),

    'electron_phi': Variable(
        'electron_phi', 
        '(tau_0 == 2)*tau_0_p4.Phi() + (tau_1 == 2)*tau_1_p4.Phi()', 
        'Electron #phi', 
        '', 
        binningPhi),

    'muon_pt': Variable(
        'muon_pt', 
        '(tau_0 == 1)*tau_0_p4.Pt() + (tau_1 == 1)*tau_1_p4.Pt()', 
        'Muon p_{T}', 
        'GeV', 
        Binning(80, 0, 160)),

    'muon_eta': Variable(
        'muon_eta', 
        '(tau_0 == 1)*tau_0_p4.Eta() + (tau_1 == 1)*tau_1_p4.Eta()', 
        'Muon #eta', 
        '', 
        Binning(50, -2.5, 2.5)),

    'muon_phi': Variable(
        'muon_phi', 
        '(tau_0 == 1)*tau_0_p4.Phi() + (tau_1 == 1)*tau_1_p4.Phi()', 
        'Muon #phi', 
        '', 
        binningPhi),

    'diff_pt': Variable(
        'diff_pt', 
        'tau_0_p4.Pt() - tau_1_p4.Pt()', 
        'leading - subleading p_{T}', 
        'GeV', 
        Binning(8, -20, 20)),

    'tau_0_pt_tauid_binning': Variable(
        'tau_0_pt_tauid_binning', 
        'tau_0_p4.Pt()', 
        'leading p_{T}', 
        'GeV', 
        VariableBinning([30, 40, 100, 200])),

    'tau_0_pt_coarse': Variable(
        'tau_0_pt_coarse', 
        'tau_0_p4.Pt()', 
        'leading p_{T}', 
        'GeV', 
        VariableBinning([40, 50, 60, 70, 80, 90, 100, 200])),

    'tau_0_truth_pt': Variable(
        'tau_0_truth_pt', 
        'tau_0_truth_p4.Pt()', 
        'p_{T}(truth #tau)', 
        'GeV', 
        Binning(40, 0, 200)),

    'tau_0_n_charged_tracks': Variable(
        'tau_0_n_charged_tracks', 
        'tau_0_n_charged_tracks', 
        'Number of Charged Tracks', 
        '',
        Binning(5, 0, 5)),

    'tau_1_eta': Variable(
        'tau_1_eta', 
        'tau_1_p4.Eta()', 
        'subleading #eta', 
        '',
        Binning( 25, -2.5, 2.5)),

    'tau_1_phi': Variable(
        'tau_1_phi', 
        'tau_1_p4.Phi()', 
        'subleading #phi', 
        '', 
        binningPhi),

    'tau_1_pt': Variable(
        'tau_1_pt', 
        'tau_1_p4.Pt()', 
        'subleading p_{T}', 
        'GeV', 
        Binning(20, 0, 100)),

    'tau_1_pt_coarse': Variable(
        'tau_1_pt_coarse', 
        'tau_1_p4.Pt()', 
        'subleading p_{T}', 
        'GeV', 
        VariableBinning([30, 40, 50, 60, 200])),

    'tau_1_truth_pt': Variable(
        'tau_1_truth_pt', 
        'tau_1_truth_p4.Pt()', 
        'subleading truth p_{T}', 
        'GeV', 
        Binning(40, 0, 200)),

    'tau_1_n_charged_tracks': Variable(
        'tau_1_n_charged_tracks', 
        'tau_1_n_charged_tracks', 
        'Number of Charged Tracks', 
        '',
        Binning(5, 0, 5)),

    'mmc_mlm_m': Variable(
        'ditau_mmc_mlm_m', 
        'ditau_mmc_mlm_m', 
        'MMC (mlm) Mass Estimator', 
        'GeV', 
        Binning(20, 0, 200)),

    'mmc_mlm_m_fine_binning': Variable(
        'ditau_mmc_mlm_m', 
        'ditau_mmc_mlm_m', 
        'MMC (mlm) Mass Estimator', 
        'GeV', 
        Binning(40, 0, 200)),

    'mmc_mlm_m_timo': Variable(
        'ditau_mmc_mlm_m', 
        'ditau_mmc_mlm_m', 
        'MMC (mlm) Mass Estimator', 
        'GeV', 
        Binning(36, 0, 180)),

    'mmc_mlm_m_closure': Variable(
        'ditau_mmc_mlm_m_closure',
        'ditau_mmc_mlm_m',
        'MMC (mlm) Mass Estimator',
        'GeV',
        VariableBinning([0, 70, 100, 150, 200])),

    'colinear_mass': Variable(
        'ditau_coll_approx_m', 
        'ditau_coll_approx_m', 
        'Colinear Mass', 
        'GeV', 
        Binning(25, 0, 250)),

    'colinear_mass_lfv': Variable(
        'coll_approx_lfv_m', 
        'coll_approx_lfv_m', 
        'Colinear Mass LFV', 
        'GeV', 
        Binning(25, 0, 250)),

    'jet_0_eta': Variable(
        'jet_0_eta',  
        'jet_0_p4.Eta()', 
        'Leading Jet #eta', 
        '',   
        Binning(40, -5, 5)),

    'jet_1_eta': Variable(
        'jet_1_eta', 
        'jet_1_p4.Eta()', 
        'Subleading Jet #eta', 
        '', 
        Binning(40, -5, 5)),

    'jet_0_pt_low': Variable(
        'jet_0_pt_low', 
        'jet_0_p4.Pt()', 
        'Leading Jet p_{T}', 
        'GeV', 
        Binning(20, 0, 200)),

    'jet_0_pt': Variable(
        'jet_0_pt', 
        'jet_0_p4.Pt()', 
        'Leading Jet p_{T}', 
        'GeV', 
        Binning(31, 40, 350)),

    'jet_1_pt': Variable(
        'jet_1_pt', 
        'jet_1_p4.Pt()', 
        'Subleading Jet p_{T}', 
        'GeV', 
        Binning(20, 0, 200)),

    'jet_2_pt': Variable(
        'jet_2_pt', 
        'jet_2_p4.Pt()', 
        'Third Jet p_{T}', 
        'GeV', 
        Binning(15, 0, 150)),

    'jet_0_jvt': Variable(
        'jet_0_jvt', 
        'jet_0_jvt', 
        'Leading Jet JVT', 
        '', 
        Binning(30, 0.97, 1.0)),

    'jets_dphi': Variable(
        'delta_phi_jj', 
        'abs(TVector2::Phi_mpi_pi(jet_0_p4.Phi() -jet_1_p4.Phi()))', 
        '|#Delta#phi(j_{0}, j_{1})|', 
        '',
        Binning(16, 0, ROOT.TMath.Pi())),

    'jets_deta': Variable(
        'dijet_deta', 
        'fabs(jet_0_p4.Eta()-jet_1_p4.Eta())', 
        '|#Delta#eta(jet_{0}, jet_{1})|', 
        '', 
        Binning(20, 3, 7)),

    'jets_deta_wide': Variable(
        'dijet_deta_wide', 
        'fabs(jet_0_p4.Eta()-jet_1_p4.Eta())', 
        '|#Delta#eta(jet_{0}, jet_{1})|', 
        '', 
        Binning(14, 0, 7)),

    'jets_eta_prod_wide': Variable(
        'jet_eta_prod_wide',
        'jet_0_p4.Eta() * jet_1_p4.Eta()', 
        '#eta(jet_{0}) #times #eta(jet_{1})', 
        '',
        Binning(40,-10, 10)),

    'jets_eta_prod': Variable(
        'jet_eta_prod',
        'jet_0_p4.Eta() * jet_1_p4.Eta()', 
        '#eta(jet_{0}) #times #eta(jet_{1})', 
        '',
        Binning(20,-10, 0)),

    'jets_dr': Variable(
        'dijet_dr',
        dr_jj_expr,
        '#DeltaR(j_{0}, j_{1})',
        '',
        Binning(10, 0, 5)),
    
    'dijetditau_dr': Variable(
        'dijetditau_dr',
        dr_dijetditau_expr,
        '#DeltaR(dijet, ditau)',
        '',
        Binning(30, 0, 6)),

    'dijet_pt': Variable(
        'dijet_pt', 
        'dijet_p4.Pt()', 
        'p_{T}(jj)', 
        'GeV', 
        Binning(50, 0, 500)),

    'mjj': Variable(
        'dijet_m', 
        'dijet_p4.M()', 
        'm_{jj}', 
        'GeV', 
        Binning(20, 250, 2250)),

    'mjj_low': Variable(
        'dijet_m_low', 
        'dijet_p4.M()', 
        'm_{jj}', 
        'GeV', 
        Binning(20, 0, 1000)),

    'mjj_very_low': Variable(
        'dijet_m_low', 
        'dijet_p4.M()', 
        'm_{jj}', 
        'GeV', 
        Binning(40, 0, 400)),

    'mjj_vh': Variable(
        'dijet_m_vh', 
        'dijet_p4.M()', 
        'm_{jj}', 
        'GeV', 
        Binning(8, 60, 120)),

    'n_jets': Variable( 
        'n_jets', 
        'n_jets', 
        'N_{jets}', 
        '',    
        Binning(11, 0, 11, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'])),
        
    'n_jets_30': Variable( 
        'n_jets_30', 
        'n_jets_30', 
        'N_{jets} (p_{T} > 30 GeV)', 
        '',   
        Binning(11, 0, 11, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'])),

    'n_bjets_DL1r_FixedCutBEff_70' : Variable(
        'n_bjets_DL1r_FixedCutBEff_70',
        'n_bjets_DL1r_FixedCutBEff_70',
        'N_{b jets} n_bjets_DL1r_FixedCutBEff_70',
        '',
        Binning(6, 0, 6, [ '0', '1', '2', '3', '4', '5' ])),

    'vbf_hh_triangle': Variable(
        'vbf_hh_triangle',
        'dijet_p4.() + 250 * abs(jet_0_p4.Eta()-jet_1_p4.Eta()) - 1550',
        'm_{jj} + 250 #times |#Delta#eta(j, j)| - 1550',
        'GeV',
        Binning(24, -400, 2000)),

    'var_ptjj': Variable( 
        'pt_jets', 
        pt_jj_expr,
        'p^{jj}_{T}',  
        'GeV', 
        Binning(20, 40, 240)),  

    'dijet_ditau_pt_balance': Variable(
        'dijet_ditau_pt_balance',
        'ditau_p4.Pt() / {}'.format(pt_jj_expr),
        'p_{T}(ll) / p_{T}(jj)',
        '',
        Binning(20, 0, 2)),

    'ditau_rapidity': Variable(
        'ditau_rapidity',
        'ditau_p4.Rapidity()',
        'Dilepton System Rapidity',
        '',
        Binning(50, -2.5, 2.5)),

    'ditau_jet_0_delta_rapidity': Variable(
        'ditau_jet_0_delta_rapidity',
        'fabs(ditau_p4.Rapidity() - jet_0_p4.Rapidity())',
        '|Y(ll) - Y(j_{0})|',
        '',
        Binning(20, 0, 10)),

    'ditau_jet_1_delta_rapidity': Variable(
        'ditau_jet_1_delta_rapidity',
        'fabs(ditau_p4.Rapidity() - jet_1_p4.Rapidity())',
        '|Y(ll) - Y(j_{1})|',
        '',
        Binning(20, 0, 10)),

    'ditau_jet_2_delta_rapidity': Variable(
        'ditau_jet_2_delta_rapidity',
        'fabs(ditau_p4.Rapidity() - jet_2_p4.Rapidity())',
        '|Y(ll) - Y(j_{2})|',
        '',
        Binning(20, 0, 10)),

    'ditau_jet_0_deta': Variable(
        'ditau_jet_0_deta',
        'fabs(ditau_p4.Eta() - jet_0_p4.Eta())',
        '|#eta(ll) - #eta(j_{0})|',
        '',
        Binning(20, 0, 10)),

    'ditau_jet_1_deta': Variable(
        'ditau_jet_1_deta',
        'fabs(ditau_p4.Eta() - jet_1_p4.Eta())',
        '|#eta(ll) - #eta(j_{1})|',
        '',
        Binning(20, 0, 10)),

    'ditau_jet_2_deta': Variable(
        'ditau_jet_2_deta',
        'fabs(ditau_p4.Eta() - jet_2_p4.Eta())',
        '|#eta(ll) - #eta(j_{2})|',
        '',
        Binning(20, 0, 10)),


    'tau_0_rnn_score':  Variable(
        'tau_0_jet_rnn_score_trans',
        'tau_0_jet_rnn_score_trans',
        '#tau RNN score',
        '',
        Binning(50, 0, 1)),

    'tau_0_rnn_score_low':  Variable(
        'tau_0_jet_rnn_score_trans_low',
        'tau_0_jet_rnn_score_trans',
        '#tau RNN score',
        '',
        Binning(45, 0, 0.45)),

    'tau_0_rnn_score_coarse':  Variable(
        'tau_0_jet_rnn_score_trans_coarse',
        'tau_0_jet_rnn_score_trans',
        '#tau RNN score',
        '',
        VariableBinning([0.005, 0.007, 0.008, 0.009, 0.01, 0.02, 0.05, 0.1, 0.2, 0.3, 0.5])),
    
    'met': Variable(
        'met_reco_et', 
        'met_p4.Pt()', 
        'MET E_{t}', 
        'GeV', 
        Binning(20, 0, 200)),

    'met_fine': Variable(
        'met_p4.Pt()', 
        'met_p4.Pt()', 
        'MET E_{t}', 
        'GeV', 
        Binning(40, 0, 200)),

    #gal:
    'met_scalar': Variable(
        'sum all pt', 
        'scalar_sum_pt+tau_1_p4.Pt()', 
        'MET manually added', 
        'GeV', 
        Binning(40, 0, 200)),

    'tau_bdt': Variable(
        'tau_bdt', 
        'tau_0_ele_bdt_score_trans_retuned', 
        'tau_0_ele_bdt_score_trans_retuned', 
        '', 
        Binning(20, 0, 1)),

    #gal:
    'met_manual_pt': Variable(
        'manual_met', 
        'PtCorrHelper::manual_met(tau_0_p4.Pt(), tau_0_p4.Eta(),tau_0_p4.Phi(),tau_1_p4.Pt(), tau_1_p4.Eta(),tau_1_p4.Phi(), jet_0_p4.Pt(), jet_0_p4.Eta(), jet_0_p4.Phi() ,jet_1_p4.Pt(), jet_1_p4.Eta(), jet_1_p4.Phi() ,jet_2_p4.Pt(), jet_2_p4.Eta(),jet_2_p4.Phi(), n_jets)', 
        'Visible Pt Sum', 
        'GeV', 
        Binning(40, 0, 200)),

    'f_met_manual_pt': Variable(
        'manual_met', 
        'manual_met.Pt()', 
        'Visible Pt Sum', 
        'GeV', 
        Binning(40, 0, 200)),


    'met_manual_phi': Variable(
        'manual_met_phi', 
        'PtCorrHelper::manual_met_phi(tau_0_p4.Pt(), tau_0_p4.Eta(),tau_0_p4.Phi(),tau_1_p4.Pt(), tau_1_p4.Eta(),tau_1_p4.Phi(), jet_0_p4.Pt(), jet_0_p4.Eta(), jet_0_p4.Phi() ,jet_1_p4.Pt(), jet_1_p4.Eta(), jet_1_p4.Phi() ,jet_2_p4.Pt(), jet_2_p4.Eta(),jet_2_p4.Phi(), n_jets)',
        'manual met phi', 
        '',
        binningPhiCoarse),

    'f_met_manual_phi': Variable(
        'manual_met_phi', 
        'manual_met.Phi()',
        'manual met phi', 
        '',
        binningPhiCoarse),

    'manual_met_cos': Variable(
        'manual_met_cos', 
        'PtCorrHelper::manual_met_cos(tau_0_p4.Pt(), tau_0_p4.Eta(),tau_0_p4.Phi(),tau_1_p4.Pt(), tau_1_p4.Eta(),tau_1_p4.Phi(), jet_0_p4.Pt(), jet_0_p4.Eta(), jet_0_p4.Phi() ,jet_1_p4.Pt(), jet_1_p4.Eta(), jet_1_p4.Phi() ,jet_2_p4.Pt(), jet_2_p4.Eta(),jet_2_p4.Phi(), n_jets)',
        'manual met phi', 
        '',
        Binning(40, -1, 1)),

    'met_cos': Variable(
        'met_cos', 
        'ditau_met_sum_cos_dphi',
        'met cos', 
        '',
        Binning(40, -1, 1)),


    'met_manual_pt_plus_soft': Variable(
        'manual_met_plus_soft', 
        'met_more_met_et_soft + PtCorrHelper::manual_met(tau_0_p4.Pt(), tau_0_p4.Eta(),tau_0_p4.Phi(),tau_1_p4.Pt(), tau_1_p4.Eta(),tau_1_p4.Phi(), jet_0_p4.Pt(), jet_0_p4.Eta(), jet_0_p4.Phi() ,jet_1_p4.Pt(), jet_1_p4.Eta(), jet_1_p4.Phi() ,jet_2_p4.Pt(), jet_2_p4.Eta(),jet_2_p4.Phi(), n_jets)', 
        'Visible Pt Sum plus soft', 
        'GeV', 
        Binning(40, 0, 200)),

    'met_manual_trans_tau': Variable(
        'manual_met_tau_transverse_mass', 
        'PtCorrHelper::manual_met_trans_tau(tau_0_p4.Pt(), tau_0_p4.Eta(),tau_0_p4.Phi(),tau_1_p4.Pt(), tau_1_p4.Eta(),tau_1_p4.Phi(), jet_0_p4.Pt(), jet_0_p4.Eta(), jet_0_p4.Phi() ,jet_1_p4.Pt(), jet_1_p4.Eta(), jet_1_p4.Phi() ,jet_2_p4.Pt(), jet_2_p4.Eta(),jet_2_p4.Phi(), n_jets)', 
        'Transverse mass manual met - tau', 
        'GeV', 
        Binning(25, 0, 250)),

    'f_met_manual_trans_tau': Variable(
        'manual_met_tau_transverse_mass', 
        'manual_mt_met_tau', 
        'Transverse mass manual met - tau', 
        'GeV', 
        Binning(25, 0, 250)),

    'f_met_manual_trans_lep': Variable(
        'manual_met_lep_transverse_mass', 
        'manual_mt_met_lep', 
        'Transverse mass manual met - lep', 
        'GeV', 
        Binning(25, 0, 250)),

    'met_manual_trans_lep': Variable(
        'manual_met_lep_transverse_mass', 
        'PtCorrHelper::manual_met_trans_lep(tau_0_p4.Pt(), tau_0_p4.Eta(),tau_0_p4.Phi(),tau_1_p4.Pt(), tau_1_p4.Eta(),tau_1_p4.Phi(), jet_0_p4.Pt(), jet_0_p4.Eta(), jet_0_p4.Phi() ,jet_1_p4.Pt(), jet_1_p4.Eta(), jet_1_p4.Phi() ,jet_2_p4.Pt(), jet_2_p4.Eta(),jet_2_p4.Phi(), n_jets)', 
        'Transverse mass manual met - lep', 
        'GeV', 
        Binning(25, 0, 250)),

    'met_manual_coll': Variable(
        'manual_met_coll_mass', 
        'manual_coll', 
        'Collinear mass manual met', 
        'GeV', 
        Binning(25, 0, 250)),

    'met_manual_coll_old': Variable(
        'manual_met_coll_mass', 
        'PtCorrHelper::manual_met_coll(tau_0_p4.Pt(), tau_0_p4.Eta(),tau_0_p4.Phi(),tau_1_p4.Pt(), tau_1_p4.Eta(),tau_1_p4.Phi(), jet_0_p4.Pt(), jet_0_p4.Eta(), jet_0_p4.Phi() ,jet_1_p4.Pt(), jet_1_p4.Eta(), jet_1_p4.Phi() ,jet_2_p4.Pt(), jet_2_p4.Eta(),jet_2_p4.Phi(), n_jets, ditau_deta, ditau_dphi)', 
        'Collinear mass manual met', 
        'GeV', 
        Binning(25, 0, 250)),

    'met_manual_control': Variable(
        'control_tau_0_pt', 
        'PtCorrHelper::control(tau_0_p4.Pt(), tau_0_p4.Eta(),tau_0_p4.Phi(),tau_1_p4.Pt(), tau_1_p4.Eta(),tau_1_p4.Phi(), jet_0_p4.Pt(), jet_0_p4.Eta(), jet_0_p4.Phi() ,jet_1_p4.Pt(), jet_1_p4.Eta(), jet_1_p4.Phi() ,jet_2_p4.Pt(), jet_2_p4.Eta(),jet_2_p4.Phi(), n_jets)', 
        'tau_0_pt', 
        'GeV', 
        Binning(20, 20, 220)),


    'met_high': Variable(
        'met_reco_et_high', 
        'met_p4.Pt()', 
        'MET E_{t}', 
        'GeV', 
        Binning(15, 50, 200)),

    'met_phi': Variable(
        'met_reco_phi', 
        'met_p4.Phi()', 
        'MET #phi', '',              
        binningPhiCoarse),
    #gal:
    'met_eta': Variable(
        'met_reco_eta', 
        'met_p4.Eta()', 
        'MET_eta', '',              
        Binning(50, -5, 5)),

    'met_soft_term': Variable(
        'met_soft_term',
        'met_more_met_et_soft',
        'MET Soft Term E_{T}',
        'GeV',
        Binning(40, 0, 200)),

    'met_delta': Variable(
        'met_delta',
        'met_p4.Et()-met_more_met_et_soft',
        'MET_delta',
        '',
        Binning(40, 0, 200)),    

    'met_Et': Variable(
        'met_Et',
        'met_p4.Et()',
        'MET_Et',
        '',
        Binning(40, 0, 200)),    

    'met_centrality': Variable(
        'met_centrality', 
        'ditau_met_centrality', 
        'MET centrality', '',              
        Binning(20, 0, 2)),

    'met_significance': Variable(
        'met_significance', 
        'met_sign_met_significance', 
        'MET significance', '',              
        Binning(40, 0, 20)),

    'met_over_met_significance': Variable(
        'met_over_met_significance',
        'met_p4.Pt() / met_sign_met_significance',
        'MET E_{t} / met significance',
        '',
        Binning(40, 0, 40)),

    'dphi_met_reco_met_truth': Variable(
        'dphi_met_reco_met_truth',
        'met_p4.Phi() - met_truth_p4.Phi()',
        '#Phi(MET_{reco}) - #Phi(MET_{truth})',
        '',
        Binning(64, -1, 1)),
        
    'det_met_reco_met_truth': Variable(
        'det_met_reco_met_truth',
        'met_p4.Et() - met_truth_p4.Et()',
        'E_{T}(MET_{reco}) - E_{T}(MET_{truth})',
        'GeV',
        Binning(16, -10, 30)),
        
    'higgs_pt_maxw': Variable(
        'ditau_mmc_maxw_pt', 
        'ditau_mmc_maxw_pt',                               
        'MMC MAXW p_{T}(H)', 
        'GeV', 
        Binning(30, 90, 390)),

    'higgs_pt_maxw_low': Variable(
        'ditau_mmc_maxw_pt', 
        'ditau_mmc_maxw_pt',                               
        'MMC MAXW p_{T}(H)', 
        'GeV', 
        Binning(39, 0, 390)),

    'higgs_pt_maxw_minus_higgs_pt': Variable(
        'ditau_mmc_maxw_pt_minus_higgs_pt', 
        'ditau_mmc_maxw_pt - ditau_higgspt',                               
        'MMC MAXW p_{T}(H) - Standard p_{T}(H)', 
        'GeV', 
        Binning(100, -50, 50)),

    'higgs_pt': Variable(
        'ditau_higgspt', 
        'ditau_higgspt',                               
        'p_{T}(H)', 
        'GeV', 
        Binning(30, 90, 390)),

    'higgs_pt_low': Variable(
        'ditau_higgspt', 
        'ditau_higgspt',                               
        'p_{T}(H)', 
        'GeV', 
        Binning(39, 0, 390)),

    # 'higgs_pt_low': Variable(
    #     'ditau_higgspt_low', 
    #     'ditau_higgspt',                               
    #     'p_{T}(H)', 
    #     'GeV', 
    #     Binning(30, 0, 300)),

    'higgs_pt_wide': Variable(
        'ditau_higgspt', 
        'ditau_higgspt',                               
        'p_{T}(H)', 
        'GeV', 
        Binning(30, 400, 700)),

    'higgs_pt_coarse': Variable(
        'ditau_higgspt_coarse', 
        'ditau_higgspt',                               
        'p_{T}(H)', 
        'GeV', 
        VariableBinning([100, 120, 200, 300, 1000])),

    'ditau_pt': Variable(
        'ditau_pt',
        'ditau_p4.Pt()',
        'p_{T}(ll)',
        'GeV',
        Binning(30, 90, 390)),

    'ditau_pt_low': Variable(
        'ditau_pt_low',
        'ditau_p4.Pt()',
        'p_{T}(ll)',
        'GeV',
        Binning(40, 0, 400)),

    'ditau_pt_coarse': Variable(
        'ditau_pt_coarse',
        'ditau_p4.Pt()',
        'p_{T}(ll)',
        'GeV',
        VariableBinning([100, 120, 200, 300, 1000])),

    'visible_mass': Variable(
        'visible_mass',
        'ditau_p4.M()',
        'Visible Mass',
        'GeV',
        Binning(20, 0, 200)),

    'visible_mass_extended': Variable(
        'visible_mass_',
        'ditau_p4.M()',
        'Visible Mass',
        'GeV',
        Binning(25, 50, 300)),

    'visible_mass_corr': Variable(
        'visible_mass_corr',
        ditau_m_corr,
        'Corrected Visible Mass',
        'GeV',
        Binning(20, 0, 200)),
    
    'mll': Variable(
        'visible_mass',
        'ditau_p4.M()',
        'm_{ll}',
        'GeV',
        Binning(40, 80, 120)),

    'higgs_pt_res': Variable(
        'higgs_pt_res', 
        '(ditau_higgspt - boson_0_truth_p4.Pt()) / boson_0_truth_p4.Pt()',                               
        'p_{T}(H): (Reco - Truth) / Truth', 
        '', 
        Binning(50, -1, 1)),

    'ditau_pt_truth': Variable(
        'ditau_pt_truth',
        'boson_0_truth_p4.Pt()',
        'Truth Boson p_{T}',
        'GeV',
        Binning(30, 90, 390)),
        #VariableBinning([100, 120, 200, 300, 1000])),

    'ditau_dr': Variable(
        'ditau_dr', 
        'ditau_dr',                               
        '#DeltaR(#tau, #tau)', 
        '',
        Binning(25, 0, 2.5)),

      'ditau_dr_corr': Variable(
        'ditau_dr_corr',
        dr_corr,
        '#DeltaR(#tau, #tau)',
        '',
        Binning(25, 0, 2.5)),

    # 'ditau_dr_corr_truth': Variable(
    #     'ditau_dr_corr_truth',
    #     'PtCorrHelper::dr_corr('+ ','.join(tau_0_m_4vec) + ','.join(tau_1_m_4vec) +'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number))',
    #     '#DeltaR(#tau, #tau)',
    #     '',
    #     Binning(25, 0, 2.5)),

    'ditau_dphi': Variable(
        'ditau_dphi',
        'ditau_dphi',
        '#Delta#phi(#tau, #tau)',
        '',
        Binning(32, -1.6, 1.6)),

    #gal:
    'ditau_dphi': Variable(
        'ditau_dphi',
        'ditau_dphi',
        '#Delta#phi(#tau, #tau)',
        '',
        Binning(64, -2*ROOT.TMath.Pi(), 2*ROOT.TMath.Pi())),

    'ditau_deta': Variable(
        'abs(ditau_deta)', 
        'fabs(ditau_deta)',                               
        '#Delta#eta(#tau, #tau)', 
        '',
        Binning(50, 0, 5)),

    'ditau_deta_corr': Variable(
        'ditau_deta_corr',
        'fabs({})'.format(deta_corr),
        '#Delta#eta(#tau, #tau)',
        '',
        Binning(20, 0, 2)),

    # 'ditau_deta_corr_truth': Variable(
    #     'ditau_deta_corr_truth',
    #     'fabs(PtCorrHelper::deta_corr('+ ','.join(tau_0_m_4vec) + ','.join(tau_1_m_4vec) +'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number)))',
    #     '#Delta#eta(#tau, #tau)',
    #     '',
    #     Binning(20, 0, 2)),
    
    'colinear_mass_corr': Variable(
        'ditau_coll_approx_m_corr',
        m_coll_corr,
        'Colinear Mass',
        'GeV',
        Binning(20, 0, 200)),

    # 'transverse_mass_l1': Variable(
    #     'transverse_mass_l1', 
    #     'ditau_mt_lep1_met', 
    #     'Transverse Mass', 
    #     'GeV',
    #     Binning(17, 30, 200)),

    # 'transverse_mass_l0': Variable(
    #     'transverse_mass_l0',
    #     'ditau_mt_lep0_met',
    #     'Transverse Mass',
    #     'GeV',
    #     Binning(17, 30, 200)),

    'transverse_mass_l1': Variable(
        'transverse_mass_l1', 
        'ditau_mt_lep1_met', 
        'Transverse Mass', 
        'GeV',
        Binning(25, 0, 250)),

    'transverse_mass_l0': Variable(
        'transverse_mass_l0',
        'ditau_mt_lep0_met',
        'Transverse Mass',
        'GeV',
        Binning(25, 0, 250)),
        
    'ditau_qxq': Variable(
        'ditau_qxq', 
        'ditau_qxq', 
        'q0 x q1',
        '',
        Binning(6, -3, 3)),

    'tau_1_iso_Gradient': Variable(
        'tau_1_iso_Gradient', 
        '(tau_1 == 2 && tau_1_iso_Gradient) || (tau_1 == 1 && tau_1_iso_FCTightTrackOnly)', 
        'Subleading lepton gradient isolation', 
        '',
        Binning(5, -2, 3)),

    'x0': Variable(
        'x0', 'ditau_coll_approx_x0', 'x_{1}', '',
        Binning(20, 0, 2)),

    'x1': Variable(
        'x1', 'ditau_coll_approx_x1', 'x_{2}', '',
        Binning(20, 0, 2)),

    'delta_phi_tau_met': Variable(
        'delta_phi_tau_met', 
        'abs(TVector2::Phi_mpi_pi(tau_0_p4.Phi() - met_p4.Phi()))', '|#Delta#Phi(#tau, MET)|', 
        '',
        Binning(16, 0, ROOT.TMath.Pi())),

    #gal added:

    'mutau_bdt_score': Variable(
        'mutau_bdt_score', 
        'mutau_bdt_score', 
        '',
        Binning(20, -1, 1)),

    'etau_bdt_score': Variable(
        'etau_bdt_score', 
        'etau_bdt_score', 
        '',
        Binning(20, -1, 1)),


    'dphi_lep_manual_met': Variable(
        'dphi_lep_manual_met', 
        'TVector2::Phi_mpi_pi(tau_1_p4.Phi() - PtCorrHelper::manual_met_phi(tau_0_p4.Pt(), tau_0_p4.Eta(),tau_0_p4.Phi(),tau_1_p4.Pt(), tau_1_p4.Eta(),tau_1_p4.Phi(), jet_0_p4.Pt(), jet_0_p4.Eta(), jet_0_p4.Phi() ,jet_1_p4.Pt(), jet_1_p4.Eta(), jet_1_p4.Phi() ,jet_2_p4.Pt(), jet_2_p4.Eta(),jet_2_p4.Phi(), n_jets))', '|#Delta#Phi(#tau, MET)|', 
        '',
        Binning(40, -5, 5)),

    'f_dphi_lep_manual_met': Variable(
        'dphi_lep_manual_met', 
        'dphi_lep_manual_met', 
        '',
        binningPhi),

    'dphi_tau_manual_met': Variable(
        'dphi_tau_manual_met', 
        'TVector2::Phi_mpi_pi(tau_0_p4.Phi() - PtCorrHelper::manual_met_phi(tau_0_p4.Pt(), tau_0_p4.Eta(),tau_0_p4.Phi(),tau_1_p4.Pt(), tau_1_p4.Eta(),tau_1_p4.Phi(), jet_0_p4.Pt(), jet_0_p4.Eta(), jet_0_p4.Phi() ,jet_1_p4.Pt(), jet_1_p4.Eta(), jet_1_p4.Phi() ,jet_2_p4.Pt(), jet_2_p4.Eta(),jet_2_p4.Phi(), n_jets))', '|#Delta#Phi(#tau, MET)|', 
        '',
        Binning(40, -5, 5)),

    'f_dphi_tau_manual_met': Variable(
        'dphi_tau_manual_met', 
        'dphi_tau_manual_met', 
        '',
        binningPhi),

    'meaninterbunchcrossing': Variable(
        'mean_interaction_bunch_crossing',
        'n_avg_int_cor',
        'Average Interactions Per Bunch Crossing', 
        '',
        VariableBinning([0, 15, 20, 25, 30, 35, 40, 45, 50, 55, 80])),

    'meaninterbunchcrossing_fine': Variable(
        'mean_interaction_bunch_crossing',
        'n_avg_int_cor',
        'Average Interactions Per Bunch Crossing', 
        '',
        Binning(40, 0, 80)),

    'actualinterbunchcrossing': Variable(
        'actual_interaction_bunch_crossing',
        'n_actual_int_cor',
        'Actual Interactions Per Bunch Crossing', 
        '',
        VariableBinning([0, 15, 20, 25, 30, 35, 40, 45, 50, 55, 80])),

    'actualinterbunchcrossing_fine': Variable(
        'actual_interaction_bunch_crossing',
        'n_actual_int_cor',
        'Actual Interactions Per Bunch Crossing', 
        '',
        Binning(40, 0, 80)),

    'n_vertices': Variable(
        'n_vertices',
        'n_vx',
        'Number of Vertices',
        '',
        Binning(12, 0, 60)),

    'norm_weight': Variable(
        'norm_weight',
        'MC_Norm::TotalWeight(mc_channel_number)',
        'xsec * kfactor / sumofweights', '',
        Binning(100, 1.0e-6, 2e-6)),

    'pt_total': Variable(
        'pt_total',
        'pt_total',
        'p_{T}(Hjj)',
        'GeV',
        Binning(30, 0, 300)),

    'pt_total_low': Variable(
        'pt_total_low',
        'pt_total',
        'p_{T}(Hjj)',
        'GeV',
        Binning(50, 0, 200)),

    'vbf_tagger': Variable(
        'vbf_tagger',
        vbf_tagger_expr,
        'VBF Tagger Score', 
        '',
        Binning(20, -0.6, 0.6)),

    'vbf_tagger_pr': Variable(
        'vbf_tagger',
        vbf_tagger_expr,
        'VBF Tagger Score', 
        '',
        Binning(20, -0.6, 0.6)),

    'vbf_tagger_medium_fine_binning': Variable(
        'vbf_tagger',
        vbf_tagger_expr,
        'VBF Tagger Score', 
        '',
        Binning(20, -1, 1)),

    'vbf_tagger_fine_binning': Variable(
        'vbf_tagger',
        vbf_tagger_expr,
        'VBF Tagger Score', 
        '',
        Binning(100, -1, 1)),

    'vh_tagger': Variable(
        'vh_tagger',
        vh_tagger_expr,
        'VH Tagger Score', 
        '',
        Binning(40, -1, 1)),

    'vh_tagger_pr': Variable(
        'vh_tagger',
        vh_tagger_expr,
        'VH Tagger Score', 
        '',
        Binning(20, -0.5, 0.5)),

    'vh_tagger_corr': Variable(
        'vh_tagger_corr',
        vh_tagger_corr_expr,
        'VH Tagger Score', 
        '',
        Binning(40, -1, 1)),

    'tau_0_pt_corr': Variable(
        'tau_0_pt_corr', 
        tau_0_pt_corr,
        'leading p_{T}', 
        'GeV', 
        Binning(20, 20, 220)),

    'tau_1_pt_corr': Variable(
        'tau_1_pt_corr', 
        tau_1_pt_corr,
        'subleading p_{T}', 
        'GeV', 
        Binning(20, 0, 100)),

    'ditau_pt_corr': Variable(
        'ditau_pt_corr',
        'PtCorrHelper::ditau_pt_corr('+','.join(met_4vec) + ','.join(tau_0_4vec) + ','.join(tau_1_4vec) + 'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number))',
        'p_{T}(H)',
        'GeV',
        Binning(40, 90, 490)),

    'met_corr': Variable(
        'met_corr',
        met_corr,
        'MET',
        'GeV',
        Binning(20, 0, 200)),            

    'taus_pt_ratio': Variable(
        'ditau_pt_ratio',
	'tau_1_p4.Pt() / tau_0_p4.Pt()',
        'p_{T}(#tau_{1}) / p_{T}(#tau_{0})',
        '',
        Binning(40, 0, 2)),

    'taus_pt_ratio_corr': Variable(
        'ditau_pt_ratio_corr',
        ' PtCorrHelper::read_pt_corr_0('+ ','.join(tau_0_m_4vec) + ','.join(tau_1_m_4vec) + 'event_number +'+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number))/PtCorrHelper::read_pt_corr_1('+ ','.join(tau_0_m_4vec) + ','.join(tau_1_m_4vec) + 'event_number +'+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number))',
        'corrected p_{T}(#tau_{1}) / p_{T}(#tau_{0})',
        '',
        Binning(40, 0, 2)),
    
    'tau_0_correction_ratio': Variable(
        'tau_0_correction_ratio',
        '{}/tau_0_p4.Pt()'.format(tau_0_pt_corr),
        'leading p_{T}(#tau_{corr}) / p_{T}(#tau_{uncorr})',
        '',
        Binning(40, 0, 2)),

    'tau_1_correction_ratio': Variable(
        'tau_1_correction_ratio',
        '{}/tau_1_p4.Pt()'.format(tau_1_pt_corr),
        'subleading p_{T}(#tau_{corr}) / p_{T}(#tau_{uncorr})',
        '',
        Binning(40, 0, 2)),

    # 'tau_0_correction_truth_ratio': Variable(
    #     'tau_0_correction_truth_ratio',
    #     'PtCorrHelper::read_pt_corr_0('+ ','.join(tau_0_m_4vec) + ','.join(tau_1_m_4vec) + 'event_number +'+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number))/tau_0_matched_p4.Pt()',
    #     'leading p_{T}(#tau_{corr}) / p_{T}(#tau_{truth})',
    #     '',
    #     Binning(40, 0, 2)),

    # 'tau_1_correction_truth_ratio': Variable(
    #     'tau_1_correction_truth_ratio',
    #     'PtCorrHelper::read_pt_corr_1('+ ','.join(tau_0_m_4vec) + ','.join(tau_1_m_4vec) + 'event_number +'+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number))/tau_1_matched_p4.Pt()',
    #     'subleading p_{T}(#tau_{corr}) / p_{T}(#tau_{truth})',
    # '',
    # Binning(40, 0, 2)),

    'tau_0_truth_ratio': Variable(
        'tau_0_truth_ratio',
        'tau_0_p4.Pt()/tau_0_matched_p4.Pt()',
        'leading p_{T}(#tau_{uncorr}) / p_{T}(#tau_{truth})',
        '',
        Binning(40, 0, 2)),

    'tau_1_truth_ratio': Variable(
        'tau_1_truth_ratio',
        'tau_1_p4.Pt()/tau_1_matched_p4.Pt()',
        'subleading p_{T}(#tau_{uncorr}) / p_{T}(#tau_{truth})',
        '',
        Binning(40, 0, 2)),

    'x0_corr': Variable(
        'x0_corr',
        x0_corr,
        'x_{1}',
        '',
        Binning(20, 0, 2)),

    'x1_corr': Variable(
        'x1_corr',
        x1_corr,
        'x_{2}',
        '',
        Binning(20, 0, 2)),

    'dijetditau_dr_corr': Variable(
        'dijetditau_dr_corr',
        'PtCorrHelper::dijet_ditau_dr_corr(dijet_p4.Pt(), dijet_p4.Eta(), dijet_p4.Phi(), dijet_p4.M(), '+','.join(tau_0_4vec) + ','.join(tau_1_4vec) + 'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number))',
        '#DeltaR(dijet, ditau)',
        '',
        Binning(20, 0, 5)),

    'eta_corr_0': Variable(
        'eta_corr_0',
        'PtCorrHelper::eta_corr_0('+','.join(tau_0_4vec) + ','.join(tau_1_4vec) + 'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number))',
        '#eta(leading #tau)',
        '',
        Binning( 50, -2.5, 2.5)),

    'eta_corr_1': Variable(
        'eta_corr_1',
        'PtCorrHelper::eta_corr_1('+','.join(tau_0_4vec) + ','.join(tau_1_4vec) + 'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number))',
        '#eta(subleading #tau)',
        '',
        Binning( 50, -2.5, 2.5)),

    'phi_corr_0': Variable(
        'phi_corr_0',
        'PtCorrHelper::phi_corr_0('+','.join(tau_0_4vec) + ','.join(tau_1_4vec) + 'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number))',
        '#phi(leading #tau)',
        '',
        binningPhi),

    'phi_corr_1': Variable(
        'phi_corr_1',
        'PtCorrHelper::phi_corr_1('+','.join(tau_0_4vec) + ','.join(tau_1_4vec) + 'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number))',
        '#phi(subleading #tau)',
        '',
        binningPhi),

    'tau_0_truth_pt_corr': Variable(
        'tau_0_truth_pt_corr',
        'PtCorrHelper::read_pt_corr_0(tau_0_matched_p4.Pt(), tau_0_matched_p4.Eta(), tau_0_matched_p4.Phi(), tau_1_matched_p4.Pt(), tau_1_matched_p4.Eta(), tau_1_matched_p4.Phi(), event_number +'+str(_random_offset)+')',
        'corrected leading p_{T}(truth #tau)',
        'GeV',
        Binning(20, 20, 220)),

    'tau_1_truth_pt_corr': Variable(
        'tau_1_truth_pt_corr',
        'PtCorrHelper::read_pt_corr_1(tau_0_matched_p4.Pt(), tau_0_matched_p4.Eta(), tau_0_matched_p4.Phi(), tau_1_matched_p4.Pt(), tau_1_matched_p4.Eta(), tau_1_matched_p4.Phi(), event_number +'+str(_random_offset)+')',
        'corrected subleading p_{T}(truth #tau)',
        'GeV',
        Binning(20, 0, 100)),

    'tau_0_truth': Variable(
	'tau_0_truth',
        'tau_0_matched_p4.Pt()',
        'leading p_{T}(truth #tau)',
        'GeV',
        Binning(20, 20, 220)),

    'tau_1_truth': Variable(
	'tau_1_truth',
        'tau_1_matched_p4.Pt()',
        'subleading p_{T}(truth #tau)',
        'GeV',
        Binning(20, 0, 100)),

    'corrected_momentum_test': Variable(
        'corrected_momentum_test',
        'PtCorrHelper::read_pt_corr_0(tau_0_matched_p4.Pt(), tau_0_matched_p4.Eta(), tau_0_matched_p4.Phi(), tau_1_matched_p4.Pt(), tau_1_matched_p4.Eta(), tau_1_matched_p4.Phi(), event_number +'+str(_random_offset)+')',
        'bla',
        '',
        Binning(10, 0, 100)),

    'channel_test': Variable(
        'channel_test',
        'PtCorrHelper::zll_choose_channel(event_number)',
        'Channel Number',
        'Arbitrary Units',
        Binning(10,0,10)),

    'higgs_pt_over_dijet_pt': Variable(
        'higgs_pt_over_dijet_pt',
        'ditau_higgspt / dijet_p4.Pt()',
        'p_{T}(H) / p_{T}(jj)',
        '',
        Binning(40, 0.5, 2.5)),

    'dphi_MET_0': Variable(
        'dphi_MET_0',
        #'(3.1415 - abs(abs(tau_0_p4.Phi() - met_p4.Phi()) - 3.1415))',
        '(PtCorrHelper::dphi_MET_0('+','.join(met_4vec) + ','.join(tau_0_4vec) + ','.join(tau_1_4vec) + 'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number)))',
        '#Phi(MET) - #Phi(leading #tau)',
        '',
        Binning(40, -5, 5)),

    'dphi_MET_1': Variable(
        'dphi_MET_1',
        #'(3.1415 - abs(abs(tau_1_p4.Phi() - met_p4.Phi()) - 3.1415))',
        '(PtCorrHelper::dphi_MET_1('+','.join(met_4vec) + ','.join(tau_0_4vec) + ','.join(tau_1_4vec) + 'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number)))',
        '#Phi(MET) - #Phi(subleading #tau)',
        '',
        Binning(40, -5, 5)),

   'dphi_MET_corr_0': Variable(
        'dphi_MET_corr_0',
        '(PtCorrHelper::dphi_MET_corr_0('+','.join(met_4vec) + ','.join(tau_0_4vec) + ','.join(tau_1_4vec) + 'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number)))',
        'corrected #Phi(MET) - #Phi(leading #tau)',
        '',
        Binning(40, -5, 5)),

    'dphi_MET_corr_1': Variable(
        'dphi_MET_corr_1',
        '(PtCorrHelper::dphi_MET_corr_1('+','.join(met_4vec) + ','.join(tau_0_4vec) + ','.join(tau_1_4vec) + 'event_number + '+str(_random_offset)+', PtCorrHelper::zll_choose_channel(event_number)))',
        'corrected #Phi(MET) - #Phi(subleading #tau)',
        '',
        Binning(40, -5, 5)),

    # isofac variable
    'tau_1_pt_isofac': Variable(
        'tau_1_pt_isofac',
        'tau_1_p4.Pt()',
        'subleading p_{T}',
        'GeV',
        VariableBinning( [ 20, 50,300])),

    'tau_1_eta_isofac': Variable(
        'tau_1_eta_isofac',
        'abs(tau_1_p4.Eta())',
        'subleading |#eta|',
        'GeV',
        VariableBinning( [ 0.0, 1.5,3.0])),

    'tau_0_pt_ff_preselection': Variable(
        'tau_0_pt_ff_preselection',
        'tau_0_p4.Pt()',
        'leading p_{T}',
        'GeV',
        VariableBinning( [ 30, 40, 70, 500])),
    
    'tau_0_pt_ff_boost': Variable(
        'tau_0_pt_ff_boost',
        'tau_0_p4.Pt()',
        'leading p_{T}',
        'GeV',
        VariableBinning( [ 30, 40, 70, 500])),
    
    'tau_0_pt_ff_vh': Variable(
        'tau_0_pt_ff_vh',
        'tau_0_p4.Pt()',
        'leading p_{T}',
        'GeV',
        VariableBinning( [ 30, 40, 500])),
 
    'tau_0_pt_ff_vbf': Variable(
        'tau_0_pt_ff_vbf',
        'tau_0_p4.Pt()',
        'leading p_{T}',
        'GeV',
        VariableBinning( [ 30, 40, 500])),

    'tau_0_pt_hhff': Variable(
        'tau_0_pt_hhff',
        'tau_0_p4.Pt()',
        'leading p_{T}',
        'GeV',
        VariableBinning( [40, 45, 50, 80, 200])),

    'tau_1_pt_hhff': Variable(
        'tau_1_pt_hhff',
        'tau_1_p4.Pt()',
        'subleading p_{T}',
        'GeV',
        VariableBinning( [30, 35, 40, 45, 50, 80, 200])),

    'tau_0_eta_hhff': Variable(
        'tau_0_eta_hhff',
        'fabs(tau_0_p4.Eta())',
        'leading #eta',
        '',
        VariableBinning( [0., 0.8, 1.37,2.0, 2.5])),

    'tau_1_eta_hhff': Variable(
        'tau_1_eta_hhff',
        'fabs(tau_1_p4.Eta())',
        'subleading #eta',
        '',
        VariableBinning( [0., 0.8, 1.37,2.0, 2.5])),

    'tau_0_pt_hhff_high_deta': Variable(
        'tau_0_pt_hhff_high_deta',
        'tau_0_p4.Pt()',
        'leading p_{T}',
        'GeV',
        VariableBinning([40, 45, 50, 60, 80, 200])),

    'tau_1_pt_hhff_high_deta': Variable(
        'tau_1_pt_hhff_high_deta',
        'tau_1_p4.Pt()',
        'subleading p_{T}',
        'GeV',
        VariableBinning([30, 34, 40, 45, 200])),

    'tau_0_eta_hhff_high_deta': Variable(
        'tau_0_eta_hhff_high_deta',
        'fabs(tau_0_p4.Eta())',
        'leading #eta',
        '',
        #VariableBinning( [0., 1.37,2.0, 2.5])),
        VariableBinning( [0., 0.8, 1.37,2.0, 2.5])),

    'tau_1_eta_hhff_high_deta': Variable(
        'tau_1_eta_hhff_high_deta',
        'fabs(tau_1_p4.Eta())',
        'subleading #eta',
        '',
        #VariableBinning( [0., 1.37,2.0, 2.5])),
        VariableBinning( [0., 0.8, 1.37,2.0, 2.5])),

### looser binnings for loose-not-medium FFs, still need 2d
    'tau_0_pt_hhff_high_deta_lnm': Variable(
        'tau_0_pt_hhff_high_deta_lnm',
        'tau_0_p4.Pt()',
        'leading p_{T}',
        'GeV',
        VariableBinning([40, 45, 50, 60, 80, 200])),

    'tau_1_pt_hhff_high_deta_lnm': Variable(
        'tau_1_pt_hhff_high_deta_lnm',
        'tau_1_p4.Pt()',
        'subleading p_{T}',
        'GeV',
        VariableBinning([30, 34, 40, 45, 200])),

    'tau_0_eta_hhff_high_deta_lnm': Variable(
        'tau_0_eta_hhff_high_deta_lnm',
        'fabs(tau_0_p4.Eta())',
        'leading #eta',
        '',
        #VariableBinning( [0., 1.37,2.0, 2.5])),
        VariableBinning( [0., 1.37, 2.5])),

    'tau_1_eta_hhff_high_deta_lnm': Variable(
        'tau_1_eta_hhff_high_deta_lnm',
        'fabs(tau_1_p4.Eta())',
        'subleading #eta',
        '',
        #VariableBinning( [0., 1.37,2.0, 2.5])),
        VariableBinning( [0., 1.37, 2.5])),

     # loose binning used for WCR FFs and parametrization uncertainty
    'tau_0_pt_hhff_param_lh': Variable(
        'tau_0_pt_hhff_param_lh',
        'tau_0_p4.Pt()',
        'leading p_{T}',
        'GeV',
        VariableBinning( [30, 40, 60, 500])),

    'tau_0_pt_hhff_param': Variable(
        'tau_0_pt_hhff_param',
        'tau_0_p4.Pt()',
        'leading p_{T}',
        'GeV',
        VariableBinning( [40, 60, 500])),

    'tau_1_pt_hhff_param': Variable(
        'tau_1_pt_hhff_param',
        'tau_1_p4.Pt()',
        'subleading p_{T}',
        'GeV',
        VariableBinning( [30, 40, 60, 500])),

    'tau_0_eta_hhff_param': Variable(
        'tau_0_eta_hhff_param',
        'fabs(tau_0_p4.Eta())',
        'leading #eta',
        '',
        VariableBinning( [0., 1.37, 2.5])),

    'tau_1_eta_hhff_param': Variable(
        'tau_1_eta_hhff_param',
        'fabs(tau_1_p4.Eta())',
        'subleading #eta',
        '',
        VariableBinning( [0., 1.37, 2.5])),


    'dphi_met_tau_rqcd' : Variable(
        'dphi_met_tau_rqcd',
        '3.1415 - fabs(fabs(tau_0_p4.Phi() - met_p4.Phi()) - 3.1415)',
        '|#Delta #Phi (#tau,MET)|',
        '',
        VariableBinning( [ 0,2,5 ])),

    'tau_0_pt_rqcd': Variable(
        'tau_0_pt_rqcd',
        'tau_0_p4.Pt()',
        'leading p_{T}',
        'GeV',
        VariableBinning( [ 30, 40, 500])),

    'n_jets_5to10': Variable(
        'n_jets_5to10',
        'n_jets',
        'N_{jets}',
        '',
        Binning(6, 4.5, 10.5, ['5', '6', '7', '8', '9', '10'])),

    'mTopWbest': Variable(
        'mTopWbest',
        'mTopWbest / 1000.',
        'm_{jjb} for best Top candidate',
        'GeV',
        Binning(20, 0, 600)),

    'mWsubbest': Variable(
        'mWsubbest',
        'mWsubbest / 1000.',
        'm_{jj} for second best W candidate',
        'GeV',
        Binning(20, 0, 150)),

    'mWbest': Variable(
        'mWbest',
        'mWbest / 1000.',
        'm_{jj} for best W candidate',
        'GeV',
        Binning(20, 0, 150)),

    'HTjets': Variable(
        'HTjets',
        'HTjets / 1000.',
        'H_{T}^{jets}',
        'GeV',
        Binning(20, 100, 900)),

    'SumPtBjet': Variable(
        'SumPtBjet',
        'SumPtBjet / 1000.',
        '#sum p_{T}^{b-jets}',
        'GeV',
        Binning(20, 0, 300)),

    'jjdrmin': Variable(
        'jjdrmin',
        'jjdrmin',
        'Minimum #DeltaR(jj)',
        '',
        Binning(25, 0, 2.5)),

    'ditau_met_min_dphi': Variable(
        'ditau_met_min_dphi',
        'ditau_met_min_dphi',
        'Min #Delta#phi(#tau-MET)',
        '',
        Binning(32, 0, 3.2)),

    # Corrected dR (tau, jet)
    'jet_0_tau_0_dr_corr': Variable(
        'jet_0_tau_0_dr_corr',
        jet_0_tau_0_dr_corr,
        '#Delta#R(#tau0-jet0)',
        '',
        Binning(50, 0, 5)),

    'jet_1_tau_0_dr_corr': Variable(
        'jet_1_tau_0_dr_corr',
        jet_1_tau_0_dr_corr,
        '#Delta#R(#tau0-jet1)',
        '',
        Binning(50, 0, 5)),

    'jet_0_tau_1_dr_corr': Variable(
        'jet_0_tau_1_dr_corr',
        jet_0_tau_1_dr_corr,
        '#Delta#R(#tau1-jet0)',
        '',
        Binning(50, 0, 5)),

    'jet_1_tau_1_dr_corr': Variable(
        'jet_1_tau_1_dr_corr',
        jet_1_tau_1_dr_corr,
        '#Delta#R(#tau1-jet1)',
        '',
        Binning(50, 0, 5)),

    # Uncorrected versions
    'jet_1_tau_0_dr': Variable(
        'jet_1_tau_0_dr',
        'PtCorrHelper::dr_helper(jet_1_p4.Pt(), jet_1_p4.Eta(), jet_1_p4.Phi(), tau_0_p4.Eta(), tau_0_p4.Phi())',
        '#Delta#R(#tau0-jet1)',
        '',
        Binning(50, 0, 5)),

    'jet_0_tau_0_dr': Variable(
        'jet_0_tau_0_dr',
        'PtCorrHelper::dr_helper(jet_0_p4.Pt(), jet_0_p4.Eta(), jet_0_p4.Phi(), tau_0_p4.Eta(), tau_0_p4.Phi())',
        '#Delta#R(#tau0-jet0)',
        '',
        Binning(50, 0, 5)),

    'jet_1_tau_1_dr': Variable(
        'jet_1_tau_1_dr',
        'PtCorrHelper::dr_helper(jet_1_p4.Pt(), jet_1_p4.Eta(), jet_1_p4.Phi(), tau_1_p4.Eta(), tau_1_p4.Phi())',
        '#Delta#R(#tau1-jet1)',
        '',
        Binning(50, 0, 5)),

    'jet_0_tau_1_dr': Variable(
        'jet_0_tau_1_dr',
        'PtCorrHelper::dr_helper(jet_0_p4.Pt(), jet_0_p4.Eta(), jet_0_p4.Phi(), tau_1_p4.Eta(), tau_1_p4.Phi())',
        '#Delta#R(#tau1-jet0)',
        '',
        Binning(50, 0, 5)),

    # tau jet width variables
    'tau_0_width': Variable(
        'tau_0_width',
        'tau_0_jet_width',
        'leading #tau jet width',
        '',
        Binning(20, 0, 0.2)),

    'tau_1_width': Variable(
        'tau_1_width',
        'tau_1_jet_width',
        'subleading #tau jet width',
        '',
        Binning(20, 0, 0.2)),

    'tth_tt_bdt': Variable(
                'tth_tt_bdt',
                tth_tt_expr,
                'ttH vs. t#bar{t} score',
                '',
                Binning(12, -1, 1)),

    'tth_Z_bdt': Variable(
                'tth_Z_bdt',
                tth_Z_expr,
                'ttH vs. Z(#tau#tau) score',
                '',
                Binning(12, -1, 1)),

}
