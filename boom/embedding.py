import ROOT
import os

# pt(reco visible tau) / pt(vis+invis truth tau)
_PT_FILE = os.path.join(os.path.dirname(
    os.path.abspath(__file__ )), "../data/embedding/ratios_scaled_normalised.root")
_pt_root_file = ROOT.TFile(_PT_FILE, "read")

# muon efficiencies
_MUON_EFF_FILE = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), "../data/embedding/efficiencies_muon.root")
_muon_root_file = ROOT.TFile(_MUON_EFF_FILE, 'read')

# muon folding efficiencies
_MUON_EFF_FILE_FOLD = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), "../data/embedding/efficiencies_muon_fold.root")
_muon_root_file_fold = ROOT.TFile(_MUON_EFF_FILE_FOLD, 'read')

# electron efficiencies
_ELE_EFF_FILE = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), "../data/embedding/efficiencies_electron.root")
_ele_root_file = ROOT.TFile(_ELE_EFF_FILE, 'read')

# electron folding efficiencies
_ELE_EFF_FILE_FOLD = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), "../data/embedding/efficiencies_electron_fold.root")
_ele_root_file_fold = ROOT.TFile(_ELE_EFF_FILE_FOLD, 'read')

# tau efficiencies
_TAU_EFF_FILE = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), "../data/embedding/efficiencies_tau.root")
_tau_root_file = ROOT.TFile(_TAU_EFF_FILE, 'read')

def setup_embedding(var_type='NOMINAL', n_oversampling=10):
    """
    setup function for the embedding inputs

    Arguments
    _________
      var_type : str
      n_oversampling : int
    """

    _keys_h =  [_k.GetName() for _k in _pt_root_file.Get("hadronic").GetListOfKeys()]
    _var_h = var_type
    if not var_type in _keys_h:
        _var_h = 'NOMINAL'

    _keys_h_eveto =  [_k.GetName() for _k in _pt_root_file.Get("hadronic_eveto").GetListOfKeys()]
    _var_h_eveto = var_type
    if not var_type in _keys_h_eveto:
        _var_h_eveto = 'NOMINAL'

    _keys_e =  [_k.GetName() for _k in _pt_root_file.Get("electronic").GetListOfKeys()]
    _var_e = var_type
    if not var_type in _keys_e:
        _var_e = 'NOMINAL'

    _keys_mu =  [_k.GetName() for _k in _pt_root_file.Get("muonic").GetListOfKeys()]
    _var_mu = var_type
    if not var_type in _keys_mu:
        _var_mu = 'NOMINAL'
        
    print ('BOOM: \t embedding --> request {} ratios, picking {} for t_had, {} for t_e and {} for t_mu'.format(
            var_type, _var_h, _var_e, _var_mu))

    # filling the maps of correction histograms
    _NBINS = 54
    _num_bins = ( str(i) for i in range(_NBINS) )
    for ip, _index in enumerate(_num_bins):
        ROOT.histPtDict.h_pt_corr_h[ip] = _pt_root_file.Get("hadronic/{var}/bin{index}".format(var=_var_h, index=_index))
        ROOT.histPtDict.h_pt_corr_h_eveto[ip] = _pt_root_file.Get("hadronic_eveto/{var}/bin{index}".format(var=_var_h_eveto, index=_index))
        ROOT.histPtDict.h_pt_corr_e[ip] = _pt_root_file.Get("electronic/{var}/bin{index}".format(var=_var_e, index=_index))
        ROOT.histPtDict.h_pt_corr_mu[ip] = _pt_root_file.Get("muonic/{var}/bin{index}".format(var=_var_mu, index=_index))


    # loading muon efficiencies
    print('BOOM: \t embedding --> loading efficiency_muon_trig_{}'.format(_var_mu))
    ROOT.histPtDict.efficiencies['efficiency_muon_trig'] = _muon_root_file.Get('efficiency_muon_trig_{}'.format(_var_mu))

    print('BOOM: \t embedding --> loading efficiency_muon_reco_{}'.format(_var_mu))
    ROOT.histPtDict.efficiencies['efficiency_muon_reco'] = _muon_root_file.Get('efficiency_muon_reco_{}'.format(_var_mu))

    print('BOOM: \t embedding --> loading efficiency_muon_charge_{}'.format(_var_mu))
    ROOT.histPtDict.efficiencies['efficiency_muon_charge'] = _muon_root_file.Get('efficiency_muon_charge_{}'.format(_var_mu))

    print('BOOM: \t embedding --> loading efficiency_muon_ditrig_{}'.format(_var_mu))
    ROOT.histPtDict.efficiencies['efficiency_muon_ditrig'] = _muon_root_file_fold.Get('efficiency_muon_ditrig_{}'.format(_var_mu))

    print('BOOM: \t embedding --> loading efficiency_muon_fold_{}'.format(_var_mu))
    ROOT.histPtDict.efficiencies['efficiency_muon_fold'] = _muon_root_file_fold.Get('efficiency_muon_singletrig_{}'.format(_var_mu))

    # loafing electron efficiencies
    print('BOOM: \t embedding --> loading efficiency_electron_trig_{}'.format(_var_e))
    ROOT.histPtDict.efficiencies['efficiency_electron_trig'] = _ele_root_file.Get('efficiency_electron_trig_{}'.format(_var_e))

    print('BOOM: \t embedding --> loading efficiency_electron_reco_{}'.format(_var_e))
    ROOT.histPtDict.efficiencies['efficiency_electron_reco'] = _ele_root_file.Get('efficiency_electron_reco_{}'.format(_var_e))

    print('BOOM: \t embedding --> loading efficiency_electron_charge_{}'.format(_var_e))
    ROOT.histPtDict.efficiencies['efficiency_electron_charge'] = _ele_root_file.Get('efficiency_electron_charge_{}'.format(_var_e))

    print('BOOM: \t embedding --> loading efficiency_electron_ditrig_{}'.format(_var_e))
    ROOT.histPtDict.efficiencies['efficiency_electron_ditrig'] = _ele_root_file_fold.Get('efficiency_electron_ditrig_{}'.format(_var_e))

    print('BOOM: \t embedding --> loading efficiency_electron_fold_{}'.format(_var_e))
    ROOT.histPtDict.efficiencies['efficiency_electron_fold'] = _ele_root_file_fold.Get('efficiency_electron_singletrig_{}'.format(_var_e))

    # loading tau efficiencies
    if 'TRIGGER' not in _var_h: 
        _var_h = 'NOMINAL'

    print('BOOM: \t embedding --> loading tau25_{}'.format(_var_h))
    ROOT.histPtDict.efficiencies['tau25'] = _tau_root_file.Get('tau25_{}'.format(_var_h))

    print('BOOM: \t embedding --> loading tau35_{}'.format(_var_h))
    ROOT.histPtDict.efficiencies['tau35'] = _tau_root_file.Get('tau35_{}'.format(_var_h))

    print('BOOM: \t embedding --> loading efficiency_charge_NOMINAL')
    ROOT.histPtDict.efficiencies['efficiency_charge'] = _tau_root_file.Get('efficiency_charge_NOMINAL')

    # ### per channel sampling
    ROOT.histPtDict.m_leplep_frac = 0.33
    ROOT.histPtDict.m_lephad_frac = 0.33

    print('BOOM: \t embedding --> fractions ll, lh, hh = {0}, {1}, {2}'.format(
        ROOT.histPtDict.m_leplep_frac, 
        ROOT.histPtDict.m_lephad_frac, 
        (1 - ROOT.histPtDict.m_leplep_frac - ROOT.histPtDict.m_lephad_frac)))

    # ### oversampling
    ROOT.histPtDict.n_oversampling = n_oversampling
    print('BOOM: \t embedding --> oversampling = {}'.format(
        ROOT.histPtDict.n_oversampling))
