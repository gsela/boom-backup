"""
Implementation of the hh fake systematics
"""
import ROOT
import os
from happy.systematics import Systematics, SystematicsSet


_HH_FAKE_SYST_FILE = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/hhfake_uncert.root")

_root_file = ROOT.TFile(_HH_FAKE_SYST_FILE, "read") 

ROOT.hhfake_histDict.h_syst_preselection[0] = _root_file.Get("hh_fake_uncert_ditau_mmc_mlm_m_preselection")
ROOT.hhfake_histDict.h_syst_boost[0]        = _root_file.Get("hh_fake_uncert_ditau_mmc_mlm_m_boost")
ROOT.hhfake_histDict.h_syst_vbf[0]          = _root_file.Get("hh_fake_uncert_ditau_mmc_mlm_m_vbf")
ROOT.hhfake_histDict.h_syst_vh[0]          = _root_file.Get("hh_fake_uncert_ditau_mmc_mlm_m_vh")

def hh_fakesyst_expr(category):
    expr = 'HHFakeSystHelper::fakesyst_weight(ditau_mmc_mlm_m,{category},0)'.format(
        category = str(getattr(ROOT.hhfake_histDict, category)),
        )
    return expr
        
