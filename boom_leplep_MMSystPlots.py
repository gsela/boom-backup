# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections, print_selection
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.fake import ll_fake_systs

#import logging
#logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes(stxs='stxs0', squash=True)
Fake_processes = filter(lambda p: 'Fake' in p.name, physicsProcesses)

syst_names = ll_fake_systs.keys()

_channels = (
    'emu',
    'mue',
    )

_years = (
    '15',
    '16',
    '17',
    '18',
)

_categories = (
    #'preselection',
    'boost',
    'vh',
    'vbf',
#    'boost_0_1J',
#    'boost_1_1J',
#    'boost_2_1J',
#    'boost_3_1J',
#    'boost_0_ge2J',
#    'boost_1_ge2J',
#    'boost_2_ge2J',
#    'boost_3_ge2J',
#    'vh_0',
#    'vh_1',
#    'vbf_0',
#    'vbf_1',
)

_regions = (
    'SR',
    #'top', # use it for Top control region
    )

### define your selection objects 
sels = get_selections(
    channels=_channels, 
    years=_years,
    categories=_categories,
    regions=_regions)

if(False):
  for sel in sels:
    print_selection(sel)
  exit()

# define your list of variables
_variables = [
     VARIABLES['mmc_mlm_m'],
     VARIABLES['norm'],
]


#### processor declaration, booking and running
processor = boom_processor(FakeProcesses, sels, _variables, systematic_names=syst_names, do_fake_mc_subtraction=False)

processor.book()
processor.run(n_cores=cpu_count() - 1)

# plot making
#for reg in _regions:
#    for var in _variables:
#        for cat in _categories:
#            make_data_mc_plot(processor, sels, var, categories=cat, regions=reg)
#
from boom.plotting import make_sys_nom_plot
for var in _variables:
    for cat in _categories:
        for syst in syst_names:

            make_sys_nom_plot(processor, sels, var, syst, process_name='Fake', categories=cat)
#            make_sys_nom_plot(processor, sels, var, syst, ratio_range=1.0, process_name='Fake', categories=cat)

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
