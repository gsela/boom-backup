
if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('file')
    parser.add_argument(
        'action', 
        choices=[
            'yields_zcr', 
            'channels_inspection', 
            'z_migration'])
    parser.add_argument('--zcr-channel', default=None, choices=[None, 'mumu', 'ee'])
    parser.add_argument('--ztt-zll-norm', default=False, action='store_true')
    parser.add_argument('--samples', default=None, type=str)
    parser.add_argument('--nps', default=None, type=str)
    parser.add_argument('--threshold', default=0.5, type=float)
    args = parser.parse_args()

    import os
    import ROOT
    from boom.workspace import channel
    from boom.wsi_tools import workspace_input
    file_name = args.file
    rfile = ROOT.TFile(file_name)
    _wsi = workspace_input(rfile)

    if args.action == 'yields_zcr':
        # ----------
        from boom.plotting.wsi import plot_zcr
        plot_zcr(_wsi, finalstate='hh', zcr_channel=args.zcr_channel, ztt_zll_norm=args.ztt_zll_norm)
        plot_zcr(_wsi, finalstate='lh', zcr_channel=args.zcr_channel, ztt_zll_norm=args.ztt_zll_norm)
        plot_zcr(_wsi, finalstate='ll', zcr_channel=args.zcr_channel, ztt_zll_norm=args.ztt_zll_norm)


    if args.action == 'channels_inspection':
        from boom.plotting.wsi import plot_channel
        for _channel in _wsi.channels:
            plot_channel(
                _channel, 
                sample_key=args.samples, 
                np_key=args.nps, 
                threshold=args.threshold,
                title=os.path.basename(args.file))



    if args.action == 'z_migration':
        # ------------
        ROOT.gStyle.SetPaintTextFormat("1.2f");
        ROOT.gStyle.SetPalette(ROOT.kCherry)
        ROOT.TColor.InvertPalette()
        from boom.plotting.wsi import plot_z_migration
        plot_z_migration(_wsi, finalstate='hh', corrected_zcr=True)
        plot_z_migration(_wsi, finalstate='lh', corrected_zcr=True)
        plot_z_migration(_wsi, finalstate='ll', corrected_zcr=True)

