# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_fake_template_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# retrieve physics processes
physicsProcesses = get_processes(no_fake=True)

### define your selection objects 
sels = get_selections(
    channels=('e1p', 'e3p', 'mu1p', 'mu3p'), 
    regions='anti_tau')

sels += get_selections(
    channels=('emu', 'mue'), 
    regions='anti_isol')

sels += get_selections(
    channels=('1p1p', '1p3p', '3p1p', '3p3p'), 
    regions=('fake_factors_hh_same_sign_anti_lead_tau', 'fake_factors_hh_same_sign_anti_sublead_tau', 'fake_factors_hh_same_sign_anti_lead_tau_loose', 'fake_factors_hh_same_sign_anti_sublead_tau_loose'))


# define your list of variables
variables = [
    VARIABLES['mmc_mlm_m'],
    #VARIABLES['tau_0_pt'],
    #VARIABLES['tau_1_pt'],
    #VARIABLES['higgs_pt'],
    #VARIABLES['ditau_dr'],
    #VARIABLES['ditau_deta'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

# plot making
for var in variables:
    # lephad
    make_fake_template_plot(
        processor, sels, var, 
        channels=('e1p', 'e3p', 'mu1p', 'mu3p'), 
        categories=('boost'),
        title='lephad Boost anti-tau');
    make_fake_template_plot(
        processor, sels, var, 
        channels=('e1p', 'e3p', 'mu1p', 'mu3p'), 
        categories=('vbf'),
        title='lephad VBF anti-tau')
    make_fake_template_plot(
        processor, sels, var,
        channels=('e1p', 'e3p', 'mu1p', 'mu3p'),
        categories=('vh'),
        title='lephad VH anti-tau')
    # emu, mue
    make_fake_template_plot(
        processor, sels, var, 
        channels=('emu', 'mue'), 
        categories=('boost'),
        title='leplep (df) Boost anti-isol');
    make_fake_template_plot(
        processor, sels, var, 
        channels=('emu', 'mue'), 
        categories=('vbf'),
        title='leplep (df) VBF anti-isols')
    make_fake_template_plot(
        processor, sels, var, 
        channels=('emu', 'mue'), 
        categories=('vh'),
        title='leplep (df) VH anti-isols')
    # hadhad
    make_fake_template_plot(
        processor, sels, var, 
        channels=('1p1p', '1p3p', '3p1p', '3p3p'), 
        categories=('boost'),
        title='hadhad Boost anti-ID');
    make_fake_template_plot(
        processor, sels, var, 
        channels=('1p1p', '1p3p', '3p1p', '3p3p'), 
        categories=('vbf'),
        title='hadhad VBF anti-ID')
    make_fake_template_plot(
        processor, sels, var, 
        channels=('1p1p', '1p3p', '3p1p', '3p3p'), 
        categories=('vh'),
        title='hadhad VH anti-ID')

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
