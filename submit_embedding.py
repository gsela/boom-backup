import os
import itertools
import subprocess
import argparse


_VARIABLE_DICT = {
    'tau_0_pt': {'sr': 'tau_0_pt', 'cr': 'tau_0_pt', 'cr_corr': 'tau_0_pt_corr'},
    'tau_1_pt': {'sr': 'tau_1_pt', 'cr': 'tau_1_pt', 'cr_corr': 'tau_1_pt_corr'},
    'jet_0_pt': {'sr': 'jet_0_pt', 'cr': 'jet_0_pt', 'cr_corr': 'jet_0_pt'},
    'jet_1_pt': {'sr': 'jet_1_pt', 'cr': 'jet_1_pt', 'cr_corr': 'jet_1_pt'},
    'tau_0_eta': {'sr': 'tau_0_eta', 'cr': 'tau_0_eta', 'cr_corr': 'eta_corr_0'},
    'tau_1_eta': {'sr': 'tau_1_eta', 'cr': 'tau_1_eta', 'cr_corr': 'eta_corr_1'},
    'dijetditau_dr': {'sr': 'dijetditau_dr', 'cr': 'dijetditau_dr', 'cr_corr': 'dijetditau_dr_corr'},
    'higgs_pt': {'sr': 'higgs_pt', 'cr': 'ditau_pt', 'cr_corr': 'higgs_pt'},
    'mjj_low': {'sr': 'mjj_low', 'cr': 'mjj_low', 'cr_corr': 'mjj_low'},
    'mjj': {'sr': 'mjj', 'cr': 'mjj', 'cr_corr': 'mjj'},
    'met': {'sr': 'met', 'cr': 'met', 'cr_corr': 'met_corr'},
    'pt_total': {'sr': 'pt_total', 'cr': 'pt_total', 'cr_corr': 'pt_total'},
    'norm': {'sr': 'norm', 'cr': 'norm', 'cr_corr': 'norm'},
    'jets_eta_prod_wide': {'sr': 'jets_eta_prod_wide', 'cr': 'jets_eta_prod_wide', 'cr_corr': 'jets_eta_prod_wide'},
    'jets_deta_wide': {'sr': 'jets_deta_wide', 'cr': 'jets_deta_wide', 'cr_corr': 'jets_deta_wide'},
    'n_jets': {'sr': 'n_jets', 'cr': 'n_jets', 'cr_corr': 'n_jets'},
    'n_jets_central': {'sr': 'n_jets_central', 'cr': 'n_jets_central', 'cr_corr': 'n_jets_central'},
    'n_jets_forward': {'sr': 'n_jets_forward', 'cr': 'n_jets_forward', 'cr_corr': 'n_jets_forward'},
    'n_jets_30': {'sr': 'n_jets_30', 'cr': 'n_jets_30', 'cr_corr': 'n_jets_30'},
    'n_jets_central_30': {'sr': 'n_jets_central_30', 'cr': 'n_jets_central_30', 'cr_corr': 'n_jets_central_30'},
    'n_jets_forward_30': {'sr': 'n_jets_forward_30', 'cr': 'n_jets_forward_30', 'cr_corr': 'n_jets_forward_30'},
    'n_jets_40': {'sr': 'n_jets_40', 'cr': 'n_jets_40', 'cr_corr': 'n_jets_40'},
    'n_jets_central_40': {'sr': 'n_jets_central_40', 'cr': 'n_jets_central_40', 'cr_corr': 'n_jets_central_40'},
    'n_jets_forward_40': {'sr': 'n_jets_forward_40', 'cr': 'n_jets_forward_40', 'cr_corr': 'n_jets_forward_40'},
    'colinear_mass': {'sr': 'colinear_mass', 'cr': 'colinear_mass', 'cr_corr': 'colinear_mass_corr'},
    'visible_mass': {'sr': 'visible_mass', 'cr': 'visible_mass', 'cr_corr': 'visible_mass_corr'},
    'jet_0_pt': {'sr': 'jet_0_pt', 'cr': 'jet_0_pt', 'cr_corr': 'jet_0_pt'},
    'jet_1_pt': {'sr': 'jet_1_pt', 'cr': 'jet_1_pt', 'cr_corr': 'jet_1_pt'},
    'jet_2_pt': {'sr': 'jet_2_pt', 'cr': 'jet_2_pt', 'cr_corr': 'jet_2_pt'},
    'jet_0_eta': {'sr': 'jet_0_eta', 'cr': 'jet_0_eta', 'cr_corr': 'jet_0_eta'},
    'jet_1_eta': {'sr': 'jet_1_eta', 'cr': 'jet_1_eta', 'cr_corr': 'jet_1_eta'},
    'vh_tagger': {'sr': 'vh_tagger', 'cr': 'vh_tagger', 'cr_corr': 'vh_tagger_corr'},
    'vbf_tagger': {'sr': 'vbf_tagger', 'cr': 'vbf_tagger', 'cr_corr': 'vbf_tagger'},
    'ditau_deta': {'sr': 'ditau_deta', 'cr': 'ditau_deta', 'cr_corr': 'ditau_deta_corr'},
    'ditau_dphi': {'sr': 'ditau_dphi', 'cr': 'ditau_dphi', 'cr_corr': 'ditau_dphi'},
    'ditau_dr': {'sr': 'ditau_dr', 'cr': 'ditau_dr', 'cr_corr': 'ditau_dr_corr'},
    'dijetditau_pt': {'sr': 'dijetditau_pt', 'cr': 'dijetditau_pt', 'cr_corr': 'dijetditau_pt_corr'},
    'dijet_met_dphi': {'sr': 'dijet_met_dphi', 'cr': 'dijet_met_dphi', 'cr_corr': 'dijet_met_dphi_corr'},
    'higgs_pt_over_dijet_pt': {'sr': 'higgs_pt_over_dijet_pt', 'cr': 'higgs_pt_over_dijet_pt', 'cr_corr': 'higgs_pt_over_dijet_pt'},
    'ditau_met_min_dphi': {'sr': 'ditau_met_min_dphi', 'cr': 'ditau_met_min_dphi', 'cr_corr': 'ditau_met_min_dphi'},
}

def _build_command(
        year,
        process_group_name,
        variable,
        json_file,
        channel=None,
        steering_script='boom_workspace_prod.py',
        z_cr=False,
        flavour = None,
        z_cr_zllqcd_dsid=None,
        corr = False,
        local=False,
        bnl=False,
        naf=False,
        queue='workday',
        outdir='condor'):

    if local:
        executable = 'python'
    elif bnl:
        executable = 'python condor_boom_bnl'
    elif naf:
        executable = 'python condor_boom_naf'
    else: 
        executable = 'python condor_boom'

    _cmd_args = [
        executable,
        steering_script,
        '--year ' + year,
        '--process-group-name ' + process_group_name,
        '--json ' + json_file,
        '--variable ' + variable,
    ]
    
    if channel != None:
        _cmd_args += ['--channel ' + channel]

    if z_cr:
        _cmd_args += ['--zcr --zcr-flavour ' + flavour + ' --channel all ']
        if not corr:
            _cmd_args += ['--zcr-deactivate-corrections ']

        if z_cr_zllqcd_dsid != None:
            _cmd_args += ['--zcr-zllqcd-dsid {}'.format(
                z_cr_zllqcd_dsid)]

    if not local:
        _cmd_args += ['--outdir ' + outdir]
    if not local and not bnl:
        _cmd_args += ['--queue ' + queue]

    if not z_cr:
        _cmd_args += ['--ztt-split-mass']

    return ' '.join(_cmd_args)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('variable')
    parser.add_argument('--year', default=None, nargs='*', choices=['15', '16', '17', '18'])
    parser.add_argument('--channel', default=None, nargs='*', choices=['ll', 'lh', 'hh'])
    parser.add_argument('--json-sr', default='data/wsi/wsi_tina_ztt.json')
    parser.add_argument('--json-cr-ee', default='data/wsi/wsi_tina_zcr_ee.json')
    parser.add_argument('--json-cr-mumu', default='data/wsi/wsi_tina_zcr_mumu.json')
    parser.add_argument('--json-cr-ee-corr', default='data/wsi/wsi_tina_zcr_ee_corr.json')
    parser.add_argument('--json-cr-mumu-corr', default='data/wsi/wsi_tina_zcr_mumu_corr.json')
    parser.add_argument('--queue', default='workday')
    parser.add_argument('--zcr-backgrounds', default=False, action='store_true')
    parser.add_argument('--submit', default=False, action='store_true')
    # submit args
    alternate_sub = parser.add_mutually_exclusive_group()
    alternate_sub.add_argument('--local', default=False, action='store_true')
    alternate_sub.add_argument('--naf', default=False, action='store_true')
    alternate_sub.add_argument('--bnl', default=False, action='store_true')
    args = parser.parse_args()

    if not args.variable in _VARIABLE_DICT.keys():
        raise KeyError
    
        
    if args.channel == None:
        _channels = [
            'll', 
            'lh', 
            'hh', 
        ]
    else:
        _channels = args.channel

    if args.year == None:
        _years = [
            '15 16', 
            '17', 
            '18',
        ]
    else:
        if '15' in args.year and '16' in args.year:
            _years = args.year
            _years.remove('15')
            _years.remove('16')
            _years.append('15 16')
        else:
            _years = args.year

    from boom.batch.utils import expected_jobs
    _cmds = []

    jobs_sr = expected_jobs(
        channel=_channels,
        year=_years,
        process_group=('ZttQCD', 'ZttEWK'),
        variation_group='nominal')
    for _job in jobs_sr:
        _cmd = _build_command(
            _job.year, 
            _job.process,
            _VARIABLE_DICT[args.variable]['sr'], 
            args.json_sr, 
            channel=_job.channel,
            local=args.local,
            bnl=args.bnl,
            naf=args.naf,
            queue=args.queue, 
            outdir='condor_'+args.variable)
        _cmds += [_cmd]

    _processes = ('ZllQCD', 'ZllEWK', 'Data',)
    if args.zcr_backgrounds:
        _processes = ('ZllQCD', 'ZllEWK', 'Data', 'ZttEWK', 'ZttQCD', 'VV', 'Top', 'W')
        
    
    jobs_cr = expected_jobs(
        channel=None,
        year=_years,
        process_group= _processes,
        variation_group='nominal',
        z_cr=True,
        z_cr_flavour=('ee', 'mumu',),
        z_cr_deactivate_corrections=True,
        z_cr_backgrounds=True)
    for _job in jobs_cr:
        if _job.zcr_flavour == 'ee':
            _json_file = args.json_cr_ee
        elif _job.zcr_flavour == 'mumu':
            _json_file = args.json_cr_mumu
        else:
            raise ValueError
        _cmd = _build_command(
            _job.year, 
            _job.process, 
            _VARIABLE_DICT[args.variable]['cr'], 
            _json_file,
            queue=args.queue, 
            z_cr=True, 
            flavour=_job.zcr_flavour,
            corr=False,
            local=args.local,
            bnl=args.bnl,
            naf=args.naf,
            outdir='condor_'+args.variable)
        _cmds += [_cmd]


    jobs_cr_corr = expected_jobs(
        channel=None,
        year=_years,
        process_group= _processes,
        variation_group='nominal',
        z_cr=True,
        z_cr_flavour=('ee', 'mumu',),
        z_cr_backgrounds=True)
    for _job in jobs_cr_corr:
        if _job.zcr_flavour == 'ee':
            _json_file = args.json_cr_ee_corr
        elif _job.zcr_flavour == 'mumu':
            _json_file = args.json_cr_mumu_corr
        else:
            raise ValueError
        _cmd = _build_command(
            _job.year, 
            _job.process, 
            _VARIABLE_DICT[args.variable]['cr_corr'], 
            _json_file,
            queue=args.queue, 
            z_cr=True, 
            corr=True,
            z_cr_zllqcd_dsid=_job.zllqcd_dsid,
            local=args.local,
            bnl=args.bnl,
            naf=args.naf,
            flavour=_job.zcr_flavour,
            outdir='condor_'+args.variable)
        _cmds += [_cmd]

    # launch!
    for i_cmd, _cmd in enumerate(_cmds):
        print '{} / {}: {}'.format(i_cmd + 1, len(_cmds), _cmd)
        if args.submit:
            subprocess.call(_cmd, shell=True)
