# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_fake_template_plot
from boom.selection_utils import get_selections, filter_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# retrieve physics processes
physicsProcesses = get_processes(no_fake=True)

sels = get_selections(
    categories=('preselection'),
    channels=('emu', 'mue'), 
    regions=('same_sign_top', 'same_sign_top_anti_isol'))


# define your list of variables
variables = [
     VARIABLES['norm'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

lines = []
for chan in ('emu', 'mue'):
    for trig in ('single-e', 'single-mu', 'e-mu'):
        print chan, trig
        sels_ss = filter_selections(sels, channels=chan, triggers=trig, regions='same_sign_top')
        hd_ss_isol = processor.get_hist_physics_process('Data', sels_ss, VARIABLES['norm'])
        data_ss_isol = hd_ss_isol.Integral()

        mc_ss_isol = 0
        for p in processor.mc_backgrounds:
            h = processor.get_hist_physics_process(p.name, sels_ss, VARIABLES['norm'])
            mc_ss_isol += h.Integral()


        sels_ss_anti_isol = filter_selections(sels, channels=chan, triggers=trig, regions='same_sign_top_anti_isol')
        hd_ss_anti_isol = processor.get_hist_physics_process('Data', sels_ss_anti_isol, VARIABLES['norm'])
        data_ss_anti_isol = hd_ss_anti_isol.Integral()

        mc_ss_anti_isol = 0
        for p in processor.mc_backgrounds:
            h = processor.get_hist_physics_process(p.name, sels_ss_anti_isol, VARIABLES['norm'])
            mc_ss_anti_isol += h.Integral()

        transfer_factor = (data_ss_isol - mc_ss_isol) / (data_ss_anti_isol - mc_ss_anti_isol) if (data_ss_anti_isol - mc_ss_anti_isol) != 0 else 0.

        line = [chan, trig, data_ss_isol, mc_ss_isol, data_ss_anti_isol, mc_ss_anti_isol, transfer_factor]
        lines.append(line)

from boom.extern.tabulate import tabulate
print tabulate(lines, headers=['channel', 'trigger', 'data (SS isol)', 'MC (SS isol)', 'data (SS anti_isol)', 'MC (SS anti_isol)', 'Transfer factor'])

print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
