# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_Quentin_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.cuts.kinematics import CutInvariantMass_zll_vr

# retrieve physics processes
physicsProcesses = get_processes()
physicsProcesses = filter(lambda p: 'Ztt' in p.name or 'Zll' in p.name, physicsProcesses)



_years = (
#   '15',
#   '16',
   '17',
#   '18',
)

_channels_cr = (
   'ee',
   'mumu',
)

_channels_sr = (
   'emu',
   'mue',
   'e1p',
   'e3p',
   'mu1p',
   'mu3p',
   '1p1p',
   '1p3p',
   '3p1p',
   '3p3p',
)

_categories = (
   'boost',
   'vbf',
)

_regions_cr = (
   'Ztt_cr_ll',
   'Ztt_cr_lh',
   'Ztt_cr_hh',
   'Ztt_cr_corr_ll',
   'Ztt_cr_corr_lh',
   'Ztt_cr_corr_hh',
)

sels_cr = get_selections(
   channels=_channels_cr,
   years=_years,
   categories=_categories,
   regions=_regions_cr)

sels_sr = get_selections(
   channels=_channels_sr,
   years=_years,
   categories=_categories,
   regions='SR')


### define your list of variables
variables = [
#     VARIABLES['pt_total'],
#     VARIABLES['mjj'],
#     VARIABLES['jets_deta'],
#     VARIABLES['jets_eta_prod'],
#     VARIABLES['var_ptjj'],
#     VARIABLES['vbf_tagger'],
     VARIABLES['met'],
#    VARIABLES['met_centrality'],
#     VARIABLES['ditau_dr'],
#     VARIABLES['higgs_pt'],
#     VARIABLES['higgs_pt_res']
#      VARIABLES['mjj'],
#      VARIABLES['jets_deta'],
#      VARIABLES['tau_0_pt'],
#      VARIABLES['tau_1_pt'],
#   VARIABLES['ditau_pt_truth'],
#   VARIABLES['pt_total'],
#   VARIABLES['mjj'],
#   VARIABLES['jets_deta'],
#   VARIABLES['jets_eta_prod'],
#   VARIABLES['var_ptjj'],
#   VARIABLES['vbf_tagger'],
#    VARIABLES['tau_0_pt'],
#    VARIABLES['tau_1_pt'],
#    VARIABLES['tau_0_pt_corr'],
#    VARIABLES['tau_1_pt_corr'],
#   VARIABLES['met'],
#   VARIABLES['ditau_dr'],
#   VARIABLES['ditau_dr_corr'],
   #   VARIABLES['met_centrality'],
#   VARIABLES['taus_pt_ratio'],
#   VARIABLES['taus_pt_ratio_corr'],
#    VARIABLES['higgs_pt'],
#    VARIABLES['ditau_pt_truth'],
#   VARIABLES['met'],
#    VARIABLES['ditau_pt_corr'],
  VARIABLES['met_corr'],
#   VARIABLES['x0'],
#   VARIABLES['x0_corr'],
#   VARIABLES['x1'],
#   VARIABLES['x1_corr'],
#   VARIABLES['ditau_deta'],
#   VARIABLES['ditau_deta_corr'],
#   VARIABLES['tau_0_eta'],
#   VARIABLES['eta_corr_0'],
#   VARIABLES['tau_1_eta'],
#   VARIABLES['eta_corr_1'],
#   VARIABLES['colinear_mass'],
#   VARIABLES['visible_mass'],
#   VARIABLES['visible_mass_corr'],
]

#### processor declaration, booking and runninge
processor = boom_processor(physicsProcesses, sels_sr + sels_cr, variables, verbose=True)
processor.book()
processor.run(n_cores=cpu_count() - 1)

decay_mode_channels = {
   'leplep': {'channels': ('emu', 'mue'), 'cr': 'Ztt_cr_ll', 'cr_corr': 'Ztt_cr_corr_ll'},
   'lephad': {'channels': ('e1p', 'e3p', 'mu1p', 'mu3p'), 'cr': 'Ztt_cr_lh', 'cr_corr': 'Ztt_cr_corr_lh'},
   'hadhad': {'channels': ('1p1p', '1p3p', '3p1p', '3p3p'), 'cr': 'Ztt_cr_hh', 'cr_corr': 'Ztt_cr_corr_hh'},
}

for _cat in _categories:
   for _decay_mode in ('leplep', 'lephad', 'hadhad'):
      title = '{}, {} SRs'.format(_decay_mode, _cat)
      
      sels_decay_mode_sr = get_selections(
         channels=decay_mode_channels[_decay_mode]['channels'],
         years=_years,
         categories=_cat,
         regions='SR')
      
      sels_decay_mode_cr = get_selections(
         channels=_channels_cr,
         years=_years,
         categories=_cat,
         regions=decay_mode_channels[_decay_mode]['cr'])

      sels_decay_mode_cr_corr = get_selections(
         channels=_channels_cr,
         years=_years,
         categories=_cat,
         regions=decay_mode_channels[_decay_mode]['cr_corr'])
      # plot making
      for var in variables:
         make_Quentin_plot(
            processor, 
            sels_decay_mode_sr, 
            sels_decay_mode_cr, 
            sels_decay_mode_cr_corr,
            var, 
            title=title)


print 'closing stores...'
close_stores(physicsProcesses)
print 'done'
